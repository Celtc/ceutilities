#ifndef UNITY_STANDARD_ANIMATED_CORE_FORWARD_INCLUDED
#define UNITY_STANDARD_ANIMATED_CORE_FORWARD_INCLUDED

#ifndef UNITY_STANDARD_CORE_FORWARD_INCLUDED
#define UNITY_STANDARD_CORE_FORWARD_INCLUDED

#if defined(UNITY_NO_FULL_STANDARD_SHADER)
#   define UNITY_STANDARD_SIMPLE 1
#endif

#include "UnityStandardConfig.cginc"

#if UNITY_STANDARD_SIMPLE
    #include "UnityStandardCoreForwardSimple.cginc"
    VertexOutputBaseSimple vertBase (VertexInput v) { return vertForwardBaseSimple(v); }
    VertexOutputForwardAddSimple vertAdd (VertexInput v) { return vertForwardAddSimple(v); }
    half4 fragBase (VertexOutputBaseSimple i) : SV_Target { return fragForwardBaseSimpleInternal(i); }
    half4 fragAdd (VertexOutputForwardAddSimple i) : SV_Target { return fragForwardAddSimpleInternal(i); }
#else
    #include "UnityStandardCore.cginc"
    VertexOutputForwardBase vertBase (VertexInput v) { return vertForwardBase(v); }
    VertexOutputForwardAdd vertAdd (VertexInput v) { return vertForwardAdd(v); }
    half4 fragBase (VertexOutputForwardBase i) : SV_Target { return fragForwardBaseInternal(i); }
    half4 fragAdd (VertexOutputForwardAdd i) : SV_Target { return fragForwardAddInternal(i); }
#endif

#endif // UNITY_STANDARD_CORE_FORWARD_INCLUDED

Vector _uvSpeed;

#if UNITY_STANDARD_SIMPLE
	#include "UnityStandardAnimatedCoreForwardSimple.cginc"
	VertexOutputBaseSimple vertBaseAnimated(VertexInput v) { return vertForwardBaseSimpleAnimated(v, _uvSpeed); }
#else
	#include "UnityStandardAnimatedCore.cginc"
	VertexOutputForwardBase vertBaseAnimated(VertexInput v) { return vertForwardBaseAnimated(v, _uvSpeed); }
#endif

#endif // UNITY_STANDARD_ANIMATED_CORE_FORWARD_INCLUDED
