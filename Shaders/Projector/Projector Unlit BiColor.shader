﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

SHADER "Projector/Unlit Bicolor" {
	Properties {
		_BlackBitColor("Black Bit Color", Color) = (1,1,1,1)
		_WhiteBitColor("White Bit Color", Color) = (1,1,1,1)
		_Bitmask ("Mask (Bitmask)", 2D) = "" { }
	}
	Subshader {
		Blend SrcAlpha OneMinusSrcAlpha
		Tags {"Queue" = "Transparent"}
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
           
            struct v2f {
                float4 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
            };
           
            float4x4 unity_Projector;
            float4x4 unity_ProjectorClip;
           
            v2f vert (float4 vertex : POSITION)
            {
                v2f o;
                o.pos = UnityObjectToClipPos (vertex);
                o.uv = mul (unity_Projector, vertex);
                return o;
            }
           
            sampler2D _Bitmask;
			fixed4 _BlackBitColor;
			fixed4 _WhiteBitColor;

			fixed4 frag(v2f i) : SV_Target
			{
				clip(i.uv.xyw);
				clip(1.0 - i.uv.xy);
				fixed4 texS = tex2Dproj(_Bitmask, UNITY_PROJ_COORD(i.uv));
				if (texS.r < .5)
					return _BlackBitColor;
				else
					return _WhiteBitColor;
			}

            ENDCG
        }
   }
}