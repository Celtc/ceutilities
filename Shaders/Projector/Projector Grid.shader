﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

SHADER "Projector/Grid" {
	Properties {
		_Color("Color Tint", Color) = (1,1,1,1)
		_Tile ("Tile (RGBA)", 2D) = "" { }
		_Colormap ("Colormap (RGBA)", 2D) = "" { }
		_GridSize ("Grid size (Dimension of colormap)", float) = 10
	}
	Subshader 
	{
		Blend SrcAlpha OneMinusSrcAlpha
		Tags {"Queue" = "Transparent"}
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
           
            struct v2f {
                float4 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
            };
           
            float4x4 unity_Projector;
            float4x4 unity_ProjectorClip;
           
            v2f vert (float4 vertex : POSITION)
            {
                v2f o;
                o.pos = UnityObjectToClipPos (vertex);
                o.uv = mul (unity_Projector, vertex);
                return o;
            }
           
			half _GridSize;
			fixed4 _Color;
            sampler2D _Tile;
			sampler2D _Colormap;

			fixed4 frag(v2f i) : SV_Target
			{
				clip(i.uv.xyw);
				clip(1.0 - i.uv.xy);
				fixed4 texS = tex2Dproj(_Tile, UNITY_PROJ_COORD(float4(i.uv.xy * _GridSize, 0, 1)));
				return texS * _Color * tex2Dproj(_Colormap, i.uv);
			}

            ENDCG
        }
   }
}