﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

SHADER "Projector/Grid Channels" {
	Properties {
		_Color("Color Tint", Color) = (1,1,1,1)
		_TileChannelsMap("Tile (RGB)", 2D) = "" { }
		_ColorMapRChannel("Colormap R Channel (RGBA)", 2D) = "" { }
		_ColorMapGChannel("Colormap G Channel (RGBA)", 2D) = "" { }
		_ColorMapBChannel("Colormap B Channel (RGBA)", 2D) = "" { }
		_GridSize ("Grid size (Dimension of colormap)", float) = 10
	}
	Subshader 
	{
		Blend SrcAlpha OneMinusSrcAlpha
		Tags {"Queue" = "Transparent"}
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
           
            struct v2f {
                float4 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
            };
           
            float4x4 unity_Projector;
            float4x4 unity_ProjectorClip;
           
            v2f vert (float4 vertex : POSITION)
            {
                v2f o;
                o.pos = UnityObjectToClipPos (vertex);
                o.uv = mul (unity_Projector, vertex);
                return o;
            }
           
			half _GridSize;
			fixed4 _Color;
            sampler2D _TileChannelsMap;
			sampler2D _ColorMapRChannel;
			sampler2D _ColorMapGChannel;
			sampler2D _ColorMapBChannel;
			
			float3 AdditiveMult(float3 a, float b)
			{
				return 1 + b * (a - 1);
			}

			fixed4 frag(v2f i) : SV_Target
			{
				clip(i.uv.xyw);
				clip(1.0 - i.uv.xy);

				fixed4 channels = tex2Dproj(_TileChannelsMap, UNITY_PROJ_COORD(float4(i.uv.xy * _GridSize, 0, 1)));
				
				fixed4 rawColorR = tex2Dproj(_ColorMapRChannel, i.uv);
				fixed4 rawColorG = tex2Dproj(_ColorMapGChannel, i.uv);
				fixed4 rawColorB = tex2Dproj(_ColorMapBChannel, i.uv);

				clip(rawColorR.a + rawColorG.a + rawColorB.a - 0.01);

				rawColorR = fixed4(AdditiveMult(rawColorR.rgb, channels.r), rawColorR.a * channels.r);
				rawColorG = fixed4(AdditiveMult(rawColorG.rgb, channels.g), rawColorG.a * channels.g);
				rawColorB = fixed4(AdditiveMult(rawColorB.rgb, channels.b), rawColorB.a * channels.b);

				return float4(rawColorR.rgb * rawColorG.rgb * rawColorB.rgb, max(max(rawColorR.a, rawColorG.a), rawColorB.a));
			}

            ENDCG
        }
   }
}