﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

SHADER "Projector/Unlit Transparent" {
	Properties {
		_Color("Color Tint", Color) = (1,1,1,1)
		_Tex ("Base (RGB)", 2D) = "" { }
	}
	Subshader {
		Blend SrcAlpha OneMinusSrcAlpha
		Tags {"Queue" = "Transparent"}
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
           
            struct v2f {
                float4 uv : TEXCOORD0;
                float4 pos : SV_POSITION;
            };
           
            float4x4 unity_Projector;
            float4x4 unity_ProjectorClip;
           
            v2f vert (float4 vertex : POSITION)
            {
                v2f o;
                o.pos = UnityObjectToClipPos (vertex);
                o.uv = mul (unity_Projector, vertex);
                return o;
            }
           
            sampler2D _Tex;
			fixed4 _Color;

			fixed4 frag(v2f i) : SV_Target
			{
				clip(i.uv.xyw);
				clip(1.0 - i.uv.xy);

				fixed4 texS = tex2Dproj(_Tex, UNITY_PROJ_COORD(i.uv));
				return texS * _Color;
			}

            ENDCG
        }
   }
}