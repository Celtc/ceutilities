﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Colorize" 
{
	Properties 
	{
		_MainTex ("Texture", 2D) = "white" {}
		_ColorA ("Color A", Color) = (1,1,1,1)
		_ColorB ("Color B", Color) = (1,1,1,1)
		_ColorC ("Color C", Color) = (1,1,1,1)
	}
 
    SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
 
		Pass 
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};
 
			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _ColorA;
			fixed4 _ColorB;
			fixed4 _ColorC;
 
			v2f vert (appdata v) 
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
 
			fixed4 frag (v2f i) : COLOR 
			{				
				float3 pixcol = tex2D(_MainTex, i.uv);
				float3 endcol = float3(1.0, 1.0, 1.0);

				float3 colors[3];
				colors[0] = _ColorA;
				colors[1] = _ColorB;
				colors[2] = _ColorC;

				float lum = dot(pixcol, 0.33);
				float ix;
				if (lum < 0.5)
				{
					ix = 0;
				}
				else if (lum > 0.5)
				{
					ix = 1;
				}
				endcol = lerp(colors[ix], colors[ix + 1], (lum - float(ix) * 0.5) / 0.5);
				return float4(endcol, 1.0);
			}
			ENDCG
		}
    }
 }