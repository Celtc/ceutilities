﻿// Upgrade NOTE: upgraded instancing buffer 'MyProperties' to new syntax.

Shader "Billboard/Diffuse/Atlas Colored"
{
    Properties
    {
        _Color("Color", Color) = (1, 1, 1, 1)
        _MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
        _Cutoff("Alpha cutoff", Range(0,1)) = 0.15
        _Cells("X= Columns, Y=Rows, Z=Speed", Vector) = (8, 5, 10, 0)
        _CurrentRow("Current Row", float) = 1.0
        _StartFrame("Start Frame", float) = 1.0
    }
    SubShader
    {
        Tags
		{ 
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "TransparentCutout"
			"LightMode" = "ForwardBase" 
		}

        LOD 300
        Cull Back
 
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "UnityLightingCommon.cginc" // for _LightColor0
            #pragma multi_compile_fwdbase
            #include "AutoLight.cginc"
 
            struct appdata_t
            {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                fixed3 color : COLOR0;
                float2 texcoord : TEXCOORD0;
            };
 
            struct v2f
            {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 pos : SV_POSITION;
                fixed4 diff : COLOR0; //Diffuse lighting color
                float2 texcoord : TEXCOORD0;
                fixed3 color : TEXCOORD1;
                float3 lightDir : TEXCOORD2;
                float3 normal : TEXCOORD3;
                LIGHTING_COORDS(4, 5)
            };
 
            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed _Cutoff;
            float4 _Cells;
 
            UNITY_INSTANCING_BUFFER_START(MyProperties)
                UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color)
#define _Color_arr MyProperties
                UNITY_DEFINE_INSTANCED_PROP(half, _CurrentRow)
#define _CurrentRow_arr MyProperties
                UNITY_DEFINE_INSTANCED_PROP(half, _StartFrame)
#define _StartFrame_arr MyProperties
            UNITY_INSTANCING_BUFFER_END(MyProperties)
 
            // Rotate always towards camera
            void Billboard(inout appdata_t v)
            {
				// Get the camera basis vectors
				const float3 right = UNITY_MATRIX_V[0].xyz;
				const float3 up = UNITY_MATRIX_V[1].xyz;
				const float3 forward = -UNITY_MATRIX_V[2].xyz;

				// Rotate to face camera
				const float4x4 rotationMatrix = float4x4(right, 0,
					up, 0,
					forward, 0,
					0, 0, 0, 1);

				v.vertex = mul(v.vertex, rotationMatrix);
				v.normal = mul(v.normal, rotationMatrix);
            }
 
            // mMves the sprite sheet from left to right restricted to a single row specified by _CurrentRow.
            void AnimateUV(appdata_t v, inout v2f o)
            {
				//Advance the frame number
                float frame = UNITY_ACCESS_INSTANCED_PROP(_StartFrame_arr, _StartFrame) + floor(_Time.g * _Cells.z); 
 
				//Update the columns based on time per frame
                o.texcoord.x += fmod(frame, _Cells.x); 
				//Keep to the specified row
                o.texcoord.y += fmod(UNITY_ACCESS_INSTANCED_PROP(_CurrentRow_arr, _CurrentRow), _Cells.y);
                o.texcoord /= _Cells.xy;
            }
 
            v2f vert(appdata_t v)
            {
                v2f o;

                UNITY_INITIALIZE_OUTPUT(v2f, o); 
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);
 
                Billboard(v);
 
                o.pos = UnityObjectToClipPos(v.vertex);
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
 
                AnimateUV(v, o);
 
                o.lightDir = normalize(ObjSpaceLightDir(v.vertex));
                o.normal = normalize(v.normal).xyz;
 
                TRANSFER_VERTEX_TO_FRAGMENT(o);
 
                return o;
            }
 
            fixed4 frag(v2f i) : SV_Target
            {
                float3 L = normalize(i.lightDir);
                float3 N = normalize(i.normal);
 
                float attenuation = LIGHT_ATTENUATION(i) * 0.75;
                float4 ambient = UNITY_LIGHTMODEL_AMBIENT * 0.5;
 
                float NdotL = saturate(dot(N, L));
                float4 diffuseTerm = NdotL * _LightColor0 * UNITY_ACCESS_INSTANCED_PROP(_Color_arr, _Color) * attenuation;
 
                float4 diffuse = tex2D(_MainTex, i.texcoord);
                clip(diffuse.a - _Cutoff); //Deal with alpha.
 
                float4 finalColor = (ambient + diffuseTerm) * diffuse;
 
                return finalColor;
            }
            ENDCG
        }
    }
    Fallback "VertexLit" //This is required because the shadow 'finalize' is handled in existing shaders, so use the simpliest one possible.
}
 