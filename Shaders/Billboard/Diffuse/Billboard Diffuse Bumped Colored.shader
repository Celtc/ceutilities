﻿// Upgrade NOTE: upgraded instancing buffer 'MyProperties' to new syntax.

Shader "Billboard/Diffuse/Bumped Colored" 
{ 
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5
        _MainTex("Albedo Map", 2D) = "white" {}
        _BumpMap("Normal Map", 2D) = "bump" {}
        _BumpIntensity("NormalMap Intensity", Float) = 1
    }
     
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "TransparentCutout"
            //"DisableBatching" = "True"
        }
 
        LOD 300
        Cull Back
        Lighting On
        ZWrite On
 
        CGPROGRAM
        #pragma target 3.0
        #pragma surface surf Lambert alpha vertex:vert alphatest:_Cutoff fullforwardshadows
 
        sampler2D _MainTex;
        sampler2D _BumpMap;
        fixed _BumpIntensity;
 
        UNITY_INSTANCING_BUFFER_START(MyProperties)
            UNITY_DEFINE_INSTANCED_PROP(fixed4, _Color)
#define _Color_arr MyProperties
        UNITY_INSTANCING_BUFFER_END(MyProperties)
 
        struct Input
        {
            float2 uv_MainTex;
            float2 uv_BumpMap; 
            UNITY_VERTEX_INPUT_INSTANCE_ID
        };
 
        void vert(inout appdata_full v, out Input OUT)
        {
            UNITY_INITIALIZE_OUTPUT(Input, OUT)
            UNITY_SETUP_INSTANCE_ID(v);
            UNITY_TRANSFER_INSTANCE_ID(v, OUT);
			
			// Get the camera basis vectors
			float3 right = UNITY_MATRIX_V[0].xyz;
			float3 up = UNITY_MATRIX_V[1].xyz;
			float3 forward = -UNITY_MATRIX_V[2].xyz;

			// Rotate to face camera
			float4x4 rotationMatrix = float4x4(right, 0,
				up, 0,
				forward, 0,
				0, 0, 0, 1);

            v.vertex = mul(v.vertex, rotationMatrix);
            v.normal = mul(v.normal, rotationMatrix);
        }
 
        void surf(Input IN, inout SurfaceOutput o)
        {
			UNITY_SETUP_INSTANCE_ID(IN);
            half4 albedo = tex2D(_MainTex, IN.uv_MainTex) *  UNITY_ACCESS_INSTANCED_PROP(_Color_arr, _Color);
            o.Albedo = albedo.rgb;
            o.Alpha = albedo.a;
			o.Normal = lerp(
				UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap)),
				fixed3(0, 0, 1), 
				-_BumpIntensity + 1				
			);
            o.Normal = normalize((half3)o.Normal);
        }
 
        half4 LightingSimpleLambert(SurfaceOutput s, half3 lightDir, half atten)
        {
            half4 c;
            c.rgb = s.Albedo * _LightColor0.rgb * (atten);
            c.a = s.Alpha;
            return c;
        }
 
        ENDCG
    }
        Fallback "Off"
}