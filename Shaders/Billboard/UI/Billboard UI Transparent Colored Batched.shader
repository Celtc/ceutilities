Shader "Billboard/UI/Transparent Colored Batched"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
		[KeywordEnum(None, Y, Z)] _Cylindrical("Cylindrical Billboard", Float) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)] unity_GUIZTestMode("ZTest", Float) = 4

        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255

        _ColorMask ("Color Mask", Float) = 15

        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
			"DisableBatching" = "False"
        }

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest [unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask [_ColorMask]

        Pass
        {
            Name "Default"
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0

            #include "UnityCG.cginc"
            #include "UnityUI.cginc"

            #pragma multi_compile __ UNITY_UI_ALPHACLIP
			#pragma multi_compile _CYLINDRICAL_NONE _CYLINDRICAL_Y _CYLINDRICAL_Z

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
				half2 pivotXY : TEXCOORD1;
				half2 pivotZW : TEXCOORD2;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            fixed4 _Color;
            fixed4 _TextureSampleAdd;
            float4 _ClipRect;
			
			// Make the vertex look towards the camera
			void Billboard(inout appdata_t v)
			{
				// Baked position
				const float4 local = mul(unity_WorldToObject, (float4(v.pivotXY.x, v.pivotXY.y, v.pivotZW.x, 0)));
				const float4 offset = v.vertex - local;

				// Get the camera basis vectors
				const float3 right = UNITY_MATRIX_V[0].xyz;
#ifdef _CYLINDRICAL_Y
				const float3 up = float3(0, 1, 0);
#elif _CYLINDRICAL_Z
				const float3 up = float3(UNITY_MATRIX_V[1].x, 0, UNITY_MATRIX_V[1].z);
#else
				const float3 up = UNITY_MATRIX_V[1].xyz;
#endif
				const float3 forward = -UNITY_MATRIX_V[2].xyz;				

				// Rotate to face camera
				const float4x4 rotationMatrix = float4x4(
					normalize(mul(unity_WorldToObject, right).xyz), 0,
					normalize(mul(unity_WorldToObject, up).xyz), 0,
					normalize(mul(unity_WorldToObject, forward).xyz), 0,
					0, 0, 0, 1);				

				v.vertex = mul(offset, rotationMatrix) + local;
			}

            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);

				Billboard(v);

                OUT.worldPosition = v.vertex;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

                OUT.texcoord = v.texcoord;

                OUT.color = v.color * _Color;
                return OUT;
            }

            sampler2D _MainTex;

            fixed4 frag(v2f IN) : SV_Target
            {
                half4 color = (tex2D(_MainTex, IN.texcoord) + _TextureSampleAdd) * IN.color;

                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);

                #ifdef UNITY_UI_ALPHACLIP
                clip (color.a - 0.001);
                #endif

                return color;
            }
        ENDCG
        }
    }
}