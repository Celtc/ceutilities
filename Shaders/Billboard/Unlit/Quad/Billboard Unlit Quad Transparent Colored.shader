﻿ Shader "Billboard/Unlit/Quad/Transparent Colored" 
 {
    Properties 
	{
		_Color("Color", Color) = (1,1,1,1)
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    SubShader 
	{
        Tags 
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent"
			"DisableBatching"="True"
		}

        pass
		{
			Cull Back
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			fixed4 _Color;

			struct appdata 
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD;
				fixed4 color : COLOR;
			};

			struct v2f 
			{
				float4 pos : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				fixed4 color : COLOR;
			};

			v2f vert(appdata input)
			{
				v2f output;

				const float scaleX = length(mul(unity_ObjectToWorld, float4(1.0, 0.0, 0.0, 0.0)));
				const float scaleY = length(mul(unity_ObjectToWorld, float4(0.0, 1.0, 0.0, 0.0)));

				output.pos = mul(UNITY_MATRIX_P,
					float4(UnityObjectToViewPos(float3(0.0, 0.0, 0.0)), 1)
					- float4(input.vertex.x, input.vertex.y, 0.0, 0.0)
					* float4(scaleX, scaleY, 1.0, 1.0)
				);

				output.color = input.color * _Color;
				output.texcoord = 1 - input.texcoord;

				return output;
			}

			float4 frag(v2f IN):COLOR
			{
				return tex2D(_MainTex, IN.texcoord) * IN.color;
			}

			ENDCG
        }//endpass
    }
}
 