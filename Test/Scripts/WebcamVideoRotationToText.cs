﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CEUtilities.API;
using UnityEngine.UI;

namespace CEUtilities.Tests
{
    public class WebcamVideoRotationToText : MonoBehaviour
    {
        #region Exposed fields
        
        [SerializeField]
        private Webcam webcam;

        [SerializeField]
        private Text text;

        #endregion Exposed fields

        #region Internal fields

        int currValue = int.MinValue;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Webcam Webcam
        {
            get
            {
                if (webcam == null)
                    webcam = FindObjectOfType<Webcam>();

                return webcam;
            }

            set
            {
                webcam = value;
            }
        }

        public Text Text
        {
            get
            {
                return text;
            }

            set
            {
                text = value;
            }
        }

        #endregion Properties

        #region Events methods

        private void Update()
        {
            if (Text != null && Webcam != null)
            {
                if (currValue != Webcam.WebcamTex.videoRotationAngle)
                {
                    currValue = Webcam.WebcamTex.videoRotationAngle;
                    Text.text = currValue.ToString();
                }
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}
