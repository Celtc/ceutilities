﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using Sirenix.Serialization;
using CEUtilities.API;
using System;

namespace CEUtilities.Tests
{
    public class ContextObjectTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private int predefinedIntValue = 3;

        [SerializeField]
        [HideInInspector]
        private Vector2 predefinedVector2Value = Vector2.one;

        [OdinSerialize]
        [HideInInspector]
        private ContextObject<int> contextObject;

        [OdinSerialize]
        [HideInInspector]
        private ContextObject<int, Vector2, GameObject> contextObjectMultipleTypes;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        [ShowInInspector]
        public int PredefinedIntValue
        {
            get
            {
                return predefinedIntValue;
            }

            set
            {
                predefinedIntValue = value;
            }
        }

        [ShowInInspector]
        public Vector2 PredefinedVector2Value
        {
            get
            {
                return predefinedVector2Value;
            }

            set
            {
                predefinedVector2Value = value;
            }
        }

        [ShowInInspector]
        public ContextObject<int> ContextObject
        {
            get
            {
                return contextObject;
            }

            set
            {
                contextObject = value;
            }
        }

        [ShowInInspector]
        public ContextObject<int, Vector2, GameObject> ContextObjectMultipleTypes
        {
            get
            {
                return contextObjectMultipleTypes;
            }

            set
            {
                contextObjectMultipleTypes = value;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Resets the context object.
        /// </summary>
        [ButtonGroup("Simple")]
        public void ResetContextObject()
        {
            ContextObject = new ContextObject<int>(PredefinedIntValue);
        }

        /// <summary>
        /// Adds the parameter.
        /// </summary>
        [Button]
        [ButtonGroup("Simple")]
        public void AddParameter()
        {
            if (ContextObject != null)
            {
                ContextObject
                    .AddParameter(new DynamicParameter<int>("Predifined Per Two", () => PredefinedIntValue * 2))
                    .AddParameter(new DynamicParameter<Vector2>("Predifined Vector2", () => PredefinedVector2Value));
            }
        }

        /// <summary>
        /// Prints the value.
        /// </summary>
        [Button]
        [ButtonGroup("Simple")]
        public void PrintValue()
        {
            Debug.Log("[ContextObjectTest] Context object value: " + ContextObject.Value);
        }


        /// <summary>
        /// Resets the context object.
        /// </summary>
        [ButtonGroup("Multiple")]
        public void ResetContextObjectMultiple()
        {
            ContextObjectMultipleTypes = new ContextObject<int, Vector2, GameObject>(PredefinedVector2Value);
        }

        /// <summary>
        /// Adds the parameter.
        /// </summary>
        [Button]
        [ButtonGroup("Multiple")]
        public void AddParameterMultiple()
        {
            if (ContextObjectMultipleTypes != null)
            {
                ContextObjectMultipleTypes
                    .AddParameter(new DynamicParameter<int>("Predifined Per Two", () => PredefinedIntValue * 2))
                    .AddParameter(new DynamicParameter<Vector2>("Predifined Vector2", () => PredefinedVector2Value));

                ContextObjectMultipleTypes.AddParameter
                (
                    DynamicParameter.Create("Procedural Type Test", typeof(GameObject), () => gameObject)
                );
            }
        }

        /// <summary>
        /// Prints the value.
        /// </summary>
        [Button]
        [ButtonGroup("Multiple")]
        public void PrintValueMultiple()
        {
            Debug.Log("[ContextObjectTest] Context object multiple types value: " + ContextObjectMultipleTypes.Value);
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}