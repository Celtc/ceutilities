﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Tests
{
    [DefaultExecutionOrder(100)]
    public class Script5 : MonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        bool calledUpdate = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private void Awake()
        {
            Debug.Log("Script5 Awaken!", this);
        }

        private void Update()
        {
            if (!calledUpdate)
            {
                Debug.Log("Script5 Updated!", this);
                calledUpdate = true;
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}