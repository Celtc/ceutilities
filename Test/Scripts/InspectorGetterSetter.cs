﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class InspectorGetterSetter : SerializedMonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        [ShowInInspector]
        [SerializeField]
        public bool Boolean { get; }

        [ShowInInspector]
        [SerializeField]
        public float Value { get; set; }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Methods

        #endregion Methods
    }
}