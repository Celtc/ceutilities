﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace CEUtilities.Tests
{
    [System.Serializable]
    public class A
    {
        [SerializeField]
        public string someString = "Some string";

        [SerializeField]
        public bool someBool = true;

        [SerializeField]
        public A otherReference;
    }
    
    public class RecursiveNonObjectSerializationTest : SerializedMonoBehaviour
    {
        #region Exposed fields

#pragma warning disable 414

        [OdinSerialize]
        internal List<A> instances = new List<A>();

        [OdinSerialize]
        private A reference1;

        [OdinSerialize]
        private A reference2;

#pragma warning restore 414

#if UNITY_EDITOR

        [Button]
        public void SetReferences()
        {
            if (instances != null && instances.Count > 0)
            {
                reference1 = instances[0];
                reference2 = reference1;
            }
        }
#endif

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}