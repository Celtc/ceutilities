﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CEUtilities.API;
using UnityEngine.UI;

namespace CEUtilities.Tests
{
    public class WebcamFrontFacingToText : MonoBehaviour
    {
        #region Exposed fields
        
        [SerializeField]
        private Webcam webcam;

        [SerializeField]
        private Text text;

        #endregion Exposed fields

        #region Internal fields

        bool currValue = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Webcam Webcam
        {
            get
            {
                if (webcam == null)
                    webcam = FindObjectOfType<Webcam>();

                return webcam;
            }

            set
            {
                webcam = value;
            }
        }

        public Text Text
        {
            get
            {
                return text;
            }

            set
            {
                text = value;
            }
        }

        #endregion Properties

        #region Events methods

        private void Start()
        {
            if (Text != null)
                Text.text = currValue.ToString();
        }

        private void Update()
        {
            if (Text != null && Webcam != null)
            {
                if (currValue != Webcam.FrontFacing)
                {
                    currValue = Webcam.FrontFacing;
                    Text.text = currValue.ToString();
                }
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}
