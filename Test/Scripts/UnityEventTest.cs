﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace CEUtilities.Tests
{
    [System.Serializable]
    public class UnityEvent_Custom : UnityEvent<int> { }

    [System.Serializable]
    public class SubClass
    {
        public UnityEvent_Custom OnPressedPositive = new UnityEvent_Custom();

        public UnityEvent_Custom OnPresseNegative = new UnityEvent_Custom();
    }

    public class UnityEventTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        [OdinSerialize]
        private SubClass events;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}