﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class RegexAttributeTest : MonoBehaviour
    {
        #region Exposed fields

        [Regex(@"^(?:\d{1,3}\.){3}\d{1,3}$", "Invalid IPv4 address!\nExample: '127.0.0.1'")]
        public string toCheckIPv4 = "127.0.0.1";

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}