﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace CEUtilities.Tests.Performance
{
    [ExecuteInEditMode]
    public class ArrayTest : TesterBehaviour
    {
        #region Exposed fields

        int[] array;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public override bool IsInit
        {
            get
            {
                return base.IsInit && array != null; 
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        public override void Init()
        {
            base.Init();

            array = Enumerable.Repeat(UnityEngine.Random.Range(0, 1000), 1000).ToArray();
        }

        public void Test_ForEach()
        {
            int num = 2;
            array.ForEach((x) => x += num);
        }

        public void Test_Loop()
        {
            int num = 2;
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += num;
            }
        }

        public void Test_Length()
        {
            int count = 0;
            for (int i = 0; i < 1000; i++)
            {
                count = array.Length;
            }
        }

        public void Test_IndexOf()
        {
            var element = array.ToArray()[500];
            for (int i = 0; i < 1000; i++)
            {
                array.IndexOf(element);
            }
        }

        public void Test_Add()
        {
            int[] newArray = (int[])array.Clone();
            for (int i = 0; i < 1000; i++)
            {
                Array.Resize(ref newArray, newArray.Length + 1);
                newArray[i] = 66;
            }
        }

        #endregion Methods
    }
}