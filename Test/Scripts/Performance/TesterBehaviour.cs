﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Tests.Performance
{
    [ExecuteInEditMode]
    public abstract class TesterBehaviour : MonoBehaviour
    {
        #region Exposed fields

        protected bool isInit = false;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public virtual bool IsInit
        {
            get
            {
                return isInit;
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Prepare for test
        /// </summary>
        public virtual void Init()
        {
            isInit = true;
        }

        /// <summary>
        /// Empty method for avoiding unused vars warning
        /// </summary>
        protected virtual void UseVar(object var) { }

        #endregion Methods
    }
}