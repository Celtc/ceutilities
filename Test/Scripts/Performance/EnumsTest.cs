﻿using UnityEngine;
using UnityEngine.Profiling;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Tests.Performance
{
    [ExecuteInEditMode]
    public class EnumsTest : TesterBehaviour
    {
        [System.Flags]
        enum TestEnum
        {
            A = 0,
            B = 1 << 0,
            C = 1 << 1,
            D = 1 << 2
        }

        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        public void Test_FlagsCheck()
        {
            var obstacles = TestEnum.A | TestEnum.B;
            
            for (int i = 0; i < 1000; i++)
            {
                if (obstacles.HasAnyFlag<TestEnum>(TestEnum.A))
                {
                    //
                }
            }
        }

        #endregion Methods
    }
}