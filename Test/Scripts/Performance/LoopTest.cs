﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CEUtilities.Tests.Performance
{
    [ExecuteInEditMode]
    public class LoopTest : TesterBehaviour
    {
        #region Exposed fields

        IEnumerable<int> enumerable;
        List<int> list;
        int[] array;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public override bool IsInit
        {
            get
            {
                return base.IsInit && enumerable != null; 
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        public override void Init()
        {
            base.Init();

            enumerable = Enumerable.Repeat(default(int), 100000);
            array = enumerable.ToArray();
            list = enumerable.ToList();
        }

        public void Test_IEnumerable()
        {
            int num = 0;
            foreach (var elem in enumerable)
            {
                num += elem;
            }
        }

        public void Test_ArrayFor()
        {
            int num = 0;
            for (int i = 0; i < array.Length; i++)
            {
                num += array[i];
            }
        }

        public void Test_ListFor()
        {
            int num = 0;
            for (int i = 0; i < list.Count; i++)
            {
                num += list[i];
            }
        }

        public void Test_ArrayForeach()
        {
            int num = 0;
			foreach(var x in array)
			{
				num += x;
			}
        }

        public void Test_ListForeach()
        {
            int num = 0;
			foreach(var x in list)
			{
				num += x;
			}
        }

        public void Test_ArrayForEach()
        {
            int num = 0;
			array.ForEach(x => num += x);
        }

        public void Test_ListForEach()
        {
            int num = 0;
			list.ForEach(x => num += x);
        }

        #endregion Methods
    }
}