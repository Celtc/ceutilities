﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace CEUtilities.Tests.Performance
{
    [ExecuteInEditMode]
    public class ReadOnlyCollection : TesterBehaviour
    {
        #region Exposed fields

        List<int> list;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public override bool IsInit
        {
            get
            {
                return base.IsInit && List != null; 
            }
        }

		public List<int> List
		{
			get
			{
				return list;
			}

			set
			{
				list = value;
			}
		}

		public ReadOnlyCollection<int> ReadOnlyList
		{
			get
			{
				return list.AsReadOnly();
			}
		}

		#endregion Properties

		#region Unity events

		#endregion Unity events

		#region Methods

		public override void Init()
        {
            base.Init();

            List = Enumerable.Repeat(Random.Range(0, 1000), 10000).ToList();
        }

        public void Test_LoopDirect()
        {
            int num = 2;
            for(int i = 0; i < list.Count; i++)
            {
                list[i] += num;
            }
        }

        public void Test_LoopGetter()
        {
            int num = 2;
            for(int i = 0; i < List.Count; i++)
            {
				num += List[i];
            }
        }

        public void Test_LoopReadOnlyGetter()
        {
            int num = 2;
            for(int i = 0; i < ReadOnlyList.Count; i++)
            {
				num += ReadOnlyList[i];
            }
        }

        #endregion Methods
    }
}