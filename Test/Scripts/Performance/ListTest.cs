﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CEUtilities.Tests.Performance
{
    [ExecuteInEditMode]
    public class ListTest : TesterBehaviour
    {
        #region Exposed fields

        List<int> list;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public override bool IsInit
        {
            get
            {
                return base.IsInit && list != null; 
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        public override void Init()
        {
            base.Init();

            list = Enumerable.Repeat(Random.Range(0, 1000), 1000).ToList();
        }

        public void Test_ForEach()
        {
            int num = 2;
            list.ForEach((x) => x += num);
        }

        public void Test_Loop()
        {
            int num = 2;
            for(int i = 0; i < list.Count; i++)
            {
                list[i] += num;
            }
        }

        public void Test_Count()
        {
            int count = 0;
            for (int i = 0; i < 1000; i++)
            {
                count = list.Count;
            }
        }

        public void Test_IndexOf()
        {
            var element = list.ToArray()[500];
            for (int i = 0; i < 1000; i++)
            {
                list.IndexOf(element);
            }
        }

        public void Test_Add()
        {
            var newList = new List<int>(list);
            for (int i = 0; i < 1000; i++)
            {
                newList.Add(66);
            }
        }

        #endregion Methods
    }
}