﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CEUtilities.Tests.Performance
{
    [ExecuteInEditMode]
    public class IEnumerableTest : TesterBehaviour
    {
        #region Exposed fields

        IEnumerable<int> enumerable;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public override bool IsInit
        {
            get
            {
                return base.IsInit && enumerable != null; 
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        public override void Init()
        {
            base.Init();

            enumerable = Enumerable.Repeat(Random.Range(0, 1000), 1000);
        }

        public void Test_ForEach()
        {
            int num = 2;
            enumerable.ForEach<int>((x) => x += num);
        }

        public void Test_GetCount()
        {
            int count = 0;
            for (int i = 0; i < 1000; i++)
            {
                count = enumerable.GetCount();
            }

            UseVar(count);
        }

        public void Test_IndexOf()
        {
            var element = enumerable.ToArray()[500];
            for (int i = 0; i < 1000; i++)
            {
                enumerable.IndexOf(element);
            }
        }

        #endregion Methods
    }
}