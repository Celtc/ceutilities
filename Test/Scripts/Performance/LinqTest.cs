﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace CEUtilities.Tests.Performance
{
    [ExecuteInEditMode]
    public class LinqTest : TesterBehaviour
    {
        public struct TestStruct
        {
            public int number;
            public string text;
        }

        public class TestClass
        {
            public int number;
            public string text;
        }

        #region Exposed fields
        
        TestClass[] arrayOfClass;
        TestStruct[] arrayOfStruct;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public override bool IsInit
        {
            get
            {
                return base.IsInit && arrayOfClass != null && arrayOfStruct != null;
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        public override void Init()
        {
            base.Init();

            arrayOfClass = Enumerable.Repeat(new TestClass(), 1000).ToArray();
            arrayOfStruct = Enumerable.Repeat(default(TestStruct), 1000).ToArray();
        }

        public void Test_ClassLinq()
        {
            var texts = arrayOfClass.Select(x => x.text).ToArray();

            UseVar(texts);
        }

        public void Test_ClassLoop()
        {
            string[] texts = new string[arrayOfClass.Length];
            for (int i = 0; i < arrayOfClass.Length; i++)
            {
                texts[i] = arrayOfClass[i].text;
            }

            UseVar(texts);
        }

        public void Test_StructLinq()
        {
            var texts = arrayOfStruct.Select(x => x.text).ToArray();

            UseVar(texts);
        }

        public void Test_StructLoop()
        {
            string[] texts = new string[arrayOfStruct.Length];
            for (int i = 0; i < arrayOfStruct.Length; i++)
            {
                texts[i] = arrayOfStruct[i].text;
            }
        }

        public void Test_ClassPreListLoop()
        {
            string[] texts;
            var textsList = new List<string>();
            for (int i = 0; i < arrayOfClass.Length; i++)
            {
                textsList.Add(arrayOfClass[i].text);
            }
            texts = textsList.ToArray();

            UseVar(texts);
        }

        public void Test_ClassPreFullArrayLoop()
        {
            string[] texts = new string[arrayOfClass.Length + 50];
            int length = 0;
            for (int i = 0; i < arrayOfClass.Length; i++)
            {
                texts[i] = arrayOfStruct[i].text;
                length++;
            }
            Array.Resize(ref texts, length);
        }

        public void Test_ClassPreArrayListLoop()
        {
            string[] texts;
            var textsList = new ArrayList();
            for (int i = 0; i < arrayOfClass.Length; i++)
            {
                textsList.Add(arrayOfClass[i].text);
            }
            texts = (string[])textsList.ToArray(typeof(string));

            UseVar(texts);
        }
        
        #endregion Methods
    }
}