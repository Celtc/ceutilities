﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace CEUtilities.Tests
{
    public class FuncSerializationTest : SerializedMonoBehaviour
    {
        #region Exposed fields
        
        [OdinSerialize]
        private Func<int, AudioClip> provider;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}