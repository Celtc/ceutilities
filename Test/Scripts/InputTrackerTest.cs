﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.IO;

namespace CEUtilities.Tests
{
    public class InputTrackerTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        public string trackerID;
        public string virtualKey;
        public bool everyFrame = false;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private void Update()
        {
            if (everyFrame)
                CheckInputState();
        }

        #endregion Events methods

        #region Public Methods

        [HideInEditorMode]
        [Button]
        public void CheckInputState()
        {
            var manager = InputTrackerManager.Instance;
            var tracker = manager[trackerID];
            var input = tracker[virtualKey];
            if (input != null)
                Debug.Log(input.State);
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}