﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace CEUtilities.Tests
{
    public interface IMyEvent
    {
        void Invoke();
    }

    [InlineProperty(LabelWidth = 50)]
    public class MyEvent : IMyEvent
    {
        [SerializeField, HideLabel, ShowIf("BaseClass")]
        private Action action;

        protected virtual bool BaseClass { get { return true; } }

        public virtual void Invoke()
        {
            action();
        }
    }

    [InlineProperty(LabelWidth = 50)]
    public abstract class MyEvent<T> : MyEvent, IMyEvent
    {
        [SerializeField, HideLabel]
        private Action<T> action;

        [SerializeField]
        private T arg1;

        protected override bool BaseClass { get { return false; } }

        public override void Invoke()
        {
            action(arg1);
        }
    }

    [System.Serializable]
    public class MyEvent_Int : MyEvent<int> { }

    [System.Serializable]
    public class DelegatesContainer
    {
        public IMyEvent DelegateA = new MyEvent_Int();

        public IMyEvent DelegateB = new MyEvent_Int();
    }

    public class DelegatesSerializationTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        [OdinSerialize]
        private DelegatesContainer events;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}