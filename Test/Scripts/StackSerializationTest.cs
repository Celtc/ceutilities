﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace CEUtilities.Tests
{
    public class StackSerializationTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        [OdinSerialize]
        [HideInInspector]
        private Stack<GameObject> stack = new Stack<GameObject>();

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        [ShowInInspector]
        public int StackCount
        {
            get
            {
                return stack.Count;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [Button]
        public void FillStack()
        {
            for (int i = 0; i < 10; i++)
            {
                var go = new GameObject("GO" + i);
                go.transform.SetParent(transform);
                stack.Push(go);
            }
        }

        [Button]
        public void EmptyStack()
        {
            foreach (var go in stack)
                Utilities.Destroy(go);

            stack.Clear();
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}