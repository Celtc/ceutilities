﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Audio;

namespace CEUtilities.Tests
{
    public class MixerManagerTest : MonoBehaviour
    {
        #region Exposed fields

        public MixerManager manager;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(5);

            Test();
        }

        #endregion Events methods

        #region Public Methods

        [Button]
        public void Test()
        {
            var mixer = manager.GetMixer("Mixer1");

            /*
            Debug.Log(mixer.Groups[0].GetPitch());
            mixer.Groups[0].SetPitch(.5f);
            Debug.Log(mixer.Groups[0].GetPitch());
            mixer.Groups[0].Mute = true;
            */

            mixer.TransitionToSnapshot("No Music");
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}