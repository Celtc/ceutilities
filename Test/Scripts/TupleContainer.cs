﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class TupleContainer : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [ShowDrawerChain]
        private Tuple<int, int> intTuple;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Methods

        [Button]
        public void RandomTuple()
        {
            intTuple = new Tuple<int, int>(UnityEngine.Random.Range(0, 100), UnityEngine.Random.Range(0, 100));
        }

        [Button]
        public void ShowInConsole()
        {
            Debug.Log(string.Format("LastSelling: {0}, {1}", intTuple.Item1, intTuple.Item2), this);
        }

        #endregion Methods
    }
}