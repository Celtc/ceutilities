﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Colors;

namespace CEUtilities.Tests
{
    public class HSVColorTest : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private Color normalColor;

        [SerializeField]
        [HideInInspector]
        private HSVColor hsvColor;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Gets or sets the HSV color.
        /// </summary>
        /// <value>
        /// The color of the HSV.
        /// </value>
        [ShowInInspector]
        public HSVColor HsvColor
        {
            get
            {
                return hsvColor;
            }

            set
            {
                hsvColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the normal color.
        /// </summary>
        /// <value>
        /// The color of the normal.
        /// </value>
        [ShowInInspector]
        public Color NormalColor
        {
            get
            {
                return normalColor;
            }

            set
            {
                normalColor = value;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}