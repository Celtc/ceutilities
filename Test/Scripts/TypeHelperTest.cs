﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Helpers;
using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class TypeHelperTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        public bool removeNamespaces = false;

        public bool convertArrayLiterals = false;

        public int selectedIndex;

        [ReadOnly]
        public Type[] typeToAlias = new Type[]
        {
            typeof(int),
            typeof(float),
            typeof(int[]),
            typeof(string[]),
            typeof(TypeHelperTest),
            typeof(TypeHelperTest[]),
            typeof(TypeHelper),
            //typeof(CEUtilities.Patterns.FileLogger),
            typeof(CEUtilities.Patterns.Singleton<>)
        };

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods
        
        [Button]
        public void LogAlias()
        {
            Debug.Log(TypeHelper.GetAlias(typeToAlias[selectedIndex], removeNamespaces, convertArrayLiterals));
        }

        [Button]
        public void LogAliasOrName()
        {
            Debug.Log(TypeHelper.GetAliasOrName(typeToAlias[selectedIndex], removeNamespaces, convertArrayLiterals));
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}