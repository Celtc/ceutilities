﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Audio;

namespace CEUtilities.Tests
{
    public class AudioManagerTest : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private string firstAudioName = null;
        [SerializeField]
        private string secondAudioName = null;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [Button]
        [HideInEditorMode]
        public void ChangeToFirstByName()
        {
            AudioManager.Instance.ChangeTo(firstAudioName);
        }

        [Button]
        [HideInEditorMode]
        public void ChangeToSecondByName()
        {
            AudioManager.Instance.ChangeTo(secondAudioName);
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}