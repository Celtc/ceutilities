﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

namespace CEUtilities.Tests
{
    [RequireComponent(typeof(Renderer))]
    public class TextureScaleTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [InfoBox("Enter Playmode to see the test buttons", "InEditor")]
        private string propertyName = "_MainTex";

        #endregion Exposed fields

        #region Internal fields

        Renderer rend;
        Material material;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Renderer Renderer
        {
            get
            {
                if (rend == null)
                    rend = GetComponent<Renderer>();

                return rend;
            }
        }

        public Material Material
        {
            get
            {
                if (material == null && Renderer != null)
                    material = Renderer.material;

                return material;
            }
        }

        public string PropertyName
        {
            get
            {
                return propertyName;
            }

            set
            {
                propertyName = value;
            }
        }

        public bool InEditor
        {
            get
            {
                return ApplicationHelper.AppInPausedEditor;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [Button]
        [HideInEditorMode]
        public void PointScaleToDouble()
        {
            if (!CheckParams()) return;

            var tex = GetTexture();
            tex.ScalePoint(tex.width * 2, tex.height * 2);
            SetTexture(tex);
        }

        [Button]
        [HideInEditorMode]
        public void PointScaleToHalf()
        {
            if (!CheckParams()) return;

            var tex = GetTexture();
            tex.ScalePoint((int)(tex.width * .5f), (int)(tex.height * .5f));
            SetTexture(tex);
        }

        [Button]
        [HideInEditorMode]
        public void BilinearScaleToDouble()
        {
            if (!CheckParams()) return;

            var tex = GetTexture();
            tex.ScaleBilinear(tex.width * 2, tex.height * 2);
            SetTexture(tex);
        }

        [Button]
        [HideInEditorMode]
        public void BilinearScaleToHalf()
        {
            if (!CheckParams()) return;

            var tex = GetTexture();
            tex.ScaleBilinear((int)(tex.width * .5f), (int)(tex.height * .5f));
            SetTexture(tex);
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Check if all data required is present
        /// </summary>
        private bool CheckParams()
        {
            return Material != null && PropertyName != null;
        }

        private Texture2D GetTexture()
        {
            var tex = (Texture2D)Material.GetTexture(PropertyName);

            return tex.Duplicate(TextureFormat.RGB24, true);
        }

        private void SetTexture(Texture2D texture)
        {
            Material.SetTexture(PropertyName, texture);
        }

        #endregion Non Public Methods
    }
}