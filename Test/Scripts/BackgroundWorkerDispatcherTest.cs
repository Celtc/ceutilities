﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using Sirenix.OdinInspector;
using CEUtilities.Threading;

namespace CEUtilities.Tests
{
	public class BackgroundWorkerDispatcherTest : MonoBehaviour
	{
		#region Exposed fields

		#endregion Exposed fields

		#region Internal fields

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		[Button]
		public void ThreadACount()
		{
			BackgroundWorkerDispatcher.Instance.Enqueue(() => ThreadCount("A "));
		}

		[Button]
		public void ThreadBCount()
		{
			BackgroundWorkerDispatcher.Instance.Enqueue(() => ThreadCount("B "));
		}

		[Button]
		public void ThreadCCount()
		{
			BackgroundWorkerDispatcher.Instance.Enqueue(() => ThreadCount("C "));
		}

		[Button]
		public void ThreadDCount()
		{
			BackgroundWorkerDispatcher.Instance.Enqueue(() => ThreadCount("D "));
		}

		[Button]
		public void ThreadECount()
		{
			BackgroundWorkerDispatcher.Instance.Enqueue(() => ThreadCount("E "));
		}

		[Button]
		public void TestIEnumeratorEnqueue()
		{
			BackgroundWorkerDispatcher.Instance.Enqueue(TestRoutine());
		}

		#endregion Public Methods

		#region Non Public Methods

		private void ThreadCount(string prefix)
		{
			for (int i = 0; i < 10; i++)
			{				
				Thread.Sleep(1000);
				Debug.Log(prefix + i);
			}
		}

		private IEnumerator TestRoutine()
		{
			yield return null;

			Thread.Sleep(1000);

			Debug.Log("Inside routine! (1)");

			// This will not work
			yield return new WaitForSeconds(10f);

			Thread.Sleep(1000);

			Debug.Log("Inside routine! (2)");
		}

		#endregion Non Public Methods
	}
}