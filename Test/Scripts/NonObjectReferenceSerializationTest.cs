﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace CEUtilities.Tests
{
    public class NonObjectReferenceSerializationTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        public RecursiveNonObjectSerializationTest instancesContainer;

        [OdinSerialize]
        internal List<A> instances = new List<A>();

        [OdinSerialize]
        private A reference;
        
#if UNITY_EDITOR

        [Button]
        public void SetOutsideReference()
        {
            if (instancesContainer != null && instancesContainer.instances.Count > 0)
            {
                reference = instancesContainer.instances[0];
            }
        }

        [Button]
        public void SetInnerReference()
        {
            if (instances != null && instances.Count > 0)
            {
                reference = instances[0];
            }
        }

#endif

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public string ReferencedField => reference?.someString;

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}