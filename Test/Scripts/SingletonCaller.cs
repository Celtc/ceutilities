﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class SingletonCaller : SerializedMonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(1);
            HasInstance();
            yield return new WaitForSeconds(1);
            Exists();
            yield return new WaitForSeconds(1);
            Msg();
            yield return new WaitForSeconds(1);
            Value();
            yield return new WaitForSeconds(1);
            GetInstance();
            yield return new WaitForSeconds(1);
            HasInstance();
            yield return new WaitForSeconds(1);
            Exists();
            yield return new WaitForSeconds(1);
            Msg();
            yield return new WaitForSeconds(1);
            Value();
        }

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Check for singleton
        /// </summary>
        [Button]
        public void HasInstance()
        {
            Debug.Log(string.Format("Singleton has instance: {0}",
                SingletonTest.HasInstance));
        }
        [Button]
        public void Exists()
        {
            Debug.Log(string.Format("Singleton existance: {0}",
                SingletonTest.Exists));
        }
        [Button]
        public void Msg()
        {
            Debug.Log(string.Format("Singleton msg: {0}",
                SingletonTest.HasInstance ? SingletonTest.Instance.Msg : "(no instance)"));
        }
        [Button]
        public void Value()
        {
            Debug.Log(string.Format("Singleton value: {0}",
                SingletonTest.HasInstance ? SingletonTest.Instance.Value.ToString() : "(no instance)"));
        }
        [Button]
        public void GetInstance()
        {
            Debug.Log(string.Format("Singleton instance: {0}",
                SingletonTest.Instance != null ? "Get" : "(no instance)"));
        }


        #endregion Methods
    }
}