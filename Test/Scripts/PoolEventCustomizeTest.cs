﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;

namespace CEUtilities.Tests
{
    [RequireComponent(typeof(PoolManager))]
    [ExecuteInEditMode]
    public class PoolEventCustomizeTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        //bool registered;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        protected void Start()
        {
            //if (!registered && PoolManager.HasInstance)
            //    RegisterDelegate();
        }

        #endregion Events methods

        #region Methods

        public void RegisterDelegate()
        {
            //
        }

        #endregion Methods
    }
}