﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class BaseGenericObject : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private Type generic1Type;

        [SerializeField]
        private Type generic2Type;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [Button]
        public void CreateGeneric()
        {
            var generic = typeof(GenericObject<,>);
            var makeme = generic.MakeGenericType(generic1Type, generic2Type);
            var instance = Activator.CreateInstance(makeme);
            Debug.Log("[BaseGenericObject] Instance null: " + instance == null);
        }

        [Button]
        public void CreateGeneric2()
        {
            var instance = this.GetOrAddComponent<GenericObject<int, int>>();
            Debug.Log("[BaseGenericObject] Instance null: " + instance == null);
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}