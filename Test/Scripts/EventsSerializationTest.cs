﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class EventsSerializationTest : SerializedMonoBehaviour
    {
        [SerializeField]
        public class InnerClass
        {
            public int algo;

            [SerializeField]
            public UnityEvent_Float OnPressedPositive = new UnityEvent_Float();

            [SerializeField]
            public UnityEvent_Float OnPressedNegative = new UnityEvent_Float();
        }

        public UnityEvent_Int eventSample = new UnityEvent_Int();

        public InnerClass container = new InnerClass();
    }
}
