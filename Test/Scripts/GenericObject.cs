﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class GenericObject<T1, T2> : BaseGenericObject
    {
        #region Exposed fields

        [SerializeField]
        private T1 generic1;

        [SerializeField]
        private T2 generic2;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}