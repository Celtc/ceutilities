﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class TableMatrixExtendedTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        //[TableMatrix]
        [TableMatrixExtended(RowLabelsMemberName = "RowsLabels", ColumnLabelsMemberName = "ColumnsLabels", RowLabelsWidth = 80)]
        public bool[,] matrix = new bool[2, 5]
        {
            { true, false, false, false, true },
            { true, false, false, false, true },
        };

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

#if UNITY_EDITOR

        private string[] RowsLabels => new string[] { "Fila A", "Fila B", "Fila C", "Fila D", "Fila E" };

        private string[] ColumnsLabels()
        {
            return new string[] { "Columna A", "Columna B", "Columna C", "Columna D", "Columna E" };
        }

#endif

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}