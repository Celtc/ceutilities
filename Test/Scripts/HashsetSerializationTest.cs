﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Tests
{
    public class HashSetSerializationTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        public HashSet<int> hashset = new HashSet<int>();

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [Button]
        public void Populate()
        {
            hashset.Clear();

            for (int i = 0; i < Random.Range(10, 20); i++)
            {
                hashset.Add(Random.Range(0, 99));
            }
        }

        [Button]
        public void Print()
        {
            var sb = new System.Text.StringBuilder("[HashsetSerializationTest] Contents: ");
            for (int i = 0; i < hashset.Count; i++)
            {
                hashset.ForEach(x => sb.Append(x.ToString() + ", "));
            }
            sb.Remove(sb.Length - 2, 2);
            Debug.Log(sb.ToString());
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}