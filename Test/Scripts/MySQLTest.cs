﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.API;

namespace CEUtilities.Tests
{
    public class MySQLTest : MonoBehaviour
    {
        #region Exposed fields

        public MySQL db;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [Button]
        public void DoTestQuery()
        {
            StartCoroutine(
                db._QueryAsync(
                    "Select * from user",
                    (table) => Debug.Log(table.ContentToString())
                )
            );
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}