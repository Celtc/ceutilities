﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.API;

namespace CEUtilities.Tests
{
    public class FTPTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        public string newDirectory;
        public string localFile;
        public string remoteFile;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [Button]
        public void MakeDir()
        {
            var task = FTP.Instance.CreateTask<MakeDirectory>();
            task.NewDirectory = newDirectory;
            task.Run();
        }

        [Button]
        public void UploadFile()
        {
            var task = FTP.Instance.CreateTask<UploadFile>();
            task.RemoteFile = remoteFile;
            task.LocalFile = localFile;
            task.Run();
        }

        [HideInEditorMode]
        [Button]
        public void ChainedCommands()
        {
            StartCoroutine(_ChainedCommands());
        }

        #endregion Public Methods

        #region Non Public Methods

        private IEnumerator _ChainedCommands()
        {
            var makedirTask = FTP.Instance.CreateTask<MakeDirectory>();
            makedirTask.NewDirectory = newDirectory;

            yield return makedirTask._RunAsync();

            var uploadTask = FTP.Instance.CreateTask<UploadFile>();
            uploadTask.RemoteFile = remoteFile;
            uploadTask.LocalFile = localFile;

            yield return uploadTask._RunAsync();
        }

        #endregion Non Public Methods
    }
}