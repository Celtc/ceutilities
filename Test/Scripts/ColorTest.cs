﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Colors;

namespace CEUtilities.Tests
{
    public class ColorTest : MonoBehaviour
    {
        #region Exposed fields

        public Color stdColor;
        public Color32 stdColor32;
        public HSVColor hsvColor;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}