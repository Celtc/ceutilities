﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.API;

namespace CEUtilities.Tests
{
    public class MailTest : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private List<Texture2D> images;

        [SerializeField]
        [TextArea]
        private string mailXML;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public List<Texture2D> Images
        {
            get
            {
                return images;
            }

            set
            {
                images = value;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [HorizontalGroup]
        [Button]
        [DisableInEditorMode]
        public void SendMailWithImages()
        {
            foreach (var image in images)
            {
                Mail.Instance.AttachFile(image.name, image);
            }

            Mail.Instance.SendMail();
        }

        [HorizontalGroup]
        [Button]
        [DisableInEditorMode]
        public void ExportMailWithImages()
        {
            foreach (var image in images)
            {
                Mail.Instance.AttachFile(image.name, image);
            }

            Mail.Instance.ExportMail();
        }

        [HorizontalGroup]
        [Button]
        [DisableInEditorMode]
        public void SendXMLMail()
        {
            Mail.Instance.SendExportedMail(mailXML);
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}