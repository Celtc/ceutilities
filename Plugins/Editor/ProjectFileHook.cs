using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

using UnityEditor;

namespace Assets.Plugins.Editor
{
    [InitializeOnLoad]
    public class ProjectFileHook
    {
        // Necessary for XLinq to save the xml project file in utf8
        class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        static void ProcessNodesWithIncludeAttribute(XDocument document, string localName, string includeValue, Action<XElement> action)
        {
            var nodes = document
                .Descendants()
                .Where(p => p.Name.LocalName == localName);

            foreach (var node in nodes)
            {
                var xa = node.Attribute("Include");
                if (xa != null && !string.IsNullOrEmpty(xa.Value) && string.Equals(xa.Value, includeValue))
                {
                    action(node);
                }
            }
        }

        // Remove System.Data from project (not from file system so Unity can compile properly)
        static void RemoveFileFromProject(XDocument document, string fileName)
        {
            ProcessNodesWithIncludeAttribute(document, "None", fileName, element => element.Remove());
        }

        // Adjust references, by using the default framework assembly instead of local file (remove the HintPath)
        static void RemoveHintPathFromReference(XDocument document, string assemblyName)
        {
            ProcessNodesWithIncludeAttribute(document, "Reference", assemblyName, element => element.Nodes().Remove());
        }

        // Entry point
        static string OnGeneration(string name, string content)
        {
            var document = XDocument.Parse(content);

            RemoveFileFromProject(document, @"Assets\CEUtilities\Plugins\Database\System.Data.dll");
            RemoveFileFromProject(document, @"Assets\CEUtilities\Plugins\Drawing\System.Drawing.dll");
            RemoveHintPathFromReference(document, "System.Data");
            RemoveHintPathFromReference(document, "System.Drawing");

            var str = new Utf8StringWriter();
            document.Save(str);

            return str.ToString();
        }

        static ProjectFileHook()
        {
            //SyntaxTree.VisualStudio.Unity.Bridge.ProjectFilesGenerator.ProjectFileGeneration = OnGeneration;

            try
            {
                var generatorType = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                                     from type in assembly.GetTypes()
                                     where type.Name == "ProjectFilesGenerator" && type.GetFields().Any(m => m.Name == "ProjectFileGeneration")
                                     select type).FirstOrDefault();

                var fieldInfo = generatorType.GetField("ProjectFileGeneration", BindingFlags.Static | BindingFlags.Public);
                var methodInfo = typeof(ProjectFileHook).GetMethod("OnGeneration", BindingFlags.Static | BindingFlags.NonPublic);
                Delegate del = Delegate.CreateDelegate(fieldInfo.FieldType, methodInfo);
                Delegate currDel = fieldInfo.GetValue(null) as Delegate;
                Delegate combinedDel = Delegate.Combine(currDel, del);
                fieldInfo.SetValue(null, combinedDel);
            }
            catch { } // If we fall here means is not using Visual Studio IDE
        }
    }
}