﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.ExtensionComponents
{
    public class GameObjectExtensionCaller : MonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Activate the game object.
        /// </summary>
        public void Activate(float delay)
        {
            gameObject.SetActive(true, delay);
        }

        /// <summary>
        /// Deactivate the game object.
        /// </summary>
        public void Deactivate(float delay)
        {
            gameObject.SetActive(false, delay);
        }

        /// <summary>
        /// Activate or deactivate the game object.
        /// </summary>
        public void SetActive(bool value, float delay)
        {
            gameObject.SetActive(value, delay);
        }

        /// <summary>
        /// Deactivate the game object.
        /// </summary>
        public void ToggleActivate(float delay)
        {
            gameObject.SetActive(!gameObject.activeInHierarchy, delay);
        }

        /// <summary>
        /// Activate the childrens of the game object (not the game object itself).
        /// </summary>
        public void ActivateChildren(float delay)
        {
            for (int i = 0; i < transform.childCount; i++)
                transform.GetChild(i).gameObject.SetActive(true, delay);
        }

        /// <summary>
        /// Deactivate the childrens of the game object (not the game object itself).
        /// </summary>
        public void DeactivateChildren(float delay)
        {
            for (int i = 0; i < transform.childCount; i++)
                transform.GetChild(i).gameObject.SetActive(false, delay);
        }

        /// <summary>
        /// Activate or deactivate the childrens of the game object (not the game object itself).
        /// </summary>
        public void SetActiveChildren(bool value, float delay)
        {
            for (int i = 0; i < transform.childCount; i++)
                transform.GetChild(i).gameObject.SetActive(value, delay);
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}