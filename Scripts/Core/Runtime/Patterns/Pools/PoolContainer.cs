﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Patterns
{
    internal class PoolContainer : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private Pool owner;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Pool Owner
        {
            get
            {
                return owner;
            }

            set
            {
                owner = value;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        public static implicit operator Transform(PoolContainer pc)
        {
            return pc.transform;
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}