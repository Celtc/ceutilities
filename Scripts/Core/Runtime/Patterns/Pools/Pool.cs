﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.Events;

using Sirenix.OdinInspector;

namespace CEUtilities.Patterns
{
    /// <summary>
    /// Pool of objects
    /// </summary>
    [Serializable]
    public class Pool
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private bool inited;

        [HideInInspector]
        [SerializeField]
        private GameObject prefab;

        [SerializeField]
        [PropertyOrder(0)]
        [TabGroup("Fields")]
        private int prefillQuantity = 0;

        [SerializeField]
        [PropertyOrder(1)]
        [TabGroup("Fields")]
        private bool allowOnDemandInstantiate = false;

        [SerializeField]
        [PropertyOrder(2)]
        [TabGroup("Fields")]
        private Transform parentForSpawned;


        [SerializeField]
        [HideInInspector]
        private List<GameObject> spawned;

        [SerializeField]
        [HideInInspector]
        private List<GameObject> stored;

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        PoolManager cachedManager;

        [SerializeField]
        [HideInInspector]
        PoolContainer cachedContainer;

        #endregion Internal fields

        #region Custom Events

        /*
        /// <summary>
        /// Event for when an object is taken from the pool
        /// </summary>
        [PropertyOrder(0)]
        [TabGroup("Events")]
        public UnityEvent_GameObject OnSpawn = new UnityEvent_GameObject();

        /// <summary>
        /// Event for when an object is added to the pool
        /// </summary>
        [PropertyOrder(1)]
        [TabGroup("Events")]
        public UnityEvent_GameObject OnRecycle = new UnityEvent_GameObject();
        */

#if UNITY_EDITOR
        [OnInspectorGUI]
        [TabGroup("Events")]
        [PropertyOrder(9000)]
        private void DrawInfoBox()
        {
            Sirenix.Utilities.Editor.SirenixEditorGUI.MessageBox("Odin does not support UntyEvent in non Component scripts. Use InputTrackerFix as a temporal workaround.", UnityEditor.MessageType.Warning);
        }
#endif

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Is the pool inited
        /// </summary>
        public bool Inited
        {
            get
            {
                return inited;
            }

            private set
            {
                inited = value;
            }
        }

        /// <summary>
        /// Prefab this pool caches
        /// </summary>
        public GameObject Prefab
        {
            get
            {
                return prefab;
            }

            set
            {
                prefab = value;
            }
        }

        /// <summary>
        /// Quantity of object to pre-instantiate
        /// </summary>
        public int PrefillQuantity
        {
            get { return prefillQuantity; }
            private set { prefillQuantity = value; }
        }

        /// <summary>
        /// Are objects available in the pool?
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(3)]
        [TabGroup("Fields")]
        public bool HasObjects
        {
            get { return Stored != null && Stored.Count > 0; }
        }

        /// <summary>
        /// How many objects are available
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(4)]
        [TabGroup("Fields")]
        public int AvailableObjects
        {
            get { return Stored != null ? Stored.Count : 0; }
        }

        /// <summary>
        /// How many objects have been spawned
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(5)]
        [TabGroup("Fields")]
        public int SpawnedObjects
        {
            get { return Spawned != null ? Spawned.Count : 0; }
        }

        /// <summary>
        /// The pooled objects currently available.
        /// </summary>
        public List<GameObject> Stored
        {
            get { return stored; }
            private set { stored = value; }
        }

        /// <summary>
        /// Object outside of pool, but belongs to it
        /// </summary>
        public List<GameObject> Spawned
        {
            get { return spawned; }
            private set { spawned = value; }
        }

        /// <summary>
        /// Spawn point for objects taken from the pool
        /// </summary>
        public Transform ParentForSpawned
        {
            get
            {
                return parentForSpawned;
            }

            set
            {
                parentForSpawned = value;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Prefills the pool
        /// </summary>
        [Button]
        [TabGroup("Methods")]
        public void PreFill()
        {
            var quantity = prefillQuantity - SpawnedObjects - AvailableObjects;
            if (quantity > 0)
                FillPool(quantity);
        }

        /// <summary>
        /// Creates new elements and stores them in the pool
        /// </summary>
        /// <param name="quantity">Number of elements to fill the pool with</param>
        public void FillPool(int quantity)
        {
            GameObject obj;
            for (int i = 0; i < quantity; i++)
            {
                obj = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.Euler(Vector3.zero)) as GameObject;
                obj.transform.SetParent(cachedContainer, false);
                obj.gameObject.SetActive(false);
                Stored.Add(obj);
            }
        }

        /// <summary>
        /// Picks an existing element from the pool or creates a new one if the pool is empty. Should be call by manager.
        /// </summary>
        [Button]
        [TabGroup("Methods")]
        public virtual GameObject Spawn()
        {
            return Spawn(Vector3.zero, Quaternion.Euler(Vector3.zero), null, allowOnDemandInstantiate);
        }

        /// <summary>
        /// Picks an existing element from the pool or creates a new one if the pool is empty. Should be call by manager.
        /// </summary>
        public virtual GameObject Spawn(Vector3 position)
        {
            return Spawn(position, Quaternion.Euler(Vector3.zero), null, allowOnDemandInstantiate);
        }

        /// <summary>
        /// Picks an existing element from the pool or creates a new one if the pool is empty. Should be call by manager.
        /// </summary>
        public virtual GameObject Spawn(Quaternion rotation)
        {
            return Spawn(Vector3.zero, rotation, null, allowOnDemandInstantiate);
        }

        /// <summary>
        /// Picks an existing element from the pool or creates a new one if the pool is empty. Should be call by manager.
        /// </summary>
        public virtual GameObject Spawn(Vector3 position, Quaternion rotation)
        {
            return Spawn(position, rotation, null, allowOnDemandInstantiate);
        }

        /// <summary>
        /// Picks an existing element from the pool or creates a new one if the pool is empty. Should be call by manager.
        /// </summary>
        public virtual GameObject Spawn(Quaternion rotation, Transform customParent)
        {
            return Spawn(Vector3.zero, rotation, customParent, allowOnDemandInstantiate);
        }

        /// <summary>
        /// Picks an existing element from the pool or creates a new one if the pool is empty. Should be call by manager.
        /// </summary>
        public virtual GameObject Spawn(Vector3 position, Quaternion rotation, Transform customParent)
        {
            return Spawn(position, rotation, customParent, allowOnDemandInstantiate);
        }

        /// <summary>
        /// Picks an existing element from the pool or creates a new one if the pool is empty. Should be call by manager.
        /// </summary>
        public virtual GameObject Spawn(Vector3 position, Quaternion rotation, Transform customParent, bool onlyStored)
        {
            GameObject candidate = null;

            // Get or create GO
            if (Stored.Count == 0)
            {
                if (!onlyStored)
                {
                    candidate = GameObject.Instantiate(prefab, position, rotation) as GameObject;
                    candidate.transform.position = position;
                }
            }
            else
            {
                // Extract from list
                // TODO: Use an Stack (Odin currently does not support it)
                var index = Stored.Count - 1;
                candidate = Stored[index];
                Stored.RemoveAt(index);

                candidate.transform.position = position;
                candidate.SetActive(true);
            }

            // Has GO?
            if (candidate == null)
            {
                Debug.Log(string.Format("[Pool: {0}] No more game objects available.", Prefab.name));
                return null;
            }

            // Init GO
            if (customParent != null)
                candidate.transform.SetParent(customParent, false);
            else if (ParentForSpawned != null)
                candidate.transform.SetParent(ParentForSpawned, false);

            Spawned.Add(candidate);

            // Add comp
            var spawnling = candidate.GetOrAddComponent<PoolSpawnling>();
            spawnling.Initialize(Prefab);

            // Event
            //OnSpawn.Invoke(candidate);
            cachedManager.OnSpawnCallback(this, candidate);

            return candidate;
        }

        /// <summary>
        /// Removes an element from the game and stores it the pool
        /// </summary>
        public void Recycle(GameObject obj)
        {
            if (!Spawned.Contains(obj))
            {
                obj.Destroy();

                // Event
                //OnRecycle.Invoke(null);
                cachedManager.OnRecycleCallback(this, obj);

                return;
            }

            ResetInstanceTransform(obj);

            obj.SetActive(false);
            obj.transform.SetParent(cachedContainer, false);

            Spawned.Remove(obj);
            Stored.Add(obj);

            // Event
            //OnRecycle.Invoke(obj);
            cachedManager.OnRecycleCallback(this, obj);
        }

        /// <summary>
        /// Removes an element from the game and stores it the pool
        /// </summary>
        /// <param name="obj">Element to remove</param>
        [Button]
        [TabGroup("Methods")]
        public void RecycleOldest()
        {
            if (Spawned.Count > 0)
                Recycle(Spawned[0]);
            else
                Debug.Log(string.Format("[Pool: {0}] No game objects spawned.", Prefab.name));
        }

        /// <summary>
        /// Empties the pool
        /// </summary>
        [Button]
        [TabGroup("Methods")]
        public void Flush()
        {
            Flush(true, true);
        }

        /// <summary>
        /// Destroys the pool
        /// </summary>
        [Button]
        [GUIColor(1, 0, 0)]
        [TabGroup("Methods")]
        public void Destroy()
        {
            cachedManager.DestroyPool(this);
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="container"></param>
        /// <param name="parentForSpawned"></param>
        /// <param name="prefillQuantity"></param>
        internal Pool(GameObject prefab, PoolContainer container, Transform parentForSpawned, int prefillQuantity)
        {
            Init(prefab, container, parentForSpawned, prefillQuantity);
        }

        /// <summary>
        /// Initialize the pool
        /// </summary>
        internal void Init(GameObject assignedPrefab, PoolContainer container, Transform parentForSpawned, int prefillQuantity)
        {
            if (Inited)
                return;
            else
                Inited = true;

            if (Stored == null)
                Stored = new List<GameObject>();

            if (Spawned == null)
                Spawned = new List<GameObject>();

            this.prefab = assignedPrefab;
            this.prefillQuantity = prefillQuantity;
            this.parentForSpawned = parentForSpawned;
            this.cachedContainer = container;
            this.cachedManager = PoolManager.Instance;

            PreFill();

            Debug.Log(string.Format("[Pool] New pool for prefab \"{0}\" created.", prefab.name), container);
        }

        /// <summary>
        /// Empties the pool
        /// </summary>
        internal void Flush(bool destroyStoredElements, bool destroySpawnedElements)
        {
            cachedManager.CleanupRelations(this);

            if (destroyStoredElements)
                for (int i = Stored.Count - 1; i >= 0; i--)
                    if (Stored[i] != null)
                        Stored[i].Destroy();

            if (destroySpawnedElements)
                for (int i = Spawned.Count - 1; i >= 0; i--)
                    if (Spawned[i] != null)
                        Spawned[i].Destroy();

            Stored.Clear();
            Spawned.Clear();
        }

        /// <summary>
        /// Resets the Transform values of the GameObject to the ones of its blueprint
        /// </summary>
        internal void ResetInstanceTransform(GameObject go)
        {
            if ((object)go == null)
                return;

            go.transform.SetParent(null);
            go.transform.position = prefab.transform.position;
            go.transform.localScale = prefab.transform.localScale;
            go.transform.rotation = prefab.transform.rotation;
        }

        #endregion Methods
    }
}