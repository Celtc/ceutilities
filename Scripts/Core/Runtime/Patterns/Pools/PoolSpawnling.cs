﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Patterns
{
    public class PoolSpawnling : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private GameObject prefab;

        [SerializeField]
        private bool selfRecycle = false;

        [SerializeField]
        [ShowIf("selfRecycle")]
        private float lifetime = 10f;


        private Pool poolSource;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Pool form this game object come
        /// </summary>
        public Pool PoolSource
        {
            get
            {
                if (poolSource == null)
                    poolSource = PoolManager.Instance.Pools[prefab];

                return poolSource;
            }
        }

        /// <summary>
        /// Should recycle after a certain time of live
        /// </summary>
        public bool SelfRecycle
        {
            get
            {
                return selfRecycle;
            }

            set
            {
                selfRecycle = value;
            }
        }

        /// <summary>
        /// Time to auto recyle
        /// </summary>
        public float Lifetime
        {
            get
            {
                return lifetime;
            }

            set
            {
                lifetime = value;
            }
        }

        /// <summary>
        /// Is the game object pooled
        /// </summary>
        public bool Pooled
        {
            get
            {
                return PoolSource.Stored.Contains(gameObject);
            }
        }

        #endregion Properties

        #region Events methods

        private void Start()
        {
            if (SelfRecycle)
                StartCoroutine(_DelayedRecycle(Lifetime));
        }

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Initialize the comp
        /// </summary>
        public void Initialize(GameObject prefab)
        {
            this.prefab = prefab;
        }

        /// <summary>
        /// Put again in the pool
        /// </summary>
        [Button]
        [HideIf("Pooled")]
        public void Recycle()
        {
            if (PoolManager.Exists)
                PoolManager.Instance.Recycle(gameObject);
        }


        /// <summary>
        /// Recycle routine
        /// </summary>
        private IEnumerator _DelayedRecycle(float delay)
        {
            yield return new WaitForSeconds(delay);

            Recycle();
        }

        #endregion Methods
    }
}