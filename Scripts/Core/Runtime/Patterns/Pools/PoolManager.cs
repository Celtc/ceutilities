﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

using Sirenix.OdinInspector;

namespace CEUtilities.Patterns
{
    [CustomSingleton(AutoGeneration = true)]
    public class PoolManager : Singleton<PoolManager>
    {
        #region Exposed fields

        [Tooltip("Default amount of clones to pre-store when a new pool is created")]
        [SerializeField]
        int defaultPrefillQuantity = 1;

        [SerializeField]
        [Tooltip("The pooleed go containers should be hidden?")]
        private bool containersHideFlag = false;

        [SerializeField]
#if UNITY_EDITOR
        [OnValueChanged("OnPoolsChanged", false)]
#endif
        [DictionaryDrawerSettings(DisplayMode = DictionaryDisplayOptions.CollapsedFoldout, KeyLabel = "Prefab", ValueLabel = "Pool Settings")]
        private Dictionary<GameObject, Pool> pools = new Dictionary<GameObject, Pool>();


#if UNITY_EDITOR
        int lastCount = 0;
        void OnPoolsChanged()
        {
            if (lastCount != pools.Count)
            {
                // Checked for manual removes
                if (lastCount > pools.Count)
                {
                    // Find removed pool
                    Pool removed = null;
                    foreach (var pool in relPoolContainer.Keys)
                        if (!pools.ContainsValue(pool))
                            removed = pool;

                    // Finded?
                    if (removed == null)
                        return;

                    // Destroy
                    DestroyPool(removed);
                }

                // Checked for adds
                if (lastCount < pools.Count)
                {
                    // Search for pools not inited
                    foreach(var pair in pools)
                    {
                        if (!pair.Value.Inited)
                        {
                            var container = CreatePoolContainer(pair.Key);
                            container.Owner = pair.Value;
                            RelPoolContainer.Add(pair.Value, container);

                            pair.Value.Init(pair.Key, container, null, defaultPrefillQuantity);
                        }
                    }
                }

                lastCount = pools.Count;
            }
        }
#endif

        #endregion Exposed fields

        #region Internal fields

        /// <summary>
        /// Each spawned GameObject and the pool that created it.
        /// </summary>
        [SerializeField]
        [HideInInspector]
        private Dictionary<GameObject, Pool> relObjectPool = null;

        /// <summary>
        /// Each pool with its container
        /// </summary>
        [SerializeField]
        [HideInInspector]
        private Dictionary<Pool, PoolContainer> relPoolContainer = null;

        #endregion Internal fields

        #region Custom Events

        /// <summary>
        /// Event for when any object is spawn
        /// </summary>
        public UnityEvent_GameObject_Pool OnSpawn = new UnityEvent_GameObject_Pool();

        /// <summary>
        /// Event for when any object is recyle
        /// </summary>
        public UnityEvent_GameObject_Pool OnRecyle = new UnityEvent_GameObject_Pool();

        /// <summary>
        /// Event for when a new pool is created
        /// </summary>
        public UnityEvent_Pool OnPoolCreated = new UnityEvent_Pool();

        /// <summary>
        /// Event indicating when a pool is about to get destroyed
        /// </summary>
        public UnityEvent_Pool OnPoolMarkedForDestuction = new UnityEvent_Pool();

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// The object prefabs which the pool can handle
        /// by the amount of objects of each type to buffer.
        /// </summary>
        public Dictionary<GameObject, Pool> Pools
        {
            get
            {
                return pools;
            }

            set
            {
                pools = value;
            }
        }

        /// <summary>
        /// Default amount of clones to pre-store when a new pool is created
        /// </summary>
        public int DefaultPrefillQuantity
        {
            get
            {
                return defaultPrefillQuantity;
            }

            set
            {
                defaultPrefillQuantity = value;
            }
        }

        /// <summary>
        /// Will the containers be hidden in the scene?
        /// </summary>
        public bool ContainersHideFlag
        {
            get
            {
                return containersHideFlag;
            }
        }


        private Dictionary<GameObject, Pool> RelObjectPool
        {
            get
            {
                if (relObjectPool == null)
                {
                    relObjectPool = new Dictionary<GameObject, Pool>();
                    foreach (var pool in Pools.Values)
                        if (pool.SpawnedObjects > 0)
                            foreach (var obj in pool.Spawned)
                                relObjectPool.Add(obj, pool);
                }

                return relObjectPool;
            }

            set
            {
                relObjectPool = value;
            }
        }

        internal Dictionary<Pool, PoolContainer> RelPoolContainer
        {
            get
            {
                if (relPoolContainer == null)
                    relPoolContainer = new Dictionary<Pool, PoolContainer>();

                return relPoolContainer;
            }

            set
            {
                relPoolContainer = value;
            }
        }

        #endregion Properties

        #region Events methods

        protected override void Awake()
        {
            base.Awake();

            if (Pools == null)
                Pools = new Dictionary<GameObject, Pool>();
        }

        internal void OnSpawnCallback(Pool pool, GameObject spawned)
        {
            if (RelObjectPool.ContainsKey(spawned))
                RelObjectPool[spawned] = pool;
            else
                RelObjectPool.Add(spawned, pool);

            OnSpawn.Invoke(spawned, pool);
        }

        internal void OnRecycleCallback(Pool pool, GameObject spawned)
        {
            if (RelObjectPool.ContainsKey(spawned))
                RelObjectPool.Remove(spawned);

            OnRecyle.Invoke(spawned, pool);
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Creates a new clone of the prefab
        /// </summary>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public GameObject Spawn(GameObject prefab)
        {
            return Spawn(prefab, Vector3.zero, Quaternion.Euler(Vector3.zero), null);
        }

        /// <summary>
        /// Creates a new clone of the prefab
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public GameObject Spawn(GameObject prefab, Quaternion rotation)
        {
            return Spawn(prefab, Vector3.zero, rotation, null);
        }

        /// <summary>
        /// Creates a new clone of the prefab 
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public GameObject Spawn(GameObject prefab, Vector3 position)
        {
            return Spawn(prefab, position, Quaternion.Euler(Vector3.zero), null);
        }

        /// <summary>
        /// Creates a new clone of the prefab
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="customParent"></param>
        /// <returns></returns>
        public GameObject Spawn(GameObject prefab, Transform customParent)
        {
            return Spawn(prefab, Vector3.zero, Quaternion.Euler(Vector3.zero), customParent);
        }

        /// <summary>
        /// Creates a new clone of the prefab
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="position"></param>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            return Spawn(prefab, position, rotation, null);
        }

        /// <summary>
        /// Creates a new clone of the prefab
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="position"></param>
        /// <param name="customParent"></param>
        /// <returns></returns>
        public GameObject Spawn(GameObject prefab, Vector3 position, Transform customParent)
        {
            return Spawn(prefab, position, Quaternion.Euler(Vector3.zero), customParent);
        }

        /// <summary>
        /// Creates a new clone of the prefab
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="rotation"></param>
        /// <param name="customParent"></param>
        /// <returns></returns>
        public GameObject Spawn(GameObject prefab, Quaternion rotation, Transform customParent)
        {
            return Spawn(prefab, Vector3.zero, rotation, customParent);
        }

        /// <summary>
        /// Creates a new clone of the prefab
        /// </summary>
        public GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform customParent, bool createPoolIfNeeded = true)
        {
            if ((object)prefab == null)
                return null;

            // Get pool
            Pool pool = null;
            if (createPoolIfNeeded)
                pool = GetOrCreatePool(prefab, customParent, defaultPrefillQuantity);
            else
                Pools.TryGetValue(prefab, out pool);

            // Valid pool?
            if (pool == null)
            {
                Debug.LogWarning(string.Format("[PrefabsObjectPool] No pool of the given prefab: \"{0}\". Create a pool, or allow on demand creation.", prefab.name), this);
                return null;
            }

            // Spawn
            var newObject = pool.Spawn(position, rotation, customParent);

            // Valid spawned?
            if (newObject != null)
            {
                if (RelObjectPool.ContainsKey(newObject))
                    RelObjectPool[newObject] = pool;
                else
                    RelObjectPool.Add(newObject, pool);
            }

            // Event
            OnSpawn.Invoke(newObject, pool);

            return newObject;
        }


        /// <summary>
        /// Removes a GameObject from play and stores it in its corresponding pool (if it applies)
        /// </summary>
        public void Recycle(GameObject go)
        {
            if (go == null) return;

            Pool parentPool = null;

#if UNITY_EDITOR
            UnityEditor.Undo.RecordObjects(new UnityEngine.Object[] { this, go }, "Recycle");
#endif

            // Get pool for go
            if (RelObjectPool.TryGetValue(go, out parentPool))
            {
                // Has pool
                if (parentPool != null)
                {
                    parentPool.Recycle(go);
                    return;
                }
                // No pool
                OnRecycleCallback(null, go);
            }

            Debug.Log(string.Format("[PoolManager] No pool to store the game object \"{0}\", the object will be destroy.", go.name), this);

            go.Destroy();
        }

        /// <summary>
        /// Resets the Transform values of the GameObject to the ones of its blueprint
        /// </summary>
        /// <param name="go">Element to reset</param>
        public void ResetInstanceTransform(GameObject go)
        {
            Pool parentPool = null;

            if (RelObjectPool.TryGetValue(go, out parentPool))
                if (parentPool != null)
                    parentPool.ResetInstanceTransform(go);
        }


        /// <summary>
        /// Given a prefab, returns all spawned clones
        /// </summary>
        public List<GameObject> GetAllSpawnedFrom(GameObject prefab)
        {
            Pool parentPool = null;
            if (RelObjectPool.TryGetValue(prefab, out parentPool))
                if (parentPool != null)
                    return parentPool.Spawned;

            return null;
        }

        /// <summary>
        /// Given a pool, returns all spawned clones
        /// </summary>
        public List<GameObject> GetAllSpawnedFrom(Pool pool)
        {
            return pool.Spawned;
        }


        /// <summary>
        /// Creates a new pool for the selected prefab
        /// </summary>
        public void CreatePool(GameObject prefab, Transform parentForSpawned, int prefillQuantity)
        {
            GetOrCreatePool(prefab, parentForSpawned, prefillQuantity);
        }

        /// <summary>
        /// Creates a new pool for the selected prefab
        /// </summary>
        public void CreatePool(GameObject prefab, int prefillQuantity)
        {
            GetOrCreatePool(prefab, null, prefillQuantity);
        }

        /// <summary>
        /// Creates a new pool for the selected prefab
        /// </summary>
        public void CreatePool(GameObject prefab, Transform parentForSpawned)
        {
            GetOrCreatePool(prefab, parentForSpawned, defaultPrefillQuantity);
        }

        /// <summary>
        /// Creates a new pool for the selected prefab
        /// </summary>
        public void CreatePool(GameObject prefab)
        {
            GetOrCreatePool(prefab, null, defaultPrefillQuantity);
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Returns the pool associated with a prefab.
        /// </summary>
        private Pool GetOrCreatePool(GameObject prefab, Transform parentForSpawned, int prefillQuantity)
        {
            Pool pool = null;

            if (pools.TryGetValue(prefab, out pool))
                return pool;

            // Create container
            var container = CreatePoolContainer(prefab);

            // Create pool
            pool = new Pool(prefab, container, parentForSpawned, prefillQuantity);
            container.Owner = pool;
            RelPoolContainer.Add(pool, container);

            // Add to dictionary
            Pools.Add(prefab, pool);

            // Event
            OnPoolCreated.Invoke(pool);

            return pool;
        }

        /// <summary>
        /// Creates a new container for a pool of  certain prefab
        /// </summary>
        private PoolContainer CreatePoolContainer(GameObject prefab)
        {
            var container = new GameObject("[Pool Container: " + prefab.name + "]");
            container.transform.SetParent(transform, false);

            if (ContainersHideFlag)
                container.hideFlags = HideFlags.HideInHierarchy;

            return container.AddComponent<PoolContainer>();
        }
        

        /// <summary>
        /// Removes all relations from auxiliar structure
        /// </summary>
        internal void CleanupRelations(Pool pool)
        {
            foreach (var obj in pool.Spawned)
                RelObjectPool.Remove(obj);
        }

        /// <summary>
        /// Destroy a pool
        /// </summary>
        internal void DestroyPool(Pool pool)
        {
            // Event
            OnPoolMarkedForDestuction.Invoke(pool);

            // Destroy spawned or pooled objects
            pool.Flush();

            // Destroy container
            RelPoolContainer[pool].gameObject.Destroy();
            RelPoolContainer.Remove(pool);

            // Remove links to spawned objects
            foreach (var pair in RelObjectPool.Where(kvp => kvp.Value == pool).ToList())
                RelObjectPool.Remove(pair.Key);

            // Remove pool
            if (Pools.ContainsKey(pool.Prefab))
                Pools.Remove(pool.Prefab);

            Debug.Log("[PoolManager] Destroyed pool for prefab: " + pool.Prefab.name);
        }

        #endregion
    }
}