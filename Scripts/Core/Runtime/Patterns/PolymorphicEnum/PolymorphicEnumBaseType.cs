﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public abstract class PolymorphicEnum<T> where T : PolymorphicEnum<T>
{
    protected static List<T> enumValues = new List<T>();

    [SerializeField]
    public int Key;

    [SerializeField]
    public string Value;

    public PolymorphicEnum(int key, string value)
    {
        Key = key;
        Value = value;
        enumValues.Add((T)this);
    }

    protected static ReadOnlyCollection<T> GetBaseValues()
    {
        return enumValues.AsReadOnly();
    }

    protected static T GetBaseByKey(int key)
    {
        foreach (T t in enumValues)
        {
            if (t.Key == key) return t;
        }
        return null;
    }

    public override string ToString()
    {
        return Value;
    }
}