﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using UnityEngine.SceneManagement;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;
using UnityEditor.Hardware;

namespace CEUtilities.Patterns
{
    public abstract class Singleton : SerializedMonoBehaviour, ISingleton
    {
        public abstract bool IsCurrentSingleton { get; }
    }

    [DefaultExecutionOrder(-5000)]
    public abstract class Singleton<T> : Singleton where T : MonoBehaviour, ISingleton
    {
        #region Exposed fields

        protected static T _instance = null;

        [SerializeField]
        [HideInPlayMode]
        [PropertyOrder(-10)]
        protected bool persistent = false;

        #endregion Exposed fields

        #region Internal fields

        protected static bool _persistent = false;
        protected static bool _persistRoot = true;
        protected static bool _allowAsset = false;
        protected static bool _autoGeneration = true;
        protected static bool _destroyDuplicated = false;
        protected static bool _generationOnlyInPlaymode = true;
        protected static bool _hideLogs = false;
        protected static HideFlags _flags = HideFlags.None;

        protected static object _lock = new object();
        protected static bool _checkedAtts = false;
        protected static bool _appIsQuitting = false;

        protected bool markForDestroy = false;
        private bool warnedInstance = false;

        [SerializeField]
        [HideInInspector]
        private long creationDate;
        [SerializeField]
        [HideInInspector]
        private bool dateSaved = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

#if UNITY_EDITOR
        [OnInspectorGUI]
        [PropertyOrder(-9999)]
        private void DrawInfoBox()
        {
            if (warnedInstance)
            {
                if (IsCurrentSingleton)
                {
                    warnedInstance = false;
                }
                else
                {
                    if (!AllowAsset && IsAsset)
                    {
                        Sirenix.Utilities.Editor.SirenixEditorGUI.MessageBox("This Singleton doesn't allow instances of assets, you may allow it using the CustomSingleton attribute.", UnityEditor.MessageType.Warning);
                    }
                    else
                    {
                        UnityEditor.EditorGUILayout.BeginHorizontal();
                        Sirenix.Utilities.Editor.SirenixEditorGUI.MessageBox("This has been detected as a second instance. Remove this, or the other one.", UnityEditor.MessageType.Warning);
                        if (GUILayout.Button("Go to First", GUILayout.ExpandWidth(false), GUILayout.Height(40)))
                        {
                            UnityEditor.Selection.activeGameObject = Instance.gameObject;
                        }
                        UnityEditor.EditorGUILayout.EndHorizontal();
                    }
                }
            }
        }
#endif

        /// <summary>
        /// Return if the singleton has instance. It will not look for one or create one.
        /// </summary>
        public static bool HasInstance
        {
            get
            {
                return _instance != null;
            }
        }

        /// <summary>
        /// Determines whether the specified Singleton exists or not.
        /// If the singleton auto generates, this call will not create it but will try to find an existing one.
        /// </summary>
        public static bool Exists
        {
            get
            {
                if (!HasInstance)
                {
                    FindInstance();
                }

                return HasInstance;
            }
        }

        /// <summary>
        /// Intance of the Singleton (will try to find it or create if auto generation is enabled).
        /// </summary>
        public static T Instance
        {
            get
            {
                if (_appIsQuitting)
                {
                    LogFormat("{0} is already destroyed. Returning null. Please check Exists first before accessing instance in destructor.", typeof(T));
                    return null;
                }
                lock (_lock)
                {
                    if (!Exists && AutoGenerate)
                    {
                        Generate();
                    }
                }

                return _instance;
            }
        }

        /// <summary>
        /// Does the singleton auto generate an instance on demand?
        /// </summary>
        public static bool AutoGenerate
        {
            get
            {
                if (!_checkedAtts)
                {
                    CheckCustomAttributes();
                }

#if UNITY_EDITOR
                return _autoGeneration && !(_generationOnlyInPlaymode && !Application.isPlaying);
#else
                return _autoGeneration;
#endif
            }
        }

        /// <summary>
        /// If true, the singleton won't be destroyed when the scene changes
        /// </summary>
        protected static bool Persistent
        {
            get
            {
                if (!_checkedAtts)
                {
                    CheckCustomAttributes();
                }

                return _persistent;
            }
        }

        /// <summary>
        /// In case of being a child of another game object, should destroy the parent?
        /// </summary>
        public static bool PersistRoot
        {
            get
            {
                if (!_checkedAtts)
                {
                    CheckCustomAttributes();
                }

                return _persistRoot;
            }
        }

        /// <summary>
        /// If true, the singleton can be assigned from an instance contained in a prefab
        /// </summary>
        protected static bool AllowAsset
        {
            get
            {
                if (!_checkedAtts)
                {
                    CheckCustomAttributes();
                }

                return _allowAsset;
            }
        }
        
        /// <summary>
        /// Gets a value indicating whether [destroy duplicated].
        /// </summary>
        /// <value>
        /// <c>true</c> if [destroy duplicated]; otherwise, <c>false</c>.
        /// </value>
        protected static bool DestroyDuplicated
        {
            get
            {
                if (!_checkedAtts)
                {
                    CheckCustomAttributes();
                }

                return _destroyDuplicated;
            }
        }

        /// <summary>
        /// Should not do any logging?
        /// </summary>
        protected static bool HideLogs
        {
            get
            {
                if (!_checkedAtts)
                {
                    CheckCustomAttributes();
                }

                return _hideLogs;
            }
        }

        /// <summary>
        /// If true, the singleton won't be destroyed when the scene changes
        /// </summary>
        protected static HideFlags Flags
        {
            get
            {
                if (!_checkedAtts)
                {
                    CheckCustomAttributes();
                }

                return _flags;
            }
        }


        /// <summary>
        /// This instance of singleton is persistent
        /// </summary>
        public bool InstancePersistent
        {
            get
            {
                return persistent;
            }

            set
            {
                persistent = value;
            }
        }

        /// <summary>
        /// Determines whether the current instance is the one true singleton
        /// </summary>
        public override bool IsCurrentSingleton
        {
            get
            {
                return Exists && _instance.gameObject.GetInstanceID() == gameObject.GetInstanceID();
            }
        }

        /// <summary>
        /// Values has been assigned
        /// </summary>
        protected bool AssignedInstancedAttributes
        {
            get;
            private set;
        }

        /// <summary>
        /// Is this instance a preab?
        /// </summary>
        protected bool IsAsset => CheckIfAsset(this.gameObject);

        #endregion Properties

        #region Events methods

        /// <summary>
        /// Component serialization event
        /// </summary>
        protected override void OnBeforeSerialize()
        {
            base.OnBeforeSerialize();

#if UNITY_EDITOR
            VerifyInstances("Serialization");
#endif
        }

        /// <summary>
        /// Destroy other instances on wake up
        /// </summary>
        protected virtual void Awake()
        {
            VerifyInstances("Awake");
        }

        /// <summary>
        /// Destroy the singleton along with the instance
        /// </summary>
        protected virtual void OnDestroy()
        {
            if (HasInstance && IsCurrentSingleton)
            {
                _instance = null;
                LogFormat("{0} instance destroyed with the OnDestroy event.", typeof(T));
            }
        }

        /// <summary>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed, it will create a buggy ghost object that will stay on the Editor scene even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </summary>
        protected virtual void OnApplicationQuit()
        {
            if (HasInstance && IsCurrentSingleton && !Application.isEditor)
            {
                _instance = null;
                _appIsQuitting = true;
                LogFormat("{0} instance destroyed with the OnApplicationQuit event.", typeof(T));
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion

        #region Non Public Methods

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        private static void Log(string message) => LogFormat(_instance, LogType.Log, message, new object[0]);

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        private static void Log(UnityEngine.Object caller, string message) => LogFormat(caller, LogType.Log, message, new object[0]);

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        private static void Log(LogType type, string message) => LogFormat(_instance, type, message, new object[0]);

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        private static void Log(UnityEngine.Object caller, LogType type, string message) => LogFormat(caller, type, message, new object[0]);

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        private static void LogFormat(string message, params object[] args) => LogFormat(_instance, LogType.Log, message, args);

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        private static void LogFormat(LogType type, string message, params object[] args) => LogFormat(_instance, type, message, args);

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        private static void LogFormat(UnityEngine.Object caller, string message, params object[] args) => LogFormat(caller, LogType.Log, message, args);

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="type">The type.</param>
        private static void LogFormat(UnityEngine.Object caller, LogType type, string message, params object[] args)
        {
            if (!HideLogs)
            {
                var endMsg = string.Format("[{0}::{1}] {2}", "Singleton", typeof(T).GetAliasOrName(true, true), message);
                switch (type)
                {
                    case LogType.Error: Debug.LogErrorFormat(caller, endMsg, args); break;
                    case LogType.Warning: Debug.LogWarningFormat(caller, endMsg, args); break;
                    default: Debug.LogFormat(caller, endMsg, args); break;
                }
            }
        }


        /// <summary>
        /// Read the attribute to get the custom singleton setup
        /// </summary>
        private static void CheckCustomAttributes()
        {
            var att = typeof(T).GetAttributes<CustomSingletonAttribute>().FirstOrDefault();
            if (att != null)
            {
                _persistent = att.Persistent;
                _allowAsset = att.AllowAsset;
                _autoGeneration = att.AutoGeneration;
                _destroyDuplicated = att.DestroyIfDuplicated;
                _generationOnlyInPlaymode = att.GenerateOnlyInPlaymode;
                _hideLogs = att.HideLogs;
                _flags = att.Flags;
            }
            _checkedAtts = true;
        }

        /// <summary>
        /// Finds any component of this type to set it as the singleton
        /// </summary>
        protected static void FindInstance()
        {
            _instance = FindObjectOfType<T>();
        }

        /// <summary>
        /// Generate a new instance of the singleton
        /// </summary>
        protected static void Generate()
        {
            _instance = new GameObject().AddComponent<T>();
            _instance.gameObject.name = _instance.GetType().Name;
            Log(typeof(T) + " generated a new instance.");
        }


        /// <summary>
        /// Lookup and assign this instance to the Singleton static reference
        /// </summary>
        protected void VerifyInstances(string eventName)
        {
            // Setup date
            SetupCreationDate();

            // Setup attributes
            SetUpAttributes();

            // Verify asset
            if (!AllowAsset && IsAsset)
            {
                if (!warnedInstance)
                {
                    warnedInstance = true;
                    //Debug.LogWarning(typeof(T) + " detected on a prefab asset, ignoring it..", this);
                }
                return;
            }

            // An instance exists
            if (Exists)
            {
                // Is not this one
                if (!IsCurrentSingleton)
                {
                    // This should be the singleton instance? (earlier, or singleton is asset and not allowed)
                    if (InstanceIsEarlier())
                    {
                        _instance = this as T;
                        warnedInstance = false;
                    }

                    // This instance is later
                    else if (!warnedInstance)
                    {
                        if (DestroyDuplicated && !IsAsset && ApplicationHelper.AppInPlayerOrPlaymode)
                        {
                            Debug.LogWarning(typeof(T) + " has a second instance. This one will be destroy.", this);
                            Destroy(gameObject);
                        }
                        else if (!warnedInstance)
                        {
                            warnedInstance = true;
                            Debug.LogWarning(typeof(T) + " has a second instance. Please remove the second one.", this);
                        }
                    }
                }
            }

            // Set this as singleton instance
            else
            {
                _instance = this as T;
                LogFormat("{0} assigned at {1}", typeof(T), eventName);
            }
        }

        /// <summary>
        /// Is the game object a prefab?
        /// </summary>
        private bool CheckIfAsset(GameObject go)
        {
#if UNITY_EDITOR
            return UnityEditor.AssetDatabase.Contains(go);
#else
            return false;
#endif
        }

        /// <summary>
        /// Set up the instance attributes
        /// </summary>
        private void SetUpAttributes()
        {
            if (!AssignedInstancedAttributes)
            {
                hideFlags = Flags;
                InstancePersistent = Persistent;

                if (ApplicationHelper.AppInPlayerOrPlaymode && InstancePersistent)
                {
                    if (gameObject.transform.parent == null)
                    {
                        DontDestroyOnLoad(gameObject);
                    }
                    else if (PersistRoot)
                    {
                        DontDestroyOnLoad(gameObject.transform.root);
                    }
                }

                AssignedInstancedAttributes = true;
            }
        }

        /// <summary>
        /// Check the date
        /// </summary>
        private void SetupCreationDate()
        {
            if (!dateSaved)
            {
                dateSaved = true;
                creationDate = DateTime.Now.Ticks;
            }
        }

        /// <summary>
        /// Use the creation time for find the first instance
        /// </summary>
        private bool InstanceIsEarlier()
        {
            // This instance is earlier
            return (Instance as Singleton<T>).creationDate > creationDate;
        }

        #endregion
    }
}
