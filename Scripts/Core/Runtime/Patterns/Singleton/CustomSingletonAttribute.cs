﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Patterns
{
	[AttributeUsage(AttributeTargets.Class)]
	public class CustomSingletonAttribute : Attribute
	{
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

		/// <summary>
		/// Gets or sets a value indicating whether [destroy if duplicated].
		/// </summary>
		/// <value>
		/// <c>true</c> if [destroy if duplicated]; otherwise, <c>false</c>.
		/// </value>
		public bool DestroyIfDuplicated { get; set; } = false;
		
        /// <summary>
        /// The singleton is not destroyed between scenes.
        /// </summary>
        public bool Persistent { get; set; } = false;

        /// <summary>
        /// In case of being a child of another game object, should destroy the parent?
        /// </summary>
        public bool PersistRoot { get; set; } = false;

        /// <summary>
        /// If true, the singleton can be assigned from an instance contained in a prefab.
        /// </summary>
        public bool AllowAsset { get; set; } = false;

        /// <summary>
        /// Should create an instance of the singleton on demand?
        /// </summary>
        public bool AutoGeneration { get; set; } = true;

        /// <summary>
        /// In case of auto generation, will it only be generated in playmode?
        /// </summary>
        public bool GenerateOnlyInPlaymode { get; set; } = true;

        /// <summary>
        /// HideFlags of the Singleton.
        /// </summary>
        public HideFlags Flags { get; set; } = HideFlags.None;

        /// <summary>
        /// Should not do any logging?
        /// </summary>
        public bool HideLogs { get; set; } = false;

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Methods

        #endregion Methods
    }
}