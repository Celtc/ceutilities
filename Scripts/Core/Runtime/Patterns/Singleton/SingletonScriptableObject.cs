﻿using UnityEngine;
using System.Linq;
using System;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

namespace CEUtilities.Patterns
{
    /// <summary>
    /// Abstract class for making reload-proof singletons out of ScriptableObjects
    /// Returns the asset created on the editor, or null if there is none
    /// Based on https://www.youtube.com/watch?v=VBA1QCoEAX4
    /// </summary>
    /// <typeparam name="T">Singleton type</typeparam>
    public abstract class SingletonScriptableObject<T> : SerializedScriptableObject, ISingleton where T : ScriptableObject, ISingleton
    {
        #region Exposed fields

        protected static T _instance = null;

        #endregion Exposed fields

        #region Internal fields

        protected static bool _persistent = false;
        protected static bool _autoGeneration = false;
        protected static HideFlags _flags = HideFlags.None;

        protected static object _lock = new object();
        protected static bool _checkedAtts = false;
        protected static bool _appIsQuitting = false;

        protected bool markForDestroy = false;
        private bool warnedInstance = false;

        [SerializeField]
        [HideInInspector]
        private long creationDate;
        [SerializeField]
        [HideInInspector]
        private bool dateSaved = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

#if UNITY_EDITOR
        [OnInspectorGUI]
        [PropertyOrder(-9999)]
        private void DrawInfoBox()
        {
            if (warnedInstance)
            {
                if (IsCurrentSingleton)
                    warnedInstance = false;
                else
                    Sirenix.Utilities.Editor.SirenixEditorGUI.MessageBox("This has been detected as a second instance. Remove this, or the other one.", UnityEditor.MessageType.Warning);
            }
        }
#endif

        /// <summary>
        /// Return if the singleton has instance. It will not look for one.
        /// </summary>
        public static bool HasInstance
        {
            get
            {
                return _instance != null;
            }
        }

        /// <summary>
        /// Determines whether the specified Singleton exists or not.
        /// If the singleton auto generates, this call will not create it.
        /// </summary>
        public static bool Exists
        {
            get
            {
#if UNITY_EDITOR
                if (_instance == null)
                    FindInstance();
                return (_instance != null);
#else
			    if ((object)_instance == null)
				    FindInstance();
			    return ((object) _instance != null);
#endif
            }
        }

        /// <summary>
        /// Intance of the Singleton
        /// </summary>
        public static T Instance
        {
            get
            {
                if (_appIsQuitting)
                {
                    Debug.Log(typeof(T) + " is already destroyed. Returning null. Please check Exists first before accessing instance in destructor.");
                    return null;
                }
                lock (_lock)
                    if (!Exists && AutoGenerate)
                        Generate();

                return _instance;
            }
        }

        /// <summary>
        /// Does the singleton auto generate an instance on demand?
        /// </summary>
        public static bool AutoGenerate
        {
            get
            {
                if (!_checkedAtts)
                    CheckCustomAttributes();

                return _autoGeneration;
            }
        }

        /// <summary>
        /// If true, the singleton won't be destroyed when the scene changes
        /// </summary>
        protected static bool Persistent
        {
            get
            {
                if (!_checkedAtts)
                    CheckCustomAttributes();

                return _persistent;
            }
        }

        /// <summary>
        /// If true, the singleton won't be destroyed when the scene changes
        /// </summary>
        protected static HideFlags Flags
        {
            get
            {
                if (!_checkedAtts)
                    CheckCustomAttributes();

                return _flags;
            }
        }


        /// <summary>
        /// Determines whether the current instance is the one true singleton
        /// </summary>
        public bool IsCurrentSingleton
        {
            get
            {
                return Exists && _instance.GetInstanceID() == GetInstanceID();
            }
        }

        /// <summary>
        /// Values has been assigned
        /// </summary>
        protected bool AssignedInstancedAttributes
        {
            get;
            private set;
        }

        #endregion Properties

        #region Events methods

        /// <summary>
        /// Component serialization event
        /// </summary>
        protected override void OnBeforeSerialize()
        {
            base.OnBeforeSerialize();

            VerifyInstances("Serialization");
        }

        /// <summary>
        /// Destroy other instances on wake up
        /// </summary>
        protected virtual void Awake()
        {
            VerifyInstances("Awake");
        }

        /// <summary>
        /// Destroy the singleton along with the instance
        /// </summary>
        protected virtual void OnDestroy()
        {
            if (IsCurrentSingleton)
            {
                _instance = null;
                Debug.Log(typeof(T) + " instance destroyed with the OnDestroy event");
            }
        }

        /// <summary>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed, it will create a buggy ghost object that will stay on the Editor scene even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </summary>
        protected virtual void OnApplicationQuit()
        {
            if (IsCurrentSingleton && !Application.isEditor)
            {
                _instance = null;
                _appIsQuitting = true;
                Debug.Log(typeof(T) + " instance destroyed with the OnApplicationQuit event");
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Read the attribute to get the custom singleton setup
        /// </summary>
        private static void CheckCustomAttributes()
        {
            var att = typeof(T).GetAttributes<CustomSingletonAttribute>().FirstOrDefault();
            if (att != null)
            {
                _persistent = att.Persistent;
                _autoGeneration = att.AutoGeneration;
                _flags = att.Flags;
            }
            _checkedAtts = true;
        }

        /// <summary>
        /// Finds any instance of this singleton (loaded or unloaded)
        /// </summary>
        protected static void FindInstance()
        {
            // Search for loaded instance
            var instances = Resources.FindObjectsOfTypeAll<T>();
            if (instances.Length > 0)
                _instance = instances.First();

            // Try loading one in resources
            if (_instance == null)
            {
                instances = Resources.LoadAll<T>("");
                if (instances.Length > 0)
                    _instance = instances.First();
            }
        }

        /// <summary>
        /// Generate a new instance of the singleton
        /// </summary>
        protected static void Generate()
        {
            _instance = CreateInstance<T>();
            Debug.Log(typeof(T) + " generated a new instance");
        }


        /// <summary>
        /// Lookup and assign this instance to the Singleton static reference
        /// </summary>
        protected void VerifyInstances(string eventName)
        {
            // Setup date
            SetupCreationDate();

            // Setup attributes
            SetUpAttributes();

            // An instance exists
            if (Exists)
            {
                // Another already exists
                if (!IsCurrentSingleton)
                {
                    // This should be the singleton instance
                    if (InstanceIsEarlier())
                    {
                        _instance = this as T;
                        warnedInstance = false;
                    }

                    // This instance is later
                    else if (!warnedInstance)
                    {
                        warnedInstance = true;
                        Debug.LogWarning(typeof(T) + " has a second instance. Please remove the second one.", this);
                    }                    
                }
            }

            // Set this as singleton instance
            else
            {
                _instance = this as T;
                Debug.Log(typeof(T) + " assigned at " + eventName);
            }
        }

        /// <summary>
        /// Set up the instance attributes
        /// </summary>
        private void SetUpAttributes()
        {
            if (!AssignedInstancedAttributes)
            {
                hideFlags = Flags;

                if (Persistent)
                {
                    //TODO: Force instance save in asset
                }

                AssignedInstancedAttributes = true;
            }
        }

        /// <summary>
        /// Check the date
        /// </summary>
        private void SetupCreationDate()
        {
            if (!dateSaved)
            {
                dateSaved = true;
                creationDate = DateTime.Now.Ticks;
            }
        }

        /// <summary>
        /// Use the creation time for find the first instance
        /// </summary>
        private bool InstanceIsEarlier()
        {
            // This instance is earlier
            if ((Instance as SingletonScriptableObject<T>).creationDate > creationDate)
                return true;
            else
                return false;
        }

        #endregion Non Public Methods
    }
}