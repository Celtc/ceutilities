﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Patterns
{
    public interface ISingleton
    {
        /// <summary>
        /// Determines whether the current instance is the one true singleton
        /// </summary>
        bool IsCurrentSingleton { get; }
    }
}