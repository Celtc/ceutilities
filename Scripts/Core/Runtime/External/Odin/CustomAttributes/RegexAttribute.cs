﻿namespace Sirenix.OdinInspector
{
    using System;

    /// <summary>
    /// Regex string validation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    [DontApplyToListElements]
    public class RegexAttribute : Attribute
    {
        #region Exposed fields

        public readonly string pattern;

        public readonly string helpMessage;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Regex string validation
        /// </summary>
        public RegexAttribute(string pattern)
        {
            this.pattern = pattern;
            this.pattern = "Pattern fails regex validation";
        }

        /// <summary>
        /// Regex string validation
        /// </summary>
        public RegexAttribute(string pattern, string helpMessage)
        {
            this.pattern = pattern;
            this.helpMessage = helpMessage;
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}