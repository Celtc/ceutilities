﻿//-----------------------------------------------------------------------
// <copyright file="GUIColorAttribute.cs" company="Sirenix IVS">
// Copyright (c) Sirenix IVS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Sirenix.OdinInspector
{
    using System;
    using UnityEngine;

    /// <summary>
    /// <para>GUIColorIf is used on any property and changes the GUI color used to draw the property depending of the value of another member.</para>
    /// </summary>
    /// <example>
    /// <para>The following example shows how GUIColorIf is used.</para>
    /// <code>
    /// public class MyComponent : MonoBehaviour
    ///	{
    ///		[HideLabel]
    ///		[GUIColorIf("check", 1f, 0f, 0f, 1f, 1f, 1f, 1f, 1f)]
    ///		public int A;
    ///	
    ///		public bool check = true;
    ///	}
    /// </code>
    /// </example>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class GUIColorIfAttribute : Attribute
    {
        /// <summary>
        /// The GUI color of the property if true
        /// </summary>
        public Color ColorTrue { get; private set; }

        /// <summary>
        /// The GUI color of the property if false
        /// </summary>
        public Color ColorFalse { get; private set; }

        /// <summary>
        /// Will paint if the check is false
        /// </summary>
        public bool HasFalseColor { get; private set; }

        /// <summary>
        /// The name of a bool member field, property or method.
        /// </summary>
        public string MemberName { get; private set; }

        /// <summary>
        /// The optional member value.
        /// </summary>
        public object Value { get; private set; }

        /// <summary>
        /// Sets the GUI color for the property depending of a member check.
        /// </summary>
        /// <param name="r1">If true color: The red channel.</param>
        /// <param name="g1">If true color: The green channel.</param>
        /// <param name="b1">If true color: The blue channel.</param>
        /// <param name="a1">If true color: The alpha channel.</param>
        public GUIColorIfAttribute(string memberName, float r, float g, float b, float a)
        {
            this.MemberName = memberName;
            this.HasFalseColor = false;
            this.ColorTrue = new Color(r, g, b, a);
        }

        /// <summary>
        /// Sets the GUI color for the property depending of a member check.
        /// </summary>
        /// <param name="r1">If true color: The red channel.</param>
        /// <param name="g1">If true color: The green channel.</param>
        /// <param name="b1">If true color: The blue channel.</param>
        /// <param name="a1">If true color: The alpha channel.</param>
        public GUIColorIfAttribute(string memberName, object optionalValue, float r, float g, float b, float a)
        {
            this.MemberName = memberName;
            this.Value = optionalValue;
            this.HasFalseColor = false;
            this.ColorTrue = new Color(r, g, b, a);
        }

        /// <summary>
        /// Sets the GUI color for the property depending of a member check.
        /// </summary>
        /// <param name="r1">If true color: The red channel.</param>
        /// <param name="g1">If true color: The green channel.</param>
        /// <param name="b1">If true color: The blue channel.</param>
        /// <param name="a1">If true color: The alpha channel.</param>
        /// <param name="r2">If false color: The red channel.</param>
        /// <param name="g2">If false color: The green channel.</param>
        /// <param name="b2">If false color: The blue channel.</param>
        /// <param name="a2">If false color: The alpha channel.</param>
        public GUIColorIfAttribute(string memberName, float r1, float g1, float b1, float a1, float r2, float g2, float b2, float a2)
        {
            this.MemberName = memberName;
            this.HasFalseColor = true;
            this.ColorTrue = new Color(r1, g1, b1, a1);
            this.ColorFalse = new Color(r2, g2, b2, a2);
        }

        /// <summary>
        /// Sets the GUI color for the property depending of a member check.
        /// </summary>
        /// <param name="r1">If true color: The red channel.</param>
        /// <param name="g1">If true color: The green channel.</param>
        /// <param name="b1">If true color: The blue channel.</param>
        /// <param name="a1">If true color: The alpha channel.</param>
        /// <param name="r2">If false color: The red channel.</param>
        /// <param name="g2">If false color: The green channel.</param>
        /// <param name="b2">If false color: The blue channel.</param>
        /// <param name="a2">If false color: The alpha channel.</param>
        public GUIColorIfAttribute(string memberName, object optionalValue, float r1, float g1, float b1, float a1, float r2, float g2, float b2, float a2)
        {
            this.MemberName = memberName;
            this.Value = optionalValue;
            this.HasFalseColor = true;
            this.ColorTrue = new Color(r1, g1, b1, a1);
            this.ColorFalse = new Color(r2, g2, b2, a2);
        }
    }
}