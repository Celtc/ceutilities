﻿namespace Sirenix.OdinInspector
{
    using System;
    using UnityEngine;
    
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    [DontApplyToListElements]
    public class MinimalGuidAttribute : Attribute
    {
        public MinimalGuidAttribute()
        {
        }
    }
}