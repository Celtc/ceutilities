﻿//-----------------------------------------------------------------------
// <copyright file="IndentAttribute.cs" company="Sirenix IVS">
// Copyright (c) Sirenix IVS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Sirenix.OdinInspector
{
    using System;

    /// <summary>
	/// <para>Indent is used on any property and moves the property's label to the right.</para>
	/// <para>Use this to clearly organize properties in the inspector.</para>
    /// </summary>
	/// <example>
	/// <para>The following example shows how a property is indented by Indent.</para>
    /// <code>
	///	public class MyComponent : MonoBehaviour
	///	{
	///		[Indenting("IndentLevel")]
	///		public int IndentedInt;
    ///		
    ///     public int IndentLevel = 2;
	///	}
	/// </code>
    /// </example>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    [DontApplyToListElements]
    public sealed class IndentingAttribute : Attribute
    {
        /// <summary>
        /// The name of a bool member field, property or method.
        /// </summary>
        public string MemberName { get; private set; }

        /// <summary>
        /// Indents a property in the inspector.
        /// </summary>
        /// <param name="memberName">Member from which the value will be taken.</param>
        public IndentingAttribute(string memberName)
        {
            this.MemberName = memberName;
        }
    }
}