﻿namespace Sirenix.OdinInspector
{
    using System;
    using UnityEngine;

    /// <summary>
    /// Spacing is used to inject space before and/or after field rendering.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method)]
    [DontApplyToListElements]
    public class SpacingAttribute : Attribute
    {
        /// <summary>
        /// The amount of space that should be added before field rendering.
        /// </summary>
        public int Before { get; set; }

        /// <summary>
        /// The amount of space that should be added after field rendering.
        /// </summary>
        public int After { get; set; }

        public SpacingAttribute() { }

        public SpacingAttribute(int before)
        {
            this.Before = before;
        }

        public SpacingAttribute(int before, int after)
        {
            this.After = after;
            this.Before = before;
        }
    }
}