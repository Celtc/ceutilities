﻿namespace Sirenix.OdinInspector
{
    using System;
	
	/// <summary>
    /// The TableMatrixExtended attribute adds new property for showing different rows and columns names.
    /// </example>
    public class TableMatrixExtendedAttribute : TableMatrixAttribute
    {
        /// <summary>
        /// The width of the column containg the labels of every row
        /// </summary>
        public int RowLabelsWidth = 25;

        /// <summary>
        /// Array of string containing the horizontal labels
        /// </summary>
        public string RowLabelsMemberName = null;

        /// <summary>
        /// Array of string containing the vertical labels
        /// </summary>
        public string ColumnLabelsMemberName = null;
    }
}