﻿namespace Sirenix.OdinInspector
{
    using System;

    /// <summary>
    /// Used in bool fields or properties, for showing the value as a toggle on / off button.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ToggleButtonAttribute : Attribute
    {
        /// <summary>
        /// Gets or sets the label of the button.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the name of the GUI style.
        /// </summary>
        /// <value>
        /// The name of the GUI style.
        /// </value>
        public string Style { get; set; }

        /// <summary>
        /// Gets or sets the GUIContent from the Unity builtin resources with the given name.
        /// </summary>
        /// <value>
        /// The content of the icon.
        /// </value>
        public string IconContent { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToggleButtonAttribute"/> class.
        /// </summary>
        public ToggleButtonAttribute()
        {
            Label = null;
            Style = null;
            IconContent = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToggleButtonAttribute"/> class.
        /// </summary>
        /// <param name="label">The label.</param>
        public ToggleButtonAttribute(string label)
        {
            Label = label;
            Style = null;
            IconContent = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToggleButtonAttribute"/> class.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="style">The style.</param>
        public ToggleButtonAttribute(string label, string style)
        {
            Label = label;
            Style = style;
            IconContent = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToggleButtonAttribute"/> class.
        /// </summary>
        /// <param name="label">The label.</param>
        /// <param name="style">The style.</param>
        /// <param name="iconContent">Content of the icon.</param>
        public ToggleButtonAttribute(string label, string style, string iconContent)
        {
            Label = label;
            Style = style;
            IconContent = iconContent;
        }
    }
}