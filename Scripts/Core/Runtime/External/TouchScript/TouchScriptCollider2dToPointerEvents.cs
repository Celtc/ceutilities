﻿#if CEUTILITIES_TOUCHSCRIPT
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;
using System.Collections;

using TouchScript;

namespace CEUtilities.TouchScript
{
    /// <summary>
    /// IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    public class TouchScriptCollider2dToPointerEvents : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private GameObject eventsReceiver;

        #endregion Exposed fields

        #region Internal fields

        private Canvas rootCanvas;
        private CanvasScaler scaler;

        private Collider2D colliderComp;

        private Dictionary<int, bool> trackedPointers = new Dictionary<int, bool>();

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        private Collider2D ColliderComp
        {
            get
            {
                if (colliderComp == null)
                    colliderComp = GetComponent<Collider2D>();

                return colliderComp;
            }

            set
            {
                colliderComp = value;
            }
        }

        private GameObject EventsReceiver
        {
            get
            {
                if (eventsReceiver == null)
                    eventsReceiver = gameObject;

                return eventsReceiver;
            }
        }

        private IPointerDownHandler[] PointerDownHandlerReceivers => EventsReceiver.GetComponents<IPointerDownHandler>();

        private IPointerUpHandler[] PointerUpHandlerReceivers => EventsReceiver.GetComponents<IPointerUpHandler>();

        private IPointerEnterHandler[] PointerEnterHandlerReceivers => EventsReceiver.GetComponents<IPointerEnterHandler>();

        private IPointerExitHandler[] PointerExitHandlerReceivers => EventsReceiver.GetComponents<IPointerExitHandler>();

        private IPointerClickHandler[] PointerClickHandlerReceivers => EventsReceiver.GetComponents<IPointerClickHandler>();

        #endregion Properties

        #region Events methods

        private void Awake()
        {
            var rt = GetComponent<RectTransform>();
            rootCanvas = rt.GetComponentInParent<Canvas>().rootCanvas;
            scaler = rootCanvas.GetComponent<CanvasScaler>();
        }

        private void OnEnable()
        {
            if (TouchManager.Instance != null)
            {
                TouchManager.Instance.PointersPressed += Instance_PointersPressed;
                TouchManager.Instance.PointersUpdated += Instance_PointersUpdated;
                TouchManager.Instance.PointersReleased += Instance_PointersReleased;
            }
        }

        private void OnDisable()
        {
            if (TouchManager.Instance != null)
            {
                TouchManager.Instance.PointersPressed -= Instance_PointersPressed;
                TouchManager.Instance.PointersUpdated -= Instance_PointersUpdated;
                TouchManager.Instance.PointersReleased -= Instance_PointersReleased;
            }
        }

        private void Instance_PointersPressed(object sender, PointerEventArgs e)
        {
            foreach (var pointer in e.Pointers)
            {
                // Tracked?
                var tracked = trackedPointers.ContainsKey(pointer.Id);
                if (tracked)
                {
                    // Now pressing?
                    var pressing = trackedPointers[pointer.Id];
                    if (!pressing)
                    {
                        trackedPointers[pointer.Id] = true;
                        var eventData = NewEventData();
                        PointerDownHandlerReceivers.ForEach(x => x.OnPointerDown(eventData));
                    }
                }
            }
        }

        /// <summary>
        /// On Input Update
        /// </summary>
        private void Instance_PointersUpdated(object sender, PointerEventArgs e)
        {
            foreach(var pointer in e.Pointers)
            {
                var tracked = trackedPointers.ContainsKey(pointer.Id);

                Vector2 checkingPoint = new Vector2(-1f, -1f);

                // Screen space overlay
                if (rootCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
                    checkingPoint = pointer.Position;

                // Screen space camera
                else if (rootCanvas.renderMode == RenderMode.ScreenSpaceCamera)
                    checkingPoint = rootCanvas.worldCamera.ScreenToWorldPoint(pointer.Position.ToVector3(rootCanvas.planeDistance));
                
                // Check collision
                var inside = ColliderComp.OverlapPoint(checkingPoint);

                // Enter?
                if (!tracked && inside)
                {
                    // Add
                    trackedPointers.Add(pointer.Id, false);
                    var eventData = NewEventData();
                    PointerEnterHandlerReceivers.ForEach(x => x.OnPointerEnter(eventData));
                }

                // Exit?
                else if (tracked && !inside)
                {
                    // Was pressing? Release
                    var eventData = NewEventData();
                    var pressing = trackedPointers[pointer.Id];
                    if (pressing)
                        PointerUpHandlerReceivers.ForEach(x => x.OnPointerUp(eventData));

                    // Remove
                    trackedPointers.Remove(pointer.Id);
                    PointerExitHandlerReceivers.ForEach(x => x.OnPointerExit(eventData));
                }
            }
        }

        private void Instance_PointersReleased(object sender, PointerEventArgs e)
        {
            foreach (var pointer in e.Pointers)
            {
                // Tracked?
                var tracked = trackedPointers.ContainsKey(pointer.Id);
                if (tracked)
                {
                    // Now pressing?
                    var pressing = trackedPointers[pointer.Id];
                    if (pressing)
                    {
                        trackedPointers[pointer.Id] = false;
                        var eventData = NewEventData();
                        PointerUpHandlerReceivers.ForEach(x => x.OnPointerUp(eventData));
                        PointerClickHandlerReceivers.ForEach(x => x.OnPointerClick(eventData));
                    }
                }
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        private PointerEventData NewEventData()
        {
            return new PointerEventData(EventSystem.current);
        }

        #endregion Methods
    }
}
#endif