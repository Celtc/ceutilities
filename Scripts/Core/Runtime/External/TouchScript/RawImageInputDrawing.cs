﻿#if CEUTILITIES_TOUCHSCRIPT
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using TouchScript;

namespace CEUtilities.TouchScript
{
    public class TextureDrawer : MonoBehaviour
    {
        #region Exposed fields

        [InfoBox("Allows using any input device to draw over a texture, like a pencil.")]

        [SerializeField]
        private RawImage textureContainer;

        [SerializeField]
        private Vector2Int resolution = new Vector2Int(1920, 1080);

        [SerializeField]
        private Texture2D pointerImage;

        #endregion Exposed fields

        #region Internal fields

        private Texture2D drawingTexture;

        private Vector2Int pointerOffset;

        private Dictionary<int, Vector2Int> pointersLastPoint = new Dictionary<int, Vector2Int>();

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private void Awake()
        {
            Init();
        }

        private void OnEnable()
        {
            if (TouchManager.Instance != null)
            {
                TouchManager.Instance.PointersPressed += Instance_PointersPressed;
                TouchManager.Instance.PointersUpdated += Instance_PointersUpdated;
                TouchManager.Instance.PointersReleased += Instance_PointersReleased;
            }
        }

        private void OnDisable()
        {
            if (TouchManager.Instance != null)
            {
                TouchManager.Instance.PointersPressed -= Instance_PointersPressed;
                TouchManager.Instance.PointersUpdated -= Instance_PointersUpdated;
                TouchManager.Instance.PointersReleased -= Instance_PointersReleased;
            }
        }

        private void Instance_PointersPressed(object sender, PointerEventArgs e)
        {
            // Cached starting points
            var count = e.Pointers.Count;
            for (var i = 0; i < count; i++)
            {
                var pointer = e.Pointers[i];
                if (pointersLastPoint.ContainsKey(pointer.Id))
                    pointersLastPoint[pointer.Id] = pointer.Position.ToVector2Int();
                else
                    pointersLastPoint.Add(pointer.Id, pointer.Position.ToVector2Int());
            }
        }

        private void Instance_PointersUpdated(object sender, PointerEventArgs e)
        {
            var count = e.Pointers.Count;
            for (var i = 0; i < count; i++)
            {
                var pointer = e.Pointers[i];
                if (pointer.Buttons.HasFlag(TouchScript.Pointers.Pointer.PointerButtonState.FirstButtonPressed))
                {
                    // Process line from last position
                    if (pointersLastPoint.ContainsKey(pointer.Id))
                    {
                        var lastPoint = pointersLastPoint[pointer.Id];
                        var currentPoint = pointer.Position.ToVector2Int();
                        DrawLine(lastPoint, currentPoint, pointerImage, drawingTexture);
                        pointersLastPoint[pointer.Id] = currentPoint;
                    }
                    // Not previously added, add the pointer
                    else
                        pointersLastPoint.Add(pointer.Id, pointer.Position.ToVector2Int());
                }
            }
        }

        private void Instance_PointersReleased(object sender, PointerEventArgs e)
        {
            // Remove from cache
            var count = e.Pointers.Count;
            for (var i = 0; i < count; i++)
            {
                var pointer = e.Pointers[i];
                if (pointersLastPoint.ContainsKey(pointer.Id))
                    pointersLastPoint.Remove(pointer.Id);
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Initialize
        /// </summary>
        private void Init()
        {
            CreateTexture();

            pointerOffset = new Vector2Int((int)(pointerImage.width * .5f), (int)(pointerImage.height * .5f));
        }

        /// <summary>
        /// Create and set the texture
        /// </summary>
        private void CreateTexture()
        {
            drawingTexture = new Texture2D(resolution.x, resolution.y, TextureFormat.RGBA32, false, true);
            //drawingTexture.alphaIsTransparency = true;
            for (int x = 0; x < resolution.x; x++)
                for (int y = 0; y < resolution.y; y++)
                    drawingTexture.SetPixel(x, y, new Color(0, 0, 0, 0));
            drawingTexture.Apply();
            textureContainer.texture = drawingTexture;
            textureContainer.enabled = true;
        }

        /// <summary>
        /// Draw a line
        /// </summary>
        private void DrawLine(Vector2Int a, Vector2Int b, Texture2D pointer, Texture2D target)
        {
            var ratio = new Vector2((float)resolution.x / Screen.width, (float)resolution.y / Screen.height);
            var scaledA = new Vector2Int((int)(a.x * ratio.x), (int)(a.y * ratio.y));
            var scaledB = new Vector2Int((int)(b.x * ratio.x), (int)(b.y * ratio.y));

            var points = GetBresenhamLine(scaledA, scaledB);
            var filteredPoints = new List<Vector2Int>();

            var step = (int)(pointer.width * .5f);
            for (int i = 0; i < points.Count; i += step)
                filteredPoints.Add(points[i]);

            for (int i = 0; i < filteredPoints.Count; i++)
                DrawPoint(pointer, target, filteredPoints[i]);

            target.Apply();
        }

        /// <summary>
        /// Draw into texture
        /// </summary>
        private void DrawPoint(Texture2D pointer, Texture2D target, Vector2Int position)
        {
            var texPosition = new Vector2Int(position.x - pointerOffset.x, position.y - pointerOffset.y);

            Color baseColor, pointerColor, endColor;
            for (int x = 0; x < pointer.width; x++)
            {
                for (int y = 0; y < pointer.height; y++)
                {
                    pointerColor = pointer.GetPixel(x, y);
                    if (pointerColor.a < .25f)
                        continue;

                    baseColor = target.GetPixel(x + texPosition.x, y + texPosition.y);
                    endColor = Color.Lerp(baseColor, pointerColor, pointerColor.a)
                        .WithA(Mathf.Max(pointerColor.a, baseColor.a));

                    target.SetPixel(x + texPosition.x, y + texPosition.y, endColor);
                }
            }
        }

        public static List<Vector2Int> GetBresenhamLine(Vector2Int p0, Vector2Int p1)
        {
            int x0 = p0.x;
            int y0 = p0.y;
            int x1 = p1.x;
            int y1 = p1.y;
            int dx = Mathf.Abs(x1 - x0);
            int dy = Mathf.Abs(y1 - y0);

            int sx = x0 < x1 ? 1 : -1;
            int sy = y0 < y1 ? 1 : -1;

            int err = dx - dy;

            var points = new List<Vector2Int>();

            while (true)
            {
                points.Add(new Vector2Int(x0, y0));
                if (x0 == x1 && y0 == y1) break;

                int e2 = 2 * err;
                if (e2 > -dy)
                {
                    err = err - dy;
                    x0 = x0 + sx;
                }
                if (e2 < dx)
                {
                    err = err + dx;
                    y0 = y0 + sy;
                }
            }

            return points;
        }


        #endregion Methods
    }
}
#endif