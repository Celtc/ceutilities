﻿#if CEUTILITIES_TOUCHSCRIPT
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using TouchScript;

namespace CEUtilities.TouchScript
{
    [RequireComponent(typeof(Collider2D))]
    public class TouchScriptCollider2dToButton : MonoBehaviour
    {
        #region Exposed fields

        [InfoBox("This component is deprecated. Use TouchScriptCollider2dToPointerEvents instead.", InfoMessageType.Warning)]

        [SerializeField]
        private Button target;

        #endregion Exposed fields

        #region Internal fields

        private Collider2D coll;

        private Dictionary<int, bool> trackedPointers = new Dictionary<int, bool>();

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        private Collider2D Collider
        {
            get
            {
                if (coll == null)
                    coll = GetComponent<Collider2D>();

                return coll;
            }

            set
            {
                coll = value;
            }
        }

        private Button Target
        {
            get
            {
                if (target == null)
                    target = GetComponent<Button>();

                return target;
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            if (TouchManager.Instance != null)
            {
                TouchManager.Instance.PointersPressed += Instance_PointersPressed;
                TouchManager.Instance.PointersUpdated += Instance_PointersUpdated;
                TouchManager.Instance.PointersReleased += Instance_PointersReleased;
            }
        }

        private void OnDisable()
        {
            if (TouchManager.Instance != null)
            {
                TouchManager.Instance.PointersPressed -= Instance_PointersPressed;
                TouchManager.Instance.PointersUpdated -= Instance_PointersUpdated;
                TouchManager.Instance.PointersReleased -= Instance_PointersReleased;
            }
        }

        private void Instance_PointersPressed(object sender, PointerEventArgs e)
        {
            foreach (var pointer in e.Pointers)
            {
                // Tracked?
                var tracked = trackedPointers.ContainsKey(pointer.Id);
                if (tracked)
                {
                    // Now pressing?
                    var pressing = trackedPointers[pointer.Id];
                    if (!pressing)
                    {
                        trackedPointers[pointer.Id] = true;
                        Target.OnPointerDown(NewEventData());
                    }
                }
            }
        }

        /// <summary>
        /// On Input Update
        /// </summary>
        private void Instance_PointersUpdated(object sender, PointerEventArgs e)
        {
            foreach(var pointer in e.Pointers)
            {
                var tracked = trackedPointers.ContainsKey(pointer.Id);
                var inside = Collider.OverlapPoint(pointer.Position);

                // Enter?
                if (!tracked && inside)
                {
                    // Add
                    trackedPointers.Add(pointer.Id, false);
                    Target.OnPointerEnter(NewEventData());
                }

                // Exit?
                else if (tracked && !inside)
                {
                    // Was pressing? Release
                    var pressing = trackedPointers[pointer.Id];
                    if (pressing)
                        Target.OnPointerUp(NewEventData());

                    // Remove
                    trackedPointers.Remove(pointer.Id);
                    Target.OnPointerExit(NewEventData());
                }
            }
        }

        private void Instance_PointersReleased(object sender, PointerEventArgs e)
        {
            foreach (var pointer in e.Pointers)
            {
                // Tracked?
                var tracked = trackedPointers.ContainsKey(pointer.Id);
                if (tracked)
                {
                    // Now pressing?
                    var pressing = trackedPointers[pointer.Id];
                    if (pressing)
                    {
                        trackedPointers[pointer.Id] = false;
                        Target.OnPointerDown(NewEventData());
                        target.onClick.Invoke();
                    }
                }
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        private PointerEventData NewEventData()
        {
            return new PointerEventData(EventSystem.current);
        }

        #endregion Methods
    }
}
#endif