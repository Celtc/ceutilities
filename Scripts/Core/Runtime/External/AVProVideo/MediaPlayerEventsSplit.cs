﻿#if AVPROVIDEO
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

using RenderHeads.Media.AVProVideo;

[RequireComponent(typeof(MediaPlayer))]
public class MediaPlayerEventsSplit : MonoBehaviour 
{
    #region Exposed fields
    
    #endregion Exposed fields

    #region Internal fields

	MediaPlayer player;

	#endregion Internal fields

	#region Custom Events

	public UnityEvent_Int OnMetaDataReady = new UnityEvent_Int();
	public UnityEvent_Int OnReadyToPlay = new UnityEvent_Int();
	public UnityEvent_Int OnStarted = new UnityEvent_Int();
	public UnityEvent_Int OnFirstFrameReady = new UnityEvent_Int();
	public UnityEvent_Int OnFinishedPlaying = new UnityEvent_Int();
	public UnityEvent_Int OnClosing = new UnityEvent_Int();
	public UnityEvent_Int OnError = new UnityEvent_Int();
	public UnityEvent_Int OnSubtitleChange = new UnityEvent_Int();
	public UnityEvent_Int OnStalled = new UnityEvent_Int();
	public UnityEvent_Int OnUnstalled = new UnityEvent_Int();

	#endregion Custom Events

	#region Properties

	private MediaPlayer Player
	{
		get
		{
			if (player == null)
				player = GetComponent<MediaPlayer>();

			return player;
		}
	}

	#endregion Properties

	#region Events methods

	private void OnEnable()
	{
		Player.Events.AddListener(OnEventCallback);
	}

	private void OnDisable()
	{
		Player.Events.RemoveListener(OnEventCallback);
	}

	private void OnEventCallback(MediaPlayer player, MediaPlayerEvent.EventType eventType, ErrorCode error)
	{
		switch(eventType)
		{
			case MediaPlayerEvent.EventType.MetaDataReady: OnMetaDataReady.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.ReadyToPlay: OnReadyToPlay.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.Started: OnStarted.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.FirstFrameReady: OnFirstFrameReady.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.FinishedPlaying: OnFinishedPlaying.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.Closing: OnClosing.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.Error: OnError.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.SubtitleChange: OnSubtitleChange.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.Stalled: OnStalled.Invoke((int)error); break;
			case MediaPlayerEvent.EventType.Unstalled: OnUnstalled.Invoke((int)error); break;
		}
	}

	#endregion Events methods

	#region Public Methods

	#endregion Public Methods

	#region Non Public Methods

	#endregion Non Public Methods
}
#endif