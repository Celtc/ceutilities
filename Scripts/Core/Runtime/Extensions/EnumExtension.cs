﻿using UnityEngine;
using System;
using CEUtilities.Helpers;

public static class EnumExtension
{
    /// <summary>
    /// Checks if the value contains the provided type.
    /// </summary>
    public static bool HasFlag<T>(this Enum type, T value)
    {
        return type.HasAllFlags(value);
    }

    /// <summary>
    /// Checks if the value contains the provided type.
    /// </summary>
    public static bool HasAnyFlag<T>(this Enum type, T value)
    {
        try
        {
            return (((int)(object)type & (int)(object)value) != 0);
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Checks if the value contains the provided type.
    /// </summary>
    public static bool HasAllFlags<T>(this Enum type, T value)
    {
        try
        {
            return (((int)(object)type & (int)(object)value) == (int)(object)value);
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Checks if the value is only the provided type.
    /// </summary>
    public static bool Is<T>(this Enum type, T value)
    {
        try
        {
            return (int)(object)type == (int)(object)value;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Appends a value.
    /// </summary>
    public static T Add<T>(this Enum type, T value)
    {
        try
        {
            return (T)(object)(((int)(object)type | (int)(object)value));
        }
        catch (Exception ex)
        {
            throw new ArgumentException(string.Format("Could not append flag value {0} to enum {1}", value, typeof(T).Name), ex);
        }
    }

    /// <summary>
    /// Completely removes the value.
    /// </summary>
    public static T Remove<T>(this Enum type, T value)
    {
        try
        {
            return (T)(object)(((int)(object)type & ~(int)(object)value));
        }
        catch (Exception ex)
        {
            throw new ArgumentException(string.Format("Could not remove flag value {0} from enum {1}", value, typeof(T).Name), ex);
        }
    }

    /// <summary>
    /// Appends or removes a value.
    /// </summary>
    public static T Toggle<T>(this Enum type, T value)
    {
        if (type.HasAllFlags(value))
            return type.Remove(value);
        else
            return type.Add(value);
    }

    /// <summary>
    /// Gets an attribute on an enum field value
    /// </summary>
    /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
    /// <param name="enumVal">The enum value</param>
    /// <returns>The attribute of type T that exists on the enum value</returns>
    /// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
    public static bool HasAttribute<T>(this Enum enumVal) where T : Attribute
    {
        return EnumHelper.GetAttributeOfType<T>(enumVal) != null;
    }

    /// <summary>
    /// Gets an attribute on an enum field value
    /// </summary>
    /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
    /// <param name="enumVal">The enum value</param>
    /// <returns>The attribute of type T that exists on the enum value</returns>
    /// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
    public static T GetAttribute<T>(this Enum enumVal) where T : Attribute
    {
        return EnumHelper.GetAttributeOfType<T>(enumVal);
    }
}
