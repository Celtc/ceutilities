﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public delegate void SimpleEventHandler();
public delegate void SimpleEventHandler<T0>(T0 arg0);
public delegate void SimpleEventHandler<T0, T1>(T0 arg0, T1 arg1);
public delegate void SimpleEventHandler<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2);
public delegate void SimpleEventHandler<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3);
public delegate void SimpleEventHandler<T0, T1, T2, T3, T4>(T0 arg0, T1 arg1, T2 arg2, T3 arg3, T4 arg4);