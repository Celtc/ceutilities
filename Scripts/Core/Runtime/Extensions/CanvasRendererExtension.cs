﻿using UnityEngine;
using System.Collections;

public static class CanvasRendererExtension
{
    /// <summary>
    /// Fade out the canvas after a delay.
    /// </summary>
    public static IEnumerator FadeOut(this CanvasRenderer source, float duration, float delay)
    {
        yield return new WaitForSeconds(delay);

        yield return source.FadeOut(duration);
    }

    /// <summary>
    /// Fade out the canvas.
    /// </summary>
    public static IEnumerator FadeOut(this CanvasRenderer source, float duration)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        while (current - start < duration)
        {
            source.SetAlpha(1 - (current - start) / duration);
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        source.SetAlpha(0f);
    }


    /// <summary>
    /// Fade in the canvas after a delay.
    /// </summary>
    public static IEnumerator FadeIn(this CanvasRenderer source, float duration, float delay)
    {
        yield return new WaitForSeconds(delay);

        yield return source.FadeIn(duration);
    }

    /// <summary>
    /// Fade in the canvas.
    /// </summary>
    public static IEnumerator FadeIn(this CanvasRenderer source, float duration)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        while (current - start < duration)
        {
            source.SetAlpha((current - start) / duration);
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        source.SetAlpha(1f);
    }


    /// <summary>
    /// Fade the canvas using an animation curve.
    /// </summary>
    public static IEnumerator Fade(this CanvasRenderer source, float duration, AnimationCurve curve)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        while (current - start < duration)
        {
            source.SetAlpha(curve.Evaluate((current - start) / duration));
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        source.SetAlpha(curve.Evaluate(1f));
    }

    /// <summary>
    /// Fade the canvas using an animation curve after a delay.
    /// </summary>
    public static IEnumerator Fade(this CanvasRenderer source, float duration, float delay, AnimationCurve curve)
    {
        yield return new WaitForSeconds(delay);

        yield return source.Fade(duration, curve);
    }
}
