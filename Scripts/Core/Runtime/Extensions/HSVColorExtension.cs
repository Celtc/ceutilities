﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Colors;
using CEUtilities.Helpers;

public static class HSVColorExtension
{
    /// <summary>
    /// Lerps the specified start.
    /// </summary>
    /// <param name="from">The start.</param>
    /// <param name="to">The end.</param>
    /// <param name="amount">The amount.</param>
    /// <returns></returns>
    public static HSVColor LerpTo(this HSVColor from, HSVColor to, float amount)
    {
        return HSVColorHelper.Lerp(from, to, amount);
    }

    /// <summary>
    /// To the RGB.
    /// </summary>
    /// <param name="hsvColor">Color of the HSV.</param>
    /// <returns></returns>
    public static Color ToRGB(this HSVColor value)
    {
        return HSVColorHelper.ToRGB(value.Hue, value.Saturation, value.Value, value.Alpha);
    }

    /// <summary>
    /// To the hexadecimal string.
    /// </summary>
    /// <param name="includeAlpha">if set to <c>true</c> [include alpha].</param>
    /// <returns></returns>
    public static string ToHexString(this HSVColor value, bool includeAlpha = false)
    {
        return HSVColorHelper.ToHexString(value, includeAlpha);
    }

    /// <summary>
    /// To a vector3.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static Vector3 ToVector3(this HSVColor value)
    {
        return HSVColorHelper.ToVector3(value);
    }

    /// <summary>
    /// To the vector4.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns></returns>
    public static Vector4 ToVector4(this HSVColor value)
    {
        return HSVColorHelper.ToVector4(value);
    }
}