﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public static class IListExtension
{
    /// <summary>
    /// Returns the last element of a List.
    /// </summary>
    /// <typeparam name="T">Type of the list</typeparam>
    /// <param name="source">List containing elements</param>
    /// <returns>The last element of the list</returns>
    public static T GetLast<T>(this IList<T> source)
    {
        if (source == null || source.Count == 0)
        {
            throw new ArgumentException("IList is empty", "source");
        }
        return source[source.Count - 1];
    }

    /// <summary>
    /// Returns a random element of a List.
    /// </summary>
    /// <typeparam name="T">Type of the list</typeparam>
    /// <param name="source">List containing the element</param>
    /// <returns>A random element of the list</returns>
    public static T GetRandom<T>(this IList<T> source)
    {
        if (source == null || source.Count == 0)
        {
            throw new ArgumentException("IList is empty", "source");
        }
        return source[UnityEngine.Random.Range(0, source.Count)];
    }

    /// <summary>
    /// Gets the random index.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">The list.</param>
    /// <returns></returns>
    /// <exception cref="System.ArgumentException">List is empty - list</exception>
    public static int GetRandomIndex<T>(this IList<T> source)
    {
        if (source == null || source.Count == 0)
        {
            throw new ArgumentException("IList is empty", "source");
        }
        return UnityEngine.Random.Range(0, source.Count);
    }

    /// <summary>
    /// Modifies a list by shuffling it using the Fisher–Yates method.
    /// </summary>
    /// <typeparam name="T">Type of the list</typeparam>
    /// <param name="source">The list to be shuffled</param>
    public static void Shuffle<T>(this IList<T> source)
    {
        if (source == null)
        {
            throw new ArgumentException("IList is empty", "list");
        }

        for (int i = source.Count - 1; i > 0; i--)
        {
            int j = UnityEngine.Random.Range(0, i + 1);
            T temp = source[j];
            source[j] = source[i];
            source[i] = temp;
        }
    }

    /// <summary>
    /// Moves an element within the list.
    /// </summary>
    public static void Move<T>(this IList<T> source, int oldIndex, int newIndex)
    {
        T item = source[oldIndex];
        source.RemoveAt(oldIndex);
        source.Insert(newIndex, item);
    }

    /// <summary>
    /// Iterate through elements.
    /// </summary>
    public static void ForEach<T>(this IList<T> source, Action<T> callback)
    {
        for (int i = 0; i < source.Count; ++i)
        {
            callback(source[i]);
        }
    }

    /// <summary>
    /// Get the element at indexes positions.
    /// </summary>
    /// <typeparam name="T">Type of the list</typeparam>
    /// <param name="source">List containing the elements</param>
    /// <param name="indices">Indices to retrieve</param>
    /// <returns></returns>
    public static T[] GetAt<T>(this IList<T> source, params int[] indices)
    {
        var result = new T[indices.Length];
        for (int i = 0; i < indices.Length; i++)
        {
            result[i] = source[indices[i]];
        }
        return result;
    }
}