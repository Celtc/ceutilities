﻿using UnityEngine;
using System;
using System.Collections;

public static class Vector3IntExtension
{
    /// <summary>
    /// Sub Vector with X Y components.
    /// </summary>
    public static Vector2Int XY(this Vector3Int v)
    {
        return new Vector2Int(v.x, v.y);
    }

    /// <summary>
    /// Sub Vector with X Z components.
    /// </summary>
    public static Vector2Int XZ(this Vector3Int v)
    {
        return new Vector2Int(v.x, v.z);
    }

    /// <summary>
    /// Sub Vector with Y Z components.
    /// </summary>
    public static Vector2Int YZ(this Vector3Int v)
    {
        return new Vector2Int(v.y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with a new X field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="column">X value of the new vector</param>
    public static Vector3Int WithX(this Vector3Int v, int x)
    {
        return new Vector3Int(x, v.y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with a new Y field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="row">Y value of the new vector</param>
    public static Vector3Int WithY(this Vector3Int v, int y)
    {
        return new Vector3Int(v.x, y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with a new Z field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="z">Z value of the new vector</param>
    public static Vector3Int WithZ(this Vector3Int v, int z)
    {
        return new Vector3Int(v.x, v.y, z);
    }

    /// <summary>
    /// Returns a copy of a vector with the X field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the X field</param>
    /// <returns></returns>
    public static Vector3Int AddX(this Vector3Int v, int delta)
    {
        return new Vector3Int(v.x + delta, v.y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the Y field</param>
    /// <returns></returns>
    public static Vector3Int AddY(this Vector3Int v, int delta)
    {
        return new Vector3Int(v.x, v.y + delta, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with the Z field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the Z field</param>
    /// <returns></returns>
    public static Vector3Int AddZ(this Vector3Int v, int delta)
    {
        return new Vector3Int(v.x, v.y, v.z + delta);
    }


    /// <summary>
    /// Returns a copy of a vector with the X field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the X field</param>
    /// <returns></returns>
    public static Vector3Int MultiplyX(this Vector3Int v, float delta)
    {
        return new Vector3Int((int)(v.x * delta), v.y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the Y field</param>
    /// <returns></returns>
    public static Vector3Int MultiplyY(this Vector3Int v, float delta)
    {
        return new Vector3Int(v.x, (int)(v.y * delta), v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with the Z field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for Z field</param>
    /// <returns></returns>
    public static Vector3Int MultiplyZ(this Vector3Int v, float delta)
    {
        return new Vector3Int(v.x, v.y, (int)(v.z * delta));
    }

    /// <summary>
    /// Multiply components between themself (x*x, y*y, z*z).
    /// </summary>
    public static Vector3Int MultiplyCompWise(this Vector3Int a, Vector3Int b)
    {
        return new Vector3Int(a.x * b.x, a.y * b.y, a.z * b.z);
    }

    /// <summary>
    /// Returns a Vector2 from a Vector3 by removing the Z field.
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector2Int ToVector2Int(this Vector3Int v)
    {
        return new Vector2Int(v.x, v.y);
    }

    /// <summary>
    /// Create a Tuple from the Vector3.
    /// </summary>
    public static Tuple<int, int, int> ToTuple(this Vector3Int v)
    {
        return new Tuple<int, int, int>(v.x, v.y, v.z);
    }
}
