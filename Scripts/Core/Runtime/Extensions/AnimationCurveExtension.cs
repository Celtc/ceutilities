﻿using UnityEngine;
using System.Linq;

public static class AnimationCurveExtension
{
    /// <summary>
    /// Evaluate in a circular way, where tmax + 1 will be tmax.
    /// </summary>
    public static float EvaluateClamped(this AnimationCurve source, float time)
    {
        var minTime = source.keys.Min(x => x.time);
        var maxTime = source.keys.Max(x => x.time);
        return source.Evaluate(Mathf.Clamp(time, minTime, maxTime));
    }

    /// <summary>
    /// Evaluate in a circular way, where tmax + 1 will be t0.
    /// </summary>
    public static float CircularEvaluate(this AnimationCurve source, float time)
    {
        time %= 1;
        if (time < 0)
        {
            time += 1;
        }
        return source.Evaluate(time);
    }

    /// <summary>
    /// Returns the absolute duration of the curve from first to last key frame.
    /// </summary>
    /// <param name="curve">The animation curve to check duration of.</param>
    /// <returns>Returns 0 if the curve is null or has less than 1 frame, otherwise returns time difference between first and last frame.</returns>
    public static float Duration(this AnimationCurve curve)
    {
        if (curve == null || curve.length <= 1)
        {
            return 0f;
        }
        return Mathf.Abs(curve[curve.length - 1].time - curve[0].time);
    }

    /// <summary>
    /// Create a new animation curve with all the keys moved an offset.
    /// </summary>
	public static AnimationCurve SetOffset(this AnimationCurve source, float offset)
    {
        var length = source.keys.Length;
        var newKeys = new Keyframe[length];
        for (int i = 0; i < length; i++)
        {
            newKeys[i] = source.keys[i];
            newKeys[i].time += offset;
        }
        return new AnimationCurve(newKeys);
    }

    /// <summary>
    /// Create a new animation where all the keys' time is multiplied by the rate.
    /// </summary>
	public static AnimationCurve Extend(this AnimationCurve source, float rate)
    {
        var length = source.keys.Length;
        var newKeys = new Keyframe[length];
        for (int i = 0; i < length; i++)
        {
            newKeys[i] = source.keys[i];
            newKeys[i].time *= rate;
        }
        return new AnimationCurve(newKeys);
    }

    /// <summary>
    /// Gets the time where the curve has a certain value. Curve must not have two times t with the same value!
    /// </summary>
    /// <param name="value">Value to finde</param>
    /// <param name="accuracy">Accuracy of the search</param>
	public static float GetCurveTimeForValue(this AnimationCurve source, float value, int accuracy = 10)
    {
        float startTime = source.keys[0].time;
        float endTime = source.keys[source.length - 1].time;
        float nearestTime = startTime;
        float step = endTime - startTime;
        float curveSign = source.Evaluate(startTime) > source.Evaluate(endTime) ? -1f : 1f;

        for (int i = 0; i < accuracy; i++)
        {
            float valueAtNearestTime = source.Evaluate(nearestTime);
            float distanceToValueAtNearestTime = Mathf.Abs(value - valueAtNearestTime);

            float timeToCompare = nearestTime + step;
            float valueAtTimeToCompare = source.Evaluate(timeToCompare);
            float distanceToValueAtTimeToCompare = Mathf.Abs(value - valueAtTimeToCompare);

            if (distanceToValueAtTimeToCompare < distanceToValueAtNearestTime)
            {
                nearestTime = timeToCompare;
                valueAtNearestTime = valueAtTimeToCompare;
            }
            step = Mathf.Abs(step * 0.5f) * Mathf.Sign(value - valueAtNearestTime) * curveSign;
        }

        return nearestTime;
    }
}