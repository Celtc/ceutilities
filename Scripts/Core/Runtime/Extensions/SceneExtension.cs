﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

public static class SceneExtension
{
    #region Enum
    public enum LoadingState
    {
        NotLoaded,
        Loading,
        Loaded,
    }

    #endregion

    #region Exposed fields

    #endregion Exposed fields

    #region Internal fields

    private static PropertyInfo loadingStateProp;

    #endregion Internal fields

    #region Custom Events

    #endregion Custom Events

    #region Properties

    private static PropertyInfo LoadingStateProp
    {
        get
        {
            if (loadingStateProp == null)
                loadingStateProp = typeof(Scene).GetProperty("loadingState", BindingFlags.NonPublic | BindingFlags.Instance);

            return loadingStateProp;
        }
    }

    #endregion Properties

    #region Events methods

    #endregion Events methods

    #region Public Methods

    /// <summary>
    /// Gets a scene state
    /// </summary>
    public static LoadingState GetState(this Scene source)
    {
        return (LoadingState)LoadingStateProp.GetValue(source);
    }

    #endregion Public Methods

    #region Non Public Methods

    #endregion Non Public Methods
}