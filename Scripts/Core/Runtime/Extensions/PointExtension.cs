﻿using UnityEngine;

using CEUtilities.Structs;
using CEUtilities.Helpers;

public static class PointExtension
{
    /// <summary>
    /// Clamps the value of the point between the values.
    /// </summary>
    public static Point Clamp(this Point point, Point min, Point max)
    {
        return PointHelper.Clamp(point, min, max);
    }

    /// <summary>
    /// DOT operation.
    /// </summary>
    public static int Dot(this Point a, Point b)
    {
        return PointHelper.Dot(a, b);
    }
}