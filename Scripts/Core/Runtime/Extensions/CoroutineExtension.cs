﻿using UnityEngine;
using System;

public static class CoroutineExtension
{
    /// <summary>
    /// <para>Executes an action.</para>
    /// </summary>
    public sealed class Do : CustomYieldInstruction
    {
        private Action action;

        public override bool keepWaiting
        {
            get
            {
                action();
                return false;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Do"/> class.
        /// </summary>
        /// <param name="action">The action.</param>
        public Do(Action action)
        {
            this.action = action;
        }
    }
}