﻿using UnityEngine;
using System.Collections.Generic;

public static class ColliderExtension
{
    /// <summary>
    /// Is the point inside a convex collider?
    /// </summary>
    public static bool IsPointInside(this Collider test, Vector3 point)
    {
        Vector3 center, direction;
        Ray ray;
        bool hit;
        RaycastHit hitInfo;

        // Use collider bounds to get the center of the collider. May be inaccurate
        // for some colliders (i.e. MeshCollider with a 'plane' mesh)
        center = test.bounds.center;

        // Cast a ray from point to center
        direction = center - point;
        ray = new Ray(point, direction);
        hit = test.Raycast(ray, out hitInfo, direction.magnitude);

        // If we hit the collider, point is outside. So we return !hit
        return !hit;
    }

    /// <summary>
    /// Is the point inside a convex collider?
    /// </summary>
    public static bool IsPointInsideNonConvex(this Collider test, Vector3 point)
    {
        return test.IsPointInsideNonConvex(point, test.bounds.max + Vector3.forward * 100);
    }

    /// <summary>
    /// Is the point inside a convex collider?
    /// </summary>
    public static bool IsPointInsideNonConvex(this Collider test, Vector3 point, Vector3 referencePoint)
    {
        Vector3 Point;
        var Start = referencePoint; // This is defined to be some arbitrary point far away from the collider.
        var Goal = point; // This is the point we want to determine whether or not is inside or outside the collider.
        var Direction = (Goal - Start).normalized; // This is the direction from start to goal.
        int iterations = 0; // If we know how many times the raycast has hit faces on its way to the target and back, we can tell through logic whether or not it is inside.

        // Change layer of collider for improved cast
        int layer = test.gameObject.layer;
        test.gameObject.layer = 30;

        Point = Start;
        while (Point != Goal) // Try to reach the point starting from the far off point.  This will pass through faces to reach its objective.
        {
            RaycastHit hit;
            if (Physics.Linecast(Point, Goal, out hit, 1 << 30)) // Progressively move the point forward, stopping everytime we see a new plane in the way.
            {
                if (hit.collider == test)
                    iterations++;
                Point = hit.point + (Direction / 100.0f); // Move the Point to hit.point and push it forward just a touch to move it through the skin of the mesh (if you don't push it, it will read that same point indefinately).
            }
            else
            {
                Point = Goal; // If there is no obstruction to our goal, then we can reach it in one step.
            }
        }
        while (Point != Start) // Try to return to where we came from, this will make sure we see all the back faces too.
        {
            RaycastHit hit;
            if (Physics.Linecast(Point, Start, out hit, 1 << 30))
            {
                if (hit.collider == test)
                    iterations++;
                Point = hit.point + (-Direction / 100.0f);
            }
            else
            {
                Point = Start;
            }
        }

        // Restore the layer
        test.gameObject.layer = layer;

        return iterations % 2 == 1;
    }

    /// <summary>
    /// Is the sphere inside a convex collider? Sphere must be completely outisde to return false
    /// </summary>
    public static bool IsSphereInsideNonConvex(this Collider test, Vector3 center, float radius, Vector3 referencePoint)
    {
        Vector3 Point;
        Vector3 Start = referencePoint; // This is defined to be some arbitrary point far away from the collider.
        Vector3 Goal = center; // This is the point we want to determine whether or not is inside or outside the collider.
        Vector3 Direction = (Goal - Start).normalized; // This is the direction from start to goal.
        int iterations = 0; // If we know how many times the raycast has hit faces on its way to the target and back, we can tell through logic whether or not it is inside.

        // Change layer of collider for improved cast
        int layer = test.gameObject.layer;
        test.gameObject.layer = 30;

        Point = Start;
        while (Point != Goal) // Try to reach the point starting from the far off point.  This will pass through faces to reach its objective.
        {
            RaycastHit hit;
            var dst = (Goal - Point).magnitude;
            if (Physics.SphereCast(Point, radius, Direction, out hit, dst, 1 << 30))
            {
                if (hit.collider == test)
                    iterations++;
                Point = hit.point + (Direction * radius) + (Direction / 100.0f); // Move the Point to hit.point and push it forward just a touch to move it through the skin of the mesh (if you don't push it, it will read that same point indefinately).
            }
            else
            {
                Point = Goal; // If there is no obstruction to our goal, then we can reach it in one step.
            }
        }
        while (Point != Start) // Try to return to where we came from, this will make sure we see all the back faces too.
        {
            RaycastHit hit;
            var dst = (Start - Point).magnitude;
            if (Physics.SphereCast(Point, radius, -Direction, out hit, dst, 1 << 30))
            {
                if (hit.collider == test)
                    iterations++;
                Point = hit.point + (-Direction * radius) + (-Direction / 100.0f);
            }
            else
            {
                Point = Start;
            }
        }

        // Restore the layer
        test.gameObject.layer = layer;

        return iterations % 2 == 1;
    }
}
