﻿using UnityEngine;
using System.Collections;

public static class Vector4Extension
{
    /// <summary>
    /// Compares two Vector3 if they are similar.
    /// </summary>
    public static bool Approximately(Vector4 a, Vector4 b)
    {
        return Mathf.Approximately(a.x, b.x) &&
            Mathf.Approximately(a.y, b.y) &&
            Mathf.Approximately(a.z, b.z) &&
            Mathf.Approximately(a.w, b.w);
    }

    /// <summary>
    /// Returns a copy of a vector with a new X field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="column">X field of the new vector</param>
    public static Vector4 WithX(this Vector4 v, float x)
    {
        return new Vector4(x, v.y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with a new Y field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="row">Y field of the new vector</param>
    public static Vector4 WithY(this Vector4 v, float y)
    {
        return new Vector4(v.x, y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with a new Z field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="z">Z field of the new vector</param>
    public static Vector4 WithZ(this Vector4 v, float z)
    {
        return new Vector4(v.x, v.y, z);
    }

    /// <summary>
    /// Returns a copy of a vector with a new W field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="w">W field of the new vector</param>
    public static Vector4 WithW(this Vector4 v, float w)
    {
        return new Vector4(v.x, v.y, v.z, w);
    }

    /// <summary>
    /// Returns a copy of a vector with the X field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the X field</param>
    /// <returns></returns>
    public static Vector4 AddX(this Vector4 v, float delta)
    {
        return new Vector4(v.x + delta, v.y, v.z, v.w);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the Y field</param>
    /// <returns></returns>
    public static Vector4 AddY(this Vector4 v, float delta)
    {
        return new Vector4(v.x, v.y + delta, v.z, v.w);
    }

    /// <summary>
    /// Returns a copy of a vector with the Z field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the Z field</param>
    /// <returns></returns>
    public static Vector4 AddZ(this Vector4 v, float delta)
    {
        return new Vector4(v.x, v.y, v.z + delta, v.w);
    }

    /// <summary>
    /// Returns a copy of a vector with the W field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the W field</param>
    /// <returns></returns>
    public static Vector4 AddW(this Vector4 v, float delta)
    {
        return new Vector4(v.x, v.y, v.z, v.w + delta);
    }

    /// <summary>
    /// Returns a copy of a vector with the X field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the X field</param>
    /// <returns></returns>
    public static Vector4 MultiplyX(this Vector4 v, float delta)
    {
        return new Vector4(v.x * delta, v.y, v.z, v.w);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the Y field</param>
    /// <returns></returns>
    public static Vector4 MultiplyY(this Vector4 v, float delta)
    {
        return new Vector4(v.x, v.y * delta, v.z, v.w);
    }

    /// <summary>
    /// Returns a copy of a vector with the Z field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the Z field</param>
    /// <returns></returns>
    public static Vector4 MultiplyZ(this Vector4 v, float delta)
    {
        return new Vector4(v.x, v.y, v.z * delta, v.w);
    }

    /// <summary>
    /// Multiply components between themself (x*x, y*y, z*z, w*w).
    /// </summary>
    public static Vector4 MultiplyCompWise(this Vector4 a, Vector4 b)
    {
        return new Vector4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
    }

    /// <summary>
    /// Returns a copy of a vector with the W field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the W field</param>
    /// <returns></returns>
    public static Vector4 MultiplyW(this Vector4 v, float delta)
    {
        return new Vector4(v.x, v.y, v.z, v.w * delta);
    }

    /// <summary>
    /// Returns a Vector3 from a Vector2 by removing the W field.
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector3 ToVector3(this Vector4 v)
    {
        return new Vector3(v.x, v.y, v.z);
    }

    /// <summary>
    /// Returns a Vector3 from a Vector2 by removing the W field.
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector2 ToVector2(this Vector4 v)
    {
        return new Vector2(v.x, v.y);
    }
}
