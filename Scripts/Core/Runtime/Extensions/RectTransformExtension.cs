﻿using UnityEngine;
using System.Collections;

using CEUtilities.Structs;

public static class RectTransformExtension
{
    /// <summary>
    /// Get local edges.
    /// </summary>
    public static void GetLocalCorners(this RectTransform source, Vector3[] fourCornersArray)
    {
        // Verify params
        if (fourCornersArray.Length != 4)
        {
            Debug.LogError("[RectTransformExtension] Invalid parameters", source);
            return;
        }

        // Set corners
        fourCornersArray[0] = new Vector3(source.rect.min.x, source.rect.min.y);
        fourCornersArray[2] = new Vector3(source.rect.max.x, source.rect.max.y);
        fourCornersArray[1] = new Vector3(fourCornersArray[0].x, fourCornersArray[2].y);
        fourCornersArray[3] = new Vector3(fourCornersArray[2].x, fourCornersArray[0].y);

        return;
    }

    /// <summary>
    /// Get the distance between edges of rect transforms.
    /// </summary>
    /// <param name="targetEdge">Side to calculate the distance</param>
    /// <param name="targetRect">Against which rect's side will the distance be calc</param>
    /// <returns></returns>
    public static float DistanceEdgeToEdge(this RectTransform source, RectTransform targetRect, Edge sourceEdge, Edge targetEdge)
    {
        // No target?
        if (targetRect == null || targetRect == source)
        {
            Debug.LogError("[RectTransformExtension] Invalid parameters", source);
            return 0f;
        }

        // Verify params
        if (((int)sourceEdge + (int)targetEdge) % 2 != 0)
        {
            Debug.LogError("[RectTransformExtension] Invalid parameters", source);
            return 0f;
        }

        // Get source corners and change space
        var sourceCorners = new Vector3[4];
        source.GetWorldCorners(sourceCorners);
        for (int i = 0; i < 4; i++)
        {
            sourceCorners[i] = targetRect.InverseTransformPoint(sourceCorners[i]);
        }

        // Get target corners
        var targetCorners = new Vector3[4];
        targetRect.GetLocalCorners(targetCorners);

        // Get local edge pos
        var sourceEdgePos = 0f;
        switch (sourceEdge)
        {
            case Edge.Top: sourceEdgePos = sourceCorners[1].y; break;
            case Edge.Bottom: sourceEdgePos = sourceCorners[0].y; break;
            case Edge.Right: sourceEdgePos = sourceCorners[2].x; break;
            case Edge.Left: sourceEdgePos = sourceCorners[0].x; break;
        }

        // Get target edge pos
        float targetEdgePos = 0f;
        switch (targetEdge)
        {
            case Edge.Top: targetEdgePos = targetCorners[1].y; break;
            case Edge.Bottom: targetEdgePos = targetCorners[0].y; break;
            case Edge.Right: targetEdgePos = targetCorners[2].x; break;
            case Edge.Left: targetEdgePos = targetCorners[0].x; break;
        }

        return targetEdgePos - sourceEdgePos;
    }


    /// <summary>
    /// Tween the scale to a value.
    /// </summary>
    public static IEnumerator TweenSize(this RectTransform target, Vector2 size, float duration, AnimationCurve curve = null)
    {
        if (curve == null)
        {
            curve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }

        var deltaTime = 0f;
        var timer = Time.realtimeSinceStartup;

        var start = target.sizeDelta;
        var deltaScale = size - start;
        while (deltaTime < duration)
        {
            var deltaCurrent = deltaScale * curve.Evaluate(deltaTime / duration);
            target.sizeDelta = start + deltaCurrent;
            deltaTime = Time.realtimeSinceStartup - timer;
            yield return null;
        }

        // Set final value
        target.sizeDelta = size;
    }

    /// <summary>
    /// Tween the scale to a value.
    /// </summary>
    public static IEnumerator TweenSizeAtSpeed(this RectTransform target, Vector2 size, float speed, MonoBehaviour caller, AnimationCurve curve = null)
    {
        var delta = target.sizeDelta - size;
        if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
        {
            caller.StartCoroutine(target.TweenSizeVerticalAtSpeed(size.y, speed, curve));
            yield return target.TweenSizeHorizontalAtSpeed(size.x, speed, curve);
        }
        else
        {
            caller.StartCoroutine(target.TweenSizeHorizontalAtSpeed(size.x, speed, curve));
            yield return target.TweenSizeVerticalAtSpeed(size.y, speed, curve);
        }
    }

    /// <summary>
    /// Tween the scale to a value.
    /// </summary>
    public static IEnumerator TweenSizeHorizontalAtSpeed(this RectTransform target, float size, float speed, AnimationCurve curve = null)
    {
        if (curve == null)
        {
            curve = AnimationCurve.Linear(0f, 1f, 1f, 1f);
        }

        var start = target.sizeDelta.x;
        var deltaScale = size - start;
        var ratioUnit = Mathf.Abs(1 / deltaScale);

        var ratio = 0f;
        while (ratio <= 1f)
        {
            // Translate in XY
            var currSpeed = curve.Evaluate(ratio) * speed;
            ratio += currSpeed * ratioUnit * Time.deltaTime;
            target.sizeDelta = new Vector2(Mathf.Lerp(start, size, ratio), target.sizeDelta.y);

            yield return null;
        }

        // Set final value
        target.sizeDelta = new Vector2(size, target.sizeDelta.y);
    }

    /// <summary>
    /// Tween the scale to a value.
    /// </summary>
    public static IEnumerator TweenSizeVerticalAtSpeed(this RectTransform target, float size, float speed, AnimationCurve curve = null)
    {
        if (curve == null)
        {
            curve = AnimationCurve.Linear(0f, 1f, 1f, 1f);
        }

        var start = target.sizeDelta.y;
        var deltaScale = size - start;
        var ratioUnit = Mathf.Abs(1 / deltaScale);

        var ratio = 0f;
        while (ratio <= 1f)
        {
            // Translate in XY
            var currSpeed = curve.Evaluate(ratio) * speed;
            ratio += currSpeed * ratioUnit * Time.deltaTime;
            target.sizeDelta = new Vector2(target.sizeDelta.x, Mathf.Lerp(start, size, ratio));

            yield return null;
        }

        // Set final value
        target.sizeDelta = new Vector2(target.sizeDelta.x, size);
    }


    /// <summary>
    /// Animate an element toward a direction.
    /// </summary>
    public static IEnumerator TweenPosition(this RectTransform target, Vector2 anchoredEnd, float duration, AnimationCurve curve = null)
    {
        yield return target.TweenPosition(target.anchoredPosition, anchoredEnd, duration, curve);
    }

    /// <summary>
    /// Animate an element toward a direction.
    /// </summary>
    public static IEnumerator TweenPosition(this RectTransform target, Vector2 anchoredStart, Vector2 anchoredEnd, float duration, AnimationCurve curve = null)
    {
        if (curve == null)
        {
            curve = AnimationCurve.Linear(0f, 1f, 1f, 1f);
        }

        var deltaTime = 0f;
        var timer = Time.realtimeSinceStartup;
        var deltaPos = anchoredEnd - anchoredStart;
        while (deltaTime < duration)
        {
            var deltaCurrent = deltaPos * curve.Evaluate(deltaTime / duration);
            target.anchoredPosition = anchoredStart + deltaCurrent;
            deltaTime = Time.realtimeSinceStartup - timer;
            yield return null;
        }

        // Set final value
        target.anchoredPosition = anchoredEnd;
    }

    /// <summary>
    /// Animate the rect outside of its main (root) canvas.
    /// </summary>
    /// <param name="towardEdge">Which side will be going towards</param>
    /// <param name="duration">Duration of the animation</param>
    /// <param name="curve">Curve used to animate the transition</param>
    public static IEnumerator TweenPositionOutside(this RectTransform sourceRect, Edge towardEdge, float duration, AnimationCurve curve = null)
    {
        var rootCanvas = sourceRect.GetComponentInParent<Canvas>().rootCanvas;
        var rootRectT = rootCanvas.GetComponent<RectTransform>();
        yield return sourceRect.TweenPositionOutsideRect(rootRectT, towardEdge, duration, curve);
    }

    /// <summary>
    /// Animate the rect transform outside other rect transform.
    /// </summary>
    /// <param name="targetRect">Rect transform we want to get outside off</param>
    /// <param name="towardEdge">Which side will be going towards</param>
    /// <param name="duration">Duration of the animation</param>
    /// <param name="curve">Curve used to animate the transition</param>
    /// <returns></returns>
    public static IEnumerator TweenPositionOutsideRect(this RectTransform sourceRect, RectTransform targetRect, Edge towardEdge, float duration, AnimationCurve curve = null)
    {
        Edge sourceEdge = default(Edge);
        switch (towardEdge)
        {
            case Edge.Bottom: sourceEdge = Edge.Top; break;
            case Edge.Top: sourceEdge = Edge.Bottom; break;
            case Edge.Left: sourceEdge = Edge.Right; break;
            case Edge.Right: sourceEdge = Edge.Left; break;
        }
        var distance = sourceRect.DistanceEdgeToEdge(targetRect, sourceEdge, towardEdge);

        Vector2 targetAnchoredPosition = sourceRect.anchoredPosition;
        if ((int)towardEdge % 2 == 0)
        {
            targetAnchoredPosition.x += distance;
        }
        else
        {
            targetAnchoredPosition.y += distance;
        }

        yield return sourceRect.TweenPosition(sourceRect.anchoredPosition, targetAnchoredPosition, duration, curve);
    }

    /// <summary>
    /// Animate local edge to another rect edge.
    /// </summary>
    public static IEnumerator TweenEdgeToRootCanvasEdge(this RectTransform sourceRect, Edge sourceEdge, Edge targetEdge, float duration, float offset = 0f, AnimationCurve curve = null)
    {
        var rootCanvas = sourceRect.GetComponentInParent<Canvas>().rootCanvas;
        var rootRectT = rootCanvas.GetComponent<RectTransform>();
        yield return sourceRect.TweenEdgeToEdge(rootRectT, sourceEdge, targetEdge, duration, offset, curve);
    }

    /// <summary>
    /// Animate local edge to another rect edge.
    /// </summary>
    public static IEnumerator TweenEdgeToEdge(this RectTransform sourceRect, RectTransform targetRect, Edge sourceEdge, Edge targetEdge, float duration, float offset = 0f, AnimationCurve curve = null)
    {
        // Verify params
        if (((int)sourceEdge + (int)targetEdge) % 2 != 0)
        {
            Debug.LogError("[RectTransformExtension] Invalid parameters", sourceRect);
            yield break;
        }

        var distance = sourceRect.DistanceEdgeToEdge(targetRect, sourceEdge, targetEdge);
        if (offset != 0f)
        {
            distance += offset;
        }

        Vector2 targetAnchoredPosition = sourceRect.anchoredPosition;
        if ((int)targetEdge % 2 == 0)
        {
            targetAnchoredPosition.x += distance;
        }
        else
        {
            targetAnchoredPosition.y += distance;
        }

        yield return sourceRect.TweenPosition(sourceRect.anchoredPosition, targetAnchoredPosition, duration, curve);
    }


    /// <summary>
    /// Move local edge to another rect edge.
    /// </summary>
    public static void MoveEdgeToRootCanvasEdge(this RectTransform sourceRect, Edge sourceEdge, Edge targetEdge, float offset = 0f)
    {
        var rootCanvas = sourceRect.GetComponentInParent<Canvas>().rootCanvas;
        var rootRectT = rootCanvas.GetComponent<RectTransform>();
        sourceRect.MoveEdgeToEdge(rootRectT, sourceEdge, targetEdge, offset);
    }

    /// <summary>
    /// Move the rect transform's edge next to another rect transform's edge.
    /// </summary>
    public static void MoveEdgeToEdge(this RectTransform sourceRect, RectTransform targetRect, Edge sourceEdge, Edge targetEdge, float offset = 0f)
    {
        // Verify params
        if (((int)sourceEdge + (int)targetEdge) % 2 != 0)
        {
            Debug.LogError("[RectTransformExtension] Invalid parameters", sourceRect);
            return;
        }

        var distance = sourceRect.DistanceEdgeToEdge(targetRect, sourceEdge, targetEdge);
        if (offset != 0f)
        {
            distance += offset;
        }


        Vector2 targetAnchoredPosition = sourceRect.anchoredPosition;
        if ((int)targetEdge % 2 == 0)
        {
            targetAnchoredPosition.x += distance;
        }
        else
        {
            targetAnchoredPosition.y += distance;
        }

        sourceRect.anchoredPosition = targetAnchoredPosition;
    }


    /// <summary>
    /// Flip rect transform around the axis.
    /// </summary>
    /// <param name="sourceRect">The RectTransform to flip.</param>
    /// <param name="axis">The axis to flip along. 0 is horizontal and 1 is vertical.</param>
    /// <param name="keepPositioning">Flips around the pivot if true. Flips within the parent rect if false.</param>
    /// <param name="recursive">Flip the children as well?</param>
    public static void FlipAroundAxis(this RectTransform sourceRect, int axis, bool keepPositioning, bool recursive)
    {
        RectTransformUtility.FlipLayoutOnAxis(sourceRect, axis, keepPositioning, recursive);
    }

    /// <summary>
    /// Utility method to change the local scale.
    /// </summary>
    /// <param name="localScale">New local scale</param>
    /// <param name="mask">Bitmask to define which axis to change</param>
    public static void SetLocalScale(this RectTransform sourceRect, Vector3 localScale, int mask = 7)
    {
        var temp = sourceRect.localScale;
        if ((mask & 1 << 2) != 0) temp.x = localScale.x;
        if ((mask & 1 << 1) != 0) temp.y = localScale.y;
        if ((mask & 1 << 0) != 0) temp.z = localScale.z;
        sourceRect.localScale = temp;
    }
}