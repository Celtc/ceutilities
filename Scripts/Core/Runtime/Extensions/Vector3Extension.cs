﻿using UnityEngine;
using System;
using System.Collections;

public static class Vector3Extension
{
    /// <summary>
    /// Compares two Vector3 if they are similar.
    /// </summary>
    public static bool Approximately(Vector3 a, Vector3 b)
    {
        return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y) && Mathf.Approximately(a.z, b.z);
    }


    /// <summary>
    /// Sub Vector with X Y components.
    /// </summary>
    public static Vector2 XY(this Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }

    /// <summary>
    /// Sub Vector with X Z components.
    /// </summary>
    public static Vector2 XZ(this Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }

    /// <summary>
    /// Sub Vector with Y Z components.
    /// </summary>
    public static Vector2 YZ(this Vector3 v)
    {
        return new Vector2(v.y, v.z);
    }


    /// <summary>
    /// Returns a copy of a vector with a new X field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="column">X value of the new vector</param>
    public static Vector3 WithX(this Vector3 v, float x)
    {
        return new Vector3(x, v.y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with a new Y field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="row">Y value of the new vector</param>
    public static Vector3 WithY(this Vector3 v, float y)
    {
        return new Vector3(v.x, y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with a new Z field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="z">Z value of the new vector</param>
    public static Vector3 WithZ(this Vector3 v, float z)
    {
        return new Vector3(v.x, v.y, z);
    }


    /// <summary>
    /// Returns a copy of a vector with the X field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the X field</param>
    /// <returns></returns>
    public static Vector3 AddX(this Vector3 v, float delta)
    {
        return new Vector3(v.x + delta, v.y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the Y field</param>
    /// <returns></returns>
    public static Vector3 AddY(this Vector3 v, float delta)
    {
        return new Vector3(v.x, v.y + delta, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with the Z field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the Z field</param>
    /// <returns></returns>
    public static Vector3 AddZ(this Vector3 v, float delta)
    {
        return new Vector3(v.x, v.y, v.z + delta);
    }


    /// <summary>
    /// Increase or decrease the length of vector by size.
    /// </summary>
    public static Vector3 AddLength(this Vector3 vector, float size)
    {
        // Get the vector length
        float magnitude = Vector3.Magnitude(vector);

        // Calculate new vector length
        float newMagnitude = magnitude + size;

        // Calculate the ratio of the new length to the old length
        float scale = newMagnitude / magnitude;

        // Scale the vector
        return vector * scale;
    }

    /// <summary>
    /// Create a vector of direction "vector" with length "size".
    /// </summary>
    public static Vector3 SetLength(this Vector3 vector, float size)
    {
        // Normalize the vector
        Vector3 vectorNormalized = Vector3.Normalize(vector);

        // Scale the vector
        return vectorNormalized *= size;
    }


    /// <summary>
    /// Returns a copy of a vector with the X field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the X field</param>
    /// <returns></returns>
    public static Vector3 MultiplyX(this Vector3 v, float delta)
    {
        return new Vector3(v.x * delta, v.y, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the Y field</param>
    /// <returns></returns>
    public static Vector3 MultiplyY(this Vector3 v, float delta)
    {
        return new Vector3(v.x, v.y * delta, v.z);
    }

    /// <summary>
    /// Returns a copy of a vector with the Z field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for Z field</param>
    /// <returns></returns>
    public static Vector3 MultiplyZ(this Vector3 v, float delta)
    {
        return new Vector3(v.x, v.y, v.z * delta);
    }

    /// <summary>
    /// Multiply components between themself (x*x, y*y, z*z).
    /// </summary>
    public static Vector3 MultiplyCompWise(this Vector3 a, Vector3 b)
    {
        return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
    }


    /// <summary>
    /// Rotate the vector around origin.
    /// </summary>
    public static Vector3 Rotate(this Vector3 a, Quaternion angle)
    {
        return RotateAroundPoint(a, Vector3.zero, angle);
    }

    /// <summary>
    /// Rotate the vector around another point.
    /// </summary>
    public static Vector3 RotateAroundPoint(this Vector3 a, Vector3 pivot, Quaternion angle)
    {
        return angle * (a - pivot) + pivot;
    }


    /// <summary>
    /// Returns a Vector2Int from a Vector3.
    /// </summary>
    public static Vector2Int ToVector2Int(this Vector3 v)
    {
        return new Vector2Int((int)v.x, (int)v.y);
    }

    /// <summary>
    /// Returns a Vector3Int from a Vector3.
    /// </summary>
    public static Vector3Int ToVector3Int(this Vector3 v)
    {
        return new Vector3Int((int)v.x, (int)v.y, (int)v.z);
    }

    /// <summary>
    /// Returns a Vector2 from a Vector3 by removing the Z field.
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector2 ToVector2(this Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }

    /// <summary>
    /// Returns a Vector4 from a Vector3 by adding a W field.
    /// </summary>
    /// <param name="v"></param>
    /// <param name="w">Optional 'w' parameter</param>
    /// <returns></returns>
    public static Vector4 ToVector4(this Vector3 v, float w = 0f)
    {
        return new Vector4(v.x, v.y, v.z, w);
    }

    /// <summary>
    /// Create a Tuple from the Vector3.
    /// </summary>
    public static Tuple<float, float, float> ToTuple(this Vector3 v)
    {
        return new Tuple<float, float, float>(v.x, v.y, v.z);
    }
}
