﻿using UnityEngine;
using System;
using System.Collections;

public static class Vector2Extension
{
    /// <summary>
    /// Compares two Vector2 if they are similar.
    /// </summary>
    public static bool Approximately(Vector2 a, Vector2 b)
    {
        return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y);
    }

    /// <summary>
    /// Return a perpendicular vector (90 degrees rotation).
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector2 Perpendicular(this Vector2 v)
    {
        return new Vector2(-v.y, v.x);
    }

    /// <summary>
    /// Return a perpendicular vector (-90 degrees rotation).
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector2 PerpendicularRight(this Vector2 v)
    {
        return new Vector2(v.y, -v.x);
    }


    /// <summary>
    /// Returns a copy of a vector with a new X field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="column">X field of the new vector</param>
    /// <returns></returns>
    public static Vector2 WithX(this Vector2 v, float x)
    {
        return new Vector2(x, v.y);
    }

    /// <summary>
    /// Returns a copy of a vector with a new Y field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="row">Y field of the new vector</param>
    public static Vector2 WithY(this Vector2 v, float y)
    {
        return new Vector2(v.x, y);
    }

    /// <summary>
    /// Returns a copy of a vector with the X field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the X field</param>
    /// <returns></returns>
    public static Vector2 AddX(this Vector2 v, float delta)
    {
        return new Vector2(v.x + delta, v.y);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the Y field</param>
    /// <returns></returns>
    public static Vector2 AddY(this Vector2 v, float delta)
    {
        return new Vector2(v.x, v.y + delta);
    }

    /// <summary>
    /// Returns a copy of a vector with the X field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the X field</param>
    /// <returns></returns>
    public static Vector2 MultiplyX(this Vector2 v, float delta)
    {
        return new Vector2(v.x * delta, v.y);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor forthe Y field</param>
    /// <returns></returns>
    public static Vector2 MultiplyY(this Vector2 v, float delta)
    {
        return new Vector2(v.x, v.y * delta);
    }

    /// <summary>
    /// Multiply components between themself (x*x, y*y).
    /// </summary>
    public static Vector2 MultiplyCompWise(this Vector2 a, Vector2 b)
    {
        return new Vector2(a.x * b.x, a.y * b.y);
    }


    /// <summary>
    /// Returns a Vector2Int from a Vector2.
    /// </summary>
    public static Vector2Int ToVector2Int(this Vector2 v)
    {
        return new Vector2Int((int)v.x, (int)v.y);
    }

    /// <summary>
    /// Returns a Vector3Int from a Vector3.
    /// </summary>
    public static Vector3Int ToVector3Int(this Vector2 v, int z = 0)
    {
        return new Vector3Int((int)v.x, (int)v.y, z);
    }

    /// <summary>
    /// Returns a Vector3 from a Vector2 by adding a Z field.
    /// </summary>
    /// <param name="v"></param>
    /// <param name="z">Optional 'z' parameter</param>
    /// <returns></returns>
    public static Vector3 ToVector3(this Vector2 v, float z = 0f)
    {
        return new Vector3(v.x, v.y, z);
    }

    /// <summary>
    /// Returns a Vector4 from a Vector2 by adding a Z & W field.
    /// </summary>
    /// <param name="v"></param>
    /// <returns></returns>
    public static Vector4 ToVector4(this Vector2 v)
    {
        return new Vector4(v.x, v.y, 0f, 0f);
    }

    /// <summary>
    /// Returns a Vector4 from a Vector2 by adding a Z & W field.
    /// </summary>
    /// <param name="v"></param>
    /// <param name="z">Optional 'z' parameter</param>
    /// <param name="w">Optional 'w' parameter</param>
    /// <returns></returns>
    public static Vector4 ToVector4(this Vector2 v, float z, float w = 0f)
    {
        return new Vector4(v.x, v.y, z, w);
    }

    /// <summary>
    /// Create a Tuple from the Vector2.
    /// </summary>
    public static Tuple<float, float> ToTuple(this Vector2 v)
    {
        return new Tuple<float, float>(v.x, v.y);
    }
}
