﻿using UnityEngine;
using System.Text;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;

public static class MaterialExtension
{
    /// <summary>
    /// Generate an OBJ compatible string.
    /// </summary>
    /// <param name="mat">Source material</param>
    /// <param name="includeTextures">Should add textures refrences</param>
    /// <param name="materialOBJ">Output string</param>
    /// <param name="textures">Textures referenced</param>
    public static void ToOBJ(this Material mat, out string materialOBJ, out Texture[] textures)
    {
        mat.ToOBJ(out materialOBJ, out textures);
    }

    /// <summary>
    /// Generate an OBJ compatible string.
    /// </summary>
    /// <param name="mat">Source material</param>
    /// <param name="includeTextures">Should add textures refrences</param>
    /// <param name="materialOBJ">Output string</param>
    /// <param name="textures">Textures referenced</param>
    public static void ToOBJ(this Material mat, bool includeTextures, out string materialOBJ, out Texture[] textures)
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("newmtl " + mat.name);

        // Color property
        if (mat.HasProperty("_Color"))
        {
            sb.AppendLine
            (
                "Kd " + mat.color.r.ToString(CultureInfo.InvariantCulture)
                + " " + mat.color.g.ToString(CultureInfo.InvariantCulture)
                + " " + mat.color.b.ToString(CultureInfo.InvariantCulture)
            );

            // Transparency (both techniques)
            if (mat.color.a < 1.0f)
            {
                sb.AppendLine("Tr " + (1f - mat.color.a).ToString(CultureInfo.InvariantCulture));
                sb.AppendLine("d " + mat.color.a.ToString(CultureInfo.InvariantCulture));
            }
        }

        // Specular property
        if (mat.HasProperty("_SpecColor"))
        {
            var specColor = mat.GetColor("_SpecColor");
            sb.AppendLine
            (
                "Ks " + specColor.r.ToString(CultureInfo.InvariantCulture)
                + " " + specColor.g.ToString(CultureInfo.InvariantCulture)
                + " " + specColor.b.ToString(CultureInfo.InvariantCulture)
            );
        }

        // Textures
        if (includeTextures)
        {
            var texturesSet = new HashSet<Texture>();

            // Diffuse map
            if (mat.HasProperty("_MainTex"))
            {
                var tex = mat.GetTexture("_MainTex");
                if (tex != null)
                {
                    texturesSet.Add(tex);
                    sb.AppendLine("map_Kd .\\" + tex.name + ".png");
                }
            }

            // Spec map
            if (mat.HasProperty("_SpecMap"))
            {
                var tex = mat.GetTexture("_SpecMap");
                if (tex != null)
                {
                    texturesSet.Add(tex);
                    sb.AppendLine("map_Ks .\\" + tex.name + ".png");
                }
            }

            // Bump map
            if (mat.HasProperty("_BumpMap"))
            {
                var tex = mat.GetTexture("_BumpMap");
                var bumpiness = mat.HasProperty("_BumpScale") ? mat.GetFloat("_BumpScale") : 1f;
                if (tex != null)
                {
                    texturesSet.Add(tex);
                    sb.AppendLine("map_Bump -bm " + bumpiness.ToString(CultureInfo.InvariantCulture) + " .\\" + tex.name + ".png");
                }
            }

            // Add textures
            textures = texturesSet.ToArray();
        }
        else
        {
            textures = null;
        }

        sb.AppendLine("illum 2");

        materialOBJ = sb.ToString();
    }
}