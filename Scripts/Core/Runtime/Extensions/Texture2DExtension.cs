﻿using UnityEngine;
using System.IO;

using CEUtilities.Helpers;
using CEUtilities.Structs;

public static class Texture2DExtension
{
    /// <summary>
    /// Verify if the texture is readable.
    /// </summary>
    public static bool IsReadable(this Texture2D tex)
    {
        return TextureHelper.CheckReadable(tex);
    }

    /// <summary>
    /// Verify if the texture is readable.
    /// </summary>
    public static bool IsReadable(this Texture tex)
    {
        return TextureHelper.CheckReadable((Texture2D)tex);
    }

    /// <summary>
    /// Is the texture in a compressed format?
    /// </summary>
    public static bool IsCompressed(this Texture2D tex)
    {
        var format = tex.format;
        return !(format == TextureFormat.R8
            || format == TextureFormat.RG16
            || format == TextureFormat.RGB24
            || format == TextureFormat.RGBA32
            || format == TextureFormat.ARGB32
            || format == TextureFormat.ARGB4444
            || format == TextureFormat.Alpha8);
    }

    /// <summary>
    /// Export a texture2d.
    /// </summary>
    public static bool ExportAsPNG(this Texture2D tex, string folderpath, bool replace)
    {
        // Get readable texture
        Texture2D targetTex;
        if (tex.IsReadable() && !tex.IsCompressed())
            targetTex = tex;
        else
            targetTex = tex.Duplicate(TextureFormat.RGBA32, true);

        // Encode and save
        var result = false;
        try
        {
            var path = Path.Combine(folderpath, tex.name + ".png");
            if (File.Exists(path))
                File.Delete(path);
            File.WriteAllBytes(path, targetTex.EncodeToPNG());
            result = true;
        }
        catch { }

        return result;
    }


    /// <summary>
    /// Duplicate a texture2d.
    /// </summary>
    public static Texture2D Duplicate(this Texture2D source)
    {
        return Duplicate(source, false);
    }

    /// <summary>
    /// Duplicate a texture2d.
    /// </summary>
    public static Texture2D Duplicate(this Texture2D source, bool readable)
    {
        var newTex = TextureHelper.CreateEmptyTexture(source);
        var rawData = source.GetRawTextureData();
        newTex.LoadRawTextureData(rawData);
        newTex.Apply(false, !readable);
        return newTex;
    }

    /// <summary>
    /// Duplicate a texture2d.
    /// </summary>
    public static Texture2D Duplicate(this Texture2D source, TextureFormat format)
    {
        return Duplicate(source, format);
    }

    /// <summary>
    /// Duplicate a texture2d.
    /// </summary>
    public static Texture2D Duplicate(this Texture2D source, TextureFormat format, bool readable)
    {
        Texture2D midTex;
        if (!source.IsReadable())
        {
            midTex = new Texture2D(source.width, source.height, source.format, source.mipmapCount > 1);
            var rawData = source.GetRawTextureData();
            midTex.LoadRawTextureData(rawData);
        }
        else
        {
            midTex = source;
        }

        var finalTex = TextureHelper.CreateEmptyTexture(source, format);
        int blockWidth, blockHeight;
        Color32[] pixels;
        for (int i = 0; i < finalTex.mipmapCount; i++)
        {
            pixels = midTex.GetPixels32(i);
            blockWidth = Mathf.Max(1, midTex.width >> i);
            blockHeight = Mathf.Max(1, midTex.height >> i);
            finalTex.SetPixels32(0, 0, blockWidth, blockHeight, pixels, i);
        }

        finalTex.Apply(false, !readable);

        return finalTex;
    }


    /// <summary>
    /// Replace this texture from a one created using a file.
    /// </summary>
    public static void Import(this Texture2D tex, string filePath)
    {
        tex = TextureHelper.ImportTexture(filePath, TextureFormat.ARGB32, true, FilterMode.Bilinear, false);
        tex.Apply();
    }

    /// <summary>
    /// Replace this texture from a one created using a file.
    /// </summary>
    public static void Import(this Texture2D tex, string filePath, TextureFormat format)
    {
        tex = TextureHelper.ImportTexture(filePath, format, true, FilterMode.Bilinear, false);
        tex.Apply();
    }

    /// <summary>
    /// Replace this texture from a one created using a file.
    /// </summary>
    public static void Import(this Texture2D tex, string filePath, TextureFormat format, bool mipmap)
    {
        tex = TextureHelper.ImportTexture(filePath, format, mipmap, FilterMode.Bilinear, false);
        tex.Apply();
    }

    /// <summary>
    /// Replace this texture from a one created using a file.
    /// </summary>
    public static void Import(this Texture2D tex, string filePath, TextureFormat format, bool mipmap, FilterMode filterMode)
    {
        tex = TextureHelper.ImportTexture(filePath, format, mipmap, filterMode, false);
        tex.Apply();
    }

    /// <summary>
    /// Replace this texture from a one created using a file.
    /// </summary>
    public static void Import(this Texture2D tex, string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable)
    {
        tex = TextureHelper.ImportTexture(filePath, format, mipmap, filterMode, readable);
        tex.Apply();
    }


    /// <summary>
    /// Use a relative UV coord to retrieve a pixel color.
    /// </summary>
    public static Color GetPixelUV(this Texture2D tex, float x, float y)
    {
        return tex.GetPixel((int)(tex.width * x), (int)(tex.height * y));
    }

    /// <summary>
    /// Use a relative UV coord to retrieve a pixel color.
    /// </summary>
    public static Color GetPixelUV(this Texture2D tex, Vector2 uv)
    {
        return tex.GetPixel((int)(tex.width * uv.x), (int)(tex.height * uv.y));
    }


    /// <summary>
    /// Add a watermark to an image.
    /// </summary>
    public static Texture2D AddWatermark(this Texture2D background, Texture2D watermark, float alpha)
    {
        return TextureHelper.AddWatermark(background, watermark, alpha, Margin.zero, Pivot.BottomLeft);
    }

    /// <summary>
    /// Add a watermark to an image.
    /// </summary>
    public static Texture2D AddWatermark(this Texture2D background, Texture2D watermark, float alpha, RectOffset margins)
    {
        return TextureHelper.AddWatermark(background, watermark, alpha, new Margin(margins), Pivot.BottomLeft);
    }

    /// <summary>
    /// Add a watermark to an image.
    /// </summary>
    public static Texture2D AddWatermark(this Texture2D background, Texture2D watermark, float alpha, RectOffset margins, Pivot pivot)
    {
        return TextureHelper.AddWatermark(background, watermark, alpha, new Margin(margins), pivot);
    }

    /// <summary>
    /// Add a watermark to an image.
    /// </summary>
    public static Texture2D AddWatermark(this Texture2D background, Texture2D watermark, float alpha, Margin margins)
    {
        return TextureHelper.AddWatermark(background, watermark, alpha, margins, Pivot.BottomLeft);
    }

    /// <summary>
    /// Add a watermark to an image.
    /// </summary>
    public static Texture2D AddWatermark(this Texture2D background, Texture2D watermark, float alpha, Margin margins, Pivot pivot)
    {
        return TextureHelper.AddWatermark(background, watermark, alpha, margins, pivot);
    }


    /// <summary>
    /// Do a point scale of a texture.
    /// </summary>
    /// <param name="tex">Texture to scale</param>
    /// <param name="width">New width of the texture</param>
    /// <param name="height">New height of the texture</param>
    public static void ScalePoint(this Texture2D tex, int width, int height)
    {
        TextureHelper.ScalePoint(tex, width, height);
    }

    /// <summary>
    /// Do a point scale of a texture.
    /// </summary>
    /// <param name="tex">Texture to scale</param>
    /// <param name="width">New width of the texture</param>
    /// <param name="height">New height of the texture</param>
    public static void ScalePoint(this Texture2D tex, int width, int height, bool preserveAR)
    {
        TextureHelper.ScalePoint(tex, width, height, preserveAR);
    }

    /// <summary>
    /// Do a bilinear scale of a texture.
    /// </summary>
    /// <param name="tex">Texture to scale</param>
    /// <param name="width">New width of the texture</param>
    /// <param name="height">New height of the texture</param>
    public static void ScaleBilinear(this Texture2D tex, int width, int height)
    {
        TextureHelper.ScaleBilinear(tex, width, height);
    }

    /// <summary>
    /// Do a bilinear scale of a texture.
    /// </summary>
    /// <param name="tex">Texture to scale</param>
    /// <param name="width">New width of the texture</param>
    /// <param name="height">New height of the texture</param>
    public static void ScaleBilinear(this Texture2D tex, int width, int height, bool preserveAR)
    {
        TextureHelper.ScaleBilinear(tex, width, height, preserveAR);
    }


    /// <summary>
    /// Crops a texture.
    /// </summary>
    public static void Crop(this Texture2D tex, int width, int height)
    {
        TextureHelper.Crop(tex, width, height);
    }

    /// <summary>
    /// Crops a texture.
    /// </summary>
    public static void Crop(this Texture2D tex, int width, int height, Pivot pivot)
    {
        TextureHelper.Crop(tex, width, height, pivot);
    }

    /// <summary>.
    /// Crops a texture
    /// </summary>
    public static void Crop(this Texture2D tex, Rect area)
    {
        TextureHelper.Crop(tex, area);
    }


    /// <summary>
    /// Invert the colores of the texture.
    /// </summary>
    public static Texture2D Invert(this Texture2D tex)
    {
        return TextureHelper.Invert(tex);
    }

    /// <summary>
    /// Invert the colores of the texture.
    /// </summary>
    public static Texture2D Invert(this Texture2D tex, bool keepAlpha)
    {
        return TextureHelper.Invert(tex, keepAlpha);
    }


    /// <summary>
    /// Gets the pixel color at coordinates.
    /// </summary>
    /// <param name="tex">Source texture</param>
    /// <param name="coord">Coordinates of the pixel</param>
    public static Color GetPixel(this Texture2D tex, Vector2Int coord)
    {
        return tex.GetPixel(coord.x, coord.y);
    }

    /// <summary>
    /// Sets the pixel color at coordinates.
    /// </summary>
    /// <param name="tex">Source texture</param>
    /// <param name="coord">Coordinates of the pixel</param>
    /// <param name="color">Color to set</param>
    public static void SetPixel(this Texture2D tex, Vector2Int coord, Color color)
    {
        tex.SetPixel(coord.x, coord.y, color);
    }

    /// <summary>
    /// Sets the pixel color at coordinates.
    /// </summary>
    /// <param name="tex">Source texture</param>
    /// <param name="coords">Coordinates of the pixels</param>
    public static Color[] GetPixels(this Texture2D tex, Vector2Int[] coords)
    {
        Color[] result = new Color[coords.Length];
        for (int i = 0; i < coords.Length; i++)
        {
            result[i] = tex.GetPixel(coords[i].x, coords[i].y);
        }
        return result;
    }

    /// <summary>
    /// Sets the pixel color at coordinates.
    /// </summary>
    /// <param name="tex">Source texture</param>
    /// <param name="coords">Coordinates of the pixels</param>
    /// <param name="color">Color to set</param>
    public static void SetPixels(this Texture2D tex, Vector2Int[] coords, Color color)
    {
        foreach (var coord in coords)
        {
            tex.SetPixel(coord.x, coord.y, color);
        }
    }

    /// <summary>
    /// Sets the pixel color at coordinates.
    /// </summary>
    /// <param name="tex">Source texture</param>
    /// <param name="coords">Coordinates of the pixels</param>
    /// <param name="color">Color to set</param>
    public static void SetPixels(this Texture2D tex, Vector2Int[] coords, Color[] colors)
    {
        for (int i = 0; i < coords.Length; i++)
        {
            tex.SetPixel(coords[i].x, coords[i].y, colors[i]);
        }
    }
}
