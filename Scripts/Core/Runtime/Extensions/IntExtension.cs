﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class IntExtension
{
    /// <summary>
    /// Pad the number with zeros to reach a number of digits.
    /// </summary>
    public static string FillNumberWithLeftZeros(this int number, int maxDigits)
    {
        var filledNumber = number.ToString();
        var zerosToAdd = maxDigits - filledNumber.Length;
        for (int i = 0; i < zerosToAdd; i++)
        {
            filledNumber = "0" + filledNumber;
        }
        return filledNumber;
    }
}