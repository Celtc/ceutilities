﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class QuaternionExtension
{
    /// <summary>
    /// Calculate the rotational difference from other to source.
    /// </summary>
    public static Quaternion SubstractRotation(this Quaternion q, Quaternion other)
    {
        Quaternion C = Quaternion.Inverse(other) * q;
        return C;
    }

    /// <summary>
    /// Add rotation other to source rotation.
    /// </summary>
    public static Quaternion AddRotation(this Quaternion q, Quaternion other)
    {
        Quaternion C = q * other;
        return C;
    }

    /// <summary>    /// Returns the forward vector of a quaternion.
    /// </summary>
    public static Vector3 GetForwardVector(this Quaternion q)
    {
        return q * Vector3.forward;
    }

    /// <summary>    /// Returns the up vector of a quaternion.
    /// </summary>
    public static Vector3 GetUpVector(this Quaternion q)
    {
        return q * Vector3.up;
    }

    /// <summary>    /// Returns the right vector of a quaternion.
    /// </summary>
    public static Vector3 GetRightVector(this Quaternion q)
    {
        return q * Vector3.right;
    }
}