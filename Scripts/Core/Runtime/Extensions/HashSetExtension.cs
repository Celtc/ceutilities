﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public static class HashSetExtension
{
    /// <summary>
    /// Adds a range of items to the HashSet.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">The source.</param>
    /// <param name="items">The items.</param>
    /// <returns>Returns true all items are added.</returns>
    public static bool AddRange<T>(this HashSet<T> source, IEnumerable<T> items)
    {
        bool allAdded = true;
        foreach (T item in items)
        {
            allAdded &= source.Add(item);
        }
        return allAdded;
    }

    /// <summary>
    /// Adds a range of items to the HashSet, if every items satisfy a predicate.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">The source.</param>
    /// <param name="items">The items.</param>
    /// <returns>Returns true all items are added.</returns>
    public static bool AddRange<T>(this HashSet<T> source, IEnumerable<T> items, System.Func<T, bool> predicate)
    {
        bool allAdded = true;
        foreach (T item in items.Where(predicate))
        {
            allAdded &= source.Add(item);
        }
        return allAdded;
    }
}
