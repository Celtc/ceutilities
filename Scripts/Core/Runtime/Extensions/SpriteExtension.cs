﻿using UnityEngine;

using CEUtilities.Helpers;

public static class SpriteExtension
{
    /// <summary>
    /// Verify if the texture is readable.
    /// </summary>
    public static bool IsReadable(this Sprite sprite)
    {
        return SpriteHelper.CheckReadable(sprite);
    }

	/// <summary>
	/// Creates an sprite from a file.
	/// </summary>
	public static void ImportSprite(this Sprite sprite, string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border)
	{
		sprite = SpriteHelper.ImportSprite(filePath, format, mipmap, filterMode, readable, pivot, pixelsPerUnit, extrude, meshType, border);
	}

	/// <summary>
	/// Return an inverted version of the sprite.
	/// </summary>
	/// <returns></returns>
	public static Sprite Invert(this Sprite sprite)
	{
		return SpriteHelper.Invert(sprite);
	}

	/// <summary>
	/// Return an inverted version of the sprite.
	/// </summary>
	/// <returns></returns>
	public static Sprite Invert(this Sprite sprite, bool keepAlpha)
	{
		return SpriteHelper.Invert(sprite, keepAlpha);
	}
}
