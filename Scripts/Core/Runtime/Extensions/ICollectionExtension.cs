﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public static class ICollectionExtension
{
    /// <summary>
    /// Copy the collection into an array
    /// </summary>
    public static T[] ToArray<T>(this ICollection<T> source)
	{
		var array = new T[source.Count];
		source.CopyTo(array, 0);
		return array;
    }

    /// <summary>
    /// Determines whether [is index valid] [the specified index].
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="collection">The collection.</param>
    /// <param name="index">The index.</param>
    /// <returns>
    ///   <c>true</c> if [is index valid] [the specified index]; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsIndexValid<T>(this ICollection<T> collection, int index)
    {
        if (collection == null)
        {
            Debug.LogError("[CollectionExtension] Trying to check bounds of a null collection. The result will be false");
            return false;
        }
        return index < collection.Count && index >= 0;
    }

    /// <summary>
    /// Adds a range of items and return the indexes of the added items.
    /// </summary>
    public static void AddRange<T>(this ICollection<T> source, IEnumerable<T> collection, ref int[] indexes)
    {
        var items = collection.ToArray();

        int i0 = source.Count;
        indexes = new int[items.Length];

        for (int i = 0; i < items.Length; i++)
        {
            source.Add(items[i]);
            indexes[i] = i0 + i;
        }
    }
}
