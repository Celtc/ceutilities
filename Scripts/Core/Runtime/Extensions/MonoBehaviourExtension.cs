﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MonoBehaviourExtension
{
    /// <summary>
    /// Runs several couroutines in order.
    /// </summary>
    /// <param name="coroutines">Coroutines to run</param>
    public static void RunCoroutines(this MonoBehaviour source, params IEnumerator[] coroutines)
    {
        source.StartCoroutine(source._RunCoroutines(coroutines));
    }

    /// <summary>
    /// Runs several couroutines in parallel or in order.
    /// </summary>
    /// <param name="parallelCoroutines">Should run coroutines in parallel?</param>
    /// <param name="coroutines">Coroutines to run</param>
    public static void RunCoroutines(this MonoBehaviour source, bool parallelCoroutines, params IEnumerator[] coroutines)
    {
        if (parallelCoroutines)
        {
            foreach (var coroutine in coroutines)
            {
                source.StartCoroutine(coroutine);
            }
        }
        else
        {
            source.StartCoroutine(source._RunCoroutines(coroutines));
        }
    }

    /// <summary>
    /// Wrapper routine to run another ones in order.
    /// </summary>
    /// <param name="coroutines">Coroutines to run</param>
    public static IEnumerator _RunCoroutines(this MonoBehaviour source, params IEnumerator[] coroutines)
    {
        foreach (IEnumerator coroutine in coroutines)
        {
            yield return coroutine;
        }
    }
}