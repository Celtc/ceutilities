﻿using UnityEngine;
using System;

using CEUtilities.Structs;

public static class OrientationExtension
{
    /// <summary>
    /// Every Vector2Int representing all orientations.
    /// </summary>
    /// <returns></returns>
    public static Vector2Int[] Vectors()
    {
        Vector2Int[] result = new Vector2Int[8];
        int index = 0;
        foreach (var o in Enum.GetValues(typeof(Orientation)))
        {
            result[index++] = ((Orientation)o).ToVector2Int();
        }
        return result;
    }

    /// <summary>
    /// Return a Quaternion rotation from an orientation.
    /// </summary>
    public static Quaternion ToRotation(this Orientation value)
    {
        return Quaternion.Euler(0, (int)value * 45, 0);
    }

    /// <summary>
    /// Return the vector2 representation of the orientation.
    /// </summary>
    public static Vector3Int ToVector3Int(this Orientation o)
    {
        switch (o)
        {
            case Orientation.N: return new Vector3Int(0, 0, 1);
            case Orientation.NE: return new Vector3Int(1, 0, 1);
            case Orientation.E: return new Vector3Int(1, 0, 0);
            case Orientation.SE: return new Vector3Int(1, 0, -1);
            case Orientation.S: return new Vector3Int(0, 0, -1);
            case Orientation.SW: return new Vector3Int(-1, 0, -1);
            case Orientation.W: return new Vector3Int(-1, 0, 0);
            case Orientation.NW: return new Vector3Int(-1, 0, 1);
            default: return default(Vector3Int);
        }
    }

    /// <summary>
    /// Return the vector2 representation of the orientation.
    /// </summary>
    public static Vector2Int ToVector2Int(this Orientation o)
    {
        return o.ToVector3Int().XZ();
    }

    /// <summary>
    /// Return the vector2 representation of the orientation.
    /// </summary>
    public static Vector3 ToVector3(this Orientation o)
    {
        return o.ToVector3Int();
    }

    /// <summary>
    /// Return the vector2 representation of the orientation.
    /// </summary>
    public static Vector2 ToVector2(this Orientation o)
    {
        return o.ToVector3Int().XZ();
    }
}
