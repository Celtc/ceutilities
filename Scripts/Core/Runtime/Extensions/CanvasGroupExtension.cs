﻿using UnityEngine;
using System.Collections;

public static class CanvasGroupExtension
{
    /// <summary>
    /// Fade out the canvas group.
    /// </summary>
    public static IEnumerator FadeOut(this CanvasGroup source, float duration)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        while (current - start < duration)
        {
            source.alpha = (1 - (current - start) / duration);
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        source.alpha = 0f;
    }

    /// <summary>
    /// Fade in the canvas group.
    /// </summary>
    public static IEnumerator FadeIn(this CanvasGroup source, float duration)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        while (current - start < duration)
        {
            source.alpha = (current - start) / duration;
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        source.alpha = 1f;
    }

    /// <summary>
    /// Fade out the canvas group after a delay.
    /// </summary>
    public static IEnumerator FadeOut(this CanvasGroup source, float duration, float delay)
    {
        yield return new WaitForSeconds(delay);

        yield return source.FadeOut(duration);
    }

    /// <summary>
    /// Fade in the canvas group after a delay.
    /// </summary>
    public static IEnumerator FadeIn(this CanvasGroup source, float duration, float delay)
    {
        yield return new WaitForSeconds(delay);

        yield return source.FadeIn(duration);
    }

    /// <summary>
    /// Fade the canvas group group using an animation curve.
    /// </summary>
    public static IEnumerator Fade(this CanvasGroup source, float duration, AnimationCurve curve)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        while (current - start < duration)
        {
            source.alpha = curve.Evaluate((current - start) / duration);
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        source.alpha = curve.Evaluate(1f);
    }

    /// <summary>
    /// Fade the canvas group using an animation curve after a delay.
    /// </summary>
    public static IEnumerator Fade(this CanvasGroup source, float duration, float delay, AnimationCurve curve)
    {
        yield return new WaitForSeconds(delay);

        yield return source.Fade(duration, curve);
    }
}
