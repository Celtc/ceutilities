﻿using UnityEngine;
using System;
using System.Collections.Generic;

public static class DictionaryExtension
{
    /// <summary>
    /// Delegate for collision solver.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="existing">The existing.</param>
    /// <param name="adding">The adding.</param>
    /// <returns></returns>
    public delegate T CollisionEntrySolver<T>(T existing, T adding);

    /// <summary>
    /// Values the or default.
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="T"></typeparam>
    /// <param name="dictionary">The dictionary.</param>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    public static T GetOrDefault<K, T>(this Dictionary<K, T> dictionary, K key)
    {
        return dictionary.ContainsKey(key) ? dictionary[key] : default(T);
    }

    /// <summary>
    /// Values the or default.
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="T"></typeparam>
    /// <param name="dictionary">The dictionary.</param>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <returns></returns>
    public static T GetOrFallback<K, T>(this Dictionary<K, T> dictionary, K key, T fallback)
    {
        return dictionary.ContainsKey(key) ? dictionary[key] : fallback;
    }

    /// <summary>
    /// Gets an entry in the dictionary or create it if it doesn't exist.
    /// </summary>
    /// <typeparam name="T1">The type of the Key.</typeparam>
    /// <typeparam name="T2">The type of the Value.</typeparam>
    /// <param name="dict">The dictionary.</param>
    /// <param name="key">The entry key.</param>
    /// <returns>The entry.</returns>
    public static T2 GetOrAdd<T1, T2>(this Dictionary<T1, T2> dict, T1 key)
    {
        T2 result = default(T2);
        if (!dict.TryGetValue(key, out result))
        {
            dict.Add(key, result);
        }
        return result;
    }

    /// <summary>
    /// Gets an entry in the dictionary or create it if it doesn't exist.
    /// </summary>
    /// <typeparam name="T1">The type of the Key.</typeparam>
    /// <typeparam name="T2">The type of the Value.</typeparam>
    /// <param name="dict">The dictionary.</param>
    /// <param name="key">The entry key.</param>
    /// <returns>The entry.</returns>
    public static T2 GetOrAdd<T1, T2>(this Dictionary<T1, T2> dict, T1 key, Func<T2> defaultValue)
    {
        T2 result = default(T2);
        if (!dict.TryGetValue(key, out result))
        {
            result = defaultValue();
            dict.Add(key, result);
        }
        return result;
    }

    /// <summary>
    /// Sets an entry in the dictionary, creates it if it doesn't exist.
    /// </summary>
    /// <typeparam name="T1">The type of the Key.</typeparam>
    /// <typeparam name="T2">The type of the Value.</typeparam>
    /// <param name="dict">The dictionary.</param>
    /// <param name="key">The entry key.</param>
    public static void SetOrAdd<T1, T2>(this Dictionary<T1, T2> dict, T1 key, T2 value)
    {
        if (dict.ContainsKey(key))
        {
            dict[key] = value;
        }
        else
        {
            dict.Add(key, value);
        }
    }

    /// <summary>
    /// Sets an entry in the dictionary, creates it if it doesn't exist.
    /// </summary>
    /// <typeparam name="T1">The type of the Key.</typeparam>
    /// <typeparam name="T2">The type of the Value.</typeparam>
    /// <param name="dict">The dictionary.</param>
    /// <param name="key">The entry key.</param>
    /// <param name="solver">Method to apply to add in case the key exists.</param>
    public static void SetOrAdd<T1, T2>(this Dictionary<T1, T2> dict, T1 key, T2 value, CollisionEntrySolver<T2> solver)
    {
        if (dict.ContainsKey(key))
        {
            dict[key] = solver(dict[key], value);
        }
        else
        {
            dict.Add(key, value);
        }
    }
}
