﻿using System;

using CEUtilities.Helpers;

public static class TypeExtension
{
    /// <summary>
    /// Gets the default.
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns></returns>
    public static object GetDefault(this Type type)
    {
        if (type.IsValueType)
        {
            return Activator.CreateInstance(type);
        }
        return null;
    }

    /// <summary>
    /// Return a type alias.
    /// </summary>
    public static string GetAlias(this Type type)
    {
        return TypeHelper.GetAlias(type);
    }

    /// <summary>
    /// Return a type alias.
    /// </summary>
    /// <param name="removeNamespaces">Should remove namespaces</param>
    public static string GetAlias(this Type type, bool removeNamespaces)
    {
        return TypeHelper.GetAlias(type, removeNamespaces);
    }

    /// <summary>
    /// Return a type alias.
    /// </summary>
    /// <param name="removeNamespaces">Should remove namespaces</param>
    /// <param name="convertArrayLiterals">Should remove array literals</param>
    public static string GetAlias(this Type type, bool removeNamespaces, bool convertArrayLiterals)
    {
        return TypeHelper.GetAlias(type, removeNamespaces, convertArrayLiterals);
    }

    /// <summary>
    /// Return a type alias or name if not found.
    /// </summary>
    public static string GetAliasOrName(this Type type)
    {
        return TypeHelper.GetAliasOrName(type);
    }

    /// <summary>
    /// Return a type alias or name if not found.
    /// </summary>
    /// <param name="removeNamespaces">Should remove namespaces</param>
    public static string GetAliasOrName(this Type type, bool removeNamespaces)
    {
        return TypeHelper.GetAliasOrName(type, removeNamespaces);
    }

    /// <summary>
    /// Return a type alias or name if not found.
    /// </summary>
    /// <param name="removeNamespaces">Should remove namespaces</param>
    /// <param name="convertArrayLiterals">Should remove array literals</param>
    public static string GetAliasOrName(this Type type, bool removeNamespaces, bool convertArrayLiterals)
    {
        return TypeHelper.GetAliasOrName(type, removeNamespaces, convertArrayLiterals);
    }

#pragma warning disable 618

    [System.Obsolete("Use GetAliasOrName() instead.")]
    /// <summary>
    /// Return a type alias
    /// </summary>
    /// <param name="removeNamespaces">Should remove namespaces</param>
    /// <param name="convertArrayLiterals">Should remove array literals</param>
    public static string ToAlias(this Type type, bool removeNamespaces, bool convertArrayLiterals)
    {
        return TypeHelper.ToAlias(type, removeNamespaces, convertArrayLiterals);
    }

#pragma warning restore 618
}
