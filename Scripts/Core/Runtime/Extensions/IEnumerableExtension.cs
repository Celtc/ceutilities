﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public static class IEnumerableExtension
{
    /// <summary>
    /// Iterate through elements.
    /// </summary>
	public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
    {
        foreach (T item in source)
        {
            action(item);
        }
    }

    /// <summary>
    /// Count the elements in the IEnumerable.
    /// </summary>
    public static int GetCount<T>(this IEnumerable<T> source)
    {
        var collection = source as ICollection<T>;
        if (collection != null)
        {
            return collection.Count;
        }

        var result = 0;
        using (IEnumerator<T> enumerator = source.GetEnumerator())
        {
            while (enumerator.MoveNext())
            {
                result++;
            }
        }
        return result;
    }

    /// <summary>
    /// Returns a random element of an IEnumerable.
    /// </summary>
    /// <typeparam name="T">Type of the IEnumerable</typeparam>
    /// <param name="source">IEnumerable containing the element</param>
    /// <returns>A random element of the IEnumerable</returns>
    public static T GetRandom<T>(this IEnumerable<T> source)
    {
        var count = source.Count();
        if (source == null || count == 0)
        {
            throw new ArgumentException("Enumerable is empty", "source");
        }
        return source.ElementAt(UnityEngine.Random.Range(0, count));
    }

    /// <summary>
    /// Return the index of an element.
    /// Return -1 if not found.
    /// </summary>
    /// <returns>Index of value, -1 if not found</returns>
	public static int IndexOf<T>(this IEnumerable<T> source, T value)
    {
        int index = 0;
        var comparer = EqualityComparer<T>.Default; // or pass in as a parameter
        foreach (T item in source)
        {
            if (comparer.Equals(item, value))
            {
                return index;
            }
            index++;
        }
        return -1;
    }

    /// <summary>
    /// Checks for an element inside the IEnumerable.
    /// </summary>
	public static bool Contains<T>(this IEnumerable<T> source, Func<T, bool> predicate)
    {
        var finded = false;
        IList<T> c = source as IList<T>;
        if (c != null)
        {
            for (int i = 0; i < c.Count; i++)
            {
                if (predicate(c[i]))
                {
                    finded = true;
                    break;
                }
            }
            return finded;
        }

        foreach (var elem in source)
        {
            if (predicate(elem))
            {
                finded = true;
                break;
            }
        }
        return finded;
    }
}
