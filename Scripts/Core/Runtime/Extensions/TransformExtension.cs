﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities;
using CEUtilities.Helpers;

public static class TransformExtension
{
    /// <summary>
    /// Animate an element toward a direction.
    /// </summary>
    public static IEnumerator TweenPosition(this Transform target, bool local, Vector3 start, Vector3 end, AnimationCurve curve, float duration)
    {
        yield return local ?
            TweenPositionLocally(target, start, end, curve, duration) :
            TweenPositionGlobally(target, start, end, curve, duration);
    }

    /// <summary>
    /// Animate an element toward a direction.
    /// </summary>
    private static IEnumerator TweenPositionGlobally(this Transform target, Vector3 start, Vector3 end, AnimationCurve curve, float duration)
    {
        var deltaTime = 0f;
        var timer = Time.realtimeSinceStartup;
        var deltaPos = end - start;
        while (deltaTime < duration)
        {
            var deltaCurrent = deltaPos * curve.Evaluate(deltaTime / duration);
            target.transform.position = start + deltaCurrent;
            deltaTime = Time.realtimeSinceStartup - timer;
            yield return null;
        }

        // Set final value
        target.transform.position = end;
    }

    /// <summary>
    /// Animate an element toward a direction locally.
    /// </summary>
    public static IEnumerator TweenPositionLocally(this Transform target, Vector3 start, Vector3 end, AnimationCurve curve, float duration)
    {
        var deltaTime = 0f;
        var timer = Time.realtimeSinceStartup;
        var deltaPos = end - start;
        while (deltaTime < duration)
        {
            var deltaCurrent = deltaPos * curve.Evaluate(deltaTime / duration);
            target.transform.localPosition = start + deltaCurrent;
            deltaTime = Time.realtimeSinceStartup - timer;
            yield return null;
        }

        // Set final value
        target.transform.localPosition = end;
    }

    /// <summary>
    /// Tween the scale to a value.
    /// </summary>
    public static IEnumerator TweenScale(this Transform target, Vector3 scale, float duration, AnimationCurve curve = null)
    {
        if (curve == null)
        {
            curve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        }

        var deltaTime = 0f;
        var timer = Time.realtimeSinceStartup;

        var start = target.localScale;
        var deltaScale = scale - start;
        while (deltaTime < duration)
        {
            var deltaCurrent = deltaScale * curve.Evaluate(deltaTime / duration);
            target.transform.localScale = start + deltaCurrent;
            deltaTime = Time.realtimeSinceStartup - timer;
            yield return null;
        }

        // Set final value
        target.transform.localScale = scale;
    }

    /// <summary>
    /// Convenience extension that destroys all children of the transform.
    /// </summary>
    public static void DestroyChildrens(this Transform t)
    {
        Transform child;
        while (t.childCount != 0)
        {
            child = t.GetChild(0);
            Utilities.Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Convenience extension that destroys the direct childrne with name.
    /// </summary>
    public static void DestroyChildren(this Transform t, string name)
    {
        var childCount = t.childCount;
        Transform child;
        for (int i = 0; i < childCount; i++)
        {
            child = t.GetChild(i);
            if (child.name == name)
            {
                Utilities.Destroy(child.gameObject);
                break;
            }
        }
    }

    /// <summary>
    /// Get all child transforms at the specified level. Level 1 are the direct children.
    /// </summary>
    public static Transform[] GetLevelChilds(this Transform source, int level)
    {
        int currLevel = 0;
        var transforms = new List<Transform>() { source };

        while (currLevel++ < level)
        {
            var currTransforms = new List<Transform>();
            // For every transform
            for (int i = 0; i < transforms.Count; i++)
            {
                var transform = transforms[i];
                // Add every child transform
                for (int ii = 0; ii < transform.childCount; ii++)
                {
                    currTransforms.Add(transform.GetChild(ii));
                }
            }
            transforms = currTransforms;
        }

        return transforms.ToArray();
    }

    /// <summary>
    /// Determines whether the 'parent' contains a 'child' in its hierarchy.
    /// </summary>
    public static bool IsChild(this Transform child, Transform parent)
    {
        if (parent == null)
        {
            return false;
        }

        while (child != null)
        {
            if (child == parent)
            {
                return true;
            }
            child = child.parent;
        }
        return false;
    }

    /// <summary>
    /// Calculates the center of a transform, including all childs in the calculation.
    /// </summary>
    /// <param name="source">Target tranform to calculate the center.</param>
    /// <returns>Calculate the center of a gameobject.</returns>
    public static Vector3 CalculateCenter(this Transform source)
    {
        return GeometryHelper.CalculateCenter(source);
    }

    /// <summary>
    /// Gets the bounds containing the given transform and all his childs.
    /// </summary>
    /// <param name="Source">Target transform.</param>
    /// <returns>Bounds containing every child renderer.</returns>
    public static Bounds RecursiveBounds(this Transform source)
    {
        return GeometryHelper.RecursiveBounds(source);
    }

    /// <summary>
    /// Gets the hierarchy path.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="separator">The separator.</param>
    /// <returns></returns>
    public static string GetHierarchyPath(this Transform value) => GetHierarchyPath(value, "/");

    /// <summary>
    /// Gets the hierarchy path.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="separator">The separator.</param>
    /// <returns></returns>
    public static string GetHierarchyPath(this Transform value, string separator)
    {
        var current = value;
        var path = current.name;
        while (current.parent != null)
        {
            current = current.parent;
            path = string.Join(separator, current.name, path);
        }
        return path;
    }
}