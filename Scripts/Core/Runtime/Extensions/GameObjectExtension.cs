﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using CEUtilities;
using CEUtilities.Coroutines;
using CEUtilities.Helpers;

public static class GameObjectExtension
{
    /// <summary>
    /// Activates/Deactivates the game object with a delay.
    /// </summary>
    public static void SetActive(this GameObject go, bool value, float delay)
    {
        StaticCoroutineManager.Instance.StartCoroutine(_SetActive(go, value, false, delay));
    }

    /// <summary>
    /// Activates/Deactivates the game object recursively with a delay.
    /// </summary>
    public static void SetActive(this GameObject go, bool value, bool recursive, float delay)
    {
        StaticCoroutineManager.Instance.StartCoroutine(_SetActive(go, value, recursive, delay));
    }

    /// <summary>
    /// Activateion / Deactivation routine for the game object, recursively with a delay.
    /// </summary>
    public static IEnumerator _SetActive(GameObject go, bool value, bool recursive, float delay)
    {
        if (delay > 0)
            yield return new WaitForSeconds(delay);
        go.SetActive(value, recursive);
    }

    /// <summary>
    /// Destroy the gameobject.
    /// </summary>
    public static void Destroy(this GameObject go)
    {
        Utilities.Destroy(go);
    }

    /// <summary>
    /// Does the game object have the component.
    /// </summary>
    public static bool HasComponent<T>(this GameObject go)
    {
        var comp = go.GetComponent<T>();
        return !(comp == null || comp.Equals(null));
    }

    /// <summary>
    /// Gets or add a component. Usage example:
    /// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
    /// </summary>
    public static T GetOrAddComponent<T>(this GameObject go) where T : Component
    {
        return go.AddMissingComponent<T>();
    }

    /// <summary>
    /// Gets or add a component. Usage example:
    /// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
    /// </summary>
    public static Component GetOrAddComponent(this GameObject go, Type componentType)
    {
        return go.AddMissingComponent(componentType);
    }

    /// <summary>
    /// Extension for the game object that checks to see if the component already exists before adding a new one.
    /// If the component is already present it will be returned instead.
    /// </summary>
    public static T AddMissingComponent<T>(this GameObject go) where T : Component
    {
        T comp = go.GetComponent<T>();
        if (comp == null || comp.Equals(null))
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.Undo.RecordObject(go, "Add " + typeof(T));
                UnityEditor.EditorUtility.SetDirty(go);
            }
#endif
            comp = go.AddComponent<T>();
        }
        return comp;
    }

    /// <summary>
    /// Extension for the game object that checks to see if the component already exists before adding a new one.
    /// If the component is already present it will be returned instead.
    /// </summary>
    public static Component AddMissingComponent(this GameObject go, Type componentType)
    {
        var comp = go.GetComponent(componentType);
        if (comp == null || comp.Equals(null))
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.Undo.RecordObject(go, "Add " + componentType.GetType().GetAliasOrName(true));
                UnityEditor.EditorUtility.SetDirty(go);
            }
#endif
            comp = go.AddComponent(componentType);
        }
        return comp;
    }

    /// <summary>
    /// Changes the game object layer, and may change all of its children layer.
    /// </summary>
    public static void SetLayer(this GameObject go, int layer, bool recursively)
    {
        go.layer = layer;

        if (recursively)
        {
            for (int i = 0; i < go.transform.childCount; i++)
            {
                go.transform.GetChild(i).gameObject.SetLayer(layer, true);
            }
        }
    }

    /// <summary>
    /// Gets all the components of the gameobject, including its children's, if the tag is correct.
    /// </summary>
    /// <typeparam name="T">Type of the component to look for</typeparam>
    /// <param name="gameObject">Base GameObject</param>
    /// <param name="tag">Tag that must match</param>
    /// <returns>All components found</returns>
    public static T[] GetComponentsInChildrenWithTag<T>(this GameObject gameObject, string tag) where T : Component
    {
        List<T> results = new List<T>();

        if (gameObject.CompareTag(tag))
        {
            results.Add(gameObject.GetComponent<T>());
        }

        foreach (Transform t in gameObject.transform)
        {
            results.AddRange(t.gameObject.GetComponentsInChildrenWithTag<T>(tag));
        }

        return results.ToArray();
    }

    /// <summary>
    /// Gets the first component of the gameobject, including its children's, that has a certain tag.
    /// </summary>
    /// <typeparam name="T">Type of the component to look for</typeparam>
    /// <param name="gameObject">Base GameObject</param>
    /// <param name="tag">Tag that must match</param>
    /// <returns>First component found</returns>
    public static T GetComponentInChildrenWithTag<T>(this GameObject gameObject, string tag) where T : Component
    {
        if (gameObject.CompareTag(tag))
        {
            return gameObject.GetComponent<T>();
        }

        foreach (Transform t in gameObject.transform)
        {
            T comp = t.gameObject.GetComponentInChildrenWithTag<T>(tag);
            if (comp != null)
            {
                return comp;
            }
        }

        return null;
    }

    /// <summary>
    /// Get all the components in the immediate children.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="go"></param>
    /// <returns></returns>
    public static List<T> GetComponentsInImmediateChildren<T>(this GameObject go)
    {
        var comps = new List<T>();
        for (int i = 0; i < go.transform.childCount; i++)
        {
            var child = go.transform.GetChild(i);
            var comp = child.GetComponent<T>();
            if (comp != null) comps.Add(comp);
        }
        return comps;
    }

    /// <summary>
    /// Gets all the components of the gameobject, including its ancestors', if the tag is correct.
    /// </summary>
    /// <typeparam name="T">Type of the component to look for</typeparam>
    /// <param name="gameObject">Base GameObject</param>
    /// <returns>All components found</returns>
    public static T[] GetComponentsInParents<T>(this GameObject gameObject)
        where T : Component
    {
        List<T> results = new List<T>();
        for (Transform t = gameObject.transform; t != null; t = t.parent)
        {
            T result = t.GetComponent<T>();
            if (result != null)
            {
                results.Add(result);
            }
        }

        return results.ToArray();
    }

    /// <summary>
    /// Gets all the components of the gameobject, including its ancestors', if the tag is correct.
    /// </summary>
    /// <typeparam name="T">Type of the component to look for</typeparam>
    /// <param name="gameObject">Base GameObject</param>
    /// <returns>All components found</returns>
    public static T[] GetComponentsInParentsWithTag<T>(this GameObject gameObject, string tag)
    where T : Component
    {
        var results = new List<T>();

        if (gameObject.CompareTag(tag))
        {
            results.Add(gameObject.GetComponent<T>());
        }

        foreach (Transform t in gameObject.transform)
        {
            results.AddRange(t.gameObject.GetComponentsInParentsWithTag<T>(tag));
        }

        return results.ToArray();
    }

    /// <summary>
    /// Activates/Deactivates the game object recursively
    /// </summary>
    public static void SetActive(this GameObject go, bool value, bool recursively)
    {
        go.SetActive(value);

        if (recursively)
        {
            for (int i = 0; i < go.transform.childCount; i++)
            {
                go.transform.GetChild(i).gameObject.SetActive(value, recursively);
            }
        }
    }

    /// <summary>
    /// Add a new child game object.
    /// </summary>
    public static GameObject AddChild(this GameObject parent)
    {
        var go = new GameObject();

#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(go, "Create Object");
#endif

        if (parent != null)
        {
            go.transform.SetParent(parent.transform, false);
            go.layer = parent.layer;
        }

        return go;
    }

    /// <summary>
    /// Instantiate an object and add it to the specified parent.
    /// </summary>
    public static GameObject AddChild(this GameObject parent, GameObject prefab)
    {
        var go = UnityEngine.Object.Instantiate(prefab, parent.transform, false) as GameObject;

#if UNITY_EDITOR
        UnityEditor.Undo.RegisterCreatedObjectUndo(go, "Create Object");
#endif

        if (go != null)
        {
            go.layer = parent.layer;
        }

        return go;
    }

    /// <summary>
    /// Sets value to ALL colliders (including the children's)
    /// </summary>
    /// <param name="gameObject">Root object</param>
    /// <param name="value">Value to be set</param>
    public static void SetCollisionRecursively(this GameObject gameObject, bool value)
    {
        var colliders = gameObject.GetComponentsInChildren<Collider>();
        foreach (Collider collider in colliders)
        {
            collider.enabled = value;
        }
    }

    /// <summary>
    /// Sets value to ALL renderers (including the children's)
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="value">Value to be set</param>
    public static void SetVisualRecursively(this GameObject gameObject, bool value)
    {
        var renderers = gameObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer renderer in renderers)
        {
            renderer.enabled = value;
        }
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="copyName">Does the new instance will have the same name from the original?</param>
    /// <returns></returns>
    public static GameObject Clone(this GameObject original)
    {
        return original.Clone(original.name);
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="position">Position of the new instance.</param>
    /// <returns>A clone of the Game Object</returns>
    public static GameObject Clone(this GameObject original, Vector3 position)
    {
        return original.Clone(position, original.transform.rotation, original.name, original.transform.parent);
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="name">Name of the new instance.</param>
    /// <returns>A clone of the Game Object</returns>
    public static GameObject Clone(this GameObject original, string name)
    {
        return original.Clone(original.transform.position, original.transform.rotation, name, original.transform.parent);
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="customParent">Parent of the new instance (use 'null' to put the new instance at the root)</param>
    /// <param name="worldPositionStays">If true, the parent-relative position, scale and rotation is modified such that the object keeps the same world space position, rotation and scale as before.</param>
    /// <returns>A clone of the Game Object</returns>
    public static GameObject Clone(this GameObject original, Transform parent, bool worldPositionStays = false)
    {
        return original.Clone(original.transform.position, original.transform.rotation, original.name, parent, worldPositionStays);
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="position">Position of the new instance.</param>
    /// <param name="customParent">Parent of the new instance (use 'null' to put the new instance at the root)</param>
    /// <param name="worldPositionStays">If true, the parent-relative position, scale and rotation is modified such that the object keeps the same world space position, rotation and scale as before.</param>
    /// <returns>A clone of the Game Object</returns>
    public static GameObject Clone(this GameObject original, Vector3 position, Transform parent, bool worldPositionStays = false)
    {
        return original.Clone(position, original.transform.rotation, original.name, parent, worldPositionStays);
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="name">Name of the new instance.</param>
    /// <param name="customParent">Parent of the new instance (use 'null' to put the new instance at the root)</param>
    /// <param name="worldPositionStays">If true, the parent-relative position, scale and rotation is modified such that the object keeps the same world space position, rotation and scale as before.</param>
    /// <returns>A clone of the Game Object</returns>
    public static GameObject Clone(this GameObject original, string name, Transform parent, bool worldPositionStays = false)
    {
        return original.Clone(original.transform.position, original.transform.rotation, name, parent, worldPositionStays);
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="position">Position of the new instance.</param>
    /// <param name="name">Name of the new instance.</param>
    /// <param name="customParent">Parent of the new instance (use 'null' to put the new instance at the root)</param>
    /// <param name="worldPositionStays">If true, the parent-relative position, scale and rotation is modified such that the object keeps the same world space position, rotation and scale as before.</param>
    /// <returns>A clone of the Game Object</returns>
    public static GameObject Clone(this GameObject original, Vector3 position, string name, Transform parent, bool worldPositionStays = false)
    {
        return original.Clone(position, original.transform.rotation, name, parent, worldPositionStays);
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="position">Position of the new instance.</param>
    /// <param name="rotation">Rotation of the new instance.</param>
    /// <returns>A clone of the Game Object</returns>
    public static GameObject Clone(this GameObject original, Vector3 position, Quaternion rotation)
    {
        return original.Clone(position, rotation, original.name, original.transform.parent);
    }

    /// <summary>
    /// Creates a new instance of the Game Object.
    /// </summary>
    /// <param name="original">Original object to clone from.</param>
    /// <param name="position">Position of the new instance.</param>
    /// <param name="rotation">Rotation of the new instance.</param>
    /// <param name="name">Name of the new instance.</param>
    /// <param name="customParent">Parent of the new instance (use 'null' to put the new instance at the root)</param>
    /// <param name="worldPositionStays">If true, the parent-relative position, scale and rotation is modified such that the object keeps the same world space position, rotation and scale as before.</param>
    /// <returns>A clone of the Game Object</returns>
    public static GameObject Clone(this GameObject original, Vector3 position, Quaternion rotation, string name, Transform parent, bool worldPositionStays = false)
    {
        var clone = UnityEngine.Object.Instantiate(original, position, rotation) as GameObject;
        clone.transform.SetParent(parent, worldPositionStays);
        clone.name = name;
        return clone;
    }

    /// <summary>
    /// Sets the game object position.
    /// </summary>
    /// <param name="go">Game object to set</param>
    /// <param name="position">Position</param>
    public static void SetPosition(this GameObject go, Vector3 position)
    {
        go.transform.position = position;
    }

    /// <summary>
    /// Sets the game object local position.
    /// </summary>
    /// <param name="go">Game object to set</param>
    /// <param name="localPosition">Local position</param>
    public static void SetLocalPosition(this GameObject go, Vector3 localPosition)
    {
        go.transform.localPosition = localPosition;
    }

    /// <summary>
    /// Sets the game object local scale.
    /// </summary>
    /// <param name="go">Game object to set</param>
    /// <param name="localScale">Local scale</param>
    public static void SetLocalScale(this GameObject go, Vector3 localScale)
    {
        go.transform.localScale = localScale;
    }

    /// <summary>
    /// Sets the game object local scale.
    /// </summary>
    /// <param name="go">Game object to set</param>
    /// <param name="scale">Global or Lossy scale</param>
    public static void SetLossyScale(this GameObject go, Vector3 scale)
    {
        go.transform.localScale = Vector3.one;
        go.transform.localScale = new Vector3
        (
            scale.x / go.transform.lossyScale.x,
            scale.y / go.transform.lossyScale.y,
            scale.z / go.transform.lossyScale.z
        );
    }

    /// <summary>
    /// Sets the game object rotation.
    /// </summary>
    /// <param name="go">Game object to set</param>
    /// <param name="rotation">Rotation</param>
    public static void SetRotation(this GameObject go, Quaternion rotation)
    {
        go.transform.rotation = rotation;
    }

    /// <summary>
    /// Calculates the center of a gameobject, including all childs in the calculation.
    /// </summary>
    /// <param name="source">Target tranform to calculate the center.</param>
    /// <returns>Calculate the center of a gameobject.</returns>
    public static Vector3 CalculateCenter(this GameObject source)
    {
        return GeometryHelper.CalculateCenter(source.transform);
    }

    /// <summary>
    /// Tells if the game object is a scene instance, and not an asset of the project.
    /// </summary>
    public static bool IsSceneObject(this GameObject source)
    {
#if UNITY_EDITOR
        return !string.IsNullOrEmpty(source.scene.name);
#else
        return true;
#endif
    }
}
