﻿using UnityEngine;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public static class DataTableExtension
{
    /// <summary>
    /// Contents to string.
    /// </summary>
    /// <param name="table">The table.</param>
    /// <returns></returns>
    public static string ContentToString(this DataTable table)
    {
        var sb = new StringBuilder();
        var index = 0;
        foreach (DataRow dataRow in table.Rows)
        {
            if (index++ > 0)
            {
                sb.AppendLine();
            }
            foreach (var item in dataRow.ItemArray)
            {
                sb.Append(item).Append("\t");
            }
        }
        return sb.ToString();
    }
}