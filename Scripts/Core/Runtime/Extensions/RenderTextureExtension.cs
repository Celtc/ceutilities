﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities;

public static class RenderTextureExtension
{
    /// <summary>
    /// Reads a RenderTexture into a teture 2d.
    /// </summary>
    public static void ToTexture2D(this RenderTexture source, ref Texture2D texture2D)
    {
        // Prepare texture
        if (texture2D == null ||
            texture2D.width != source.width ||
            texture2D.height != source.height)
        {
            if (texture2D != null)
            {
                Utilities.Destroy(texture2D);
            }

            texture2D = new Texture2D(source.width, source.height, TextureFormat.ARGB32, false);
        }

        // Read
        RenderTexture.active = source;
        texture2D.ReadPixels(new Rect(0, 0, source.width, source.height), 0, 0);
        RenderTexture.active = null;
    }
}