﻿using UnityEngine;

using CEUtilities.Helpers;
using CEUtilities.Colors;

public static class ColorExtension
{
    /// <summary>
    /// Return a variated color.
    /// </summary>
    public static Color Invert(this Color source)
    {
        return source.Invert(false);
    }

    /// <summary>
    /// Return a variated color.
    /// </summary>
    public static Color Invert(this Color source, bool invertAlpha)
    {
        return new Color(1 - source.r, 1 - source.g, 1 - source.b, invertAlpha ? 1 - source.a : source.a);
    }

    /// <summary>
    /// Return a variated color.
    /// </summary>
    public static Color Change(this Color source, float value, int colorMask)
    {
        float r, g, b, a;
        r = (colorMask & (1 << 0)) != 0 ? value : source.r;
        g = (colorMask & (1 << 1)) != 0 ? value : source.g;
        b = (colorMask & (1 << 2)) != 0 ? value : source.b;
        a = (colorMask & (1 << 3)) != 0 ? value : source.a;
        return new Color(r, g, b, a);
    }

    /// <summary>
    /// Return a variated color.
    /// </summary>
    public static Color WithA(this Color source, float alpha)
    {
        var newColor = source;
        newColor.a = alpha;
        return newColor;
    }

    /// <summary>
    /// Pre-multiply shaders result in a black outline if this operation is done in the shader. It's better to do it outside.
    /// </summary>
    public static Color ApplyPMA(this Color source)
    {
        if (source.a != 1f)
        {
            source.r *= source.a;
            source.g *= source.a;
            source.b *= source.a;
        }
        return source;
    }

    /// <summary>
    /// Return the greyscale of the color.
    /// </summary>
    public static Color ToGreyscale(this Color source)
    {
        var greyscale = source.grayscale;
        return new Color(greyscale, greyscale, greyscale, source.a);
    }

    /// <summary>
    /// Creates a hex string from the color.
    /// </summary>
    public static string ToHexString(this Color source, bool includeAlpha = true)
    {
        return ColorHelper.ToHexString(source, includeAlpha);
    }

    /// <summary>
    /// Creates a HSVColor from this RGB color.
    /// </summary>
    public static HSVColor ToHSV(this Color source)
    {
        return ColorHelper.RGBToHSV(source);
    }
}
