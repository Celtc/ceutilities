﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class BoxColliderExtension
{
    /// <summary>
    /// Retrieve the max point of the box collider.
    /// </summary>
    public static Vector3 Max(this BoxCollider source)
    {
        return source.center + source.size * .5f;
    }

    /// <summary>
    /// Retrieve the min point of the box collider.
    /// </summary>
    public static Vector3 Min(this BoxCollider source)
    {
        return source.center - source.size * .5f;
    }

    /// <summary>
    /// Sets the box collider values using the min and max values.
    /// </summary>
    /// <param name="source">Box collider to set.</param>
    /// <param name="min">Min value.</param>
    /// <param name="max">Max value.</param>
    public static void SetMinMax(this BoxCollider source, Vector3 min, Vector3 max)
    {
        source.center = (max + min) * .5f;
        source.size = max - min;
    }

    /// <summary>
    /// Encapsulate a world point in the box collider.
    /// </summary>
    /// <param name="source">Source box collider.</param>
    /// <param name="worldPoint">World point to encapsulate.</param>
    public static void Encapsulate(this BoxCollider source, Vector3 worldPoint)
    {
        var localPoint = source.transform.InverseTransformPoint(worldPoint);

        Vector3 max = source.Max(), min = source.Min();

        if (localPoint.x > max.x)
        {
            max.x = localPoint.x;
        }
        else if (localPoint.x < min.x)
        {
            min.x = localPoint.x;
        }

        if (localPoint.y > max.y)
        {
            max.y = localPoint.y;
        }
        else if (localPoint.y < min.y)
        {
            min.y = localPoint.y;
        }

        if (localPoint.z > max.z)
        {
            max.z = localPoint.z;
        }
        else if (localPoint.z < min.z)
        {
            min.z = localPoint.z;
        }

        source.SetMinMax(min, max);
    }

    /// <summary>
    /// Encapsulate an array of world point sin the box collider.
    /// </summary>
    /// <param name="source">Source box collider.</param>
    /// <param name="worldPoint">World points to encapsulate.</param>
    public static void Encapsulate(this BoxCollider source, Vector3[] worldPoints)
    {
        Vector3 max = source.Max(), min = source.Min();
        var matrix = source.transform.worldToLocalMatrix;

        foreach (var point in worldPoints)
        {
            var localPoint = matrix.MultiplyPoint3x4(point);

            if (localPoint.x > max.x)
            {
                max.x = localPoint.x;
            }
            else if (localPoint.x < min.x)
            {
                min.x = localPoint.x;
            }

            if (localPoint.y > max.y)
            {
                max.y = localPoint.y;
            }
            else if (localPoint.y < min.y)
            {
                min.y = localPoint.y;
            }

            if (localPoint.z > max.z)
            {
                max.z = localPoint.z;
            }
            else if (localPoint.z < min.z)
            {
                min.z = localPoint.z;
            }
        }

        source.SetMinMax(min, max);
    }

    /// <summary>
    /// Encapsulate a mesh inside the box collider.
    /// </summary>
    /// <param name="source">Source box collider.</param>
    /// <param name="filter">Mesh filter containing the mesh to encapsulate.</param>
    public static void Encapsulate(this BoxCollider source, MeshFilter filter)
    {
        // Get world position of mesh vetices
        var vertices = filter.sharedMesh.vertices;

        var points = new Vector3[vertices.Length];
        var matrix = filter.transform.localToWorldMatrix;
        for (int i = 0; i < vertices.Length; i++)
        {
            points[i] = matrix.MultiplyPoint3x4(vertices[i]);
        }

        // Encapsulate points
        source.Encapsulate(points);
    }

    /// <summary>
    /// Encapsulates a gameobject if it has an enabled Renderer.
    /// </summary>
    /// <param name="source">Affecting bounding box.</param>
    /// <param name="target">Game object to encapsulate.</param>
    /// <param name="includeChilds">Should encapsulate also its child game objects?</param>
    public static void Encapsulate(this BoxCollider source, GameObject target, bool includeChilds)
    {
        // Set targets to encapsulate
        Renderer[] renderers = null;
        if (!includeChilds)
        {
            var renderer = target.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderers = new Renderer[] { renderer };
            }
        }
        else
        {
            renderers = target.GetComponentsInChildren<Renderer>();
        }

        // Encapsulate
        if (renderers != null && renderers.Length > 0)
        {
            foreach (var renderer in renderers)
            {
                if (renderer.enabled)
                {
                    source.Encapsulate(renderer.GetComponent<MeshFilter>());
                }
            }
        }
    }
}