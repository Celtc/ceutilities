﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

using CEUtilities;
using CEUtilities.Patterns;
using CEUtilities.API;
using CEUtilities.Audio;

namespace UnityEngine.Events
{
    [System.Serializable]
    public class UnityEvent_GameState : UnityEvent<GameState> { }
    [System.Serializable]
    public class UnityEvent_Pool : UnityEvent<Pool> { }
    [System.Serializable]
    public class UnityEvent_GameObject_Pool : UnityEvent<GameObject, Pool> { }
    [System.Serializable]
    public class UnityEvent_FTPTask : UnityEvent<FTPTask> { }
    [System.Serializable]
    public class UnityEvent_FTPTask_String : UnityEvent<FTPTask, string> { }
    [System.Serializable]
    public class UnityEvent_FTPTask_StringArray : UnityEvent<FTPTask, string[]> { }
    [System.Serializable]
    public class UnityEvent_FTPTask_Bool : UnityEvent<FTPTask, bool> { }
    [System.Serializable]
    public class UnityEvent_AudioTrack : UnityEvent<AudioTrack> { }
    [System.Serializable]
    public class UnityEvent_SceneHandler : UnityEvent<SceneHandler> { }
#if VUFORIA_ANDROID_SETTINGS
    [System.Serializable]
    public class UnityEvent_ARWaypoint : UnityEvent<ARWaypoint> { }
    [System.Serializable]
    public class UnityEvent_TimeoutTrackableEventHandler : UnityEvent<TimeoutTrackableEventHandler> { }
#endif
}


