﻿using UnityEngine;

public static class RectExtension
{
    /// <summary>
    /// Sums another rect. The resulting rect will be the addition of both sizes and posisitions.
    /// </summary>
    public static Rect Sum(this Rect source, Rect other)
    {
        source = new Rect(
            source.x + other.x,
            source.y + other.y,
            source.width + other.width,
            source.height + other.height
        );
        return source;
    }

    /// <summary>
    /// Sums another rect position values.
    /// </summary>
    public static Rect SumPosition(this Rect source, Rect other)
    {
        source = new Rect(
            source.x + other.x,
            source.y + other.y,
            source.width,
            source.height
        );
        return source;
    }

    /// <summary>
    /// Sums another rect size.
    /// </summary>
    public static Rect SumSize(this Rect source, Rect other)
    {
        source = new Rect(
            source.x,
            source.y,
            source.width + other.width,
            source.height + other.height
        );
        return source;
    }

    /// <summary>
    /// Sums another rect X Y values.
    /// </summary>
    public static Rect Replace(this Rect source, Rect other, int mask)
    {
        source = new Rect(
            (mask & (1 << 0)) != 0 ? other.x : source.x,
            (mask & (1 << 1)) != 0 ? other.y : source.y,
            (mask & (1 << 2)) != 0 ? other.width : source.width,
            (mask & (1 << 3)) != 0 ? other.height : source.height
        );
        return source;
    }

#if UNITY_EDITOR

    /// <summary>
    /// Remove first Editor line.
    /// </summary>
    public static Rect RemoveTopLine(this Rect source)
    {
        source = new Rect(
            source.x,
            source.y + UnityEditor.EditorGUIUtility.singleLineHeight,
            source.width,
            source.height - UnityEditor.EditorGUIUtility.singleLineHeight
        );
        return source;
    }

    /// <summary>
    /// Remove last editor line.
    /// </summary>
    public static Rect RemoveBottomLine(this Rect source)
    {
        source = new Rect(
            source.x,
            source.y,
            source.width,
            source.height - UnityEditor.EditorGUIUtility.singleLineHeight
        );
        return source;
    }

#endif
}
