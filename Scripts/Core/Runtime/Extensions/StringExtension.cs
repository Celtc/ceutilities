﻿using UnityEngine;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Linq;

public static class StringExtension
{
    #region Static Regex

    private readonly static Regex tokenMatcher = new Regex(@"\{([^:}]*):*([^}]*)}", RegexOptions.Compiled);

    private readonly static Regex wordMatcher = new Regex(@"[^\s]+");

    private readonly static Regex mailMatcher = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");

    private readonly static Regex ipv6Matcher = new Regex(@"^(?:[A-F0-9]{1,4}:){7}[A-F0-9]{1,4}$");

    private readonly static Regex ipv4Matcher = new Regex(@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");

    #endregion Static Regex

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}. Born {date:yyyyMMdd}" => "His name is Ryan. Born 20170927".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator. First parameter is the token, second any suffix.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, Func<string, string, string> tokenEvaluator) => TokensReplace(value, tokenEvaluator, false);

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}. Born {date:yyyyMMdd}" => "His name is Ryan. Born 20170927".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator. First parameter is the token, seconde any suffix.</param>
    /// <param name="keepBrackets">if set to <c>true</c> [keep brackets].</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, Func<string, string, string> tokenEvaluator, bool keepBrackets)
    {
        return tokenMatcher.Replace(value, match => string.Format("{0}{1}{2}",
            keepBrackets ? "{" : string.Empty,
            tokenEvaluator(match.Groups[1].Value, match.Groups[2].Value),
            keepBrackets ? "}" : string.Empty));
    }

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}. Born {date:yyyyMMdd}" => "His name is Ryan. Born 20170927".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator. First parameter is the token, seconde any suffix.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, Dictionary<string, string> tokenDictionary) => TokensReplace(value, tokenDictionary, false);

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}." => "His name is Ryan.".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, Dictionary<string, string> tokenDictionary, bool keepBrackets)
    {
        return tokenMatcher.Replace(value, match => string.Format("{0}{1}{2}{3}",
            keepBrackets ? "{" : string.Empty,
            tokenDictionary.GetOrFallback(match.Groups[1].Value, match.Groups[1].Value),
            !string.IsNullOrEmpty(match.Groups[2].Value) ? ":" + match.Groups[2].Value : string.Empty,
            keepBrackets ? "}" : string.Empty));
    }

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}. Born {date:yyyyMMdd}" => "His name is Ryan. Born 20170927".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator. First parameter is the token, seconde any suffix.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, bool keepBrackets, params Tuple<string, string>[] tokensPair) => TokensReplace(value, tokensPair.ToDictionary(p => p.Item1, p => p.Item2), keepBrackets);

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}. Born {date:yyyyMMdd}" => "His name is Ryan. Born 20170927".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator. First parameter is the token, seconde any suffix.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, params Tuple<string, string>[] tokensPair) => TokensReplace(value, tokensPair.ToDictionary(p => p.Item1, p => p.Item2), false);

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}. Born {date:yyyyMMdd}" => "His name is Ryan. Born 20170927".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator. First parameter is the token, seconde any suffix.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, Dictionary<string, Func<string>> tokenGetters) => TokensReplace(value, tokenGetters, false);

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}." => "His name is Ryan.".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, Dictionary<string, Func<string>> tokenGetters, bool keepBrackets)
    {
        return tokenMatcher.Replace(value, match => string.Format("{0}{1}{2}{3}",
            keepBrackets ? "{" : string.Empty,
            tokenGetters.GetOrFallback(match.Groups[1].Value, () => match.Groups[1].Value).Invoke(),
            !string.IsNullOrEmpty(match.Groups[2].Value) ? ":" + match.Groups[2].Value : string.Empty,
            keepBrackets ? "}" : string.Empty));
    }

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}. Born {date:yyyyMMdd}" => "His name is Ryan. Born 20170927".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator. First parameter is the token, seconde any suffix.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, dynamic param) => TokensReplace(value, (dynamic)param, false);

    /// <summary>
    /// Replce all the tokens in the string in the format "His name is {name}." => "His name is Ryan.".
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tokenEvaluator">The token evaluator.</param>
    /// <returns></returns>
    public static string TokensReplace(this string value, dynamic param, bool keepBrackets)
    {
        var type = param.GetType();
        return tokenMatcher.Replace(value, match => string.Format("{0}{1}{2}{3}",
            keepBrackets ? "{" : string.Empty,
            type.GetProperty(match.Groups[1].Value).GetValue(param, null),
            !string.IsNullOrEmpty(match.Groups[2].Value) ? ":" + match.Groups[2].Value : string.Empty,
            keepBrackets ? "}" : string.Empty));
    }


    /// <summary>
    /// Wraps the with rich text tag.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tag">The tag.</param>
    /// <param name="tagValue">The tag value.</param>
    /// <returns></returns>
    public static string WrapWithRichTextTag(this string value, string tag) => WrapWithRichTextTag(value, tag, null);

    /// <summary>
    /// Wraps the with rich text tag.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="tag">The tag.</param>
    /// <param name="tagValue">The tag value.</param>
    /// <returns></returns>
    public static string WrapWithRichTextTag(this string value, string tag, string tagValue)
    {
        var template = string.IsNullOrEmpty(tagValue) ? "<{0}>{1}</{0}>" : "<{0}={2}>{1}</{0}>";
        return string.Format(template, 
            tag,
            value,
            tagValue);
    }


    /// <summary>
    /// Determines if a string has a valid email address structure.
    /// </summary>
    /// <param name="value">String to be tested</param>
    /// <returns>'True' if is a valid address, otherwise 'False'</returns>
    public static bool IsValidEmailAddress(this string value)
    {
        return mailMatcher.IsMatch(value);
    }

    /// <summary>
    /// Determines if a string has a valid IPv4 address structure.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static bool IsValidIPv4Address(this string value)
    {
        return ipv4Matcher.IsMatch(value);
    }

    /// <summary>
    /// Determines if a string has a valid IPv6 address structure.
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static bool IsValidIPv6Address(this string value)
    {
        return ipv6Matcher.IsMatch(value);
    }


    /// <summary>
    /// Format an string to a pretty displayable form (as Unity label).
    /// </summary>
    /// <param name="value">Compact string</param>
    /// <param name="preserveAcronyms">Will detect sequential capital letters and leave them without space</param>
    public static string AsLabel(this string value, bool preserveAcronyms = true)
    {
        if (string.IsNullOrEmpty(value))
        {
            return string.Empty;
        }

        var length = value.Length;
        var newText = new StringBuilder(length * 2);

        newText.Append(value[0]);
        for (int i = 1; i < length; i++)
        {
            if (char.IsUpper(value[i]))
            {
                if ((value[i - 1] != ' ' && !char.IsUpper(value[i - 1])) ||
                    (preserveAcronyms && char.IsUpper(value[i - 1]) &&
                     i < value.Length - 1 && !char.IsUpper(value[i + 1])))
                {
                    newText.Append(' ');
                }
            }
            newText.Append(value[i]);
        }

        return newText.ToString();
    }

    /// <summary>
    /// Modify to lower an specific char.
    /// </summary>
    public static string ToLower(this string value, int index)
    {
        var sb = new StringBuilder();
        if (index > 0)
        {
            sb.Append(value.Substring(0, 0));
        }
        sb.Append(char.ToLower(value[index]));
        if (index < value.Length - 1)
        {
            sb.Append(value.Substring(index + 1));
        }
        return sb.ToString();
    }

    /// <summary>
    /// Count lines of an string.
    /// </summary>
    public static int Lines(this string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return 0;
        }

        int index = -1, count = 0;
        while ((index = value.IndexOf('\n', index + 1)) != -1)
        {
            count++;
        }
        return count + 1;
    }

    /// <summary>
    /// Count all words in a given string.
    /// </summary>
    public static int WordCount(this string value)
    {
        var count = -1;
        try
        {
            // Exclude whitespaces, Tabs and line breaks
            var matches = wordMatcher.Matches(value);
            count = matches.Count;
        }
        catch { }
        return count;
    }

    /// <summary>
    /// Returns a copy of the input string, with its characters reversed.
    /// </summary>
    public static string Reverse(this string value)
    {
        var charArray = value.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }

    /// <summary>
    /// Trims the string with all the ocurrences of the exact string.
    /// </summary>
    public static string TrimStart(this string value, string trimString)
    {
        var result = value;
        while (result.StartsWith(trimString))
        {
            result = result.Substring(trimString.Length);
        }
        return result;
    }

    /// <summary>
    /// Trims the string with all the ocurrences of the exact string.
    /// </summary>
    public static string TrimEnd(this string value, string trimString)
    {
        var result = value;
        while (result.EndsWith(trimString))
        {
            result = result.Substring(0, result.Length - trimString.Length);
        }
        return result;
    }

    /// <summary>
    /// Split an string in parts of length.
    /// </summary>
    public static string[] SplitInParts(this string value, int partLength)
    {
        if (value == null)
        {
            throw new ArgumentNullException("value");
        }

        if (partLength <= 0)
        {
            throw new ArgumentException("Part length has to be positive.", "partLength");
        }

        var parts = new string[Mathf.CeilToInt(value.Length / partLength)];
        for (int i = 0, p = 0; p < value.Length; i++, p += partLength)
        {
            parts[i] = value.Substring(p, Math.Min(partLength, value.Length - p));
        }

        return parts;
    }

    /// <summary>
    /// Gets the until or empty.
    /// </summary>
    /// <param name="text">The text.</param>
    /// <param name="stopAt">The stop at.</param>
    /// <returns></returns>
    public static string GetUntilOrEmpty(this string value, string stopAt)
    {
        if (!string.IsNullOrWhiteSpace(value))
        {
            var charLocation = value.IndexOf(stopAt, StringComparison.Ordinal);
            if (charLocation > 0)
            {
                return value.Substring(0, charLocation);
            }
        }
        return string.Empty;
    }
    
    /// <summary>
    /// Safely cuts a string.
    /// </summary>
    /// <param name="orig">The original.</param>
    /// <param name="length">The length.</param>
    /// <returns></returns>
    public static string SafeSubstring(this string orig, int length)
    {
        return orig.Substring(0, orig.Length >= length ? length : orig.Length);
    }


    /// <summary>
    /// Compress a string using GZIP, and returns a string.
    /// </summary>
    /// <returns>Compressed string</returns>
    public static string ZipToString(string text)
    {
        var buffer = Encoding.Unicode.GetBytes(text);
        var ms = new MemoryStream();
        using (var zip = new GZipStream(ms, CompressionLevel.Optimal, true))
        {
            zip.Write(buffer, 0, buffer.Length);
        }

        var compressed = new byte[ms.Length];
        ms.Position = 0;
        ms.Read(compressed, 0, compressed.Length);
        var gzBuffer = new byte[compressed.Length + 4];

        Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
        Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);

        return Convert.ToBase64String(gzBuffer);
    }

    /// <summary>
    /// Compress a string using GZIP, and returns a byte array.
    /// </summary>
    /// <returns>Compressed string</returns>
    public static byte[] ZipToByte(string text)
    {
        var buffer = Encoding.Unicode.GetBytes(text);
        MemoryStream ms = new MemoryStream();
        using (var zip = new GZipStream(ms, CompressionLevel.Optimal, true))
        {
            zip.Write(buffer, 0, buffer.Length);
        }

        var compressed = new byte[ms.Length];
        ms.Position = 0;
        ms.Read(compressed, 0, compressed.Length);
        var gzBuffer = new byte[compressed.Length + 4];

        Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
        Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4);

        return gzBuffer;
    }

    /// <summary>
    /// Uncompress a compressed string, and returns a string.
    /// </summary>
    /// <returns>Uncompressed string</returns>
    public static string UnZip(string compressedText)
    {
        var gzBuffer = Convert.FromBase64String(compressedText);
        using (var ms = new MemoryStream())
        {
            var msgLength = BitConverter.ToInt32(gzBuffer, 0);
            ms.Write(gzBuffer, 4, gzBuffer.Length - 4);
            ms.Position = 0;

            var buffer = new byte[msgLength];
            using (var zip = new GZipStream(ms, CompressionMode.Decompress))
            {
                zip.Read(buffer, 0, buffer.Length);
            }

            return Encoding.Unicode.GetString(buffer, 0, buffer.Length);
        }
    }

    /// <summary>
    /// Uncompress a compressed string as byte array, and returns a string.
    /// </summary>
    /// <returns>Uncompressed string</returns>
    public static string UnZip(byte[] gzBuffer)
    {
        using (var ms = new MemoryStream())
        {
            var msgLength = BitConverter.ToInt32(gzBuffer, 0);
            ms.Write(gzBuffer, 4, gzBuffer.Length - 4);
            ms.Position = 0;

            var buffer = new byte[msgLength];
            using (var zip = new GZipStream(ms, CompressionMode.Decompress))
            {
                zip.Read(buffer, 0, buffer.Length);
            }

            return Encoding.Unicode.GetString(buffer, 0, buffer.Length);
        }
    }
}
