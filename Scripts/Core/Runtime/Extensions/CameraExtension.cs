﻿using UnityEngine;

using CEUtilities;
using CEUtilities.Helpers;

public static class CameraExtension
{
    // Temporary variable to avoid GC allocation
    private static Vector3[] mSides = new Vector3[4];


    /// <summary>
    /// Get mid point of every edge of the frustrum plane relative to the specified camera at half distance between near and far plane. 
    /// The order is left, top, right, bottom.
    /// </summary>
    public static Vector3[] GetWorldSides(this Camera cam)
    {
        return cam.GetWorldSides(Mathf.Lerp(cam.nearClipPlane, cam.farClipPlane, 0.5f), null);
    }

    /// <summary>
    /// Get mid point of every edge of the frustrum plane relative to the specified camera. 
    /// The order is left, top, right, bottom.
    /// </summary>
    public static Vector3[] GetWorldSides(this Camera cam, float depth)
    {
        return cam.GetWorldSides(depth, null);
    }

    /// <summary>
    /// Get mid point of every edge of the frustrum plane relative to the specified camera. 
    /// The order is left, top, right, bottom.
    /// </summary>
    public static Vector3[] GetWorldSides(this Camera cam, Transform relativeTo)
    {
        return cam.GetWorldSides(Mathf.Lerp(cam.nearClipPlane, cam.farClipPlane, 0.5f), relativeTo);
    }

    /// <summary>
    /// Get mid point of every edge of the frustrum plane relative to the specified camera. 
    /// The order is left, top, right, bottom.
    /// </summary>
    public static Vector3[] GetWorldSides(this Camera cam, float depth, Transform relativeTo)
    {
        // Orto
#if UNITY_4_3 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7
		if (cam.isOrthoGraphic)
#else
        if (cam.orthographic)
#endif
        {
            float os = cam.orthographicSize;
            float x0 = -os;
            float x1 = os;
            float y0 = -os;
            float y1 = os;

            Rect rect = cam.rect;
            Vector2 size = UIMathHelper.ScreenSize;

            float aspect = size.x / size.y;
            aspect *= rect.width / rect.height;
            x0 *= aspect;
            x1 *= aspect;

            // We want to ignore the scale, as scale doesn't affect the camera's view region in Unity
            Transform t = cam.transform;
            Quaternion rot = t.rotation;
            Vector3 pos = t.position;

            int w = Mathf.RoundToInt(size.x);
            int h = Mathf.RoundToInt(size.y);

            if ((w & 1) == 1) pos.x -= 1f / size.x;
            if ((h & 1) == 1) pos.y += 1f / size.y;

            mSides[0] = rot * (new Vector3(x0, 0f, depth)) + pos;
            mSides[1] = rot * (new Vector3(0f, y1, depth)) + pos;
            mSides[2] = rot * (new Vector3(x1, 0f, depth)) + pos;
            mSides[3] = rot * (new Vector3(0f, y0, depth)) + pos;
        }
        // Perspective
        else
        {
            mSides[0] = cam.ViewportToWorldPoint(new Vector3(0f, 0.5f, depth));
            mSides[1] = cam.ViewportToWorldPoint(new Vector3(0.5f, 1f, depth));
            mSides[2] = cam.ViewportToWorldPoint(new Vector3(1f, 0.5f, depth));
            mSides[3] = cam.ViewportToWorldPoint(new Vector3(0.5f, 0f, depth));
        }

        // Change space
        if (relativeTo != null)
        {
            for (int i = 0; i < 4; ++i)
            {
                mSides[i] = relativeTo.InverseTransformPoint(mSides[i]);
            }
        }

        return mSides;
    }

    /// <summary>
    /// Get the camera's world-space corners. 
    /// The order is bottom-left, top-left, top-right, bottom-right.
    /// </summary>
    public static Vector3[] GetWorldCorners(this Camera cam)
    {
        return cam.GetWorldCorners(Mathf.Lerp(cam.nearClipPlane, cam.farClipPlane, 0.5f), null);
    }

    /// <summary>
    /// Get the camera's world-space corners. 
    /// The order is bottom-left, top-left, top-right, bottom-right.
    /// </summary>
    public static Vector3[] GetWorldCorners(this Camera cam, float depth)
    {
        return cam.GetWorldCorners(depth, null);
    }

    /// <summary>
    /// Get the camera's world-space corners. 
    /// The order is bottom-left, top-left, top-right, bottom-right.
    /// </summary>
    public static Vector3[] GetWorldCorners(this Camera cam, Transform relativeTo)
    {
        return cam.GetWorldCorners(Mathf.Lerp(cam.nearClipPlane, cam.farClipPlane, 0.5f), relativeTo);
    }

    /// <summary>
    /// Get the camera's world-space corners. 
    /// The order is bottom-left, top-left, top-right, bottom-right.
    /// </summary>
    public static Vector3[] GetWorldCorners(this Camera cam, float depth, Transform relativeTo)
    {
        // Orto
#if UNITY_4_3 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7
		if (cam.isOrthoGraphic)
#else
        if (cam.orthographic)
#endif
        {
            float os = cam.orthographicSize;
            float x0 = -os;
            float x1 = os;
            float y0 = -os;
            float y1 = os;

            Rect rect = cam.rect;
            Vector2 size = UIMathHelper.ScreenSize;
            float aspect = size.x / size.y;
            aspect *= rect.width / rect.height;
            x0 *= aspect;
            x1 *= aspect;

            // We want to ignore the scale, as scale doesn't affect the camera's view region in Unity
            Transform t = cam.transform;
            Quaternion rot = t.rotation;
            Vector3 pos = t.position;

            mSides[0] = rot * (new Vector3(x0, y0, depth)) + pos;
            mSides[1] = rot * (new Vector3(x0, y1, depth)) + pos;
            mSides[2] = rot * (new Vector3(x1, y1, depth)) + pos;
            mSides[3] = rot * (new Vector3(x1, y0, depth)) + pos;
        }
        // Perspective
        else
        {
            mSides[0] = cam.ViewportToWorldPoint(new Vector3(0f, 0f, depth));
            mSides[1] = cam.ViewportToWorldPoint(new Vector3(0f, 1f, depth));
            mSides[2] = cam.ViewportToWorldPoint(new Vector3(1f, 1f, depth));
            mSides[3] = cam.ViewportToWorldPoint(new Vector3(1f, 0f, depth));
        }

        // Change space
        if (relativeTo != null)
        {
            for (int i = 0; i < 4; ++i)
            {
                mSides[i] = relativeTo.InverseTransformPoint(mSides[i]);
            }
        }

        return mSides;
    }

    /// <summary>
    /// Render the camera into a render texture. 
    /// Should be call after camera in redered in the pipeline.
    /// </summary>
    public static void ToRenderTexture(this Camera cam, ref RenderTexture renderTexture)
    {
        cam.ToRenderTexture(cam.cullingMask, ref renderTexture);
    }

    /// <summary>
    /// Render the camera into a render texture. 
    /// Should be call after camera in redered in the pipeline.
    /// </summary>
    public static void ToRenderTexture(this Camera cam, LayerMask cullingMask, ref RenderTexture renderTexture)
    {
        // Prepare render texture
        if (renderTexture == null ||
            renderTexture.width != Screen.width ||
            renderTexture.height != Screen.height)
        {
            if (renderTexture != null)
            {
                Utilities.Destroy(renderTexture);
            }

            renderTexture = new RenderTexture(Screen.width, Screen.height, 24);
        }

        // Render to rendertexture
        var prevLayermask = cam.cullingMask;
        cam.targetTexture = renderTexture;
        cam.cullingMask = cullingMask.value;
        cam.Render();

        // Restore
        cam.targetTexture = null;
        cam.cullingMask = prevLayermask;
    }

    /// <summary>
    /// Render the camera into a texture 2d. 
    /// Should be call after camera in redered in the pipeline.
    /// </summary>
    public static void ToTexture2D(this Camera cam, ref Texture2D tex)
    {
        cam.ToTexture2D(cam.cullingMask, ref tex);
    }

    /// <summary>
    /// Render the camera into a texture 2D. 
    /// Should be call after camera in redered in the pipeline.
    /// </summary>
    public static void ToTexture2D(this Camera cam, LayerMask cullingMask, ref Texture2D tex)
    {
        int width = Screen.width, height = Screen.height;

        // Prepare texture
        if (tex == null ||
            tex.width != Screen.width ||
            tex.height != Screen.height)
        {
            if (tex != null)
            {
                tex.Resize(width, height, TextureFormat.ARGB32, false);
            }
            else
            {
                tex = new Texture2D(width, height, TextureFormat.ARGB32, false);
            }
        }

        // Create temporary Render Texture
        var rt = RenderTexture.GetTemporary(width, height, 24);
        cam.ToRenderTexture(cullingMask, ref rt);

        // Read rt into tex
        var activeRT = RenderTexture.active;
        RenderTexture.active = rt;
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        tex.Apply();

        // Release temporary
        RenderTexture.ReleaseTemporary(rt);
        RenderTexture.active = activeRT;
    }

    /// <summary>
    /// Checks if an object is visible by the camera.
    /// </summary>
    public static bool IsObjectVisible(this Camera cam, Renderer renderer)
    {
        return GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(cam), renderer.bounds);
    }

    /// <summary>
    /// Checks if an object is visible by the camera.
    /// </summary>
    public static bool IsObjectVisible(this Camera cam, GameObject go, bool includeChilds)
    {
        var visible = false;
        if (!includeChilds)
        {
            var renderer = go.GetComponent<Renderer>();
            if (renderer == null)
            {
                Debug.LogFormat("The game object '{0}' has no Renderer to check visibility.", go, go);
            }
            else
            {
                visible = cam.IsObjectVisible(renderer);
            }
        }
        else
        {
            var renderers = go.GetComponentsInChildren<Renderer>();
            foreach (var renderer in renderers)
            {
                if (cam.IsObjectVisible(renderer))
                {
                    visible = true;
                    break;
                }
            }
        }
        return visible;
    }
}