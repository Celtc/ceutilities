﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public static class GraphicExtension
{
    /// <summary>
    /// Verify if the texture is readable.
    /// </summary>
    public static bool IsReadable(this Graphic source)
    {
        return source.mainTexture.IsReadable();
    }


    /// <summary>
    /// Fade out the graphic, alpha from current value to 0.
    /// </summary>
    public static IEnumerator FadeOut(this Graphic source, float duration)
    {
        yield return source.FadeOut(duration, source.color.a);
    }

    /// <summary>
    /// Fade out the graphic, alpha parting from value to 0.
    /// </summary>
    public static IEnumerator FadeOut(this Graphic source, float duration, float startAlpha)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        var color = source.color;
        while (current - start < duration)
        {
            color.a = (1 - (current - start) / duration) * startAlpha;
            source.color = color;
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        color.a = 0;
        source.color = color;
    }


    /// <summary>
    /// Fade in the graphic, alpha from current value to 1.
    /// </summary>
    public static IEnumerator FadeIn(this Graphic source, float duration)
    {
        yield return source.FadeIn(duration, source.color.a);
    }

    /// <summary>
    /// Fade in the graphic, alpha parting from value to 1.
    /// </summary>
    public static IEnumerator FadeIn(this Graphic source, float duration, float endAlpha)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        var color = source.color;
        while (current - start < duration)
        {
            color.a = (current - start) / duration * endAlpha;
            source.color = color;
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        color.a = endAlpha;
        source.color = color;
    }


    /// <summary>
    /// Fade the graphic using an animation curve.
    /// </summary>
    public static IEnumerator Fade(this Graphic source, float duration, AnimationCurve curve)
    {
        var start = Time.realtimeSinceStartup;
        var current = start;
        var color = source.color;
        while (current - start < duration)
        {
            color.a = curve.Evaluate((current - start) / duration);
            source.color = color;
            yield return null;
            current = Time.realtimeSinceStartup;
        }
        color.a = curve.Evaluate(1f);
        source.color = color;
    }


    /// <summary>
    /// Tween the graphics color.
    /// </summary>
    public static IEnumerator TweenColor(this Graphic source, Color targetColor, float duration)
    {
        yield return TweenColor(source, targetColor, duration, false);
    }

    /// <summary>
    /// Tween the graphics color.
    /// </summary>
    public static IEnumerator TweenColor(this Graphic source, Color targetColor, float duration, bool ignoreTimeScale)
    {
        if (Mathf.Approximately(duration, 0f))
        {
            source.color = targetColor;
            yield break;
        }

        var sourceColor = source.color;
        var timer = 0f;
        while (timer <= duration)
        {
            source.color = Color.Lerp(sourceColor, targetColor, timer / duration);
            timer += ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
            yield return null;
        }
        source.color = targetColor;
    }


    /// <summary>
    /// Tweens the anchored position of the graphic.
    /// </summary>
    public static IEnumerator TweenPosition(this Graphic source, Vector2 anchoredStart, Vector2 anchoredEnd, float duration)
    {
        yield return source.rectTransform.TweenPosition(anchoredStart, anchoredEnd, duration);
    }

    /// <summary>
    /// Tweens the anchored position of the graphic using an animation curve
    /// </summary>
    public static IEnumerator TweenPosition(this Graphic source, Vector2 anchoredStart, Vector2 anchoredEnd, float duration, AnimationCurve curve)
    {
        yield return source.rectTransform.TweenPosition(anchoredStart, anchoredEnd, duration, curve);
    }


    /// <summary>
    /// Tweens the size of the graphic.
    /// </summary>
    public static IEnumerator TweenSize(this Graphic source, Vector2 size, float duration)
    {
        yield return source.rectTransform.TweenSize(size, duration);
    }

    /// <summary>
    /// Tweens the size of the graphic using an animation curve
    /// </summary>
    public static IEnumerator TweenPosition(this Graphic source, Vector2 size, float duration, AnimationCurve curve)
    {
        yield return source.rectTransform.TweenSize(size, duration, curve);
    }
}
