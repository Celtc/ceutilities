﻿using UnityEngine;
using System;
using System.Collections;

public static class Vector2IntExtension
{
    /// <summary>
    /// Is the point inside an area?
    /// </summary>
    /// <param name="x1">Min x</param>
    /// <param name="x2">Max x</param>
    /// <param name="y1">Min y</param>
    /// <param name="y2">MAx y</param>
    public static bool Inside(this Vector2Int s, int x1, int x2, int y1, int y2)
    {
        return s.x >= x1 && s.x <= x2 && s.y >= y1 && s.y <= y2;
    }

    /// <summary>
    /// Return a perpendicular vector (90 degrees rotation).
    /// </summary>
    public static Vector2Int Perpendicular(this Vector2Int v)
    {
        return new Vector2Int(-v.y, v.x);
    }

    /// <summary>
    /// Return a perpendicular vector (-90 degrees rotation).
    /// </summary>
    /// <param name="v"></param>
    public static Vector2Int PerpendicularRight(this Vector2Int v)
    {
        return new Vector2Int(v.y, -v.x);
    }


    /// <summary>
    /// Returns a copy of a vector with a new X field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="column">X field of the new vector</param>
    public static Vector2Int WithX(this Vector2Int v, int x)
    {
        return new Vector2Int(x, v.y);
    }

    /// <summary>
    /// Returns a copy of a vector with a new Y field.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="row">Y field of the new vector</param>
    public static Vector2Int WithY(this Vector2Int v, int y)
    {
        return new Vector2Int(v.x, y);
    }

    /// <summary>
    /// Returns a copy of a vector with the X field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the X field</param>
    public static Vector2Int AddX(this Vector2Int v, int delta)
    {
        return new Vector2Int(v.x + delta, v.y);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field changed by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">Difference in the Y field</param>
    public static Vector2Int AddY(this Vector2Int v, int delta)
    {
        return new Vector2Int(v.x, v.y + delta);
    }

    /// <summary>
    /// Returns a copy of a vector with the X field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor for the X field</param>
    public static Vector2Int MultiplyX(this Vector2Int v, int delta)
    {
        return new Vector2Int(v.x * delta, v.y);
    }

    /// <summary>
    /// Returns a copy of a vector with the Y field multiplied by delta.
    /// </summary>
    /// <param name="v">Original vector</param>
    /// <param name="delta">New factor forthe Y field</param>
    public static Vector2Int MultiplyY(this Vector2Int v, int delta)
    {
        return new Vector2Int(v.x, v.y * delta);
    }

    /// <summary>
    /// Multiply components between themself (x*x, y*y).
    /// </summary>
    public static Vector2Int MultiplyCompWise(this Vector2Int a, Vector2Int b)
    {
        return new Vector2Int(a.x * b.x, a.y * b.y);
    }

    /// <summary>
    /// Returns a Vector3 from a Vector2Int by adding a Z field.
    /// </summary>
    /// <param name="v"></param>
    /// <param name="z">Optional 'z' parameter</param>
    public static Vector3Int ToVector3Int(this Vector2Int v, int z = 0)
    {
        return new Vector3Int(v.x, v.y, z);
    }

    /// <summary>
    /// Create a Tuple from the Vector2.
    /// </summary>
    public static Tuple<int, int> ToTuple(this Vector2Int v)
    {
        return new Tuple<int, int>(v.x, v.y);
    }
}
