﻿using UnityEngine;
using System;
using System.Reflection;

public static class ComponentExtension
{
    /// <summary>
    /// Copy a component to another game object.
    /// </summary>
    public static void CopyTo(this Component sourceComp, GameObject target, bool copyProperties = false)
    {
        var targetComp = target.AddComponent(sourceComp.GetType());

        // Properties
        if (copyProperties)
        {
            var sourceProperties = sourceComp.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            for (int i = 0; i < sourceProperties.Length; i++)
            {
                var value = sourceProperties[i].GetValue(sourceComp);
                sourceProperties[i].SetValue(targetComp, value);
            }
        }

        // Fields
        var sourceFields = sourceComp.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        for (int i = 0; i < sourceFields.Length; i++)
        {
            var value = sourceFields[i].GetValue(sourceComp);
            sourceFields[i].SetValue(targetComp, value);
        }
    }

    /// <summary>
    /// Does the game object have the component.
    /// </summary>
    public static bool HasComponent<T>(this Component component)
    {
        return component.gameObject.HasComponent<T>();
    }

    /// <summary>
    /// Gets or add a component. Usage example:
    /// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
    /// </summary>
    public static T GetOrAddComponent<T>(this Component component) where T : Component
    {
        return component.gameObject.GetOrAddComponent<T>();
    }

    /// <summary>
    /// Gets or add a component. Usage example:
    /// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
    /// </summary>
    public static Component GetOrAddComponent(this Component component, Type componentType)
    {
        return component.gameObject.GetOrAddComponent(componentType);
    }

    /// <summary>
    /// Extension for the game object that checks to see if the component already exists before adding a new one.
    /// If the component is already present it will be returned instead.
    /// </summary>
    public static T AddMissingComponent<T>(this Component component) where T : Component
    {
        return component.gameObject.AddMissingComponent<T>();
    }

    /// <summary>
    /// Extension for the game object that checks to see if the component already exists before adding a new one.
    /// If the component is already present it will be returned instead.
    /// </summary>
    public static Component AddMissingComponent(this Component component, Type componentType)
    {
        return component.gameObject.AddMissingComponent(componentType);
    }
}