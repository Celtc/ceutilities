﻿using UnityEngine;
using System.Text;
using System.Globalization;
using System.Collections;
using System.Collections.Generic;

public static class MeshExtension
{
    #region Sub Classes

    /// <summary>
    /// Auxiliar class for extracting and storing vertices data
    /// </summary>
    private class Vertices
    {
        List<Vector3> verts = null;
        List<Vector2> uv1 = null;
        List<Vector2> uv2 = null;
        List<Vector2> uv3 = null;
        List<Vector2> uv4 = null;
        List<Vector3> normals = null;
        List<Vector4> tangents = null;
        List<Color> colors = null;
        List<Color32> colors32 = null;
        List<BoneWeight> boneWeights = null;
        List<Matrix4x4> bindposes = null;

        public Vertices()
        {
            verts = new List<Vector3>();
        }
        public Vertices(Mesh aMesh)
        {
            verts = CreateList(aMesh.vertices);
            uv1 = CreateList(aMesh.uv);
            uv2 = CreateList(aMesh.uv2);
            uv3 = CreateList(aMesh.uv3);
            uv4 = CreateList(aMesh.uv4);
            normals = CreateList(aMesh.normals);
            tangents = CreateList(aMesh.tangents);
            colors = CreateList(aMesh.colors);
            colors32 = CreateList(aMesh.colors32);
            boneWeights = CreateList(aMesh.boneWeights);
            bindposes = CreateList(aMesh.bindposes);
        }

        private List<T> CreateList<T>(T[] aSource)
        {
            if (aSource == null || aSource.Length == 0)
                return null;
            return new List<T>(aSource);
        }
        private void Copy<T>(ref List<T> aDest, List<T> aSource, int aIndex)
        {
            if (aSource == null)
                return;
            if (aDest == null)
                aDest = new List<T>();
            aDest.Add(aSource[aIndex]);
        }
        public int Add(Vertices aOther, int aIndex)
        {
            int i = verts.Count;
            Copy(ref verts, aOther.verts, aIndex);
            Copy(ref uv1, aOther.uv1, aIndex);
            Copy(ref uv2, aOther.uv2, aIndex);
            Copy(ref uv3, aOther.uv3, aIndex);
            Copy(ref uv4, aOther.uv4, aIndex);
            Copy(ref normals, aOther.normals, aIndex);
            Copy(ref tangents, aOther.tangents, aIndex);
            Copy(ref colors, aOther.colors, aIndex);
            Copy(ref colors32, aOther.colors32, aIndex);
            Copy(ref boneWeights, aOther.boneWeights, aIndex);
            Copy(ref bindposes, aOther.bindposes, aIndex);
            return i;
        }
        public void AssignTo(Mesh aTarget)
        {
            aTarget.SetVertices(verts);
            if (uv1 != null) aTarget.SetUVs(0, uv1);
            if (uv2 != null) aTarget.SetUVs(1, uv2);
            if (uv3 != null) aTarget.SetUVs(2, uv3);
            if (uv4 != null) aTarget.SetUVs(3, uv4);
            if (normals != null) aTarget.SetNormals(normals);
            if (tangents != null) aTarget.SetTangents(tangents);
            if (colors != null) aTarget.SetColors(colors);
            if (colors32 != null) aTarget.SetColors(colors32);
            if (boneWeights != null) aTarget.boneWeights = boneWeights.ToArray();
            if (bindposes != null) aTarget.bindposes = bindposes.ToArray();
        }
    }

    #endregion

    /// <summary>
    /// Method to combine meshes with severan submeshes and Materials, from a GameObject and children.
    /// If the GameObject does not have MeshFilter nor renderer, will add those.
    /// </summary>
    /// <param name="aGo">GameObject to process</param>
    public static Mesh CombineMeshes(this GameObject aGo)
    {
        MeshRenderer[] meshRenderers = aGo.GetComponentsInChildren<MeshRenderer>(false);
        int totalVertexCount = 0;
        int totalMeshCount = 0;

        if (meshRenderers != null && meshRenderers.Length > 0)
        {
            foreach (MeshRenderer meshRenderer in meshRenderers)
            {
                MeshFilter filter = meshRenderer.gameObject.GetComponent<MeshFilter>();
                if (filter != null && filter.sharedMesh != null)
                {
                    totalVertexCount += filter.sharedMesh.vertexCount;
                    totalMeshCount++;
                }
            }
        }

        if (totalMeshCount == 0)
        {
            Debug.Log("No meshes found in children. There's nothing to combine.");
            return null;
        }
        if (totalMeshCount == 1)
        {
            Debug.Log("Only 1 mesh found in children. There's nothing to combine.");
            return null;
        }
        if (totalVertexCount > 65535)
        {
            Debug.Log("There are too many vertices to combine into 1 mesh (" + totalVertexCount + "). The max. limit is 65535");
            return null;
        }

        Mesh mesh = new Mesh();
        Matrix4x4 myTransform = aGo.transform.worldToLocalMatrix;
        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        List<Vector2> uv1s = new List<Vector2>();
        List<Vector2> uv2s = new List<Vector2>();
        Dictionary<Material, List<int>> subMeshes = new Dictionary<Material, List<int>>();

        if (meshRenderers != null && meshRenderers.Length > 0)
        {
            foreach (MeshRenderer meshRenderer in meshRenderers)
            {
                MeshFilter filter = meshRenderer.gameObject.GetComponent<MeshFilter>();
                if (filter != null && filter.sharedMesh != null)
                {
                    MergeMeshInto(filter.sharedMesh, meshRenderer.sharedMaterials, myTransform * filter.transform.localToWorldMatrix, vertices, normals, uv1s, uv2s, subMeshes);
                    if (filter.gameObject != aGo)
                    {
                        filter.gameObject.SetActive(false);
                    }
                }
            }
        }

        mesh.vertices = vertices.ToArray();
        if (normals.Count > 0) mesh.normals = normals.ToArray();
        if (uv1s.Count > 0) mesh.uv = uv1s.ToArray();
        if (uv2s.Count > 0) mesh.uv2 = uv2s.ToArray();
        mesh.subMeshCount = subMeshes.Keys.Count;
        Material[] materials = new Material[subMeshes.Keys.Count];
        int mIdx = 0;
        foreach (Material m in subMeshes.Keys)
        {
            materials[mIdx] = m;
            mesh.SetTriangles(subMeshes[m].ToArray(), mIdx++);
        }

        if (meshRenderers != null && meshRenderers.Length > 0)
        {
            MeshRenderer meshRend = aGo.GetComponent<MeshRenderer>();
            if (meshRend == null) meshRend = aGo.AddComponent<MeshRenderer>();
            meshRend.sharedMaterials = materials;

            MeshFilter meshFilter = aGo.GetComponent<MeshFilter>();

            if (meshFilter == null)
                meshFilter = aGo.AddComponent<MeshFilter>();
#if UNITY_EDITOR
            UnityEditor.MeshUtility.Optimize(mesh);
#endif
            meshFilter.sharedMesh = mesh;
        }
        return mesh;
    }

    /// <summary>
    /// Helper method for CombineMeshes. 
    /// </summary>
    /// <param name="meshToMerge"></param>
    /// <param name="ms"></param>
    /// <param name="transformMatrix"></param>
    /// <param name="vertices"></param>
    /// <param name="normals"></param>
    /// <param name="uv1s"></param>
    /// <param name="uv2s"></param>
    /// <param name="subMeshes"></param>
    private static void MergeMeshInto(Mesh meshToMerge, Material[] ms, Matrix4x4 transformMatrix, List<Vector3> vertices, List<Vector3> normals, List<Vector2> uv1s, List<Vector2> uv2s, Dictionary<Material, List<int>> subMeshes)
    {
        if (meshToMerge == null) return;
        int vertexOffset = vertices.Count;
        Vector3[] vs = meshToMerge.vertices;

        for (int i = 0; i < vs.Length; i++)
        {
            vs[i] = transformMatrix.MultiplyPoint3x4(vs[i]);
        }
        vertices.AddRange(vs);

        Quaternion rotation = Quaternion.LookRotation(transformMatrix.GetColumn(2), transformMatrix.GetColumn(1));
        Vector3[] ns = meshToMerge.normals;
        if (ns != null && ns.Length > 0)
        {
            for (int i = 0; i < ns.Length; i++) ns[i] = rotation * ns[i];
            normals.AddRange(ns);
        }

        Vector2[] uvs = meshToMerge.uv;
        if (uvs != null && uvs.Length > 0) uv1s.AddRange(uvs);
        uvs = meshToMerge.uv2;
        if (uvs != null && uvs.Length > 0) uv2s.AddRange(uvs);

        for (int i = 0; i < ms.Length; i++)
        {
            if (i < meshToMerge.subMeshCount)
            {
                int[] ts = meshToMerge.GetTriangles(i);
                if (ts.Length > 0)
                {
                    if (ms[i] != null && !subMeshes.ContainsKey(ms[i]))
                    {
                        subMeshes.Add(ms[i], new List<int>());
                    }
                    List<int> subMesh = subMeshes[ms[i]];
                    for (int t = 0; t < ts.Length; t++)
                    {
                        ts[t] += vertexOffset;
                    }
                    subMesh.AddRange(ts);
                }
            }
        }
    }

    /// <summary>
    /// Extract a submesh from a mesh.
    /// </summary>
    public static Mesh GetSubmesh(this Mesh sourceMesh, int submeshIndex)
    {
        if (submeshIndex < 0 || submeshIndex >= sourceMesh.subMeshCount)
            return null;
        int[] indices = sourceMesh.GetTriangles(submeshIndex);
        Vertices source = new Vertices(sourceMesh);
        Vertices dest = new Vertices();
        Dictionary<int, int> map = new Dictionary<int, int>();
        int[] newIndices = new int[indices.Length];
        for (int i = 0; i < indices.Length; i++)
        {
            int o = indices[i];
            int n;
            if (!map.TryGetValue(o, out n))
            {
                n = dest.Add(source, o);
                map.Add(o, n);
            }
            newIndices[i] = n;
        }
        Mesh m = new Mesh();
        dest.AssignTo(m);
        m.triangles = newIndices;
        return m;
    }

    /// <summary>
    /// Do a deep copy of the mesh.
    /// </summary>
    public static void DeepCopy(this Mesh srcMesh, Mesh dstMesh)
    {
        dstMesh.Clear();
        if (srcMesh.vertices != null)
        {
            dstMesh.vertices = (Vector3[])srcMesh.vertices.Clone();
        }
        if (srcMesh.normals != null)
        {
            dstMesh.normals = (Vector3[])srcMesh.normals.Clone();
        }
        if (srcMesh.tangents != null)
        {
            dstMesh.tangents = (Vector4[])srcMesh.tangents.Clone();
        }
        if (srcMesh.uv != null)
        {
            dstMesh.uv = (Vector2[])srcMesh.uv.Clone();
        }
        if (srcMesh.uv2 != null)
        {
            dstMesh.uv2 = (Vector2[])srcMesh.uv2.Clone();
        }
        if (srcMesh.uv3 != null)
        {
            dstMesh.uv3 = (Vector2[])srcMesh.uv3.Clone();
        }
        if (srcMesh.uv4 != null)
        {
            dstMesh.uv4 = (Vector2[])srcMesh.uv4.Clone();
        }
        if (srcMesh.colors != null)
        {
            dstMesh.colors = (Color[])srcMesh.colors.Clone();
        }
        if (srcMesh.colors32 != null)
        {
            dstMesh.colors32 = (Color32[])srcMesh.colors32.Clone();
        }
        if (srcMesh.boneWeights != null)
        {
            dstMesh.boneWeights = (BoneWeight[])srcMesh.boneWeights.Clone();
        }
        if (srcMesh.bindposes != null)
        {
            dstMesh.bindposes = (Matrix4x4[])srcMesh.bindposes.Clone();
        }
        dstMesh.subMeshCount = srcMesh.subMeshCount;
        for (int i = 0; i < srcMesh.subMeshCount; i++)
        {
            int[] triangles = srcMesh.GetTriangles(i);
            dstMesh.SetTriangles(triangles, i);
        }
        dstMesh.bounds = srcMesh.bounds;
        dstMesh.name = srcMesh.name;
    }

    /// <summary>
    /// Create an OBJ string from the mesh.
    /// </summary>
    /// <param name="srcMesh">Source mesh</param>
    public static string ToOBJ(this Mesh srcMesh)
    {
        int startIndex = 0;
        return ToOBJ(srcMesh, null, ref startIndex, true, Vector3.zero, Vector3.one, Quaternion.identity, null);
    }
    
    /// <summary>
    /// Create an OBJ string from the mesh.
    /// </summary>
    /// <param name="srcMesh">Source mesh</param>
    /// <param name="name">Name of the object inside the OBJ</param>
    public static string ToOBJ(this Mesh srcMesh, string name)
    {
        int startIndex = 0;
        return ToOBJ(srcMesh, name, ref startIndex, true, Vector3.zero, Vector3.one, Quaternion.identity, null);
    }

    /// <summary>
    /// Create an OBJ string from the mesh.
    /// </summary>
    /// <param name="srcMesh">Source mesh</param>
    /// <param name="name">Name of the object inside the OBJ</param>
    /// <param name="startIndex">Starting index of the mesh inside the obj</param>
    public static string ToOBJ(this Mesh srcMesh, string name, ref int startIndex)
    {
        return ToOBJ(srcMesh, name, ref startIndex, true, Vector3.zero, Vector3.one, Quaternion.identity, null);
    }

    /// <summary>
    /// Create an OBJ string from the mesh.
    /// </summary>
    /// <param name="srcMesh">Source mesh</param>
    /// <param name="name">Name of the object inside the OBJ</param>
    /// <param name="startIndex">Starting index of the mesh inside the obj</param>
    /// <param name="addObjectHeader">Add an object header which allows to split into several objects inside the obj or merge all into one</param>
    public static string ToOBJ(this Mesh srcMesh, string name, ref int startIndex, bool addObjectHeader)
    {
        return ToOBJ(srcMesh, name, ref startIndex, addObjectHeader, Vector3.zero, Vector3.one, Quaternion.identity, null);
    }

    /// <summary>
    /// Create an OBJ string from the mesh.
    /// </summary>
    /// <param name="srcMesh">Source mesh</param>
    /// <param name="name">Name of the object inside the OBJ</param>
    /// <param name="startIndex">Starting index of the mesh inside the obj</param>
    /// <param name="addObjectHeader">Add an object header which allows to split into several objects inside the obj or merge all into one</param>
    /// <param name="position">Position of the mesh in world space</param>
    /// <param name="scale">Lossy scale of the mesh</param>
    /// <param name="rotation">Rotation of the mesh</param>
    /// <returns>An OBJ compatible string</returns>
    public static string ToOBJ(this Mesh srcMesh, string name, ref int startIndex, bool addObjectHeader, Vector3 position, Vector3 scale, Quaternion rotation)
    {
        return ToOBJ(srcMesh, name, ref startIndex, addObjectHeader, position, scale, rotation, null);
    }

    /// <summary>
    /// Create an OBJ string from the mesh.
    /// </summary>
    /// <param name="srcMesh">Source mesh</param>
    /// <param name="name">Name of the object inside the OBJ</param>
    /// <param name="startIndex">Starting index of the mesh inside the obj</param>
    /// <param name="addObjectHeader">Add an object header which allows to split into several objects inside the obj or merge all into one</param>
    /// <param name="position">Position of the mesh in world space</param>
    /// <param name="scale">Lossy scale of the mesh</param>
    /// <param name="rotation">Rotation of the mesh</param>
    /// <param name="materials">Referencing materials</param>
    /// <returns>An OBJ compatible string</returns>
    public static string ToOBJ(this Mesh srcMesh, string name, ref int startIndex, bool addObjectHeader, Vector3 position, Vector3 scale, Quaternion rotation, Material[] materials)
    {
        var sb = new StringBuilder();

        // Object sperator
        if (addObjectHeader)
        {
            sb.AppendLine("g " + name ?? srcMesh.name);
        }

        // Export the mesh
        var faceOrder = (int)Mathf.Clamp((scale.x * scale.z), -1, 1);

        // Vertices
        foreach (Vector3 rawVertex in srcMesh.vertices)
        {
            // Make a copy to be able to modify it
            var v = rawVertex;

            // Scale
            if (scale != Vector3.one)
            {
                v = v.MultiplyCompWise(scale);
            }

            // Rotation
            if (rotation != Quaternion.identity)
            {
                v = v.Rotate(rotation);
            }

            // Position
            if (position != Vector3.zero)
            {
                v += position;
            }

            // Add string line to output
            v.x *= -1;
            sb.Append("v ").Append(v.x.ToString(CultureInfo.InvariantCulture))
              .Append(" ").Append(v.y.ToString(CultureInfo.InvariantCulture))
              .Append(" ").AppendLine(v.z.ToString(CultureInfo.InvariantCulture));
        }

        // Normals
        foreach (Vector3 rawNormal in srcMesh.normals)
        {
            // Make a copy to be able to modify it
            Vector3 n = rawNormal;

            // Scale
            if (scale != Vector3.one)
            {
                n = n.MultiplyCompWise(scale.normalized);
            }

            // Rotation
            if (rotation != Quaternion.identity)
            {
                n = n.Rotate(rotation);
            }

            // Add string line to output
            n.x *= -1;
            sb.Append("vn ").Append(n.x.ToString(CultureInfo.InvariantCulture))
              .Append(" ").Append(n.y.ToString(CultureInfo.InvariantCulture))
              .Append(" ").AppendLine(n.z.ToString(CultureInfo.InvariantCulture));
        }

        // UV
        foreach (Vector2 rawUV in srcMesh.uv)
        {
            // Add string line to output
            sb.Append("vt ").Append(rawUV.x.ToString(CultureInfo.InvariantCulture))
              .Append(" ").AppendLine(rawUV.y.ToString(CultureInfo.InvariantCulture));
        }

        // Submeshes
        for (int j = 0; j < srcMesh.subMeshCount; j++)
        {
            // Get existing material
            if (materials != null && j < materials.Length)
            {
                sb.AppendLine("usemtl " + materials[j].name);
            }

            // Create a default one
            else
            {
                sb.AppendLine("usemtl " + name ?? srcMesh.name + "_sm" + j);
            }

            // Triangles
            var triangles = srcMesh.GetTriangles(j);
            for (int i = 0; i < triangles.Length; i += 3)
            {
                if (faceOrder < 0)
                {
                    sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
                    triangles[i] + 1 + startIndex, triangles[i + 1] + 1 + startIndex, triangles[i + 2] + 1 + startIndex));
                }
                else
                {
                    sb.Append(string.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}\n",
                    triangles[i + 2] + 1 + startIndex, triangles[i + 1] + 1 + startIndex, triangles[i] + 1 + startIndex));
                }
            }
        }

        // Increase index
        startIndex += srcMesh.vertices.Length;

        return sb.ToString();
    }
}