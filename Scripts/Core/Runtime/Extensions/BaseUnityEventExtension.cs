﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Assertions;

#if UNITY_EDITOR
using UnityEditor.Events;
#endif

namespace UnityEngine
{
    namespace Events
    {
        [System.Serializable]
        public class UnityEvent_Bool : UnityEvent<bool> { }
        [System.Serializable]
        public class UnityEvent_BoolArray : UnityEvent<bool[]> { }
        [System.Serializable]
        public class UnityEvent_Bool_String : UnityEvent<bool, string> { }
        [System.Serializable]
        public class UnityEvent_Bool_Bool : UnityEvent<bool, bool> { }
        [System.Serializable]
        public class UnityEvent_Int : UnityEvent<int> { }
        [System.Serializable]
        public class UnityEvent_NullableInt : UnityEvent<int?> { }
        [System.Serializable]
        public class UnityEvent_IntArray : UnityEvent<int[]> { }
        [System.Serializable]
        public class UnityEvent_Int_Int : UnityEvent<int, int> { }
        [System.Serializable]
        public class UnityEvent_Float : UnityEvent<float> { }
        [System.Serializable]
        public class UnityEvent_FloatArray : UnityEvent<float[]> { }
        [System.Serializable]
        public class UnityEvent_Vector2 : UnityEvent<Vector2> { }
        [System.Serializable]
        public class UnityEvent_Vector2Array : UnityEvent<Vector2[]> { }
        [System.Serializable]
        public class UnityEvent_Vector3 : UnityEvent<Vector3> { }
        [System.Serializable]
        public class UnityEvent_Vector3Array : UnityEvent<Vector3[]> { }
        [System.Serializable]
        public class UnityEvent_Quaternion : UnityEvent<Quaternion> { }
        [System.Serializable]
        public class UnityEvent_QuaternionArray : UnityEvent<Quaternion[]> { }
        [System.Serializable]
        public class UnityEvent_String : UnityEvent<string> { }
        [System.Serializable]
        public class UnityEvent_String_String : UnityEvent<string, string> { }
        [System.Serializable]
        public class UnityEvent_StringArray : UnityEvent<string[]> { }
        [System.Serializable]
        public class UnityEvent_Image : UnityEvent<Image> { }
        [System.Serializable]
        public class UnityEvent_ImageArray : UnityEvent<Image[]> { }
        [System.Serializable]
        public class UnityEvent_Sprite : UnityEvent<Sprite> { }
        [System.Serializable]
        public class UnityEvent_SpriteArray : UnityEvent<Sprite[]> { }
        [System.Serializable]
        public class UnityEvent_Texture2D : UnityEvent<Texture2D> { }
        [System.Serializable]
        public class UnityEvent_Texture2DArray : UnityEvent<Texture2D[]> { }
        [System.Serializable]
        public class UnityEvent_Object : UnityEvent<Object> { }
        [System.Serializable]
        public class UnityEvent_ObjectArray : UnityEvent<Object[]> { }
        [System.Serializable]
        public class UnityEvent_GameObject : UnityEvent<GameObject> { }
        [System.Serializable]
        public class UnityEvent_GameObjectArray : UnityEvent<GameObject[]> { }
        [System.Serializable]
        public class UnityEvent_GameObject_Float : UnityEvent<GameObject, float> { }
        [System.Serializable]
        public class UnityEvent_GameObject_Float_Float : UnityEvent<GameObject, float, float> { }
        [System.Serializable]
        public class UnityEvent_Collision2D : UnityEvent<Collision2D> { }
    }

    public static class UnityEventExtension
    {
        /// <summary>
        /// Add a listener with the possibility of executing the call just once.
        /// </summary>
        public static void AddListener(this UnityEvent ev, UnityAction call, bool runOnce)
        {
            if (runOnce)
            {
                UnityAction singleCall = null;
                singleCall = () =>
                {
                    call.Invoke();
                    ev.RemoveListener(singleCall);
                };
                ev.AddListener(singleCall);
            }
            else
            {
                ev.AddListener(call);
            }
        }

        /// <summary>
        /// Add a listener with the possibility of executing the call just once.
        /// </summary>
        public static void AddListener<T0>(this UnityEvent<T0> ev, UnityAction<T0> call, bool runOnce)
        {
            if (runOnce)
            {
                UnityAction<T0> singleCall = null;
                singleCall = (t0) =>
                {
                    call.Invoke(t0);
                    ev.RemoveListener(singleCall);
                };
                ev.AddListener(singleCall);
            }
            else
            {
                ev.AddListener(call);
            }
        }

        /// <summary>
        /// Add a listener with the possibility of executing the call just once.
        /// </summary>
        public static void AddListener<T0, T1>(this UnityEvent<T0, T1> ev, UnityAction<T0, T1> call, bool runOnce)
        {
            if (runOnce)
            {
                UnityAction<T0, T1> singleCall = null;
                singleCall = (t0, t1) =>
                {
                    call.Invoke(t0, t1);
                    ev.RemoveListener(singleCall);
                };
                ev.AddListener(singleCall);
            }
            else
            {
                ev.AddListener(call);
            }
        }

        /// <summary>
        /// Add a listener with the possibility of executing the call just once.
        /// </summary>
        public static void AddListener<T0, T1, T2>(this UnityEvent<T0, T1, T2> ev, UnityAction<T0, T1, T2> call, bool runOnce)
        {
            if (runOnce)
            {
                UnityAction<T0, T1, T2> singleCall = null;
                singleCall = (t0, t1, t2) =>
                {
                    call.Invoke(t0, t1, t2);
                    ev.RemoveListener(singleCall);
                };
                ev.AddListener(singleCall);
            }
            else
            {
                ev.AddListener(call);
            }
        }

        /// <summary>
        /// Add a listener with the possibility of executing the call just once.
        /// </summary>
        public static void AddListener<T0, T1, T2, T3>(this UnityEvent<T0, T1, T2, T3> ev, UnityAction<T0, T1, T2, T3> call, bool runOnce)
        {
            if (runOnce)
            {
                UnityAction<T0, T1, T2, T3> singleCall = null;
                singleCall = (t0, t1, t2, t3) =>
                {
                    call.Invoke(t0, t1, t2, t3);
                    ev.RemoveListener(singleCall);
                };
                ev.AddListener(singleCall);
            }
            else
            {
                ev.AddListener(call);
            }
        }

#if UNITY_EDITOR

        /// <summary>
        /// Add a persistent listemer
        /// </summary>
        public static void AddPersistentListener(this UnityEvent ev, UnityAction call, bool runOnce, UnityEventCallState eventCallState)
        {
            // Index number of the asset
            var index = ev.GetPersistentEventCount();

            if (runOnce)
            {
                // Add a wrapper to call once
                UnityAction singleCall = null;
                singleCall = () =>
                {
                    call.Invoke();
                    UnityEventTools.RemovePersistentListener(ev, index);
                };
                ev.AddListener(singleCall);
                ev.SetPersistentListenerState(index, eventCallState);
            }

            else
            {
                // Add persistent listener
                UnityEventTools.AddPersistentListener(ev, call);
                ev.SetPersistentListenerState(index, eventCallState);
            }
        }

        /// <summary>
        /// Add a persistent listemer
        /// </summary>
        public static void AddPersistentListener<T0>(this UnityEvent<T0> ev, UnityAction<T0> call, bool runOnce, UnityEventCallState eventCallState)
        {
            // Index number of the asset
            var index = ev.GetPersistentEventCount();

            if (runOnce)
            {
                // Add a wrapper to call once
                UnityAction<T0> singleCall = null;
                singleCall = (t0) =>
                {
                    call.Invoke(t0);
                    UnityEventTools.RemovePersistentListener(ev, index);
                };
                ev.AddListener(singleCall);
                ev.SetPersistentListenerState(index, eventCallState);
            }

            else
            {
                // Add persistent listener
                UnityEventTools.AddPersistentListener(ev, call);
                ev.SetPersistentListenerState(index, eventCallState);
            }
        }

        /// <summary>
        /// Add a persistent listemer
        /// </summary>
        public static void AddPersistentListener<T0, T1>(this UnityEvent<T0, T1> ev, UnityAction<T0, T1> call, bool runOnce, UnityEventCallState eventCallState)
        {
            // Index number of the asset
            var index = ev.GetPersistentEventCount();

            if (runOnce)
            {
                // Add a wrapper to call once
                UnityAction<T0, T1> singleCall = null;
                singleCall = (t0, t1) =>
                {
                    call.Invoke(t0, t1);
                    UnityEventTools.RemovePersistentListener(ev, index);
                };
                ev.AddListener(singleCall);
                ev.SetPersistentListenerState(index, eventCallState);
            }

            else
            {
                Assert.IsNull(ev, "ev parameter is null");
                Assert.IsNull(call, "call parameter is null");

                // Add persistent listener
                UnityEventTools.AddPersistentListener(ev, call);
                ev.SetPersistentListenerState(index, eventCallState);
            }
        }

        /// <summary>
        /// Remove an specific call from an event
        /// </summary>
        public static void RemovePersistentListener(this UnityEvent ev, UnityAction call)
        {
            UnityEventTools.RemovePersistentListener(ev, call);
        }

        /// <summary>
        /// Remove an specific call from an event
        /// </summary>
        public static void RemovePersistentListener<T0>(this UnityEvent<T0> ev, UnityAction<T0> call)
        {
            UnityEventTools.RemovePersistentListener(ev, call);
        }

        /// <summary>
        /// Remove an specific call from an event
        /// </summary>
        public static void RemovePersistentListener<T0, T1>(this UnityEvent<T0, T1> ev, UnityAction<T0, T1> call)
        {
            UnityEventTools.RemovePersistentListener(ev, call);
        }

#endif
    }
}


