﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.API
{
	public enum FTPCommand
	{
		Download,
		Upload,
		Delete,
		Rename,
		CreateDir,
		CheckDir,
		GetDateTimestamp,
		GetFileSize,
		Dir,
		DirDetails
	}
}