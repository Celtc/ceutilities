﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace CEUtilities.API
{
	public interface IFTPTask
	{
		int Hash
		{
			get;
		}

		string Host
		{
			get; set;
		}

		string User
		{
			get; set;
		}

		string Pass
		{
			get; set;
		}

		string RequestMethod
		{
			get;
		}

		string Message
		{
			get;
		}

		Exception Error
		{
			get;
		}

		bool HasError
		{
			get;
		}


		void Run();

		IEnumerator _RunAsync();
	}

	public abstract partial class FTPTask : IFTPTask
	{
		#region Exposed fields

		// Pre
		private string host;
		private string user;
		private string pass;
		private string requestMethod;

		// Post
		private string message;
		private Exception error;
		private bool hasError;

		#endregion Exposed fields

		#region Internal fields

		private int hash;
		private BackgroundWorker worker;

		private object _running = new object();
		private bool _ended = false;

		#endregion Internal fields

		#region Custom Events

		/// <summary>
		/// Called upon task completition
		/// </summary>
		public UnityEvent OnTaskCompleted = new UnityEvent();

		/// <summary>
		/// Called upon task fail
		/// </summary>
		public UnityEvent_String OnTaskFail = new UnityEvent_String();

		#endregion Custom Events

		#region Properties

		public string Host
		{
			get
			{
				return host;
			}

			set
			{
				host = value;
			}
		}

		public string User
		{
			get
			{
				return user;
			}

			set
			{
				user = value;
			}
		}

		public string Pass
		{
			get
			{
				return pass;
			}

			set
			{
				pass = value;
			}
		}

		public int Hash
		{
			get
			{
				return hash;
			}

			private set
			{
				hash = value;
			}
		}

		public string RequestMethod
		{
			get
			{
				return requestMethod;
			}

			protected set
			{
				requestMethod = value;
			}
		}


		public string Message
		{
			get
			{
				return message;
			}

			set
			{
				message = value;
			}
		}

		public Exception Error
		{
			get
			{
				return error;
			}

			set
			{
				error = value;
			}
		}

		public bool HasError
		{
			get
			{
				return hasError;
			}

			set
			{
				hasError = value;
			}
		}


		private BackgroundWorker Worker
		{
			get
			{
				return worker;
			}

			set
			{
				worker = value;
			}
		}

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		/// <summary>
		/// Create a new FTP Task
		/// </summary>
		protected FTPTask(string host, string user, string pass, string requestMethod)
		{
			Host = host;
			User = user;
			Pass = pass;

			Worker = new BackgroundWorker();
			Worker.DoWork += _RunTask;
			Worker.RunWorkerCompleted += _CompleteTask;

			RequestMethod = requestMethod;
			Hash = GetHashCode();
			HasError = false;
			Error = null;
		}

		/// <summary>
		/// Run the task
		/// </summary>
		public void Run()
		{
			Message = this.ToString();

			Worker.RunWorkerAsync(this);
		}

		/// <summary>
		/// Run the task
		/// </summary>
		public IEnumerator _RunAsync()
		{		
			Message = this.ToString();
					
			lock (_running)
			{
				Worker.RunWorkerAsync(this);

				yield return new WaitUntil(() => _ended);
			}
		}

		#region Operators override

		public override string ToString()
		{
			return string.Format("{0}: {1}", CommandToString(), ArgumentsToString());
		}

		public override bool Equals(object other)
		{
			return (other is IFTPTask) && Equals((IFTPTask)other);
		}

		public bool Equals(IFTPTask other)
		{
			return Hash == other.Hash;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		#endregion

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Printable string of the command
		/// </summary>
		protected string CommandToString()
		{
			switch (RequestMethod)
			{
				case WebRequestMethods.Ftp.AppendFile:
				return "AppendFile";
				case WebRequestMethods.Ftp.DeleteFile:
				return "DeleteFile";
				case WebRequestMethods.Ftp.DownloadFile:
				return "DownloadFile";
				case WebRequestMethods.Ftp.GetDateTimestamp:
				return "GetDateTimestamp";
				case WebRequestMethods.Ftp.GetFileSize:
				return "GetFileSize";
				case WebRequestMethods.Ftp.ListDirectory:
				return "ListDirectory";
				case WebRequestMethods.Ftp.ListDirectoryDetails:
				return "ListDirectoryDetails";
				case WebRequestMethods.Ftp.MakeDirectory:
				return "MakeDirectory";
				case WebRequestMethods.Ftp.PrintWorkingDirectory:
				return "PrintWorkingDirectory";
				case WebRequestMethods.Ftp.RemoveDirectory:
				return "RemoveDirectory";
				case WebRequestMethods.Ftp.Rename:
				return "Rename";
				case WebRequestMethods.Ftp.UploadFile:
				return "UploadFile";
				case WebRequestMethods.Ftp.UploadFileWithUniqueName:
				return "UploadFileWithUniqueName";
				default:
				return "Unknwon";
			}
		}

		/// <summary>
		/// Printable string of the argument
		/// </summary>
		protected abstract string ArgumentsToString();

		#region Worker methods

		/// <summary>
		/// Encapsulates the collections usage
		/// </summary>
		protected virtual void _RunTask(object sender, DoWorkEventArgs e)
		{
			_ended = false;

			Debug.Log("[FTP] Running task: " + this.ToString());
		}

		/// <summary>
		/// Encapsulates the collections usage
		/// </summary>
		protected virtual void _CompleteTask(object sender, RunWorkerCompletedEventArgs e)
		{
			_ended = true;

            if (HasError)
            {
                Debug.LogError(Message);
            }
            else
            {
                Debug.Log(Message);
            }
		}

		#endregion

		#endregion Non Public Methods
	}

	public abstract class FTPTask<Ti, To> : FTPTask
	{
		#region Exposed fields

		// Pre
		private Ti argument;

		// Post
		private To result;

		#endregion Exposed fields

		#region Internal fields

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		public Ti Argument
		{
			get
			{
				return argument;
			}

			internal set
			{
				argument = value;
			}
		}

		public To Result
		{
			get
			{
				return result;
			}

			internal set
			{
				result = value;
			}
		}

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		/// <summary>
		/// Create a new FTP Task
		/// </summary>
		protected FTPTask(string host, string user, string pass, string requestMethod, Ti argument) : base(host, user, pass, requestMethod)
		{
			Argument = argument;
		}

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Printable string of the argument
		/// </summary>
		protected override string ArgumentsToString()
		{
			return Argument.ToString();
		}

		#region Worker methods

		/// <summary>
		/// Encapsulates the collections usage
		/// </summary>
		protected override void _RunTask(object sender, DoWorkEventArgs e)
		{
			base._RunTask(sender, e);

			Error = _TaskWork(Argument, ref result);
			HasError = Error != null;
			Message += '\t' + (HasError ? ("Error: " + Error.ToString()) : "Success");
		}

		/// <summary>
		/// Encapsulates the collections usage
		/// </summary>
		protected override void _CompleteTask(object sender, RunWorkerCompletedEventArgs e)
		{
			base._CompleteTask(sender, e);
		}

		/// <summary>
		/// Main logic of the task
		/// </summary>
		protected abstract Exception _TaskWork(Ti argument, ref To result);

		#endregion

		#endregion Non Public Methods
	}

	public abstract class FTPTask<Ti0, Ti1, To> : FTPTask
	{
		#region Exposed fields

		// Pre
		private Ti0 argument0;
		private Ti1 argument1;

		// Post
		private To result;

		#endregion Exposed fields

		#region Internal fields

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		public Ti0 Argument0
		{
			get
			{
				return argument0;
			}

			set
			{
				argument0 = value;
			}
		}

		public Ti1 Argument1
		{
			get
			{
				return argument1;
			}

			set
			{
				argument1 = value;
			}
		}

		public To Result
		{
			get
			{
				return result;
			}

			set
			{
				result = value;
			}
		}

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		/// <summary>
		/// Create a new FTP Task
		/// </summary>
		protected FTPTask(string host, string user, string pass, string requestMethod, Ti0 argument0, Ti1 argument1) : base(host, user, pass, requestMethod)
		{
			Argument0 = argument0;
			Argument1 = argument1;
		}

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Printable string of the argument
		/// </summary>
		protected override string ArgumentsToString()
		{
			return Argument0.ToString() + " " + Argument1.ToString();
		}

		#region Worker methods

		/// <summary>
		/// Encapsulates the collections usage
		/// </summary>
		protected override void _RunTask(object sender, DoWorkEventArgs e)
		{
			base._RunTask(sender, e);

			Error = _TaskWork(Argument0, Argument1, ref result);
			HasError = Error != null;
			Message += '\t' + (HasError ? ("Error: " + Error.ToString()) : "Success");
		}

		/// <summary>
		/// Encapsulates the collections usage
		/// </summary>
		protected override void _CompleteTask(object sender, RunWorkerCompletedEventArgs e)
		{
			base._CompleteTask(sender, e);
		}

		/// <summary>
		/// Main logic of the task
		/// </summary>
		protected abstract Exception _TaskWork(Ti0 argument0, Ti1 argument1, ref To result);

		#endregion

		#endregion Non Public Methods
	}

	public class MakeDirectory : FTPTask<string, bool>
	{
		public string NewDirectory
		{
			get
			{
				return Argument;
			}

			set
			{
				Argument = value;
			}
		}
		
		public MakeDirectory() : this(null, null, null, null)
		{
		}

		public MakeDirectory(string host, string user, string pass, string newDirectory) : base(host, user, pass, WebRequestMethods.Ftp.MakeDirectory, newDirectory)
		{
		}
		
		protected override Exception _TaskWork(string argument, ref bool result)
		{
			result = false;
			var newDirectory = argument;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)WebRequest.Create(new Uri("ftp://" + Host + "/") + newDirectory);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */
				ftpRequest.Method = RequestMethod;

				/* Establish Return Communication with the FTP Server */
				var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
				/* Resource Cleanup */
				ftpResponse.Close();
				ftpRequest = null;
			}
			catch (Exception ex)
			{
				return ex;
			}

			return null;
		}
	}

	public class CheckDirectory : FTPTask<string, bool>
	{
		public string Directory
		{
			get
			{
				return Argument;
			}

			set
			{
				Argument = value;
			}
		}
		
		public CheckDirectory() : this(null, null, null, null)
		{
		}

		public CheckDirectory(string host, string user, string pass, string directory) : base(host, user, pass, WebRequestMethods.Ftp.PrintWorkingDirectory, directory)
		{
		}
		
		protected override Exception _TaskWork(string argument, ref bool result)
		{
			result = false;
			var directory = argument;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/") + directory);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */
				ftpRequest.Method = RequestMethod;

				FtpWebResponse ftpResponse = null;
				try
				{
					/* Establish Return Communication with the FTP Server */
					ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
				}
				catch (WebException ex)
				{
					// Real error?
					if (!ex.Message.Contains("error: 550"))
						return ex;

					return null;
				}
				/* Resource Cleanup */
				ftpResponse.Close();
				ftpRequest = null;

				result = true;
			}
			catch (Exception ex) { return ex; }

			return null;
		}
	}

	public class ListDirectory : FTPTask<string, string[]>
	{
		public string Directory
		{
			get
			{
				return Argument;
			}

			set
			{
				Argument = value;
			}
		}
		
		public ListDirectory() : this(null, null, null, null)
		{
		}

		public ListDirectory(string host, string user, string pass, string directory) : base(host, user, pass, WebRequestMethods.Ftp.ListDirectory, directory)
		{
		}
		
		protected override Exception _TaskWork(string argument, ref string[] result)
		{
			result = null;
			var directory = Argument;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/") + directory);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */
				ftpRequest.Method = RequestMethod;

				/* Establish Return Communication with the FTP Server */
				var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
				/* Establish Return Communication with the FTP Server */
				var ftpStream = ftpResponse.GetResponseStream();
				/* Get the FTP Server's Response Stream */
				StreamReader ftpReader = new StreamReader(ftpStream);
				/* Store the Raw Response */
				string directoryRaw = null;
				/* Read Each Line of the Response and Append a Pipe to Each Line for Easy Parsing */
				try
				{
					while (ftpReader.Peek() != -1)
					{
						directoryRaw += ftpReader.ReadLine() + "|";
					}
				}
				catch (Exception ex) { return ex; }
				/* Resource Cleanup */
				ftpReader.Close();
				ftpStream.Close();
				ftpResponse.Close();
				ftpRequest = null;
				/* Return the Directory Listing as a string Array by Parsing 'directoryRaw' with the Delimiter you Append (I use | in This Example) */
				try
				{
					string[] directoryList = directoryRaw.Split("|".ToCharArray());
					result = directoryList;
				}
				catch (Exception ex) { return ex; }
			}
			catch (Exception ex) { return ex; }

			return null;
		}
	}

	public class ListDirectoryDetails : ListDirectory
	{		
		public ListDirectoryDetails() : this(null, null, null, null)
		{
		}

		public ListDirectoryDetails(string host, string user, string pass, string directory) : base(host, user, pass, directory)
		{
			RequestMethod = WebRequestMethods.Ftp.ListDirectoryDetails;
		}		
	}

	public class UploadFile : FTPTask<string, string, bool>
	{
		public string RemoteFile
		{
			get
			{
				return Argument0;
			}

			set
			{
				Argument0 = value;
			}
		}

		public string LocalFile
		{
			get
			{
				return Argument1;
			}

			set
			{
				Argument1 = value;
			}
		}

		public int BufferSize
		{
			get; set;
		}
		
		public UploadFile() : this(null, null, null, null, null)
		{

		}

		public UploadFile(string host, string user, string pass, string remoteFile, string localFile) : base(host, user, pass, WebRequestMethods.Ftp.UploadFile, remoteFile, localFile)
		{
			BufferSize = FTP.Instance.BufferSize;
		}
		
		protected override Exception _TaskWork(string argument0, string argument1, ref bool result)
		{
			result = false;
			var remoteFile = argument0;
			var localFile = argument1;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/") + remoteFile);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */

				ftpRequest.Method = RequestMethod;
				/* Establish Return Communication with the FTP Server */
				var ftpStream = ftpRequest.GetRequestStream();
				/* Open a File Stream to Read the File for Upload */
				FileStream localFileStream = new FileStream(localFile, FileMode.Open);
				/* Buffer for the Downloaded Data */
				byte[] byteBuffer = new byte[BufferSize];
				int bytesSent = localFileStream.Read(byteBuffer, 0, BufferSize);
				/* Upload the File by Sending the Buffered Data Until the Transfer is Complete */
				try
				{
					while (bytesSent != 0)
					{
						ftpStream.Write(byteBuffer, 0, bytesSent);
						bytesSent = localFileStream.Read(byteBuffer, 0, BufferSize);
					}
				}
				catch (Exception ex) { return ex; }
				/* Resource Cleanup */
				localFileStream.Close();
				ftpStream.Close();
				ftpRequest = null;

				result = true;
			}
			catch (Exception ex) { return ex; }

			return null;
		}
	}

	public class DownloadFile : FTPTask<string, string, bool>
	{
		public string RemoteFile
		{
			get
			{
				return Argument0;
			}

			set
			{
				Argument0 = value;
			}
		}

		public string LocalFile
		{
			get
			{
				return Argument1;
			}

			set
			{
				Argument1 = value;
			}
		}

		public int BufferSize
		{
			get; set;
		}
		
		public DownloadFile() : this(null, null, null, null, null)
		{

		}

		public DownloadFile(string host, string user, string pass, string remoteFile, string localFile) : base(host, user, pass, WebRequestMethods.Ftp.DownloadFile, remoteFile, localFile)
		{
			BufferSize = FTP.Instance.BufferSize;
		}
		
		protected override Exception _TaskWork(string argument0, string argument1, ref bool result)
		{
			result = false;
			var remoteFile = argument0;
			var localFile = argument1;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/") + remoteFile);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */
				ftpRequest.Method = RequestMethod;

				/* Establish Return Communication with the FTP Server */
				var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
				/* Get the FTP Server's Response Stream */
				var ftpStream = ftpResponse.GetResponseStream();
				/* Open a File Stream to Write the Downloaded File */
				FileStream localFileStream = new FileStream(localFile, FileMode.Create);
				/* Buffer for the Downloaded Data */
				byte[] byteBuffer = new byte[BufferSize];
				int bytesRead = ftpStream.Read(byteBuffer, 0, BufferSize);
				/* Download the File by Writing the Buffered Data Until the Transfer is Complete */
				try
				{
					while (bytesRead > 0)
					{
						localFileStream.Write(byteBuffer, 0, bytesRead);
						bytesRead = ftpStream.Read(byteBuffer, 0, BufferSize);
					}
				}
				catch (Exception ex) { return ex; }
				
				/* Resource Cleanup */
				localFileStream.Close();
				ftpStream.Close();
				ftpResponse.Close();
				ftpRequest = null;

				result = true;
			}
			catch (Exception ex) { return ex; }

			return null;
		}
	}
		
	public class DeleteFile : FTPTask<string, bool>
	{
		public string RemoteFile
		{
			get
			{
				return Argument;
			}

			set
			{
				Argument = value;
			}
		}
		
		public DeleteFile() : this(null, null, null, null)
		{

		}

		public DeleteFile(string host, string user, string pass, string remoteFile) : base(host, user, pass, WebRequestMethods.Ftp.DeleteFile, remoteFile)
		{
		}
		
		protected override Exception _TaskWork(string argument, ref bool result)
		{
			result = false;
			var remoteFile = argument;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/") + remoteFile);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */
				ftpRequest.Method = RequestMethod;
				
				/* Establish Return Communication with the FTP Server */
				var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();

				/* Resource Cleanup */
				ftpResponse.Close();
				ftpRequest = null;

				result = true;
			}
			catch (Exception ex) { return ex; }

			return null;
		}
	}

	public class Rename : FTPTask<string, string, bool>
	{
		public string CurrentFilepath
		{
			get
			{
				return Argument0;
			}

			set
			{
				Argument0 = value;
			}
		}

		public string NewFilepath
		{
			get
			{
				return Argument1;
			}

			set
			{
				Argument1 = value;
			}
		}
		
		public Rename() : this(null, null, null, null, null)
		{

		}

		public Rename(string host, string user, string pass, string currentFilepath, string newFilepath) : base(host, user, pass, WebRequestMethods.Ftp.Rename, currentFilepath, newFilepath)
		{
		}
		
		protected override Exception _TaskWork(string argument0, string argument1, ref bool result)
		{
			result = false;
			var currentFilepath = argument0;
			var newFilepath = argument1;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/") + currentFilepath);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */
				ftpRequest.Method = RequestMethod;

				/* Rename the File */
				ftpRequest.RenameTo = newFilepath;
				/* Establish Return Communication with the FTP Server */
				var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();

				/* Resource Cleanup */
				ftpResponse.Close();
				ftpRequest = null;

				result = true;
			}
			catch (Exception ex) { return ex; }

			return null;
		}
	}
		
	public class GetFileTimestamp : FTPTask<string, string>
	{
		public string Filepath
		{
			get
			{
				return Argument;
			}

			set
			{
				Argument = value;
			}
		}
		
		public GetFileTimestamp() : this(null, null, null, null)
		{
		}

		public GetFileTimestamp(string host, string user, string pass, string filepath) : base(host, user, pass, WebRequestMethods.Ftp.GetDateTimestamp, filepath)
		{
		}
		
		protected override Exception _TaskWork(string argument, ref string result)
		{
			result = string.Empty;
			var filepath = argument;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/") + filepath);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */
				ftpRequest.Method = RequestMethod;

				/* Establish Return Communication with the FTP Server */
				var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
				/* Establish Return Communication with the FTP Server */
				var ftpStream = ftpResponse.GetResponseStream();
				/* Get the FTP Server's Response Stream */
				StreamReader ftpReader = new StreamReader(ftpStream);

				/* Store the Raw Response */
				string fileInfo = null;
				/* Read the Full Response Stream */
				try
				{
					fileInfo = ftpReader.ReadToEnd();
				}
				catch (Exception ex) { return ex; }

				/* Resource Cleanup */
				ftpReader.Close();
				ftpStream.Close();
				ftpResponse.Close();
				ftpRequest = null;

				result = fileInfo;
			}
			catch (Exception ex) { return ex; }

			return null;
		}
	}
		
	public class GetFileSize : FTPTask<string, string>
	{
		public string Filepath
		{
			get
			{
				return Argument;
			}

			set
			{
				Argument = value;
			}
		}
		
		public GetFileSize() : this(null, null, null, null)
		{
		}

		public GetFileSize(string host, string user, string pass, string filepath) : base(host, user, pass, WebRequestMethods.Ftp.GetFileSize, filepath)
		{
		}
		
		protected override Exception _TaskWork(string argument, ref string result)
		{
			result = string.Empty;
			var filepath = argument;
			try
			{
				/* Create an FTP Request */
				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + Host + "/") + filepath);
				/* Log in to the FTP Server with the User Name and Password Provided */
				ftpRequest.Credentials = new NetworkCredential(User, Pass);
				/* When in doubt, use these options */
				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;
				/* Specify the Type of FTP Request */
				ftpRequest.Method = RequestMethod;

				/* Establish Return Communication with the FTP Server */
				var ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
				/* Establish Return Communication with the FTP Server */
				var ftpStream = ftpResponse.GetResponseStream();
				/* Get the FTP Server's Response Stream */
				StreamReader ftpReader = new StreamReader(ftpStream);

				/* Store the Raw Response */
				string fileInfo = null;
				/* Read the Full Response Stream */
				try
				{
					while (ftpReader.Peek() != -1)
					{
						fileInfo = ftpReader.ReadToEnd();
					}
				}
				catch (Exception ex) { return ex; }

				/* Resource Cleanup */
				ftpReader.Close();
				ftpStream.Close();
				ftpResponse.Close();
				ftpRequest = null;

				result = fileInfo;
			}
			catch (Exception ex) { return ex; }

			return null;
		}
	}
}