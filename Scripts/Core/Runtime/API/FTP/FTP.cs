﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;

using CEUtilities.Patterns;

namespace CEUtilities.API
{
	public class FTP : Singleton<FTP>
	{
		#region Exposed fields

		[Header("FTP Setup")]
		[SerializeField]
		private string host;
		[SerializeField]
		private string user;
		[SerializeField]
		private string pass;

		[Header("Advanced")]
		[SerializeField]
		private int bufferSize = 2048;

		#endregion Exposed fields

		#region Internal fields

		#endregion Internal fields

		#region Custom Events		

		#endregion Custom Events

		#region Properties

		public string Host
		{
			get
			{
				return host;
			}

			set
			{
				host = value;
			}
		}

		public string User
		{
			get
			{
				return user;
			}

			set
			{
				user = value;
			}
		}

		public string Pass
		{
			get
			{
				return pass;
			}

			set
			{
				pass = value;
			}
		}

		public int BufferSize
		{
			get
			{
				return bufferSize;
			}

			set
			{
				bufferSize = value;
			}
		}

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		/// <summary>
		/// Create a task to be executed later
		/// </summary>
		public T CreateTask<T>() where T : FTPTask
		{
			if (!CheckSetup())
			{
				Debug.LogError("[FTP] Incomplete data");
				return null;
			}

			var task = Activator.CreateInstance<T>();
			task.User = User;
			task.Pass = Pass;
			task.Host = Host;
			return task;
		}

		/// <summary>
		/// Execute the task in background
		/// </summary>
		public void Run(FTPTask task)
		{
			task.Run();
		}

		/// <summary>
		/// Yieldable method for background task
		/// </summary>
		public IEnumerator _RunAsync(FTPTask task)
		{
			yield return task._RunAsync();
		}

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Has all needed fields?
		/// </summary>
		private bool CheckSetup()
		{
			if (Host == null || user == null || Pass == null)
				return false;

			return true;
		}

		#endregion Non Public Methods
	}
}