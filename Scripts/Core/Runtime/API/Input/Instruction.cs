﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Runtime.InteropServices;
using WindowsInput;
using WindowsInput.Native;

namespace CEUtilities.API
{
    /// <summary>
    /// Generic instruction.
    /// </summary>
    [Serializable]
    public abstract class Instruction
    {
        public abstract void Execute();

        public virtual int Delay { get; set; }

        public virtual int Duration { get; set; }
    }

    /// <summary>
    /// Keyboard instruction.
    /// </summary>
    /// <seealso cref="CEUtilities.API.Instruction" />
    [Serializable]
    public class KeyInstruction : Instruction
    {
        //Const
        [NonSerialized]
        private const int VKKEYSCANSHIFTON = 0x0100;

        [NonSerialized]
        private const int VKKEYSCANCTRLON = 0x0200;

        [NonSerialized]
        private const int VKKEYSCANALTON = 0x0400;

        //Atributos
        private VirtualKeyCode _key = 0x00;
        private List<VirtualKeyCode> _keyModifiers;

        private string keyText = string.Empty;
        private List<string> keyModifiersText = new List<string>();

        // Getters y Setters
        public string KeyText
        {
            get
            {
                return keyText;
            }

            set
            {
                keyText = value;
            }
        }
        public List<string> KeyModifiersText
        {
            get
            {
                return keyModifiersText;
            }

            set
            {
                keyModifiersText = value;
            }
        }

        //Builders
        public KeyInstruction() { }
        public KeyInstruction(string keyText) : this(keyText, null, 0, 0) { }
        public KeyInstruction(string keyText, int delay) : this(keyText, null, delay, 0) { }
        public KeyInstruction(string keyText, int delay, int duration) : this(keyText, null, delay, duration) { }
        public KeyInstruction(string keyText, List<string> keyModifiersText, int delay, int duration)
        {
            Init(keyText, keyModifiersText, delay, duration);
        }

        //Inicializa la variable
        public void Init(string keyText, List<string> keyModifiersText, int delay, int duration)
        {
            this.Delay = delay;
            this.Duration = duration;
            this.KeyText = keyText;
            this.KeyModifiersText = keyModifiersText;

            Parse();
        }

        //Interpreta los strings almacenados
        public void Parse()
        {
            processKey(keyText, keyModifiersText);
        }

        //Procedimiento de ejecución de la instruccion
        public override void Execute()
        {
            //Simula la pulsación
            if (_key != 0x00)
            {
                //Delay
                if (Delay > 0)
                {
                    Thread.Sleep(Delay);
                }

                InputSimulator inSim = new InputSimulator();

                foreach (VirtualKeyCode key in _keyModifiers)
                    inSim.Keyboard.KeyDown((VirtualKeyCode)key);

                inSim.Keyboard.KeyDown((VirtualKeyCode)_key);
                inSim.Keyboard.Sleep(Duration);
                inSim.Keyboard.KeyUp((VirtualKeyCode)_key);

                foreach (VirtualKeyCode key in _keyModifiers)
                    inSim.Keyboard.KeyUp((VirtualKeyCode)key);
            }
        }



        //Extrae la key y la almacena
        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern short VkKeyScan(char ch);
        private void processKey(string keyText, List<string> keyModifiersText)
        {
            // Reset vars
            _key = 0x00;
            if (_keyModifiers != null)
            {
                _keyModifiers.Clear();
            }
            else
            {
                _keyModifiers = new List<VirtualKeyCode>();
            }

            try
            {
                //Asigna la tecla
                _key = parseKeyText(keyText);

                //Asigna los modificadores básicos
                if (HasFlag(_key, (VirtualKeyCode)VKKEYSCANSHIFTON))
                    _keyModifiers.Add(VirtualKeyCode.SHIFT);
                if (HasFlag(_key, (VirtualKeyCode)VKKEYSCANCTRLON))
                    _keyModifiers.Add(VirtualKeyCode.CONTROL);
                if (HasFlag(_key, (VirtualKeyCode)VKKEYSCANALTON))
                    _keyModifiers.Add(VirtualKeyCode.MENU);

                //Asigna los modificadores extras
                foreach (string keyModifierText in keyModifiersText)
                {
                    VirtualKeyCode modifierKey = parseKeyText(keyModifierText);
                    if (!_keyModifiers.Any(x => x == modifierKey))
                    {
                        _keyModifiers.Add(modifierKey);
                    }
                }
            }
            catch
            {
                //En caso de error invalida la instruccion haciendola nula
                _key = 0x00;
            }
        }

        //Chequea si posee el flag
        private bool HasFlag(VirtualKeyCode flags, VirtualKeyCode flag)
        {
            return (flags & flag) != 0;
        }

        //Determina que key es especifica en el string
        private VirtualKeyCode parseKeyText(string keyText)
        {
            VirtualKeyCode key = 0x00;
            try
            {
                if (keyText.Length == 1)
                {
                    key = (VirtualKeyCode)VkKeyScan(keyText[0]);
                }
                else
                {
                    key = (VirtualKeyCode)Enum.Parse(typeof(VirtualKeyCode), keyText, true);
                }
            }
            catch { }
            return key;
        }
    }
}
