﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using CEUtilities.API;
using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
    [RequireComponent(typeof(RawImage))]
    public class WebcamImage : SerializedMonoBehaviour
    {
        #region Structs

        public enum SizeMode
        {
            Normal,
            Fit,
            Crop
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        private Webcam webcam;

        [SerializeField]
        private bool useRenderTexture;

        [SerializeField]
        private SizeMode imageSizeMode = SizeMode.Crop;

        #endregion Exposed fields

        #region Internal fields

        bool initDone = false;

        RawImage targetImage;

        RectTransform canvasRT;
        RectTransform imageRT;

        bool cachedFrontFacing = false;
        int cachedVideoRotation = int.MinValue;
        Vector2 cachedCanvasSize = Vector2.zero;

        bool forceScaleUpdate = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Webcam Webcam
        {
            get
            {
                return webcam;
            }

            set
            {
                webcam = value;
            }
        }

        public bool UseRenderTexture
        {
            get
            {
                return useRenderTexture;
            }

            set
            {
                useRenderTexture = value;
            }
        }

        public SizeMode ImageSizeMode
        {
            get
            {
                return imageSizeMode;
            }

            set
            {
                imageSizeMode = value;
            }
        }


        private RawImage TargetImage
        {
            get
            {
                if (targetImage == null)
                    targetImage = GetComponent<RawImage>();

                return targetImage;
            }
        }

        private Vector2 FinalSizeDelta
        {
            get
            {
                return (cachedVideoRotation % 180 == 0) ?
                    imageRT.sizeDelta :
                    new Vector2(imageRT.sizeDelta.y, imageRT.sizeDelta.x);
            }

            set
            {
                imageRT.sizeDelta = (cachedVideoRotation % 180 == 0) ?
                    value :
                    new Vector2(value.y, value.x);
            }
        }

        #endregion Properties

        #region Events methods

        protected IEnumerator Start()
        {
            imageRT = GetComponent<RectTransform>();
            canvasRT = TargetImage.canvas.GetComponent<RectTransform>();

            if (webcam == null || !webcam.IsPlaying)
            {
                Debug.LogWarning("[WebcamImage] Waiting for webcam...");
                yield return new WaitUntil(() => webcam.IsPlaying);
            }

            if (UseRenderTexture)
            {
                TargetImage.texture = webcam.RenderTexture;
                imageRT.sizeDelta = new Vector2(webcam.RenderTexture.width, webcam.RenderTexture.height);
            }
            else
            {
                TargetImage.texture = webcam.WebcamTex;
                imageRT.sizeDelta = new Vector2(webcam.WebcamTex.width, webcam.WebcamTex.height);
            }

            initDone = true;
        }

        protected void Update()
        {
            if (!initDone)
                return;

            if (cachedVideoRotation != Webcam.RotationAngle)
            {
                cachedVideoRotation = Webcam.RotationAngle;
                forceScaleUpdate = true;
                UpdateImageRotation();
            }

            if (cachedFrontFacing != Webcam.FrontFacing || forceScaleUpdate)
            {
                cachedFrontFacing = Webcam.FrontFacing;
                forceScaleUpdate = false;
                UpdateImageScale();
            }

            if (cachedCanvasSize != canvasRT.sizeDelta)
            {
                cachedCanvasSize = canvasRT.sizeDelta;
                UpdateImageSize();
            }
        }

        #endregion Events methods

        #region Public Methods

        public void UpdateImageScale()
        {
            if (cachedFrontFacing)
                imageRT.localScale = (cachedVideoRotation % 180 == 0) ?
                    new Vector3(-1, 1, 1) :
                    new Vector3(1, -1, 1);
            else
                imageRT.localScale = Vector3.one;

            Debug.Log(string.Format("[WebcamImage] Front facing: {0}, Image scale: {1}", cachedFrontFacing, imageRT.localScale));
        }

        public void UpdateImageRotation()
        {
            var rotation = imageRT.localEulerAngles;
            rotation.z = -cachedVideoRotation;
            imageRT.localEulerAngles = rotation;

            Debug.Log(string.Format("[WebcamImage] Video rotation: {0}, Image rotation (local): {1}", cachedVideoRotation, rotation));
        }

        public void UpdateImageSize()
        {
            if (ImageSizeMode == SizeMode.Normal) return;

            var preImageSize = FinalSizeDelta;
            var imageSize = preImageSize;
            var canvasSize = cachedCanvasSize;

            if (ImageSizeMode == SizeMode.Crop)
            {
                var imageAR = imageSize.x / imageSize.y;
                var canvasAR = canvasSize.x / canvasSize.y;

                if (imageAR > canvasAR)
                    imageSize = new Vector2(canvasSize.y * imageAR, canvasSize.y);
                else
                    imageSize = new Vector2(canvasSize.x, canvasSize.x / imageAR);

                FinalSizeDelta = imageSize;
            }

            Debug.Log(string.Format("[WebcamImage] Pre Image Size: {0}, Image Size: {1}, Canvas Size: {2}", preImageSize, imageSize, canvasSize));
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}