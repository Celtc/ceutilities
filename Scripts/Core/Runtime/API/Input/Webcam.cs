﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;
using System.Collections;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

namespace CEUtilities.API
{
    public class Webcam : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [PropertyOrder(0)]
        private bool autoStart = false;

        [SerializeField]
        [PropertyOrder(1)]
        private string defaultDevice;

        [SerializeField]
        [PropertyOrder(3)]
        private RawImage preview = null;

        [SerializeField]
        [PropertyOrder(4)]
        private RenderTexture renderTexture = null;

        [SerializeField]
        [PropertyOrder(5)]
		[Indent]
        private bool flipX = false;

        [SerializeField]
        [PropertyOrder(6)]
		[Indent]
        private bool flipY = false;
        
        [Header("Settings")]

        [SerializeField]
        [Indent]
        [PropertyOrder(6)]
        private Vector2 requestedResolution = new Vector2(1920, 1080);

        [SerializeField]
        [Indent]
        [PropertyOrder(7)]
        private int requestedFPS = 25;
        

        private WebCamDevice device;

        #endregion Exposed fields

        #region Internal fields

        private WebCamDevice[] devices;
        private WebCamTexture webCamTex;

        private int lastAngle = int.MinValue;

        #endregion Internal fields

        #region Properties (public)

        /// <summary>
        /// Should start cam automatically
        /// </summary>
        public bool AutoStart
        {
            get
            {
                return autoStart;
            }

            set
            {
                autoStart = value;
            }
        }

        /// <summary>
        /// Available devices
        /// </summary>
        public WebCamDevice[] Devices
        {
            get
            {
                if (devices == null)
                    devices = WebCamTexture.devices;

                return devices;
            }
        }

        /// <summary>
        /// Default device on init
        /// </summary>
        public string DefaultDevice
        {
            get
            {
                return defaultDevice;
            }

            set
            {
                defaultDevice = value;
            }
        }

        /// <summary>
        /// Used device
        /// </summary>
        public WebCamDevice Device
        {
            get
            {
                return device;
            }

            private set
            {
                device = value;
            }
        }

        /// <summary>
        /// Webcam texture
        /// </summary>
        public WebCamTexture WebcamTex
        {
            get
            {
                return webCamTex;
            }

            private set
            {
                webCamTex = value;
            }
        }

        /// <summary>
        /// Requested res
        /// </summary>
        public Vector2 RequestedResolution
        {
            get
            {
                return requestedResolution;
            }

            set
            {
                requestedResolution = value;
            }
        }

        /// <summary>
        /// Requested fps
        /// </summary>
        public int RequestedFPS
        {
            get
            {
                return requestedFPS;
            }

            set
            {
                requestedFPS = value;
            }
        }


        /// <summary>
        /// Is currently a device selected
        /// </summary>
        public bool HasDeviceSelected
        {
            get
            {
                return Device.name != null;
            }
        }

        /// <summary>
        /// Is the webcam running?
        /// </summary>
        public bool IsPlaying
        {
            get
            {
                return webCamTex != null && webCamTex.isPlaying;
            }
        }

        /// <summary>
        /// Is the webcam running?
        /// </summary>
        public bool IsPausedOrStopped
        {
            get
            {
                return webCamTex == null || !webCamTex.isPlaying;
            }
        }

        /// <summary>
        /// Roitation angle of the video
        /// </summary>
        public int RotationAngle
        {
            get
            {
                return WebcamTex.videoRotationAngle;
            }
        }

        /// <summary>
        /// Is the current device a front facing cam
        /// </summary>
        public bool FrontFacing
        {
            get
            {
                return Device.isFrontFacing;
            }
        }

		public bool FlipX
		{
			get
			{
				return flipX;
			}

			set
			{
				flipX = value;
			}
		}

		public bool FlipY
		{
			get
			{
				return flipY;
			}

			set
			{
				flipY = value;
			}
		}


        /// <summary>
        /// Preview RawImage
        /// </summary>
		public RawImage Preview
        {
            get
            {
                return preview;
            }

            private set
            {
                preview = value;
            }
        }

        /// <summary>
        /// Target RenderTexture
        /// </summary>
		public RenderTexture RenderTexture
        {
            get
            {
                return renderTexture;
            }

            private set
            {
                renderTexture = value;
            }
        }


        [ShowInInspector]
        [Indent]
        [PropertyOrder(100)]
        public string DeviceSelected
        {
            get
            {
                return HasDeviceSelected ? Device.name : string.Empty;
            }
        }

        [ShowInInspector]
        [Indent]
        [PropertyOrder(101)]
        public Vector2 ActualResolution
        {
            get
            {
                return webCamTex != null ? new Vector2(webCamTex.width, webCamTex.height) : Vector2.zero;
            }
        }

        [ShowInInspector]
        [Indent]
        [PropertyOrder(102)]
        public float ActualFPS
        {
            get
            {
                return webCamTex != null ? Mathf.Min(webCamTex.requestedFPS, 1.0f / Time.deltaTime) : 0;
            }
        }

		#endregion Properties

		#region Custom Events

		/// <summary>
		/// Call when the device changes
		/// </summary>
		public UnityEvent OnDeviceChange = new UnityEvent();

        /// <summary>
        /// Call when the video rotation changes
        /// </summary>
        public UnityEvent_Int OnVideoRotationChange = new UnityEvent_Int();

        /// <summary>
        /// Call when a photo is taken
        /// </summary>
        public UnityEvent_Texture2D OnPhotoTaken = new UnityEvent_Texture2D();

        #endregion Custom Events

        #region Unity events

        void Start()
        {
            ChangeToDefaultDevice();

            if (AutoStart)
                StartCam();
        }

        void Update()
        {
            if (IsPlaying)
            {
                // Blitz
                if (RenderTexture != null)
                    Graphics.Blit(
						WebcamTex, 
						RenderTexture, 
						new Vector2(flipX ? -1 : 1, flipY ? -1 : 1), new Vector2(flipX ? 1 : 0, flipY ? 1 : 0));

                // Check video rotation event
                if (lastAngle != RotationAngle)
                {
                    lastAngle = RotationAngle;
                    OnVideoRotationChange.Invoke(lastAngle);
                }
            }
        }

        #endregion Unity events

        #region Public Methods

        /// <summary>
        /// Refresh the available devices
        /// </summary>
        public void RefreshDevices()
        {
            devices = null;
        }

        /// <summary>
        /// Change to the device index
        /// </summary>
        public void ChangeToDevice(int index)
        {
            if (index < 0 || index >= Devices.Length)
            {
                Debug.LogWarning("[WebCampAPI] Trying to change to a not existing device");
                return;
            }

            // Stop current device
            var playing = IsPlaying;
            if (playing)
                WebcamTex.Stop();
            WebcamTex = null;

            Device = Devices[index];
            GetWebcamTexture();

            // Resume if was playing
            if (playing)
                WebcamTex.Play();

            Debug.Log("[WebCampAPI] Changed device");

            // Event
            OnDeviceChange.Invoke();
        }

        /// <summary>
        /// Setup a new camera
        /// </summary>
        public void ChangeToDevice(string deviceName)
        {
            // Check for devices
            if (Devices.Length == 0)
            {
                Debug.LogWarning("[WebCampAPI] No devices available");
                return;
            }

            // Change to device index
            var index = Devices.Select(x => x.name).IndexOf(deviceName);

            // Change to
            ChangeToDevice(index);
        }

        /// <summary>
        /// Setup the default device
        /// </summary>
        [Button]
        [HideInEditorMode]
        public void ChangeToDefaultDevice()
        {
            // Check for devices
            if (Devices.Length == 0)
            {
                Debug.LogWarning("[WebCampAPI] No devices available");
                return;
            }

            // Change to device index
            int index;
            if (!string.IsNullOrEmpty(DefaultDevice))
                index = Devices.Select(x => x.name).IndexOf(DefaultDevice);
            else
                index = Devices.Length - 1;

            // Change to
            ChangeToDevice(index);
        }

        /// <summary>
        /// Change to next camera if available
        /// </summary>
        [Button]
        [HideInEditorMode]
        public void ChangeToNextDevice()
        {
            // Check for devices
            if (Device.name == null || Devices.Length <= 1)
            {
                Debug.Log("[WebCampAPI] No other device available");
                return;
            }

            // Find device
            int index;
            if (HasDeviceSelected)
            {
                index = Devices.IndexOf(Device);
                index = MathHelper.RepeatIndex(index + 1, Devices.Length);
            }
            else
                index = 0;

            // Change to
            ChangeToDevice(index);
        }


        /// <summary>
        /// Starts the camera
        /// </summary>
        [Button]
        [HideInEditorMode]
        [ShowIf("IsPausedOrStopped")]
        public void StartCam()
        {
            // Check for device
            if (WebcamTex == null)
            {
                Debug.LogError("[WebCampAPI] No valid device set");
                return;
            }

            // Plays
            WebcamTex.Play();
            Debug.Log("[WebCampAPI] Started camera");
        }

        /// <summary>
        /// Starts the camera
        /// </summary>
        [Button]
        [HideInEditorMode]
        [ShowIf("IsPlaying")]
        public void StopCam()
        {
            StopCam(false);
        }

        /// <summary>
        /// Starts the camera
        /// </summary>
        public void StopCam(bool dispose)
        {
            // Check for device
            if (WebcamTex == null)
            {
                Debug.LogError("[WebCampAPI] No valid device set");
                return;
            }

            // Plays
            WebcamTex.Stop();
            Debug.Log("[WebCampAPI] Stopped camera");

            // Clear
            if (dispose)
            {
                Device = default(WebCamDevice);
                WebcamTex = null;
            }
        }

        /// <summary>
        /// Take a photo
        /// </summary>
        public Texture2D TakeSnapshot()
        {
            // Check for device
            if (WebcamTex == null || !WebcamTex.isPlaying)
            {
                Debug.LogError("[WebCampAPI] The device is not started");
                return null;
            }

            // Take the snap
            var snap = new Texture2D(WebcamTex.width, WebcamTex.height, TextureFormat.RGB24, false);
            snap.SetPixels(WebcamTex.GetPixels());
            snap.Apply();

            // Event call
            Debug.Log("[WebCampAPI] A new photo was taken. Format: " + snap.format);
            OnPhotoTaken.Invoke(snap);

            return snap;
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// For the current device, get the webcam texture
        /// </summary>
        public void GetWebcamTexture()
        {
            if (!HasDeviceSelected)
            {
                Debug.LogError("[Webcam] Can't retrieve webcam texture");
                return;
            }

            if (WebcamTex != null)
                WebcamTex.Stop();

            webCamTex = new WebCamTexture(Device.name, (int)RequestedResolution.x, (int)RequestedResolution.y, RequestedFPS);
            lastAngle = webCamTex.videoRotationAngle;
            if (Preview != null)
            {
                Preview.texture = WebcamTex;
                Preview.color = Color.white;
            }
        }

        #endregion
    }
}