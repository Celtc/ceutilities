﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.API
{
    public class KeyboardEventSimulate : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private string virtualKeycode;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public string VirtualKeycode
        {
            get
            {
                return virtualKeycode;
            }

            set
            {
                virtualKeycode = value;
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Simulate a key
        /// </summary>
        public void DoSimulate(string virtualKeycode)
        {
            // Create a new instruction
            var keyInstruction = new KeyInstruction();

            keyInstruction.KeyText = virtualKeycode;
            keyInstruction.Parse();
            keyInstruction.Execute();
        }

        /// <summary>
        /// Simulate a key
        /// </summary>
        public void DoSimulate()
        {
            // Create a new instruction
            var keyInstruction = new KeyInstruction();

            keyInstruction.KeyText = VirtualKeycode;
            keyInstruction.Parse();
            keyInstruction.Execute();
        }

        #endregion Methods
    }
}
