﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityPrinter;
using Sirenix.OdinInspector;

namespace CEUtilities.API
{
	public class Printer : MonoBehaviour
	{
		#region Exposed fields

		[SerializeField]
		private Texture2D imageToPrint;

		[SerializeField]
		private bool fitPage = true;

        [SerializeField]
        private int copies = 1;

        [SerializeField]
        private bool copyToDisk = false;

        [SerializeField]
        [Indent]
        [ShowIf("copyToDisk")]
        private string copiesPath;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        [SerializeField]
		public UnityEvent OnPrintSuccess = new UnityEvent();

        [SerializeField]
        public UnityEvent OnPrintFail = new UnityEvent();

        #endregion Custom Events

        #region Properties (public)

        public Texture2D ImageToPrint
		{
			get
			{
				return imageToPrint;
			}

			set
			{
				imageToPrint = value;
			}
		}


		public bool FitPage
		{
			get
			{
				return fitPage;
			}

			set
			{
				fitPage = value;
			}
		}

        public int Copies
        {
            get
            {
                return copies;
            }

            set
            {
                copies = value;
            }
        }

        public bool CopyToDisk
        {
            get
            {
                return copyToDisk;
            }

            set
            {
                copyToDisk = value;
            }
        }

        public string CopiesPath
        {
            get
            {
                return copiesPath;
            }

            set
            {
                copiesPath = value;
            }
        }

        #endregion Properties

        #region Event Methods

        #endregion Event Methods

        #region Public Methods

        /// <summary>
        /// Simples the print.
        /// </summary>
        public void SimplePrint() => Print();

        /// <summary>
        /// Send to print the stored values
        /// </summary>
        public bool Print()
		{
            if (CopyToDisk)
            {
                SaveToDisk(ImageToPrint);
            }

            bool success = UnityPrinter.Printer.PrintImage(ImageToPrint, FitPage, (short)Copies);
            if (success)
            {
                OnPrintSuccess.Invoke();
            }
            else
            {
                OnPrintFail.Invoke();
            }
            return success;
		}

		/// <summary>
		/// Send to print an image
		/// </summary>
		public bool Print(Texture2D texture, bool fitPage, int copies)
        {
            if (CopyToDisk)
            {
                SaveToDisk(texture);
            }

            bool success = UnityPrinter.Printer.PrintImage(texture, fitPage, (short)copies);
            if (success)
            {
                OnPrintSuccess.Invoke();
            }
            else
            {
                OnPrintFail.Invoke();
            }
            return success;
        }

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Saves to disk.
        /// </summary>
        /// <param name="texture">The texture.</param>
        /// <returns></returns>
        private bool SaveToDisk(Texture2D texture)
        {
            var success = false;
            try
            {
                var exists = true;
                if (!Directory.Exists(CopiesPath))
                {
                    exists = Directory.CreateDirectory(CopiesPath) != null;
                }

                if (exists)
                {
                    var filename = (string.IsNullOrEmpty(texture.name) ? "Image" : texture.name) + ".PNG";
                    var filepath = Path.Combine(CopiesPath, filename);

                    if (File.Exists(filepath))
                    {
                        File.Delete(filepath);
                    }

                    var bytes = texture.EncodeToPNG();
                    var file = File.Open(filepath, FileMode.Create);
                    var binary = new BinaryWriter(file);
                    binary.Write(bytes);
                    file.Close();
                    binary.Close();

                    success = true;
                }
            }
            catch
            {
                Debug.Log("[Printer] Could not copy image to disk.");
            }
            return success;
        }

        #endregion Non Public Methods
    }
}