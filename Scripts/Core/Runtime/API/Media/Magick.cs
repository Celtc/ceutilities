﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using CEUtilities.Patterns;
using CEUtilities.Structs;
using Sirenix.OdinInspector;

namespace CEUtilities.API
{
	public class Magick : Singleton<Magick>
	{
		#region Sub Classes / Structs

		/// <summary>
		/// Is the argument from the input section, or the output
		/// </summary>
		public enum ArgumentType
		{
			Input,
			Output
		}

		/// <summary>
		/// Encoder input mode
		/// </summary>
		public enum InputMode
		{
			Images,
			Video
		}

		#endregion

		#region Exposed fields

		[SerializeField]
		private InputMode inputMode = InputMode.Images;

		[SerializeField]
		private string inputFormat = "JPG";
		[SerializeField]
		private string outputFormat = "GIF";

		[SerializeField]
		private string inputArguments;
		[SerializeField]
		private string outputArguments;

		[SerializeField]
		private string outputFolder = @"%USERPROFILE%\Desktop";
		
        [Header("Watermark")]

		[SerializeField]
		private Texture2D watermark;
		[SerializeField]
		private Pivot pivot = Pivot.BottomLeft;
		[SerializeField]
		private Margin margins = new Margin();
		[SerializeField]
        [Range(0, 1)]
		private float alpha = 1;

		#endregion Exposed fields

		#region Internal fields

		private Queue<string> results = new Queue<string>();

		private Dictionary<int, string> processesFile = new Dictionary<int, string>();

		private Queue<Texture2D> inputFrames = new Queue<Texture2D>();

		#endregion Internal fields

		#region Custom Events

		/// <summary>
		/// Call when the process start encoding
		/// </summary>
		public UnityEvent OnEncodeStart = new UnityEvent();
		/// <summary>
		/// Call when the process ends it's encoding
		/// </summary>
		public UnityEvent_String OnEncodeCompleted = new UnityEvent_String();
		/// <summary>
		/// Call when the encoding fails
		/// </summary>
		public UnityEvent OnEncodeFailed = new UnityEvent();

		#endregion Custom Events

		#region Properties (public)

		private string TemplateArguments
		{
			get
			{
				return "{input_args} \"{input_path}\" {output_args} \"{output_path}\"";
			}
		}

		public string ExecutablePath
		{
			get
			{
				return Path.Combine(Application.streamingAssetsPath, "Magick") + @"\magick.exe";
			}
		}

		public InputMode ProcessInputMode
		{
			get
			{
				return inputMode;
			}

			set
			{
				inputMode = value;
			}
		}

		public string InputFormat
		{
			get
			{
				return inputFormat;
			}

			set
			{
				inputFormat = value;
			}
		}

		public string OutputFormat
		{
			get
			{
				return outputFormat;
			}

			set
			{
				outputFormat = value;
			}
		}

		public string InputArguments
		{
			get
			{
				return inputArguments;
			}

			set
			{
				inputArguments = value;
			}
		}

		public string OutputArguments
		{
			get
			{
				return outputArguments;
			}

			set
			{
				outputArguments = value;
			}
		}

		public string OutputFolder
		{
			get
			{
				return outputFolder;
			}

			set
			{
				outputFolder = value;
			}
		}

		public Queue<Texture2D> InputFrames
		{
			get
			{
				return inputFrames;
			}

			set
			{
				inputFrames = value;
			}
		}

		public Texture2D Watermark
		{
			get
			{
				return watermark;
			}

			set
			{
				watermark = value;
			}
		}

		public Pivot Pivot
		{
			get
			{
				return pivot;
			}

			set
			{
				pivot = value;
			}
		}

		public Margin Margins
		{
			get
			{
				return margins;
			}

			set
			{
				margins = value;
			}
		}

		public float Alpha
		{
			get
			{
				return alpha;
			}

			set
			{
				alpha = value;
			}
		}

		#endregion Properties

		#region Event Methods

		void Update()
		{
			ProcessQueue();
		}

        #endregion Event Methods

        #region Methods

        /// <summary>
        /// Check if the executable exists
        /// </summary>
        /// <returns></returns>
        private bool ExecutableExists()
		{
			return File.Exists(ExecutablePath);
		}

		/// <summary>
		/// Verify the existance of the temp folder and return it's path
		/// </summary>
		private string GetTempFolder(string outputName)
		{
			var path = Path.Combine(Application.temporaryCachePath, outputName);
			if (!Directory.Exists(path))
			{
				try
				{
					Directory.CreateDirectory(path);
				}
				catch
				{
					path = null;
				}
			}
			return path;
		}

		/// <summary>
		/// Expand any environment variables and verify its existance, if not create it
		/// </summary>
		/// <returns></returns>
		private string GetOutputFolder()
		{
			var expandedFolder = Environment.ExpandEnvironmentVariables(OutputFolder);
			if (!Directory.Exists(expandedFolder))
			{
				try
				{
					Directory.CreateDirectory(expandedFolder);
				}
				catch
				{
					expandedFolder = null;
				}
			}
			return expandedFolder;
		}

		/// <summary>
		/// Verify paths and files
		/// </summary>
		private bool EarlyChecks(string outputName, out string temporaryFolder, out string outputFilepath)
		{
			outputFilepath = null;
			temporaryFolder = null;

			// Check for exe
			if (!ExecutableExists())
			{
				Debug.LogError("[Magick] The exe was not found!");
				return false;
			}

			// Check for temp folder
			var tempFolder = GetTempFolder(outputName);
			if (tempFolder == null)
			{
				Debug.LogError("[Magick] Couldn't create temporary folder!");
				return false;
			}

			// Check for output folder
			var outputFolder = GetOutputFolder();
			if (outputFolder == null)
			{
				Debug.LogError("[Magick] Couldn't create output folder!");
				return false;
			}

			// Assign exit values
			temporaryFolder = tempFolder;
			outputFilepath = Path.Combine(outputFolder, outputName + "." + OutputFormat);
			return true;
		}

		/// <summary>
		/// Replace the arguments template with actual values
		/// </summary>
		/// <returns></returns>
		private string FormatArguments(string inputFilepath, string outputFilepath)
		{
			return TemplateArguments
				.Replace("{input_args}", InputArguments)
				.Replace("{input_path}", inputFilepath)
				.Replace("{output_args}", OutputArguments)
				.Replace("{output_path}", outputFilepath);
		}

		/// <summary>
		/// Add or replace an argument
		/// </summary>
		public void AddOrReplaceArgument(string key, ArgumentType argType)
		{
			if (argType == ArgumentType.Input)
			{
				InputArguments = AddOrReplaceArgumentString(InputArguments, key, null);
			}
			else
			{
				OutputArguments = AddOrReplaceArgumentString(OutputArguments, key, null);
			}
		}

		/// <summary>
		/// Add or replace an argument
		/// </summary>
		public void AddOrReplaceArgument(string key, string value, ArgumentType argType)
		{
			if (argType == ArgumentType.Input)
			{
				InputArguments = AddOrReplaceArgumentString(InputArguments, key, value);
			}
			else
			{
				OutputArguments = AddOrReplaceArgumentString(OutputArguments, key, value);
			}
		}

		/// <summary>
		/// Replace an argument
		/// </summary>
		private string AddOrReplaceArgumentString(string currentArgs, string key)
		{
			return AddOrReplaceArgumentString(currentArgs, key, null);
		}

		/// <summary>
		/// Replace an argument
		/// </summary>
		private string AddOrReplaceArgumentString(string currentArgs, string key, string value)
		{
			var index = currentArgs.IndexOf("-" + key);
			if (index == -1)
			{
				return currentArgs + " -" + key + (string.IsNullOrEmpty(value) ? "" : (" " + value));
			}
			else
			{
				// Get the string to replace
				var endIndex = currentArgs.IndexOf("-", index + 1);
				string toReplace;
				if (endIndex == -1)
				{
					toReplace = currentArgs.Substring(index);
				}
				else
				{
					toReplace = currentArgs.Substring(index, endIndex - index - 1);
				}

				// Replace the string
				return currentArgs.Replace(toReplace, "-" + key + (string.IsNullOrEmpty(value) ? "" : (" " + value)));
			}
		}

		/// <summary>
		/// Generates a filename with the format yyyyMMddHHmmssffff
		/// </summary>
		private string GenerateFileName()
		{
			return DateTime.Now.ToString("yyyyMMddHHmmssffff");
		}


		/// <summary>
		/// set the frames of the video to be generated
		/// </summary>
		public void SetFrames(Texture2D[] frames)
		{
			InputFrames = new Queue<Texture2D>(frames);
		}

		/// <summary>
		/// Parameterless call
		/// </summary>
		public void Encode()
		{
			Encode(GenerateFileName());
		}

		/// <summary>
		/// Starts the encoding process
		/// </summary>
		public void Encode(string outputName)
		{
			// Working paths
			string tempFolder, outputFilepath;

			// Early check
			if (!EarlyChecks(outputName, out tempFolder, out outputFilepath))
			{
				Debug.LogError("[Magick] Enconding failed!");
				OnEncodeFailed.Invoke();
				return;
			}

			// Split to input mode
			int error = 0;
			string inputFilepath = null;
			switch (ProcessInputMode)
			{
				case InputMode.Images:
				error = PrepareImagesInput(tempFolder, outputFilepath, out inputFilepath);
				break;
				case InputMode.Video:
				error = PrepareVideoInputMode(tempFolder, outputFilepath, out inputFilepath);
				break;
			}

			// Add verbose
			AddOrReplaceArgument("verbose", ArgumentType.Input);

			// Start process
			if (error == 0)
			{
				StartProcess(inputFilepath, outputFilepath);
			}
		}

		/// <summary>
		/// Images input mode
		/// </summary>
		private int PrepareImagesInput(string temporaryFolder, string outputFilepath, out string inputFilepath)
		{
			inputFilepath = null;

			// Has images?
			if (InputFrames == null || InputFrames.Count <= 0)
			{
				Debug.LogError("[Magick] No input images provided!");
				OnEncodeFailed.Invoke();
				return 1;
			}

			// Retrieve image format
			int mode = -1;
			if (InputFormat.ToUpper() == "JPG")
			{
				mode = 0;
			}
			else if (InputFormat.ToUpper() == "PNG")
			{
				mode = 1;
			}
			else
			{
				Debug.LogError("[Magick] Image input format not supported!");
				OnEncodeFailed.Invoke();
				return 2;
			}

			// Save them to temporary folder
			int counter = 0;
			while (InputFrames.Count > 0)
			{
				// Dequeue
				var image = InputFrames.Dequeue();

				// Add watermark
				if (Watermark != null)
					image.AddWatermark(Watermark, Alpha, Margins, Pivot);

				// Encode texture into image
				var bytes = mode == 0 ? image.EncodeToJPG() : image.EncodeToPNG();
				Texture2D.Destroy(image);

				// Write it
				File.WriteAllBytes(Path.Combine(temporaryFolder, counter++.ToString() + "." + InputFormat), bytes);
			}

			// Format inputfilepath
			inputFilepath = Path.Combine(temporaryFolder, "*." + InputFormat);

			return 0;
		}

		/// <summary>
		/// Video input mode
		/// </summary>
		private int PrepareVideoInputMode(string temporaryFolder, string outputFilepath, out string inputFilepath)
		{
			inputFilepath = null;

			Debug.LogError("[Magick] Video input mode not implemented!");
			OnEncodeFailed.Invoke();
			return -1;
		}

		/// <summary>
		/// Start processing
		/// </summary>
		private void StartProcess(string inputFilepath, string outputFilepath)
		{
			// Process start parameters
			var info = new System.Diagnostics.ProcessStartInfo();
			info.CreateNoWindow = true;
			info.UseShellExecute = false;
			info.RedirectStandardInput = true;
			info.RedirectStandardOutput = true;
			info.RedirectStandardError = true;

			// Exe arguments
			info.WorkingDirectory = Path.GetDirectoryName(inputFilepath);
			info.FileName = ExecutablePath;
			info.Arguments = FormatArguments(inputFilepath, outputFilepath);

			// Create process
			var process = new System.Diagnostics.Process();
			process.StartInfo = info;
			process.OutputDataReceived += Process_DataReceived;
			process.ErrorDataReceived += Process_DataReceived;

			// Start the process
			Debug.Log(string.Format("[Magick] Starting {0} with args: {1}", info.FileName, info.Arguments));
			process.Start();
			process.BeginOutputReadLine();
			process.BeginErrorReadLine();

			// Add to dict
			processesFile.Add(process.Id, outputFilepath);

			// Event call
			OnEncodeStart.Invoke();
		}

		/// <summary>
		/// Output callback
		/// </summary>
		private void Process_DataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e)
		{
			var process = sender as System.Diagnostics.Process;

			if (e.Data == null)
				return;

			var output = e.Data;

			// Error
			if (Regex.IsMatch(output, WildCardToRegular("convert:*")))
			{
				lock (results)
				{
					var filename = processesFile[process.Id];
					var result = string.Format("{0}{1}", "E", filename);
					processesFile.Remove(process.Id);
					results.Enqueue(result);
				}
			}
			// Ended
			if (Regex.IsMatch(output, WildCardToRegular("*=>*")))
			{
				lock (results)
				{
					var filename = processesFile[process.Id];
					var result = string.Format("{0}{1}", "I", filename);
					processesFile.Remove(process.Id);
					results.Enqueue(result);
				}
			}
			// Progress
			else
			{
				//
			}
		}


		/// <summary>
		/// Unity synched thread treatmennt
		/// </summary>
		private void ProcessQueue()
		{
			lock (results)
			{
				while (results.Count > 0)
				{
					var entry = results.Dequeue();
					var filename = entry.Substring(1);

					if (entry[0] == 'I')
					{
						Debug.Log("[Magick] Encoded file " + filename);
						OnEncodeCompleted.Invoke(filename);
					}
					else if (entry[0] == 'E')
					{
						Debug.LogError("[Magick] Failed to encode file " + filename);
						OnEncodeFailed.Invoke();
					}
				}
			}
		}

		/// <summary>
		/// Wildward method
		/// </summary>
		private static String WildCardToRegular(String value)
		{
			return "^" + Regex.Escape(value).Replace("\\?", ".").Replace("\\*", ".*") + "$";
		}

		#endregion Methods
	}
}
