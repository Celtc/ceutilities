﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;
using CEUtilities.Structs;

namespace CEUtilities.API
{
	public class FFMPEG : Singleton<FFMPEG>
	{
		#region Sub Classes / Structs

		/// <summary>
		/// Is the argument from the input section, or the output
		/// </summary>
		public enum ArgumentType
		{
			Input,
			Output
		}

		/// <summary>
		/// Encoder input mode
		/// </summary>
		public enum InputMode
		{
			Images,
			Video
		}

		#endregion

		#region Exposed fields

		[SerializeField]
		private InputMode inputMode = InputMode.Images;

		[SerializeField]
		private bool externalAudio = false;

		[SerializeField]
		private string inputFormat = "JPG";
		[SerializeField]
		private string outputFormat = "MP4";

		[SerializeField]
		private string inputArguments;
		[SerializeField]
		private string outputVideoArguments;
		[SerializeField]
		private string outputAudioArguments;

		[SerializeField]
		private string outputFolder = @"%USERPROFILE%\Desktop";

		[Header("Watermark")]

		[SerializeField]
		private Texture2D watermark;
		[SerializeField]
		private Pivot pivot = Pivot.BottomLeft;
		[SerializeField]
		private Margin margins = new Margin();
		[SerializeField]
		[Range(0, 1)]
		private float alpha = 1;

        [Header("Config file")]
        [InfoBox("Config paths are relative to the streaming folder")]
        [SerializeField]
        private bool readConfig = false;
        [SerializeField]
        private string configFile = "FFMPEG.config";

        #endregion Exposed fields

        #region Internal fields

        private Queue<string> results = new Queue<string>();

		private Dictionary<int, string> processesFile = new Dictionary<int, string>();

		private List<Texture2D> inputFrames = new List<Texture2D>();
		private string inputAudioFilepath = null;

		private object inputFrames_mutex = new object();

		#endregion Internal fields

		#region Custom Events

		/// <summary>
		/// Call when the process start encoding
		/// </summary>
		public UnityEvent OnEncondeStart = new UnityEvent();
		/// <summary>
		/// Call when the process ends it's encoding
		/// </summary>
		public UnityEvent_String OnEncondeCompleted = new UnityEvent_String();
		/// <summary>
		/// Call when the encoding fails
		/// </summary>
		public UnityEvent OnEncondeFailed = new UnityEvent();
		/// <summary>
		/// Call when the progress change
		/// </summary>
		public UnityEvent_Float OnEncondeProgressChange = new UnityEvent_Float();

        #endregion Custom Events

        #region Properties (public)

        public bool ReadConfig
        {
            get
            {
                return readConfig;
            }

            set
            {
                readConfig = value;
            }
        }

        public string ConfigFile
        {
            get
            {
                return configFile;
            }

            set
            {
                configFile = value;
            }
        }

        protected virtual string TemplateArguments
		{
			get
			{
				return "{input_args} -i \"{input_v_path}\" {output_v_args} \"{output_path}\"";
			}
		}

		protected virtual string TemplateArgumentsWithAudio
		{
			get
			{
				return "{input_args} -i \"{input_v_path}\" -i \"{input_a_path}\" {output_a_args} {output_v_args} \"{output_path}\"";
			}
		}

		public string ExecutablePath
		{
			get
			{
				return Path.Combine(Application.streamingAssetsPath, "FFMPEG") + @"\ffmpeg.exe";
			}
		}

		public InputMode ProcessInputMode
		{
			get
			{
				return inputMode;
			}

			set
			{
				inputMode = value;
			}
		}

		public bool ExternalAudio
		{
			get
			{
				return externalAudio;
			}

			set
			{
				externalAudio = value;
			}
		}

		public string InputFormat
		{
			get
			{
				return inputFormat;
			}

			set
			{
				inputFormat = value;
			}
		}

		public string OutputFormat
		{
			get
			{
				return outputFormat;
			}

			set
			{
				outputFormat = value;
			}
		}

		public string InputArguments
		{
			get
			{
				return inputArguments;
			}

			set
			{
				inputArguments = value;
			}
		}

		public string OutputVideoArguments
		{
			get
			{
				return outputVideoArguments;
			}

			set
			{
				outputVideoArguments = value;
			}
		}

		public string OutputAudioArguments
		{
			get
			{
				return outputAudioArguments;
			}

			set
			{
				outputAudioArguments = value;
			}
		}

		public string OutputFolder
		{
			get
			{
				return outputFolder;
			}

			set
			{
				outputFolder = value;
			}
		}

		public List<Texture2D> InputFrames
		{
			get
			{
				return inputFrames;
			}

			set
			{
				inputFrames = value;
			}
		}

		public Texture2D Watermark
		{
			get
			{
				return watermark;
			}

			set
			{
				watermark = value;
			}
		}

		public Sprite WatermarkSprite
		{
			set
			{
				watermark = value.texture;
			}
		}

		public Pivot Pivot
		{
			get
			{
				return pivot;
			}

			set
			{
				pivot = value;
			}
		}

		public Margin Margins
		{
			get
			{
				return margins;
			}

			set
			{
				margins = value;
			}
		}

		public float Alpha
		{
			get
			{
				return alpha;
			}

			set
			{
				alpha = value;
			}
		}

        #endregion Properties

        #region Event Methods

        void Start()
        {
            // Read files
            if (ReadConfig)
                ReadConfigFile();
        }

        void Update()
		{
			ProcessQueue();
		}

		#endregion Event Methods

		#region Public Methods

		/// <summary>
		/// Add or replace an argument
		/// </summary>
		public void AddOrReplaceArgument(string key, string value, ArgumentType argType)
		{
			if (argType == ArgumentType.Input)
			{
				InputArguments = AddOrReplaceArgumentString(InputArguments, key, value);
			}
			else
			{
				OutputVideoArguments = AddOrReplaceArgumentString(OutputVideoArguments, key, value);
			}
		}

		/// <summary>
		/// set the frames of the video to be generated
		/// </summary>
		public void SetFrames(Texture2D[] frames)
		{
			lock (inputFrames_mutex)
				InputFrames = new List<Texture2D>(frames);
		}

		/// <summary>
		/// Set the input external audio
		/// </summary>
		public void SetAudio(string audiopath)
		{
			inputAudioFilepath = audiopath;
		}

		/// <summary>
		/// Parameterless call
		/// </summary>
		public void Encode()
		{
			StartCoroutine(Encode_Routine(GenerateFileName()));
		}

		/// <summary>
		/// Parameterless call
		/// </summary>
		public void Encode(string outputName)
		{
			StartCoroutine(Encode_Routine(outputName));
		}

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Read config file
        /// </summary>
        private void ReadConfigFile()
        {
            // Check for file
            var configPath = Application.streamingAssetsPath + "/" + ConfigFile;
            if (!File.Exists(configPath)) return;

            // Read content
            var sr = new StreamReader(configPath, Encoding.GetEncoding("iso-8859-1"));
            var entries = Regex.Split(sr.ReadToEnd(), "\r\n|\r|\n");
            sr.Close();
            sr.Dispose();
            sr = null;

            // Search configs
            string line;

            // Arguments
            line = entries.FirstOrDefault(x => x.StartsWith("input_arguments"));
            if (!string.IsNullOrEmpty(line)) InputArguments = line.Substring(line.IndexOf('=') + 1);

            line = entries.FirstOrDefault(x => x.StartsWith("output_video_arguments"));
            if (!string.IsNullOrEmpty(line)) OutputVideoArguments = line.Substring(line.IndexOf('=') + 1);

            line = entries.FirstOrDefault(x => x.StartsWith("output_audio_arguments"));
            if (!string.IsNullOrEmpty(line)) OutputAudioArguments = line.Substring(line.IndexOf('=') + 1);            
        }

        /// <summary>
        /// Check if the executable exists
        /// </summary>
        /// <returns></returns>
        protected virtual bool ExecutableExists()
		{
			return File.Exists(ExecutablePath);
		}

		/// <summary>
		/// Verify the existance of the temp folder and return it's path
		/// </summary>
		protected virtual string GetTempFolder(string outputName)
		{
			var path = Path.Combine(Application.temporaryCachePath, outputName);
			if (!Directory.Exists(path))
			{
				try
				{
					Directory.CreateDirectory(path);
				}
				catch
				{
					path = null;
				}
			}
			return path;
		}

		/// <summary>
		/// Expand any environment variables and verify its existance, if not create it
		/// </summary>
		/// <returns></returns>
		protected virtual string GetOutputFolder()
		{
			var expandedFolder = Environment.ExpandEnvironmentVariables(OutputFolder);
			if (!Directory.Exists(expandedFolder))
			{
				try
				{
					Directory.CreateDirectory(expandedFolder);
				}
				catch
				{
					expandedFolder = null;
				}
			}
			return expandedFolder;
		}

		/// <summary>
		/// Verify paths and files
		/// </summary>
		protected virtual bool EarlyChecks(string outputName, out string temporaryFolder, out string outputFilepath)
		{
			outputFilepath = null;
			temporaryFolder = null;

			// Check for exe
			if (!ExecutableExists())
			{
				Debug.LogError("[FFMPEG] The exe was not found!");
				return false;
			}

			// Check for temp folder
			var tempFolder = GetTempFolder(outputName);
			if (tempFolder == null)
			{
				Debug.LogError("[FFMPEG] Couldn't create temporary folder!");
				return false;
			}

			// Check for output folder
			var outputFolder = GetOutputFolder();
			if (outputFolder == null)
			{
				Debug.LogError("[FFMPEG] Couldn't create output folder!");
				return false;
			}

			// Assign exit values
			temporaryFolder = tempFolder;
			outputFilepath = Path.Combine(outputFolder, outputName + "." + OutputFormat);
			return true;
		}

		/// <summary>
		/// Replace the arguments template with actual values
		/// </summary>
		protected virtual string FormatArguments(string inputFilepath, string outputFilepath)
		{
			return TemplateArguments
				.Replace("{input_args}", InputArguments)
				.Replace("{input_v_path}", inputFilepath)
				.Replace("{output_v_args}", OutputVideoArguments)
				.Replace("{output_path}", outputFilepath);
		}

		/// <summary>
		/// Replace the arguments template with actual values
		/// </summary>
		protected virtual string FormatArguments(string inputVideoFilepath, string inputAudioFilepath, string outputFilepath)
		{
			if (ExternalAudio && string.IsNullOrEmpty(inputAudioFilepath))
				return FormatArguments(inputVideoFilepath, outputFilepath);

			return TemplateArgumentsWithAudio
				.Replace("{input_args}", InputArguments)
				.Replace("{input_v_path}", inputVideoFilepath)
				.Replace("{input_a_path}", inputAudioFilepath)
				.Replace("{output_v_args}", OutputVideoArguments)
				.Replace("{output_a_args}", OutputAudioArguments)
				.Replace("{output_path}", outputFilepath);
		}

		/// <summary>
		/// Replace an argument
		/// </summary>
		protected virtual string AddOrReplaceArgumentString(string currentArgs, string key, string value)
		{
			var index = currentArgs.IndexOf("-" + key);
			if (index == -1)
			{
				return currentArgs + " -" + key + " " + value;
			}
			else
			{
				// Get the string to replace
				var endIndex = currentArgs.IndexOf(" -", index + 1);
				string toReplace;
				if (endIndex == -1)
				{
					toReplace = currentArgs.Substring(index);
				}
				else
				{
					toReplace = currentArgs.Substring(index, endIndex - index);
				}

				// Replace the string
				return currentArgs.Replace(toReplace, "-" + key + " " + value);
			}
		}

		/// <summary>
		/// Generates a filename with the format yyyyMMddHHmmssffff
		/// </summary>
		protected virtual string GenerateFileName()
		{
			return DateTime.Now.ToString("yyyyMMddHHmmssffff");
		}

		/// <summary>
		/// Images input mode
		/// </summary>
		protected virtual int PrepareImagesInput(string temporaryFolder, out string inputFilepath)
		{
			inputFilepath = null;

			// Retrieve image format
			int mode = -1;
			if (InputFormat.ToUpper() == "JPG")
				mode = 0;
			else if (InputFormat.ToUpper() == "PNG")
				mode = 1;
			else
			{
				Debug.LogError("[FFMPEG] Image input format not supported!");
				OnEncondeFailed.Invoke();
				return 2;
			}

			// Start using input frames
			lock (inputFrames_mutex)
			{
				// Has images?
				if (InputFrames == null || InputFrames.Count <= 0)
				{
					Debug.LogError("[FFMPEG] No input images provided!");
					OnEncondeFailed.Invoke();
					return 1;
				}

				// Add watermarks
				if (Watermark != null)
					InputFrames.ForEach((frame) => frame.AddWatermark(Watermark, Alpha, Margins, Pivot));

				// Custom arrange
				//CustomFramesArrange(InputFrames);

				// Save them to temporary folder
				int counter = 0;
				InputFrames.ForEach((frame) =>
				{
					// Encode texture into image
					var bytes = mode == 0 ? frame.EncodeToJPG() : frame.EncodeToPNG();
					Destroy(frame);

					// Write it
					File.WriteAllBytes(Path.Combine(temporaryFolder, counter++.ToString() + "." + InputFormat), bytes);
				});

				// Clear
				InputFrames.Clear();
				InputFrames = null;
			}

			// Format inputfilepath
			inputFilepath = Path.Combine(temporaryFolder, "%d." + InputFormat);

			return 0;
		}

		/// <summary>
		/// Video input mode
		/// </summary>
		protected virtual int PrepareVideoInputMode(string temporaryFolder, out string inputFilepath)
		{
			inputFilepath = null;

			Debug.LogError("[FFMPEG] Video input mode not implemented!");
			OnEncondeFailed.Invoke();
			return -1;
		}

		/// <summary>
		/// Start processing
		/// </summary>
		protected virtual void StartProcess(string inputVideoFilepath, string inputAudioFilepath, string outputFilepath)
		{
			// Process start parameters
			var info = new System.Diagnostics.ProcessStartInfo();
			info.CreateNoWindow = true;
			info.UseShellExecute = false;
			info.RedirectStandardInput = true;
			info.RedirectStandardOutput = true;
			info.RedirectStandardError = true;

			// Exe arguments
			info.WorkingDirectory = Path.GetDirectoryName(inputVideoFilepath);
			info.FileName = ExecutablePath;
			info.Arguments = FormatArguments(inputVideoFilepath, inputAudioFilepath, outputFilepath);

			// Create process
			var process = new System.Diagnostics.Process();
			process.StartInfo = info;
			process.OutputDataReceived += Process_DataReceived;
			process.ErrorDataReceived += Process_DataReceived;

			// Start the process
			process.Start();
			process.BeginOutputReadLine();
			process.BeginErrorReadLine();

			// Add to dict
			processesFile.Add(process.Id, outputFilepath);

			// Event call
			Debug.Log(string.Format("[FFMPEG] Starting {0} with args: {1}", info.FileName, info.Arguments));
			OnEncondeStart.Invoke();
		}

		/// <summary>
		/// Output callback
		/// </summary>
		protected virtual void Process_DataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e)
		{
			var process = sender as System.Diagnostics.Process;

			if (e.Data == null)
				return;

			var output = e.Data;
			// Progess
			if (output.StartsWith("frame="))
			{
				//
			}
			// Ended
			else if (output.StartsWith("video:"))
			{
				lock (results)
				{
					var filename = processesFile[process.Id];
					var result = string.Format("{0}{1}", "I", filename);
					processesFile.Remove(process.Id);
					results.Enqueue(result);
				}
			}
		}

		/// <summary>
		/// Unity synched thread treatmennt
		/// </summary>
		protected virtual void ProcessQueue()
		{
			lock (results)
			{
				while (results.Count > 0)
				{
					var entry = results.Dequeue();
					var filename = entry.Substring(1);

					if (entry[0] == 'I')
					{
						Debug.Log("[FFMPEG] Encoded file " + filename);
						OnEncondeCompleted.Invoke(filename);
					}
					else if (entry[0] == 'E')
					{
						Debug.LogError("[FFMPEG] Failed to encode file " + filename);
						OnEncondeFailed.Invoke();
					}
				}
			}
		}

		/// <summary>
		/// Custom frames arrange
		/// </summary>
		protected virtual void CustomFramesArrange(List<Texture2D> frames)
		{
			// Triplicate every 2 frames
			for (int i = 0; i < 3; i++)
				for (int ii = 0; ii < 2; ii++)
					frames.InsertRange(2 + 6 * i, frames.GetRange(6 * i, 2));
		}


		/// <summary>
		/// Starts the encoding process
		/// </summary>
		protected IEnumerator Encode_Routine(string outputName)
		{
			// Working paths
			string tempFolder, outputFilepath;

			// Early check
			if (!EarlyChecks(outputName, out tempFolder, out outputFilepath))
			{
				Debug.LogError("[FFMPEG] Enconding failed!");
				OnEncondeFailed.Invoke();
				yield break;
			}

			// Split to input mode
			int error = 0;
			string inputVideoFilepath = null;
			switch (ProcessInputMode)
			{
				case InputMode.Images:
				error = PrepareImagesInput(tempFolder, out inputVideoFilepath);
				break;
				case InputMode.Video:
				error = PrepareVideoInputMode(tempFolder, out inputVideoFilepath);
				break;
			}

			// Start process
			if (error == 0)
				StartProcess(inputVideoFilepath, inputAudioFilepath, outputFilepath);
		}

		#endregion Non Public Methods
	}
}
