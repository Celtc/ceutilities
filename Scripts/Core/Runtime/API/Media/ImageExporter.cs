﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.Events;

using CEUtilities.Structs;

namespace CEUtilities.API
{
	public class ImageExporter : MonoBehaviour
	{
		#region Exposed fields

		public const int DEFAULT_QUALITY = 95;

		[SerializeField]
		private string outputFolder = @"%USERPROFILE%\Desktop";

		[Header("Watermark")]

		[SerializeField]
		private Texture2D watermark;
		[SerializeField]
		private Pivot pivot = Pivot.BottomLeft;
		[SerializeField]
		private Margin margins = new Margin();
		[SerializeField]
		[Range(0, 1)]
		private float alpha = 1;

		#endregion Exposed fields

		#region Internal fields

		#endregion Internal fields

		#region Custom Events

		public UnityEvent_String OnExported = new UnityEvent_String();

		public UnityEvent OnFailed = new UnityEvent();

		#endregion Custom Events

		#region Properties

		public string OutputFolder
		{
			get
			{
				return outputFolder;
			}

			set
			{
				outputFolder = value;
			}
		}

		public Texture2D Watermark
		{
			get
			{
				return watermark;
			}

			set
			{
				watermark = value;
			}
		}

		public Sprite WatermarkSprite
		{
			set
			{
				watermark = value.texture;
			}
		}

		public Pivot Pivot
		{
			get
			{
				return pivot;
			}

			set
			{
				pivot = value;
			}
		}

		public Margin Margins
		{
			get
			{
				return margins;
			}

			set
			{
				margins = value;
			}
		}

		public float Alpha
		{
			get
			{
				return alpha;
			}

			set
			{
				alpha = value;
			}
		}

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		public void ExportImage(Texture2D image)
		{
			ExportImage(image, DEFAULT_QUALITY);
		}

		public void ExportImage(Texture2D image, int quality)
		{
			string path = string.Empty;
			bool error = false;
			Exception errorEx = null;

			try
			{
				// Path
				var expandedFolder = Environment.ExpandEnvironmentVariables(OutputFolder);
				if (!Directory.Exists(expandedFolder))
				{
					try
					{
						Directory.CreateDirectory(expandedFolder);
					}
					catch
					{
						expandedFolder = null;
					}

				}

				// File
				path = Path.Combine(expandedFolder, GenerateFileName() + ".jpg");
				var file = File.Open(path, FileMode.Create);
				using (var bw = new BinaryWriter(file))
				{
					// Prepare image
					if (Watermark != null)
						AddWatermark(image);

					// Save bytes
					bw.Write(image.EncodeToJPG(quality));
					bw.Close();
				}
			}
			catch (Exception ex)
			{
				error = true;
				errorEx = ex;
			}

			// Event
			if (error)
			{
				Debug.LogError("[ImageExporter] Exporting fail: " + errorEx.Message);
				OnFailed.Invoke();
			}
			else
			{
				Debug.Log("[ImageExporter] Created file " + path);
				OnExported.Invoke(path);
			}
		}

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Generates a filename with the format yyyyMMddHHmmssffff
		/// </summary>
		protected virtual string GenerateFileName()
		{
			return DateTime.Now.ToString("yyyyMMddHHmmssffff");
		}

		/// <summary>
		/// Adds a watermark
		/// </summary>
		protected virtual void AddWatermark(Texture2D image)
		{
			// Add watermarks
			if (Watermark != null)
				image.AddWatermark(Watermark, Alpha, Margins, Pivot);
		}

		#endregion Non Public Methods
	}
}