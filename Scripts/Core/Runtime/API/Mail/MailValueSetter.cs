﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.API
{
    public class MailValueSetter : SerializedMonoBehaviour
    {
        #region Enum

        [Flags]
        public enum MailValueType
        {
            Username = 1 << 0,
            Password = 1 << 1,
            SourceMail = 1 << 2,
            SourceDisplayName = 1 << 3,
            DstMail = 1 << 4,
            DstDisplayName = 1 << 5,
            CcMail = 1 << 6,
            CcDisplayName = 1 << 7,
            BccMail = 1 << 8,
            BccDisplayName = 1 << 9,
            Subject = 1 << 10,
            Body = 1 << 11
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private Func<string> valueGetter;

        [SerializeField]
        [HideInInspector]
        private MailValueType valueType;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the type of the value.
        /// </summary>
        /// <value>
        /// The type of the value.
        /// </value>
        [ShowInInspector]
        public MailValueType ValueType
        {
            get
            {
                return valueType;
            }

            set
            {
                valueType = value;
            }
        }

        /// <summary>
        /// Gets or sets the value getter.
        /// </summary>
        /// <value>
        /// The value getter.
        /// </value>
        [ShowInInspector]
        public Func<string> ValueGetter
        {
            get
            {
                return valueGetter;
            }

            set
            {
                valueGetter = value;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Sets the value in the mail API.
        /// </summary>
        public void Set()
        {
            if (ValueGetter == null)
            {
                Debug.LogWarning("[MailValueSetter] Value Getter not asigned.");
                return;
            }

            var mail = Mail.Instance;
            var value = ValueGetter() ?? string.Empty;

            if (ValueType.HasFlag(MailValueType.Username))
            {
                mail.Username = value;
            }
            if (ValueType.HasFlag(MailValueType.Password))
            {
                mail.Password = value;
            }
            if (ValueType.HasFlag(MailValueType.SourceMail))
            {
                mail.SourceMail = value;
            }
            if (ValueType.HasFlag(MailValueType.SourceDisplayName))
            {
                mail.SourceDisplayName = value;
            }
            if (ValueType.HasFlag(MailValueType.DstMail))
            {
                mail.DstMail = value;
            }
            if (ValueType.HasFlag(MailValueType.DstDisplayName))
            {
                mail.DstDisplayName = value;
            }
            if (ValueType.HasFlag(MailValueType.CcMail))
            {
                mail.CcMail = value;
            }
            if (ValueType.HasFlag(MailValueType.CcDisplayName))
            {
                mail.CcDisplayName = value;
            }
            if (ValueType.HasFlag(MailValueType.BccMail))
            {
                mail.BccMail = value;
            }
            if (ValueType.HasFlag(MailValueType.BccDisplayName))
            {
                mail.BccDisplayName = value;
            }
            if (ValueType.HasFlag(MailValueType.Subject))
            {
                mail.Subject = value;
            }
            if (ValueType.HasFlag(MailValueType.Body))
            {
                mail.Body = value;
            }
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}