﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.ComponentModel;
using System.Text.RegularExpressions;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;

namespace CEUtilities.API
{
	/// <summary>
	/// Important! It's requires that the application has Api Compatibility Level: .NET 2.0
	/// </summary>
	public class Mail : Singleton<Mail>
	{
		#region Exposed fields

		[Header("Connections")]
		[SerializeField]
		private bool useSSL = true;
		[SerializeField]
		private int mailServerPort = 587;
		[SerializeField]
		private string mailServerHost = "smtp.gmail.com";

		[Header("Setup")]
		[SerializeField]
		private string username;
		[SerializeField]
		private string password;
		[SerializeField]
		private string sourceMail;
		[SerializeField]
		private string sourceDisplayName;
		[SerializeField]
		private string dstMail;
		[SerializeField]
		private string dstDisplayName;
		[SerializeField]
		private string ccMail;
		[SerializeField]
		private string ccDisplayName;
		[SerializeField]
		private string bccMail;
		[SerializeField]
		private string bccDisplayName;
		[SerializeField]
		private string subject;
		[SerializeField]
		[OnValueChanged("BodyChanged")]
		private string body;
		[SerializeField]
		private bool htmlBody;

		[Header("Exporting")]
		[SerializeField]
		private string outputXMLFolder = @"%USERPROFILE%\Desktop\Mails";
		[SerializeField]
		private bool includeFileInXML = false;

		[Header("Config file")]
		[InfoBox("Config paths are relative to the streaming folder")]
		[SerializeField]
		private bool readConfig = false;
		[SerializeField]
		private string configFile = "Mail.config";
		[SerializeField]
		private bool readBody = false;
		[SerializeField]
		private string bodyFile = "MailBody.txt";

		#endregion Exposed fields

		#region Internal fields

		private string finalBody = null;
		private List<Attachment> attachments = new List<Attachment>();

		private Queue<string> results = new Queue<string>();

		#endregion Internal fields

		#region Custom Events

		[SerializeField]
		public static UnityEvent_String OnMailSent = new UnityEvent_String();
		[SerializeField]
		public static UnityEvent_String OnMailFailed = new UnityEvent_String();

		#endregion Custom Events

		#region Properties (public)

		public string Username
		{
			get
			{
				return username;
			}

			set
			{
				username = value;
			}
		}

		public string SourceMail
		{
			get
			{
				return sourceMail;
			}

			set
			{
				sourceMail = value;
			}
		}

		public string SourceDisplayName
		{
			get
			{
				return sourceDisplayName;
			}

			set
			{
				sourceDisplayName = value;
			}
		}

		public string Password
		{
			get
			{
				return password;
			}

			set
			{
				password = value;
			}
		}

		public string DstMail
		{
			get
			{
				return dstMail;
			}

			set
			{
				dstMail = value;
			}
		}

		public string DstDisplayName
		{
			get
			{
				return dstDisplayName;
			}

			set
			{
				dstDisplayName = value;
			}
		}

		public string CcMail
		{
			get
			{
				return ccMail;
			}

			set
			{
				ccMail = value;
			}
		}

		public string CcDisplayName
		{
			get
			{
				return ccDisplayName;
			}

			set
			{
				ccDisplayName = value;
			}
		}

		public string BccMail
		{
			get
			{
				return bccMail;
			}

			set
			{
				bccMail = value;
			}
		}

		public string BccDisplayName
		{
			get
			{
				return bccDisplayName;
			}

			set
			{
				bccDisplayName = value;
			}
		}

		public string Subject
		{
			get
			{
				return subject;
			}

			set
			{
				subject = value;
			}
		}

		public string Body
		{
			get
			{
				return body;
			}

			set
			{
				body = value;

				BodyChanged();
			}
		}

		public string FinalBody
		{
			get
			{
				return finalBody;
			}

			set
			{
				finalBody = value;
			}
		}

		public bool HtmlBody
		{
			get
			{
				return htmlBody;
			}

			set
			{
				htmlBody = value;
			}
		}

		public List<Attachment> Attachments
		{
			get
			{
				return attachments;
			}

			set
			{
				attachments = value;
			}
		}

		public bool UseSSL
		{
			get
			{
				return useSSL;
			}

			set
			{
				useSSL = value;
			}
		}

		public int MailServerPort
		{
			get
			{
				return mailServerPort;
			}

			set
			{
				mailServerPort = value;
			}
		}

		public string MailServerHost
		{
			get
			{
				return mailServerHost;
			}

			set
			{
				mailServerHost = value;
			}
		}

		public string OutputXMLFolder
		{
			get
			{
				return outputXMLFolder;
			}

			set
			{
				outputXMLFolder = value;
			}
		}

		public string ConfigFile
		{
			get
			{
				return configFile;
			}

			set
			{
				configFile = value;
			}
		}

		public string BodyFile
		{
			get
			{
				return bodyFile;
			}

			set
			{
				bodyFile = value;
			}
		}

		public bool ReadConfig
		{
			get
			{
				return readConfig;
			}

			set
			{
				readConfig = value;
			}
		}

		public bool ReadBody
		{
			get
			{
				return readBody;
			}

			set
			{
				readBody = value;
			}
		}

		#endregion Properties

		#region Unity events

		void Start()
		{
			// Read files
			if (ReadConfig)
				ReadConfigFile();

			// Read body
			if (ReadBody)
				ReadBodyFile();

			// Setup final body
			finalBody = Body;
		}

		void Update()
		{
			ProcessQueue();
		}

		#endregion Unity events

		#region Public Methods

		/// <summary>
		/// Attach a new fle.
		/// </summary>
		public void AttachFile(string filename)
		{
			Attachments.Add(new Attachment(filename));
        }

        /// <summary>
        /// Attach an image using a Texture2D.
        /// </summary>
        public void AttachImage(Texture2D texture) => AttachImage(texture, 75, true);

        /// <summary>
        /// Attach an image using a Texture2D.
        /// </summary>
        public void AttachImage(Texture2D texture, int quality) => AttachImage(texture, quality, true);

        /// <summary>
        /// Attach an image using a Texture2D.
        /// </summary>
        public void AttachImage(Texture2D texture, int quality, bool forceReadable)
        {
            var targetTexture = texture;
            if (!targetTexture.IsReadable())
            {
                if (!forceReadable)
                {
                    Debug.LogWarning("[Mail] The attaching texture is not readable.");
                    return;
                }
                else
                {
                    targetTexture = texture.Duplicate(true);
                }
            }
            var stream = new MemoryStream(targetTexture.EncodeToJPG(quality));
            var name = targetTexture.name + ".JPG";
            Attachments.Add(new Attachment(stream, name));

            Debug.Log("[Mail] Attached texture: " + name);
        }

        /// <summary>
        /// Attach a new fle
        /// </summary>
        public void AttachFile(string name, Texture2D tex, int quality = 95)
		{
			Texture2D attachingTex = null;
			if (tex.IsReadable())
				attachingTex = tex;
			else
				attachingTex = tex.Duplicate(TextureFormat.RGB24, true);

			// Verify name
			var upperName = name.ToUpper();
			if (!upperName.EndsWith(".JPG") && !upperName.EndsWith(".JPEG"))
				name += ".jpg";

			// Convert to stream
			var ms = new MemoryStream(attachingTex.EncodeToJPG(quality));
			Attachments.Add(new Attachment(ms, name));
		}

		/// <summary>
		/// Attach a new fle
		/// </summary>
		public void AttachFile(string name, Stream stream)
		{
			Attachments.Add(new Attachment(stream, name));
		}

		/// <summary>
		/// Clear all the attachments
		/// </summary>
		public void ClearAttachments()
		{
			Attachments.Clear();
		}


		/// <summary>
		/// Generates a new XML file for the specified mail
		/// </summary>
		[Button]
		[HideInEditorMode]
		public void ExportMail()
		{
			ExportMail(true);
		}

		/// <summary>
		/// Generates a new XML file for the specified mail
		/// </summary>
		public void ExportMail(bool clearAttachments)
		{
			// Send
			InternalExportMail();

			// Clear
			if (clearAttachments)
				Attachments.Clear();
		}


		/// <summary>
		/// Import a previous exported mail and send it
		/// </summary>
		public void SendExportedMail(string xmlPath)
		{
			try
			{
				// Open XML
				var xmlData = XDocument.Load(xmlPath);

				// Get Values
				var dstMail = xmlData.Root.Element("mail").Value;
				var dstDisplayName = xmlData.Root.Element("name").Value;
				var ccMail = xmlData.Root.Element("ccMail").Value;
				var ccDisplayName = xmlData.Root.Element("ccName").Value;
				var bccMail = xmlData.Root.Element("bccMail").Value;
				var bccDisplayName = xmlData.Root.Element("bccName").Value;
				var subject = xmlData.Root.Element("subject").Value;
				var body = xmlData.Root.Element("body").Value;
				var htmlBody = bool.Parse(xmlData.Root.Element("htmlBody").Value);

				// Get attachments
				var attachments = new List<Attachment>();
				foreach (var attachment in xmlData.Root.Element("attachments").Elements("attachment"))
				{
					var name = attachment.Element("name").Value;
					var data = Convert.FromBase64String(attachment.Element("data").Value);
					attachments.Add(new Attachment(new MemoryStream(data), name));
				}

				// Send
				InternalSendMail(Username, SourceMail, SourceDisplayName, Password,
					dstMail, dstDisplayName, ccMail, ccDisplayName, bccMail, bccDisplayName, subject, body, htmlBody, attachments);
			}
			catch (Exception ex)
			{
				Debug.LogError("[Mail] Error parsing the mail XML file \"" + xmlPath + "\": " + ex.Message);
			}
		}


		/// <summary>
		/// Sends an email with the current set values
		/// </summary>
		[Button]
		[HideInEditorMode]
		public void SendMail()
		{
			SendMail(true);
		}

		/// <summary>
		/// Sends an email with the current set values
		/// </summary>
		public void SendMail(bool clearAttachments)
		{
			// Send
			InternalSendMail(Username, SourceMail, SourceDisplayName, Password, DstMail, DstDisplayName, CcMail, CcDisplayName, BccMail, BccDisplayName, Subject, FinalBody, HtmlBody, Attachments);

            // Clear
            if (clearAttachments)
            {
                Attachments.Clear();
            }
		}

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Triggers when the main body changes
		/// </summary>
		private void BodyChanged()
		{
			FinalBody = Body;
		}


		/// <summary>
		/// Read config file
		/// </summary>
		private void ReadConfigFile()
		{
			// Check for file
			var configPath = Application.streamingAssetsPath + "/" + ConfigFile;
			if (!File.Exists(configPath))
				return;

			// Read content
			var sr = new StreamReader(configPath, Encoding.GetEncoding("iso-8859-1"));
			var entries = Regex.Split(sr.ReadToEnd(), "\r\n|\r|\n");
			sr.Close();
			sr.Dispose();
			sr = null;

			// Search configs
			string line;

			// Connection
			line = entries.FirstOrDefault(x => x.StartsWith("useSSL"));
			if (!string.IsNullOrEmpty(line))
				UseSSL = line.Substring(line.IndexOf('=') + 1).ToUpper() == "TRUE" ? true : false;

			line = entries.FirstOrDefault(x => x.StartsWith("mailServerPort"));
			if (!string.IsNullOrEmpty(line))
				MailServerPort = int.Parse(line.Substring(line.IndexOf('=') + 1));

			line = entries.FirstOrDefault(x => x.StartsWith("mailServerHost"));
			if (!string.IsNullOrEmpty(line))
				MailServerHost = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("username"));
			if (!string.IsNullOrEmpty(line))
				Username = line.Substring(line.IndexOf('=') + 1);

			// Config
			line = entries.FirstOrDefault(x => x.StartsWith("sourceMail"));
			if (!string.IsNullOrEmpty(line))
				SourceMail = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("sourceDisplayName"));
			if (!string.IsNullOrEmpty(line))
				SourceDisplayName = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("password"));
			if (!string.IsNullOrEmpty(line))
				Password = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("ccMail"));
			if (!string.IsNullOrEmpty(line))
				CcMail = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("ccDisplayName"));
			if (!string.IsNullOrEmpty(line))
				CcDisplayName = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("bccMail"));
			if (!string.IsNullOrEmpty(line))
				BccMail = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("bccDisplayName"));
			if (!string.IsNullOrEmpty(line))
				BccDisplayName = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("subject"));
			if (!string.IsNullOrEmpty(line))
				Subject = line.Substring(line.IndexOf('=') + 1);

			line = entries.FirstOrDefault(x => x.StartsWith("htmlBody"));
			if (!string.IsNullOrEmpty(line))
				HtmlBody = line.Substring(line.IndexOf('=') + 1).ToUpper() == "TRUE" ? true : false;
		}

		/// <summary>
		/// Read the mail body
		/// </summary>
		private void ReadBodyFile()
		{
			// Check for file
			var filePath = Application.streamingAssetsPath + "/" + BodyFile;
			if (!File.Exists(filePath))
				return;

			// Read body		
			using (var sr = new StreamReader(filePath))
			{
				Body = sr.ReadToEnd();
				sr.Close();
			}
		}


		/// <summary>
		/// Sends an email to a destination using another mail as source.
		/// Requires to enable access to less secure apps from the control panel of the source mail: https://www.google.com/settings/security/lesssecureapps.
		/// </summary>
		/// <param name="data">Data structure</param>
		private void InternalSendMail(string username, string sourceMail, string sourceDisplayName, string password, string dstMail, string dstDisplayName,
			string ccMail, string ccDisplayName, string bccMail, string bccDisplayName,
			string subject, string body, bool htmlBody = false, List<Attachment> attachments = null)
		{
			// Early check (min params)
			if (string.IsNullOrEmpty(sourceMail) ||
				string.IsNullOrEmpty(password) ||
				string.IsNullOrEmpty(dstMail))
			{
				Debug.LogError("[Mail] " + dstDisplayName + "	" + dstMail + "	" + attachments[0].Name + "	" + DateTime.Now + "	" + "Error: Mail not defined");
				return;
			}

			Debug.Log("[Mail] Enqueueing mail send...");

			try
			{
				MailMessage mail = new MailMessage();

				// Mail source
				if (sourceDisplayName != null && sourceDisplayName != string.Empty)
				{
					mail.From = new MailAddress(sourceMail, sourceDisplayName);
				}
				else
				{
					mail.From = new MailAddress(sourceMail);
				}

				// Mail dst
				if (sourceDisplayName != null && sourceDisplayName != string.Empty)
				{
					mail.To.Add(new MailAddress(dstMail, dstDisplayName));
				}
				else
				{
					mail.To.Add(new MailAddress(dstMail));
				}

				// Mail cc
				if (ccMail != null && ccMail != string.Empty)
				{
					if (ccDisplayName != null && ccDisplayName != string.Empty)
					{
						mail.To.Add(new MailAddress(ccMail, ccDisplayName));
					}
					else
					{
						mail.To.Add(new MailAddress(ccMail));
					}
				}

				// Mail bcc
				if (bccMail != null && bccMail != string.Empty)
				{
					if (bccDisplayName != null && bccDisplayName != string.Empty)
					{
						mail.To.Add(new MailAddress(bccMail, bccDisplayName));
					}
					else
					{
						mail.To.Add(new MailAddress(bccMail));
					}
				}

				// Subject and body
				mail.Subject = subject;
				mail.Body = body;
				mail.IsBodyHtml = htmlBody;

				// Attachments
				if (attachments != null && attachments.Count > 0)
				{
					foreach (var attach in attachments)
					{
						mail.Attachments.Add(attach);
					}
				}

#pragma warning disable 0618
				//TODO: `System.Net.Mail.SmtpClient' is obsolete: `SmtpClient and its network of types are poorly designed, we strongly recommend you use https://github.com/jstedfast/MailKit and https://github.com/jstedfast/MimeKit instead'
				// Create client
				var smtpClient = new SmtpClient(MailServerHost);
				smtpClient.Port = MailServerPort;
				smtpClient.Credentials = new NetworkCredential(username, password) as ICredentialsByHost;
				smtpClient.EnableSsl = UseSSL;
				ServicePointManager.ServerCertificateValidationCallback =
					delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
					{
						return true;
					};
				smtpClient.SendCompleted += new SendCompletedEventHandler(SendCompleted_Callback);
#pragma warning restore 0618

				// Send the email with the entry data
				var logEntry = string.Format("{0}\t{1}\t{2}\t{3}\t",
					dstDisplayName,
					dstMail,
					attachments == null ? "No attachments" :
						(attachments.Count == 1 ? Path.GetFileName(attachments[0].Name) :
							(attachments.Count > 1 ? attachments.Count.ToString() :
								"No valid attachment")),
					DateTime.Now
				);
				smtpClient.SendAsync(mail, logEntry);
			}
			catch
			{
				throw;
			}
		}

		/// <summary>
		/// Exports a mail data
		/// </summary>
		private void InternalExportMail()
		{
			// Verify output folder
			var outputFolder = GetOutputFolder();
			if (outputFolder == null)
			{
				Debug.LogError("[MailExporter] Couldn't create output folder");
				return;
			}

			// Save XML
			var xmlMail =
				new XElement("mailData",
					new XElement("name", DstDisplayName),
					new XElement("mail", DstMail),
					new XElement("ccName", CcDisplayName),
					new XElement("ccMail", CcMail),
					new XElement("bccName", BccDisplayName),
					new XElement("bccMail", BccMail),
					new XElement("subject", Subject),
					new XElement("body", new XCData(FinalBody)),
					new XElement("htmlBody", HtmlBody.ToString()),
					new XElement("attachments",
						Attachments.Select(x =>
							{
								// Filepath element
								XElement pathXElement = null;
								var fs = x.ContentStream as FileStream;
								var path = fs == null ? x.Name : fs.Name;
								pathXElement = new XElement("filepath", path);

								// Filestream element
								XElement fileXElement = null;
								if (includeFileInXML)
								{
									var ms = new MemoryStream();
									x.ContentStream.CopyTo(ms);
									fileXElement = new XElement("data", new XCData(Convert.ToBase64String(ms.ToArray())));
								}

								if (includeFileInXML)
									return new XElement("attachment", pathXElement, fileXElement);
								else
									return new XElement("attachment", pathXElement);

							}
						).ToArray()
					)
				);
			xmlMail.Save(Path.Combine(outputFolder, GenerateFileName() + ".XML"));

			// Log
			var entry = string.Format("{0}\t{1}\t{2}\t{3}\t{4}",
				dstDisplayName,
				dstMail,
				Attachments == null ? "No attachments" :
					(Attachments.Count == 1 ? Path.GetFileName(Attachments[0].Name) :
						(Attachments.Count > 1 ? Attachments.Count.ToString() :
							"No valid attachment")),
				DateTime.Now,
				"Exported"
			);
			Debug.Log("[Mail] " + entry);
		}


		/// <summary>
		/// Callback for sending the email
		/// </summary>
		private void SendCompleted_Callback(object sender, AsyncCompletedEventArgs e)
		{
			String entry = e.UserState.ToString();
			if (e.Error != null)
				entry += "Error: " + e.Error.Message;
			else if (e.Cancelled)
				entry += "Cancelled";
			else
				entry += "Sent";

			lock (results)
				results.Enqueue(entry);
		}

		/// <summary>
		/// Unity synched thread treatmennt
		/// </summary>
		private void ProcessQueue()
		{
			lock (results)
			{
				while (results.Count > 0)
				{
					var entry = results.Dequeue();
					Debug.Log(entry);
					OnMailFailed.Invoke(entry);
				}
			}
		}

		/// <summary>
		/// Verify the existance of the output folder
		/// </summary>
		/// <returns></returns>
		private string GetOutputFolder()
		{
			var expandedFolder = Environment.ExpandEnvironmentVariables(OutputXMLFolder);
			if (!Directory.Exists(expandedFolder))
			{
				try
				{
					Directory.CreateDirectory(expandedFolder);
				}
				catch
				{
					expandedFolder = null;
				}
			}
			return expandedFolder;
		}

		/// <summary>
		/// Generates a filename with the format yyyyMMddHHmmssffff
		/// </summary>
		private string GenerateFileName()
		{
			return DateTime.Now.ToString("yyyyMMddHHmmssffff");
		}

		#endregion Non Public Methods
	}
}
