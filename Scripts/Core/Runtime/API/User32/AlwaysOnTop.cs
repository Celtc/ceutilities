﻿#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN

using UnityEngine;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Sirenix.OdinInspector;

namespace CEUtilities.API
{
    public class AlwaysOnTop : MonoBehaviour
    {
#if UNITY_STANDALONE
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private string windowName;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the name of the window.
        /// </summary>
        /// <value>
        /// The name of the window.
        /// </value>
        [ShowInInspector]
        [InlineButton("SetDefaultWindowName", "Lookup")]
        public string WindowName
        {
            get
            {
                return windowName;
            }

            set
            {
                windowName = value;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        void Start()
        {
            AssignTopmostWindow("Aeropuertos-ClubViajeros-Scanner", true);
        }

#if UNITY_EDITOR

        [OnInspectorGUI]
        private void OnInspectorGUI()
        {
            if (WindowName == null)
            {
                SetDefaultWindowName();
            }
        }

#endif

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Assigns the topmost window.
        /// </summary>
        /// <param name="windowTitle">The window title.</param>
        /// <param name="makeTopmost">if set to <c>true</c> [make topmost].</param>
        /// <returns></returns>
        public bool AssignTopmostWindow(string windowTitle, bool makeTopmost)
        {
            UnityEngine.Debug.LogFormat("[AlwaysOnTop] Assigning top most flag to window of title '{0}'.", windowTitle);

            var rect = new User32API.RECT();
            var hWnd = User32API.FindWindow(null, WindowName);
            User32API.GetWindowRect(new HandleRef(this, hWnd), out rect);

            UnityEngine.Debug.LogFormat("[AlwaysOnTop] Window handler: {0}, rect: {1}.", hWnd, rect);

            return User32API.SetWindowPos(hWnd, makeTopmost ? User32API.HWND_TOPMOST : User32API.HWND_NOT_TOPMOST, rect.X, rect.Y, rect.Width, rect.Height, User32API.SWP_SHOWWINDOW);
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Gets the window titles.
        /// </summary>
        /// <returns></returns>
        private string[] GetWindowTitles()
        {
            var windowList = new List<string>();
            var processArray = Process.GetProcesses();
            foreach (Process p in processArray)
            {
                if (!string.IsNullOrEmpty(p.MainWindowTitle))
                {
                    windowList.Add(p.MainWindowTitle);
                }
            }
            return windowList.ToArray();
        }

#if UNITY_EDITOR

        /// <summary>
        /// Sets the default name of the window.
        /// </summary>
        private void SetDefaultWindowName()
        {
            WindowName = Application.productName;
        }

#endif

        #endregion Non Public Methods    
#endif
    }
}
#endif