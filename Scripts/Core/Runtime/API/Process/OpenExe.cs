﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System;

using Sirenix.OdinInspector;

namespace CEUtilities.API
{
    public class OpenExe : SerializedMonoBehaviour
    {
        #region Variables (private)

        [SerializeField]
        private string filepath;

        #endregion Variables

        #region Properties (public)

        /// <summary>
        /// Filepath
        /// </summary>
        public string Filepath
        {
            get
            {
                return filepath;
            }

            set
            {
                filepath = value;
            }
        }

        #endregion Properties

        #region Event Functions

        /// <summary>
        /// Called before the first frame update only if the script instance is enabled.
        /// </summary>
        void Start()
        {
            if (!string.IsNullOrEmpty(Filepath))
                Filepath = Environment.ExpandEnvironmentVariables(Filepath);
        }

        #endregion Event Functions

        #region Methods

        /// <summary>
        /// Run the exe
        /// </summary>
        [Button]
        public void Execute()
        {
            try
            {
                Process foo = new Process();
                foo.StartInfo.WorkingDirectory = Path.GetDirectoryName(Filepath);
                foo.StartInfo.FileName = Path.GetFileName(Filepath);
                foo.Start();
            }
            catch (Exception ex)
            {
                UnityEngine.Debug.LogError("[OpenEXE] Error when trying to execute the exe: " + ex.Data);
            }
        }

        #endregion Methods
    }
}
