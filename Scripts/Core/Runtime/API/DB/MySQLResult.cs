﻿using UnityEngine;
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.API
{
    [System.Serializable]
    public struct MySQLResult
    {
        #region Exposed fields

        public long lastInsertedID;

        public int commandResult;

        public DataTable queryResult;

        public Exception error;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// In case of query, the number of results.
        /// </summary>
        public int QueryResultCount => queryResult != null ? queryResult.Rows.Count : 0;

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Prepare a new result
        /// </summary>
        public MySQLResult(bool query)
        {
            lastInsertedID = -1;
            commandResult = -1;
            queryResult = query ? new DataTable() : null;
            error = null;
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}