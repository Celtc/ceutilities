﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Data;
using System.Xml.Linq;
using System.Collections;
using System.ComponentModel;

using MySql.Data.MySqlClient;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;
using CEUtilities.Coroutines;
using CEUtilities.Threading;

namespace CEUtilities.API
{
    [CustomSingleton(AutoGeneration = false, Persistent = true)]
    public class MySQL : Singleton<MySQL>
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private string host;
        [SerializeField]
        [HideInInspector]
        private string user;
        [SerializeField]
        [HideInInspector]
        private string password;
        [SerializeField]
        [HideInInspector]
        private string database;
        [SerializeField]
        [HideInInspector]
        private string port;

        [SerializeField]
        [HideInInspector]
        private bool loadSetupFromConfig;
        [SerializeField]
        [HideInInspector]
        private string configPath;

        // Debug
        [ShowInInspector]
        [FoldoutGroup("Debug", order: 2)]
        [InlineButton("DebugQuery", "Execute")]
        private string query = "Select * from user";

        [ShowInInspector]
        [FoldoutGroup("Debug")]
        [InlineButton("DebugNonQuery", "Execute")]
        private string nonQuery = "StoreProcedure";

        [ShowInInspector]
        [FoldoutGroup("Debug")]
        [Sirenix.OdinInspector.ReadOnly]
        public volatile Exception error = null;

        #endregion Exposed fields

        #region Internal fields

        private bool initedConfig = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Database host.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Setup", order: 0)]
        public string Host
        {
            get
            {
                return host;
            }

            set
            {
                host = value;
            }
        }

        /// <summary>
        /// Database name.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public string Database
        {
            get
            {
                return database;
            }

            set
            {
                database = value;
            }
        }

        /// <summary>
        /// Database user.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public string User
        {
            get
            {
                return user;
            }

            set
            {
                user = value;
            }
        }

        /// <summary>
        /// Database password.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        /// <summary>
        /// Database port.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public string Port
        {
            get
            {
                return port;
            }

            set
            {
                port = value;
            }
        }

        /// <summary>
        /// Connection string.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public string ConnectionString
        {
            get
            {
                return string.Format("Server={0};Database={1};User ID={2};Password={3};Port={4};Pooling=true",
                    Host,
                    Database,
                    User,
                    Password,
                    Port);
            }
        }


        /// <summary>
        /// If true, tries to load the setup from the config file.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Config File", order: 1)]
        public bool LoadSetupFromConfig
        {
            get
            {
                return loadSetupFromConfig;
            }

            set
            {
                loadSetupFromConfig = value;
            }
        }

        /// <summary>
        /// Path of the configuration file.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Config File")]
        [FilePath(ParentFolder = "Assets/StreamingAssets")]
        public string ConfigPath
        {
            get
            {
                return configPath;
            }

            set
            {
                configPath = value;
            }
        }

        #endregion Properties

        #region Events methods

        private void Start()
        {
            if (LoadSetupFromConfig && !initedConfig)
            {
                ReadConfig();
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Run a query
        /// </summary>
        public void RunQuery(string query, Action<DataTable> resultCallback = null)
        {
            RunQuery(query, resultCallback, null);
        }

        /// <summary>
        /// Run a query
        /// </summary>
        public void RunQuery(string query, Action<DataTable> resultCallback, params MySqlParameter[] parameters)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                StartCoroutine(_QueryAsync(query, resultCallback, parameters));
            else
                EditorCoroutines.StartCoroutine(_QueryAsync(query, resultCallback, parameters), this);
#else
            StartCoroutine(_QueryAsync(query, resultCallback, parameters));
#endif
        }

        /// <summary>
        /// Run a query
        /// </summary>
        public void RunQuery(string query, Action<MySQLResult> resultCallback, params MySqlParameter[] parameters)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                StartCoroutine(_QueryAsync(query, resultCallback, parameters));
            else
                EditorCoroutines.StartCoroutine(_QueryAsync(query, resultCallback, parameters), this);
#else
            StartCoroutine(_QueryAsync(query, resultCallback, parameters));
#endif
        }


        /// <summary>
        /// Run a command
        /// </summary>
        public void RunCommand(string cmd, Action<int> resultCallback = null)
        {
            RunCommand(cmd, resultCallback, null);
        }

        /// <summary>
        /// Run a command
        /// </summary>
        public void RunCommand(string cmd, Action<int> resultCallback, params MySqlParameter[] parameters)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                StartCoroutine(_CommandAsync(cmd, resultCallback, parameters));
            else
                EditorCoroutines.StartCoroutine(_CommandAsync(cmd, resultCallback, parameters), this);
#else
            StartCoroutine(_CommandAsync(cmd, resultCallback));
#endif
        }

        /// <summary>
        /// Run a command
        /// </summary>
        public void RunCommand(string cmd, Action<MySQLResult> resultCallback, params MySqlParameter[] parameters)
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                StartCoroutine(_CommandAsync(cmd, resultCallback, parameters));
            else
                EditorCoroutines.StartCoroutine(_CommandAsync(cmd, resultCallback, parameters), this);
#else
            StartCoroutine(_CommandAsync(cmd, resultCallback));
#endif
        }


        /// <summary>
        /// Performs an asynchronous query
        /// </summary>
        public IEnumerator _QueryAsync(string query, Action<DataTable> resultCallback, params MySqlParameter[] parameters)
        {
            yield return BackgroundWorkerDispatcher.Instance._Enqueue(
                () => DoQuery(query, parameters),
                (workerResult) => resultCallback(workerResult.result.queryResult)
            );
            //yield return _BackgroundTask(query, parameters, DoQuery, (result) => resultCallback(result.queryResult));
        }

        /// <summary>
        /// Performs an asynchronous query
        /// </summary>
        public IEnumerator _QueryAsync(string query, Action<MySQLResult> resultCallback, params MySqlParameter[] parameters)
        {
            yield return BackgroundWorkerDispatcher.Instance._Enqueue(
                () => DoQuery(query, parameters),
                (workerResult) => resultCallback(workerResult.result)
            );
            //yield return _BackgroundTask(query, parameters, DoQuery, resultCallback);
        }

        /// <summary>
        /// Performs an asynchronous non query
        /// </summary>
        public IEnumerator _CommandAsync(string cmd, Action<int> resultCallback, params MySqlParameter[] parameters)
        {
            yield return BackgroundWorkerDispatcher.Instance._Enqueue(
                () => DoNonQuery(cmd, parameters),
                (workerResult) => resultCallback(workerResult.result.commandResult)
            );
            //yield return _BackgroundTask(cmd, parameters, DoNonQuery, (result) => resultCallback(result.commandResult));
        }

        /// <summary>
        /// Performs an asynchronous non query
        /// </summary>
        public IEnumerator _CommandAsync(string cmd, Action<MySQLResult> resultCallback, params MySqlParameter[] parameters)
        {
            yield return BackgroundWorkerDispatcher.Instance._Enqueue(
                () => DoNonQuery(cmd, parameters),
                (workerResult) => resultCallback(workerResult.result)
            );
            //yield return _BackgroundTask(cmd, parameters, DoNonQuery, resultCallback);
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Read the config.
        /// </summary>
        private void ReadConfig()
        {
            // Set flag
            initedConfig = true;

            try
            {
                // Load xml
                var xDoc = XDocument.Load(Path.Combine(Application.streamingAssetsPath, configPath));

                // Look for database entry
                var xEntries = xDoc.Descendants("database");
                //if (xEntries.GetCount() > 1)
                //{
                //    Debug.LogWarning("[MySQL] Duplicated setups in config file. First one will be used.");
                //}

                // Use first only
                foreach (var xId in xEntries)
                {
                    // Host
                    var xHost = xId.Element("host");
                    if (xHost != null)
                    {
                        Host = xHost.Value;
                    }

                    // User
                    var xUser = xId.Element("user");
                    if (xUser != null)
                    {
                        User = xUser.Value;
                    }

                    // Password
                    var xPassword = xId.Element("password");
                    if (xPassword != null)
                    {
                        Password = xPassword.Value;
                    }

                    // Database
                    var xDatabase = xId.Element("database");
                    if (xDatabase != null)
                    {
                        Database = xDatabase.Value;
                    }

                    // Port
                    var xPort = xId.Element("port");
                    if (xPort != null)
                    {
                        Port = xPort.Value;
                    }

                    // Skip next setups
                    break;
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning("[MySQL] Error when trying to load the config file: " + e.Message);
            }
        }

        /// <summary>
        /// Performs a debugging query
        /// </summary>
        private void DebugQuery()
        {
            RunQuery(query, (result) => PrintDataTable(result));
        }

        /// <summary>
        /// Performs a debugging non query
        /// </summary>
        private void DebugNonQuery()
        {
            RunCommand(nonQuery, (int result) => Debug.Log("[DB] NonQuery: " + result.ToString()));
        }

        /// <summary>
        /// Prints a data table
        /// </summary>
        private void PrintDataTable(DataTable table, string info = null)
        {
            var sb = new StringBuilder("[DB]");
            if (!string.IsNullOrEmpty(info))
                sb.Append(": " + info);

            sb.Append(table.ContentToString());

            Debug.Log(sb.ToString());
        }

        #region Background Worker

        /*
        /// <summary>
        /// Performs a yieldable asynchronous task
        /// </summary>
        private IEnumerator _BackgroundTask<T>(string query, MySqlParameter[] parameters, Func<string, MySqlParameter[], T> taskCallback, Action<T> resultCallback)
        {
            T result = default(T);
            bool working = true;

            // Resets error
            error = null;

            // Setup worker
            var dbWorker = new BackgroundWorker();
            dbWorker.DoWork += (sender, e) =>
            {
                try
                {
                    result = taskCallback(query, parameters);
                }
                catch { }
                finally
                {
                    working = false;
                }
            };

            // Run and wait
            dbWorker.RunWorkerAsync();
            yield return new WaitUntil(() => !working);
            dbWorker.Dispose();

            // Result
            resultCallback?.Invoke(result);
        }
        */

        /// <summary>
        /// Query method
        /// </summary>
        private MySQLResult DoQuery(string query, MySqlParameter[] parameters)
        {
            MySQLResult result = new MySQLResult(true);
            try
            {
                using (var connection = new MySqlConnection(ConnectionString))
                {
                    // Connect
                    connection.Open();

                    // Select
                    using (var command = new MySqlCommand(query, connection))
                    {
                        if (parameters != null && parameters.Length > 0)
                            parameters.ForEach(x => command.Parameters.Add(x));

                        using (var reader = command.ExecuteReader())
                            result.queryResult.Load(reader);
                    }

                    // Disconnect
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("[DB] " + ex.ToString() + ex.InnerException);
                result.error = ex;
                //throw ex;
            }
            return result;
        }

        /// <summary>
        /// Non Query method
        /// </summary>
        private MySQLResult DoNonQuery(string cmd, MySqlParameter[] parameters)
        {
            MySQLResult result = new MySQLResult(false);
            try
            {
                using (var connection = new MySqlConnection(ConnectionString))
                {
                    // Connect
                    connection.Open();

                    // Select
                    using (var command = new MySqlCommand(cmd, connection))
                    {
                        if (parameters != null && parameters.Length > 0)
                            parameters.ForEach(x => command.Parameters.Add(x));

                        result.commandResult = command.ExecuteNonQuery();
                        result.lastInsertedID = command.LastInsertedId;
                    }

                    // Disconnect
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Debug.LogError("[DB] " + ex.ToString() + ex.InnerException);
                result.error = ex;
                //throw ex;
            }
            return result;
        }

        #endregion

        #endregion Non Public Methods
    }
}