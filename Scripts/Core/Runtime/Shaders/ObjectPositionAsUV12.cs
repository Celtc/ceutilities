﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace CEUtilities.UI
{
    [ExecuteInEditMode]
    public class ObjectPositionAsUV12 : BaseMeshEffect
    {
        public const float shiftRight16 = .0000152587890625f;
        public const float shiftLeft16 = 65536f;

        protected ObjectPositionAsUV12()
        { }

        public override void ModifyMesh(VertexHelper vh)
        {
			if (graphic == null || graphic.canvas == null) return;

            UIVertex vertex = default(UIVertex);
            Vector3 positionInCanvas = graphic.rectTransform.position - graphic.canvas.transform.position;
            for (int i = 0; i < vh.currentVertCount; i++)
            {
                vh.PopulateUIVertex(ref vertex, i);
                vertex.uv1 = new Vector2(positionInCanvas.x, positionInCanvas.y);
                vertex.uv2 = new Vector2(positionInCanvas.z, 0f);
                vh.SetUIVertex(vertex, i);
            }
        }
    }
}