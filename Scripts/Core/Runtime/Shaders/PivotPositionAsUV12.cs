﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
    [ExecuteInEditMode]
    public class PivotPositionAsUV12 : BaseMeshEffect
    {
        [InfoBox("This component will copy the pivot world position of the element into the UV1 and UV2 channels. For this to work TexCoord1 and TexCoord2 are required to be enable as additional shader channels in the drawing canvas.", InfoMessageType.Info)]

        public bool updateEveryFrame = false;

        protected PivotPositionAsUV12()
        { }

        private void LateUpdate()
        {
            if (updateEveryFrame)
            {
                graphic.SetVerticesDirty();
            }
        }

        /// <summary>
        /// Forces the refresh.
        /// </summary>
        public void ForceRefresh()
        {
            graphic.SetVerticesDirty();
        }

        /// <summary>
        /// Mesh modify event callback.
        /// </summary>
        /// <param name="vh">The vh.</param>
        public override void ModifyMesh(VertexHelper vh)
        {
            if (graphic == null || graphic.canvas == null) return;

            var vertex = default(UIVertex);
            var positionInCanvas = graphic.rectTransform.position - graphic.canvas.transform.position;
            for (int i = 0; i < vh.currentVertCount; i++)
            {
                vh.PopulateUIVertex(ref vertex, i);
                vertex.uv1 = new Vector2(positionInCanvas.x, positionInCanvas.y);
                vertex.uv2 = new Vector2(positionInCanvas.z, 0f);
                vh.SetUIVertex(vertex, i);
            }
        }
    }
}