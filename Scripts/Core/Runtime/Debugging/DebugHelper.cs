﻿using UnityEngine;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Sirenix.OdinInspector;
using System.Reflection;

public static class DebugHelper
{
    #region Structs

    internal struct Context
    {
        public bool insideIterator;
        public string classname;
        public string methodname;
        public string filename;
        public int line;
    }

    #endregion

    #region Exposed fields

    #endregion Exposed fields

    #region Internal fields

    private static StackTrace stackTrace;

    #endregion Internal fields

    #region Custom Events

    #endregion Custom Events

    #region Properties

    public static StackTrace StackTrace
    {
        get
        {
            if (stackTrace == null)
                stackTrace = new StackTrace();

            return stackTrace;
        }
    }

    #endregion Properties

    #region Events methods

    #endregion Events methods

    #region Public Methods

    /// <summary>
    /// Add metadata information to the logging message.
    /// </summary>
    /// <param name="message">Message to log</param>
    public static string Format(string message)
    {
        return InternalFormat(message, true, false, true);
    }

    /// <summary>
    /// Add metadata information to the logging message.
    /// </summary>
    /// <param name="message">Message to log</param>
    /// <param name="args">Arguments to format the message</param>
    public static string Format(string message, params string[] args)
    {
        return InternalFormat(string.Format(message, args), true, false, true);
    }

    /// <summary>
    /// Add metadata information to the logging message.
    /// </summary>
    /// <param name="message">Message to log</param>
    /// <param name="addCallerMeta">Add caller class name and method</param>
    /// <param name="addSourceMeta">Add source file and line</param>
    /// <param name="args">Arguments to format the message</param>
    public static string Format(string message, bool addCallerMeta, bool addSourceMeta, params string[] args)
    {
        return InternalFormat(string.Format(message, args), addCallerMeta, addSourceMeta, true);
    }

    /// <summary>
    /// Add metadata information to the logging message.
    /// </summary>
    /// <param name="message">Message to log</param>
    /// <param name="addCallerMeta">Add caller class name and method</param>
    /// <param name="addSourceMeta">Add source file and line</param>
    /// <param name="removeNamespace">Remove the namespace from the classname</param>
    /// <param name="args">Arguments to format the message</param>
    public static string Format(string message, bool addCallerMeta, bool addSourceMeta, bool removeNamespace, params string[] args)
    {
        return InternalFormat(string.Format(message, args), addCallerMeta, addSourceMeta, removeNamespace);
    }

    #endregion Public Methods

    #region Non Public Methods

    /// <summary>
    /// Retrieve the context data
    /// </summary>
    /// <param name="level">Being 3 the direct caller class</param>
    private static Context GetContextData(bool removeNamespace, int level)
    {
        var stackFrame = StackTrace.GetFrame(level);
        string classname = null, methodname = null;
        bool insideIterator = false;

        // Inside iterator (coroutine)
        var type = stackFrame.GetMethod().ReflectedType;
        if (type.MemberType == MemberTypes.NestedType && type.Name.LastIndexOf("__Iterator") >= 0)
        {
            insideIterator = true;
            classname = type.ReflectedType.GetAliasOrName(removeNamespace, true);
            methodname = type.Name.Substring(1, type.Name.LastIndexOf(">c__Iterator") - 1);
        }
        else
        {
            classname = stackFrame.GetMethod().ReflectedType.GetAliasOrName(removeNamespace, true);
            methodname = stackFrame.GetMethod().Name;
        }

        return new Context()
        {
            insideIterator = insideIterator,
            classname = classname,
            methodname = methodname,
            filename = stackFrame.GetFileName(),
            line = stackFrame.GetFileLineNumber()
        };
    }

    /// <summary>
    /// Appends in the string builder the caller meta data.
    /// </summary>
    private static void AppendCallerMeta(StringBuilder sb, Context context)
    {
        sb.Append("[")
            .Append(context.classname)
            .Append(":")
            .Append(context.methodname)
            .Append("] ");
    }

    /// <summary>
    /// Appends in the string builder the source meta data.
    /// </summary>
    private static void AppendSourceMeta(StringBuilder sb, Context context)
    {
        if (context.insideIterator)
        {
            sb.Append(" (at iterator block: Coroutine)");
        }
        else
        {
            sb.Append(" (at ")
                .Append(context.filename)
                .Append(":")
                .Append(context.line)
                .Append(")");
        }
    }

    /// <summary>
    /// Add metadata information to the logging message.
    /// </summary>
    /// <param name="message">Message to log</param>
    /// <param name="addCallerMeta">Add caller class name and method</param>
    /// <param name="addSourceMeta">Add source file and line</param>
    /// <param name="removeNamespace">Remove the namespaces from the classnames</param>
    private static string InternalFormat(
        string message,
        bool addCallerMeta,
        bool addSourceMeta,
        bool removeNamespace)
    {
        var sb = new StringBuilder();
        var context = GetContextData(removeNamespace, 4);

        if (addCallerMeta)
            AppendCallerMeta(sb, context);

        sb.Append(message);

        if (addSourceMeta)
            AppendSourceMeta(sb, context);

        return sb.ToString();
    }

    #endregion Non Public Methods
}