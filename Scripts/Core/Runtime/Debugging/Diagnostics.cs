﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace CEUtilities.Debugging
{
    public class Diagnostics
    {
        #region Enum

        public enum TimeUnit
        {
            Ticks,
            Miliseconds,
            Seconds
        }

        #endregion

        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private static Stack<Tuple<Stopwatch, string>> watchs;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Gets the watchs.
        /// </summary>
        /// <value>
        /// The watchs.
        /// </value>
        private static Stack<Tuple<Stopwatch, string>> Watchs
        {
            get
            {
                if (watchs == null)
                {
                    watchs = new Stack<Tuple<Stopwatch, string>>();
                }
                return watchs;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Starts a new watcher for performance.
        /// </summary>
        public static void StartWatcher() => StartWatcher("Unnamed Watch", false);

        /// <summary>
        /// Starts a new watcher for performance.
        /// </summary>
        public static void StartWatcher(string name) => StartWatcher(name, false);

        /// <summary>
        /// Starts a new watcher for performance.
        /// </summary>
        public static void StartWatcher(string name, bool doGC)
        {
            if (doGC)
            {
                GC.Collect();
            }

            var stopWatch = new Stopwatch();
            Watchs.Push(Tuple.Create(stopWatch, name));
            stopWatch.Start();
        }


        /// <summary>
        /// Ends the watcher.
        /// </summary>
        /// <returns></returns>
        public static TimeSpan EndWatcher() => EndWatcher(true, TimeUnit.Miliseconds);

        /// <summary>
        /// Ends the watcher.
        /// </summary>
        /// <returns></returns>
        public static TimeSpan EndWatcher(bool print) => EndWatcher(print, TimeUnit.Miliseconds);

        /// <summary>
        /// Ends the last watcher.
        /// </summary>
        public static TimeSpan EndWatcher(bool print, TimeUnit unit)
        {
            var stopWatchTuple = Watchs.Pop();
            stopWatchTuple.Item1.Stop();

            if (print)
            {
                var elapsedTime = unit == TimeUnit.Ticks ? stopWatchTuple.Item1.ElapsedTicks :
                    unit == TimeUnit.Miliseconds ? stopWatchTuple.Item1.ElapsedMilliseconds :
                    stopWatchTuple.Item1.ElapsedMilliseconds * 1000;

                UnityEngine.Debug.LogFormat("[Diagnostics] Stopwatch '{0}' ended, elapsed: {1} {2}.",
                    stopWatchTuple.Item2,
                    elapsedTime,
                    unit
                );
            }

            return stopWatchTuple.Item1.Elapsed;
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}