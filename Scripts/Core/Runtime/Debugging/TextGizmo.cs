﻿using UnityEngine;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

namespace CEUtilities.Debugging
{
    /// <summary>
    /// Script that shows info on screen above a GameObject (only works in the Editor)
    /// </summary>
    public class TextGizmo : MonoBehaviour
    {
        #region Enums

        public enum ObjectTextOptions
        {
            Name,
            Position,
            CustomText
        };

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private ObjectTextOptions mode;

        [SerializeField]
        [HideInInspector]
        private string customText;

        [SerializeField]
        [HideInInspector]
        private FontStyle fontStyle = FontStyle.Normal;

        [SerializeField]
        [HideInInspector]
        private int fontSize = 15;

        [SerializeField]
        [HideInInspector]
        private Color fontColor = Color.black;

        [SerializeField]
        [HideInInspector]
        private Color backgroundColor;


        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        /// <value>
        /// The mode.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public ObjectTextOptions Mode
        {
            get
            {
                return mode;
            }

            set
            {
                mode = value;
            }
        }

        /// <summary>
        /// Gets or sets the custom text.
        /// </summary>
        /// <value>
        /// The custom text.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [Indent]
        [ShowIf("Mode", ObjectTextOptions.CustomText)]
        public string CustomText
        {
            get
            {
                return customText;
            }

            set
            {
                customText = value;
            }
        }

        /// <summary>
        /// Gets or sets the font style.
        /// </summary>
        /// <value>
        /// The font style.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public FontStyle FontStyle
        {
            get
            {
                return fontStyle;
            }

            set
            {
                fontStyle = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the font.
        /// </summary>
        /// <value>
        /// The size of the font.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public int FontSize
        {
            get
            {
                return fontSize;
            }

            set
            {
                fontSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the font.
        /// </summary>
        /// <value>
        /// The color of the font.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public Color FontColor
        {
            get
            {
                return fontColor;
            }

            set
            {
                fontColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the color of the background.
        /// </summary>
        /// <value>
        /// The color of the background.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public Color BackgroundColor
        {
            get
            {
                return backgroundColor;
            }

            set
            {
                backgroundColor = value;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        /// <summary>
        /// Awakes this instance.
        /// </summary>
        private void Awake()
        {
            if (!ApplicationHelper.PlatformIsEditor)
            {
                Destroy(this);
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        private void Start()
        {
            // Just to show the enable toggle
        }

#if UNITY_EDITOR

        /// <summary>
        /// Draw a sphere to signal the object's position on editor view
        /// </summary>
        private void OnDrawGizmos()
        {
            if (enabled)
            {
                // Get style
                var style = new GUIStyle();
                style.normal.textColor = FontColor;
                style.fontSize = FontSize;
                style.fontStyle = FontStyle;

                // Get background image
                Texture2D backgroundTexture = null;
                if (BackgroundColor.a > 0.01f)
                {
                    backgroundTexture = new Texture2D(1, 1, TextureFormat.ARGB32, false);
                    backgroundTexture.SetPixel(0, 0, BackgroundColor);
                    backgroundTexture.Apply();
                    backgroundTexture.hideFlags = HideFlags.DontSave;
                    style.normal.background = backgroundTexture;
                }

                // Set text
                var text = string.Empty;
                switch (Mode)
                {
                    case ObjectTextOptions.Name:
                        text = gameObject.name;
                        break;

                    case ObjectTextOptions.Position:
                        text = transform.position.ToString();
                        break;

                    case ObjectTextOptions.CustomText:
                        text = CustomText;
                        break;

                    default:
                        text = gameObject.name;
                        break;
                }

                // Draw
                UnityEditor.Handles.Label(transform.position, text, style);

                // Release
                if (backgroundTexture != null)
                {
                    DestroyImmediate(backgroundTexture);
                }
            }
        }

#endif

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}