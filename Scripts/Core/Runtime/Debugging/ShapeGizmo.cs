﻿using UnityEngine;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

namespace CEUtilities.Debugging
{
    /// <summary>
    /// Script that shows the location of a GameObject (only works in the Editor)
    /// </summary>
    public class ShapeGizmo : MonoBehaviour
    {
        #region Enums

        public enum GizmoShape
        {
            Sphere,
            Cube
        };

        #endregion Enums

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private GizmoShape shape = GizmoShape.Sphere;

        [SerializeField]
        [HideInInspector]
        private float radius = 1f;

        [SerializeField]
        [HideInInspector]
        private Vector3 scale = Vector3.one;

        [SerializeField]
        [HideInInspector]
        private Vector3 offset = Vector3.zero;

        [SerializeField]
        [HideInInspector]
        private Color color = Color.white;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the shape.
        /// </summary>
        /// <value>
        /// The shape.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public GizmoShape Shape
        {
            get
            {
                return shape;
            }

            set
            {
                shape = value;
            }
        }

        /// <summary>
        /// Gets or sets the radius.
        /// </summary>
        /// <value>
        /// The radius.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [Indent]
        [ShowIf("Shape", GizmoShape.Sphere)]
        public float Radius
        {
            get
            {
                return radius;
            }

            set
            {
                radius = value;
            }
        }

        /// <summary>
        /// Gets or sets the scale.
        /// </summary>
        /// <value>
        /// The scale.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [Indent]
        [ShowIf("Shape", GizmoShape.Cube)]
        public Vector3 Scale
        {
            get
            {
                return scale;
            }

            set
            {
                scale = value;
            }
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public Vector3 Offset
        {
            get
            {
                return offset;
            }

            set
            {
                offset = value;
            }
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public Color Color
        {
            get
            {
                return color;
            }

            set
            {
                color = value;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        /// <summary>
        /// Awakes this instance.
        /// </summary>
        private void Awake()
        {
            if (!ApplicationHelper.PlatformIsEditor)
            {
                DestroyImmediate(this);
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        private void Start()
        {
            // Just to show the enable toggle
        }

#if UNITY_EDITOR

        /// <summary>
        /// Draw a sphere to signal the object's position on editor view
        /// </summary>
        private void OnDrawGizmos()
        {
            if (enabled)
            {
                var cachedColor = Gizmos.color;
                Gizmos.color = Color;

                if (Shape == GizmoShape.Sphere)
                {
                    Gizmos.DrawWireSphere(transform.position + Offset, Radius);
                }
                else
                {
                    Gizmos.DrawWireCube(transform.position + Offset, transform.localScale.MultiplyCompWise(Scale));
                }

                Gizmos.color = cachedColor;
            }
        }

#endif

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}