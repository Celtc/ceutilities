﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Helpers
{
    public static class GizmosHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Draw a bounds gizmo.
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="color"></param>
        public static void DrawBounds(Bounds bounds, Color color)
        {
            Gizmos.color = color;
            Gizmos.DrawCube(bounds.center, bounds.size);
        }

        /// <summary>
        /// Draws a cube gizmo in the Scene window.
        /// </summary>
        /// <param name="transform">The transform of the object to draw around</param>
		/// <param name="color"> The colour of the cube</param>
        public static void DrawCubeCollider(Transform transform, Color color)
        {
            if (transform.GetComponent<BoxCollider2D>() != null)
            {
                BoxCollider2D _boxCollider2D = transform.GetComponent<BoxCollider2D>();
                Vector2 pos = _boxCollider2D.offset;

                Gizmos.matrix = transform.localToWorldMatrix;
                Gizmos.color = color;
                Gizmos.DrawCube(pos, _boxCollider2D.size);
                Gizmos.matrix = Matrix4x4.identity;
            }
            else if (transform.GetComponent<BoxCollider>() != null)
            {
                BoxCollider _boxCollider = transform.GetComponent<BoxCollider>();

                Gizmos.matrix = transform.localToWorldMatrix;
                Gizmos.color = color;
                Gizmos.DrawCube(_boxCollider.center, _boxCollider.size);
                Gizmos.matrix = Matrix4x4.identity;
            }
        }

        /// <summary>
        /// Draws a cube gizmo in the Scene window.
        /// </summary>
        /// <param name="collider">The collider to draw.</param>
		/// <param name="color"> The colour of the cube</param>
        public static void DrawCubeCollider(BoxCollider collider, Color color)
        {
                Gizmos.matrix = collider.transform.localToWorldMatrix;
                Gizmos.color = color;
                Gizmos.DrawCube(collider.center, collider.size);
                Gizmos.matrix = Matrix4x4.identity;
        }

        /// <summary>
        /// Draws a box gizmo in the Scene window.
        /// </summary>
        /// <param name="transform">The transform of the object to draw around</param>
        /// <param name="color"> The colour of the cube</param>
        public static void DrawBoxCollider(Transform transform, Color color)
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = color;
            Gizmos.DrawLine(new Vector3(-0.5f, -0.5f), new Vector3(-0.5f, 0.5f));
            Gizmos.DrawLine(new Vector3(-0.5f, 0.5f), new Vector3(0.5f, 0.5f));
            Gizmos.DrawLine(new Vector3(0.5f, 0.5f), new Vector3(0.5f, -0.5f));
            Gizmos.DrawLine(new Vector3(0.5f, -0.5f), new Vector3(-0.5f, -0.5f));
            Gizmos.matrix = Matrix4x4.identity;
        }
                
        /// <summary>
        /// Draws an outline of a Polygon Collider 2D in the Scene window.
        /// </summary>
        /// <param name="transform">The transform of the object to draw around</param>
		/// <param name="poly"> The Polygon Collider 2D</param>
		/// <param name="color"> The colour of the outline</param>
        public static void DrawPolygonCollider(Transform transform, PolygonCollider2D poly, Color color)
        {
            Gizmos.color = color;
            Gizmos.DrawLine(transform.TransformPoint(poly.points[0]), transform.TransformPoint(poly.points[poly.points.Length - 1]));
            for (int i = 0; i < poly.points.Length - 1; i++)
            {
                Gizmos.DrawLine(transform.TransformPoint(poly.points[i]), transform.TransformPoint(poly.points[i + 1]));
            }
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}