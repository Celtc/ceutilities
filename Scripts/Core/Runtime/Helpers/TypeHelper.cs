﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.CodeDom;
using System.Linq;
using System.Diagnostics.Contracts;
using System.Linq.Expressions;

namespace CEUtilities.Helpers
{
    public static class TypeHelper
    {
        #region Static

        private static readonly Dictionary<Type, string> ValueTypeAliases = new Dictionary<Type, string>
        {
            { typeof(byte), "byte" },
            { typeof(sbyte), "sbyte" },
            { typeof(short), "short" },
            { typeof(ushort), "ushort" },
            { typeof(int), "int" },
            { typeof(uint), "uint" },
            { typeof(long), "long" },
            { typeof(ulong), "ulong" },
            { typeof(float), "float" },
            { typeof(double), "double" },
            { typeof(decimal), "decimal" },
            { typeof(object), "object" },
            { typeof(bool), "bool" },
            { typeof(char), "char" },
            { typeof(string), "string" },
            { typeof(void), "void" },
            { typeof(byte?), "byte?" },
            { typeof(sbyte?), "sbyte?" },
            { typeof(short?), "short?" },
            { typeof(ushort?), "ushort?" },
            { typeof(int?), "int?" },
            { typeof(uint?), "uint?" },
            { typeof(long?), "long?" },
            { typeof(ulong?), "ulong?" },
            { typeof(float?), "float?" },
            { typeof(double?), "double?" },
            { typeof(decimal?), "decimal?" },
            { typeof(bool?), "bool?" },
            { typeof(char?), "char?" },
            { typeof(byte[]), "byte[]" },
            { typeof(sbyte[]), "sbyte[]" },
            { typeof(short[]), "short[]" },
            { typeof(ushort[]), "ushort[]" },
            { typeof(int[]), "int[]" },
            { typeof(uint[]), "uint[]" },
            { typeof(long[]), "long[]" },
            { typeof(ulong[]), "ulong[]" },
            { typeof(float[]), "float[]" },
            { typeof(double[]), "double[]" },
            { typeof(decimal[]), "decimal[]" },
            { typeof(object[]), "object[]" },
            { typeof(bool[]), "bool[]" },
            { typeof(char[]), "char[]" },
            { typeof(string[]), "string[]" },
            { typeof(byte?[]), "byte?[]" },
            { typeof(sbyte?[]), "sbyte?[]" },
            { typeof(short?[]), "short?[]" },
            { typeof(ushort?[]), "ushort?[]" },
            { typeof(int?[]), "int?[]" },
            { typeof(uint?[]), "uint?[]" },
            { typeof(long?[]), "long?[]" },
            { typeof(ulong?[]), "ulong?[]" },
            { typeof(float?[]), "float?[]" },
            { typeof(double?[]), "double?[]" },
            { typeof(decimal?[]), "decimal?[]" },
            { typeof(bool?[]), "bool?[]" },
            { typeof(char?[]), "char?[]" }
        };

        #endregion

        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        [System.Obsolete("Use GetAliasOrName() instead.")]
        /// <summary>
        /// Return a type alias. DEPRECATED
        /// </summary>
        /// <param name="removeNamespaces">Should remove namespaces</param>
        /// <param name="convertArrayLiterals">Should remove array literals</param>
        public static string ToAlias(Type type, bool removeNamespaces, bool convertArrayLiterals)
        {
            //string alias = "(None)";
            //using (var provider = new CSharpCodeProvider())
            //{
            //    var typeRef = new CodeTypeReference(type);
            //    alias = provider.GetTypeOutput(typeRef);
            //}

            //if (removeNamespaces)
            //    alias = alias.Substring(alias.LastIndexOf('.') + 1);

            //if (convertArrayLiterals)
            //    alias = alias.Replace("[]", "Array");

            //return alias;

            return GetAliasOrName(type);
        }

        /// <summary>
        /// Return a type alias
        /// </summary>
        public static string GetAlias(Type type)
        {
            return GetAlias(type, false, false);
        }

        /// <summary>
        /// Return a type alias
        /// </summary>
        /// <param name="removeNamespaces">Should remove namespaces</param>
        public static string GetAlias(Type type, bool removeNamespaces)
        {
            return GetAlias(type, removeNamespaces, false);
        }

        /// <summary>
        /// Return a type alias
        /// </summary>
        /// <param name="removeNamespaces">Should remove namespaces</param>
        /// <param name="convertArrayLiterals">Should remove array literals</param>
        public static string GetAlias(Type type, bool removeNamespaces, bool convertArrayLiterals)
        {
            string result = string.Empty;

            if (ValueTypeAliases.ContainsKey(type))
                result = ValueTypeAliases[type];
            else
                return result;

            if (removeNamespaces)
                result = result.Substring(result.LastIndexOf('.') + 1);

            if (convertArrayLiterals)
                result = result.Replace("[]", "Array");

            return result;
        }

        /// <summary>
        /// Get the alias or the name
        /// </summary>
        public static string GetAliasOrName(Type type)
        {
            return GetAliasOrName(type, false, false);
        }

        /// <summary>
        /// Get the alias or the name
        /// </summary>
        /// <param name="removeNamespaces">Should remove namespaces</param>
        public static string GetAliasOrName(Type type, bool removeNamespaces)
        {
            return GetAliasOrName(type, removeNamespaces, false);
        }

        /// <summary>
        /// Get the alias or the name
        /// </summary>
        /// <param name="removeNamespaces">Should remove namespaces</param>
        /// <param name="convertArrayLiterals">Should remove array literals</param>
        public static string GetAliasOrName(Type type, bool removeNamespaces, bool convertArrayLiterals)
        {
            string result = string.Empty;

            if (ValueTypeAliases.ContainsKey(type))
                result = GetAlias(type, removeNamespaces, convertArrayLiterals);
            else
            {
                result = removeNamespaces ? type.Name : type.FullName;

                if (convertArrayLiterals)
                    result = result.Replace("[]", "Array");
            }

            return result;
        }

        /// <summary>
        /// Return a type for a given typename and namespace
        /// </summary>
        public static Type GetType(string typeName, string nameSpace) { return GetType(nameSpace + "." + typeName); }

        /// <summary>
        /// Return a type for a given typename
        /// </summary>
        public static Type GetType(string typeName)
        {
            var type = Type.GetType(typeName);
            if (type != null) return type;
            var assamblies = AppDomain.CurrentDomain.GetAssemblies();   // TODO: Check performace
            for (int i = 0; i < assamblies.Length; i++)
            {
                type = assamblies[i].GetType(typeName);
                if (type != null) return type;
            }
            return null;
        }

        /// <summary>
        /// Get all derived types of an specific type
        /// </summary>
        public static Type[] GetAllDerivedTypes<T>()
        {
            return GetAllDerivedTypes(typeof(T));
        }

        /// <summary>
        /// Get all derived types of an specific type
        /// </summary>
        public static Type[] GetAllDerivedTypes(Type desiredType)
        {
            return (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                    from type in assembly.GetTypes()
                    where type.IsSubclassOf(desiredType)
                    select type).ToArray();
        }


        /// <summary>
        /// Converts <see cref="Func{object, object}" /> to <see cref="Func{T, TResult}" />.
        /// </summary>
        /// <param name="func">The function.</param>
        /// <param name="resultType">Type of the result.</param>
        /// <returns></returns>
        public static Delegate Convert(Func<object> func, Type resultType)
        {
            // If we need more versions of func then consider using params Type as we can abstract some of the
            // conversion then.

            Contract.Requires(func != null);
            Contract.Requires(resultType != null);

            // This is gnarly... If a func contains a closure, then even though its static, its first
            // param is used to carry the closure, so its as if it is not a static method, so we need
            // to check for that param and call the func with it if it has one...
            Expression call;
            call = Expression.Convert(
                func.Target == null
                ? Expression.Call(func.Method)
                : Expression.Call(Expression.Constant(func.Target), func.Method), resultType);

            var delegateType = typeof(Func<>).MakeGenericType(resultType);
            return Expression.Lambda(delegateType, call).Compile();
        }

        /// <summary>
        /// Converts <see cref="Func{object, object}" /> to <see cref="Func{T, TResult}" />.
        /// </summary>
        /// <param name="func">The function.</param>
        /// <param name="argType">Type of the argument.</param>
        /// <param name="resultType">Type of the result.</param>
        /// <returns></returns>
        public static Delegate Convert(Func<object, object> func, Type argType, Type resultType)
        {
            // If we need more versions of func then consider using params Type as we can abstract some of the
            // conversion then.

            Contract.Requires(func != null);
            Contract.Requires(resultType != null);

            var param = Expression.Parameter(argType);
            var convertedParam = new Expression[] { Expression.Convert(param, typeof(object)) };

            // This is gnarly... If a func contains a closure, then even though its static, its first
            // param is used to carry the closure, so its as if it is not a static method, so we need
            // to check for that param and call the func with it if it has one...
            Expression call;
            call = Expression.Convert(
                func.Target == null
                ? Expression.Call(func.Method, convertedParam)
                : Expression.Call(Expression.Constant(func.Target), func.Method, convertedParam), resultType);

            var delegateType = typeof(Func<,>).MakeGenericType(argType, resultType);
            return Expression.Lambda(delegateType, call, param).Compile();
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}