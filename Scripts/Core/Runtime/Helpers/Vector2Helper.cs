﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Helpers
{
    public class Vector2Helper
    {
        #region Static

        public static Vector2 MaxValue = new Vector2(float.MaxValue, float.MaxValue);

        #endregion

        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}