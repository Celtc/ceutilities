﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Structs;

namespace CEUtilities.Helpers
{
    public static class PointHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Clamps the value of the point between the values
        /// </summary>
        public static Point Clamp(Point point, Point min, Point max)
        {
            return new Point(Mathf.Clamp(point.x, min.x, max.x), Mathf.Clamp(point.y, min.y, max.y));
        }

        /// <summary>
        /// DOT operation
        /// </summary>
        public static int Dot(Point a, Point b)
        {
            return a.x * b.x + a.y + b.y;
        }

        #endregion Methods
    }
}