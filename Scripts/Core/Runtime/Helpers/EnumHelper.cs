﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace CEUtilities.Helpers
{
    public static class EnumHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Convert provided enum type to list of values.
        /// This is convenient when you need to iterate enum values.
        /// </summary>
        public static List<T> Values<T>()
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException();
            }
            var values = Enum.GetNames(typeof(T));
            return values.Select(value => ParseEnum<T>(value)).ToList();
        }

        /// <summary>
        /// Present the enum values as a comma separated string.
        /// </summary>eeeee
        public static string ValuesToString<T>()
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException();
            }
            var values = Enum.GetNames(typeof(T));
            return string.Join(", ", values);
        }

        /// <summary>
        /// Get an enum value from a string.
        /// </summary>
        public static T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        /// <summary>
        /// Get random enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetRandomEnum<T>()
        {
            var values = Enum.GetValues(typeof(T));
            if (values.Length == 0)
            {
                throw new ArgumentException("Enum " + typeof(T).ToString() + " is empty", "array");
            }
            T randomValue = (T)values.GetValue(UnityEngine.Random.Range(0, values.Length));
            return randomValue;
        }

        /// <summary>
        /// Gets an attribute on an enum field value.
        /// </summary>
        /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
        /// <param name="enumVal">The enum value</param>
        /// <returns>The attribute of type T that exists on the enum value</returns>
        /// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
        public static T GetAttributeOfType<T>(Enum enumVal) where T : Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }

        #endregion Public Methods
    }
}