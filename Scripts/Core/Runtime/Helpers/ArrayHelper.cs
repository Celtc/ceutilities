﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Helpers
{
    public static class ArrayHelper
    {
        #region Enum

        public enum MultidimensionalLinearOrder
        {
            RowMajor,
            ColumnMajor
        }

        #endregion

        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Truncate an array to an specific length
        /// </summary>
        public static void Truncate<T>(ref T[] array, int length)
        {
            if (array.Length > length)
                Array.Resize(ref array, length);
        }

        /// <summary>
        /// Removes an element from an array
        /// </summary>
        public static T[] Remove<T>(ref T[] array, T itemToRemove)
        {
            var index = Array.IndexOf(array, itemToRemove);
            if (index >= 0) RemoveAt<T>(ref array, index);
            return array;
        }

        /// <summary>
        /// Adds an element
        /// </summary>
        public static T[] Add<T>(ref T[] source, T itemToAdd)
        {
            Array.Resize(ref source, source.Length + 1);
            source[source.Length - 1] = itemToAdd;
            return source;
        }

        /// <summary>
        /// Removes an element at an index
        /// </summary>
        public static void RemoveAt<T>(ref T[] array, int index)
        {
            for (int a = index; a < array.Length - 1; a++)
            {
                // Moving elements downwards, to fill the gap at [index]
                array[a] = array[a + 1];
            }

            // Finally, let's decrement Array's size by one
            Array.Resize(ref array, array.Length - 1);
        }

        /// <summary>
        /// Removes all empty references from an array
        /// </summary>
        /// <returns>Was any null removed?</returns>
        public static bool RemoveNulls<T>(ref T[] array) where T : class
        {
            var length = array.Length;
            var temp = new T[length];
            var realLength = 0;
            var change = false;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != null)
                {
                    temp[realLength++] = array[i];
                    change = true;
                }
            }
            Array.Resize(ref temp, realLength);
            array = temp;
            return change;
        }

        /// <summary>
        /// Resizes the array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original">The original.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="cols">The cols.</param>
        /// <returns></returns>
        public static T[,] Resize<T>(T[,] original, int rows, int cols)
        {
            var newArray = new T[rows, cols];
            int minRows = Math.Min(rows, original.GetLength(0));
            int minCols = Math.Min(cols, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
            {
                for (int j = 0; j < minCols; j++)
                {
                    newArray[i, j] = original[i, j];
                }
            }
            return newArray;
        }

        /// <summary>
        /// Removes a row from the array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original">The original.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="cols">The cols.</param>
        /// <returns></returns>
        public static T[,] RemoveRow<T>(T[,] original, int rowIndex, MultidimensionalLinearOrder order = MultidimensionalLinearOrder.RowMajor)
        {
            if (order == MultidimensionalLinearOrder.RowMajor)
            {
                return RemoveMajor(original, rowIndex);
            }
            else
            {
                return RemoveMinor(original, rowIndex);
            }
        }

        /// <summary>
        /// Removes a column from the array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original">The original.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="cols">The cols.</param>
        /// <returns></returns>
        public static T[,] RemoveColumn<T>(T[,] original, int columnIndex, MultidimensionalLinearOrder order = MultidimensionalLinearOrder.RowMajor)
        {
            if (order == MultidimensionalLinearOrder.RowMajor)
            {
                return RemoveMinor(original, columnIndex);
            }
            else
            {
                return RemoveMajor(original, columnIndex);
            }
        }

        /// <summary>
        /// Removes the major coord by index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original">The original.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public static T[,] RemoveMajor<T>(T[,] original, int index)
        {
            int majors = original.GetLength(0) - 1;
            int minors = original.GetLength(1);
            var newArray = new T[majors, minors];
            for (int i = 0; i < majors; i++)
            {
                for (int j = 0; j < minors; j++)
                {
                    var ii = i >= majors ? i + 1 : i;
                    newArray[i, j] = original[ii, j];
                }
            }
            return newArray;
        }

        /// <summary>
        /// Removes the minor coord by index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original">The original.</param>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public static T[,] RemoveMinor<T>(T[,] original, int index)
        {
            int majors = original.GetLength(0);
            int minors = original.GetLength(1) - 1;
            var newArray = new T[majors, minors];
            for (int i = 0; i < majors; i++)
            {
                for (int j = 0; j < minors; j++)
                {
                    var jj = j >= index ? j + 1 : j;
                    newArray[i, j] = original[i, jj];
                }
            }
            return newArray;
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}