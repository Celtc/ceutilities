﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Colors;
using CEUtilities.Helpers;

namespace CEUtilities.Helpers
{
    public static class ColorHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Get a color from an hex representation.
        /// If the hex value doesn't account for an alpha, it'll be assumed as 100%.
        /// </summary>
        public static Color FromHexString(string hex)
        {
            Color color;
            ColorUtility.TryParseHtmlString(hex, out color);
            return color;
        }

        /// <summary>
        /// Creates a hex string from the color
        /// </summary>
        public static string ToHexString(Color color) => ToHexString(color, true);

        /// <summary>
        /// Creates a hex string from the color
        /// </summary>
        public static string ToHexString(Color color, bool includeAlpha)
        {
            return includeAlpha ? 
                ColorUtility.ToHtmlStringRGBA(color) :
                ColorUtility.ToHtmlStringRGB(color);
        }

                
        /// <summary>
        /// Convenience conversion function, allowing hex format (0xRrGgBbAa).
        /// </summary>
        public static Color FromHex(uint val)
        {
            return FromInt((int)val);
        }

        /// <summary>
        /// Convenience conversion function, allowing hex format (0xRrGgBbAa).
        /// </summary>
        public static uint ToHex(Color color)
        {
            return (uint)ToInt(color);
        }


        /// <summary>
        /// Convert the specified color to RGBA32 integer format.
        /// </summary>
        public static int ToInt(Color color)
        {
            int retVal = 0;
            retVal |= Mathf.RoundToInt(color.r * 255f) << 24;
            retVal |= Mathf.RoundToInt(color.g * 255f) << 16;
            retVal |= Mathf.RoundToInt(color.b * 255f) << 8;
            retVal |= Mathf.RoundToInt(color.a * 255f);
            return retVal;
        }

        /// <summary>
        /// Convert the specified RGBA32 integer to Color.
        /// </summary>
        public static Color FromInt(int val)
        {
            float inv = 1f / 255f;
            Color c = Color.black;
            c.r = inv * ((val >> 24) & 0xFF);
            c.g = inv * ((val >> 16) & 0xFF);
            c.b = inv * ((val >> 8) & 0xFF);
            c.a = inv * (val & 0xFF);
            return c;
        }


        /// <summary>
        /// Generates a RGB hsvColor using specified hue, saturation, brightness and customAlpha
        /// http://wiki.unity3d.com/index.php?title=HSBColor
        /// </summary>
        /// <returns>RGB hsvColor</returns>
        public static Color HSVToRGB(float hue, float saturation, float brightness, float customAlpha = 1.0f)
        {
            return ColorHelper.HSVToRGB(hue, saturation, brightness, customAlpha);
        }

        /// <summary>
        /// Returns the ToHSV values of a RGB hsvColor
        /// http://www.rapidtables.com/convert/hsvColor/rgb-to-hsv.htm
        /// </summary>
        /// <param name="hsvColor">RGB hsvColor</param>
        /// <returns>Vector3 containing the ToHSV representation of the RGB hsvColor</returns>
        public static HSVColor RGBToHSV(Color rgb)
        {
            return ColorHelper.RGBToHSV(rgb);
        }


        /// <summary>
        /// Lerps from one color to another, avoiding clamp checks
        /// </summary>
        /// <param name="value">Ratio from c1 to c2</param>
        public static Color ColorLerpUnclamped(this Color c1, Color c2, float value)
        {
            return new Color(c1.r + (c2.r - c1.r) * value,
                              c1.g + (c2.g - c1.g) * value,
                              c1.b + (c2.b - c1.b) * value,
                              c1.a + (c2.a - c1.a) * value);
        }

        /// <summary>
        /// Invert a color
        /// </summary>
        public static Color Invert(Color original)
		{
			return Invert(original, true);
		}

		/// <summary>
		/// Invert a color
		/// </summary>
		public static Color Invert(Color original, bool keepAlpha)
		{
			Color newColor;
			if (keepAlpha)
				newColor = new Color(1f - original.r,
							 1f - original.g,
							 1f - original.b,
							 original.a);
			else				
				newColor = new Color(1f - original.r,
							 1f - original.g,
							 1f - original.b);
			return newColor;
		}

        #endregion Methods
    }
}