﻿using UnityEngine;

namespace CEUtilities.Helpers
{
    public static class ApplicationHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Is the platform Desktop
        /// </summary>
        public static bool PlatformIsDesktop
        {
            get
            {
                return
                    ApplicationHelper.PlatformIsEditor ||
                    ApplicationHelper.PlatformIsDesktopStandalone;
            }
        }

        /// <summary>
        /// Is the platform Standalone
        /// </summary>
        public static bool PlatformIsDesktopStandalone
        {
            get
            {
                return
                    Application.platform == RuntimePlatform.WindowsPlayer ||
                    Application.platform == RuntimePlatform.LinuxPlayer ||
                    Application.platform == RuntimePlatform.OSXPlayer ||


#if UNITY_5 || UNITY_2017
                    Application.platform == RuntimePlatform.WSAPlayerX86 ||
                    Application.platform == RuntimePlatform.WSAPlayerX64;
#else
                    Application.platform == RuntimePlatform.MetroPlayerX86||
                    Application.platform == RuntimePlatform.MetroPlayerX64;
#endif

            }
        }

        /// <summary>
        /// Is the platform Editor
        /// </summary>
        public static bool PlatformIsEditor
        {
            get
            {
                return Application.isEditor;
            }
        }

        /// <summary>
        /// Is the platform Web
        /// </summary>
        public static bool PlatformIsWebGL
        {
            get
            {
                return Application.platform == RuntimePlatform.WebGLPlayer;
            }
        }

        /// <summary>
        /// Is the platform Mobile
        /// </summary>
        public static bool PlatformIsMobile
        {
            get
            {
#pragma warning disable 0618
                return Application.platform == RuntimePlatform.Android ||
                       Application.platform == RuntimePlatform.IPhonePlayer ||
                       Application.platform == RuntimePlatform.BlackBerryPlayer ||
                       Application.platform == RuntimePlatform.WP8Player ||

#if UNITY_5 || UNITY_2017
                        Application.platform == RuntimePlatform.WSAPlayerARM;
#else
                        Application.platform == RuntimePlatform.MetroPlayerARM;
#endif
#pragma warning restore 0618
            }
        }


        /// <summary>
        /// Is the aplication playing (player or playmode)?
        /// </summary>
        public static bool AppInPlayerOrPlaymode
        {
            get
            {
#if UNITY_EDITOR
                return Application.isPlaying;   // Playmode?
#else
			    return true;					// Player
#endif
            }
        }

        /// <summary>
        /// Is the aplication in a player (build)?
        /// </summary>
        public static bool AppInPlayer
        {
            get
            {
#if UNITY_EDITOR
                return false;
#else
			return true;
#endif
            }
        }

        /// <summary>
        /// Is the aplication in playmode inside editor?
        /// </summary>
        public static bool AppInPlaymode
        {
            get
            {
#if UNITY_EDITOR
                return Application.isPlaying;   // Playmode
#else
			    return false;
#endif
            }
        }

        /// <summary>
        /// Is the aplication playing?
        /// </summary>
        public static bool AppInPausedEditor
        {
            get
            {
#if UNITY_EDITOR
                return !Application.isPlaying;  // Not Playmode
#else
			    return false;
#endif
            }
        }

        /// <summary>
        /// Access to the clipboard via undocumented APIs.
        /// </summary>
        public static string Clipboard
        {
            get
            {
                var textEditor = new TextEditor();
                textEditor.Paste();
#if UNITY_4
                return te.content.text;
#else
                return textEditor.text;
#endif
            }

            set
            {
                TextEditor te = new TextEditor();
#if UNITY_4
			    te.content = new GUIContent(value);
#else
                te.text = value;
#endif
                te.OnFocus();
                te.Copy();
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}