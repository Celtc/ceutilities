﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace CEUtilities.Helpers
{
    public static class MathHelper
    {
        /// <summary>
        /// Compares two Vector2 if they are similar
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool Approximately(Vector2 a, Vector2 b)
        {
            return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y);
        }

        /// <summary>
        /// Compares two Vector3 if they are similar
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool Approximately(Vector3 a, Vector3 b)
        {
            return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y) && Mathf.Approximately(a.z, b.z);
        }

        /// <summary>
        /// Compares two Quaternion if they are similar
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool Approximately(Quaternion a, Quaternion b)
        {
            return Mathf.Approximately(a.x, b.x) 
                && Mathf.Approximately(a.y, b.y) 
                && Mathf.Approximately(a.z, b.z)
                && Mathf.Approximately(a.w, b.w);
        }

        /// <summary>
        /// Hermite interpolation between two values.
        /// Returns 0 to 1 based on the value x between range a to b. Exceeding values will be clamped to 0,1.
        /// </summary>
        public static float Smoothstep(float a, float b, float x)
        {
            float t = Mathf.Clamp01((x - a) / (b - a));
            return t * t * (3f - (2f * t));
        }

        /// <summary>
        /// Clamp the specified integer to be between 0 and below 'max'.
        /// </summary>
        /// <param name="val">Index</param>
        /// <param name="max">Exclusive upper limit (Usually the 'Length')</param>
        /// <returns></returns>
        public static int ClampIndex(int val, int max) { return (val < 0) ? 0 : (val < max ? val : max - 1); }

        /// <summary>
        /// Wrap the index using repeating logic, so that for example index of 'Length' means '0'.
        /// Values under 0 will be clamped to 0 (only repeat towards the upper limit)
        /// </summary>
        /// <param name="val">Index</param>
        /// <param name="max">Exclusive upper limit (Usually the 'Length' or 'Count')</param>
        /// <returns></returns>
        public static int RepeatIndex(int val, int max)
        {
            if (max < 1) return 0;
            while (val < 0) val += max;
            while (val >= max) val -= max;
            return val;
        }

        /// <summary>
        /// Wrap the index using repeating logic, so that for example index of 'Length' means '0', and -1 means 'Length - 1'
        /// </summary>
        /// <param name="val">Index</param>
        /// <param name="max">Exclusive upper limit (Usually the 'Length' or 'Count')</param>
        /// <returns></returns>
        public static int CircularRepeatIndex(int val, int max)
        {
            while (val < 0) val += max;
            while (val >= max) val -= max;
            return val;
        }

        /// <summary>
        /// In the shader, equivalent function would be 'fract'
        /// </summary>
        public static float Wrap01(float val) { return val - Mathf.FloorToInt(val); }

        /// <summary>
        /// Clamp an angle between the given values, all angle is keep between -180 - 180
        /// </summary>
        public static float ClampAngle(float angle, float min, float max)
        {
            angle = WrapAngle(angle);
            min = WrapAngle(min);
            max = WrapAngle(max);

            angle = Mathf.Clamp(angle, min, max);

            return angle;
        }

        /// <summary>
        /// Ensure that the angle is within -180 to 180 range.
        /// </summary>
        public static float WrapAngle(float angle)
        {
            while (angle > 180f) angle -= 360f;
            while (angle < -180f) angle += 360f;
            return angle;
        }

        /// <summary>
        /// Convert a hexadecimal character to its decimal value.
        /// </summary>
        public static int HexCharToDecimal(char ch)
        {
            switch (ch)
            {
                case '0': return 0x0;
                case '1': return 0x1;
                case '2': return 0x2;
                case '3': return 0x3;
                case '4': return 0x4;
                case '5': return 0x5;
                case '6': return 0x6;
                case '7': return 0x7;
                case '8': return 0x8;
                case '9': return 0x9;
                case 'a':
                case 'A': return 0xA;
                case 'b':
                case 'B': return 0xB;
                case 'c':
                case 'C': return 0xC;
                case 'd':
                case 'D': return 0xD;
                case 'e':
                case 'E': return 0xE;
                case 'f':
                case 'F': return 0xF;
            }
            return 0xF;
        }

        /// <summary>
        /// Convert a single 0-15 value into its hex representation.
        /// It's coded because int.ToString(format) syntax doesn't seem to be supported by Unity's Flash. It just silently crashes.
        /// </summary>
        public static char DecimalToHexChar(int num)
        {
            if (num > 15) return 'F';
            if (num < 10) return (char)('0' + num);
            return (char)('A' + num - 10);
        }

        /// <summary>
        /// Convert the specified integer to a human-readable string representing the binary value. Useful for debugging bytes.
        /// </summary>
        public static string IntToBinary(int val, int bits)
        {
            string final = "";

            for (int i = bits; i > 0;)
            {
                if (i == 8 || i == 16 || i == 24) final += " ";
                final += ((val & (1 << --i)) != 0) ? '1' : '0';
            }
            return final;
        }


        /// <summary>
        /// Framerate independent Lerp
        /// </summary>
        public static Vector3 SpringDampen(ref Vector3 velocity, float strength, float deltaTime)
        {
            if (deltaTime > 1f) deltaTime = 1f;
            float dampeningFactor = 1f - strength * 0.001f;
            int ms = Mathf.RoundToInt(deltaTime * 1000f);
            float totalDampening = Mathf.Pow(dampeningFactor, ms);
            Vector3 vTotal = velocity * ((totalDampening - 1f) / Mathf.Log(dampeningFactor));
            velocity = velocity * totalDampening;
            return vTotal * 0.06f;
        }

        /// <summary>
        /// Same as the Vector3 version, it's a framerate-independent Lerp.
        /// </summary>
        public static Vector2 SpringDampen(ref Vector2 velocity, float strength, float deltaTime)
        {
            if (deltaTime > 1f) deltaTime = 1f;
            float dampeningFactor = 1f - strength * 0.001f;
            int ms = Mathf.RoundToInt(deltaTime * 1000f);
            float totalDampening = Mathf.Pow(dampeningFactor, ms);
            Vector2 vTotal = velocity * ((totalDampening - 1f) / Mathf.Log(dampeningFactor));
            velocity = velocity * totalDampening;
            return vTotal * 0.06f;
        }

        /// <summary>
        /// Lerp function that doesn't clamp the 'factor' in 0-1 range.
        /// </summary>
        public static float Lerp(float from, float to, float factor) { return from * (1f - factor) + to * factor; }

        /// <summary>
        /// Calculate how much to interpolate by.
        /// </summary>
        public static float SpringLerp(float strength, float deltaTime)
        {
            if (deltaTime > 1f) deltaTime = 1f;
            int ms = Mathf.RoundToInt(deltaTime * 1000f);
            deltaTime = 0.001f * strength;
            float cumulative = 0f;
            for (int i = 0; i < ms; ++i) cumulative = Mathf.Lerp(cumulative, 1f, deltaTime);
            return cumulative;
        }

        /// <summary>
        /// Mathf.Lerp(from, to, Time.deltaTime * strength) is not framerate-independent. This function is.
        /// </summary>
        public static float SpringLerp(float from, float to, float strength, float deltaTime)
        {
            if (deltaTime > 1f) deltaTime = 1f;
            int ms = Mathf.RoundToInt(deltaTime * 1000f);
            deltaTime = 0.001f * strength;
            for (int i = 0; i < ms; ++i) from = Mathf.Lerp(from, to, deltaTime);
            return from;
        }

        /// <summary>
        /// Vector2.Lerp(from, to, Time.deltaTime * strength) is not framerate-independent. This function is.
        /// </summary>
        public static Vector2 SpringLerp(Vector2 from, Vector2 to, float strength, float deltaTime)
        {
            return Vector2.Lerp(from, to, SpringLerp(strength, deltaTime));
        }

        /// <summary>
        /// Vector3.Lerp(from, to, Time.deltaTime * strength) is not framerate-independent. This function is.
        /// </summary>
        public static Vector3 SpringLerp(Vector3 from, Vector3 to, float strength, float deltaTime)
        {
            return Vector3.Lerp(from, to, SpringLerp(strength, deltaTime));
        }

        /// <summary>
        /// Quaternion.Slerp(from, to, Time.deltaTime * strength) is not framerate-independent. This function is.
        /// </summary>
        public static Quaternion SpringLerp(Quaternion from, Quaternion to, float strength, float deltaTime)
        {
            return Quaternion.Slerp(from, to, SpringLerp(strength, deltaTime));
        }


        /// <summary>
        /// Determine the distance from the specified point to the line segment.
        /// </summary>
        public static float DistancePointToLineSegment(Vector2 point, Vector2 a, Vector2 b)
        {
            return UIMathHelper.DistancePointToLineSegment(point, a, b);
        }

        /// <summary>
        /// Determine the distance from the mouse position to the screen space rectangle specified by the 4 points.
        /// </summary>
        public static float DistanceToRectangle(Vector2[] screenPoints, Vector2 mousePos)
        {
            return UIMathHelper.DistanceToRectangle(screenPoints, mousePos);
        }

        /// <summary>
        /// Determine the distance from the mouse position to the world rectangle specified by the 4 points.
        /// </summary>
        public static float DistanceToRectangle(Vector3[] worldPoints, Vector2 mousePos, Camera cam)
        {
            Vector2[] screenPoints = new Vector2[4];
            for (int i = 0; i < 4; ++i)
                screenPoints[i] = cam.WorldToScreenPoint(worldPoints[i]);
            return DistanceToRectangle(screenPoints, mousePos);
        }

        /// <summary>
        /// Move the angle towards a value by a maximun amount
        /// </summary>
        public static float RotateTowards(float from, float to, float maxAngle)
        {
            float diff = WrapAngle(to - from);
            if (Mathf.Abs(diff) > maxAngle) diff = maxAngle * Mathf.Sign(diff);
            return from + diff;
        }

        /// <summary>
        /// Return a point mirrored to a vector given as origin and direction
        /// </summary>
        /// <param name="_origin"></param>
        /// <param name="_direction"></param>
        /// <param name="_pointA"></param>
        /// <returns></returns>
        public static Vector2 FindMirror(Vector2 _origin, Vector2 _direction, Vector2 _pointA)
        {
            float s = 0f; //Scalar projection

            /*float angle = Mathf.Atan2(_pointA.y, _pointA.x) - Mathf.Atan2(_direction.y, _direction.x);
            float lengthA = _pointA.magnitude;
            s = lengthA * Mathf.Cos(angle);*/
            //We use the vector projection formula

            //An alternate way is by using the dot product, in which case
            //  the following code of line substitutes the last three.
            s = Vector3.Dot(_pointA, _direction);

            //The projection of point A over _direction
            Vector2 projA = _origin + _direction * s;

            //The rejection of point A from _direction
            Vector2 rejA = _pointA - projA;

            //We want the opposite, as it's the rejection of point B from "_direction"
            rejA *= -1;

            //We return the symmetric point of point A over _direction
            return projA + rejA;
        }

        /// <summary>
        /// Complex mirrored pair points generation in 2d
        /// </summary>
        /// <param name="_origin">Origin of the vector which is used to mirror the points</param>
        /// <param name="_direction">Direction of the vector which is used to mirror the points</param>
        /// <param name="_distanceInLine"></param>
        /// <param name="_distanceFromLine"></param>
        /// <param name="_goAgainstDirection"></param>
        /// <param name="_swapHemispheres"></param>
        /// <returns></returns>
        public static Vector2[] GenerateMirrorPair(Vector2 _origin, Vector2 _direction, float _maxDistanceInLine, float _maxDistanceFromLine)
        {
            return MathHelper.GenerateMirrorPair(_origin, _direction, UnityEngine.Random.Range(0f, _maxDistanceInLine), UnityEngine.Random.Range(0f, _maxDistanceFromLine), UnityEngine.Random.Range(0f, 1f) <= 0.5f, UnityEngine.Random.Range(0f, 1f) <= 0.5f);
        }

        /// <summary>
        /// Complex mirrored pair points generation in 2d
        /// </summary>
        /// <param name="_origin">Origin of the vector which is used to mirror the points</param>
        /// <param name="_direction">Direction of the vector which is used to mirror the points</param>
        /// <param name="_distanceInLine"></param>
        /// <param name="_distanceFromLine"></param>
        /// <param name="_goAgainstDirection"></param>
        /// <param name="_swapHemispheres"></param>
        /// <returns></returns>
        public static Vector2[] GenerateMirrorPair(Vector2 _origin, Vector2 _direction, float _distanceInLine, float _distanceFromLine, bool _goAgainstDirection, bool _swapHemispheres)
        {
            Vector2[] points = new Vector2[2];

            //This sets the max distance along the line where both points can be generated
            //float maxDistance = maxD; //[This is just an example]

            //Length of the projection vector
            float distanceInLine = _distanceInLine; // Rnd.Range(0f, maxDistance);

            //Length of the rejection vector
            float distanceFromLine = _distanceFromLine; // Rnd.Range(0f, maxDistance);

            Vector2 projection = _direction;

            //The projection can go with or against the _direction
            if (_goAgainstDirection) // Rnd.Range(0f, 1f) <= 0.5f)
            {
                projection *= -1;
            }

            //Given the projection vector, we calculate the future rejection vector
            Vector2 rejection = projection.PerpendicularRight(); // Perpendicular(projection);

            rejection *= distanceFromLine;

            if (_goAgainstDirection) // !_swapHemispheres && // Rnd.Range(0f, 1f) <= 0.5f)
            {
                rejection *= -1f;
            }

            projection *= distanceInLine;
            projection += _origin;

            if (_swapHemispheres)
            {
                points[0] = projection - rejection;
                points[1] = projection + rejection;
            }
            else
            {
                points[0] = projection + rejection;
                points[1] = projection - rejection;
            }

            return points;
        }

        /// <summary>
        /// Complex mirrored pair points generation in 2d
        /// </summary>
        /// <param name="_origin">Origin of the vector which is used to mirror the points</param>
        /// <param name="_direction">Direction of the vector which is used to mirror the points</param>
        /// <param name="_distanceInLine"></param>
        /// <param name="_distanceFromLine"></param>
        /// <param name="_goAgainstDirection"></param>
        /// <param name="_swapHemispheres"></param>
        /// <returns></returns>
        public static Vector2[] GenerateMirrorPairRadially(Vector2 _origin, Vector2 _direction, float _maxRadius)
        {
            return MathHelper.GenerateMirrorPairRadially(_origin, _direction, UnityEngine.Random.Range(1f, _maxRadius), UnityEngine.Random.Range(0f, 180f));
        }

        /// <summary>
        /// Complex mirrored pair points generation in 2d
        /// </summary>
        /// <param name="_origin">Origin of the vector which is used to mirror the points</param>
        /// <param name="_direction">Direction of the vector which is used to mirror the points</param>
        /// <param name="_distanceInLine"></param>
        /// <param name="_distanceFromLine"></param>
        /// <param name="_goAgainstDirection"></param>
        /// <param name="_swapHemispheres"></param>
        /// <returns></returns>
        public static Vector2[] GenerateMirrorPairRadially(Vector2 _origin, Vector2 _direction, float _radius, float _deltaAngle)
        {
            Vector2[] points = new Vector2[2];

            float x, y;

            //Distance away from the _origin (i.e. the _radius of the circle)
            float distance = _radius; //Rnd.Range(0f, float.MaxValue);

            //Angle of the line used as a mirror
            float lineAngle = Mathf.Atan2(_direction.y, _direction.x);

            //Variation to be applied to the line angle
            //  Max value is 180 degrees because the point over the 
            //  other hemisphere will be calculated using this one
            float deltaAngle = _deltaAngle; // Rnd.Range(0f, 180f);

            //The deltaAngle in radians
            float deltaAngleR;

            //We generate the first point
            deltaAngleR = lineAngle + (deltaAngle * Mathf.Deg2Rad);

            //Basic position over a circle formula
            x = _origin.x + Mathf.Cos(deltaAngleR) * distance;
            y = _origin.y + Mathf.Sin(deltaAngleR) * distance;

            points[0] = new Vector2(x, y);

            //The mirror point's angular distance from the 360 degrees
            //is equal to the distance between the original point and 0 degrees
            deltaAngleR = lineAngle + ((360f - deltaAngle) * Mathf.Deg2Rad);

            x = _origin.x + Mathf.Cos(deltaAngleR) * distance;
            y = _origin.y + Mathf.Sin(deltaAngleR) * distance;

            points[1] = new Vector2(x, y);

            return points;
        }


        /// <summary>
        /// Constrain 'rect' to be within 'area' as much as possible, returning the Vector2 offset necessary for this to happen.
        /// This function is useful when trying to restrict one area (window) to always be within another (viewport).
        /// </summary>
        public static Vector2 ConstrainRect(Vector2 minRect, Vector2 maxRect, Vector2 minArea, Vector2 maxArea)
        {
            Vector2 offset = Vector2.zero;

            float contentX = maxRect.x - minRect.x;
            float contentY = maxRect.y - minRect.y;

            float areaX = maxArea.x - minArea.x;
            float areaY = maxArea.y - minArea.y;

            if (contentX > areaX)
            {
                float diff = contentX - areaX;
                minArea.x -= diff;
                maxArea.x += diff;
            }

            if (contentY > areaY)
            {
                float diff = contentY - areaY;
                minArea.y -= diff;
                maxArea.y += diff;
            }

            if (minRect.x < minArea.x) offset.x += minArea.x - minRect.x;
            if (maxRect.x > maxArea.x) offset.x -= maxRect.x - maxArea.x;
            if (minRect.y < minArea.y) offset.y += minArea.y - minRect.y;
            if (maxRect.y > maxArea.y) offset.y -= maxRect.y - maxArea.y;

            return offset;
        }


        /// <summary>
        /// Mathf.Pow is used as an alternative for cube root (Math.cbrt) here.
        /// </summary>
        public static float CubeRoot(float d)
        {
            if (d < 0.0f)
            {
                return -Mathf.Pow(-d, 1f / 3f);
            }

            else
            {
                return Mathf.Pow(d, 1f / 3f);
            }
        }

        /// <summary>
        /// Solve cubic equation according to Cardano. 
        /// Source: https://www.cs.rit.edu/~ark/pj/lib/edu/rit/numeric/Cubic.shtml
        /// </summary>
        public static void SolveCubic(out int nRoots, out float x1, out float x2, out float x3, float a, float b, float c, float d)
        {
            float TWO_PI = 2f * Mathf.PI;
            float FOUR_PI = 4f * Mathf.PI;

            // Normalize coefficients.
            float denom = a;
            a = b / denom;
            b = c / denom;
            c = d / denom;

            // Commence solution.
            float a_over_3 = a / 3f;
            float Q = (3f * b - a * a) / 9f;
            float Q_CUBE = Q * Q * Q;
            float R = (9f * a * b - 27f * c - 2f * a * a * a) / 54f;
            float R_SQR = R * R;
            float D = Q_CUBE + R_SQR;

            if (D < 0.0f)
            {
                // Three unequal real roots.
                nRoots = 3;
                float theta = Mathf.Acos(R / Mathf.Sqrt(-Q_CUBE));
                float SQRT_Q = Mathf.Sqrt(-Q);
                x1 = 2f * SQRT_Q * Mathf.Cos(theta / 3f) - a_over_3;
                x2 = 2f * SQRT_Q * Mathf.Cos((theta + TWO_PI) / 3f) - a_over_3;
                x3 = 2f * SQRT_Q * Mathf.Cos((theta + FOUR_PI) / 3f) - a_over_3;
            }

            else if (D > 0.0f)
            {
                // One real root.
                nRoots = 1;
                float SQRT_D = Mathf.Sqrt(D);
                float S = CubeRoot(R + SQRT_D);
                float T = CubeRoot(R - SQRT_D);
                x1 = (S + T) - a_over_3;
                x2 = float.NaN;
                x3 = float.NaN;
            }

            else
            {
                // Three real roots, at least two equal.
                nRoots = 3;
                float CBRT_R = CubeRoot(R);
                x1 = 2 * CBRT_R - a_over_3;
                x2 = CBRT_R - a_over_3;
                x3 = x2;
            }
        }


        /// <summary>
        /// Try to parse an string into an int, in case it fails, the value returned is 'defaultValue'
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <param name="defaultValue">Returned value if fails</param>
        public static int ParseInt(string s, int defaultValue)
        {
            int value;
            if (!int.TryParse(s, out value))
                value = defaultValue;
            return value;
        }

        /// <summary>
        /// Try to parse an string into a float, in case it fails, the value returned is 'defaultValue'
        /// </summary>
        /// <param name="s">String to parse</param>
        /// <param name="defaultValue">Returned value if fails</param>
        public static float ParseFloat(string s, float defaultValue)
        {
            float value;
            if (!float.TryParse(s, out value))
                value = defaultValue;
            return value;
        }
    }
}