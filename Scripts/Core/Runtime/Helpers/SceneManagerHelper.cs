﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;
using CEUtilities.Debugging;

namespace CEUtilities.Helpers
{
    public static class SceneManagerHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Is the scene the currently active?
        /// </summary>
        public static bool IsActive(string scenePath)
        {
            return SceneManager.GetActiveScene().path == scenePath;
        }

        /// <summary>
        /// Is the scene the currently active?
        /// </summary>
        public static bool IsActive(int buildIndex)
        {
            return SceneManager.GetActiveScene().buildIndex == buildIndex;
        }

        /// <summary>
        /// Is the scene the currently active?
        /// </summary>
        public static bool IsActive(Scene scene)
        {
            return SceneManager.GetActiveScene() == scene;
        }

        /// <summary>
        /// Is the scene the currently active?
        /// </summary>
        public static bool IsActive(SceneHandler sceneInfo)
        {
            return SceneManager.GetActiveScene() == sceneInfo.Scene;
        }


        /// <summary>
        /// Is the scene loaded and showing?
        /// </summary>
        public static bool IsLoaded(string scenePath)
        {
            var buildIndex = SceneUtility.GetBuildIndexByScenePath(scenePath);
            return IsLoaded(buildIndex);
        }

        /// <summary>
        /// Is the scene loaded and showing?
        /// </summary>
        public static bool IsLoaded(int buildIndex)
        {
            var loadedScenes = GetLoadedScenes();
            foreach (var scene in loadedScenes)
                if (scene.buildIndex == buildIndex)
                {
                    if (scene.isLoaded)
                        return true;
                    break;
                }
            return false;
        }

        /// <summary>
        /// Is the scene loaded and showing?
        /// </summary>
        public static bool IsLoaded(Scene scene)
        {
            return scene.isLoaded;
        }

        /// <summary>
        /// Is the scene loaded and showing?
        /// </summary>
        public static bool IsLoaded(SceneHandler sceneInfo)
        {
            return sceneInfo.Loaded;
        }


        /// <summary>
        /// Is the scene loaded but not showing?
        /// </summary>
        public static bool IsLoading(string scenePath)
        {
            var buildIndex = SceneUtility.GetBuildIndexByScenePath(scenePath);
            return IsLoading(buildIndex);
        }

        /// <summary>
        /// Is the scene loaded but not showing?
        /// </summary>
        public static bool IsLoading(int buildIndex)
        {
            var loadedScenes = GetLoadedScenes();
            foreach (var scene in loadedScenes)
                if (scene.buildIndex == buildIndex)
                {
                    if (!scene.isLoaded)
                        return true;
                    break;
                }
            return false;
        }

        /// <summary>
        /// Is the scene loaded but not showing?
        /// </summary>
        public static bool IsLoading(Scene scene)
        {
            return scene.GetState() == SceneExtension.LoadingState.Loading;
        }

        /// <summary>
        /// Is the scene loaded but not showing?
        /// </summary>
        public static bool IsLoading(SceneHandler sceneInfo)
        {
            return sceneInfo.Loading;
        }


        /// <summary>
        /// Check if the given scene name exists in the build index
        /// </summary>
        public static bool IsInBuildIndexByName(string sceneName)
        {
            var names = GetBuildSceneNames();
            foreach (var name in names)
                if (name == sceneName)
                    return true;
            return false;
        }

        /// <summary>
        /// Check if the given scene path exists in the build index
        /// </summary>
        public static bool IsInBuildIndexByPath(string scenePath)
        {
            var paths = GetBuildScenePaths();
            foreach (var path in paths)
                if (path == scenePath)
                    return true;
            return false;
        }


        /// <summary>
        /// Get all loaded scenes
        /// </summary>
        public static Scene[] GetLoadedScenes()
        {
            var scenes = new Scene[SceneManager.sceneCount];
            for (int i = 0; i < SceneManager.sceneCount; i++)
                scenes[i] = SceneManager.GetSceneAt(i);
            return scenes;
        }

        /// <summary>
        /// Get all names of the scenes listed in the build index
        /// </summary>
        public static string[] GetBuildSceneNames()
        {
            var totalScenes = SceneManager.sceneCountInBuildSettings;
            var scenes = new string[totalScenes];
            for (int i = 0; i < totalScenes; i++)
                scenes[i] = Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));
            return scenes;
        }

        /// <summary>
        /// Get all paths of the scenes listed in the build index
        /// </summary>
        public static string[] GetBuildScenePaths()
        {
            var totalScenes = SceneManager.sceneCountInBuildSettings;
            var scenes = new string[totalScenes];
            for (int i = 0; i < totalScenes; i++)
                scenes[i] = SceneUtility.GetScenePathByBuildIndex(i);
            return scenes;
        }

        /// <summary>
        /// Return the first path which math the scene name
        /// </summary>
        public static string GetFirstPathFromName(string name)
        {
            var paths = GetBuildScenePaths();
            foreach (var path in paths)
                if (Path.GetFileNameWithoutExtension(path) == name)
                    return path;
            return null;
        }


        /// <summary>
        /// Loads a scene.
        /// </summary>
        /// <param name = "buildIndex">The scene to load</param>
        /// <param name = "forceReload">If True in Single mode, the scene will be re-loaded if it is already open.</param>
        /// <param name = "loadAdditively">Load mode</param>
        public static bool OpenScene(int buildIndex, bool forceReload, LoadSceneMode mode, out Scene scene)
        {
            scene = default(Scene);

            if (buildIndex == -1)
                return false;

            var sceneCount = SceneManager.sceneCount;
            if (mode == LoadSceneMode.Additive || forceReload || !IsActive(buildIndex))
                SceneManager.LoadScene(buildIndex, mode);
            scene = SceneManager.GetSceneAt(sceneCount);

            return scene.IsValid();
        }

        /// <summary>
        /// Loads a scene.
        /// </summary>
        /// <param name = "sceneName">The scene to load</param>
        /// <param name = "forceReload">If True, the scene will be re-loaded if it is already open.</param>
        /// <param name = "loadAdditively">If True, the scene will be loaded on top of the active scene</param>
        public static bool OpenScene(string scenePath, bool forceReload, LoadSceneMode mode, out Scene scene)
        {
            var buildIndex = SceneUtility.GetBuildIndexByScenePath(scenePath);
            return OpenScene(buildIndex, forceReload, mode, out scene);
        }

        /// <summary>
        /// Loads a scene.
        /// </summary>
        /// <param name = "buildIndex">The scene to load</param>
        /// <param name = "forceReload">If True, the scene will be re-loaded if it is already open.</param>
        /// <param name = "loadAdditively">If True, the scene will be loaded on top of the active scene</param>
        public static AsyncOperation OpenSceneAsync(int buildIndex, bool forceReload, LoadSceneMode mode, bool selfActivate, out Scene scene)
        {
            AsyncOperation result = null;
            scene = default(Scene);

            if (buildIndex == -1)
                return result;

            if (mode == LoadSceneMode.Additive || forceReload || !IsActive(buildIndex))
            {
                var operation = SceneManager.LoadSceneAsync(buildIndex, mode);
                if (operation != null)
                {
                    operation.allowSceneActivation = selfActivate;
                    result = operation;
                    scene = SceneManager.GetSceneByBuildIndex(buildIndex);
                }
            }
            return result;
        }

        /// <summary>
        /// Loads a scene.
        /// </summary>
        /// <param name = "scenePath">The scene to load</param>
        /// <param name = "forceReload">If True, the scene will be re-loaded if it is already open.</param>
        /// <param name = "loadAdditively">If True, the scene will be loaded on top of the active scene</param>
        public static AsyncOperation OpenSceneAsync(string scenePath, bool forceReload, LoadSceneMode mode, bool selfActivate, out Scene scene)
        {
            var buildIndex = SceneUtility.GetBuildIndexByScenePath(scenePath);
            return OpenSceneAsync(buildIndex, forceReload, mode, selfActivate, out scene);
        }

        /// <summary>
        /// Closes a scene (addtive added scene).
        /// </summary>
        /// <param name = "scene">The scene to load</param>
        /// <returns>True if the close was successful</returns>
        public static bool CloseScene(Scene scene)
        {
            if (!scene.IsValid())
            {
                Debug.LogError(DebugHelper.Format("Trying to close an invalid scene handler."));
                return false;
            }

            if (!IsLoaded(scene) && !IsLoading(scene))
            {
                Debug.LogError(DebugHelper.Format("Trying to close an invalid or not loaded scene."));
                return false;
            }

            if (IsActive(scene))
            {
                Debug.LogError(DebugHelper.Format("Trying to close the main active scene."));
                return false;
            }

            SceneManager.UnloadSceneAsync(scene);
            return true;
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}