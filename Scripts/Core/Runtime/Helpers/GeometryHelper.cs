﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

namespace CEUtilities.Helpers
{
    public static class GeometryHelper
    {
        #region Structs

        /// <summary>
        /// Helper struct for exporting gameobjects to obj
        /// </summary>
        private struct MeshExportTarget
        {
            private MeshFilter filter;

            private Renderer renderer;


            public string Name => filter.gameObject.name;

            public Transform Transform => renderer.transform;

            public Mesh SharedMesh => filter.sharedMesh;

            public Material[] SharedMaterials => renderer.sharedMaterials;


            public MeshExportTarget(MeshFilter filter, Renderer renderer)
            {
                this.filter = filter;
                this.renderer = renderer;
            }

            public string ToOBJ(ref int startIndex, ref HashSet<Material> materialsLibrary, bool applyPosition, bool applyScale, bool applyRotation)
            {
                materialsLibrary.AddRange(SharedMaterials);

                return SharedMesh.ToOBJ
                (
                    Name,
                    ref startIndex,
                    true,
                    applyPosition ? Transform.position : Vector3.zero,
                    applyScale ? Transform.lossyScale : Vector3.one,
                    applyRotation ? Transform.rotation : Quaternion.identity,
                    SharedMaterials
                );
            }
        }

        #endregion Structs

        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods 

        /// <summary>
        /// Calculates the center of a transform, including all childs in the calculation.
        /// </summary>
        /// <param name="target">Target tranform to calculate the center.</param>
        /// <returns>Calculate the center of a gameobject.</returns>
        public static Vector3 CalculateCenter(Transform target)
        {
            return RecursiveBounds(target).center;
        }

        /// <summary>
        /// Gets the bounds containing the given transform and all his childs.
        /// </summary>
        /// <param name="target">Target transform.</param>
        /// <returns>Bounds containing every child renderer.</returns>
        public static Bounds RecursiveBounds(Transform target)
        {
            Bounds result = default(Bounds);
            var rends = target.GetComponentsInChildren<Renderer>();

            // Early exit
            if (rends == null || rends.Length == 0)
            {
                return result;
            }

            // Filter inactives
            var activeRends = new List<Renderer>();
            foreach(var rend in rends)
            {
                if (rend.enabled && rend.gameObject.activeInHierarchy)
                {
                    activeRends.Add(rend);
                }
            }

            // Encapsulates all
            if (activeRends.Count > 0)
            {
                var bounds = activeRends[0].bounds;
                for (int i = 1; i < activeRends.Count; i++)
                {
                    bounds.Encapsulate(activeRends[i].bounds);
                }
                result = bounds;
            }

            return result;
        }

        /// <summary>
        /// Generate OBJ compatible strings from the selected game objects
        /// </summary>
        /// <param name="sourceObjects"></param>
        /// <param name="name"></param>
        /// <param name="includeChildren"></param>
        /// <param name="splitObjects"></param>
        /// <param name="applyPosition"></param>
        /// <param name="applyScale"></param>
        /// <param name="applyRotation"></param>
        /// <param name="generateMaterials"></param>
        /// <param name="progressCallback"></param>
        /// <param name="objectsOBJ"></param>
        /// <param name="materialsLibraryOBJ"></param>
        public static void GenerateOBJ(GameObject[] sourceObjects, string name, bool includeChildren, bool splitObjects, bool applyPosition, bool applyScale, bool applyRotation, bool generateMaterials, Action<float, string, string> progressCallback, out string objectsOBJ, out string materialsLibraryOBJ, out Texture[] referencedTextures)
        {
            // Progress
            progressCallback?.Invoke(0f, "Starting...", "Analizing objects");

            // Get all objects
            var targetObjects = new HashSet<GameObject>();
            if (includeChildren)
            {
                var renderers = new HashSet<Renderer>();

                foreach (var gameObject in sourceObjects)
                    renderers.AddRange(gameObject.GetComponentsInChildren<Renderer>());

                foreach (var renderer in renderers)
                    targetObjects.Add(renderer.gameObject);
            }
            else
                targetObjects.AddRange(sourceObjects);

            // Create targets
            var targets = new List<MeshExportTarget>(targetObjects.Count);
            foreach (var gameObject in targetObjects)
            {
                var filter = gameObject.GetComponent<MeshFilter>();
                var renderer = gameObject.GetComponent<Renderer>();

                // Check for comps
                if (filter == null || renderer == null)
                {
                    Debug.LogWarning(string.Format("[GeometryHelper] The gameobject {0} is missing a MeshFilter o Renderer, it will be ignored.", gameObject.name));
                    continue;
                }

                // Check for static batching
                if (renderer.isPartOfStaticBatch && ApplicationHelper.AppInPlayerOrPlaymode)
                {
                    Debug.LogWarning(string.Format("[GeometryHelper] The gameobject {0} is static batched, which is incompatible with this exporter. The game object will be ignored.", gameObject.name));
                    continue;
                }

                // Add target
                targets.Add(new MeshExportTarget(filter, renderer));
            }

            // Store non repiting materials for future export
            var materialsLibrary = new HashSet<Material>();

            // Export
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("# Export from Unity '{0}', {1} objects", name, targets.Count));

            // Material library
            if (generateMaterials)
                sb.AppendLine("mtllib " + name + ".mtl");

            // Start export
            int targetIndex = 0, lastIndex = 0;
            foreach (var target in targets)
            {
                // Progress
                var progress = (float)targetIndex / targets.Count;
                progressCallback?.Invoke
                (
                    progress,
                    string.Format("Exporting object...({0}%)", (int)(progress * 100)),
                    string.Format("Exporting object {0}", target.Name)
                );

                // Generate obj string from target
                sb.AppendLine(target.ToOBJ(ref lastIndex, ref materialsLibrary, applyPosition, applyScale, applyRotation));

                // Inc index
                targetIndex++;
            }

            // Set main OBJ
            objectsOBJ = sb.ToString();

            // Generate materials
            targetIndex = 0;
            if (generateMaterials && materialsLibrary.Count > 0)
            {
                sb.Clear();
                var textures = new HashSet<Texture>();

                // 
                foreach (var material in materialsLibrary)
                {
                    // Progress
                    var progress = (float)targetIndex / materialsLibrary.Count;
                    progressCallback?.Invoke
                    (
                        .99f,
                        string.Format("Exporting materials...({0}%)", (int)(progress * 100)),
                        string.Format("Exporting material {0}", material.name)
                    );

                    // Generate obj string from material
                    string matString;
                    Texture[] matTextures;
                    material.ToOBJ(true, out matString, out matTextures);

                    sb.AppendLine(matString);
                    textures.AddRange(matTextures);

                    // Inc index
                    targetIndex++;
                }

                // Set materials OBJ
                materialsLibraryOBJ = sb.ToString();
                referencedTextures = textures.ToArray();
            }
            else
            {
                materialsLibraryOBJ = null;
                referencedTextures = null;
            }

            // Progress
            progressCallback?.Invoke(1f, "Completed", string.Empty);
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}