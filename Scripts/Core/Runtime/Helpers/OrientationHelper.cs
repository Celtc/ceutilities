﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Structs;

namespace CEUtilities.Helpers
{
    public class OrientationHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Transforms an Orientation to Quaternion rotation
        /// </summary>
        public static Orientation OrientationFromRotation(Quaternion rotation)
        {
            return (Orientation)((rotation.eulerAngles.y + 23) / 45);
        }

        /// <summary>
        /// Sum two orientation. Or change the one base for another
        /// </summary>
        public static Orientation Sum(Orientation a, Orientation b)
        {
            return (Orientation)MathHelper.RepeatIndex((int)a + (int)b, 8);
        }

        /// <summary>
        /// Get the nearest cam orieantation
        /// </summary>
        public static Orientation MainCamOrientation()
        {
            var absRotation = Mathf.Repeat(Camera.main.transform.rotation.eulerAngles.y, 360f);
            return (Orientation)(int)((absRotation + 45 * .5f) / 45f);
        }

        /// <summary>
        /// Get the nearest cam orieantation
        /// </summary>
        public static Orientation CamOrientation(Camera cam)
        {
            var absRotation = Mathf.Repeat(cam.transform.rotation.eulerAngles.y, 360f);
            return (Orientation)(int)((absRotation + 45 * .5f) / 45f);
        }

        /// <summary>
        /// Create a vector from the orientation
        /// </summary>
        public static Vector2 OrientationToVector2(Orientation o)
        {
            return new Vector2(
                (int)o > 0 && (int)o < 4 ? 1 :
                (int)o > 4 && (int)o < 8 ? -1 :
                0,
                (int)o > 6 && (int)o < 2 ? 1 :
                (int)o > 2 && (int)o < 6 ? -1 :
                0
            );
        }

        /// <summary>
        /// Create a vector from the orientation
        /// </summary>
        public static Orientation FromVector2(Vector2 vector)
        {
            if (vector.x == 1)
            {
                if (vector.y == 1) return Orientation.NE;
                if (vector.y == 0) return Orientation.E;
                if (vector.y == -1) return Orientation.SE;
            }
            else if (vector.x == 0)
            {
                if (vector.y == 1) return Orientation.N;
                if (vector.y == -1) return Orientation.S;
            }
            else if (vector.x == -1)
            {
                if (vector.y == 1) return Orientation.NW;
                if (vector.y == 0) return Orientation.W;
                if (vector.y == -1) return Orientation.SW;
            }
            return default(Orientation);
        }

        #endregion Methods
    }
}