﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using CEUtilities.Helpers;
using CEUtilities.Structs;
using System.IO;

namespace CEUtilities.Helpers
{
	public static class SpriteHelper
	{
		#region Sub Classes / Structs

		/// <summary>
		/// Auxiliar class for texture scaling
		/// </summary>
		public class ScaleThreadData
		{
			public int start;
			public int end;
			public ScaleData data;

			public ScaleThreadData(int s, int e, ScaleData d)
			{
				start = s;
				end = e;
				data = d;
			}
		}

		/// <summary>
		/// Auxiliar class for texture scaling
		/// </summary>
		public class ScaleData
		{
			public Color[] texColors;
			public Color[] newColors;
			public int w;
			public float ratioX;
			public float ratioY;
			public int w2;

			public int finishCount;
		}

		#endregion

		#region Exposed fields

		#endregion Exposed fields

		#region Internal fields

		private static Mutex mutex;

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		/// <summary>
		/// Verify if the texture is readable
		/// </summary>
		public static bool CheckReadable(Sprite sprite)
		{
			return sprite.texture.IsReadable();
		}


		/// <summary>
		/// Create a new Sprite object fast
		/// </summary>
		public static Sprite Create(Texture2D tex)
		{
			return Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.one * .5f);
		}


		/// <summary>
		/// Invert the colores of the texture
		/// </summary>
		public static Sprite Invert(Sprite sprite)
		{
			return Invert(sprite, true);
		}

		/// <summary>
		/// Return an inverted version of the sprite
		/// </summary>
		/// <returns></returns>
		public static Sprite Invert(Sprite sprite, bool keepAlpha)
		{
			var invertedTex = sprite.texture.Invert(keepAlpha);

			var result = Sprite.Create(invertedTex,
				new Rect(0, 0, invertedTex.width, invertedTex.height), Vector2.one * .5f);

			return result;
		}


		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath)
		{
			return ImportSprite(filePath, TextureFormat.ARGB32, true, FilterMode.Bilinear, false, Vector2.one * .5f, 100.0f, 0, SpriteMeshType.Tight, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format)
		{
			return ImportSprite(filePath, format, true, FilterMode.Bilinear, false, Vector2.one * .5f, 100.0f, 0, SpriteMeshType.Tight, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format, bool mipmap)
		{
			return ImportSprite(filePath, format, mipmap, FilterMode.Bilinear, false, Vector2.one * .5f, 100.0f, 0, SpriteMeshType.Tight, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode)
		{
			return ImportSprite(filePath, format, mipmap, filterMode, false, Vector2.one * .5f, 100.0f, 0, SpriteMeshType.Tight, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable)
		{
			return ImportSprite(filePath, format, mipmap, filterMode, readable, Vector2.one * .5f, 100.0f, 0, SpriteMeshType.Tight, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable, Vector2 pivot)
		{
			return ImportSprite(filePath, format, mipmap, filterMode, readable, pivot, 100.0f, 0, SpriteMeshType.Tight, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable, Vector2 pivot, float pixelsPerUnit)
		{
			return ImportSprite(filePath, format, mipmap, filterMode, readable, pivot, pixelsPerUnit, 0, SpriteMeshType.Tight, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable, Vector2 pivot, float pixelsPerUnit, uint extrude)
		{
			return ImportSprite(filePath, format, mipmap, filterMode, readable, pivot, pixelsPerUnit, extrude, SpriteMeshType.Tight, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType)
		{
			return ImportSprite(filePath, format, mipmap, filterMode, readable, pivot, pixelsPerUnit, extrude, meshType, Vector4.zero);
		}

		/// <summary>
		/// Creates an sprite from a file
		/// </summary>
		public static Sprite ImportSprite(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, Vector4 border)
		{
			Sprite result = null;

			if (File.Exists(filePath))
			{
				var texture = TextureHelper.ImportTexture(filePath, format, mipmap, filterMode, readable);
				result = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
					pivot, pixelsPerUnit, extrude, meshType, border);
				;
			}
			else
			{
				Debug.LogError("[SpriteHelper] File not found: " + filePath);
			}

			return result;
		}

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Main method of texture scale
		/// </summary>
		private static void ThreadedScale(Texture2D tex, int newWidth, int newHeight, bool useBilinear)
		{
			ScaleData scaleData = new ScaleData();

			scaleData.texColors = tex.GetPixels();
			scaleData.newColors = new Color[newWidth * newHeight];
			if (useBilinear)
			{
				scaleData.ratioX = 1.0f / ((float) newWidth / (tex.width - 1));
				scaleData.ratioY = 1.0f / ((float) newHeight / (tex.height - 1));
			}
			else
			{
				scaleData.ratioX = ((float) tex.width) / newWidth;
				scaleData.ratioY = ((float) tex.height) / newHeight;
			}
			scaleData.w = tex.width;
			scaleData.w2 = newWidth;
			var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
			var slice = newHeight / cores;

			scaleData.finishCount = 0;

			if (mutex == null)
				mutex = new Mutex(false);

			if (cores > 1)
			{
				int i = 0;
				ScaleThreadData threadData;
				for (i = 0; i < cores - 1; i++)
				{
					threadData = new ScaleThreadData(slice * i, slice * (i + 1), scaleData);
					ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(InternalBilinearScale) : new ParameterizedThreadStart(InternalPointScale);
					Thread thread = new Thread(ts);
					thread.Start(threadData);
				}
				threadData = new ScaleThreadData(slice * i, newHeight, scaleData);
				if (useBilinear)
				{
					InternalBilinearScale(threadData);
				}
				else
				{
					InternalPointScale(threadData);
				}
				while (scaleData.finishCount < cores)
				{
					Thread.Sleep(1);
				}
			}
			else
			{
				ScaleThreadData threadData = new ScaleThreadData(0, newHeight, scaleData);
				if (useBilinear)
				{
					InternalBilinearScale(threadData);
				}
				else
				{
					InternalPointScale(threadData);
				}
			}

			tex.Resize(newWidth, newHeight);
			tex.SetPixels(scaleData.newColors);
			tex.Apply();
		}

		/// <summary>
		/// Applies bilinear scale
		/// </summary>
		private static void InternalBilinearScale(System.Object obj)
		{
			ScaleThreadData threadData = (ScaleThreadData) obj;
			ScaleData data = threadData.data;

			for (var y = threadData.start; y < threadData.end; y++)
			{
				int yFloor = (int) Mathf.Floor(y * data.ratioY);
				var y1 = yFloor * data.w;
				var y2 = (yFloor + 1) * data.w;
				var yw = y * data.w2;

				for (var x = 0; x < data.w2; x++)
				{
					int xFloor = (int) Mathf.Floor(x * data.ratioX);
					var xLerp = x * data.ratioX - xFloor;
					data.newColors[yw + x] = ColorHelper.ColorLerpUnclamped(ColorHelper.ColorLerpUnclamped(data.texColors[y1 + xFloor], data.texColors[y1 + xFloor + 1], xLerp),
																	   ColorHelper.ColorLerpUnclamped(data.texColors[y2 + xFloor], data.texColors[y2 + xFloor + 1], xLerp),
																	   y * data.ratioY - yFloor);
				}
			}

			mutex.WaitOne();
			data.finishCount++;
			mutex.ReleaseMutex();
		}

		/// <summary>
		/// Applies point scale
		/// </summary>
		private static void InternalPointScale(System.Object obj)
		{
			ScaleThreadData threadData = (ScaleThreadData) obj;
			ScaleData data = threadData.data;

			for (var y = threadData.start; y < threadData.end; y++)
			{
				var thisY = (int) (data.ratioY * y) * data.w;
				var yw = y * data.w2;
				for (var x = 0; x < data.w2; x++)
				{
					data.newColors[yw + x] = data.texColors[(int) (thisY + data.ratioX * x)];
				}
			}

			mutex.WaitOne();
			data.finishCount++;
			mutex.ReleaseMutex();
		}

		#endregion Non Public Methods
	}
}