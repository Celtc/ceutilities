﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Colors;

namespace CEUtilities.Helpers
{
    public static class HSVColorHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Lerps the specified start.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="amount">The amount.</param>
        /// <returns></returns>
        public static HSVColor Lerp(HSVColor start, HSVColor end, float amount)
        {
            HSVColor result;
            if (amount >= 1f)
            {
                result = new HSVColor(end);
            }
            else if (amount <= 0f)
            {
                result = new HSVColor(start);
            }
            else
            {
                result = new HSVColor
                (
                    start.Hue + (end.Hue - start.Hue) * amount,
                    start.Saturation + (end.Saturation - start.Saturation) * amount,
                    start.Value + (end.Value - start.Value) * amount,
                    start.Alpha + (end.Alpha - start.Alpha) * amount
                );
            }
            return result;
        }


        /// <summary>
        /// Returns the ToHSV values of a RGB color.
        /// </summary>
        /// <param name="r">'Red' component</param>
        /// <param name="g">'Green' component</param>
        /// <param name="b">'Blue' component</param>
        /// <returns></returns>
        public static HSVColor FromRGB(float r, float g, float b, float a = 1f)
        {
            float hue, saturation, value;

            var max = Mathf.Max(Mathf.Max(r, g), b);
            var min = Mathf.Min(Mathf.Min(r, g), b);
            var delta = max - min;

            // Value is our max hsvColor
            value = max;

            // Extremes
            if (Mathf.Approximately(max, 0f) || Mathf.Approximately(delta, 0f))
            {
                // All colors are white, no saturation and hue are defined
                hue = 0f;
                saturation = 0f;
            }
            else
            {
                // Saturation is a percentage of max
                saturation = delta / max;

                // Hue depends which hsvColor is max (this creates a rainbow effect)
                if (Mathf.Approximately(r, max))
                {
                    // Between yellow & magenta
                    hue = ((g - b) / delta) % 6f;
                }
                else if (Mathf.Approximately(g, max))
                {
                    // Between cyan & yellow
                    hue = 2f + (b - r) / delta;
                }
                else
                {
                    // Between magenta & cyan
                    hue = 4f + (r - g) / delta;
                }

                // Turn hue into 0-360 degrees
                hue *= 60f;
                if (hue < 0f)
                {
                    hue += 360f;
                }
            }

            return new HSVColor(hue, saturation, value, a);
        }

        /// <summary>
        /// Returns the ToHSV values of a RGB color.
        /// </summary>
        public static HSVColor FromRGB(Color rgb) => FromRGB(rgb.r, rgb.g, rgb.b, rgb.a);


        /// <summary>
        /// Generates a RGB hsvColor using specified hue, saturation, value (brightness) and  hue and customAlpha
        /// </summary>
        /// <param name="hue">[0f, 360f] degrees</param>
        /// <param name="saturation">[0f, 1f]</param>
        /// <param name="value">[0f, 1f]</param>
        /// <param name="customAlpha">[0f, 1f]</param>
        /// <returns>RGB hsvColor</returns>
        public static Color ToRGB(float hue, float saturation, float value, float alpha = 1.0f)
        {
            float r = 0f, g = 0f, b = 0f;

            var clampHue = hue % 360f;
            var clampSaturation = Mathf.Clamp01(saturation);
            var clampValue = Mathf.Clamp01(value);

            float c = clampValue * clampSaturation;
            float x = c * (1f - Mathf.Abs(((clampHue / 60f) % 2) - 1f));
            float m = clampValue - c;

            if (clampHue < 60f)
            {
                r = c;
                g = x;
            }
            else if (clampHue < 120f)
            {
                r = x;
                g = c;
            }
            else if (clampHue < 180f)
            {
                g = c;
                b = x;
            }
            else if (clampHue < 240f)
            {
                g = x;
                b = c;
            }
            else if (clampHue < 300f)
            {
                r = x;
                b = c;
            }
            else if (clampHue <= 360f)
            {
                r = c;
                b = x;
            }
            else
            {
                r = 0f;
                g = 0f;
                b = 0f;
            }

            return new Color(r + m, g + m, b + m, alpha);
        }

        /// <summary>
        /// To the RGB.
        /// </summary>
        /// <param name="hsvColor">Color of the HSV.</param>
        /// <returns></returns>
        public static Color ToRGB(HSVColor hsv) => ToRGB(hsv.Hue, hsv.Saturation, hsv.Value, hsv.Alpha);


        /// <summary>
        /// To the hexadecimal string.
        /// </summary>
        /// <param name="includeAlpha">if set to <c>true</c> [include alpha].</param>
        /// <returns></returns>
        public static string ToHexString(HSVColor hsv, bool includeAlpha = false)
        {
            var rgb = ToRGB(hsv);
            return ColorHelper.ToHexString(rgb, includeAlpha);
        }

        /// <summary>
        /// Copy color values from an HEX color.
        /// </summary>
        /// <param name="hex"></param>
        /// <param name="customAlpha">Use non-negative values to override the alpha that could come from the hex</param>
        public static HSVColor FromHexString(string hex)
        {
            var rgb = ColorHelper.FromHexString(hex);
            return FromRGB(rgb);
        }


        /// <summary>
        /// To a vector3.
        /// </summary>
        /// <param name="hsv">The value.</param>
        /// <returns></returns>
        public static Vector3 ToVector3(HSVColor hsv)
        {
            return new Vector3(hsv.Hue, hsv.Saturation, hsv.Value);
        }

        /// <summary>
        /// To the vector4.
        /// </summary>
        /// <param name="hsv">The value.</param>
        /// <returns></returns>
        public static Vector4 ToVector4(HSVColor hsv)
        {
            return new Vector4(hsv.Hue, hsv.Saturation, hsv.Value, hsv.Alpha);
        }
        

        /// <summary>
        /// Generates an HSV color random hue, saturation, value (brightness).
        /// </summary>
        /// <param name="useGoldenRatio">'True': the golden ration will be used to better randomize the hue</param>
        /// <returns>RGB hsvColor</returns>
        public static HSVColor GetRandom(bool useGoldenRatio = false)
        {
            return GetRandomWithSaturationValue(Random.value, Random.value, useGoldenRatio);
        }

        /// <summary>
        /// Generates a RGB hsvColor using random saturation and value (brightness) and specified hue and customAlpha.
        /// </summary>
        /// <param name="hue">Hue of the hsvColor</param>
        /// <returns>RGB hsvColor</returns>
        public static HSVColor GetRandomWithHue(float hue)
        {
            return new HSVColor(hue, Random.value, Random.value);
        }

        /// <summary>
        /// Generates a RGB hsvColor using random saturation and value (brightness) and specified hue and customAlpha.
        /// </summary>
        /// <param name="hue">Hue of the hsvColor</param>
        /// <param name="saturation">Saturation of the hsvColor</param>
        /// <returns>RGB hsvColor</returns>
        public static HSVColor WithHueSaturation(float hue, float saturation)
        {
            return new HSVColor(hue, saturation, Random.value);
        }

        /// <summary>
        /// Generates a RGB hsvColor using random saturation and value (brightness) and specified hue and customAlpha.
        /// </summary>
        /// <param name="hue">Hue of the hsvColor</param>
        /// <param name="value">Brightness of the hsvColor</param>
        /// <returns>RGB hsvColor</returns>
        public static HSVColor WithHueValue(float hue, float value)
        {
            return new HSVColor(hue, Random.value, value);
        }

        /// <summary>
        /// Generates a RGB hsvColor using random hue and saturation, and specified value (brightness).
        /// </summary>
        /// <param name="value">Brightness of the hsvColor</param>
        /// <param name="useGoldenRatio">'True': the golden ration will be used to better randomize the hue</param>
        /// <returns>RGB hsvColor</returns>
        public static HSVColor GetRandomWithValue(float value, bool useGoldenRation = false)
        {
            return GetRandomWithSaturationValue(Random.value, value, useGoldenRation);
        }

        /// <summary>
        /// Generates a RGB hsvColor using random hue and value (brightness), and specified saturation.
        /// </summary>
        /// <param name="saturation">Saturation of the hsvColor</param>
        /// <param name="useGoldenRatio">'True': the golden ration will be used to better randomize the hue</param>
        /// <returns>RGB hsvColor</returns>
        public static HSVColor GetRandomWithSaturation(float saturation, bool useGoldenRation = false)
        {
            return GetRandomWithSaturationValue(saturation, Random.value, useGoldenRation);
        }

        /// <summary>
        /// Generates a RGB hsvColor using random hue and specified saturation and value (brightness).
        /// </summary>
        /// <param name="saturation">Saturation of the hsvColor</param>
        /// <param name="value">Brightness of the hsvColor</param>
        /// <param name="useGoldenRatio">'True': the golden ration will be used to better randomize the hue</param>
        /// <returns>RGB hsvColor</returns>
        public static HSVColor GetRandomWithSaturationValue(float saturation, float value, bool useGoldenRatio = false)
        {
            var newHue = Random.value;
            if (useGoldenRatio)
            {
                var goldenRadioConj = 0.618033988749895f;
                newHue += goldenRadioConj;
                newHue %= 1;
            }
            return new HSVColor(newHue, saturation, value);
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}