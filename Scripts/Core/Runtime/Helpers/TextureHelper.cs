﻿using UnityEngine;
using System.IO;
using System.Runtime.CompilerServices;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using CEUtilities.Helpers;
using CEUtilities.Structs;
using System.Linq;

namespace CEUtilities.Helpers
{
    public static class TextureHelper
    {
        #region Sub Classes / Structs

        /// <summary>
        /// Auxiliar class for texture scaling
        /// </summary>
        public class ScaleThreadData
        {
            public int start;
            public int end;
            public ScaleData data;

            public ScaleThreadData(int s, int e, ScaleData d)
            {
                start = s;
                end = e;
                data = d;
            }
        }

        /// <summary>
        /// Auxiliar class for texture scaling
        /// </summary>
        public class ScaleData
        {
            public Color[] texColors;
            public Color[] newColors;
            public int w;
            public float ratioX;
            public float ratioY;
            public int w2;

            public int finishCount;
        }

        #endregion

        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private static Mutex mutex;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Converts a pixel X Y coordinate into a linear coord
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int PixelCoordToIndex(int x, int y, int width)
        {
            return y * width + x;
        }

        /// <summary>
        /// Verify if the texture is readable
        /// </summary>
        public static bool CheckReadable(Texture2D tex)
        {
            try
            {
                tex.GetPixel(0, 0);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Create a texture filled with a color
        /// </summary>
        public static Texture2D CreateColorTexture(Color color, int width, int height)
        {
            return CreateColorTexture(color, width, height, TextureFormat.ARGB32, false, true);
        }

        /// <summary>
        /// Create a texture filled with a color
        /// </summary>
        public static Texture2D CreateColorTexture(Color color, int width, int height, TextureFormat format, bool mipmap, bool linear)
        {
            var tex = new Texture2D(width, height, format, mipmap, linear);
            var colors = new Color[width * height];
            for (int i = 0; i < colors.Length; i++)
                colors[i] = color;
            tex.SetPixels(colors);
            tex.Apply();
            return tex;
        }

        /// <summary>
        /// Creates an empty texture with the same parameters as the source texture
        /// </summary>
        public static Texture2D CreateEmptyTexture(Texture2D tex)
        {
            var emptyTex = new Texture2D(tex.width, tex.height, tex.format, tex.mipmapCount > 1);
            CopyTextureParameters(tex, emptyTex);
            return emptyTex;
        }

        /// <summary>
        /// Creates an empty texture with the same parameters as the source texture
        /// </summary>
        public static Texture2D CreateEmptyTexture(Texture2D tex, TextureFormat format)
        {
            var emptyTex = new Texture2D(tex.width, tex.height, format, tex.mipmapCount > 1);
            CopyTextureParameters(tex, emptyTex);
            return emptyTex;
        }

        /// <summary>
        /// Copy the parameters form one texture to another
        /// </summary>
        private static void CopyTextureParameters(Texture2D from, Texture2D to)
        {
            to.wrapMode = from.wrapMode;
            to.anisoLevel = from.anisoLevel;
            to.filterMode = from.filterMode;
        }


        /// <summary>
        /// Add a watermark to an image
        /// </summary>
        public static Texture2D AddWatermark(Texture2D background, Texture2D watermark, float alpha)
        {
            return AddWatermark(background, watermark, alpha, Margin.zero, Pivot.BottomLeft);
        }

        /// <summary>
        /// Add a watermark to an image
        /// </summary>
        public static Texture2D AddWatermark(Texture2D background, Texture2D watermark, float alpha, Margin margins)
        {
            return AddWatermark(background, watermark, alpha, margins, Pivot.BottomLeft);
        }

        /// <summary>
        /// Add a watermark to an image
        /// </summary>
        public static Texture2D AddWatermark(Texture2D background, Texture2D watermark, float alpha, Margin margins, Pivot pivot)
        {
            int startX = 0;
            int startY = 0;

            if (pivot == Pivot.TopLeft)
            {
                startX = (int)margins.left + 0;
                startY = background.height - watermark.height - (int)margins.top;
            }
            else if (pivot == Pivot.TopRight)
            {
                startX = background.width - watermark.width - (int)margins.right;
                startY = background.height - watermark.height - (int)margins.top;
            }
            else if (pivot == Pivot.BottomLeft)
            {
                startX = (int)margins.left + 0;
                startY = (int)margins.bottom + 0;
            }
            else if (pivot == Pivot.BottomRight)
            {
                startX = background.width - watermark.width - (int)margins.right;
                startY = (int)margins.bottom + 0;
            }
            else if (pivot == Pivot.Center)
            {
                startX = (int)(background.width * .5f - watermark.width * .5f + margins.left * .5f - margins.right * .5f);
                startY = (int)(background.height * .5f - watermark.height * .5f - margins.top * .5f + margins.bottom * .5f);
            }

            for (int x = 0; x < watermark.width; x++)
            {
                for (int y = 0; y < watermark.height; y++)
                {
                    Color bgColor = background.GetPixel(x + startX, y + startY);
                    Color wmColor = watermark.GetPixel(x, y);
                    Color final_color = Color.Lerp(bgColor, wmColor, wmColor.a * alpha);

                    background.SetPixel(x + startX, y + startY, final_color);
                }
            }

            background.Apply();
            return background;
        }

        /// <summary>
        /// Blends a texture into another one.
        /// </summary>
        /// <param name="source">Source texture</param>
        /// <param name="blending">Texture to blend into source</param>
        /// <param name="margins">Margins to apply into the source</param>
        /// <param name="pivot">Pivot used when blending</param>
        /// <param name="doApply">Should apply changes in the texture?</param>
        public static void BlendTextures(Texture2D source, Texture2D blending, Margin margins, Pivot pivot, bool doApply)
        {
            int startX = 0;
            int startY = 0;
            
            // Pivot
            if (pivot == Pivot.TopLeft)
            {
                startX = (int)margins.left + 0;
                startY = source.height - blending.height - (int)margins.top;
            }
            else if (pivot == Pivot.TopRight)
            {
                startX = source.width - blending.width - (int)margins.right;
                startY = source.height - blending.height - (int)margins.top;
            }
            else if (pivot == Pivot.BottomLeft)
            {
                startX = (int)margins.left + 0;
                startY = (int)margins.bottom + 0;
            }
            else if (pivot == Pivot.BottomRight)
            {
                startX = source.width - blending.width - (int)margins.right;
                startY = (int)margins.bottom + 0;
            }
            else if (pivot == Pivot.Center)
            {
                startX = (int)(source.width * .5f - blending.width * .5f + margins.left * .5f - margins.right * .5f);
                startY = (int)(source.height * .5f - blending.height * .5f - margins.top * .5f + margins.bottom * .5f);
            }

            // Get pixels blocks
            var srcBlockWidth = blending.width;//source.width - startX;
            var srcBlockHeight = blending.height;//source.height - startY;
            var srcColors = source.GetPixels(startX, startY, srcBlockWidth, srcBlockHeight);
            var dstColors = blending.GetPixels();

            // Iterate through block
            Color srcColor, dstColor;
            for (int i = 0; i < srcColors.Length; i++)
            {
                dstColor = dstColors[i];
                if (dstColor.a < 0.01f)
                    continue;
                
                srcColor = srcColors[i];

                srcColors[i] = BlendingNormal(srcColor, dstColor);
            }            

            // Set
            source.SetPixels(startX, startY, srcBlockWidth, srcBlockHeight, srcColors);

            if (doApply)
                source.Apply();
        }

        /// <summary>
        /// Blends multiple textures into another one. All blending textures must be the same resolution.
        /// </summary>
        /// <param name="source">Source texture</param>
        /// <param name="margins">Margins to apply into the source</param>
        /// <param name="pivot">Pivot used when blending</param>
        /// <param name="doApply">Should apply changes in the texture?</param>
        /// <param name="blendings">Textures to blend into source</param>
        public static void BlendMultipleTextures(Texture2D source, Margin margins, Pivot pivot, bool doApply, params Texture2D[] blendings)
        {
            int startX = 0;
            int startY = 0;

            // Pivot
            if (pivot == Pivot.TopLeft)
            {
                startX = (int)margins.left + 0;
                startY = source.height - blendings[0].height - (int)margins.top;
            }
            else if (pivot == Pivot.TopRight)
            {
                startX = source.width - blendings[0].width - (int)margins.right;
                startY = source.height - blendings[0].height - (int)margins.top;
            }
            else if (pivot == Pivot.BottomLeft)
            {
                startX = (int)margins.left + 0;
                startY = (int)margins.bottom + 0;
            }
            else if (pivot == Pivot.BottomRight)
            {
                startX = source.width - blendings[0].width - (int)margins.right;
                startY = (int)margins.bottom + 0;
            }
            else if (pivot == Pivot.Center)
            {
                startX = (int)(source.width * .5f - blendings[0].width * .5f + margins.left * .5f - margins.right * .5f);
                startY = (int)(source.height * .5f - blendings[0].height * .5f - margins.top * .5f + margins.bottom * .5f);
            }

            // Get source pixels block
            var srcBlockWidth = blendings[0].width;//source.width - startX;
            var srcBlockHeight = blendings[0].height;//source.height - startY;
            var srcColors = source.GetPixels(startX, startY, srcBlockWidth, srcBlockHeight);

            // Get blendings pixels blocks
            Color[][] dstsColors = new Color[blendings.Length][];
            for (int i = 0; i < blendings.Length; i++)
                dstsColors[i] = blendings[i].GetPixels();

            // Iterate through source block
            Color dstColor, outColor;
            for (int i = 0; i < srcColors.Length; i++)
            {
                outColor = srcColors[i];

                // Blends
                for (int ii = 0; ii < blendings.Length; ii++)
                {
                    dstColor = dstsColors[ii][i];
                    if (dstColor.a < 0.01f)
                        continue;

                    outColor = BlendingNormal(outColor, dstColor);
                }
                
                srcColors[i] = outColor;
            }

            // Set
            source.SetPixels(startX, startY, srcBlockWidth, srcBlockHeight, srcColors);

            if (doApply)
                source.Apply();
        }

        /// <summary>
        /// Do a point scale of a texture.
        /// </summary>
        /// <param name="tex">Texture to scale</param>
        /// <param name="width">New width of the texture</param>
        /// <param name="height">New height of the texture</param>
        public static void ScalePoint(Texture2D tex, int width, int height)
        {
            ThreadedScale(tex, width, height, false);
        }

        /// <summary>
        /// Do a point scale of a texture.
        /// </summary>
        /// <param name="tex">Texture to scale</param>
        /// <param name="width">New width of the texture</param>
        /// <param name="height">New height of the texture</param>
        public static void ScalePoint(Texture2D tex, int width, int height, bool preserveAR)
        {
            if (!preserveAR)
            {
                ThreadedScale(tex, width, height, false);
            }
            else
            {
                var aspectRatio = (float)tex.width / (float)tex.height;

                var widthBase = (aspectRatio > 1) ?
                    (new Vector2(width, (int)(width / aspectRatio))) :
                    (new Vector2((int)(width * aspectRatio), width));
                var heightBase = (aspectRatio > 1) ?
                    (new Vector2(height, (int)(height / aspectRatio))) :
                    (new Vector2((int)(height * aspectRatio), height));

                if (widthBase.y > height)
                    ScaleBilinear(tex, (int)heightBase.x, (int)heightBase.y, false);
                else
                    ScaleBilinear(tex, (int)widthBase.x, (int)widthBase.y, false);
            }
        }

        /// <summary>
        /// Do a bilinear scale of a texture.
        /// </summary>
        /// <param name="tex">Texture to scale</param>
        /// <param name="width">New width of the texture</param>
        /// <param name="height">New height of the texture</param>
        public static void ScaleBilinear(Texture2D tex, int width, int height)
        {
            ThreadedScale(tex, width, height, true);
        }

        /// <summary>
        /// Do a bilinear scale of a texture.
        /// </summary>
        /// <param name="tex">Texture to scale</param>
        /// <param name="width">New width of the texture</param>
        /// <param name="height">New height of the texture</param>
        public static void ScaleBilinear(Texture2D tex, int width, int height, bool preserveAR)
        {
            if (!preserveAR)
            {
                ThreadedScale(tex, width, height, true);
            }
            else
            {
                var aspectRatio = (float)tex.width / (float)tex.height;

                var widthBase = (aspectRatio > 1) ?
                    (new Vector2(width, (int)(width / aspectRatio))) :
                    (new Vector2((int)(width * aspectRatio), width));
                var heightBase = (aspectRatio > 1) ?
                    (new Vector2(height, (int)(height / aspectRatio))) :
                    (new Vector2((int)(height * aspectRatio), height));

                if (widthBase.y > height)
                    ScaleBilinear(tex, (int)heightBase.x, (int)heightBase.y, true);
                else
                    ScaleBilinear(tex, (int)widthBase.x, (int)widthBase.y, true);
            }
        }


        /// <summary>
        /// Crops a texture
        /// </summary>
        public static void Crop(Texture2D tex, int width, int height)
        {
            Crop(tex, width, height, Pivot.Center);
        }

        /// <summary>
        /// Crops a texture
        /// </summary>
        public static void Crop(Texture2D tex, int width, int height, Pivot pivot)
        {
            // Verify
            if (width >= tex.width && height >= tex.height)
                return;

            // Clamp
            width = Mathf.Min(width, tex.width);
            height = Mathf.Min(height, tex.height);

            // Set the x and y pos to crop
            int x = 0, y = 0;
            switch (pivot)
            {
                case Pivot.Center:
                    x = (int)((tex.width - width) / 2f);
                    y = (int)((tex.height - height) / 2f);
                    break;
                case Pivot.BottomLeft:
                    x = 0;
                    y = 0;
                    break;
                case Pivot.TopRight:
                    x = tex.width - width;
                    y = tex.height - height;
                    break;
                case Pivot.BottomRight:
                    x = tex.width - width;
                    y = 0;
                    break;
                case Pivot.TopLeft:
                    x = 0;
                    y = tex.height - height;
                    break;
            }

            // Crop
            var pix = tex.GetPixels(x, y, width, height);
            tex.Resize(width, height);
            tex.SetPixels(pix);
            tex.Apply();
        }

        /// <summary>
        /// Crops a texture
        /// </summary>
        public static void Crop(Texture2D tex, Rect area)
        {
            // Verify
            if (area.width >= tex.width && area.height >= tex.height)
                return;

            // Clamp
            var width = (int)Mathf.Min(area.width, tex.width);
            var height = (int)Mathf.Min(area.height, tex.height);

            // Crop
            var pix = tex.GetPixels((int)area.x, (int)area.y, width, height);
            tex.Resize(width, height);
            tex.SetPixels(pix);
            tex.Apply();
        }


        /// <summary>
        /// Invert the colores of the texture
        /// </summary>
        public static Texture2D Invert(Texture2D tex)
        {
            return Invert(tex, true);
        }

        /// <summary>
        /// Invert the colores of the texture
        /// </summary>
        public static Texture2D Invert(Texture2D tex, bool keepAlpha)
        {
            var pixels = tex.GetPixels();
            for (int i = 0; i < pixels.Length; i++)
                pixels[i] = ColorHelper.Invert(pixels[i]);

            var result = new Texture2D(tex.width, tex.height, tex.format, tex.mipmapCount > 0);
            result.SetPixels(pixels);

            return result;
        }


        /// <summary>
        /// Creates a texture from a file
        /// </summary>
        public static Texture2D ImportTexture(string filePath)
        {
            return ImportTexture(filePath, TextureFormat.ARGB32, true, FilterMode.Bilinear, false);
        }

        /// <summary>
        /// Creates a texture from a file
        /// </summary>
        public static Texture2D ImportTexture(string filePath, TextureFormat format)
        {
            return ImportTexture(filePath, format, true, FilterMode.Bilinear, false);
        }

        /// <summary>
        /// Creates a texture from a file
        /// </summary>
        public static Texture2D ImportTexture(string filePath, TextureFormat format, bool mipmap)
        {
            return ImportTexture(filePath, format, mipmap, FilterMode.Bilinear, false);
        }

        /// <summary>
        /// Creates a texture from a file
        /// </summary>
        public static Texture2D ImportTexture(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode)
        {
            return ImportTexture(filePath, format, mipmap, filterMode, false);
        }

        /// <summary>
        /// Creates a texture from a file
        /// </summary>
        public static Texture2D ImportTexture(string filePath, TextureFormat format, bool mipmap, FilterMode filterMode, bool readable)
        {
            Texture2D result = null;

            if (File.Exists(filePath))
            {
                try
                {
                    byte[] fileData;
                    fileData = File.ReadAllBytes(filePath);
                    result = new Texture2D(2, 2, format, mipmap)
                    {
                        filterMode = filterMode
                    };
                    result.LoadImage(fileData, !readable);
                    result.name = Path.GetFileNameWithoutExtension(filePath);
                }
                catch (System.Exception e)
                {
                    Debug.LogErrorFormat("[TextureHelper] Error trying to import texture at path \"{0}\": {1}", filePath, e.Message);
                }
            }
            else
            {
                Debug.LogError("[TextureHelper] File not found: " + filePath);
            }

            return result;
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Main method of texture scale
        /// </summary>
        private static void ThreadedScale(Texture2D tex, int newWidth, int newHeight, bool useBilinear)
        {
            ScaleData scaleData = new ScaleData();

            scaleData.texColors = tex.GetPixels();
            scaleData.newColors = new Color[newWidth * newHeight];
            if (useBilinear)
            {
                scaleData.ratioX = 1.0f / ((float)newWidth / (tex.width - 1));
                scaleData.ratioY = 1.0f / ((float)newHeight / (tex.height - 1));
            }
            else
            {
                scaleData.ratioX = ((float)tex.width) / newWidth;
                scaleData.ratioY = ((float)tex.height) / newHeight;
            }
            scaleData.w = tex.width;
            scaleData.w2 = newWidth;
            var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
            var slice = newHeight / cores;

            scaleData.finishCount = 0;

            if (mutex == null)
                mutex = new Mutex(false);

            if (cores > 1)
            {
                int i = 0;
                ScaleThreadData threadData;
                for (i = 0; i < cores - 1; i++)
                {
                    threadData = new ScaleThreadData(slice * i, slice * (i + 1), scaleData);
                    ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(InternalBilinearScale) : new ParameterizedThreadStart(InternalPointScale);
                    Thread thread = new Thread(ts);
                    thread.Start(threadData);
                }
                threadData = new ScaleThreadData(slice * i, newHeight, scaleData);
                if (useBilinear)
                {
                    InternalBilinearScale(threadData);
                }
                else
                {
                    InternalPointScale(threadData);
                }
                while (scaleData.finishCount < cores)
                {
                    Thread.Sleep(1);
                }
            }
            else
            {
                ScaleThreadData threadData = new ScaleThreadData(0, newHeight, scaleData);
                if (useBilinear)
                {
                    InternalBilinearScale(threadData);
                }
                else
                {
                    InternalPointScale(threadData);
                }
            }

            tex.Resize(newWidth, newHeight);
            tex.SetPixels(scaleData.newColors);
            tex.Apply();
        }

        /// <summary>
        /// Applies bilinear scale
        /// </summary>
        private static void InternalBilinearScale(System.Object obj)
        {
            ScaleThreadData threadData = (ScaleThreadData)obj;
            ScaleData data = threadData.data;

            for (var y = threadData.start; y < threadData.end; y++)
            {
                int yFloor = (int)Mathf.Floor(y * data.ratioY);
                var y1 = yFloor * data.w;
                var y2 = (yFloor + 1) * data.w;
                var yw = y * data.w2;

                for (var x = 0; x < data.w2; x++)
                {
                    int xFloor = (int)Mathf.Floor(x * data.ratioX);
                    var xLerp = x * data.ratioX - xFloor;
                    data.newColors[yw + x] = ColorHelper.ColorLerpUnclamped(ColorHelper.ColorLerpUnclamped(data.texColors[y1 + xFloor], data.texColors[y1 + xFloor + 1], xLerp),
                                                                       ColorHelper.ColorLerpUnclamped(data.texColors[y2 + xFloor], data.texColors[y2 + xFloor + 1], xLerp),
                                                                       y * data.ratioY - yFloor);
                }
            }

            mutex.WaitOne();
            data.finishCount++;
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// Applies point scale
        /// </summary>
        private static void InternalPointScale(System.Object obj)
        {
            ScaleThreadData threadData = (ScaleThreadData)obj;
            ScaleData data = threadData.data;

            for (var y = threadData.start; y < threadData.end; y++)
            {
                var thisY = (int)(data.ratioY * y) * data.w;
                var yw = y * data.w2;
                for (var x = 0; x < data.w2; x++)
                {
                    data.newColors[yw + x] = data.texColors[(int)(thisY + data.ratioX * x)];
                }
            }

            mutex.WaitOne();
            data.finishCount++;
            mutex.ReleaseMutex();
        }

        /// <summary>
        /// Normal colors blending base on Porter and Duff equations
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static Color BlendingNormal(Color srcColor, Color dstColor)
        {
            var temp = srcColor.a * (1 - dstColor.a);
            var outAlpha = dstColor.a + temp;
            temp /= outAlpha;
            return new Color
            (
                dstColor.r * dstColor.a + srcColor.r * temp,
                dstColor.g * dstColor.a + srcColor.g * temp,
                dstColor.b * dstColor.a + srcColor.b * temp,
                outAlpha
            );
        }

        #endregion Non Public Methods
    }
}