﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace CEUtilities.Helpers
{
    public static class UIMathHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

#pragma warning disable 0414
        static int mSizeFrame = -1;
#pragma warning restore 0414
        static MethodInfo s_GetSizeOfMainGameView;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Size of the game view cannot be retrieved from Screen.width and Screen.height when the game view is hidden.
        /// </summary>
        public static Vector2 ScreenSize
        {
#if UNITY_EDITOR
            get
            {
                Vector2 mGameSize = Vector2.one;
                int frame = Time.frameCount;

                if (mSizeFrame != frame || !Application.isPlaying)
                {
                    mSizeFrame = frame;

                    if (s_GetSizeOfMainGameView == null)
                    {
                        Type type = Type.GetType("UnityEditor.GameView,UnityEditor");
                        s_GetSizeOfMainGameView = type.GetMethod("GetSizeOfMainGameView", BindingFlags.NonPublic | BindingFlags.Static);
                    }
                    mGameSize = (Vector2)s_GetSizeOfMainGameView.Invoke(null, null);
                }
                return mGameSize;
            }
#else
		    get { return new Vector2(Screen.width, Screen.height); }
#endif
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Convert from top-left based pixel coordinates to bottom-left based UV coordinates.
        /// </summary>
        public static Rect ConvertToTexCoords(Rect rect, int width, int height)
        {
            Rect final = rect;

            if (width != 0f && height != 0f)
            {
                final.xMin = rect.xMin / width;
                final.xMax = rect.xMax / width;
                final.yMin = 1f - rect.yMax / height;
                final.yMax = 1f - rect.yMin / height;
            }
            return final;
        }

        /// <summary>
        /// Convert from bottom-left based UV coordinates to top-left based pixel coordinates.
        /// </summary>
        public static Rect ConvertToPixels(Rect rect, int width, int height, bool round)
        {
            Rect final = rect;

            if (round)
            {
                final.xMin = Mathf.RoundToInt(rect.xMin * width);
                final.xMax = Mathf.RoundToInt(rect.xMax * width);
                final.yMin = Mathf.RoundToInt((1f - rect.yMax) * height);
                final.yMax = Mathf.RoundToInt((1f - rect.yMin) * height);
            }
            else
            {
                final.xMin = rect.xMin * width;
                final.xMax = rect.xMax * width;
                final.yMin = (1f - rect.yMax) * height;
                final.yMax = (1f - rect.yMin) * height;
            }
            return final;
        }

        /// <summary>
        /// Round the pixel rectangle's dimensions.
        /// </summary>
        public static Rect MakePixelPerfect(Rect rect)
        {
            rect.xMin = Mathf.RoundToInt(rect.xMin);
            rect.yMin = Mathf.RoundToInt(rect.yMin);
            rect.xMax = Mathf.RoundToInt(rect.xMax);
            rect.yMax = Mathf.RoundToInt(rect.yMax);
            return rect;
        }

        /// <summary>
        /// Round the texture coordinate rectangle's dimensions.
        /// </summary>
        public static Rect MakePixelPerfect(Rect rect, int width, int height)
        {
            rect = ConvertToPixels(rect, width, height, true);
            rect.xMin = Mathf.RoundToInt(rect.xMin);
            rect.yMin = Mathf.RoundToInt(rect.yMin);
            rect.xMax = Mathf.RoundToInt(rect.xMax);
            rect.yMax = Mathf.RoundToInt(rect.yMax);
            return ConvertToTexCoords(rect, width, height);
        }

        /// <summary>
        /// Constrain 'rect' to be within 'area' as much as possible, returning the Vector2 offset necessary for this to happen.
        /// This function is useful when trying to restrict one area (window) to always be within another (viewport).
        /// </summary>
        public static Vector2 ConstrainRect(Vector2 minRect, Vector2 maxRect, Vector2 minArea, Vector2 maxArea)
        {
            Vector2 offset = Vector2.zero;

            float contentX = maxRect.x - minRect.x;
            float contentY = maxRect.y - minRect.y;

            float areaX = maxArea.x - minArea.x;
            float areaY = maxArea.y - minArea.y;

            if (contentX > areaX)
            {
                float diff = contentX - areaX;
                minArea.x -= diff;
                maxArea.x += diff;
            }

            if (contentY > areaY)
            {
                float diff = contentY - areaY;
                minArea.y -= diff;
                maxArea.y += diff;
            }

            if (minRect.x < minArea.x) offset.x += minArea.x - minRect.x;
            if (maxRect.x > maxArea.x) offset.x -= maxRect.x - maxArea.x;
            if (minRect.y < minArea.y) offset.y += minArea.y - minRect.y;
            if (maxRect.y > maxArea.y) offset.y -= maxRect.y - maxArea.y;

            return offset;
        }
        
        /// <summary>
        /// Determine the distance from the specified point to the line segment.
        /// </summary>
        public static float DistancePointToLineSegment(Vector2 point, Vector2 a, Vector2 b)
        {
            float l2 = (b - a).sqrMagnitude;
            if (l2 == 0f) return (point - a).magnitude;
            float t = Vector2.Dot(point - a, b - a) / l2;
            if (t < 0f) return (point - a).magnitude;
            else if (t > 1f) return (point - b).magnitude;
            Vector2 projection = a + t * (b - a);
            return (point - projection).magnitude;
        }

        /// <summary>
        /// Determine the distance from the mouse position to the screen space rectangle specified by the 4 points.
        /// </summary>
        public static float DistanceToRectangle(Vector2[] screenPoints, Vector2 mousePos)
        {
            bool oddNodes = false;
            int j = 4;

            for (int i = 0; i < 5; i++)
            {
                Vector3 v0 = screenPoints[MathHelper.RepeatIndex(i, 4)];
                Vector3 v1 = screenPoints[MathHelper.RepeatIndex(j, 4)];

                if ((v0.y > mousePos.y) != (v1.y > mousePos.y))
                {
                    if (mousePos.x < (v1.x - v0.x) * (mousePos.y - v0.y) / (v1.y - v0.y) + v0.x)
                    {
                        oddNodes = !oddNodes;
                    }
                }
                j = i;
            }

            if (!oddNodes)
            {
                float dist, closestDist = -1f;

                for (int i = 0; i < 4; i++)
                {
                    Vector3 v0 = screenPoints[i];
                    Vector3 v1 = screenPoints[MathHelper.RepeatIndex(i + 1, 4)];

                    dist = DistancePointToLineSegment(mousePos, v0, v1);

                    if (dist < closestDist || closestDist < 0f) closestDist = dist;
                }
                return closestDist;
            }
            else return 0f;
        }

        /// <summary>
        /// Determine the distance from the mouse position to the world rectangle specified by the 4 points.
        /// </summary>
        public static float DistanceToRectangle(Vector3[] worldPoints, Vector2 mousePos, Camera cam)
        {
            Vector2[] screenPoints = new Vector2[4];
            for (int i = 0; i < 4; ++i)
                screenPoints[i] = cam.WorldToScreenPoint(worldPoints[i]);
            return DistanceToRectangle(screenPoints, mousePos);
        }

        /// <summary>
        /// Adjust the specified value by DPI: height * 96 / DPI.
        /// This will result in in a smaller value returned for higher pixel density devices.
        /// </summary>
        public static int AdjustByDPI(float height)
        {
            float dpi = Screen.dpi;

            RuntimePlatform platform = Application.platform;

            if (dpi == 0f)
            {
                dpi = (platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer) ? 160f : 96f;
#if UNITY_BLACKBERRY
			if (platform == RuntimePlatform.BB10Player) dpi = 160f;
#elif UNITY_WP8 || UNITY_WP_8_1
			if (platform == RuntimePlatform.WP8Player) dpi = 160f;
#endif
            }

            int h = Mathf.RoundToInt(height * (96f / dpi));
            if ((h & 1) == 1) ++h;
            return h;
        }

        /// <summary>
        /// Convert the specified position, making it relative to the specified object.
        /// </summary>
        public static Vector2 ScreenToPixels(Vector2 pos, Transform relativeTo, Camera cam)
        {
            int layer = relativeTo.gameObject.layer;

            if (cam == null)
            {
                Debug.LogWarning("No camera found for layer " + layer);
                return pos;
            }

            Vector3 wp = cam.ScreenToWorldPoint(pos);
            return relativeTo.InverseTransformPoint(wp);
        }

        /// <summary>
        /// Convert the specified position, making it relative to the specified object's parent.
        /// Useful if you plan on positioning the widget using the specified value (think mouse cursor).
        /// </summary>
        public static Vector2 ScreenToParentPixels(Vector2 pos, Transform relativeTo, Camera cam)
        {
            int layer = relativeTo.gameObject.layer;
            if (relativeTo.parent != null)
                relativeTo = relativeTo.parent;

            if (cam == null)
            {
                Debug.LogWarning("No camera found for layer " + layer);
                return pos;
            }

            Vector3 wp = cam.ScreenToWorldPoint(pos);
            return (relativeTo != null) ? relativeTo.InverseTransformPoint(wp) : wp;
        }
        
        /// <summary>
        /// Convert the specified world point from one camera's world space to another, then make it relative to the specified transform.
        /// You should use this function if you want to position UI using some 3D point in space.
        /// </summary>
        public static Vector3 WorldToLocalPoint(Vector3 worldPos, Camera worldCam, Camera uiCam, Transform relativeTo)
        {
            worldPos = worldCam.WorldToViewportPoint(worldPos);
            worldPos = uiCam.ViewportToWorldPoint(worldPos);
            if (relativeTo == null) return worldPos;
            relativeTo = relativeTo.transform.parent;
            if (relativeTo == null) return worldPos;
            return relativeTo.InverseTransformPoint(worldPos);
        }
        
        /// <summary>
        /// Helper function that can set the transform's position to be at the specified world position.
        /// Ideal usage: positioning a UI element to be directly over a 3D point in space.
        /// </summary>	
        /// <param name="worldPos">World position, visible by the worldCam</param>
        /// <param name="worldCam">Camera that is able to see the worldPos</param>
        /// <param name="myCam">Camera that is able to see the transform this function is called on</param>
        public static void OverlayPosition(this Transform trans, Vector3 worldPos, Camera worldCam, Camera myCam)
        {
            worldPos = worldCam.WorldToViewportPoint(worldPos);
            worldPos = myCam.ViewportToWorldPoint(worldPos);
            Transform parent = trans.parent;
            trans.localPosition = (parent != null) ? parent.InverseTransformPoint(worldPos) : worldPos;
        }

        /// <summary>
        /// Convert a rect transform's canvas position to screen position
        /// </summary>
        public static Vector2 CanvasToScreen(Vector2 canvasPosition, RectTransform target)
        {
            var scaler = target.GetComponentInParent<CanvasScaler>();

            var guiScaleWidth = scaler.referenceResolution.x / (float)Screen.width;
            var guiScaleHeight = scaler.referenceResolution.y / (float)Screen.height;
            var guiScale = guiScaleWidth * (1 - scaler.matchWidthOrHeight) + guiScaleHeight * scaler.matchWidthOrHeight;

            return new Vector2(
                (canvasPosition.x / guiScale) + (Screen.width * target.anchorMin.x),
                (canvasPosition.y / guiScale) + (Screen.height * target.anchorMin.y));
        }

        /// <summary>
        /// Convert a rect transform's canvas position to screen position
        /// </summary>
        public static Vector2 CanvasToScreen(RectTransform target)
        {
            return CanvasToScreen(target.anchoredPosition, target);
        }

        /// <summary>
        /// Convert an screen position to a rect transform's canvas relative position
        /// </summary>
        public static Vector2 ScreenToCanvasPoint(Vector2 screenPosition, RectTransform target)
        {
            var scaler = target.GetComponentInParent<CanvasScaler>();

            var guiScaleWidth = scaler.referenceResolution.x / (float)Screen.width;
            var guiScaleHeight = scaler.referenceResolution.y / (float)Screen.height;
            var guiScale = guiScaleWidth * (1 - scaler.matchWidthOrHeight) + guiScaleHeight * scaler.matchWidthOrHeight;

            return new Vector2(
                (screenPosition.x - (Screen.width * target.anchorMin.x)) * guiScale,
                (screenPosition.y - (Screen.height * target.anchorMin.y)) * guiScale);
        }

        /// <summary>
        /// Convert an screen position to a canvas relative position
        /// </summary>
        public static Vector2 ScreenToCanvasPoint(Vector2 screenPosition, Canvas canvas)
        {
            return ScreenToCanvasPoint(screenPosition, canvas.GetComponent<RectTransform>());
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion
    }
}