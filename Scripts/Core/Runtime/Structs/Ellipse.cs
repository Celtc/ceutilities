﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace CEUtilities.Structs
{
    [System.Serializable]
    public class Ellipse : IGeometryArea
    {
        #region Exposed fields

        [SerializeField]
        private Vector2 center;

        [SerializeField]
        private float width;

        [SerializeField]
        private float height;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Vector2 Center
        {
            get
            {
                return center;
            }

            set
            {
                center = value;
            }
        }

        public Vector2 Position
        {
            get
            {
                return center;
            }

            set
            {
                center = value;
            }
        }

        public float Width
        {
            get
            {
                return width;
            }

            set
            {
                width = value;
            }
        }

        public float Height
        {
            get
            {
                return height;
            }

            set
            {
                height = value;
            }
        }

        public float XRadius
        {
            get
            {
                return width * .5f;
            }
        }

        public float YRadius
        {
            get
            {
                return height * .5f;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        public Ellipse()
        {
            //
        }

        public Ellipse(Ellipse source)
        {
            center = source.center;
            width = source.width;
            height = source.height;
        }

        public Ellipse(Vector2 center, float width, float height)
        {
            this.center = center;
            this.width = width;
            this.height = height;
        }

        public Ellipse(float x, float y, float width, float height)
        {
            center = new Vector2(x, y);
            this.width = width;
            this.height = height;
        }

        public bool Contains(Vector2 point)
        {
            var normalized = point - center;
            return 
                (((double)(normalized.x * normalized.x) / (XRadius * XRadius)) + 
                ((double)(normalized.y * normalized.y) / (YRadius * YRadius))) <= 1.0;
        }

        public bool Overlaps(Rect other)
        {
            throw new System.NotImplementedException();
        }

        public void Set(Vector2 center, float width, float height)
        {
            this.center = center;
            this.width = width;
            this.height = height;
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}