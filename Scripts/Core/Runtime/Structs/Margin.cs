﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace CEUtilities.Structs
{
    [System.Serializable]
    public struct Margin
    {
        #region Static

        public static Margin zero { get { return new Margin(0, 0, 0, 0); } }

        #endregion

        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

		[HorizontalGroup("Vertical")]
		[LabelWidth(50f)]
		public float top;
		[HorizontalGroup("Vertical")]
		[LabelWidth(50f)]
		public float bottom;
		[HorizontalGroup("Horizontal")]
		[LabelWidth(50f)]
		public float left;
		[HorizontalGroup("Horizontal")]
		[LabelWidth(50f)]
		public float right;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties
		
        /// <summary>
		/// Shortcut for left + right. (Read Only)
		/// </summary>
        public float horizontal { get { return left + right; } }
        
		/// <summary>
		/// Shortcut for top + bottom. (Read Only)
		/// </summary>
        public float vertical { get { return top + bottom; } }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Constructor
        /// </summary>
        public Margin(float left, float right, float top, float bottom)
        {
            this.left = left;
            this.right = right;
            this.top = top;
            this.bottom = bottom;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Margin(RectOffset rect)
        {
            left = rect.left;
            right = rect.right;
            top = rect.top;
            bottom = rect.bottom;
        }

        /// <summary>
        /// String override
        /// </summary>
        public override string ToString()
        {
            return string.Format("Margins (l:{0} r:{1} t:{2} b:{3})",
                left,
                right,
                top,
                bottom);
        }

        /// <summary>
        /// Add the border offsets to a rect.
        /// </summary>
        public Margin Add(Margin other)
        {
            return new Margin(left + other.left, right + other.right, top + other.top, bottom + other.bottom);
        }

        /// <summary>
        /// Remove the border offsets from a rect.
        /// </summary>
        public Margin Remove(Margin other)
        {
            return new Margin(left - other.left, right - other.right, top - other.top, bottom - other.bottom);
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}