﻿namespace CEUtilities.Structs
{
    public enum Pivot
    {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        Center
    }
}