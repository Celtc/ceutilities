﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Structs
{
    public interface IGeometryArea
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        Vector2 Center { get; set; }

        Vector2 Position { get; set; }

        float Width { get; set; }

        float Height { get; set; }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        bool Contains(Vector2 point);

        bool Overlaps(Rect other);

        void Set(Vector2 center, float width, float height);

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}