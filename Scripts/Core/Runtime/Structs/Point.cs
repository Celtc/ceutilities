﻿using UnityEngine;

using CEUtilities.Helpers;

namespace CEUtilities.Structs
{
    [System.Serializable]
    public struct Point
    {
        #region Static

        /// <summary>
        /// Shorthand for writing Point(0, 0).
        /// </summary>
        public static Point zero { get { return new Point(0, 0); } }

        /// <summary>
        /// Shorthand for writing Point(1, 1).
        /// </summary>
        public static Point one { get { return new Point(1, 1); } }

        /// <summary>
        /// Shorthand for writing Point(0, -1).
        /// </summary>
        public static Point down { get { return new Point(0, -1); } }

        /// <summary>
        /// Shorthand for writing Point(-1, 0).
        /// </summary>
        public static Point left { get { return new Point(-1, 0); } }

        /// <summary>
        /// Shorthand for writing Point(1, 0).
        /// </summary>
        public static Point right { get { return new Point(1, 0); } }

        /// <summary>
        /// Shorthand for writing Point(0, 1).
        /// </summary>
        public static Point up { get { return new Point(0, 1); } }

        /// <summary>
        /// Clamp the point between two other points
        /// </summary>
        public static Point Clamp(Point clamping, Point min, Point max)
        {
            return PointHelper.Clamp(clamping, min, max);            
        }

        /// <summary>
        /// Dot operator
        /// </summary>
        public static int Dot(Point a, Point b)
        {
            return PointHelper.Dot(a, b);
        }

        #endregion

        #region Exposed fields

        public int x;
        public int y;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Constructor
        /// </summary>
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        /// <summary>
        /// Return the magnitude of a Point
        /// </summary>
        public float Magnitude()
        {
            return Mathf.Sqrt(this.Dot(this));
        }

        /// <summary>
        /// Return the magnitude of a Point without the Sqr operation
        /// </summary>
        public int SqrMagnitude()
        {
            return this.Dot(this);
        }

        #region Operators Overload

        public static Point operator +(Point left, Point right)
        {
            return new Point(right.x + left.x, right.y + left.y);
        }

        public static Point operator -(Point left, Point right)
        {
            return new Point(right.x - left.x, right.y - left.y);
        }

        public static Point operator *(Point point, int magnitude)
        {
            return new Point(point.x * magnitude, point.y * magnitude);
        }

        public override bool Equals(System.Object other)
        {
            return other is Point && Equals((Point)other);
        }

        public bool Equals(Point other)
        {
            return (x == other.x) && (y == other.y);
        }

        public static bool operator ==(Point lhs, Point rhs)
        {
            return lhs.Equals(rhs);
        }

        public static bool operator !=(Point lhs, Point rhs)
        {
            return !lhs.Equals(rhs);
        }

        public override int GetHashCode()
        {
            return x ^ y;
        }

        #endregion

        #endregion Methods
    }
}
