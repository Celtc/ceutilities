﻿
namespace CEUtilities.Structs
{
    public enum Edge
    {
        Top = 1,
        Right = 2,
        Bottom = 3,
        Left = 4
    }
}