﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace CEUtilities.Structs
{
    [System.Serializable]
    public class Rectangle : IGeometryArea
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        private Rect _rect;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Vector2 Center
        {
            get
            {
                return _rect.center;
            }

            set
            {
                _rect.center = value;
            }
        }

        public Vector2 Position
        {
            get
            {
                return _rect.position;
            }

            set
            {
                _rect.position = value;
            }
        }

        public float Width
        {
            get
            {
                return _rect.width;
            }

            set
            {
                _rect.width = value;
            }
        }

        public float Height
        {
            get
            {
                return _rect.height;
            }

            set
            {
                _rect.height = value;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        public Rectangle()
        {
            _rect = new Rect();
        }

        public Rectangle(Rectangle source)
        {
            _rect = new Rect(source._rect);
        }

        public Rectangle(Vector2 position, Vector2 size)
        {
            _rect = new Rect(position, size);
        }

        public Rectangle(float x, float y, float width, float height)
        {
            _rect = new Rect(x, y, width, height);
        }

        public bool Contains(Vector2 point)
        {
            return _rect.Contains(point);
        }

        public bool Overlaps(Rect other)
        {
            return _rect.Overlaps(other);
        }

        public void Set(Vector2 center, float width, float height)
        {
            _rect.width = width;
            _rect.height = height;
            _rect.center = center;
        }

        public void Set(float minX, float minY, float width, float height)
        {
            _rect.x = minX;
            _rect.y = minY;
            _rect.width = width;
            _rect.height = height;
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}