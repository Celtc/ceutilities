﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using static CEUtilities.Coroutines.EditorCoroutines;

namespace CEUtilities.Coroutines
{
    public class EditorCoroutine
    {
#if UNITY_EDITOR      
        #region Exposed fields

        public ICoroutineYield currentYield = new YieldDefault();
        public IEnumerator routine;
        public string routineUniqueHash;
        public string ownerUniqueHash;
        public string MethodName = "";

        public int ownerHash;
        public string ownerType;

        public bool finished = false;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        public EditorCoroutine(IEnumerator routine, int ownerHash, string ownerType)
        {
            this.routine = routine;
            this.ownerHash = ownerHash;
            this.ownerType = ownerType;
            ownerUniqueHash = ownerHash + "_" + ownerType;

            if (routine != null)
            {
                string[] split = routine.ToString().Split('<', '>');
                if (split.Length == 3)
                {
                    this.MethodName = split[1];
                }
            }

            routineUniqueHash = ownerHash + "_" + ownerType + "_" + MethodName;
        }

        public EditorCoroutine(string methodName, int ownerHash, string ownerType)
        {
            MethodName = methodName;
            this.ownerHash = ownerHash;
            this.ownerType = ownerType;
            ownerUniqueHash = ownerHash + "_" + ownerType;
            routineUniqueHash = ownerHash + "_" + ownerType + "_" + MethodName;
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
#endif
    }
}