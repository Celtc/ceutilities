﻿using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CEUtilities.Coroutines
{
	public static class EditorCoroutineExtensions
	{
#if UNITY_EDITOR
		public static EditorCoroutine StartCoroutine(this EditorWindow thisRef, IEnumerator coroutine)
		{
#if UNITY_EDITOR
			return EditorCoroutines.StartCoroutine(coroutine, thisRef);
#else
			return null;
#endif
		}

		public static EditorCoroutine StartCoroutine(this EditorWindow thisRef, string methodName)
		{
#if UNITY_EDITOR
			return EditorCoroutines.StartCoroutine(methodName, thisRef);
#else
			return null;
#endif
		}

		public static EditorCoroutine StartCoroutine(this EditorWindow thisRef, string methodName, object value)
		{
#if UNITY_EDITOR
			return EditorCoroutines.StartCoroutine(methodName, value, thisRef);
#else
			return null;
#endif
		}

		public static void StopCoroutine(this EditorWindow thisRef, IEnumerator coroutine)
		{
#if UNITY_EDITOR
			EditorCoroutines.StopCoroutine(coroutine, thisRef);
#else
			return null;
#endif
		}

		public static void StopCoroutine(this EditorWindow thisRef, string methodName)
		{
#if UNITY_EDITOR
			EditorCoroutines.StopCoroutine(methodName, thisRef);
#else
			return null;
#endif
		}

		public static void StopAllCoroutines(this EditorWindow thisRef)
		{
#if UNITY_EDITOR
			EditorCoroutines.StopAllCoroutines(thisRef);
#else
			return null;
#endif
		}
#endif
	}
}