﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Patterns;

namespace CEUtilities.Coroutines
{
    [CustomSingleton(AutoGeneration = true, Persistent = true, Flags = HideFlags.HideInHierarchy)]
    public class StaticCoroutineManager : Singleton<StaticCoroutineManager>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}