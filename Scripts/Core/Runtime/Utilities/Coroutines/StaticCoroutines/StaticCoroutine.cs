﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Helpers;
using CEUtilities.Patterns;

namespace CEUtilities.Coroutines
{
    [CustomSingleton(AllowAsset = false, AutoGeneration = true, GenerateOnlyInPlaymode = true, Flags = HideFlags.HideAndDontSave)]
    public class StaticCoroutine : Singleton<StaticCoroutine>
    {
        #region Static Accessors

        /// <summary>
        /// Starts the specified routine.
        /// </summary>
        /// <param name="routine">The routine.</param>
        /// <returns></returns>
        public static Coroutine Start(IEnumerator routine)
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                return Instance.StartCoroutine(routine);
            }
            else
            {
                Debug.LogError("[StaticCoroutine] Static coroutine are not supported in editor.");

                return null;
            }
        }

        /// <summary>
        /// Starts the specified routine.
        /// </summary>
        /// <param name="routine">The routine.</param>
        /// <returns></returns>
        public static void Stop(Coroutine coroutine)
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                Instance.StopCoroutine(coroutine);
            }
            else
            {
                Debug.LogError("[StaticCoroutine] Static coroutine are not supported in editor.");

                return;
            }
        }

        #endregion Static Accessors
    }
}