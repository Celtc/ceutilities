﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

using Sirenix.OdinInspector;

namespace CEUtilities.Materials
{
	[System.Serializable]
	public class MaterialChangeSetup
	{
		[System.Flags]
		public enum Index
		{
			I0 = 0,
			I1 = 1 << 0
		}

		[SerializeField]
		public Renderer target;

		[SerializeField]
		//[EnumBitmask(typeof(Index))]
		public int materialsIndex;

		[SerializeField]
		public Material newMaterial;

		public Material oldMaterial;
	}

	public class MaterialChange : SerializedMonoBehaviour
	{
		#region Exposed fields

		[SerializeField]
		private MaterialChangeSetup[] changes;

		#endregion Exposed fields

		#region Internal fields

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Methods

		/// <summary>
		/// Change to new material
		/// </summary>
		public void ChangeToNewMaterial()
		{
			foreach (var change in changes)
			{
				change.oldMaterial = change.target.material;
				change.target.material = change.newMaterial;
			}
		}

		/// <summary>
		/// Change to old material
		/// </summary>
		public void RestoreMaterial()
		{
			foreach (var change in changes)
			{
				if (change.oldMaterial != null)
					change.target.material = change.newMaterial;
			}
		}

		#endregion Methods
	}
}