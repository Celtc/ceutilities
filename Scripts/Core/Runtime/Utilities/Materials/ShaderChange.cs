﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace CEUtilities.Materials
{
    public class ShaderChange : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private Shader shaderA;
        [SerializeField]
        private Shader shaderB;

        [SerializeField]
        private GameObject[] renderersGO = null;
        [SerializeField]
        private bool recursive = false;
        [SerializeField]
        private bool swapBumps = true;

        #endregion Exposed fields

        #region Internal fields

        private Material[] cachedMats;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Material[] CachedMats
        {
            get
            {
                if (cachedMats == null)
                    CacheMaterials();

                return cachedMats;
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            if (cachedMats == null)
                CacheMaterials();
        }

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Change to A Shader
        /// </summary>
        public void ChangeToA()
        {
            foreach (var mat in CachedMats)
            {
                Texture norm = null;
                if (swapBumps)
                    norm = mat.GetTexture("_BumpMap");
                mat.shader = shaderA == null ? Shader.Find("Standard") : shaderA;
                if (swapBumps && norm != null)
                    mat.SetTexture("_V_WIRE_NormalMap", norm);
            }
        }

        /// <summary>
        /// Change to B Shader
        /// </summary>
        public void ChangeToB()
        {
            foreach (var mat in CachedMats)
            {
                Texture norm = null;
                if (swapBumps)
                    norm = mat.GetTexture("_V_WIRE_NormalMap");
                mat.shader = shaderB == null ? Shader.Find("Standard") : shaderB;
                if (swapBumps && norm != null)
                    mat.SetTexture("_BumpMap", norm);
            }
        }

        /// <summary>
        /// Get all materials to anim, filters the ones which dows not have the prop
        /// </summary>
        private void CacheMaterials()
        {
            var result = new List<Material>();

            foreach (var rendererGO in renderersGO)
                if (recursive)
                    foreach (var subrenderer in rendererGO.GetComponentsInChildren<Renderer>())
                        result.AddRange(subrenderer.materials);
                else
                    result.AddRange(rendererGO.GetComponent<Renderer>().materials);

            cachedMats = result.ToArray();
        }

        #endregion Methods
    }
}