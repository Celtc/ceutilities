﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace CEUtilities.Materials
{
    public class ShaderAlphaAnim : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private bool startOnEnable = false;

        [SerializeField]
        private AnimationCurve animCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

        [SerializeField]
        private float delay = 0f;
        [SerializeField]
        private float duration = 1f;

        [SerializeField]
        private string varName = "_Color";
        [SerializeField]
        private bool recursive = false;

        [SerializeField]
        private GameObject[] renderersGO = null;

        #endregion Exposed fields

        #region Internal fields

        private Material[] cachedMats;

        #endregion Internal fields

        #region Custom Events

        public UnityEvent OnStarted = new UnityEvent();
        public UnityEvent OnFinished = new UnityEvent();

        #endregion Custom Events

        #region Properties

        public Material[] CachedMats
        {
            get
            {
                if (cachedMats == null)
                    CacheMaterials();

                return cachedMats;
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            if (cachedMats == null)
            {
                CacheMaterials();
            }

            Rewind();

            if (startOnEnable)
            {
                Animate();
            }
        }

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Make the object appear
        /// </summary>
        public void Animate()
        {
            Animate(0f);
        }

        /// <summary>
        /// Make the object appear
        /// </summary>
        public void Animate(float delay)
        {
            StopAllCoroutines();
            StartCoroutine(_Animate());
        }

        /// <summary>
        /// Rewind the object to its initial state
        /// </summary>
        public void Rewind()
        {
            StopAllCoroutines();

            foreach (var material in CachedMats)
            {
                var color = material.GetColor(varName);
                color.a = animCurve.Evaluate(0f);
                material.SetColor(varName, color);
            }
        }


        /// <summary>
        /// Get all materials to anim, filters the ones which dows not have the prop
        /// </summary>
        private void CacheMaterials()
        {
            var result = new List<Material>();

            foreach (var rendererGO in renderersGO)
                if (recursive)
                    foreach (var subrenderer in rendererGO.GetComponentsInChildren<Renderer>())
                        result.AddRange(subrenderer.materials);
                else
                    result.AddRange(rendererGO.GetComponent<Renderer>().materials);

            result.RemoveAll(x => !x.HasProperty(varName));

            cachedMats = result.ToArray();
        }


        private IEnumerator _Animate()
        {
            yield return new WaitForSeconds(delay);

            OnStarted.Invoke();

            foreach (var material in CachedMats)
                StartCoroutine(_ShaderAnim(material, duration));

            yield return new WaitForSeconds(duration);

            OnFinished.Invoke();
        }

        private IEnumerator _ShaderAnim(Material material, float duration)
        {
            var color = material.GetColor(varName);

            var timer = 0f;
            while (timer <= duration)
            {
                color.a = animCurve.Evaluate(timer / duration);
                material.SetColor(varName, color);

                yield return null;
                timer += Time.deltaTime;
            }

            color.a = animCurve.Evaluate(1f);
            material.SetColor(varName, color);
        }

        #endregion Methods
    }
}