﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace CEUtilities.Materials
{
    public class ShaderColorAnim : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private AnimationCurve animCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

        [SerializeField]
        private Color startColor = Color.white;
        [SerializeField]
        private Color endColor = Color.white;

        [SerializeField]
        private float delay = 0f;
        [SerializeField]
        private float duration = 1f;

        [SerializeField]
        private string varName = "_Color";
        [SerializeField]
        private bool recursive = false;

        [SerializeField]
        private GameObject[] renderersGO = null;

        #endregion Exposed fields

        #region Internal fields

        private Material[] cachedMats;

        #endregion Internal fields

        #region Custom Events

        public UnityEvent OnStarted = new UnityEvent();
        public UnityEvent OnFinished = new UnityEvent();

        #endregion Custom Events

        #region Properties

        public Material[] CachedMats
        {
            get
            {
                if (cachedMats == null)
                    CacheMaterials();

                return cachedMats;
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            if (cachedMats == null)
                CacheMaterials();

            Rewind();
        }

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Make the object appear
        /// </summary>
        public void Animate()
        {
            Animate(0f);
        }

        /// <summary>
        /// Make the object appear
        /// </summary>
        public void Animate(float delay)
        {
            StopAllCoroutines();
            StartCoroutine(_Animate());
        }

        /// <summary>
        /// Rewind the object to its initial state
        /// </summary>
        public void Rewind()
        {
            StopAllCoroutines();

            foreach (var material in CachedMats)
            {
                var color = material.GetColor(varName);
                var newColor = startColor;
                newColor.a = color.a;
                material.SetColor(varName, newColor);
            }
        }


        /// <summary>
        /// Get all materials to anim, filters the ones which dows not have the prop
        /// </summary>
        private void CacheMaterials()
        {
            var result = new List<Material>();

            foreach (var rendererGO in renderersGO)
                if (recursive)
                    foreach (var subrenderer in rendererGO.GetComponentsInChildren<Renderer>())
                        result.AddRange(subrenderer.materials);
                else
                    result.AddRange(rendererGO.GetComponent<Renderer>().materials);

            result.RemoveAll(x => !x.HasProperty(varName));

            cachedMats = result.ToArray();
        }


        private IEnumerator _Animate()
        {
            yield return new WaitForSeconds(delay);

            OnStarted.Invoke();

            foreach (var material in CachedMats)
                StartCoroutine(_ShaderAnim(material, duration));

            yield return new WaitForSeconds(duration);

            OnFinished.Invoke();
        }

        private IEnumerator _ShaderAnim(Material material, float duration)
        {
            Color color;

            var timer = 0f;
            while (timer <= duration)
            {
                color = material.GetColor(varName);
                color.r = Mathf.Lerp(startColor.r, endColor.r, animCurve.Evaluate(timer / duration));
                color.g = Mathf.Lerp(startColor.g, endColor.g, animCurve.Evaluate(timer / duration));
                color.b = Mathf.Lerp(startColor.b, endColor.b, animCurve.Evaluate(timer / duration));
                material.SetColor(varName, color);

                yield return null;
                timer += Time.deltaTime;
            }

            color = material.GetColor(varName);
            color.r = endColor.r;
            color.g = endColor.g;
            color.b = endColor.b;
            material.SetColor(varName, color);
        }

        #endregion Methods
    }
}