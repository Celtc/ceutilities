﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace CEUtilities
{
    public class Counter : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [Range(0, 999)]
        private int maxNumber = 10;

        [SerializeField]
        [Range(0, 999)]
        private int minNumber = 0;

        [SerializeField]
        private int startValue = 0;

        #endregion Exposed fields

        #region Internal fields

        private int count;

        private HashSet<Object> signed = new HashSet<Object>();

        #endregion Internal fields

        #region Custom Events

        public UnityEvent OnMax = new UnityEvent();

        public UnityEvent_Int OnChanged = new UnityEvent_Int();

        public UnityEvent OnMin = new UnityEvent();

        #endregion Custom Events

        #region Properties

        public int Count
        {
            get
            {
                return count;
            }

            private set
            {
                count = value;
            }
        }

        public int StartValue
        {
            get
            {
                return startValue;
            }

            set
            {
                startValue = value;
            }
        }

        public int MaxNumber
        {
            get
            {
                return maxNumber;
            }

            set
            {
                maxNumber = value;
            }
        }

        public int MinNumber
        {
            get
            {
                return minNumber;
            }

            set
            {
                minNumber = value;
            }
        }

        #endregion Properties

        #region Events methods

        private void Awake()
        {
            count = StartValue;
        }

        #endregion Events methods

        #region Public Methods

        public void SignUp()
        {
            if (++count >= maxNumber)
            {
                count = maxNumber;
                OnMax.Invoke();
            }
            OnChanged.Invoke(count);
        }

        public void SignUpUnique(Object caller)
        {
            if (signed.Contains(caller))
                return;

            SignUp();

            signed.Add(caller);
        }

        public void SignDown()
        {
            if (--count <= minNumber)
            {
                count = minNumber;
                OnMin.Invoke();
            }
            OnChanged.Invoke(count);
        }

        public void SignDownUnique(Object caller)
        {
            if (signed.Contains(caller))
                return;

            SignDown();

            signed.Add(caller);
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}