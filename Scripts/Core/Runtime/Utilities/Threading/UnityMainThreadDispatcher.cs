﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

using CEUtilities.Patterns;
using CEUtilities.Helpers;

namespace CEUtilities.Threading
{
    /// <summary>
    /// A thread-safe class which holds a queue with actions to execute on the next Update() method. It can be used to make calls to the main thread for things such as UI Manipulation in Unity
    /// </summary>
    [CustomSingleton(AutoGeneration = true, Persistent = true)]
    [DefaultExecutionOrder(-2000)]
    public class UnityMainThreadDispatcher : Singleton<UnityMainThreadDispatcher>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private static readonly ConcurrentQueue<Action> executionQueue = new ConcurrentQueue<Action>();

#if UNITY_EDITOR

        private bool registered;

#endif

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

#if UNITY_EDITOR

        public void EditorUpdate()
        {
            if (!ApplicationHelper.AppInPlayerOrPlaymode)
            {
                Update();
            }
        }

#endif

        public void Update()
        {
            Action action;
            while (executionQueue.Count > 0)
            {
                executionQueue.TryDequeue(out action);

                if (this != null && action != null)
                {
                    action.Invoke();
                }
            }
        }

        #endregion Events methods

        #region Public Methods

#if UNITY_EDITOR

        public UnityMainThreadDispatcher()
        {
            if (!registered)
            {
                registered = true;
                UnityEditor.EditorApplication.update += EditorUpdate;
            }
        }

#endif

        /// <summary>
        /// Locks the queue and adds the IEnumerator to the queue
        /// </summary>
        /// <param name="action">IEnumerator function that will be executed from the main thread.</param>
        public void Enqueue(IEnumerator action)
        {
            if (!_appIsQuitting && this != null)
            {
                executionQueue.Enqueue(() => StartCoroutine(action));
            }
        }

        /// <summary>
        /// Locks the queue and adds the Action to the queue
        /// </summary>
        /// <param name="action">Function that will be executed from the main thread.</param>
        public void Enqueue(Action action)
        {
            Enqueue(ActionWrapper(action));
        }

        #endregion Public Methods

        #region Non Public Methods

        IEnumerator ActionWrapper(Action a)
        {
            a();
            yield return null;
        }

        #endregion Non Public Methods
    }
}
