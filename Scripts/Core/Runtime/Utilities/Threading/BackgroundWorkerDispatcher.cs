﻿using UnityEngine;
using System;
using System.Threading;
using System.Collections;
using System.ComponentModel;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;
using CEUtilities.Patterns;
using CEUtilities.Coroutines;

namespace CEUtilities.Threading
{
    /// <summary>
    /// Dispatchs code into real threads.
    /// </summary>
    /// <seealso cref="Patterns.Singleton{BackgroundWorkerDispatcher}" />
    [CustomSingleton(AutoGeneration = true, Persistent = true)]
	public class BackgroundWorkerDispatcher : Singleton<BackgroundWorkerDispatcher>
	{
		#region Constants		

		private const int INIT_MAX_WORKERS = 100;

		#endregion

		#region Structs

		/// <summary>
		/// Structure used to return the output of a background worker
		/// </summary>
		public struct WorkerResult
		{
			/// <summary>
			/// Error thrown by the worker
			/// </summary>
			public Exception error;
		}

		/// <summary>
		/// Structure used to return the output of a background worker
		/// </summary>
		public struct WorkerResult<T>
		{
			/// <summary>
			/// Returned result by the worker
			/// </summary>
			public T result;

			/// <summary>
			/// Error thrown by the worker
			/// </summary>
			public Exception error;
		}

		#endregion

		#region Exposed fields

		[SerializeField]
		[Range(1, INIT_MAX_WORKERS)]
		[OnValueChanged("UpdateSemaphoreMax")]
		private int maxWorkers = 10;

		#endregion Exposed fields

		#region Internal fields

		private int debugCount;

		private int lastMaxWorkers;
		private Semaphore semaphore;

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		/// <summary>
		/// Max amount of simultaneous workers running
		/// </summary>
		public int MaxWorkers
		{
			get
			{
				return maxWorkers;
			}

			set
			{
				CheckForSemaphore();

				maxWorkers = value;

				UpdateSemaphoreMax();
			}
		}


		/// <summary>
		/// Semaphore
		/// </summary>
		private Semaphore Semaphore
		{
			get
			{
				CheckForSemaphore();

				return semaphore;
			}
		}

		#endregion Properties

		#region Events methods

		[OnInspectorGUI]
		void OnInspectorGUI()
		{
			CheckForSemaphore();
		}

		#endregion Events methods

		#region Public Methods

		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// </summary>
		public void Enqueue(Action action)
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
				StartCoroutine(_BackgroundTask(action, null));
			else
				EditorCoroutines.StartCoroutine(_BackgroundTask(action, null), this);
#else
			StartCoroutine(_BackgroundTask(action, null));
#endif
		}
		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// </summary>
		public void Enqueue(Action action, Action<WorkerResult> resultCallback)
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
				StartCoroutine(_BackgroundTask(action, resultCallback));
			else
				EditorCoroutines.StartCoroutine(_BackgroundTask(action, resultCallback), this);
#else
			StartCoroutine(_BackgroundTask(action, resultCallback));
#endif
		}
		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// </summary>
		public void Enqueue<To>(Func<To> action, Action<WorkerResult<To>> resultCallback)
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
				StartCoroutine(_BackgroundTask(action, resultCallback));
			else
				EditorCoroutines.StartCoroutine(_BackgroundTask(action, resultCallback), this);
#else
			StartCoroutine(_BackgroundTask(action, resultCallback));
#endif
		}

		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// The method is yieldable by Unity
		/// </summary>
		public IEnumerator _Enqueue(Action action)
		{
			yield return _BackgroundTask(action, null);
		}
		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// The method is yieldable by Unity
		/// </summary>
		public IEnumerator _Enqueue(Action action, Action<WorkerResult> resultCallback)
		{
			yield return _BackgroundTask(action, resultCallback);
		}
		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// The method is yieldable by Unity
		/// </summary>
		public IEnumerator _Enqueue<To>(Func<To> action, Action<WorkerResult<To>> resultCallback)
		{
			yield return _BackgroundTask(action, resultCallback);
		}

		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// </summary>
		public void Enqueue(IEnumerator action)
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
				StartCoroutine(_BackgroundTask(IEnumeratorWrapper(action), null));
			else
				EditorCoroutines.StartCoroutine(_BackgroundTask(IEnumeratorWrapper(action), null), this);
#else
			StartCoroutine(_BackgroundTask(IEnumeratorWrapper(action), null));
#endif
		}
		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// </summary>
		public void Enqueue(IEnumerator action, Action<WorkerResult> resultCallback)
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
				StartCoroutine(_BackgroundTask(IEnumeratorWrapper(action), resultCallback));
			else
				EditorCoroutines.StartCoroutine(_BackgroundTask(IEnumeratorWrapper(action), resultCallback), this);
#else
			StartCoroutine(_BackgroundTask(IEnumeratorWrapper(action), resultCallback));
#endif
		}

		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// The method is yieldable by Unity
		/// </summary>
		public IEnumerator _Enqueue(IEnumerator action)
		{
			yield return _BackgroundTask(IEnumeratorWrapper(action), null);
		}
		
		/// <summary>
		/// Enqueue an action to be run in a background worker
		/// The method is yieldable by Unity
		/// </summary>
		public IEnumerator _Enqueue(IEnumerator action, Action<WorkerResult> resultCallback)
		{
			yield return _BackgroundTask(IEnumeratorWrapper(action), resultCallback);
		}

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Verify if semaphore is init
		/// </summary>
		private void CheckForSemaphore()
		{
			if (semaphore == null)
			{
				debugCount = lastMaxWorkers = maxWorkers;
				semaphore = new Semaphore(maxWorkers, INIT_MAX_WORKERS + 1);
			}
		}

		/// <summary>
		/// Update the semaphore max count
		/// </summary>
		private void UpdateSemaphoreMax()
		{
			var delta = Mathf.Min(maxWorkers, INIT_MAX_WORKERS) - lastMaxWorkers;
			lastMaxWorkers = maxWorkers;

            if (delta < 0)
            {
                Wait(-delta);
            }

            else if (delta > 0)
            {
                Release(delta);
            }
		}

		/// <summary>
		/// Wait for a resource
		/// </summary>
		private void Wait(int count = 1)
		{
            for (int i = 0; i < count; i++)
            {
                Semaphore.WaitOne();
            }

			debugCount -= count;

			Debug.Log(string.Format("[BackgroundWorkerDispatcher] Workers: {0}, Still Available: {1}", lastMaxWorkers - debugCount, debugCount));
		}

		/// <summary>
		/// Release a resource
		/// </summary>
		private void Release(int count = 1)
		{
			debugCount += count;

			Debug.Log(string.Format("[BackgroundWorkerDispatcher] Workers: {0}, Still Available: {1}", lastMaxWorkers - debugCount, debugCount));

            for (int i = 0; i < count; i++)
            {
                Semaphore.Release();
            }
		}

		/// <summary>
		/// Utility wrapper for IEnumerator routines
		/// </summary>
		Action IEnumeratorWrapper(IEnumerator e)
		{
			return () =>
			{
				while (e.MoveNext())
					;
			};
		}


		/// <summary>
		/// Performs a yieldable asynchronous task
		/// </summary>
		private IEnumerator _BackgroundTask(Action taskCallback, Action<WorkerResult> resultCallback)
		{
			var workerResult = new WorkerResult();
			var working = true;

			// Setup worker
			var worker = new BackgroundWorker();
			worker.DoWork += (sender, e) =>
			{
				Wait();
                try
                {
                    taskCallback();
                }
                catch (Exception error)
                {
                    workerResult.error = error;
                }
                finally
                {
                    working = false;
                    Release();
                }
			};

			// Run and wait
			worker.RunWorkerAsync();
			yield return new WaitUntil(() => !working);
			worker.Dispose();

			// Result
			resultCallback?.Invoke(workerResult);
		}

		/// <summary>
		/// Performs a yieldable asynchronous task
		/// </summary>
		private IEnumerator _BackgroundTask<To>(Func<To> taskCallback, Action<WorkerResult<To>> resultCallback)
		{
			var workerResult = new WorkerResult<To>();
			var working = true;

			// Setup worker
			var worker = new BackgroundWorker();
			worker.DoWork += (sender, e) =>
            {
                Wait();
                try
                {
                    workerResult.result = taskCallback();
                }
                catch (Exception error)
                {
                    workerResult.error = error;
                }
                finally
                {
                    working = false;
                    Release();
                }
			};

			// Run and wait
			worker.RunWorkerAsync();
			yield return new WaitUntil(() => !working);
			worker.Dispose();

			// Result
			resultCallback?.Invoke(workerResult);
		}

		#endregion Non Public Methods
	}
}