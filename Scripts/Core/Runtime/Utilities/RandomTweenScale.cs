﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;
using CEUtilities.Coroutines;

namespace CEUtilities
{
    public class RandomTweenScale : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private bool runOnAwake = true;
        [SerializeField]
        private Vector2 range = new Vector2(.5f, 1.5f);
        [SerializeField]
        private float duration = 1f;

        #endregion Exposed fields

        #region Internal fields

        public bool RunOnAwake
        {
            get
            {
                return runOnAwake;
            }

            set
            {
                runOnAwake = value;
            }
        }

        public Vector2 Range
        {
            get
            {
                return range;
            }

            set
            {
                range = value;
            }
        }

        public float Duration
        {
            get
            {
                return duration;
            }

            set
            {
                duration = value;
            }
        }

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        #endregion Properties

        #region Unity events

        protected virtual void Awake()
        {
            if (RunOnAwake)
                Randomize();
        }

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Do a random tween
        /// </summary>
        [Button]
        public void Randomize()
        {
            Randomize(Range);
        }

        /// <summary>
        /// Do a random tween
        /// </summary>
        public void Randomize(float upperRange)
        {
            Randomize(new Vector2(1f / upperRange, upperRange));
        }

        /// <summary>
        /// Do a random tween
        /// </summary>
        public void Randomize(Vector2 range)
        {
#if UNITY_EDITOR
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                StopAllCoroutines();
                StartCoroutine(transform.TweenScale(Vector3.one * Random.Range(range.x, range.y), Duration));
            }
            else
            {
                EditorCoroutines.StopAllCoroutines(this);
                EditorCoroutines.StartCoroutine(transform.TweenScale(Vector3.one * Random.Range(range.x, range.y), Duration), this);
            }
#else
            StopAllCoroutines();
            StartCoroutine(transform.TweenScale(Vector3.one * Random.Range(range.x, range.y), Duration));
#endif
        }

        #endregion Methods
    }
}