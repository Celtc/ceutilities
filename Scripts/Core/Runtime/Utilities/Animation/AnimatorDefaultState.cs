﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Animation
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorDefaultState : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private string defaultState;

        [SerializeField]
        private int layer;

        #endregion Exposed fields

        #region Internal fields

        private Animator animatorComp;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public string DefaultState
        {
            get
            {
                return defaultState;
            }

            set
            {
                defaultState = value;
            }
        }

        public int Layer
        {
            get
            {
                return layer;
            }

            set
            {
                layer = value;
            }
        }


        private Animator AnimatorComp
        {
            get
            {
                if (animatorComp == null)
                    animatorComp = GetComponent<Animator>();

                return animatorComp;
            }            
        }

        #endregion Properties

        #region Events methods

        private void Start()
        {
            AnimatorComp.Play(defaultState, layer, 0);
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}