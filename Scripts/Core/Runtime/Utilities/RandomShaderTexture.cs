﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;
using CEUtilities.Coroutines;

namespace CEUtilities
{
    public class RandomShaderTexture : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private bool runOnAwake = true;

        [SerializeField]
        private List<Texture> textures = new List<Texture>();

        [SerializeField]
        private bool useGameObjectRenderTexture = true;

        [SerializeField]
        [HideIf("useGameObjectRenderTexture")]
        private Renderer targetRenderer;

        [SerializeField]
        private int materialNumber;

        [SerializeField]
        private string property;

        [SerializeField]
        private bool applyOnInstance = true;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public bool RunOnAwake
        {
            get
            {
                return runOnAwake;
            }

            set
            {
                runOnAwake = value;
            }
        }

        public List<Texture> Textures
        {
            get
            {
                return textures;
            }

            set
            {
                textures = value;
            }
        }

        public Renderer TargetRenderer
        {
            get
            {
                return targetRenderer;
            }

            set
            {
                targetRenderer = value;
            }
        }

        public int MaterialNumber
        {
            get
            {
                return materialNumber;
            }

            set
            {
                materialNumber = value;
            }
        }

        public string Property
        {
            get
            {
                return property;
            }

            set
            {
                property = value;
            }
        }

        #endregion Properties

        #region Unity events

        protected virtual void Awake()
        {
            if (RunOnAwake)
            {
                Randomize();
            }
        }

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Do a random.
        /// </summary>
        [Button]
        public void Randomize()
        {
            if (targetRenderer != null && !string.IsNullOrEmpty(property))
            {
                Renderer renderer;
                if (useGameObjectRenderTexture)
                {
                    renderer = GetComponent<Renderer>();
                }
                else
                {
                    renderer = targetRenderer;
                }

                Material material;
                if (applyOnInstance)
                {
                    material = renderer.materials[materialNumber];
                }
                else
                {
                    material = renderer.sharedMaterials[materialNumber];
                }

                material.SetTexture(property, textures[Random.Range(0, textures.Count)]);
            }
        }

        #endregion Methods
    }
}