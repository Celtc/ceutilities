﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;
using UnityEngine.Events;

namespace CEUtilities
{
    [System.Serializable]
    public struct GameState
    {
        /// <summary>
        /// Name of the state
        /// </summary>
        [FoldoutGroup("$DisplayableName")]
        public string Name;

        /// <summary>
        /// A set of gameobject which will be activated on enter, and deactivated on exit
        /// </summary>
        [FoldoutGroup("$DisplayableName")]
        public GameObject[] ActivateDuringState;

        /// <summary>
        /// Events triggered when entered state
        /// </summary>
        [FoldoutGroup("$DisplayableName")]
        [HideReferenceObjectPicker]
        public UnityEvent OnEnter;

        /// <summary>
        /// Events triggered before exiting
        /// </summary>
        [FoldoutGroup("$DisplayableName")]
        [HideReferenceObjectPicker]
        public UnityEvent OnExit;

#if UNITY_EDITOR
        public string DisplayableName
        {
            get
            {
                return "State: " + Name;
            }
        }
#endif

        /// <summary>
        /// Ctor
        /// </summary>
        public GameState(string name)
        {
            Name = name;

            ActivateDuringState = new GameObject[0];

            OnEnter = new UnityEvent();

            OnExit = new UnityEvent();
        }

        /// <summary>
        /// Enters the state
        /// </summary>
        public void Enter()
        {
            foreach (var go in ActivateDuringState)
            {
                go.SetActive(true);
            }

            OnEnter.Invoke();
        }

        /// <summary>
        /// Exit the state
        /// </summary>
        public void Exit()
        {
            foreach (var go in ActivateDuringState)
            {
                go.SetActive(false);
            }

            OnExit.Invoke();
        }
    }

    public class GameManager : Singleton<GameManager>
    {
        #region Enum

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private int startingState = 0;

        [SerializeField]
        [HideInInspector]
        private bool disableOthersOnStart = true;

        [SerializeField]
        [HideInInspector]
        private int numberOfStates = 0;

        [SerializeField]
        [HideInInspector]
        private List<GameState> states = new List<GameState>();


        private int currentStateIndex = -1;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the state of the starting.
        /// </summary>
        /// <value>
        /// The state of the starting.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup", order: 0)]
        [ValueDropdown("StatesNames")]
        public int StartingState
        {
            get
            {
                return startingState;
            }

            set
            {
                startingState = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [disable others on start].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [disable others on start]; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public bool DisableOthersOnStart
        {
            get
            {
                return disableOthersOnStart;
            }

            set
            {
                disableOthersOnStart = value;
            }
        }

        /// <summary>
        /// Gets the number of states.
        /// </summary>
        /// <value>
        /// The number of states.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [InlineButton("UpdateStatesAmount", "Change")]
        public int NumberOfStates
        {
            get
            {
                return numberOfStates;
            }

            private set
            {
                numberOfStates = value;
            }
        }

        /// <summary>
        /// Gets the states.
        /// </summary>
        /// <value>
        /// The states.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [OnValueChanged("OnExternalUpdateStatesAmount")]
        [ListDrawerSettings(HideAddButton = true)]
        public List<GameState> States
        {
            get
            {
                return states;
            }

            private set
            {
                states = value;
            }
        }

        /// <summary>
        /// Has states?
        /// </summary>
        public bool HasStates => states.Count > 0;

        /// <summary>
        /// The current state.
        /// </summary>
        public GameState? CurrentState => HasState(currentStateIndex) ? (GameState?)states[currentStateIndex] : null;

        /// <summary>
        /// The current state index.
        /// </summary>
        public int CurrentStateIndex => currentStateIndex;

#if UNITY_EDITOR

        /// <summary>
        /// Gets the states names.
        /// </summary>
        /// <value>
        /// The states names.
        /// </value>
        private ValueDropdownList<int> StatesNames
        {
            get
            {
                var valueItem = new ValueDropdownList<int>();
                for (int i = 0; i < states.Count; i++)
                {
                    valueItem.Add(new ValueDropdownItem<int>(states[i].Name, i));
                }
                return valueItem;
            }
        }

        /// <summary>
        /// Gets the name of the current state.
        /// </summary>
        /// <value>
        /// The name of the current state.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Runtime", order: 2)]
        private string CurrentStateName => CurrentState.HasValue ? CurrentState.Value.Name : "None";

#endif

        #endregion Properties

        #region Custom Eventss

        [ShowInInspector]
        [FoldoutGroup("Events", order: 1)]
        public UnityEvent_GameState OnEnterState = new UnityEvent_GameState();

        [ShowInInspector]
        [FoldoutGroup("Events")]
        public UnityEvent_GameState OnExitState = new UnityEvent_GameState();

        #endregion Custom Events

        #region Events methods

        private void Start()
        {
            if (HasState(StartingState))
            {
                if (DisableOthersOnStart)
                {
                    foreach (var state in states)
                    {
                        foreach (var go in state.ActivateDuringState)
                        {
                            go.SetActive(false);
                        }
                    }
                }

                ChangeToState(StartingState);
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Has an state?
        /// </summary>
        public bool HasState(string name)
        {
            return states.Any(x => x.Name == name);
        }

        /// <summary>
        /// Has an state?
        /// </summary>
        public bool HasState(int index)
        {
            return index >= 0 && states.Count > index;
        }

        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public GameState? GetState(string name)
        {
            var index = states.FindIndex(x => x.Name == name);
            if (index >= 0)
            {
                return states[index];
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Delayed change to the previous state.
        /// </summary>
        /// <param name="delay"></param>
        public void ChangeToPreviousState(float delay)
        {
            // Valid?
            var prevState = currentStateIndex - 1;
            if (!HasState(prevState))
            {
                Debug.LogWarning("[GameManager] No previous state.");
                return;
            }

            // Stop any possible pending change
            StopAllCoroutines();

            StartCoroutine(DelayedChangeToState(delay, prevState));
        }

        /// <summary>
        /// Change to the previous state.
        /// </summary>
        [ShowInInspector]
        [ButtonGroup("Runtime/Controls")]
        [DisableInEditorMode]
        public void ChangeToPreviousState()
        {
            var prevState = currentStateIndex - 1;
            if (!HasState(prevState))
            {
                Debug.LogWarning("[GameManager] No previous state");
                return;
            }

            ChangeToState(prevState);
        }

        /// <summary>
        /// Delayed change to the next state
        /// </summary>
        /// <param name="delay"></param>
        public void ChangeToNextState(float delay)
        {
            // Valid?
            var nextState = currentStateIndex + 1;
            if (!HasState(nextState))
            {
                Debug.LogWarning("[GameManager] No next state");
                return;
            }

            // Stop any possible pending change
            StopAllCoroutines();

            StartCoroutine(DelayedChangeToState(delay, nextState));
        }

        /// <summary>
        /// Change to the next state
        /// </summary>
        [ShowInInspector]
        [ButtonGroup("Runtime/Controls")]
        [DisableInEditorMode]
        public void ChangeToNextState()
        {
            var nextState = currentStateIndex + 1;
            if (!HasState(nextState))
            {
                Debug.LogWarning("[GameManager] No next state");
                return;
            }

            ChangeToState(nextState);
        }

        /// <summary>
        /// Change to a state by name
        /// </summary>
        public void ChangeToState(string name)
        {
            ChangeToState(states.FindIndex(x => x.Name == name));
        }

        /// <summary>
        /// Change to a state by index
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        [DisableInEditorMode]
        [Button]
        public void ChangeToState(int index)
        {
            // Stop any possible pending change
            StopAllCoroutines();

            // Not change?
            if (currentStateIndex == index)
            {
                return;
            }

            // Valid state?
            if (!HasState(index))
            {
                Debug.LogError("[GameManager] Invalid target state");
                return;
            }

            // Leave current state
            if (CurrentState != null)
            {
                CurrentState?.Exit();
                OnExitState.Invoke((GameState)CurrentState);
            }

            // Change
            currentStateIndex = index;

            // Enter next state
            CurrentState?.Enter();
            OnEnterState.Invoke((GameState)CurrentState);
        }

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Delayed change
        /// </summary>
        private IEnumerator DelayedChangeToState(float delay, int state)
        {
            yield return new WaitForSeconds(delay);

            ChangeToState(state);
        }

#if UNITY_EDITOR
        private void OnExternalUpdateStatesAmount()
        {
            // Show the correct number
            numberOfStates = states.Count;

            // Set the selection
            currentStateIndex = states.Count - 1;
        }

        private void UpdateStatesAmount()
        {
            // Remove states
            if (states.Count > numberOfStates)
            {
                var diff = states.Count - numberOfStates;
                states.RemoveRange(states.Count - diff, diff);
            }

            // Add states
            if (states.Count < numberOfStates)
            {
                var diff = numberOfStates - states.Count;
                for (int i = 0; i < diff; i++)
                {
                    States.Add(new GameState((states.Count + 1).ToString()));
                }

                currentStateIndex = states.Count - 1;
            }
        }
#endif

        #endregion Methods
    }
}