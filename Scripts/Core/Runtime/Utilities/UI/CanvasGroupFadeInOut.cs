﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace CEUtilities.UI
{
	[RequireComponent(typeof(CanvasGroup))]
	public class CanvasGroupFadeInOut : MonoBehaviour
	{
		#region Exposed fields

		[SerializeField]
		private float defaultDuration = .25f;

		#endregion Exposed fields

		#region Internal fields

		private CanvasGroup cGroup;

		#endregion Internal fields

		#region Custom Events

		public UnityEvent OnStartFade = new UnityEvent();

		public UnityEvent OnEndFade = new UnityEvent();

		public UnityEvent OnFadedIn = new UnityEvent();

		public UnityEvent OnFadedOut = new UnityEvent();

		#endregion Custom Events

		#region Properties

		public float DefaultDuration
		{
			get
			{
				return defaultDuration;
			}

			set
			{
				defaultDuration = value;
			}
		}


		private CanvasGroup CGroup
		{
			get
			{
				if (cGroup == null)
					cGroup = GetComponent<CanvasGroup>();

				return cGroup;
			}
		}

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		public void FadeOut()
		{
			StartCoroutine(_FadeOut(defaultDuration, 0f));
		}

		public void FadeOut(float duration)
		{
			StartCoroutine(_FadeOut(duration, 0f));
		}

		public void FadeOutDelayed(float delay)
		{
			StartCoroutine(_FadeOut(defaultDuration, delay));
		}

		public void FadeOutDelayed(float delay, float duration)
		{
			StartCoroutine(_FadeOut(duration, delay));
		}
				

		public void FadeIn()
		{
			StartCoroutine(_FadeIn(defaultDuration, 0f));
		}

		public void FadeIn(float duration)
		{
			StartCoroutine(_FadeIn(duration, 0f));
		}

		public void FadeInDelayed(float delay)
		{
			StartCoroutine(_FadeIn(defaultDuration, delay));
		}

		public void FadeInDelayed(float delay, float duration)
		{
			StartCoroutine(_FadeIn(duration, delay));
		}

		#endregion Public Methods

		#region Non Public Methods

		private IEnumerator _FadeOut(float duration, float delay)
		{
			if (delay > 0)
				yield return new WaitForSeconds(delay);

			OnStartFade.Invoke();

			yield return CGroup.FadeOut(duration, 0f);

			OnEndFade.Invoke();
			OnFadedOut.Invoke();
		}

		private IEnumerator _FadeIn(float duration, float delay)
		{
			if (delay > 0)
				yield return new WaitForSeconds(delay);

			OnStartFade.Invoke();

			yield return CGroup.FadeIn(duration, 0f);

			OnEndFade.Invoke();
			OnFadedIn.Invoke();
		}

		#endregion Non Public Methods
	}
}