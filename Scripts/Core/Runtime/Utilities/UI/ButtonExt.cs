﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
	[RequireComponent(typeof(Button))]
	public class ButtonExt : SerializedMonoBehaviour
	{
		#region Exposed fields

		#endregion Exposed fields

		#region Internal fields

		private Button buttonComp;

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		public Button ButtonComp
		{
			get
			{
				if (buttonComp == null)
					buttonComp = GetComponent<Button>();

				return buttonComp;
			}

			private set
			{
				buttonComp = value;
			}
		}

		#endregion Properties

		#region Events methods

		#endregion Events methods

		#region Public Methods

		[Button]
		public void Click()
		{
			ButtonComp.onClick.Invoke();
		}


		public void Click(float delay)
		{
			StartCoroutine(_Click(delay));
		}

		#endregion Public Methods

		#region Non Public Methods

		private IEnumerator _Click(float delay)
		{
			yield return new WaitForSeconds(delay);
			
			ButtonComp.onClick.Invoke();
		}

		#endregion Non Public Methods
	}
}