﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace CEUtilities.UI
{
    public class SpinningBar : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private float speed = 1f;

        #endregion Exposed fields

        #region Internal fields
        

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private void FixedUpdate()
        {
            transform.Rotate(Vector3.forward, Time.fixedDeltaTime * speed);
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}