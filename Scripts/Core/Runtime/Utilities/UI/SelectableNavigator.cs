﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace CEUtilities.UI
{
    public class SelectableNavigator : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private Selectable firstElement = null;

        #endregion Exposed fields

        #region Internal fields

        private EventSystem system;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        private EventSystem System
        {
            get
            {
                if (system == null)
                    system = EventSystem.current;

                return system;
            }
        }

        #endregion Properties

        #region Events methods

        void OnEnable()
        {
            SelectFirst();
        }

        void Update()
        {
            if (Input.anyKey)
            {
                Selectable target = null;
                bool pressedNavigateKey = false;

                if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.DownArrow))
                {
                    var selectedObject = System.currentSelectedGameObject;
                    if (selectedObject != null)
                    {
                        target = selectedObject.GetComponent<Selectable>().FindSelectableOnDown();
                    }
                    pressedNavigateKey = true;
                }
                else if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    var selectedObject = System.currentSelectedGameObject;
                    if (selectedObject != null)
                    {
                        target = selectedObject.GetComponent<Selectable>().FindSelectableOnUp();
                    }
                    pressedNavigateKey = true;
                }

                if (pressedNavigateKey && target != null)
                {
                    var field = target.GetComponent<Selectable>();
                    if (field != null)
                    {
                        if (field.GetType() == typeof(InputField))
                            (field as InputField).OnPointerClick(new PointerEventData(System));
                        if (field.GetType() == typeof(Dropdown))
                            (field as Dropdown).OnPointerClick(new PointerEventData(System));
                    }

                    System.SetSelectedGameObject(target.gameObject, new BaseEventData(System));
                }
                else if (pressedNavigateKey)
                {
                    SelectFirst();
                }
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Select the first element
        /// </summary>
        public void SelectFirst()
        {
            System.SetSelectedGameObject(null);
            if (firstElement != null)
                System.SetSelectedGameObject(firstElement.gameObject, new BaseEventData(System));
            else

                System.SetSelectedGameObject(System.firstSelectedGameObject, new BaseEventData(System));
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}