﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
	/// <summary>
	/// The sprite used needs to have a FullRect MeshType!
	/// </summary>
    [RequireComponent(typeof(Image))]
    [ExecuteInEditMode]
    public class ImageAlphaHitThreshold : SerializedMonoBehaviour
    {
        #region Exposed fields

        [InfoBox("The sprite mesh type should be full rect in order to this comp to work well", InfoMessageType.Info)]

        [SerializeField]
        [Range(0f, 1f)]
        [OnValueChanged("UpdateInternal")]
        private float alphaHitThreshold = .5f;

        [SerializeField]
        [HideInInspector]
        private Image image;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public Image Image
        {
            get
            {
                if (image == null)
                    image = GetComponent<Image>();

                return image;
            }

            set
            {
                image = value;
            }
        }

        public float AlphaHitThreshold
        {
            get
            {
                return alphaHitThreshold;
            }

            set
            {
                alphaHitThreshold = value;
                UpdateInternal();
            }
        }

        #endregion Properties

        #region Unity events

        private void Awake()
        {
            UpdateInternal();
        }

        #endregion Unity events

        #region Non Public Methods

        /// <summary>
        /// Udpates internal alpha threshold
        /// </summary>
        private void UpdateInternal()
        {
            if (!Image.IsReadable())
            {
                Debug.LogError("[ImageAlphaHitThreshold] The image must be readable to be able to use the alpha hit threshold", this);
                return;
            }

            //if (!Image.sprite.packed || Image.sprite.packingMode != SpritePackingMode.Rectangle)
            //    Debug.LogWarning("[ImageAlphaHitThreshold] The image packing should be full rect work well");

            Image.alphaHitTestMinimumThreshold = AlphaHitThreshold;
        }

        #endregion Non Public Methods

        #region Public Methods

        #endregion
    }
}
