﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
	[RequireComponent(typeof(Selectable))]
	public class SelectableKeyPressed : SerializedMonoBehaviour
	{
		#region Exposed fields

		[InfoBox("Allows to detect a key press when a selectable UI element is selected")]

		[SerializeField]
		private KeyCode key;

		[SerializeField]
		private float timegap;

		#endregion Exposed fields

		#region Internal fields

		private float timer;
		private EventSystem system;
		private Selectable tracked;

		#endregion Internal fields

		#region Custom Events

		/// <summary>
		/// Upong key pressed
		/// </summary>
		public UnityEvent OnKeyPressed = new UnityEvent();

		#endregion Custom Events

		#region Properties (public)

		public EventSystem System
		{
			get
			{
				if (system == null)
				{
					system = EventSystem.current;
				}
				return system;
			}

			set
			{
				system = value;
			}
		}

		public Selectable Tracked
		{
			get
			{
				if (tracked == null)
				{
					tracked = GetComponent<Selectable>();
				}
				return tracked;
			}

			set
			{
				tracked = value;
			}
		}

		#endregion Properties

		#region Unity events

		void OnEnable()
		{
			timer = timegap;
		}

		void Update()
		{
			if (timer < timegap)
			{
				timer += Time.deltaTime;
				return;
			}

			if (Input.GetKeyDown(key))
			{
				var selectedObject = System.currentSelectedGameObject;
				if (selectedObject == Tracked.gameObject)
					_Press();
			}
		}

		#endregion Unity events

		#region Public Methods

		[Button]
		public void Simulate()
		{
			_Press();
		}

		#endregion Non Public Methods

		#region Public Methods

		private void _Press()
		{
			OnKeyPressed.Invoke();
			timer = 0;
		}

		#endregion Non Public Methods
	}
}
