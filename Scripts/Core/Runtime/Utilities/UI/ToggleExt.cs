﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
    [RequireComponent(typeof(Toggle))]
    public class ToggleExt : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private bool triggerOnHookedValue = true;

		[SerializeField]
		private bool spriteChange = false;

        [SerializeField]
		[Indent]
		[ShowIf("spriteChange")]
        private Sprite onSprite = null;
        [SerializeField]
		[Indent]
		[ShowIf("spriteChange")]
        private Sprite offSprite = null;

		[SerializeField]
		private bool tintTransition = false;

        [SerializeField]
		[Indent]
		[ShowIf("tintTransition")]
        private Color onColor = Color.white;
        [SerializeField]
		[Indent]
		[ShowIf("tintTransition")]
        private Color offColor = Color.white;
        [SerializeField]
		[Indent]
		[ShowIf("tintTransition")]
        private float fadeDuration = .1f;

        #endregion Exposed fields

        #region Internal fields

        private Toggle toggleComp;

        #endregion Internal fields

        #region Custom Events

        [SerializeField]
        public UnityEvent OnValueIsOn = new UnityEvent();
        [SerializeField]
        public UnityEvent OnValueIsOff = new UnityEvent();

        #endregion Custom Events

        #region Properties (public)

        private Toggle ToggleComp
        {
            get
            {
                if (toggleComp == null)
                    toggleComp = GetComponent<Toggle>();

                return toggleComp;
            }
        }

		public bool SpriteChange
		{
			get
			{
				return spriteChange;
			}

			set
			{
				spriteChange = value;
			}
		}

		public Sprite OnSprite
		{
			get
			{
				return onSprite;
			}

			set
			{
				onSprite = value;
			}
		}

		public Sprite OffSprite
		{
			get
			{
				return offSprite;
			}

			set
			{
				offSprite = value;
			}
		}

		public bool TintTransition
		{
			get
			{
				return tintTransition;
			}

			set
			{
				tintTransition = value;
			}
		}

		public Color OnColor
		{
			get
			{
				return onColor;
			}

			set
			{
				onColor = value;
			}
		}

		public Color OffColor
		{
			get
			{
				return offColor;
			}

			set
			{
				offColor = value;
			}
		}

		public float FadeDuration
		{
			get
			{
				return fadeDuration;
			}

			set
			{
				fadeDuration = value;
			}
		}

		#endregion Properties

		#region Unity events

		void OnEnable()
        {
            RegisterCallback();

            if (triggerOnHookedValue)
                OnToggleValueChanged(ToggleComp.isOn);
        }

        void OnDisable()
        {
            UnregisterCallback();
        }

        public void OnToggleValueChanged(bool value)
        {
            if (value)
			{
				if (SpriteChange)
					ToggleComp.image.sprite = OnSprite;

				if (TintTransition)
					StartCoroutine(ToggleComp.image.TweenColor(OnColor, FadeDuration, false));

                OnValueIsOn.Invoke();
			}
            else
			{
				if (SpriteChange)
					ToggleComp.image.sprite = OffSprite;

				if (TintTransition)
					StartCoroutine(ToggleComp.image.TweenColor(OffColor, FadeDuration, false));

                OnValueIsOff.Invoke();
			}
        }

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Switch the value
        /// </summary>
        [Button]
        public void ToggleValue()
        {
            ToggleComp.isOn = !toggleComp.isOn;
        }

        /// <summary>
        /// Register callback for toggle value changed
        /// </summary>
        private void RegisterCallback()
        {
            ToggleComp.onValueChanged.AddListener(OnToggleValueChanged);
        }

        /// <summary>
        /// Unregister callback
        /// </summary>
        private void UnregisterCallback()
        {
            ToggleComp.onValueChanged.RemoveListener(OnToggleValueChanged);
        }

        #endregion Methods
    }
}