﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
	[RequireComponent(typeof(Image))]
	public class ImageColorSetter : MonoBehaviour
	{
		#region Variables (private)

		[SerializeField]
		[HideInInspector]
		private Image target;

		[SerializeField]
		private Color colorA;

        [SerializeField]
        private Color colorB;

        #endregion Variables

        #region Properties (public)

        public Color ColorA
        {
            get
            {
                return colorA;
            }

            set
            {
                colorA = value;
            }
        }

        public Color ColorB
        {
            get
            {
                return colorB;
            }

            set
            {
                colorB = value;
            }
        }


        private Image Target
		{
			get
			{
				if (target == null)
					target = GetComponent<Image>();

				return target;
			}
		}

		#endregion Properties

		#region Event Functions

		#endregion Event Functions

		#region Methods

        public void SetColorA()
        {
            SetTargetColor(colorA);
        }

        public void SetColorB()
        {
            SetTargetColor(colorB);
        }

        public void SetColor(Color color)
        {
            SetTargetColor(color);
        }


        private void SetTargetColor(Color color)
		{
			Target.color = color;
		}

		#endregion Methods
	}
}
