﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;
using CEUtilities.Colors;

namespace CEUtilities.UI
{
    [RequireComponent(typeof(Text))]
    public class TextExt : SerializedMonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private Text target;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        /// <summary>
        /// Target text
        /// </summary>
        public Text Target
        {
            get
            {
                if (target == null)
                    target = GetComponent<Text>();

                return target;
            }

            set
            {
                target = value;
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Set text color
        /// </summary>
        public void SetColor(string hex)
        {
            Target.color = ColorHelper.FromHexString(hex);
        }

        /// <summary>
        /// Set text color
        /// </summary>
        public void SetColor(HSVColor color)
        {
            Target.color = color.ToRGB();
        }

        /// <summary>
        /// Set text color
        /// </summary>
        public void SetColor(Vector4 color)
        {
            Target.color = new Color(color.x, color.y, color.z, color.w);
        }

        /// <summary>
        /// Set text color
        /// </summary>
        public void SetColor(Vector3 color)
        {
            Target.color = new Color(color.x, color.y, color.z);
        }

        /// <summary>
        /// Set text color
        /// </summary>
        public void LerpColor(string hex, float duration = .2f)
        {
            StartCoroutine(LerpColor_Routine(ColorHelper.FromHexString(hex), duration));
        }

        /// <summary>
        /// Set text color
        /// </summary>
        public void LerpColor(string hex)
        {
            StopAllCoroutines();
            StartCoroutine(LerpColor_Routine(ColorHelper.FromHexString(hex), .2f));
        }

        private IEnumerator LerpColor_Routine(Color color, float duration)
        {
            var startColor = Target.color;
            var timer = 0f;
            var ratio = 0f;
            while (timer <= duration)
            {
                ratio = timer / duration;
                Target.color = new Color(
                    Mathf.Lerp(startColor.r, color.r, ratio),
                    Mathf.Lerp(startColor.g, color.g, ratio),
                    Mathf.Lerp(startColor.b, color.b, ratio),
                    Mathf.Lerp(startColor.a, color.a, ratio)
                );

                yield return null;
                timer += Time.deltaTime;
            }
            Target.color = color;
        }

        #endregion Methods
    }
}