﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace CEUtilities.UI
{
    public class SelectableGroupEventTrigger : MonoBehaviour
    {
        #region Exposed fields

        private EventSystem system;

        #endregion Exposed fields

        #region Internal fields

        bool selected;

        #endregion Internal fields

        #region Custom Events

        public UnityEvent OnSelectGroup = new UnityEvent();

        public UnityEvent OnDeselectGroup = new UnityEvent();

        #endregion Custom Events

        #region Properties

        public EventSystem System
        {
            get
            {
                if (system == null)
                {
                    system = EventSystem.current;
                }
                return system;
            }

            set
            {
                system = value;
            }
        }

        #endregion Properties

        #region Events methods

        private void Update()
        {
            var selection = System.currentSelectedGameObject;

            // Deselect
            if (selected && !IsChildOrSelf(selection))
            {
                selected = false;
                OnDeselectGroup.Invoke();
            }

            // Select
            else if (!selected && IsChildOrSelf(selection))
            {
                selected = true;
                OnSelectGroup.Invoke();
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Is the transform a child of this transform?
        /// </summary>
        private bool IsChildOrSelf(GameObject go)
        {
            return go != null && (go.transform.IsChildOf(transform) || go == gameObject);
        }

        #endregion Methods
    }
}