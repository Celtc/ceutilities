﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace CEUtilities.UI
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(CanvasScaler))]
    public class CanvasScreenFit : MonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private CanvasScaler canvasScalerComp;

        #endregion Internal fields

        #region Properties

        private CanvasScaler CanvasScalerComp
        {
            get
            {
                if (canvasScalerComp == null)
                {
                    canvasScalerComp = GetComponent<CanvasScaler>();
                }
                return canvasScalerComp;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        private void Awake()
        {
            SetMatchingRatio();
        }

        private void OnRectTransformDimensionsChange()
        {
            SetMatchingRatio();
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Sets the matching ratio.
        /// </summary>
        private void SetMatchingRatio()
        {
            if (Screen.width > 0 && Screen.height > 0 && CanvasScalerComp.referenceResolution.y > 0f)
            {
                var idealRatio = CanvasScalerComp.referenceResolution.x / CanvasScalerComp.referenceResolution.y;
                var actualRatio = Screen.width / Screen.height;

                CanvasScalerComp.matchWidthOrHeight = actualRatio > idealRatio ? 1f : 0f;
            }
        }

        #endregion Methods
    }
}