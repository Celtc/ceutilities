﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
    [RequireComponent(typeof(Collider2D))]
	public class Collider2dToButton : MonoBehaviour
	{
        #region Exposed fields

        [InfoBox("This component is deprecated. Use Collider2dToPointerEvents instead.", InfoMessageType.Warning)]

        [SerializeField]
        private Button target;

        #endregion Exposed fields

        #region Internal fields

        private bool pressing = false;
        private bool inside = false;

        private Collider2D col;

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		private Collider2D Col
		{
			get
			{
				if (col == null)
					col = GetComponent<Collider2D>();

				return col;
			}

			set
			{
				col = value;
			}
		}

		private Button Target
		{
			get
			{
				if (target == null)
					target = GetComponent<Button>();

				return target;
			}
		}

		#endregion Properties

		#region Events methods

		private void Update()
		{
            var eventData = new PointerEventData(EventSystem.current);

            // Outside
            if (!inside)
            {
                // Enter?
                if (Col.OverlapPoint(Input.mousePosition))
                {
                    inside = true;
                    Target.OnPointerEnter(eventData);
                }
            }

            else
            {
                // Down?
                if (!pressing && Input.GetMouseButtonDown(0))
                {
                    pressing = true;
                    Target.OnPointerDown(eventData);
                }

                // Up?
                if (pressing && Input.GetMouseButtonUp(0))
                {
                    pressing = false;
                    Target.OnPointerUp(eventData);
                    target.onClick.Invoke();
                }

                // Exit?
                if (!Col.OverlapPoint(Input.mousePosition))
                {
                    inside = false;
                    if (pressing)
                    {
                        pressing = false;
                        Target.OnPointerUp(eventData);
                    }
                    Target.OnPointerExit(eventData);
                }
            }
		}

		#endregion Events methods

		#region Public Methods

		/// <summary>
		/// Force a new lookup of a collider
		/// </summary>
		public void UpdateCollider()
		{
			Col = null;
		}

		#endregion Public Methods

		#region Non Public Methods

		#endregion Non Public Methods
	}
}