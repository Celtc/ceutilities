﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CEUtilities.UI
{
    public class AutoCompleteOption : MonoBehaviour
    {
        #region Exposed fields

        private Text cachedText;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text
        {
            get
            {
                return CachedText.text;
            }

            set
            {
                CachedText.text = value;
            }
        }


        /// <summary>
        /// Text.
        /// </summary>
        private Text CachedText
        {
            get
            {
                if (cachedText == null)
                {
                    cachedText = GetComponentInChildren<Text>();
                }
                return cachedText;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}
