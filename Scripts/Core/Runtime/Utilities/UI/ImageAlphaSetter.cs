﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
	[RequireComponent(typeof(Image))]
	public class ImageAlphaSetter : MonoBehaviour
	{
		#region Variables (private)

		[SerializeField]
		[HideInInspector]
		private Image target;

		[SerializeField]
		[Range(0f, 1f)]
		[OnValueChanged("SetTargetAlpha")]
		private float alpha;

		#endregion Variables

		#region Properties (public)

		public float Alpha
		{
			get
			{
				return alpha;
			}
		}

		private Image Target
		{
			get
			{
				if (target == null)
					target = GetComponent<Image>();

				return target;
			}
		}

		#endregion Properties

		#region Event Functions

		#endregion Event Functions

		#region Methods

		public void SetAlpha(float alpha)
		{
			this.alpha = alpha;
			SetTargetAlpha();
		}
		

		private void SetTargetAlpha()
		{
			Target.color = Target.color.WithA(alpha);
		}

		#endregion Methods
	}
}
