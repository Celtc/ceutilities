using UnityEngine;

namespace CEUtilities.UI
{
    /// <summary>
    /// A helper class to turn on and off <see cref="CanvasRenderer"/> without causing allocations.
    /// </summary>
    public class TextureSwitch : MonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private CanvasRenderer r;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private void Awake()
        {
            r = GetComponent<CanvasRenderer>();
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Shows this instance.
        /// </summary>
        public void Show()
        {
            r.SetAlpha(1);
        }

        /// <summary>
        /// Hides this instance.
        /// </summary>
        public void Hide()
        {
            r.SetAlpha(0);
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}