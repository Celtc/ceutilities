using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CEUtilities.UI
{
    /// <summary>
    /// Generates a texture with a circle gradient.
    /// </summary>
    public class GradientTexture : MonoBehaviour
    {
        #region Enum

        /// <summary>
        /// Resolution in pixels.
        /// </summary>
        public enum Res
        {
            /// <summary>
            /// 16x16
            /// </summary>
            Pix16 = 16,

            /// <summary>
            /// 32x32
            /// </summary>
            Pix32 = 32,

            /// <summary>
            /// 64x64
            /// </summary>
            Pix64 = 64,

            /// <summary>
            /// 128x128
            /// </summary>
            Pix128 = 128,

            /// <summary>
            /// 256x256
            /// </summary>
            Pix256 = 256,

            /// <summary>
            /// 512x512
            /// </summary>
            Pix512 = 512
        }

        #endregion Enum

        #region Exposed fields

        /// <summary>
        /// The gradient.
        /// </summary>
        [OnValueChanged("Refresh")]
        public Gradient Gradient = new Gradient();

        /// <summary>
        /// Gradient's name. Used to cache textures.
        /// </summary>
        [OnValueChanged("Refresh")]
        public string Name = "Gradient";

        /// <summary>
        /// Texture resolution.
        /// </summary>
        [OnValueChanged("Refresh")]
        public Res Resolution = Res.Pix128;

        #endregion Exposed fields

        #region Internal fields

        private Texture2D texture;

        private static Dictionary<int, Texture2D> textureCache = new Dictionary<int, Texture2D>();

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private void Start()
        {
            var hash = Name.GetHashCode();
            if (!textureCache.TryGetValue(hash, out texture))
            {
                texture = Generate();
                textureCache.Add(hash, texture);
            }
            Apply();
        }

        /*
        private void OnValidate()
        {
            Refresh();
        }
        */

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Generates the gradient texture.
        /// </summary>
        /// <returns>Generated texture.</returns>
        public Texture2D Generate()
        {
            var res = (int)Resolution;
            var tex = new Texture2D(res, 1, TextureFormat.ARGB32, false, true)
            {
                name = Name,
                filterMode = FilterMode.Bilinear,
                wrapMode = TextureWrapMode.Clamp
            };

            var colors = new Color[res];
            float div = res;
            for (var i = 0; i < res; i++)
            {
                float t = i / div;
                colors[i] = Gradient.Evaluate(t);
            }
            tex.SetPixels(colors);
            tex.Apply(false, true);

            return tex;
        }

        #endregion Methods

        #region Non Public Methods

        private void Refresh()
        {
            if (texture != null)
                DestroyImmediate(texture);
            texture = Generate();
            Apply();
        }

        private void Apply()
        {
            var r = GetComponent<RawImage>();
            if (r == null) throw new Exception("GradientTexture must be on an UI element with RawImage component.");
            r.texture = texture;
        }

        #endregion Methods
    }
}