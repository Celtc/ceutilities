﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
    [System.Serializable]
    public abstract class CustomDropdown : Dropdown
    {
        #region Exposed fields
        
        [SerializeField]
        [BoxGroup("Constant Options", order: 100)]
        protected bool useTitle = false;
        [SerializeField]
        [BoxGroup("Constant Options")]
        [ShowIf("useTitle")]
        protected OptionData title;

        [SerializeField]
        [BoxGroup("Constant Options")]
        protected List<OptionData> preOptions = new List<OptionData>();
        [SerializeField]
        [BoxGroup("Constant Options")]
        protected List<OptionData> postOptions = new List<OptionData>();

        #endregion Exposed fields

        #region Internal fields

        protected bool showingTitle = false;
        protected bool addedTitle = false;
        protected bool removedTitle = false;

        #endregion Internal fields

        #region Custom Events

        [BoxGroup("Base Events", order: 200)]
        public UnityEvent_Sprite OnOptionSelectedImage = new UnityEvent_Sprite();

        [BoxGroup("Base Events")]
        public UnityEvent_String OnOptionSelectedText = new UnityEvent_String();

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Use a title?
        /// </summary>
        public bool UseTitle
        {
            get
            {
                return useTitle;
            }

            set
            {
                useTitle = value;
            }
        }

        /// <summary>
        /// Title
        /// </summary>
        public OptionData Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
            }
        }

        /// <summary>
        /// Added pre options
        /// </summary>
        public List<OptionData> PreOptions
        {
            get
            {
                return preOptions;
            }

            set
            {
                preOptions = value;
            }
        }

        /// <summary>
        /// Added post options
        /// </summary>
        public List<OptionData> PostOptions
        {
            get
            {
                return postOptions;
            }

            set
            {
                postOptions = value;
            }
        }


        /// <summary>
        /// Native value
        /// </summary>
        public new int value
        {
            get
            {
                return (addedTitle && !removedTitle) ? -1 : base.value;
            }

            set
            {
                RemoveTitle();

                base.value = value;
            }
        }

        /// <summary>
        /// String part of the selection value
        /// </summary>
        public string valueString
        {
            get
            {
                return value == -1 ? "None" : options[value].text;
            }
        }

        #endregion Properties

        #region Events methods

        protected override void Awake()
        {
            base.Awake();

            onValueChanged.AddListener((value) =>
            {
                if (options.Count >= value)
                {
                    OnOptionSelectedText.Invoke(options[value].text);
                    OnOptionSelectedImage.Invoke(options[value].image);
                }
            });
        }

        /// <summary>
        /// Extends click event for removing the title
        /// </summary>
        public override void OnPointerClick(PointerEventData eventData)
        {
            if (UseTitle && showingTitle)
            {
                RemoveTitle();
            }

            base.OnPointerClick(eventData);
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Adds the dropdown title
        /// </summary>
        public void ShowTitle()
        {
            if (showingTitle)
                return;

            var newOptions = options;
            newOptions.Insert(0, Title);
            options = newOptions;
            ForceValueUpdate();

            showingTitle = true;
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Clears all options (including title)
        /// </summary>
        public void Clear()
        {
            showingTitle = false;
            options.Clear();
        }

        /// <summary>
        /// Removes the title
        /// </summary>
        protected virtual void RemoveTitle()
        {
            if (!showingTitle)
                return;
            
            options.RemoveAt(0);
            ForceValueUpdate(true);

            showingTitle = false;
        }

        /// <summary>
        /// Force the update of the values
        /// </summary>
        protected virtual void ForceValueUpdate(bool triggerChanged = false)
        {
            RefreshShownValue();
            UISystemProfilerApi.AddMarker("Dropdown.value", this);
            if (triggerChanged)
                onValueChanged.Invoke(base.value);
        }

        #endregion
    }
}