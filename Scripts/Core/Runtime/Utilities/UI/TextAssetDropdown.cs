﻿using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
	public class TextAssetDropdown : CustomDropdown
	{
        #region Exposed fields

        [SerializeField]
        private TextAsset optionsAsset;
        [SerializeField]
        private bool fixEndlines = true;

        [SerializeField]
		private bool loadOnStart = true;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        public UnityEvent OnLoadOptions = new UnityEvent();

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Asset from where the options will be loaded
        /// </summary>
        public TextAsset OptionsAsset
		{
			get
			{
				return optionsAsset;
			}

			set
			{
				optionsAsset = value;
			}
        }

        /// <summary>
        /// Should fix endlines?
        /// </summary>
        public bool FixEndlines
        {
            get
            {
                return fixEndlines;
            }

            set
            {
                fixEndlines = value;
            }
        }

        /// <summary>
        /// Load on start
        /// </summary>
        public bool LoadOnStart
        {
            get
            {
                return loadOnStart;
            }

            set
            {
                loadOnStart = value;
            }
        }

        #endregion Properties

        #region Events methods

        protected override void Start()
		{
			base.Start();

			if (Application.isPlaying)
				if (LoadOnStart)
					Load();
        }

        /// <summary>
        /// Called after loading the options
        /// </summary>
        private void OnLoadOptions_Callback()
        {
            OnLoadOptions.Invoke();
        }

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Load the options
        /// </summary>
        public void Load()
		{
            // Load options
            var text = OptionsAsset.text;
            if (FixEndlines)
                text = text.Replace("\r\n", "\n");
			var rawOptions = text.Split(new[] { '\r', '\n' });
			var newOptions = new List<OptionData>(rawOptions.Select(x => new OptionData(x)));

            // Add pre
            if (PreOptions != null && PreOptions.Count > 0)
                newOptions.InsertRange(0, PreOptions);

            // Add post
            if (PostOptions != null && PostOptions.Count > 0)
                newOptions.AddRange(PostOptions);
            
			options = newOptions;

            // Title
            if (UseTitle)
                ShowTitle();
            else if (options.Count > 0)
                ForceValueUpdate(true);

            // Event
            OnLoadOptions_Callback();
        }

		#endregion Methods
	}
}