﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

namespace CEUtilities.UI
{
    [RequireComponent(typeof(Slider))]
    public class SliderExt : MonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private Slider sliderComp;

        #endregion Internal fields

        #region Custom Events

        public UnityEvent OnFilled = new UnityEvent();

        public UnityEvent OnEmpty = new UnityEvent();

        #endregion Custom Events

        #region Properties

        private Slider SliderComp
        {
            get
            {
                if (sliderComp == null)
                    sliderComp = GetComponent<Slider>();

                return sliderComp;
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            SliderComp.onValueChanged.AddListener(OnValueChangedCallback);
        }

        private void OnDisable()
        {
            SliderComp.onValueChanged.RemoveListener(OnValueChangedCallback);
        }

        private void OnValueChangedCallback(float value)
        {
            if (value == 1f)
                OnFilled.Invoke();

            else if (value == 0f)
                OnFilled.Invoke();
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}