﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

using Sirenix.OdinInspector;
using CEUtilities.API;
using Sirenix.Serialization;
using UnityEngine.Events;

namespace CEUtilities.UI
{
    public class ExternalFeedDropdown : CustomDropdown, ISerializationCallbackReceiver
    {
        #region Enum

        public enum FetchMode : int
        {
            Synchronous = 0,
            Asynchronous = 1
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        private FetchMode mode;
        [ShowInInspector]
        [OdinSerialize]
        [ShowIf("mode", optionalValue: FetchMode.Synchronous)]
        [Indent]
        private Func<List<string>> provider;
        [ShowInInspector]
        [OdinSerialize]
        [ShowIf("mode", optionalValue: FetchMode.Asynchronous)]
        [Indent]
        private Action<Action<List<string>>> asyncProvider;

        [SerializeField]
        private bool fetchOnStart = true;

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        private SerializationData serializationData;

        #endregion Internal fields

        #region Custom Events

        public UnityEvent OnFetchedOptions = new UnityEvent();

        public UnityEvent_String OnOptionSelected = new UnityEvent_String();

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Current fetch mode
        /// </summary>
        public FetchMode Mode
        {
            get
            {
                return mode;
            }

            set
            {
                mode = value;
            }
        }
        
        /// <summary>
        /// Provider for options
        /// </summary>
        public Func<List<string>> Provider
        {
            get
            {
                return provider;
            }

            set
            {
                provider = value;
            }
        }

        /// <summary>
        /// Async provider for options
        /// </summary>
        public Action<Action<List<string>>> AsyncProvider
        {
            get
            {
                return asyncProvider;
            }

            set
            {
                asyncProvider = value;
            }
        }

        /// <summary>
        /// Should fetch on start?
        /// </summary>
        public bool FetchOnStart
        {
            get
            {
                return fetchOnStart;
            }

            set
            {
                fetchOnStart = value;
            }
        }

        #endregion Properties

        #region Events methods

        protected override void Start()
        {
            base.Start();

            if (Application.isPlaying)
                if (FetchOnStart)
                    Fetch();
        }

        /// <summary>
        /// Called after feeding the options from the external provider
        /// </summary>
        private void OnFetchedOptions_Callback()
        {
            OnFetchedOptions.Invoke();
        }
        
        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            UnitySerializationUtility.DeserializeUnityObject(this, ref this.serializationData);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            UnitySerializationUtility.SerializeUnityObject(this, ref this.serializationData);
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Load the options.
        /// </summary>
        public void Fetch()
        {
            if (Mode == FetchMode.Synchronous && Provider != null)
                Populate(Provider());

            else if (Mode == FetchMode.Asynchronous && AsyncProvider != null)
                AsyncProvider((texts) => Populate(texts));
        }

        /// <summary>
        /// Manually populate the options from an array of strings.
        /// </summary>
        public void Populate(List<string> texts)
        {
            Clear();

            // Generate options
            var newOptions = texts.Select(x => new OptionData(x)).ToList();

            // Add pre
            if (PreOptions != null && PreOptions.Count > 0)
                newOptions.InsertRange(0, PreOptions);

            // Add post
            if (PostOptions != null && PostOptions.Count > 0)
                newOptions.AddRange(PostOptions);

            options = newOptions;

            // Title
            if (UseTitle)
                ShowTitle();
            else if (options.Count > 0)
                ForceValueUpdate(true);

            // Event
            OnFetchedOptions_Callback();
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion
    }
}