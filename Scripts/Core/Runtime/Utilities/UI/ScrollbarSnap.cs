﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace CEUtilities.UI
{
    [RequireComponent(typeof(Scrollbar))]
    public class ScrollbarSnap : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        #region Exposed fields

        [SerializeField]
        private float normalizedTargetPosition = 0f;

        #endregion Exposed fields

        #region Internal fields

        private Scrollbar target;

        private bool externalControl = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public float NormalizedTargetPosition
        {
            get
            {
                return normalizedTargetPosition;
            }

            set
            {
                normalizedTargetPosition = value;
            }
        }

        private Scrollbar Target
        {
            get
            {
                if (target == null)
                    target = GetComponent<Scrollbar>();

                return target;
            }
        }

        #endregion Properties

        #region Events methods

        public void OnPointerDown(PointerEventData eventData)
        {
            externalControl = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            normalizedTargetPosition = target.value;
            externalControl = false;
        }

        void LateUpdate()
        {
            if (!externalControl)
                Target.value = NormalizedTargetPosition;
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}