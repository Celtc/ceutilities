﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using PhoneDirectory;

namespace CEUtilities.UI
{
    public class AutoCompleteInputField : InputField, ISubmitHandler
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private bool forceExistingOption;

        [SerializeField]
        [HideInInspector]
        private bool caseSensitive;

        [SerializeField]
        [HideInInspector]
        private int shownOptions = 5;


        [SerializeField]
        [HideInInspector]
        private bool loadOptionsFromText;

        [SerializeField]
        [HideInInspector]
        private TextAsset optionsAsset;


        [SerializeField]
        [HideInInspector]
        private GameObject template;

        #endregion Exposed fields

        #region Internal fields

        // All options (usernames)
        private HashSet<string> options = new HashSet<string>();

        // Shown options
        private List<string> availableOptions;

        private bool showingOptions;

        private Node lookupTrieRootNode = new Node(string.Empty, string.Empty);


        private bool ignoreChange;

        private bool deselecting;

        private int selectedAvailableOption;


        private GameObject instancedOptionsContainer;

        private RectTransform instancedOptionsLayout;

        private List<AutoCompleteOption> instancedOptions;

        #endregion Internal fields

        #region Properties

        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public bool ForceExistingOption
        {
            get
            {
                return forceExistingOption;
            }

            set
            {
                forceExistingOption = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public bool CaseSensitive
        {
            get
            {
                return caseSensitive;
            }

            set
            {
                caseSensitive = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public int ShownOptions
        {
            get
            {
                return shownOptions;
            }

            set
            {
                shownOptions = value;
            }
        }

        [HideInInspector]
        //[ShowInInspector]
        [FoldoutGroup("Setup")]
        public HashSet<string> Options
        {
            get
            {
                return options;
            }

            set
            {
                options = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Setup/Config")]
        public bool LoadOptionsFromText
        {
            get
            {
                return loadOptionsFromText;
            }

            set
            {
                loadOptionsFromText = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Setup/Config")]
        public TextAsset OptionsAsset
        {
            get
            {
                return optionsAsset;
            }

            set
            {
                optionsAsset = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Setup/Advance", expanded: false)]
        public GameObject Template
        {
            get
            {
                return template;
            }

            set
            {
                template = value;
            }
        }


        /// <summary>
        /// The current value of the input field.
        /// </summary>
        public new string text
        {
            get
            {
                return base.text;
            }

            private set
            {
                base.text = value;
            }
        }


        /// <summary>
        /// Is there any available option.
        /// </summary>
        private bool HasAvailableOptions => showingOptions && availableOptions.Count > 0;

        /// <summary>
        /// Has currently selected an option from the availables.
        /// </summary>
        private bool SelectedFromAvailables => selectedAvailableOption >= 0;

        #endregion Properties

        #region Custom Events

        [ShowInInspector]
        [FoldoutGroup("Events", order: 2)]
        public UnityEvent_String OnEndEditPostAutoComplete = new UnityEvent_String();

        #endregion Custom Events

        #region Events methods

        /// <summary>
        /// See MonoBehaviour.Start.
        /// </summary>
        protected override void Start()
        {
            base.Start();

            if (LoadOptionsFromText && OptionsAsset != null)
            {
                ReadOptions();
            }
        }

        /// <summary>
        /// Called when [enable].
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();

            onValueChanged.AddListener(OnValueChanged);
            onEndEdit.AddListener(OnEndEdit);
        }

        /// <summary>
        /// See MonoBehaviour.OnDisable.
        /// </summary>
        protected override void OnDisable()
        {
            base.OnDisable();

            HideOptions();

            onValueChanged.RemoveListener(OnValueChanged);
            onEndEdit.RemoveListener(OnEndEdit);
        }

        /// <summary>
        /// Updates this instance.
        /// </summary>
        private void Update()
        {
            if (showingOptions)
            {
                if (UnityEngine.Input.GetKeyDown(KeyCode.DownArrow))
                {
                    SelectNextAvailable();
                }

                else if (UnityEngine.Input.GetKeyDown(KeyCode.UpArrow))
                {
                    SelectPreviousAvailable();
                }
            }
        }


        /// <summary>
        /// Called when [value changed].
        /// </summary>
        /// <param name="value">The value.</param>
        private void OnValueChanged(string value)
        {
            // Ignore value change
            if (ignoreChange || deselecting)
            {
                return;
            }

            // Set value by external component
            if (!isFocused)
            {
                return;
            }

            if (!showingOptions)
            {
                ShowOptions();
            }

            RefreshOptions();
        }

        /// <summary>
        /// Called when [end edit].
        /// </summary>
        /// <param name="value">The value.</param>
        private void OnEndEdit(string value)
        {
            if (ForceExistingOption && !SelectedFromAvailables && !OptionExist(text))
            {
                if (!SelectFirstAvailable())
                {
                    SelectFirst();
                }
            }

            HideOptions();

            OnEndEditPostAutoComplete.Invoke(text);
        }


        /// <summary>
        /// Called when [end edit].
        /// </summary>
        /// <param name="eventData">The event data.</param>
        public override void OnDeselect(BaseEventData eventData)
        {
            deselecting = true;

            if (eventData is PointerEventData)
            {
                var pointerEventData = (PointerEventData)eventData;
                var pointerEnter = pointerEventData.pointerEnter;
                if (pointerEnter != null)
                {
                    var pointerEnterParent = pointerEnter.transform.parent;
                    if (pointerEnterParent != null)
                    {
                        var pointerEnterParentGO = pointerEnterParent.gameObject;
                        if (pointerEnterParentGO.HasComponent<AutoCompleteOption>())
                        {
                            var option = pointerEnterParentGO.GetComponent<AutoCompleteOption>();
                            selectedAvailableOption = instancedOptions.IndexOf(option);
                            SelectByValue(option.Text);
                            StartCoroutine(_LateMoveCaretToEnd());
                        }
                    }
                }
            }

            deselecting = false;

            base.OnDeselect(eventData);
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Sets the options.
        /// </summary>
        public void SetOptions(HashSet<string> options)
        {
            Options = options;
            
            lookupTrieRootNode = new Node(string.Empty, string.Empty);
            foreach (var option in Options)
            {
                if (!string.IsNullOrEmpty(option))
                {
                    Trie.InsertNode(option, lookupTrieRootNode, CaseSensitive);
                }
            }
        }

        /// <summary>
        /// Selects the first available option if possible.
        /// </summary>
        public bool SelectFirstAvailable()
        {
            if (HasAvailableOptions)
            {
                text = availableOptions[0];

                MoveCaretToEnd();

                return true;
            }

            return false;
        }

        /// <summary>
        /// Selects the next available option.
        /// </summary>
        public void SelectNextAvailable()
        {
            if (HasAvailableOptions && selectedAvailableOption + 1 < availableOptions.Count)
            {
                ignoreChange = true;

                text = availableOptions[++selectedAvailableOption];

                ignoreChange = false;

                MoveCaretToEnd();
            }
        }

        /// <summary>
        /// Selects the previous available option.
        /// </summary>
        public void SelectPreviousAvailable()
        {
            if (HasAvailableOptions && selectedAvailableOption > 0)
            {
                ignoreChange = true;

                text = availableOptions[--selectedAvailableOption];

                ignoreChange = false;

                MoveCaretToEnd();
            }
        }

        /// <summary>
        /// Selects the first option.
        /// </summary>
        public void SelectFirst()
        {
            if (Options.Count > 0)
            {
                text = Options.ElementAt(0);

                MoveCaretToEnd();
            }

            if (!isFocused)
            {
                OnEndEdit(text);
            }
        }

        /// <summary>
        /// Selects an option by value.
        /// </summary>
        public void SelectByValue(string value)
        {
            // Set value
            if (ForceExistingOption && !OptionExist(value))
            {
                SelectFirst();
            }
            else
            {
                text = value;

                if (!isFocused)
                {
                    OnEndEdit(text);
                }
            }
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Waits for end of frame before moving the caret to the end.
        /// </summary>
        /// <returns></returns>
        private IEnumerator _LateMoveCaretToEnd()
        {
            if (isFocused)
            {
                yield return new WaitForEndOfFrame();

                MoveCaretToEnd();
            }

            yield break;
        }

        /// <summary>
        /// Moves the caret to the end.
        /// </summary>
        private void MoveCaretToEnd()
        {
            caretPosition = text.Length;
        }

        /// <summary>
        /// Options the exist.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private bool OptionExist(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            var exactNode = Trie.GetChildNode(lookupTrieRootNode, value, CaseSensitive);
            if (exactNode == null || !exactNode.IsTerminal)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reads the options.
        /// </summary>
        private void ReadOptions()
        {
            Options = new HashSet<string>(OptionsAsset.text
                .Replace("\r\n", "\n")
                .Split(new[] { '\r', '\n' })
                .OrderBy(x => x)
            );
            
            lookupTrieRootNode = new Node(string.Empty, string.Empty);
            foreach (var option in Options)
            {
                if (!string.IsNullOrEmpty(option))
                {
                    Trie.InsertNode(option, lookupTrieRootNode, CaseSensitive);
                }
            }
        }

        /// <summary>
        /// Instances the template.
        /// </summary>
        private void InstanceTemplate()
        {
            // Instantiate the container
            instancedOptionsContainer = Instantiate(Template, transform);

            // Cache the reference to the layout group
            instancedOptionsLayout = instancedOptionsContainer.GetComponentInChildren<VerticalLayoutGroup>()?.transform as RectTransform;
            instancedOptionsLayout.DestroyChildrens();

            // Pre instantiate options
            var optionTemplate = Template.GetComponentInChildren<AutoCompleteOption>();
            instancedOptions = new List<AutoCompleteOption>(ShownOptions);
            for (int i = 0; i < ShownOptions; i++)
            {
                var newOption = Instantiate(optionTemplate, instancedOptionsLayout);
                newOption.gameObject.SetActive(false);

                instancedOptions.Add(newOption);
            }

            // Initialize auxiliar vars
            availableOptions = new List<string>(ShownOptions);
        }

        /// <summary>
        /// Shows the options.
        /// </summary>
        private void ShowOptions()
        {
            if (!showingOptions)
            {
                if (instancedOptionsContainer == null)
                {
                    InstanceTemplate();
                }

                instancedOptionsContainer.SetActive(true);

                showingOptions = true;
            }
        }

        /// <summary>
        /// Hides the options.
        /// </summary>
        private void HideOptions()
        {
            if (showingOptions)
            {
                instancedOptionsContainer.SetActive(false);

                showingOptions = false;

                // Clear flag
                selectedAvailableOption = -1;
            }
        }

        /// <summary>
        /// Refreshes the options.
        /// </summary>
        private void RefreshOptions()
        {
            availableOptions = DoFilter(text, ShownOptions);

            // Enable
            for (int i = 0; i < availableOptions.Count; i++)
            {
                instancedOptions[i].gameObject.SetActive(true);
                instancedOptions[i].Text = availableOptions[i];
            }

            // Disable
            for (int i = ShownOptions - 1; i >= availableOptions.Count; i--)
            {
                instancedOptions[i].gameObject.SetActive(false);
            }

            // Clear flag
            selectedAvailableOption = -1;
        }

        /// <summary>
        /// Does the filter.
        /// </summary>
        /// <returns></returns>
        private List<string> DoFilter(string filter, int maxResults)
        {
            // No options?
            if (Options.Count == 0)
            {
                return new List<string>();
            }

            // Check for empty filter
            if (string.IsNullOrEmpty(filter))
            {
                return Options.Take(maxResults).ToList();
            }

            // Get starting node
            var results = new List<string>(ShownOptions);
            var startingNode = Trie.GetChildNode(lookupTrieRootNode, filter, CaseSensitive);
            if (startingNode != null)
            {
                // Get the childrens
                var iterator = startingNode.GetDepthNodes();
                while (iterator.MoveNext())
                {
                    var childNode = (Node)iterator.Current;
                    if (childNode.IsTerminal)
                    {
                        results.Add(childNode.Value);
                        if (results.Count == maxResults)
                        {
                            break;
                        }
                    }
                }
            }

            return results;
        }

        #endregion Non Public Methods
    }
}
