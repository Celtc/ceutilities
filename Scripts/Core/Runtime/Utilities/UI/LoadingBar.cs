﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Video;

using Sirenix.OdinInspector;

namespace CEUtilities.UI
{
    [RequireComponent(typeof(Image))]
    public class LoadingBar : MonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private Image target;

        private bool beenLoading = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public Image Target
        {
            get
            {
                if (target == null)
                    target = GetComponent<Image>();

                return target;
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            Target.fillAmount = 0f;
        }

        private void FixedUpdate()
        {
            float amount = 0;

            if (SceneHandlerManager.Instance.IsLoading)
            {
                beenLoading = true;
                amount = SceneHandlerManager.Instance.OverallLoadingProgress;
            }
            else if (beenLoading)
                amount = 1f;
            else
                amount = 0f;
            
            Target.fillAmount = amount;
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}