﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace CEUtilities.UI
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(RawImage))]
	public class RawImageAlphaSetter : MonoBehaviour
	{
		#region Variables (private)

		[SerializeField]
		[HideInInspector]
		private RawImage target;

		[SerializeField]
		[Range(0f, 1f)]
		private float alpha;

		#endregion Variables

		#region Properties (public)

		public float Alpha
		{
			get
			{
				return alpha;
			}

			set
			{
				alpha = value;
			}
		}

		private RawImage Target
		{
			get
			{
				if (target == null)
					target = GetComponent<RawImage>();

				return target;
			}
		}

		#endregion Properties

		#region Event Functions

		void Update()
		{
			if (Target.color.a != Alpha)
				Target.color = new Color(Target.color.r, Target.color.g, Target.color.b, Alpha);
		}

		#endregion Event Functions

		#region Methods

		#endregion Methods
	}
}
