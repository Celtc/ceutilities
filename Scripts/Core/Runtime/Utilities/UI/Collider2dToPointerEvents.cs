﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;
using CEUtilities.Helpers;
using UnityEngine.EventSystems;

namespace CEUtilities.UI
{
    /// <summary>
    /// IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
	public class Collider2dToPointerEvents : MonoBehaviour
	{
        #region Exposed fields

        [SerializeField]
        private GameObject eventsReceiver;

        #endregion Exposed fields

        #region Internal fields

        private Canvas rootCanvas;
        //private CanvasScaler scaler;

        private PointerEventData pointerEventData;
        private bool pressing = false;
        private bool inside = false;

        private Collider2D colliderComp;

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		private Collider2D ColliderComp
        {
			get
			{
				if (colliderComp == null)
					colliderComp = GetComponent<Collider2D>();

				return colliderComp;
			}

			set
			{
				colliderComp = value;
			}
        }

        private GameObject EventsReceiver
        {
            get
            {
                if (eventsReceiver == null)
                    eventsReceiver = gameObject;

                return eventsReceiver;
            }
        }

        private IPointerDownHandler[] PointerDownHandlerReceivers => EventsReceiver.GetComponents<IPointerDownHandler>();

        private IPointerUpHandler[] PointerUpHandlerReceivers => EventsReceiver.GetComponents<IPointerUpHandler>();

        private IPointerEnterHandler[] PointerEnterHandlerReceivers => EventsReceiver.GetComponents<IPointerEnterHandler>();

        private IPointerExitHandler[] PointerExitHandlerReceivers => EventsReceiver.GetComponents<IPointerExitHandler>();

        private IPointerClickHandler[] PointerClickHandlerReceivers => EventsReceiver.GetComponents<IPointerClickHandler>();

        #endregion Properties

        #region Events methods

        private void Awake()
        {
            var rt = GetComponent<RectTransform>();
            rootCanvas = rt.GetComponentInParent<Canvas>().rootCanvas;
            //scaler = rootCanvas.GetComponent<CanvasScaler>();
        }

        private void Update()
		{
            // Cache event data
            pointerEventData = NewEventData();
            
            // Check updates
            OnPointerUpdate();

            // Early exit if not inside nor pressing
            if (!pressing && !inside)
                return;

            // Left click down
            if (Input.GetMouseButtonDown(0))
                OnPointerPressed();

            // Left click up
            if (Input.GetMouseButtonUp(0))
                OnPointerReleased();
		}

        private void OnPointerPressed()
        {
            // Wasn't pressing and inside bounds => Down
            if (!pressing && inside)
            {
                pressing = true;
                PointerDownHandlerReceivers.ForEach(x => x.OnPointerDown(pointerEventData));
            }
        }

        private void OnPointerUpdate()
        {
            Vector2 checkingPoint = new Vector2(-1f, -1f);

            // Screen space overlay
            if (rootCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
                checkingPoint = Input.mousePosition;

            // Screen space camera
            else if (rootCanvas.renderMode == RenderMode.ScreenSpaceCamera)
                checkingPoint = rootCanvas.worldCamera.ScreenToWorldPoint(Input.mousePosition.WithZ(rootCanvas.planeDistance));

            // Check collision
            var insideBounds = ColliderComp.OverlapPoint(checkingPoint);

            // Wasn't inside and now is => Enter
            if (!inside && insideBounds)
            {
                inside = true;
                PointerEnterHandlerReceivers.ForEach(x => x.OnPointerEnter(pointerEventData));
            }

            // Was inside and now isn't => Exit
            else if (inside && !insideBounds)
            {
                inside = false;
                PointerExitHandlerReceivers.ForEach(x => x.OnPointerExit(pointerEventData));
            }
        }

        private void OnPointerReleased()
        {
            // If was pressing => Up
            if (pressing)
            {
                pressing = false;
                PointerUpHandlerReceivers.ForEach(x => x.OnPointerUp(pointerEventData));

                // Was inside bounds => Click
                if (inside)
                {
                    PointerClickHandlerReceivers.ForEach(x => x.OnPointerClick(pointerEventData));
                }
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        private PointerEventData NewEventData()
        {
            return new PointerEventData(EventSystem.current);
        }

        #endregion Non Public Methods
    }
}