﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;
using CEUtilities.Coroutines;

namespace CEUtilities
{
    public class RandomPosition : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private bool runOnAwake = true;

        [SerializeField]
        private Vector2 rangeX = new Vector2(-5f, 5f);
        [SerializeField]
        private Vector2 rangeY = new Vector2(-5f, 5f);
        [SerializeField]
        private Vector2 rangeZ = new Vector2(-5f, 5f);

        #endregion Exposed fields

        #region Internal fields

        public bool RunOnAwake
        {
            get
            {
                return runOnAwake;
            }

            set
            {
                runOnAwake = value;
            }
        }

        public Vector2 RangeX
        {
            get
            {
                return rangeX;
            }

            set
            {
                rangeX = value;
            }
        }

        public Vector2 RangeY
        {
            get
            {
                return rangeY;
            }

            set
            {
                rangeY = value;
            }
        }

        public Vector2 RangeZ
        {
            get
            {
                return rangeZ;
            }

            set
            {
                rangeZ = value;
            }
        }

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        #endregion Properties

        #region Unity events

        protected virtual void Awake()
        {
            if (RunOnAwake)
            {
                Randomize();
            }
        }

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Do a random tween
        /// </summary>
        [Button]
        public void Randomize()
        {
            Randomize(RangeX, RangeY, RangeZ);
        }

        /// <summary>
        /// Do a random tween
        /// </summary>
        public void Randomize(Vector2 rangeX, Vector2 rangeY, Vector2 rangeZ)
        {
            transform.position = new Vector3
            (
                Random.Range(rangeX.x, rangeX.y),
                Random.Range(rangeY.x, rangeY.y),
                Random.Range(rangeZ.x, rangeZ.y)
            );
        }

        #endregion Methods
    }
}