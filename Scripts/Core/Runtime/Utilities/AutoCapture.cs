﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

using CEUtilities.API;
using Sirenix.OdinInspector;

namespace CEUtilities
{
    public class AutoCapture : MonoBehaviour
    {
        #region Enums

        public enum CaptureSource
        {
            Webcam,
            RenderTexture,
            Camera,
            Screen
        }

        #endregion

        #region Variables (private)

        [SerializeField]
        private CaptureSource source = CaptureSource.Screen;

        [SerializeField]
        [ShowIf("source", CaptureSource.Webcam)]
        [Indent]
        private Webcam webcamSource;

        [SerializeField]
        [ShowIf("source", CaptureSource.RenderTexture)]
        [Indent]
        private RenderTexture renderTextureSource;

        [SerializeField]
        [ShowIf("source", CaptureSource.Camera)]
        [Indent]
        private Camera cameraSource;

        [SerializeField]
        [ShowIf("source", CaptureSource.Camera)]
        [Indent]
        private LayerMask cullingMask;

        [SerializeField]
        [Range(0, 100)]
        private int quantity = 4;

        [SerializeField]
        private float initialDelay = 2f;

        [SerializeField]
        private float captureDelay = 2f;

        [SerializeField]
        private float endingDelay = 2f;

        [SerializeField]
        private string baseTextureName = "Capture";

        #endregion Variables

        #region Custom Events

        /// <summary>
        /// Callen when a photo is taken
        /// </summary>
        public UnityEvent_Texture2D OnCapture = new UnityEvent_Texture2D();

        /// <summary>
        /// Callen upon start captures
        /// </summary>
        public UnityEvent OnStarted = new UnityEvent();

        /// <summary>
        /// Callen upon finished
        /// </summary>
        public UnityEvent OnFinished = new UnityEvent();

        #endregion

        #region Properties (public)

        public float InitialDelay
        {
            get
            {
                return initialDelay;
            }

            set
            {
                initialDelay = value;
            }
        }

        public float CaptureDelay
        {
            get
            {
                return captureDelay;
            }

            set
            {
                captureDelay = value;
            }
        }

        public float EndingDelay
        {
            get
            {
                return endingDelay;
            }

            set
            {
                endingDelay = value;
            }
        }

        public int Quantity
        {
            get
            {
                return quantity;
            }

            set
            {
                quantity = value;
            }
        }

        public Webcam WebcamSource
        {
            get
            {
                return webcamSource;
            }

            set
            {
                webcamSource = value;
            }
        }

        public RenderTexture RenderTextureSource
        {
            get
            {
                return renderTextureSource;
            }

            set
            {
                renderTextureSource = value;
            }
        }

        public Camera CameraSource
        {
            get
            {
                return cameraSource;
            }

            set
            {
                cameraSource = value;
            }
        }

        public LayerMask CullingMask
        {
            get
            {
                return cullingMask;
            }

            set
            {
                cullingMask = value;
            }
        }

        #endregion Properties

        #region Event Functions

        #endregion Event Functions

        #region Methods

        /// <summary>
        /// Do the captures
        /// </summary>
        public void SetQuantity(int quantity)
        {
            Quantity = quantity;
        }

        /// <summary>
        /// Do the captures
        /// </summary>
        public void Capture()
        {
            StartCoroutine(CaptureLoop(0f));
        }

        /// <summary>
        /// Do the captures
        /// </summary>
        public void Capture(float delay)
        {
            StartCoroutine(CaptureLoop(delay));
        }

        /// <summary>
        /// Routine of capture loop
        /// </summary>
        private IEnumerator CaptureLoop(float delay)
        {
            if (delay > 0)
            {
                yield return new WaitForSeconds(delay);
            }

            // Delay
            if (InitialDelay > 0)
            {
                yield return new WaitForSeconds(InitialDelay);
            }

            // Start
            OnStarted.Invoke();

            for (int i = 0; i < quantity; i++)
            {
                yield return new WaitForEndOfFrame();

                Texture2D snap = null;

                // Webcam source
                if (source == CaptureSource.Webcam)
                {
                    snap = CaptureFromWebcam();
                }

                // Render texture source
                else if (source == CaptureSource.RenderTexture)
                {
                    snap = CaptureFromRenderTexture();
                }

                // Camera source
                else if (source == CaptureSource.Camera)
                {
                    snap = CaptureFromCamera();
                }

                // Screen source
                else if (source == CaptureSource.Screen)
                {
                    snap = CaptureFromScreen();
                }

                // Set texture name
                snap.name = baseTextureName;
                if (quantity > 1)
                {
                    snap.name += " " + i.ToString();
                }

                // Event
                OnCapture.Invoke(snap);

                // Wait for next capture?
                if (i < quantity - 1)
                {
                    yield return new WaitForSeconds(CaptureDelay);
                }
            }

            // Event
            if (EndingDelay > 0)
            {
                yield return new WaitForSeconds(EndingDelay);
            }

            OnFinished.Invoke();
        }

        private Texture2D CaptureFromWebcam()
        {
            return WebcamSource.TakeSnapshot();
        }

        private Texture2D CaptureFromRenderTexture()
        {
            Texture2D capture = null;
            RenderTextureSource.ToTexture2D(ref capture);
            return capture;
        }

        private Texture2D CaptureFromCamera()
        {
            Texture2D capture = null;
            CameraSource.ToTexture2D(cullingMask, ref capture);
            return capture;
        }

        private Texture2D CaptureFromScreen()
        {
            var capture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            capture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
            capture.Apply();
            return capture;
        }

        #endregion Methods
    }
}
