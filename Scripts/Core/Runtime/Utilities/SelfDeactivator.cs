﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

namespace CEUtilities
{
    [DefaultExecutionOrder(-500)]
    public class SelfDeactivator : MonoBehaviour
    {
        #region Enums

        public enum EventPipe
        {
            Awake,
            OnEnable,
            Start
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        private EventPipe executeOn = EventPipe.OnEnable;

        [SerializeField]
        private float delay = -1;

        #endregion Exposed fields

        #region Internal fields

        bool ticking = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        /// <summary>
        /// Delay to deactivation the game object
        /// </summary>
        public float Delay
        {
            get
            {
                return delay;
            }

            set
            {
                delay = value;
            }
        }

        /// <summary>
        /// At which momento we should hook
        /// </summary>
        public EventPipe ExecuteOn
        {
            get
            {
                return executeOn;
            }

            set
            {
                executeOn = value;
            }
        }

        #endregion Properties

        #region Unity events

        private void Awake()
        {
            if (ExecuteOn == EventPipe.Awake)
                StartDeactivation();
        }

        private void OnEnable()
        {
            if (ExecuteOn == EventPipe.OnEnable)
                StartDeactivation();
        }

        private void Start()
        {
            if (ExecuteOn == EventPipe.Start)
                StartDeactivation();
        }

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Start the deactivation countdown
        /// </summary>
        [LabelText("Start")]
        [HideIf("ticking")]
        [HideInEditorMode]
        [Button]
        public void StartDeactivation()
        {
            if (Delay > 0)
                StartCoroutine(_DeactivateRoutine());
            else
                Deactivate();
        }

        /// <summary>
        /// Stop the deactivation
        /// </summary>
        [ShowIf("ticking")]
        [HideInEditorMode]
        [Button]
        public void Stop()
        {
            if (ticking)
            {
                StopAllCoroutines();
                ticking = false;
            }
        }


        /// <summary>
        /// Deactivation routine
        /// </summary>
        private IEnumerator _DeactivateRoutine()
        {
            ticking = true;

            if (Delay > 0f)
                yield return new WaitForSeconds(Delay);

            Deactivate();
        }

        /// <summary>
        /// Deactivate this go
        /// </summary>
        private void Deactivate()
        {
            gameObject.SetActive(false);
        }

        #endregion Methods
    }
}