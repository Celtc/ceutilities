﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace CEUtilities
{
    public class ForceResolution : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private bool enable = true;

        [SerializeField]
        private int width;

        [SerializeField]
        private int height;

        [SerializeField]
        private bool fullScreen = false;

        [SerializeField]
        private bool readFromConfig = false;

        [SerializeField]
        [ShowIf("readFromConfig")]
        [InfoBox("Path is relative to the StreamingAssets folder.")]
        private string configFile = "resolution.config";

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public bool FullScreen
        {
            get
            {
                return fullScreen;
            }

            set
            {
                fullScreen = value;
            }
        }

        public int Width
        {
            get
            {
                return width;
            }

            set
            {
                width = value;
            }
        }

        public int Height
        {
            get
            {
                return height;
            }

            set
            {
                height = value;
            }
        }

        public string ConfigFile
        {
            get
            {
                return configFile;
            }

            set
            {
                configFile = value;
            }
        }

        public bool ReadFromConfig
        {
            get
            {
                return readFromConfig;
            }

            set
            {
                readFromConfig = value;
            }
        }

        public bool Enable
        {
            get
            {
                return enable;
            }

            set
            {
                enable = value;
            }
        }

        #endregion Properties

        #region Events methods

        protected virtual void Awake()
        {
            if (Enable)
            {
                if (ReadFromConfig)
                    ReadConfigFile();

                ForceRes(Width, Height, FullScreen);
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Read config file
        /// </summary>
        private void ReadConfigFile()
        {
            // Check for file
            var configPath = Application.streamingAssetsPath + "/" + ConfigFile;
            if (!File.Exists(configPath)) return;

            // Read content
            var sr = new StreamReader(configPath, Encoding.GetEncoding("iso-8859-1"));
            var entries = Regex.Split(sr.ReadToEnd(), "\r\n|\r|\n");
            sr.Close();
            sr.Dispose();
            sr = null;

            // Search configs
            string line;

            line = entries.FirstOrDefault(x => x.StartsWith("width"));
            if (!string.IsNullOrEmpty(line)) Width = int.Parse(line.Substring(line.IndexOf('=') + 1));

            line = entries.FirstOrDefault(x => x.StartsWith("height"));
            if (!string.IsNullOrEmpty(line)) Height = int.Parse(line.Substring(line.IndexOf('=') + 1));

            line = entries.FirstOrDefault(x => x.StartsWith("fullscreen"));
            if (!string.IsNullOrEmpty(line)) FullScreen = line.Substring(line.IndexOf('=') + 1).ToUpper() == "TRUE" ? true : false;
        }

        /// <summary>
        /// Force resolution
        /// </summary>
        private void ForceRes(int width, int height, bool fullscreen)
        {
            Screen.SetResolution(width, height, fullscreen);
        }

        #endregion Non Public Methods
    }
}