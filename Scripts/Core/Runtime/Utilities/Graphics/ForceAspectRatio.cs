﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities
{
    //
    // Resumen:
    //     ///
    //     Aspect ratio.
    //     ///
    public enum AspectRatio
    {
        AspectOthers = 0,
        Aspect4by3 = 1,
        Aspect5by4 = 2,
        Aspect16by10 = 3,
        Aspect16by9 = 4,
        Aspect3by4 = 5,
        Aspect4by5 = 6,
        Aspect10by16 = 7,
        Aspect9by16 = 8
    }

    public class ForceAspectRatio : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private AspectRatio aspectRatio;

        [SerializeField]
        private bool fullScreen = false;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public AspectRatio AspectRatio
        {
            get
            {
                return aspectRatio;
            }

            set
            {
                aspectRatio = value;
            }
        }

        public bool FullScreen
        {
            get
            {
                return fullScreen;
            }

            set
            {
                fullScreen = value;
            }
        }

        #endregion Properties

        #region Events methods

        protected virtual void Awake()
        {
            ForceAR(AspectRatio);
        }

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        private void ForceAR(AspectRatio ratio)
        {
            if (ratio == AspectRatio.AspectOthers) return;

            int maxWidth = 0;
            int maxHeight = 0;
            var resolutions = Screen.resolutions;
            if ((int)ratio > 4)
            {
                foreach (var res in resolutions)
                    if (res.height > maxHeight)
                    {
                        maxHeight = res.height;
                        maxWidth = res.width;
                    }

                switch(ratio)
                {
                    case AspectRatio.Aspect3by4: maxWidth = maxHeight * 3 / 4; break;
                    case AspectRatio.Aspect4by5: maxWidth = maxHeight * 4 / 5; break;
                    case AspectRatio.Aspect10by16: maxWidth = maxHeight * 10 / 16; break;
                    case AspectRatio.Aspect9by16: maxWidth = maxHeight * 9 / 16; break;
                }
            }
            else if ((int)ratio > 0)
            {
                foreach (var res in resolutions)
                    if (res.width > maxWidth)
                    {
                        maxHeight = res.height;
                        maxWidth = res.width;
                    }

                switch (ratio)
                {
                    case AspectRatio.Aspect4by3: maxWidth = maxHeight * 4 / 3; break;
                    case AspectRatio.Aspect5by4: maxWidth = maxHeight * 5 / 4; break;
                    case AspectRatio.Aspect16by10: maxWidth = maxHeight * 16 / 10; break;
                    case AspectRatio.Aspect16by9: maxWidth = maxHeight * 16 / 9; break;
                }
            }

            Screen.SetResolution(maxWidth, maxHeight, FullScreen);
        }

        #endregion Non Public Methods
    }
}