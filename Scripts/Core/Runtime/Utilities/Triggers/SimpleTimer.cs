﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

using Sirenix.OdinInspector;
using System;

namespace CEUtilities.Triggers
{
	public class SimpleTimer : MonoBehaviour
	{
        #region Exposed fields

        [SerializeField]
        private bool runOnStart = false;

        [SerializeField]
        private bool runOnEnable = false;

        [SerializeField]
		private float time = 60f;

        [SerializeField]
        private float delay = 0f;

        [ShowInInspector]
        [ReadOnly]
		private float timeLeft;
		private bool running;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        public UnityEvent OnTimeRun = new UnityEvent();

        public UnityEvent OnTimeUp = new UnityEvent();

		public UnityEvent OnTimePaused = new UnityEvent();

		public UnityEvent_Float OnTimeUpdate = new UnityEvent_Float();

		public UnityEvent_Float OnTimeUpdateNormalize = new UnityEvent_Float();

        #endregion Custom Events

        #region Properties

        public bool RunOnEnable
        {
            get
            {
                return runOnEnable;
            }

            set
            {
                runOnEnable = value;
            }
        }

        public bool RunOnStart
        {
            get
            {
                return runOnStart;
            }

            set
            {
                runOnStart = value;
            }
        }

        public float Time
		{
			get
			{
				return time;
			}

			set
			{
				time = value;

				Restart();
			}
        }

        public float Delay
        {
            get
            {
                return delay;
            }

            set
            {
                delay = value;
            }
        }

        public float TimeLeft
		{
			get
			{
				return timeLeft;
			}

			protected set
			{
				if (timeLeft == value)
					return;

				timeLeft = value;

				OnTimeUpdate.Invoke(TimeLeft);
				OnTimeUpdateNormalize.Invoke(1 - (TimeLeft / Time));
			}
		}

		public bool Running
		{
			get
			{
				return running;
			}

			protected set
			{
				running = value;
			}
		}

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            TimeLeft = Time;

            if (!Running && RunOnEnable)
            {
                if (TimeLeft <= 0)
                {
                    OnTimeUp.Invoke();
                }
                else
                {
                    Run(Delay);
                }
            }
        }

        private void OnDisable()
        {
            if (Running)
            {
                Pause();
                Restart();
            }
        }

        private void Start()
		{
            if (!Running && RunOnStart)
            {
                if (TimeLeft <= 0)
                {
                    OnTimeUp.Invoke();
                }
                else
                {
                    Run(Delay);
                }
            }
		}

		private void Update()
		{
			if (Running)
			{
				if (TimeLeft >= 0)
				{
					TimeLeft -= UnityEngine.Time.deltaTime;
				}
				else
				{
					Running = false;

					OnTimeUp.Invoke();
				}
			}
		}

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Runs the specified delay.
        /// </summary>
        /// <param name="delay">The delay.</param>
        public void Run(float delay)
        {
            StartCoroutine(_Run(delay));
        }

		/// <summary>
		/// Run the timer. Will not reset the time left.
		/// </summary>
		public void Run()
		{
            if (!Running)
            {
                StopAllCoroutines();

                Running = true;

                OnTimeRun.Invoke();
            }
        }

		/// <summary>
		/// Pause the timer
		/// </summary>
		public void Pause()
        {
            if (Running)
            {
                StopAllCoroutines();

                Running = false;

                OnTimePaused.Invoke();
            }
		}

        /// <summary>
        /// Stops the timer (pause and restart).
        /// </summary>
        public void Stop()
        {
            StopAllCoroutines();

            Pause();

            Stop();
        }

		/// <summary>
		/// Restart the time left. Will not run the timer if not running
		/// </summary>
		public void Restart()
        {
            StopAllCoroutines();

            TimeLeft = Time;
		}

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Runs the specified delay.
        /// </summary>
        /// <param name="delay">The delay.</param>
        /// <returns></returns>
        private IEnumerator _Run(float delay)
        {
            if (delay > 0)
            {
                yield return new WaitForSeconds(delay);
            }

            Run();
        }

		#endregion Methods
	}
}