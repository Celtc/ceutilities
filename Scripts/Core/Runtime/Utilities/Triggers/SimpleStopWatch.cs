﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

using Sirenix.OdinInspector;

namespace CEUtilities.Triggers
{
    public class SimpleStopWatch : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private bool runOnStart = false;

        [SerializeField]
        [HideInInspector]
        private bool runOnEnable = false;

        [SerializeField]
        [HideInInspector]
        private float delay = 0f;


        private float time;

        private bool running;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether [run on enable].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [run on enable]; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup", order: 0)]
        public bool RunOnEnable
        {
            get
            {
                return runOnEnable;
            }

            set
            {
                runOnEnable = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [run on start].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [run on start]; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public bool RunOnStart
        {
            get
            {
                return runOnStart;
            }

            set
            {
                runOnStart = value;
            }
        }

        /// <summary>
        /// Gets or sets the delay.
        /// </summary>
        /// <value>
        /// The delay.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public float Delay
        {
            get
            {
                return delay;
            }

            set
            {
                delay = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="SimpleStopWatch"/> is running.
        /// </summary>
        /// <value>
        ///   <c>true</c> if running; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Runtime", order: 3)]
        [ReadOnly]
        public bool Running
        {
            get
            {
                return running;
            }

            protected set
            {
                running = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        [ReadOnly]
        public float Time
        {
            get
            {
                return time;
            }

            set
            {
                if (time != value)
                {
                    time = value;

                    OnTimeUpdate.Invoke(Time);
                }
            }
        }

        #endregion Properties

        #region Custom Events

        [ShowInInspector]
        [FoldoutGroup("Events", order: 2)]
        public UnityEvent OnTimeRun = new UnityEvent();

        [ShowInInspector]
        [FoldoutGroup("Events")]
        public UnityEvent_Float OnTimePaused = new UnityEvent_Float();

        [ShowInInspector]
        [FoldoutGroup("Events")]
        public UnityEvent OnTimeRestart = new UnityEvent();

        [ShowInInspector]
        [FoldoutGroup("Events")]
        public UnityEvent_Float OnTimeUpdate = new UnityEvent_Float();

        #endregion Custom Events

        #region Events methods

        private void OnEnable()
        {
            Restart();

            if (!Running && RunOnEnable)
            {
                Run(Delay);
            }
        }

        private void OnDisable()
        {
            if (Running)
            {
                Pause();

                Restart();
            }
        }

        private void Start()
        {
            if (!Running && RunOnStart)
            {
                Run(Delay);
            }
        }

        private void Update()
        {
            if (Running)
            {
                Time += UnityEngine.Time.deltaTime;
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Runs the specified delay.
        /// </summary>
        /// <param name="delay">The delay.</param>
        public void Run(float delay)
        {
            if (delay <= 0)
            {
                Run();
            }
            else if (!Running)
            {
                StartCoroutine(_Run(delay));
            }
        }

        /// <summary>
        /// Run the timer. Will not reset the time left.
        /// </summary>
        public void Run()
        {
            if (!Running)
            {
                StopAllCoroutines();

                Running = true;

                OnTimeRun.Invoke();
            }
        }

        /// <summary>
        /// Pause the timer
        /// </summary>
        public void Pause()
        {
            if (Running)
            {
                StopAllCoroutines();

                Running = false;

                OnTimePaused.Invoke(Time);
            }
        }

        /// <summary>
        /// Restart the time left. Will not run the timer if not running
        /// </summary>
        public void Restart()
        {
            StopAllCoroutines();

            Time = 0f;

            OnTimeRestart.Invoke();
        }

        /// <summary>
        /// Stops the timer (pause and restart).
        /// </summary>
        public void Stop()
        {
            StopAllCoroutines();

            Pause();

            Restart();
        }

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Runs the specified delay.
        /// </summary>
        /// <param name="delay">The delay.</param>
        /// <returns></returns>
        private IEnumerator _Run(float delay)
        {
            yield return new WaitForSeconds(delay);

            Run();
        }

        #endregion Methods
    }
}