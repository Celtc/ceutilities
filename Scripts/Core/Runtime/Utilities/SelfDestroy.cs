﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities
{
    public class SelfDestroy : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private bool startOnEnable = true;

        [SerializeField]
        private float delay = -1;

        #endregion Exposed fields

        #region Internal fields

        bool ticking = false;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        /// <summary>
        /// Delay to destroy the game object
        /// </summary>
        public float Delay
        {
            get
            {
                return delay;
            }

            set
            {
                delay = value;
            }
        }

        /// <summary>
        /// Should start destruction on component enable?
        /// </summary>
        public bool StartOnEnable
        {
            get
            {
                return startOnEnable;
            }

            set
            {
                startOnEnable = value;
            }
        }

        #endregion Properties

        #region Unity events

        private void OnEnable()
        {
            if (StartOnEnable)
            {
                StartCoroutine(_DestroyRoutine());
            }
        }

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Start the destruction coutdown
        /// </summary>
        [LabelText("Start")]
        [HideIf("ticking")]
        [HideInEditorMode]
        [Button]
        public void StartDestruction()
        {
            StartCoroutine(_DestroyRoutine());
        }

        /// <summary>
        /// Stop the destruction
        /// </summary>
        [ShowIf("ticking")]
        [HideInEditorMode]
        [Button]
        public void Stop()
        {
            if (ticking)
            {
                StopAllCoroutines();
                ticking = false;
            }
        }


        /// <summary>
        /// Destruction routine
        /// </summary>
        /// <returns></returns>
        private IEnumerator _DestroyRoutine()
        {
            ticking = true;

            if (Delay > 0f)
            {
                yield return new WaitForSeconds(Delay);
            }

            Utilities.Destroy(gameObject);
        }

        #endregion Methods
    }
}