﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Globalization;

using Sirenix.OdinInspector;
using Sirenix.Utilities;
using CEUtilities.UtilityClasses;

#if UNITY_EDITOR
using UnityEditor;
using Sirenix.OdinInspector.Editor;
#endif

namespace CEUtilities.Localization
{
#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    [HideMonoScript]
    [DefaultExecutionOrder(-500)]
    [GlobalConfig("Config/Resources")]
    public class LocalizationManager : GlobalConfig<LocalizationManager>
    {
        #region Static

        /// <summary>
        /// The native culture code.
        /// </summary>
        public static readonly int NATIVE_LANGUAGE_ID = new CultureInfo("es-AR").LCID;

#if UNITY_EDITOR

        [MenuItem("Tools/Localization Manager")]
        public static void Open()
        {
            var window = OdinEditorWindow.InspectObject(Instance);
            window.position = new Rect
            (
                Screen.currentResolution.width * .5f - 240f,
                Screen.currentResolution.height * .5f - 300f,
                480f,
                600f
            );
        }

#endif

        #endregion

        #region Enums

        #endregion

        #region Structs

        /// <summary>
        /// Auxiliar struct for showing the search result.
        /// </summary>
        private class TextLocalizationSearchResult
        {
            [ShowInInspector]
            [TextArea(20, 60)]
            public string result;

            /// <summary>
            /// Copies to clipboard.
            /// </summary>
            [Button]
            public void CopyToClipboard()
            {
                CEUtilities.Helpers.ApplicationHelper.Clipboard = result;
            }
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private bool enable;

        [SerializeField]
        [HideInInspector]
        private int selectedLanguage = NATIVE_LANGUAGE_ID;

        [SerializeField]
        [HideInInspector]
        private string translationsTemplate = "StreamingAssets/Translation_??-??.xml";

        #endregion Exposed fields

        #region Internal fields

        private LayeredDictionary<string, string> localizations = new LayeredDictionary<string, string>() { CreateLayersOnDemand = true };

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FileLogger"/> is enable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enable; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [ToggleButton("On / Off")]
        [GUIColorIf("enable", .25f, 1f, .25f, 1f, 1f, .25f, .25f, 1f)]
        public bool Enable
        {
            get
            {
                return enable;
            }

            set
            {
                enable = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected language.
        /// </summary>
        /// <value>
        /// The selected language.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [ValueDropdown("Languages")]
        [InlineButton("SelectLanguage", "+")]
        public int SelectedLanguage
        {
            get
            {
                return selectedLanguage;
            }

            private set
            {
                selectedLanguage = value;
            }
        }

        /// <summary>
        /// Gets or sets the translations template.
        /// </summary>
        /// <value>
        /// The translations template.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public string TranslationsTemplate
        {
            get
            {
                return translationsTemplate;
            }

            set
            {
                translationsTemplate = value;
            }
        }


        /// <summary>
        /// Gets the current translations.
        /// </summary>
        /// <value>
        /// The current translations.
        /// </value>
        public Dictionary<string, string> CurrentTranslations => localizations[SelectedLanguage];

        /// <summary>
        /// Gets the native translations.
        /// </summary>
        /// <value>
        /// The native translations.
        /// </value>
        public Dictionary<string, string> NativeTranslations => localizations[NATIVE_LANGUAGE_ID];


        /// <summary>
        /// Gets the number of supported languages.
        /// </summary>
        /// <value>
        /// The number of supported languages.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        public int NumberOfSupportedLanguages => localizations.CountLayers();

#if UNITY_EDITOR

        /// <summary>
        /// Gets the languages printable codes.
        /// </summary>
        /// <value>
        /// The languages.
        /// </value>
        private ValueDropdownItem<int>[] Languages => CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures).Select(x => new ValueDropdownItem<int>(x.DisplayName, x.LCID)).ToArray();

#endif

        #endregion Properties

        #region Custom Events

        /// <summary>
        /// The on language change.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Events", order: 1)]
        public UnityEvent_Int OnLanguageChange = new UnityEvent_Int();

        #endregion Custom Events

        #region Events 

#if UNITY_EDITOR

        /// <summary>
        /// Called when [method load].
        /// </summary>
        [InitializeOnLoadMethod]
        public static void OnEditorLoadHandler()
        {
            LoadInstanceIfAssetExists();
        }

#endif

        /// <summary>
        /// Called when [runtime method load].
        /// </summary>
        [RuntimeInitializeOnLoadMethod]
        private static void OnRuntimeLoadHandler()
        {
            LoadInstanceIfAssetExists();
            if (HasInstanceLoaded && Instance.Enable)
            {
                Instance.LoadTranslations();
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Changes to language.
        /// </summary>
        public void ChangeToLanguage(int LCID)
        {
            var cultureInfo = new CultureInfo(LCID);
            if (cultureInfo != null)
            {
                SelectedLanguage = cultureInfo.LCID;

                OnLanguageChange.Invoke(SelectedLanguage);
            }
            else
            {
                Debug.LogErrorFormat(this, "[LocalizatioManager] Couldn't resolve language from LCID '{0}'.", LCID);
            }
        }

        /// <summary>
        /// Changes to language.
        /// </summary>
        public void ChangeToLanguage(string languageCode)
        {
            var cultureInfo = new CultureInfo(languageCode);
            if (cultureInfo != null)
            {
                SelectedLanguage = cultureInfo.LCID;

                OnLanguageChange.Invoke(SelectedLanguage);
            }
            else
            {
                Debug.LogErrorFormat(this, "[LocalizatioManager] Couldn't resolve language from language code '{0}'.", languageCode);
            }
        }


        /// <summary>
        /// Gets the translations for the native language.
        /// </summary>
        public Dictionary<string, string> GetNativeTranslations() => GetTranslations(NATIVE_LANGUAGE_ID);

        /// <summary>
        /// Gets the translations.
        /// </summary>
        /// <param name="languageId">The language code.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetTranslations() => GetTranslations(SelectedLanguage);

        /// <summary>
        /// Gets the translations.
        /// </summary>
        /// <param name="languageId">The language code.</param>
        /// <returns></returns>
        public Dictionary<string, string> GetTranslations(int languageId)
        {
            Dictionary<string, string> translations;
            if (localizations.HasLayer(languageId))
            {
                translations = localizations[languageId];
            }
            else
            {
                translations = new Dictionary<string, string>();

                Debug.LogWarningFormat("[LocalizationManager] Trying to get translation for language id '{0}' not supported.", languageId);
            }
            return translations;
        }


        /// <summary>
        /// Determines whether [has native translation] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if [has native translation] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public bool HasNativeTranslation(string key) => HasTranslation(NATIVE_LANGUAGE_ID, key);

        /// <summary>
        /// Determines whether the specified key has translation.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if the specified key has translation; otherwise, <c>false</c>.
        /// </returns>
        public bool HasTranslation(string key) => HasTranslation(SelectedLanguage, key);

        /// <summary>
        /// Determines whether the specified language identifier has translation.
        /// </summary>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if the specified language identifier has translation; otherwise, <c>false</c>.
        /// </returns>
        public bool HasTranslation(int languageId, string key)
        {
            // Look in specified language
            var translations = GetTranslations(languageId);
            return translations.ContainsKey(key);
        }


        /// <summary>
        /// Gets the translation.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetNativeTranslation(string key) => GetTranslation(NATIVE_LANGUAGE_ID, key);

        /// <summary>
        /// Gets the translation.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string GetTranslation(string key) => GetTranslation(SelectedLanguage, key);

        /// <summary>
        /// Gets the translation.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public string GetTranslation(int languageId, string key)
        {
            // Look in specified language
            var translations = GetTranslations(languageId);
            if (translations.ContainsKey(key))
            {
                return translations[key];
            }

            // Look in native language
            else if (languageId != NATIVE_LANGUAGE_ID && NativeTranslations.ContainsKey(key))
            {
                Debug.LogWarningFormat(this, "[LocalizationManager] The language '{0}' does not include a translation for '{1}', using value of native language.", languageId, key);

                return NativeTranslations[key];
            }

            // Not existing
            else
            {
                Debug.LogErrorFormat(this, "[LocalizationManager] Couldn't find a translation for '{0}'.", key);

                return "Undefined";
            }
        }


        /// <summary>
        /// Sets the translation for.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public void SetNativeTranslation(string key, string value, bool overrideExisting = true) => SetTranslation(NATIVE_LANGUAGE_ID, key, value, overrideExisting);

        /// <summary>
        /// Sets the translation for.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public void SetTranslation(string key, string value, bool overrideExisting = true) => SetTranslation(SelectedLanguage, key, value, overrideExisting);

        /// <summary>
        /// Sets the translation for.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public void SetTranslation(int languageId, string key, string value, bool overrideExisting = true)
        {
            var translations = GetTranslations(languageId);
            if (!translations.ContainsKey(key) || overrideExisting)
            {
                translations.SetOrAdd(key, value);
            }
        }


        /// <summary>
        /// Reloads the translations.
        /// </summary>
        [Button(ButtonSizes.Medium)]
        [PropertyOrder(9)]
        public void ReloadTranslations()
        {
            // Clear current
            localizations = new LayeredDictionary<string, string>() { CreateLayersOnDemand = true };

            // Loads
            LoadTranslations();
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Loads the translations.
        /// </summary>
        private void LoadTranslations()
        {
            // Get final template filepath
            var filepath = Path.Combine(Application.streamingAssetsPath, TranslationsTemplate);

            // Check folder path
            var path = Path.GetDirectoryName(filepath);
            if (Directory.Exists(path))
            {
                // Read all files from folder
                var files = Directory.GetFiles(path, "*.txt");
                if (files.Length > 0)
                {
                    // Get the code index in template
                    var codeIndex = Path.GetFileNameWithoutExtension(TranslationsTemplate).IndexOf("??-??");
                    if (codeIndex >= 0)
                    {
                        // Load every file
                        foreach (var file in files)
                        {
                            // Extract language ID from filename
                            var languageId = LanguageIdFromFile(file, codeIndex);
                            if (languageId.HasValue)
                            {
                                LoadTranslationFile(file, languageId.Value);
                            }
                            else
                            {
                                Debug.LogErrorFormat(this, "[LocalizatioManager] The translation file '{0}' has an invalid language code.", Path.GetFileName(file));
                            }
                        }
                    }
                    else
                    {
                        Debug.LogErrorFormat(this, "[LocalizatioManager] The template name '{0}' does not include the language code ??-??.", TranslationsTemplate);
                    }
                }
            }
            else
            {
                Debug.LogErrorFormat(this, "[LocalizatioManager] The path '{0}' for reading the localizations was not found.", path);
            }
        }

        /// <summary>
        /// Loads the translation file.
        /// </summary>
        /// <param name="filepath">The filepath.</param>
        private void LoadTranslationFile(string filepath, int languageId)
        {
            try
            {
                var lines = File.ReadAllLines(filepath);
                var translations = new Dictionary<string, string>(lines.Length);
                foreach (var line in lines)
                {
                    var parse = ParseTranslationLine(line);
                    if (parse != null)
                    {
                        translations.Add(parse.Item1, parse.Item2);
                    }
                }
                localizations.SetLayer(languageId, translations);
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat(this, "[LocalizatioManager] Error while trying to read translation file '{0}': {1}", filepath, e.Message);
            }
        }

        /// <summary>
        /// Parses the translation line.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns></returns>
        private Tuple<string, string> ParseTranslationLine(string line)
        {
            Tuple<string, string> result = null;
            var equalIndex = line.IndexOf('=');
            if (equalIndex > 0)
            {
                result = new Tuple<string, string>(line.Substring(0, equalIndex), line.Substring(equalIndex + 1));
            }
            return result;
        }

        /// <summary>
        /// Languages the code from file.
        /// </summary>
        /// <returns></returns>
        private int? LanguageIdFromFile(string filepath, int codeIndex)
        {
            int? result = null;
            var languageCode = Path.GetFileNameWithoutExtension(filepath).Substring(codeIndex, 5);
            result = new CultureInfo(languageCode)?.LCID;
            return result;
        }

#if UNITY_EDITOR

        /// <summary>
        /// Selects the language.
        /// </summary>
        private void SelectLanguage()
        {
            var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);
            CEUtilities.Helpers.ObjectPicker<CultureInfo>.Show(
                "Select Language",
                true,
                cultures,
                (selected) => SelectedLanguage = selected.LCID,
                (element) => string.Format($"{element.DisplayName} [{element.Name}]"),
                () => { }
            );
        }

        /// <summary>
        /// Searches all text localizations.
        /// </summary>
        [Button(ButtonSizes.Medium)]
        [PropertyOrder(10)]
        private void SearchAllTextLocalizations()
        {
            //var localizations = FindObjectsOfType<LocalizationText>();
            var localizations = Resources.FindObjectsOfTypeAll<LocalizationText>();
            var stringBuilder = new StringBuilder();
            foreach (var localization in localizations)
            {
                if (localization.gameObject.IsSceneObject())
                {
                    stringBuilder.AppendLine(string.Format("{0}={1}", localization.Id, localization.Text));
                }
            }
            var result = new TextLocalizationSearchResult() { result = stringBuilder.ToString() };
            Sirenix.OdinInspector.Editor.OdinEditorWindow.InspectObject(result);
        }

#endif

        #endregion Non Public Methods
    }
}