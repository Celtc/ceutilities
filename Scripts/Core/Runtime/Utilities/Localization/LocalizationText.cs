﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

#if TMP
using TMPro;
#endif

namespace CEUtilities.Localization
{
    [ExecuteInEditMode]
    public class LocalizationText : MonoBehaviour, ISerializationCallbackReceiver
    {
        #region Enums

        public enum TextType
        {
            None,
            TextMeshPro,
            TextUnity
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private string id = null;

        #endregion Exposed fields

        #region Internal fields

        private TextType type = TextType.None;

        private Text textComponent;

#if TMP
        private TMP_Text textMeshProComponent;
#endif

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [ShowInInspector]
        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public TextType Type
        {
            get
            {
#if TMP
                if (type == TextType.None || (textMeshProComponent == null && textComponent == null))
#endif
                if (type == TextType.None || textComponent == null)
                {
                    var graphic = GetComponent<MaskableGraphic>();
                    if (graphic != null)
                    {

                        // Is text
                        if (graphic is Text)
                        {
                            textComponent = graphic as Text;
                            type = TextType.TextUnity;
                        }

#if TMP
                        // Is text mesh pro
                        else if (graphic is TMP_Text)
                        {
                            textMeshProComponent = graphic as TMP_Text;
                            type = TextType.TextMeshPro;
                        }
#endif
                    }
                }
                return type;
            }
        }

        /// <summary>
        /// Gets or sets the localized text.
        /// </summary>
        /// <value>
        /// The localized text.
        /// </value>
        public string Text
        {
            get
            {
                switch (Type)
                {
#if TMP
                    case TextType.TextMeshPro: return textMeshProComponent.text;
#endif

                    case TextType.TextUnity: return textComponent.text;

                    default: return null;
                }
            }

            set
            {
                switch (Type)
                {
#if TMP
                    case TextType.TextMeshPro: textMeshProComponent.text = value; break;
#endif

                    case TextType.TextUnity: textComponent.text = value; break;
                }
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                if (LocalizationManager.HasInstanceLoaded)
                {
                    LocalizationManager.Instance.OnLanguageChange.AddListener(OnLanguangeChangeHandler);
                }

                TranslateText();
            }
        }

        private void OnDisable()
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                if (LocalizationManager.HasInstanceLoaded)
                {
                    LocalizationManager.Instance.OnLanguageChange.RemoveListener(OnLanguangeChangeHandler);
                }
            }
        }

        /// <summary>
        /// Called when [languange change handler].
        /// </summary>
        /// <param name="code">The code.</param>
        private void OnLanguangeChangeHandler(int code)
        {
            TranslateText();
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
#if UNITY_EDITOR
            if (string.IsNullOrEmpty(Id))
            {
                SetDefaultId();
            }
#endif
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
#if UNITY_EDITOR
            if (string.IsNullOrEmpty(Id))
            {
                SetDefaultId();
            }
#endif
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Refreshes the text.
        /// </summary>
        public void TranslateText()
        {
            if (LocalizationManager.HasInstanceLoaded)
            {
                Text = LocalizationManager.Instance.GetTranslation(id);
            }
        }

        #endregion Public Methods

        #region Non Public Methods

#if UNITY_EDITOR

        /// <summary>
        /// Sets the default identifier.
        /// </summary>
        [Button]
        private void SetDefaultId()
        {
            var path = transform.GetHierarchyPath("/").Replace(" ", string.Empty);
            var pathNodes = path.Split('/');
            var finalPath = string.Format("{0}.{1}", pathNodes[0], GenerateRandomId());
            if (pathNodes.Length > 1)
            {
                var minIndex = pathNodes.Length - Mathf.Min(3, pathNodes.Length - 1);
                for (int i = minIndex; i < pathNodes.Length; i++)
                {
                    finalPath = string.Join(".", finalPath, pathNodes[i]);
                }
            }
            id = finalPath;
        }

        /// <summary>
        /// Generates the random identifier.
        /// </summary>
        private string GenerateRandomId()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[6];
            //var random = new System.Random();
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[UnityEngine.Random.Range(0, chars.Length)];
            }
            var finalString = new string(stringChars);
            return finalString;
        }

#endif

        #endregion Non Public Methods
    }
}
