﻿using UnityEngine;
using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;

#if UNITY_EDITOR
using UnityEditor;
using Sirenix.OdinInspector.Editor;
#endif

using Sirenix.OdinInspector;
using Sirenix.Utilities;
using Sirenix.Serialization;
using CEUtilities.Helpers;
using CEUtilities.Patterns;
using CEUtilities.Threading;

namespace CEUtilities.Logging
{
#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    [HideMonoScript]
    [GlobalConfig("Config/Resources")]
    public class FileLogger : GlobalConfig<FileLogger>, ISerializationCallbackReceiver
    {
        #region Static

        private readonly static Regex traceLinesSplit = new Regex(@"\r\n|\r|\n", RegexOptions.Compiled);
        private readonly static Regex traceLineParse = new Regex(@"([^:]*):([^)]*\))(?:(?:\s.*\s)?(.*)?(?::)+(\d*)?)?", RegexOptions.Compiled);

        /// <summary>
        /// Splits the stack trace.
        /// </summary>
        /// <param name="stackTrace">The stack trace.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string[] SplitStackTrace(string stackTrace)
        {
            return traceLinesSplit.Split(stackTrace);
        }

        /// <summary>
        /// Parses the trace.
        /// </summary>
        /// <param name="traceLine">The trace line.</param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static GroupCollection ParseTrace(string traceLine)
        {
            return traceLineParse.Match(traceLine).Groups;
        }

        /// <summary>
        /// The ignored class members.
        /// </summary>
        private static readonly HashSet<string> ignoredClassMembers = new HashSet<string>()
        {
            "CEUtilities.Logging.FileLogger:MessageReceivedHandler",

            "UnityEngine.Application:CallLogCallback",

            "UnityEngine.DebugLogHandler:Internal_Log",
            "UnityEngine.DebugLogHandler:LogFormat",

            "UnityEngine.Logger:Log",
            "UnityEngine.Logger:LogError",
            "UnityEngine.Logger:LogException",
            "UnityEngine.Logger:LogWarning",
            "UnityEngine.Logger:LogFormat",

            "UnityEngine.Debug:LogException",
            "UnityEngine.Debug:Log",
            "UnityEngine.Debug:LogFormat",
            "UnityEngine.Debug:LogWarning",
            "UnityEngine.Debug:LogWarningFormat",
            "UnityEngine.Debug:LogError",
            "UnityEngine.Debug:LogErrorFormat",
            "UnityEngine.Debug:LogAssertion",
            "UnityEngine.Debug:LogAssertionFormat",
            "UnityEngine.Debug:Assert",
            "UnityEngine.Debug:AssertFormat",
        };

        /// <summary>
        /// The tokens mapper.
        /// </summary>
        private static readonly Dictionary<string, string> tokensMapper = new Dictionary<string, string>()
        {
            { "date", "0" },
            { "productName", "1" },
            { "companyName", "2" },
            { "version", "3" },
            { "identifier", "4" },
            { "buildGUID", "5" },
            { "genuine", "6"},
            { "platform", "7" },
            { "environment", "8" },
            { "unityVersion", "9" },
            { "temporaryCachePath", "10" },
            { "persistentDataPath", "11" },
            { "streamingAssetsPath", "12" },
            { "dataPath", "13" },
            { "classname", "14" },
            { "member", "15" },
            { "memberParameterless", "16" },
            { "file", "17" },
            { "line", "18" },
            { "type", "19" },
            { "log", "20" },
            { "newline", "21" },
        };

#if UNITY_EDITOR

        [MenuItem("Tools/File Logger")]
        public static void Open()
        {
            var window = OdinEditorWindow.InspectObject(Instance);
            window.position = new Rect
            (
                Screen.currentResolution.width * .5f - 240f,
                Screen.currentResolution.height * .5f - 300f,
                480f,
                600f
            );
        }

#endif

        #endregion

        #region Subclasses

        [CustomSingleton(AutoGeneration = true, Persistent = true, HideLogs = true, Flags = HideFlags.HideAndDontSave)]
        private class ApplicationStateTracker : Singleton<ApplicationStateTracker>
        {
            #region Static

            /// <summary>
            /// Occurs when [quitting].
            /// </summary>
            public static event Action Quitting
            {
                add
                {
                    lock (Instance)
                    {
                        Instance._Quitting += value;
                    }
                }

                remove
                {
                    lock (Instance)
                    {
                        Instance._Quitting -= value;
                    }
                }
            }

            #endregion

            #region Exposed fields

            #endregion Exposed fields

            #region Internal fields

            private event Action _Quitting;

            #endregion Internal fields

            #region Custom Events

            #endregion Custom Events

            #region Properties

            #endregion Properties

            #region Events methods

            /// <summary>
            /// Occurs when [quitting the application].
            /// </summary>
            protected override void OnApplicationQuit()
            {
                base.OnApplicationQuit();

                _Quitting?.Invoke();
            }

            #endregion Events methods

            #region Public Methods

            #endregion Public Methods

            #region Non Public Methods

            #endregion Non Public Methods
        }

        #endregion

        #region Structs

        private struct StackLevelMetadata
        {
            /// <summary>
            /// The classname
            /// </summary>
            public string classname;

            /// <summary>
            /// The member
            /// </summary>
            public string member;

            /// <summary>
            /// The member
            /// </summary>
            public string memberParameterless;

            /// <summary>
            /// The file
            /// </summary>
            public string file;

            /// <summary>
            /// The line
            /// </summary>
            public string line;
        }

        private struct LogContext
        {
            /// <summary>
            /// Valid.
            /// </summary>
            public readonly bool valid;

            /// <summary>
            /// The type.
            /// </summary>
            public readonly LogType type;

            /// <summary>
            /// The stack trace.
            /// </summary>
            public readonly string stackTrace;

            /// <summary>
            /// The levels metadata.
            /// </summary>
            public readonly List<StackLevelMetadata> levelsMetadata;

            /// <summary>
            /// Gets the first level.
            /// </summary>
            /// <value>
            /// The first level.
            /// </value>
            public StackLevelMetadata FirstLevel => levelsMetadata[0];

            /// <summary>
            /// Gets the type of the printable.
            /// </summary>
            /// <value>
            /// The type of the printable.
            /// </value>
            public string PrintableType
            {
                get
                {
                    switch (type)
                    {
                        case LogType.Error: return "ERROR";
                        case LogType.Warning: return "WARNING";
                        case LogType.Exception: return "EXCEPTION";
                        default: return "INFO";
                    }
                }
            }

            public LogContext(LogType type) : this(type, null, null)
            {
                //
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="LogContext"/> struct.
            /// </summary>
            /// <param name="stacktrace">The stacktrace.</param>
            public LogContext(LogType type, string stackTrace, HashSet<string> ignoredStacks)
            {
                this.type = type;
                this.stackTrace = string.Empty;
                levelsMetadata = new List<StackLevelMetadata>();

                // Only if stack is present
                if (!string.IsNullOrEmpty(stackTrace))
                {
                    // Foreach stack
                    foreach (var line in SplitStackTrace(stackTrace))
                    {
                        // Parse stack
                        var elements = ParseTrace(line);
                        var level = new StackLevelMetadata
                        {
                            classname = elements.Count > 1 ? elements[1].Value : string.Empty,
                            member = elements.Count > 2 ? elements[2].Value : string.Empty,
                            memberParameterless = elements.Count > 2 ? elements[2].Value.GetUntilOrEmpty("(") : string.Empty,
                            file = elements.Count > 3 ? elements[3].Value : string.Empty,
                            line = elements.Count > 4 ? elements[4].Value : string.Empty
                        };

                        // Ignore this stack level, and all related to Unity logging
                        if (!ignoredStacks.Contains(string.Join(":", level.classname, level.memberParameterless)))
                        {
                            levelsMetadata.Add(level);
                            this.stackTrace = string.Join("\r\n", this.stackTrace, line);
                        }
                    }

                    // Clear stack trace start
                    if (!string.IsNullOrEmpty(this.stackTrace))
                    {
                        this.stackTrace = this.stackTrace.Substring(2);
                    }

                    // Flag
                    valid = true;
                }
                else
                {
                    valid = false;
                }
            }
        }

        #endregion Structs

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private bool enable;

        [SerializeField]
        [HideInInspector]
        private bool logInEditor = true;

        [SerializeField]
        [HideInInspector]
        private bool hideDevelopmentConsole = true;

#pragma warning disable 414

        [SerializeField]
        [HideInInspector]
        private string standaloneFilepathTemplate = "./Logs/log_{date:yyyyMMddHHmmss}.log";

        [SerializeField]
        [HideInInspector]
        private string mobileFilepathTemplate = "/sdcard/Logs/log_{date:yyyyMMddHHmmss}.log";

#pragma warning restore 414

        [SerializeField]
        [HideInInspector]
        private string releaseEntryTemplate = "{date:HH:mm:ss}: {type}: [{classname}:{memberParameterless}] {log}";

        [SerializeField]
        [HideInInspector]
        private string debugEntryTemplate = "{date:HH:mm:ss}: {type}: [{classname}:{memberParameterless}] {log} (at {file}:{line})";

        [OdinSerialize]
        [HideInInspector]
        private bool[,] loggingLevels = new bool[5, 2]
        {
            { true, true },
            { true, false },
            { true, false },
            { true, false },
            { true, true }
        };

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        private SerializationData serializationData;

        private FileStream fileHandler = null;

        private object threadMonitor = new object();

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FileLogger"/> is enable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enable; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [ToggleButton("On / Off")]
        [GUIColorIf("enable", .25f, 1f, .25f, 1f, 1f, .25f, .25f, 1f)]
        public bool Enable
        {
            get
            {
                return enable;
            }

            set
            {
                enable = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [log in editor].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [log in editor]; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [Spacing(12)]
        public bool LogInEditor
        {
            get
            {
                return logInEditor;
            }

            set
            {
                logInEditor = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [hide development console].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hide development console]; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public bool HideDevelopmentConsole
        {
            get
            {
                return hideDevelopmentConsole;
            }

            set
            {
                hideDevelopmentConsole = value;
            }
        }

        /// <summary>
        /// Gets or sets the stand alone path log.
        /// </summary>
        /// <value>
        /// The stand alone path log.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [HideLabel]
        [Title("Standalone Filepath Template", horizontalLine: false, bold: false)]
        public string StandaloneFilepathTemplate
        {
            get
            {
                return standaloneFilepathTemplate;
            }

            private set
            {
                standaloneFilepathTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the mobile path log.
        /// </summary>
        /// <value>
        /// The mobile path log.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [HideLabel]
        [Title("Mobile Filepath Template", horizontalLine: false, bold: false)]
        public string MobileFilepathTemplate
        {
            get
            {
                return mobileFilepathTemplate;
            }

            private set
            {
                mobileFilepathTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the entry template.
        /// </summary>
        /// <value>
        /// The entry template.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [HideLabel]
        [Title("Release Entry Template", horizontalLine: false, bold: false)]
        public string ReleaseEntryTemplate
        {
            get
            {
                return releaseEntryTemplate;
            }

            set
            {
                releaseEntryTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the entry template.
        /// </summary>
        /// <value>
        /// The entry template.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup")]
        [HideLabel]
        [Spacing(After = 12)]
        [Title("Debug Entry Template", horizontalLine: false, bold: false)]
        public string DebugEntryTemplate
        {
            get
            {
                return debugEntryTemplate;
            }

            set
            {
                debugEntryTemplate = value;
            }
        }

        /// <summary>
        /// Gets or sets the logging levels.
        /// </summary>
        /// <value>
        /// The logging levels.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup/Logging Levels")]
        [TableMatrixExtended(RowLabelsMemberName = "LoggingLevelsRows", ColumnLabelsMemberName = "LoggingLevelsColumns", RowLabelsWidth = 60)]
        public bool[,] LoggingLevels
        {
            get
            {
                return loggingLevels;
            }

            set
            {
                loggingLevels = value;
            }
        }

        /// <summary>
        /// Gets the log folder path.
        /// </summary>
        /// <value>
        /// The path log.
        /// </value>
#if UNITY_STANDALONE
        public string FilepathTemplate => StandaloneFilepathTemplate;
#else
        public string FilepathTemplate => MobileFilepathTemplate;
#endif

        /// <summary>
        /// Gets the entry template.
        /// </summary>
        /// <value>
        /// The entry template.
        /// </value>
        public string EntryTemplate => Debug.isDebugBuild ? DebugEntryTemplate : ReleaseEntryTemplate;

        /// <summary>
        /// Gets a value indicating whether [file opened].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [file opened]; otherwise, <c>false</c>.
        /// </value>
        public bool FileOpened => fileHandler != null;

#if UNITY_EDITOR

        private string EnableLabelText => Enable ? "On" : "Off";

        private string[] LoggingLevelsRows => new string[] { "Logging", "Tracing" };

        private string[] LoggingLevelsColumns => new string[] { "Error", "Assert", "Warning", "Info", "Exception" };

        private bool InfoFolded { get; set; } = false;

#endif

        #endregion Properties

        #region Events methods

#if UNITY_EDITOR

        /// <summary>
        /// Called when [method load].
        /// </summary>
        [InitializeOnLoadMethod]
        public static void OnEditorLoadHandler()
        {
            LoadInstanceIfAssetExists();
        }

#endif

        /// <summary>
        /// Called when [runtime method load].
        /// </summary>
        [RuntimeInitializeOnLoadMethod]
        public static void OnRuntimeLoadHandler()
        {
            LoadInstanceIfAssetExists();
            if (HasInstanceLoaded && Instance.Enable
                && !(Application.isEditor && !Instance.LogInEditor))
            {
                Instance.Start();
            }
        }

        /// <summary>
        /// Called when [destroyed].
        /// </summary>
        private void OnQuittingHandler()
        {
            if (Instance != null)
            {
                Instance.Stop();
            }
            /*
            if (logger != null)
            {
                logger.Stop();
            }
            */
        }

        /// <summary>
        /// Handler of messages.
        /// </summary>
        /// <param name="logString">The log string.</param>
        /// <param name="stackTrace">The stack trace.</param>
        /// <param name="type">The type.</param>
        private void MessageReceivedHandler(string logString, string stackTrace, LogType type)
        {
            bool lockWasTaken = false;
            try
            {
                Monitor.Enter(threadMonitor, ref lockWasTaken);

                var actualStackTrace = !string.IsNullOrEmpty(stackTrace) ? stackTrace : StackTraceUtility.ExtractStackTrace();

                UnityMainThreadDispatcher.Instance.Enqueue(() => WriteLog(logString, new LogContext(type, actualStackTrace, ignoredClassMembers)));
            }
            catch
            {
                // Write exception
            }
            finally
            {
                if (lockWasTaken)
                {
                    Monitor.Exit(threadMonitor);
                }
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            UnitySerializationUtility.DeserializeUnityObject(this, ref this.serializationData);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            UnitySerializationUtility.SerializeUnityObject(this, ref this.serializationData);
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Registers to the logging pipeline and create the file.
        /// </summary>
        public void Start()
        {
            if (HideDevelopmentConsole)
            {
                Debug.developerConsoleVisible = false;
            }

            OpenFile();

            if (FileOpened)
            {
                Register();
            }
        }

        /// <summary>
        /// Close the file and unregister.
        /// </summary>
        public void Stop()
        {
            if (FileOpened)
            {
                CloseFile();

                Unregister();
            }
        }

        #endregion Public Methods

        #region Non Public Methods

#if UNITY_EDITOR

        [OnInspectorGUI]
        [FoldoutGroup("Setup")]
        [PropertyOrder(int.MaxValue)]
        [Spacing(12)]
        private void DrawInfoBox()
        {
            InfoFolded = Sirenix.Utilities.Editor.SirenixEditorGUI.DetailedMessageBox(
                "Several tokens can be used wrapped by brackets. IE.: \"{productName}\". Some tokens can receive formatting options like: \"{date:HHmmsss}\"\r\n\r\nClick to see tokens.",
                "All possible tokens:\r\n\r\n{date}\r\n{productName}\r\n{companyName}\r\n{version}\r\n{identifier}\r\n{buildGUID}\r\n{genuine},\r\n{platform}\r\n{unityVersion}\r\n{temporaryCachePath}\r\n{persistentDataPath}\r\n{streamingAssetsPath}\r\n{dataPath}\r\n{classname}\r\n{member}\r\n{memberParameterless}\r\n{file}\r\n{line}\r\n{type}\r\n{log}\r\n{environment}\r\n{newline}",
                MessageType.Info,
                InfoFolded,
                true
            );
        }

#endif

        /// <summary>
        /// Registers this instance to the logging pipeline.
        /// </summary>
        private void Register()
        {
            Application.logMessageReceivedThreaded += MessageReceivedHandler;
            ApplicationStateTracker.Quitting += OnQuittingHandler;
        }

        /// <summary>
        /// Unregister this instance to the logging pipeline.
        /// </summary>
        private void Unregister()
        {
            Application.logMessageReceivedThreaded -= MessageReceivedHandler;
            ApplicationStateTracker.Quitting -= OnQuittingHandler;
        }

        /// <summary>
        /// Gets the log filepath.
        /// </summary>
        /// <returns></returns>
        private string GetLogFilepath()
        {
            var expandedPath = Environment.ExpandEnvironmentVariables(FilepathTemplate);
            return ReplaceTokens(expandedPath);
        }

        /// <summary>
        /// Opens the file.
        /// </summary>
        /// <returns></returns>
        public void OpenFile()
        {
            // Get filepath
            var filepath = GetLogFilepath();

            try
            {
                // Verify the folder existance, if not create it
                var folderpath = Path.GetDirectoryName(filepath);
                if (!Directory.Exists(folderpath))
                {
                    Directory.CreateDirectory(folderpath);
                }

                // Create log
                fileHandler = new FileStream(filepath, FileMode.CreateNew, FileAccess.ReadWrite);

                // Header
                WriteLog("Log Opened.");

                // Notify only to console (this will not be outputted to file, since we have not registered yet)
                Debug.LogFormat("[FileLogger] Created log file at: {0}.", filepath);
            }
            catch //(Exception e)
            {
                fileHandler?.Close();

                Debug.LogErrorFormat("[FileLogger] Error creating log file at: {0}.", filepath);
            }
        }

        /// <summary>
        /// Closes the file.
        /// </summary>
        /// <returns></returns>
        public void CloseFile()
        {
            try
            {
                // TODO: Should do a Debug.Log here?

                // Footer
                WriteLog("Log Closed.");

                // Force flush of possible reminaing text
                fileHandler.Flush();

                // Close file
                fileHandler.Close();
                fileHandler.Dispose();
            }
            catch //(Exception e)
            {
                fileHandler?.Close();

                // TODO: Should do a Debug.LogError here?
            }
        }

        /// <summary>
        /// Shoulds the write logging.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private bool ShouldWriteLogging(LogType type)
        {
            return LoggingLevels[(int)type, 0];
        }

        /// <summary>
        /// Shoulds the write trace.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private bool ShouldWriteTrace(LogType type)
        {
            return LoggingLevels[(int)type, 1];
        }

        /// <summary>
        /// Writes the log.
        /// </summary>
        /// <param name="line">The line.</param>
        private void WriteLog(string line) => WriteLog(line, new LogContext(LogType.Log));

        /// <summary>
        /// Writes the log.
        /// </summary>
        /// <param name="line">The line.</param>
        private void WriteLog(string line, LogType type) => WriteLog(line, new LogContext(type));

        /// <summary>
        /// Writes the line.
        /// </summary>
        /// <param name="logString">The line.</param>
        /// <param name="metadata">The metadata.</param>
        private void WriteLog(string logString, LogContext context)
        {
            var logging = ShouldWriteLogging(context.type);
            var tracing = ShouldWriteTrace(context.type);

            if (logging)
            {
                var rawLine = string.Format("{0}\r\n{1}",
                    context.valid ? ReplaceTokens(EntryTemplate, logString, context) : logString,
                    tracing ? context.stackTrace : string.Empty
                );
                WriteRaw(rawLine);
            }
        }

        /// <summary>
        /// Outputs a line into the file.
        /// </summary>
        private void WriteRaw(string text)
        {
            IOHelper.AddText(fileHandler, text);
        }

        /// <summary>
        /// Replaces the tokens.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="logString">The log.</param>
        /// <param name="type">The type.</param>
        /// <param name="metadata">The metadata.</param>
        /// <returns></returns>
        private string ReplaceTokens(string template, string logString = null, LogContext? context = null)
        {
            var formattedLine = template.TokensReplace(tokensMapper, true);
            return string.Format(formattedLine,
                DateTime.Now,
                Application.productName,
                Application.companyName,
                Application.version,
                Application.identifier,
                Application.buildGUID,
                Application.genuine,
                Application.platform,
                Application.isEditor ? "Editor" : "Runtime",
                Application.unityVersion,
                Application.temporaryCachePath,
                Application.persistentDataPath,
                Application.streamingAssetsPath,
                Application.dataPath,
                context?.FirstLevel.classname,
                context?.FirstLevel.member,
                context?.FirstLevel.memberParameterless,
                context?.FirstLevel.file,
                context?.FirstLevel.line,
                context?.PrintableType,
                logString,
                "\r\n"
            );
        }

        #endregion Non Public Methods
    }
}
