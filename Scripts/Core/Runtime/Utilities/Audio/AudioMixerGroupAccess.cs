﻿using UnityEngine;
using UnityEngine.Audio;
using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

namespace CEUtilities.Audio
{
    /// <summary>
    /// Facilitate the access to internal properties and methods of the AudioMixerGroup
    /// </summary>
    [Serializable]
    public class AudioMixerGroupAccess
    {
        #region Exposed fields

        [SerializeField]
        [InlineEditor]
        [HideReferenceObjectPicker]
        [LabelText("$Name")]
        [Indenting("hierarchyLevel")]
        private AudioMixerGroup group;

        [SerializeField]
        [HideInInspector]
        private int hierarchyLevel = 0;

        #endregion Exposed fields

        #region Internal fields

        [NonSerialized]
        AudioMixerAccess mixerAccess;

        // Reflection

        Assembly __assembly;
        Type __groupControllerType;
        PropertyInfo __controller;
        PropertyInfo __solo;
        PropertyInfo __mute;
        PropertyInfo __bypass;
        PropertyInfo __children;
        MethodInfo __pitchSetter;
        MethodInfo __pitchGetter;
        MethodInfo __volumeSetter;
        MethodInfo __volumeGetter;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Printable label
        /// </summary>
        public string Name
        {
            get
            {
                return Group.name;
            }
        }

        /// <summary>
        /// Actual group
        /// </summary>
        public AudioMixerGroup Group
        {
            get
            {
                return group;
            }
        }

        /// <summary>
        /// Owner mixer
        /// </summary>
        public AudioMixerAccess MixerAccess
        {
            get
            {
                if (mixerAccess == null)
                    mixerAccess = new AudioMixerAccess((AudioMixer)__Controller.GetValue(group));

                return mixerAccess;
            }
        }

        /// <summary>
        /// Hierarchy level
        /// </summary>
        public int HierarchyLevel
        {
            get
            {
                return hierarchyLevel;
            }
        }

        /// <summary>
        /// Solo the group. This will affect all Snapshots.
        /// </summary>
        public bool Solo
        {
            get
            {
                if (ApplicationHelper.AppInPlayerOrPlaymode)
                {
                    PrintPlaymodeError();
                    return false;
                }
                return (bool)__Solo.GetValue(Group);
            }

            set
            {
                if (ApplicationHelper.AppInPlayerOrPlaymode)
                {
                    PrintPlaymodeError();
                    return;
                }
                __Solo.SetValue(Group, value);
                MixerAccess.UpdateMuteSolo();
            }
        }

        private bool EditorSolo => (bool)__Solo.GetValue(Group);

        /// <summary>
        /// Mute the group. This will affect all Snapshots.
        /// </summary>
        public bool Mute
        {
            get
            {
                if (ApplicationHelper.AppInPlayerOrPlaymode)
                {
                    PrintPlaymodeError();
                    return false;
                }
                return (bool)__Mute.GetValue(Group);
            }

            set
            {
                if (ApplicationHelper.AppInPlayerOrPlaymode)
                {
                    PrintPlaymodeError();
                    return;
                }
                __Mute.SetValue(Group, value);
                MixerAccess.UpdateMuteSolo();
            }
        }

        private bool EditorMute => (bool)__Mute.GetValue(Group);

        /// <summary>
        /// Bypass the effects. This will affect all Snapshots.
        /// </summary>
        public bool Bypass
        {
            get
            {
                if (ApplicationHelper.AppInPlayerOrPlaymode)
                {
                    PrintPlaymodeError();
                    return false;
                }
                return (bool)__Bypass.GetValue(Group);
            }

            set
            {
                if (ApplicationHelper.AppInPlayerOrPlaymode)
                {
                    PrintPlaymodeError();
                    return;
                }
                __Bypass.SetValue(Group, value);
                MixerAccess.UpdateBypass();
            }
        }
        private bool EditorBypass => (bool)__Bypass.GetValue(Group);

        /// <summary>
        /// Get all the groups which depend on this
        /// </summary>
        public AudioMixerGroupAccess[] ChildrenGroups
        {
            get
            {
                return (__Children.GetValue(Group) as AudioMixerGroup[])
                    .Select(x => new AudioMixerGroupAccess(x, hierarchyLevel + 1))
                    .ToArray();
            }
        }


        /// <summary>
        /// Printable state of solo
        /// </summary>
        public string SoloState
        {
            get
            {
                return "Solo: " + (Solo ? "On" : "Off");
            }
        }

        private string EditorSoloState => "Solo: " + (EditorSolo ? "On" : "Off");

        /// <summary>
        /// Printable state of mute.
        /// </summary>
        public string MuteState
        {
            get
            {
                return "Mute: " + (Mute ? "On" : "Off");
            }
        }

        private string EditorMuteState => "Mute: " + (EditorMute ? "On" : "Off");

        /// <summary>
        /// Printable state of bypass
        /// </summary>
        public string BypassState
        {
            get
            {
                return "Bypass: " + (Bypass ? "On" : "Off");
            }
        }

        private string EditorBypassState => "Bypass: " + (EditorBypass ? "On" : "Off");


        // Reflection

        public Assembly __Assembly
        {
            get
            {
                if (__assembly == null)
                    __assembly = Assembly.Load("UnityEditor");
                return __assembly;
            }
        }

        public Type __GroupControllerType
        {
            get
            {
                if (__groupControllerType == null)
                    __groupControllerType = __Assembly.GetType("UnityEditor.Audio.AudioMixerGroupController");
                return __groupControllerType;
            }
        }

        public PropertyInfo __Controller
        {
            get
            {
                if (__controller == null)
                    __controller = __GroupControllerType.GetProperty("controller");
                return __controller;
            }
        }

        public PropertyInfo __Solo
        {
            get
            {
                if (__solo == null)
                    __solo = __GroupControllerType.GetProperty("solo");
                return __solo;
            }
        }

        public PropertyInfo __Mute
        {
            get
            {
                if (__mute == null)
                    __mute = __GroupControllerType.GetProperty("mute");
                return __mute;
            }
        }

        public PropertyInfo __Bypass
        {
            get
            {
                if (__bypass == null)
                    __bypass = __GroupControllerType.GetProperty("bypassEffects");
                return __bypass;
            }
        }

        public PropertyInfo __Children
        {
            get
            {
                if (__children == null)
                    __children = __GroupControllerType.GetProperty("children");
                return __children;
            }
        }

        public MethodInfo __PitchSetter
        {
            get
            {
                if (__pitchSetter == null)
                    __pitchSetter = __GroupControllerType.GetMethod("SetValueForPitch");
                return __pitchSetter;
            }
        }

        public MethodInfo __PitchGetter
        {
            get
            {
                if (__pitchGetter == null)
                    __pitchGetter = __GroupControllerType.GetMethod("GetValueForPitch");
                return __pitchGetter;
            }
        }

        public MethodInfo __VolumeSetter
        {
            get
            {
                if (__volumeSetter == null)
                    __volumeSetter = __GroupControllerType.GetMethod("SetValueForVolume");
                return __volumeSetter;
            }
        }

        public MethodInfo __VolumeGetter
        {
            get
            {
                if (__volumeGetter == null)
                    __volumeGetter = __GroupControllerType.GetMethod("GetValueForVolume");
                return __volumeGetter;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Ctor
        /// </summary>
        public AudioMixerGroupAccess(AudioMixerGroup group, int hierarchyLevel)
        {
            this.group = group;
            this.hierarchyLevel = hierarchyLevel;
        }

        /// <summary>
        /// Set the pitch for the current snapshot
        /// </summary>
        public void SetPitch(float value)
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                PrintPlaymodeError();
                return;
            }
            SetPitch(MixerAccess.TargetSnapshot, value);
        }

        /// <summary>
        /// Set the pitch for a snapshot
        /// </summary>
        public void SetPitch(AudioMixerSnapshot snapshot, float value)
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                PrintPlaymodeError();
                return;
            }
            __PitchSetter.Invoke(group, new object[] { MixerAccess.Mixer, snapshot, value });
        }

        /// <summary>
        /// Get the pitch for the current snapshot
        /// </summary>
        public float GetPitch()
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                PrintPlaymodeError();
                return 1f;
            }
            return GetPitch(MixerAccess.TargetSnapshot);
        }

        /// <summary>
        /// Get the pitch for a snapshot
        /// </summary>
        public float GetPitch(AudioMixerSnapshot snapshot)
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                PrintPlaymodeError();
                return 1f;
            }
            return (float)__PitchGetter.Invoke(group, new object[] { MixerAccess.Mixer, snapshot });
        }

        /// <summary>
        /// Set the volume for the current snapshot
        /// </summary>
        public void SetVolume(float value)
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                PrintPlaymodeError();
                return;
            }
            SetVolume(MixerAccess.TargetSnapshot, value);
        }

        /// <summary>
        /// Set the volume for a snapshot
        /// </summary>
        public void SetVolume(AudioMixerSnapshot snapshot, float value)
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                PrintPlaymodeError();
                return;
            }
            __VolumeSetter.Invoke(group, new object[] { MixerAccess, snapshot, value });
        }

        /// <summary>
        /// Get the volume for the current snapshot
        /// </summary>
        public float GetVolume()
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                PrintPlaymodeError();
                return 1f;
            }
            return GetVolume(MixerAccess.TargetSnapshot);
        }

        /// <summary>
        /// Get the volume for a snapshot
        /// </summary>
        public float GetVolume(AudioMixerSnapshot snapshot)
        {
            if (ApplicationHelper.AppInPlayerOrPlaymode)
            {
                PrintPlaymodeError();
                return 1f;
            }
            return (float)__VolumeGetter.Invoke(group, new object[] { MixerAccess, snapshot });
        }

        /// <summary>
        /// Toggle the Solo value
        /// </summary>
        [HorizontalGroup("Buttons")]
        [Indenting("hierarchyLevel")]
        [LabelText("$EditorSoloState")]
        [GUIColorIf("EditorSolo", 1f, .6f, 0f, 1f, 1f, 1f, 1f, 1f)]
        public void ToggleSolo() { __Solo.SetValue(Group, !(bool)__Solo.GetValue(Group)); }

        /// <summary>
        /// Toggle the Mute value
        /// </summary>
        [HorizontalGroup("Buttons")]
        [LabelText("$EditorMuteState")]
        [GUIColorIf("EditorMute", 1f, .6f, 0f, 1f, 1f, 1f, 1f, 1f)]
        public void ToggleMute() { __Mute.SetValue(Group, !(bool)__Mute.GetValue(Group)); }

        /// <summary>
        /// Toggle the Bypass value
        /// </summary>
        [HorizontalGroup("Buttons")]
        [LabelText("$EditorBypassState")]
        [GUIColorIf("EditorBypass", 1f, .6f, 0f, 1f, 1f, 1f, 1f, 1f)]
        public void ToggleBypass() { __Bypass.SetValue(Group, !(bool)__Bypass.GetValue(Group)); }

        #endregion Methods

        #region Non Public Methods

        private void PrintPlaymodeError()
        {
            Debug.LogWarning("[AudioMixerGroupAccess] Cannot edit or access inner values during playmode. This is not supported by Unity. Try transitioning between Snapshots.");
        }

        #endregion Methods
    }
}