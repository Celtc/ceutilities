﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.Audio
{
    [System.Serializable]
    public class AudioTrack
    {
        #region Exposed fields

        [SerializeField]
        [BoxGroup("$Name")]
        [Required]
        [InlineEditor(InlineEditorModes.LargePreview, Expanded = false)]
        public AudioClip clip;

        [SerializeField]
        [BoxGroup("$Name")]
        [Range(0f, 1f)]
        public float volume = 1f;

        [SerializeField]
        [BoxGroup("$Name")]
        public bool loop = true;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Name of the inner clip
        /// </summary>
        [HideInInspector]
        public string Name
        {
            get
            {
                return clip != null ? clip.name : "Empty";
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Empty ctor
        /// </summary>
        public AudioTrack() { volume = 1f; }

        /// <summary>
        /// Ctor
        /// </summary>
        public AudioTrack(AudioClip clip, float volume)
        {
            this.clip = clip;
            this.volume = volume;
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}