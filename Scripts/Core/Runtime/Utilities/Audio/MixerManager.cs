﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;

namespace CEUtilities.Audio
{
    public class MixerManager : Singleton<MixerManager>
    {
        #region Exposed fields

        [SerializeField]
        [LabelText("Mixers")]
        [ListDrawerSettings(HideAddButton = true, NumberOfItemsPerPage = 1, DraggableItems = false)]
        private List<AudioMixerAccess> mixersAccess = new List<AudioMixerAccess>();

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Mixers
        /// </summary>
        public List<AudioMixerAccess> Mixers
        {
            get
            {
                return mixersAccess;
            }
        }

        /// <summary>
        /// All the mixers names
        /// </summary>
        public string[] MixersNames
        {
            get
            {
                var count = mixersAccess.Count;
                var names = new string[count];
                for (int i = 0; i < count; i++)
                    names[i] = mixersAccess[i].Name;
                return names;
            }
        }

        #endregion Properties

        #region Events methods

        /// <summary>
        /// 
        /// </summary>
        protected override void OnAfterDeserialize()
        {
            base.OnAfterDeserialize();

#if UNITY_EDITOR
            foreach (var access in mixersAccess)
                access.OnAfterDeserialization();
#endif
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnBeforeSerialize()
        {
            base.OnBeforeSerialize();

#if UNITY_EDITOR
            UpdateMixers();

            foreach (var access in mixersAccess)
                access.OnBeforeSerialization();
#endif
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Get the mixer with name
        /// </summary>
        public AudioMixerAccess GetMixer(string name)
        {
            return Mixers.FirstOrDefault(x => x.Name == name);
        }

        #endregion Methods

        #region Non Public Methods

#if UNITY_EDITOR
        /// <summary>
        /// Used for keeping updated the mixers
        /// </summary>
        private class AudioMixerComparer : IEqualityComparer<AudioMixer>
        {
            public bool Equals(AudioMixer a, AudioMixer b) { return a.GetInstanceID() == b.GetInstanceID(); }

            public int GetHashCode(AudioMixer obj) { return obj.GetInstanceID(); }
        }

        /// <summary>
        /// Keep updated the cached mixers
        /// </summary>
        private void UpdateMixers()
        {
            // Get actual mixers
            var newMixers = new List<AudioMixer>();
            var guids = UnityEditor.AssetDatabase.FindAssets("t:AudioMixer");
            foreach (var guid in guids)
            {
                var path = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
                newMixers.Add(UnityEditor.AssetDatabase.LoadAssetAtPath<AudioMixer>(path));
            }

            // Modification?
            if (!Enumerable.SequenceEqual(
                newMixers.OrderBy(t => t.GetInstanceID()),
                mixersAccess.Select(x => x.Mixer).OrderBy(t => t.GetInstanceID()),
                new AudioMixerComparer()))
            {
                // New mixers?
                foreach (var newMixer in newMixers)
                {
                    if (!mixersAccess.Contains(x => x.Mixer == newMixer))
                        mixersAccess.Add(new AudioMixerAccess(newMixer));
                }

                // Deleted mixer?
                for (int i = 0; i < mixersAccess.Count; i++)
                {
                    var mixerAccess = mixersAccess[i];
                    if (!newMixers.Contains(x => x == mixerAccess.Mixer))
                        mixersAccess.Remove(mixerAccess);
                }
            }
        }
#endif

        #endregion Methods
    }
}