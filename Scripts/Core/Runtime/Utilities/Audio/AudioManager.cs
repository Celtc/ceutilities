﻿using UnityEngine;
using UnityEngine.Audio;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;
using CEUtilities.Helpers;

namespace CEUtilities.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioManager : Singleton<AudioManager>
    {
        #region Exposed fields

        [ShowInInspector]
        [BoxGroup("Playmode", true, false, -50)]
        [ReadOnly]
        private bool playing;

        [BoxGroup("Playmode")]
        [ShowInInspector]
        [ReadOnly]
        private bool fading;


        [SerializeField]
        [BoxGroup("Setup")]
        private bool autoPlay = false;

        [SerializeField]
        [BoxGroup("Setup")]
        [EnableIf("autoPlay")]
        [ValueDropdown("trackNames")]
        private string defaultAudio;

        [SerializeField]
        [BoxGroup("Setup")]
        private float crossFadeDuration = 1f;

        [SerializeField]
        [BoxGroup("Setup")]
        private float generalVolume = 1f;

        [SerializeField]
        [BoxGroup("Setup")]
        [LabelText("Mixer Output (Optional)")]
        [AssetsOnly]
        private AudioMixerGroup mixerOutput = null;


        [SerializeField]
        [OnValueChanged("RegenerateIndexTable", true)]
        [ValidateInput("validTrack", "Should not contain two tracks with same name")]
        [ValidateInput("hasTracks", "Add tracks for valid usage", InfoMessageType.Warning)]
        private List<AudioTrack> tracks = new List<AudioTrack>();


#if UNITY_EDITOR
        /// <summary>
        /// For editor
        /// </summary>
        private string[] trackNames
        {
            get
            {
                return tracks.Select(x => x.Name).ToArray();
            }
        }

        /// <summary>
        /// For editor
        /// </summary>
        private bool hasTracks(List<AudioTrack> tracks)
        {
            return tracks.Count > 0;
        }

        /// <summary>
        /// Validate no tracks with the same name
        /// </summary>
        private bool validTrack(List<AudioTrack> prop)
        {
            return !tracks.Any(x => tracks.Contains(y => y.Name == x.Name && y != x));
        }
#endif

        #endregion Exposed fields

        #region Internal fields

        private int index = -1;
        private AudioSource source;

        [SerializeField]
        [HideInInspector]
        private Dictionary<string, int> trackNameToIndex = new Dictionary<string, int>();

        #endregion Internal fields

        #region Custom Events

        /// <summary>
        /// Triggered on track change.
        /// Param: New track
        /// </summary>
        [FoldoutGroup("Events", false)]
        public UnityEvent_AudioTrack OnTrackChanged = new UnityEvent_AudioTrack();

        /// <summary>
        /// Triggered when a new track starts
        /// </summary>
        [FoldoutGroup("Events")]
        public UnityEvent_AudioTrack OnTrackStarted = new UnityEvent_AudioTrack();

        /// <summary>
        /// Triggered when a track ends
        /// </summary>
        [FoldoutGroup("Events")]
        public UnityEvent_AudioTrack OnTrackEnded = new UnityEvent_AudioTrack();

        #endregion Custom Events

        #region Properties (public)

        /// <summary>
        /// Should start automatically?
        /// </summary>
        public bool AutoPlay
        {
            get
            {
                return autoPlay;
            }

            set
            {
                autoPlay = value;
            }
        }

        /// <summary>
        /// Overall volume
        /// </summary>
        public float GeneralVolume
        {
            get
            {
                return generalVolume;
            }

            set
            {
                generalVolume = value;
            }
        }

        /// <summary>
        /// Time to fade between tracks
        /// </summary>
        public float CrossFadeDuration
        {
            get
            {
                return crossFadeDuration;
            }

            set
            {
                crossFadeDuration = value;
            }
        }

        /// <summary>
        /// Is currently playing?
        /// </summary>
        public bool Playing
        {
            get
            {
                return playing;
            }

            private set
            {
                playing = value;
            }
        }

        /// <summary>
        /// Is currently not playing?
        /// </summary>
        public bool NotPlaying
        {
            get
            {
                return !playing;
            }
        }

        /// <summary>
        /// Is currently fading?
        /// </summary>
        public bool Fading
        {
            get
            {
                return fading;
            }

            private set
            {
                fading = value;
            }
        }

        /// <summary>
        /// Selected index from available tracks
        /// </summary>
        public int SelectedIndex
        {
            get
            {
                return index;
            }

            private set
            {
                index = value;
            }
        }

        /// <summary>
        /// Selected track
        /// </summary>
        public AudioTrack SelectedTrack
        {
            get
            {
				if (!IsIndexValid(index))
					return null;
				else
					return Tracks?[index];
            }
        }

        /// <summary>
        /// Selected clip
        /// </summary>
        public AudioClip SelectedClip
        {
            get
            {
				return SelectedTrack?.clip;
            }
        }

        /// <summary>
        /// Selected clip
        /// </summary>
        public string SelectedName
        {
            get
            {
                return SelectedTrack?.Name;
            }
        }

		/// <summary>
		/// Source audio
		/// </summary>
		public AudioSource Source
        {
            get
            {
                if (source == null)
                    source = GetComponent<AudioSource>();

                return source;
            }

            private set
            {
                source = value;
            }
        }


        /// <summary>
        /// Array of available tracks
        /// </summary>
        private List<AudioTrack> Tracks
        {
            get
            {
                return tracks;
            }

            set
            {
                tracks = value;
            }
        }

#if UNITY_EDITOR
		/// <summary>
		/// Selected clip
		/// </summary>
		[ShowInInspector]
        [BoxGroup("Playmode")]
        [ReadOnly]
        [LabelText("Track")]
        private string SelectedClipName
        {
            get
            {
                return IsIndexValid(SelectedIndex) ? trackNames[SelectedIndex] : "None";
            }
        }
#endif

        #endregion Properties

        #region Event callbacks

        protected override void Awake()
        {
            base.Awake();

            SetupAudioSource();
        }

        protected void Start()
        {
            if (AutoPlay && defaultAudio != null)
                StartCoroutine(PlayTrack(NameToIndex(defaultAudio), 0f, crossFadeDuration * .5f));
        }

        #endregion Event callbacks

        #region Public Methods

        /// <summary>
        /// Get all clips
        /// </summary>
        public AudioClip[] GetClips()
        {
            return Tracks.Select(x => x.clip).ToArray();
        }

        /// <summary>
        /// Get all tracks
        /// </summary>
        public AudioTrack[] GetTracks()
        {
            return Tracks.ToArray();
        }

        /// <summary>
        /// Set all clips
        /// </summary>
        public void SetClips(AudioClip[] clips, float volume = .5f)
        {
            Tracks = new List<AudioTrack>(clips.Length);
            foreach (var clip in clips)
                Tracks.Add(new AudioTrack(clip, volume));

            RegenerateIndexTable();
        }

        /// <summary>
        /// Set all clips
        /// </summary>
        public void SetTracks(AudioTrack[] tracks)
        {
            Tracks = tracks.ToList();

            RegenerateIndexTable();
        }

        /// <summary>
        /// Add one clip
        /// </summary>
        public void AddClip(AudioClip clip, float volume = .5f)
        {
            Tracks.Add(new AudioTrack(clip, volume));

            RegenerateIndexTable();
        }

        /// <summary>
        /// Add one track
        /// </summary>
        public void AddTrack(AudioTrack track)
        {
            Tracks.Add(track);

            RegenerateIndexTable();
        }

        /// <summary>
        /// Get a single clip
        /// </summary>
        public AudioClip GetClip(int index)
        {
            return GetTrack(index).clip;
        }

        /// <summary>
        /// Get a single clip
        /// </summary>
        public AudioTrack GetTrack(int index)
        {
            return Tracks[index];
        }

        /// <summary>
        /// Remove a single clip
        /// </summary>
        public void RemoveClip(int index)
        {
            RemoveTrack(index);
        }

        /// <summary>
        /// Remove a single track
        /// </summary>
        public void RemoveTrack(int index)
        {
            Tracks.RemoveAt(index);

            RegenerateIndexTable();
        }

        /// <summary>
        /// Remove a single clip
        /// </summary>
        public void RemoveClip(string name)
        {
            RemoveTrack(name);
        }

        /// <summary>
        /// Remove a single clip
        /// </summary>
        public void RemoveTrack(string name)
        {
            var index = NameToIndex(name);
            if (index >= 0)
                Tracks.RemoveAt(index);

            RegenerateIndexTable();
        }


        /// <summary>
        /// Transition from the current track to the next available
        /// </summary>
        [DisableInEditorMode]
        [EnableIf("Playing")]
        [BoxGroup("Playmode")]
        [HorizontalGroup("Playmode/Buttons")]
        [PropertyOrder(100)]
        [Button]
        public void ChangeToNextTrack()
        {
            if (Tracks.Count <= 1)
            {
                Debug.LogWarning("[AudioManager] No other track available.");
                return;
            }

            var index = MathHelper.RepeatIndex(SelectedIndex + 1, Tracks.Count);
            ChangeToAt(index, 0f, CrossFadeDuration);
        }


        /// <summary>
        /// Transition from the current music (if playing) to a target music
        /// </summary>
        public void ChangeTo(string name)
        {
            ChangeToAt(NameToIndex(name), 0f, CrossFadeDuration);
        }

        /// <summary>
        /// Transition from the current music (if playing) to a target music
        /// </summary>
        public void ChangeTo(string name, float transition)
        {
            ChangeToAt(NameToIndex(name), 0f, transition);
        }

        /// <summary>
        /// Transition from the current music (if playing) to a target music
        /// </summary>
        public void ChangeTo(int index)
        {
            ChangeToAt(index, 0f, CrossFadeDuration);
        }

        /// <summary>
        /// Transition from the current music (if playing) to a target music
        /// </summary>
        public void ChangeTo(int index, float transition)
        {
            ChangeToAt(index, 0f, transition);
        }


        /// <summary>
        /// Transition from the current music (if playing) to a target music
        /// </summary>
        public void ChangeToAt(string name, float startTime)
        {
            ChangeToAt(NameToIndex(name), startTime, CrossFadeDuration);
        }

        /// <summary>
        /// Transition from the current music (if playing) to a target music
        /// </summary>
        public void ChangeToAt(int index, float startTime)
        {
            ChangeToAt(index, startTime, CrossFadeDuration);
        }

        /// <summary>
        /// Transition from the current music (if playing) to a target music
        /// </summary>
        public void ChangeToAt(string name, float startTime, float transition)
        {
            ChangeToAt(NameToIndex(name), startTime, transition);
        }

        /// <summary>
        /// Transition from the current music (if playing) to a target music
        /// </summary>
        public void ChangeToAt(int index, float startTime, float transition)
        {
            if (!IsIndexValid(index))
            {
                Debug.LogWarning("[AudioManager] The selected track index \"" + index + "\" is not valid.");
                return;
            }

            if (Fading)
                StopAllCoroutines();

            StartCoroutine(ChangeTrack(index, startTime, transition));
        }


        /// <summary>
        /// Stops all music
        /// </summary>
        [DisableInEditorMode]
        [EnableIf("NotPlaying")]
        [BoxGroup("Playmode")]
        [HorizontalGroup("Playmode/Buttons")]
        [Button]
        public void Play()
        {
            if (Fading)
                StopAllCoroutines();

			if (SelectedIndex == -1 && !string.IsNullOrEmpty(defaultAudio))
				SelectedIndex = NameToIndex(defaultAudio);

            StartCoroutine(PlayTrack(SelectedIndex, 0f, CrossFadeDuration * .5f));
        }


        /// <summary>
        /// Plays at position
        /// </summary>
        public void PlayAt(float time)
        {
            if (Fading)
                StopAllCoroutines();

			if (SelectedIndex == -1 && !string.IsNullOrEmpty(defaultAudio))
				SelectedIndex = NameToIndex(defaultAudio);

            StartCoroutine(PlayTrack(SelectedIndex, time, CrossFadeDuration * .5f));
        }


        /// <summary>
        /// Stops all music
        /// </summary>
        [DisableInEditorMode]
        [EnableIf("Playing")]
        [BoxGroup("Playmode")]
        [HorizontalGroup("Playmode/Buttons")]
        [Button]
        public void Stop()
        {
            if (Fading)
                StopAllCoroutines();

            StartCoroutine(StopTrack(CrossFadeDuration * .5f));
        }


        /// <summary>
        /// Plays a single clip
        /// </summary>
        public void PlaySFX(AudioClip clip)
        {
            AudioPlayer.PlaySingle(clip);
        }

        /// <summary>
        /// Plays a single clip
        /// </summary>
        public void PlaySFX(AudioClip clip, float volume)
        {
            AudioPlayer.PlaySingle(clip, volume);
        }

        /// <summary>
        /// Plays a single clip
        /// </summary>
        public void PlaySFX(AudioClip clip, float volume, AudioMixerGroup mixer)
        {
            AudioPlayer.PlaySingle(clip, volume, mixer);
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Is the candidate index valid?
        /// </summary>
        private bool IsIndexValid(int index)
        {
            return index >= 0 && Tracks.Count > index;
        }

        /// <summary>
        /// Regenerates the lookup table
        /// </summary>
        private void RegenerateIndexTable()
        {
            trackNameToIndex.Clear();
            for (int i = 0; i < Tracks.Count; i++)
            {
                var track = Tracks[i];
                if (track != null && !trackNameToIndex.ContainsKey(track.Name))
                    trackNameToIndex.Add(track.Name, i);
            }
        }


        /// <summary>
        /// Setup the must have comp
        /// </summary>
        private void SetupAudioSource()
        {
            //Source.loop = true;
            Source.playOnAwake = false;
            Source.outputAudioMixerGroup = mixerOutput;
        }

        /// <summary>
        /// Find the index from the name
        /// </summary>
        private int NameToIndex(string name)
        {
            var result = -1;

            if (trackNameToIndex.TryGetValue(name, out result))
                return result;
            else
                return -1;
        }

        /// <summary>
        /// Change music routine
        /// </summary>
        private IEnumerator ChangeTrack(int index, float startTime, float transition)
        {
            // Event
            OnTrackChanged.Invoke(Tracks[index]);

            // Playing?
			var playing = Playing;
            if (playing)
                yield return StopTrack(transition * .5f);

            if (Fading) yield break;

            // Event
			if (playing)
				OnTrackEnded.Invoke(SelectedTrack);

            // Play selected music
            yield return PlayTrack(index, startTime, transition * .5f);

            // Event
            OnTrackStarted.Invoke(SelectedTrack);
        }

        /// <summary>
        /// Play music routine
        /// </summary>
        private IEnumerator PlayTrack(int index, float startTime, float fadeInDuration)
        {
            // Flag
            Fading = true;

            SelectedIndex = index;
            var track = Tracks[index];

            // No clip?
            if (track == null)
            {
                Playing = false;
                yield break;
            }

            Source.clip = track.clip;
            Source.loop = track.loop;
            Source.time = startTime;
            Source.volume = 0f;
            Source.Play();
            Playing = true;

            // If fade
            if (fadeInDuration > 0f)
            {
                // Transition
                var timer = 0f;
                while (timer <= fadeInDuration)
                {
                    Source.volume = track.volume * GeneralVolume * Mathf.Clamp01(timer / fadeInDuration);
                    yield return null;
                    timer += Time.deltaTime;
                }
            }

            // Set end value
            Source.volume = track.volume * GeneralVolume;

            // Flag
            Fading = false;
        }

        /// <summary>
        /// Stops the playing music
        /// </summary>
        private IEnumerator StopTrack(float fadeOutDuration)
        {
            // Flag
            Fading = true;

            // If fade
            var initVolume = Source.volume;
            if (fadeOutDuration > 0f)
            {
                // Transition
                var timer = 0f;
                while (timer <= fadeOutDuration)
                {
                    Source.volume = initVolume * Mathf.Clamp01(1 - (timer / fadeOutDuration));
                    yield return null;
                    timer += Time.deltaTime;
                }
            }

            // Set end value
            Source.volume = 0f;
            Source.Stop();
            Playing = false;

            // Flag
            Fading = false;
        }

        #endregion Non Public Methods
    }
}