﻿using UnityEngine;
using UnityEngine.Audio;
using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;

namespace CEUtilities.Audio
{
    /// <summary>
    /// Facilitate the access to internal properties and methods of the AudioMixer
    /// </summary>
    [Serializable]
    public class AudioMixerAccess
    {
        #region Exposed fields

        [SerializeField]
        [ReadOnly]
        [PropertyOrder(-10)]
        [Spacing(5, 0)]
        private AudioMixer mixer;
        
        [SerializeField]
        [TabGroup("Groups")]
        [LabelText("Groups")]
        [ListDrawerSettings(DraggableItems = false, HideAddButton = true)]
        private AudioMixerGroupAccess[] groupsAccess;

        [SerializeField]
        [TabGroup("Snapshots")]
        [LabelText("Snapshots")]
        [InlineEditor]
        [ListDrawerSettings(DraggableItems = false, HideAddButton = true)]
        private AudioMixerSnapshot[] snapshots;

        #endregion Exposed fields

        #region Internal fields

        // Reflection
        Type __mixerControllerType;
        PropertyInfo __masterGroup;
        PropertyInfo __snapshots;
        PropertyInfo __targetSnapshot;
        MethodInfo __updateMuteSolo;
        MethodInfo __updateBypass;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Name of the mixer
        /// </summary>
        public string Name
        {
            get
            {
                return mixer.name;
            }
        }

        /// <summary>
        /// Mixer of this access
        /// </summary>
        public AudioMixer Mixer
        {
            get
            {
                return mixer;
            }
        }

        /// <summary>
        /// All groups of the mixer
        /// </summary>
        public AudioMixerGroupAccess[] Groups
        {
            get
            {
                if (groupsAccess == null)
                    SyncToMixer();

                return groupsAccess;
            }
        }

        /// <summary>
        /// All groups of the mixer
        /// </summary>
        public AudioMixerGroupAccess[] MasterGroup
        {
            get
            {
                return groupsAccess;
            }
        }

        /// <summary>
        /// All mixer snapshots
        /// </summary>
        public AudioMixerSnapshot[] Snapshots
        {
            get
            {
                if (snapshots == null)
                    SyncToMixer();

                return snapshots;
            }
        }

        /// <summary>
        /// Every snapshot name
        /// </summary>
        public List<string> SnapshotsName
        {
            get { return Snapshots.Select(x => x.name).ToList(); }
        }

        /// <summary>
        /// The current target snapshot for editing
        /// </summary>
        public AudioMixerSnapshot TargetSnapshot
        {
            get
            {
                return (AudioMixerSnapshot)__TargetSnapshot.GetValue(Mixer);
            }

            set
            {
                __TargetSnapshot.SetValue(Mixer, value);
            }
        }


        // Reflection

        private Type __MixerControllerType
        {
            get
            {
                if (__mixerControllerType == null)
                    __mixerControllerType = mixer.GetType();

                return __mixerControllerType;
            }
        }

        private PropertyInfo __Snapshots
        {
            get
            {
                if (__snapshots == null)
                    __snapshots = __MixerControllerType.GetProperty("snapshots");

                return __snapshots;
            }
        }

        private PropertyInfo __TargetSnapshot
        {
            get
            {
                if (__targetSnapshot == null)
                    __targetSnapshot = __MixerControllerType.GetProperty("TargetSnapshot");

                return __targetSnapshot;
            }
        }

        private PropertyInfo __MasterGroup
        {
            get
            {
                if (__masterGroup == null)
                    __masterGroup = __MixerControllerType.GetProperty("masterGroup");

                return __masterGroup;
            }
        }

        private MethodInfo __UpdateMuteSolo
        {
            get
            {
                if (__updateMuteSolo == null)
                    __updateMuteSolo = __MixerControllerType.GetMethod("UpdateMuteSolo");

                return __updateMuteSolo;
            }
        }

        private MethodInfo __UpdateBypass
        {
            get
            {
                if (__updateBypass == null)
                    __updateBypass = __MixerControllerType.GetMethod("UpdateBypass");

                return __updateBypass;
            }
        }

#if UNITY_EDITOR
        [ShowInInspector]
        [PropertyOrder(-1)]
        [Spacing(5, 5)]
        [LabelText("Current Snapshot")]
        [ValueDropdown("SnapshotsName")]
        public string PrintableTargetSnapshot
        {
            get
            {
                return TargetSnapshot.name;
            }

            set
            {
                TargetSnapshot = snapshots.FirstOrDefault(x => x.name == value);
            }
        }
#endif

        #endregion Properties

        #region Events methods

        internal void OnAfterDeserialization()
        {
            //
        }

        internal void OnBeforeSerialization()
        {
#if UNITY_EDITOR
            SyncToMixer();
#endif
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Ctor
        /// </summary>
        public AudioMixerAccess(AudioMixer mixer)
        {
            this.mixer = mixer;
        }

        /// <summary>
        /// Get the group with the name
        /// </summary>
        public AudioMixerGroupAccess GetGroup(string name)
        {
            return Groups.FirstOrDefault(x => x.Name == name);
        }

        /// <summary>
        /// Get the snapshot with the name
        /// </summary>
        public AudioMixerSnapshot GetSnapshot(string name)
        {
            return Snapshots.FirstOrDefault(x => x.name == name);
        }


        /// <summary>
        /// Transition the snapshot
        /// </summary>
        public void TransitionToSnapshot(string name)
        {
            TransitionToSnapshot(GetSnapshot(name), .25f);
        }

        /// <summary>
        /// Transition the snapshot
        /// </summary>
        public void TransitionToSnapshot(string name, float timeToReach)
        {
            TransitionToSnapshot(GetSnapshot(name), timeToReach);
        }

        /// <summary>
        /// Transition the snapshot
        /// </summary>
        public void TransitionToSnapshot(AudioMixerSnapshot snapshot)
        {
            TransitionToSnapshot(snapshot, .25f);
        }

        /// <summary>
        /// Transition the snapshot
        /// </summary>
        public void TransitionToSnapshot(AudioMixerSnapshot snapshot, float timeToReach)
        {
            snapshot.TransitionTo(timeToReach);
        }

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Keep updated the mixer data
        /// </summary>
        internal void SyncToMixer()
        {
            // Only if mixer assigned
            if (Mixer == null)
                return;

            // Temp list
            var groupsAccess = new List<AudioMixerGroupAccess>();

            // Get master group and access
            var masterGroup = (AudioMixerGroup)__MasterGroup.GetValue(Mixer);
            var masterGroupAccess = new AudioMixerGroupAccess(masterGroup, 0);

            // Add master and childrens
            groupsAccess.Add(masterGroupAccess);
            AddChildGroups(masterGroupAccess, ref groupsAccess);

            this.groupsAccess = groupsAccess.ToArray();

            // Snapshots
            snapshots = (AudioMixerSnapshot[])__Snapshots.GetValue(Mixer, null);
        }

        /// <summary>
        /// Updates the state of the mute and solo
        /// </summary>
        internal void UpdateMuteSolo()
        {
            __UpdateMuteSolo.Invoke(Mixer, null);
        }

        /// <summary>
        /// Updates the state of the bypass
        /// </summary>
        internal void UpdateBypass()
        {
            __UpdateBypass.Invoke(Mixer, null);
        }

        /// <summary>
        /// Editor tool
        /// </summary>
        private void AddChildGroups(AudioMixerGroupAccess groupAccess, ref List<AudioMixerGroupAccess> list)
        {
            foreach(var childGroupAccess in groupAccess.ChildrenGroups)
            {
                list.Add(childGroupAccess);
                AddChildGroups(childGroupAccess, ref list);
            }
        }

        #endregion Methods
    }
}