﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;

using Sirenix.OdinInspector;

namespace CEUtilities.Audio
{
	[RequireComponent(typeof(AudioSource))]
	public class AudioPlayer : MonoBehaviour
	{
		#region Static

		public static void PlaySingle(AudioClip clip, float volume = 1f, AudioMixerGroup mixer = null)
		{
			var player = new GameObject("Player").AddComponent<AudioPlayer>();
            if (mixer != null)
                player.SetOutput(mixer);
            player.SetVolume(volume);
			player.Play(clip);
		}

        #endregion

        #region Exposed fields

        [SerializeField]
        private bool autoDestroy = true;

        [SerializeField]
        private AudioClip defaultClip;

		#endregion Exposed fields

		#region Internal fields

		private AudioSource audioSource = null;

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties (public)

		public AudioSource Audio
		{
			get
			{
                if (audioSource == null)
                {
                    audioSource = GetComponent<AudioSource>();
                }

				return audioSource;
			}

			private set
			{
                audioSource = value;
			}
		}

        public bool AutoDestroy
        {
            get
            {
                return autoDestroy;
            }

            set
            {
                autoDestroy = value;
            }
        }

        #endregion Properties

        #region Unity events

        #endregion Unity events

        #region Methods

        [ButtonGroup("Control")]
        public void Play()
		{
			Audio.clip = defaultClip;
			Audio.loop = false;
			Audio.Play();

            if (AutoDestroy)
            {
                StartCoroutine(AutoKill(defaultClip.length));
            }
		}

        public void Play(AudioClip clip)
		{
			Audio.clip = clip;
			Audio.loop = false;
			Audio.time = 0f;
			Audio.Play();

            if (AutoDestroy)
			    StartCoroutine(AutoKill(clip.length));
		}

        [ButtonGroup("Control")]
        public void Stop()
		{
			Audio.Stop();
		}

        private void SetVolume(float volume)
        {
            Audio.volume = volume;
        }

        public void SetOutput(AudioMixerGroup mixer)
        {
            Audio.outputAudioMixerGroup = mixer;
        }

		IEnumerator AutoKill(float timer)
		{
			yield return new WaitForSeconds(timer);
			Destroy(gameObject);
		}

		#endregion Methods
	}
}
