﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using Sirenix.OdinInspector;

namespace CEUtilities.Audio
{
    public class StaticAudioPlayerAccess : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private bool playOnEnable = false;

        [SerializeField]
        [HideInInspector]
        private AudioClip defaultClip;

        #endregion Exposed fields

        #region Internal fields

        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public bool PlayOnEnable
        {
            get
            {
                return playOnEnable;
            }

            set
            {
                playOnEnable = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Setup")]
        public AudioClip DefaultClip
        {
            get
            {
                return defaultClip;
            }

            set
            {
                defaultClip = value;
            }
        }

        #endregion Internal fields

        #region Properties

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        private void OnEnable()
        {
            if (PlayOnEnable)
            {
                PlaySFX();
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Plays a single clip
        /// </summary>
        [Button]
        public void PlaySFX()
        {
            AudioPlayer.PlaySingle(DefaultClip);
        }

        /// <summary>
        /// Plays a single clip
        /// </summary>
        public void PlaySFX(AudioClip clip)
        {
            AudioPlayer.PlaySingle(clip);
        }

        /// <summary>
        /// Plays a single clip
        /// </summary>
        public void PlaySFX(AudioClip clip, float volume)
        {
            AudioPlayer.PlaySingle(clip, volume);
        }

        /// <summary>
        /// Plays a single clip
        /// </summary>
        public void PlaySFX(AudioClip clip, float volume, AudioMixerGroup mixer)
        {
            AudioPlayer.PlaySingle(clip, volume, mixer);
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}