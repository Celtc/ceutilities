﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CEUtilities.Helpers;

namespace CEUtilities
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(LensFlare))]
    public class FlareDistanceBrightness : MonoBehaviour
    {
        #region Exposed fields

        public Vector2 distanceLimits = new Vector2(5f, 15f);

        public Vector2 brightnessLimits = new Vector2(0f, .35f);

        public AnimationCurve brightnessCurve = AnimationCurve.Linear(0, 1, 1, 0);

        #endregion Exposed fields

        #region Internal fields

        private bool dirty;

        private LensFlare lensFlareComp;

        private Vector3 lastCamaraPos;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public LensFlare LensFlareComp
        {
            get
            {
                if (lensFlareComp == null)
                    lensFlareComp = GetComponent<LensFlare>();

                return lensFlareComp;
            }
        }

        #endregion Properties

        #region Events methods

        private void Update()
        {
            var camaraPos = Camera.main.transform.position;
            if (!MathHelper.Approximately(camaraPos, lastCamaraPos))
            {
                lastCamaraPos = camaraPos;
                dirty = true;
            }

            if (dirty)
            {
                var dst = Vector3.Distance(camaraPos, transform.position);
                var x = (dst - distanceLimits.x) / (distanceLimits.y - distanceLimits.x);
                var y = brightnessCurve.EvaluateClamped(x);
                LensFlareComp.brightness = brightnessLimits.x * (1 - y) + brightnessLimits.y * y;
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}