﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

#if VUFORIA_ANDROID_SETTINGS

namespace CEUtilities.AR
{
    using Vuforia;

    public class ObjectTrackerAccess : MonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private ObjectTracker cachedObjectTracker;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        private ObjectTracker CachedObjectTracker
        {
            get
            {
                if (cachedObjectTracker == null)
                {
                    cachedObjectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
                }
                return cachedObjectTracker;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Starts the tracking.
        /// </summary>
        [Button]
        public void StartTracking()
        {
            CachedObjectTracker?.Start();
        }

        /// <summary>
        /// Stops the tracking.
        /// </summary>
        [Button]
        public void StopTracking()
        {
            CachedObjectTracker?.Stop();
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}

#endif