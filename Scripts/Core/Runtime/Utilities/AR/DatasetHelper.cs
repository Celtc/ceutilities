﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

#if VUFORIA_ANDROID_SETTINGS

namespace CEUtilities.AR
{
    using Vuforia;

    public static class DatasetHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Activate all Datasets
        /// </summary>
        public static void ActivateAll()
        {
            var objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            var datasets = objectTracker.GetDataSets();
            var activeDataSets = objectTracker.GetActiveDataSets();
            var inactiveDataSets = datasets.Where(x => !activeDataSets.Contains(x)).ToList(); ;

            objectTracker.Stop();

            for (int i = inactiveDataSets.Count - 1; i >= 0; i--)
            {
                objectTracker.ActivateDataSet(inactiveDataSets[i]);
            }

            objectTracker.Start();
        }

        /// <summary>
        /// Deactivate all Datasets
        /// </summary>
        public static void DeactivateAll()
        {
            var objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            var activeDataSets = objectTracker.GetActiveDataSets().ToList();

            for (int i = activeDataSets.Count - 1; i >= 0; i--)
            {
                objectTracker.DeactivateDataSet(activeDataSets[i]);
            }
        }

        /// <summary>
        /// Activate a particular dataset
        /// </summary>
        public static void ActivateDataSet(string name, bool deativateOthers = true)
        {
            var objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            var datasets = objectTracker.GetDataSets();

            // Deactivate others datasets
            if (deativateOthers)
            {
                var activeDataSets = objectTracker.GetActiveDataSets();
                var activeDataSetsToBeRemoved = activeDataSets.ToList();

                // Loop through all the active datasets and deactivate them.
                foreach (DataSet ads in activeDataSetsToBeRemoved)
                {
                    objectTracker.DeactivateDataSet(ads);
                }
            }

            // Swapping of the datasets should NOT be done while the ObjectTracker is running.
            // So, Stop the tracker first.
            objectTracker.Stop();

            // Then, look up the new dataset and if one exists, activate it.
            foreach (var dataset in datasets)
            {
                if (dataset.Path.Contains(name))
                {
                    objectTracker.ActivateDataSet(dataset);
                }
            }

            // Finally, restart the object tracker.
            objectTracker.Start();
        }

        /// <summary>
        /// Activate a particular dataset
        /// </summary>
        public static void DeactivateDataSet(string name)
        {
            var objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

            var activeDataSets = objectTracker.GetActiveDataSets();

            // Loop through all the active datasets and deactivate
            foreach (var dataset in activeDataSets)
            {
                if (dataset.Path.Contains(name))
                {
                    objectTracker.DeactivateDataSet(dataset);
                }
            }
        }

        #endregion Public Methods
    }
}

#endif