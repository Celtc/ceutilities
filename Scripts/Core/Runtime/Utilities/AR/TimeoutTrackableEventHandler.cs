﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

#if VUFORIA_ANDROID_SETTINGS

namespace CEUtilities.AR
{
    using Vuforia;

    public class TimeoutTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private float timeoutTime = 4f;

        #endregion Exposed fields

        #region Internal fields

        private bool tracked = false;
        private bool active = false;
        private float timer = float.MaxValue;

        protected TrackableBehaviour trackableBehaviour;

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the timeout time.
        /// </summary>
        /// <value>
        /// The timeout time.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Setup", order: 0)]
        public float TimeoutTime
        {
            get
            {
                return timeoutTime;
            }

            set
            {
                timeoutTime = value;
            }
        }

        #endregion Properties

        #region Custom Events

        [FoldoutGroup("Events", order: 1)]
        [Tooltip("Event triggered when the marker is visible in the camera and recognized.")]
        public UnityEvent OnTrackingFoundEvent = new UnityEvent();

        [FoldoutGroup("Events")]
        [Tooltip("Event triggered when the marker lost from recognition.")]
        public UnityEvent OnTrackingLostEvent = new UnityEvent();

        [FoldoutGroup("Events")]
        [Tooltip("Event triggered when the marker is visible, recognized, and now active. An activation event always implies a found event, but not vice versa, since the trackable may be still active after lost.")]
        public UnityEvent OnTrackingActiveEvent = new UnityEvent();

        [FoldoutGroup("Events")]
        [Tooltip("Event triggered when the marker has been lost certain amount of time.")]
        public UnityEvent OnTrackingTimeoutEvent = new UnityEvent();

        #endregion Custom Events

        #region Events methods

        private void Start()
        {
            trackableBehaviour = GetComponent<TrackableBehaviour>();
            if (trackableBehaviour)
            {
                trackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        private void Update()
        {
            if (active && !tracked)
            {
                timer += Time.deltaTime;
                if (timer > TimeoutTime)
                {
                    OnTrackingTimeout();
                }
            }
        }


        /// <summary>
        ///     Implementation of the ITrackableEventHandler function called when the
        ///     tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
            TrackableBehaviour.Status previousStatus,
            TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                Debug.Log("Trackable " + trackableBehaviour.TrackableName + " found");
                OnTrackingFound();
            }
            else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                     newStatus == TrackableBehaviour.Status.NOT_FOUND)
            {
                Debug.Log("Trackable " + trackableBehaviour.TrackableName + " lost");
                OnTrackingLost();
            }
            else
            {
                // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
                // Vuforia is starting, but tracking has not been lost or found yet
                // Call OnTrackingLost() to hide the augmentations
                OnTrackingLost();
            }
        }

#if UNITY_EDITOR

        /// <summary>
        /// Forces the found.
        /// </summary>
        [FoldoutGroup("Runtime", order: 2)]
        [ButtonGroup("Runtime/Force")]
        private void ForceFound() => OnTrackingFound();

        /// <summary>
        /// Forces the lost.
        /// </summary>
        [ButtonGroup("Runtime/Force")]
        private void ForceLost() => OnTrackingLost();

#endif

        #endregion Events methods

        #region Non Public Methods

        /// <summary>
        /// Called when [tracking found].
        /// </summary>
        private void OnTrackingFound()
        {
            var rendererComponents = GetComponentsInChildren<Renderer>(true);
            var colliderComponents = GetComponentsInChildren<Collider>(true);
            var canvasComponents = GetComponentsInChildren<Canvas>(true);

            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;

            // Enable colliders:
            foreach (var component in colliderComponents)
                component.enabled = true;

            // Enable canvas':
            foreach (var component in canvasComponents)
                component.enabled = true;

            tracked = true;
            if (!active)
            {
                active = true;

                OnTrackingActiveEvent.Invoke();
            }

            OnTrackingFoundEvent.Invoke();
        }

        /// <summary>
        /// Called when [tracking lost].
        /// </summary>
        private void OnTrackingLost()
        {
            var rendererComponents = GetComponentsInChildren<Renderer>(true);
            var colliderComponents = GetComponentsInChildren<Collider>(true);
            var canvasComponents = GetComponentsInChildren<Canvas>(true);

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

            // Disable colliders:
            foreach (var component in colliderComponents)
                component.enabled = false;

            // Disable canvas':
            foreach (var component in canvasComponents)
                component.enabled = false;

            tracked = false;
            timer = 0f;

            OnTrackingLostEvent.Invoke();
        }

        /// <summary>
        /// Called when [tracking timeout].
        /// </summary>
        private void OnTrackingTimeout()
        {
            active = false;

            OnTrackingTimeoutEvent.Invoke();
        }

        #endregion Non Public Methods
    }
}

#endif