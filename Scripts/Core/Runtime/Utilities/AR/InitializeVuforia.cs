﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

#if VUFORIA_ANDROID_SETTINGS

namespace CEUtilities.AR
{
    using Vuforia;

    public class InitializeVuforia : SerializedMonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        [ShowInInspector]
        [InfoBox("Use with the option \"Delayed Initialization\" in Vuforia configuration. A delayed initialization is needed in some GPUs to allow video playback to render it's frames. See: https://forum.unity3d.com/threads/released-avpro-video-complete-video-playback-solution.385611/page-18#post-2878977")]
        public bool IsInitialized
        {
            get
            {
                return ApplicationHelper.AppInPlayerOrPlaymode ? VuforiaManager.Instance.Initialized : false;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

#if VUFORIA_ANDROID_SETTINGS
        [HideInInspector]
        [Button]
        public void Initialize()
        {
            VuforiaRuntime.Instance.InitVuforia();
        }

        [HideInInspector]
        [Button]
        public void Deinitialize()
        {
            VuforiaRuntime.Instance.Deinit();
        }
#endif

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}

#endif