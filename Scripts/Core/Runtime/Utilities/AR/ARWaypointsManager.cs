﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.AR
{
    public class ARWaypointsManager : MonoBehaviour
    {
#if VUFORIA_ANDROID_SETTINGS
        #region Exposed fields

        [SerializeField]
        public ARWaypoint[] waypoints;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        public UnityEvent_ARWaypoint OnWaypointChecked = new UnityEvent_ARWaypoint();
        public UnityEvent OnAllWaypointsChecked = new UnityEvent();

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private void Awake()
        {
            RegisterAREvents();
        }

        /// <summary>
        /// Called when a waypoint is checked
        /// </summary>
        private void OnWaypointChecked_Callback(ARWaypoint waypoint)
        {
            OnWaypointChecked.Invoke(waypoint);

            if (waypoints.All(x => x.check))
                OnAllWaypointsChecked.Invoke();
        }

        #endregion Events methods

        #region Methods

        /// <summary>
        /// Register the check events
        /// </summary>
        private void RegisterAREvents()
        {
            // For every waypoint
            foreach (var waypoint in waypoints)
            {
                // For every handler
                foreach (var handler in waypoint.eventHandlers)
                {
                    // Register a successful check
                    handler.OnTrackingFoundEvent.AddListener(() =>
                    {
                        if (waypoint.check) return;

                        waypoint.founds++;

                        // Check waypoint?
                        var foundsRatio = (float)waypoint.founds / (float)waypoint.eventHandlers.Length;
                        if (foundsRatio >= waypoint.minRate)
                        {
                            waypoint.check = true;
                            OnWaypointChecked_Callback(waypoint);
                        }
                    });
                }
            }
        }

        /// <summary>
        /// Reset all waypoints check state
        /// </summary>
        public void ResetWaypoints()
        {
            // For every waypoint
            foreach (var waypoint in waypoints)
            {
                waypoint.check = false;
                waypoint.founds = 0;
            }
        }

        #endregion Methods
#endif
    }
}