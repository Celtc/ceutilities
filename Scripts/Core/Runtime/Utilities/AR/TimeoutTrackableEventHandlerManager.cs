﻿using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace CEUtilities.AR
{
    public class TimeoutTrackableEventHandlerManager : MonoBehaviour
    {
#if VUFORIA_ANDROID_SETTINGS
        #region Exposed fields

        [SerializeField]
        private bool autoRegister;

        [SerializeField]
        [DisableIf("autoRegister")]
        private List<TimeoutTrackableEventHandler> registered;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        [Tooltip("Event triggered when the marker is visible in the camera and recognized.")]
        public UnityEvent_TimeoutTrackableEventHandler OnTrackingFoundEvent = new UnityEvent_TimeoutTrackableEventHandler();

        [Tooltip("Event triggered when the marker lost from recognition.")]
        public UnityEvent_TimeoutTrackableEventHandler OnTrackingLostEvent = new UnityEvent_TimeoutTrackableEventHandler();

        [Tooltip("Event triggered when the marker is visible, recognized, and now active. An activation event always implies a found event, but not vice versa, since the trackable may be still active after lost.")]
        public UnityEvent_TimeoutTrackableEventHandler OnTrackingActiveEvent = new UnityEvent_TimeoutTrackableEventHandler();

        [Tooltip("Event triggered when the marker has been lost certain amount of time.")]
        public UnityEvent_TimeoutTrackableEventHandler OnTrackingTimeoutEvent = new UnityEvent_TimeoutTrackableEventHandler();

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        private void Start()
        {
            if (autoRegister)
            {
                registered = FindObjectsOfType<TimeoutTrackableEventHandler>().ToList();
            }
            foreach (var handler in registered)
            {
                handler.OnTrackingFoundEvent.AddListener(() => OnTrackingFoundEvent.Invoke(handler));
                handler.OnTrackingLostEvent.AddListener(() => OnTrackingLostEvent.Invoke(handler));
                handler.OnTrackingActiveEvent.AddListener(() => OnTrackingActiveEvent.Invoke(handler));
                handler.OnTrackingTimeoutEvent.AddListener(() => OnTrackingTimeoutEvent.Invoke(handler));
            }
        }

        #endregion Events methods

        #region Methods

        #endregion Methods
#endif
    }
}