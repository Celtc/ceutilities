﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

#if VUFORIA_ANDROID_SETTINGS

namespace CEUtilities.AR
{
    [System.Serializable]
    public class ARWaypoint
    {
        [SerializeField]
        public float minRate = 1f;
        [SerializeField]
        public TimeoutTrackableEventHandler[] eventHandlers;
        [SerializeField]
        public UnityEvent OnChecked = new UnityEvent();

        [HideInInspector]
        public int founds = 0;
        [HideInInspector]
        public bool check = false;
    }
}

#endif