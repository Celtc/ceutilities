﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Audio;

namespace CEUtilities.AR
{
    [RequireComponent(typeof(AudioManager))]
    public class ARAudioChanger : MonoBehaviour
    {
#if VUFORIA_ANDROID_SETTINGS
        #region Struct

        [System.Serializable]
        public class TrackableGroupAudio
        {
            [SerializeField]
            [BoxGroup("Trackable Group Audio")]
            public List<TimeoutTrackableEventHandler> trackables = new List<TimeoutTrackableEventHandler>();

            [System.NonSerialized]
            [BoxGroup("Trackable Group Audio/Runtime")]
            [ReadOnly]
            public int active = 0;
        }

        #endregion Struct

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private Dictionary<int, TrackableGroupAudio> audioGroups = new Dictionary<int, TrackableGroupAudio>();

        #endregion Exposed fields

        #region Internal fields

        private int currentIndex = -1;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Gets or sets the audio groups.
        /// </summary>
        /// <value>
        /// The audio groups.
        /// </value>
        [ShowInInspector]
        [HideReferenceObjectPicker]
        public Dictionary<int, TrackableGroupAudio> AudioGroups
        {
            get
            {
                return audioGroups;
            }

            set
            {
                audioGroups = value;
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            if (currentIndex >= 0)
            {
                AudioManager.Instance.ChangeTo(currentIndex);
            }
        }

        private void OnDisable()
        {
            if (currentIndex >= 0)
            {
                AudioManager.Instance.Stop();
            }
        }

        private void Start()
        {
            RegisterDelegates();
        }

        /// <summary>
        /// Called when [group trackable found].
        /// </summary>
        /// <param name="audioIndex">Index of the audio.</param>
        private void OnGroupTrackableFound(int audioIndex)
        {
            if (--AudioGroups[audioIndex].active <= 0 && currentIndex == audioIndex)
            {
                AudioGroups[audioIndex].active = 0;

                // Look for next audio index
                var nextIndex = -1;
                var maxActives = 0;
                foreach(var pair in AudioGroups)
                {
                    if (pair.Value.active > maxActives)
                    {
                        maxActives = pair.Value.active;
                        nextIndex = pair.Key;
                    }
                }

                if (nextIndex >= 0)
                {
                    // Change to next with more actives
                    if (enabled)
                    {
                        AudioManager.Instance.ChangeTo(nextIndex);
                    }
                    currentIndex = audioIndex;
                }
                else
                {
                    // No actives
                    if (enabled)
                    {
                        AudioManager.Instance.Stop();
                    }
                    currentIndex = -1;
                }
            }
        }

        /// <summary>
        /// Called when [group trackable timeout].
        /// </summary>
        /// <param name="audioIndex">Index of the audio.</param>
        private void OnGroupTrackableTimeout(int audioIndex)
        {
            if (++AudioGroups[audioIndex].active > 0 && currentIndex < 0)
            {
                if (enabled)
                {
                    AudioManager.Instance.ChangeTo(audioIndex);
                }
                currentIndex = audioIndex;
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Restarts this instance.
        /// </summary>
        public void Restart()
        {
            AudioManager.Instance.Stop();

            foreach (var group in AudioGroups.Values)
            {
                group.active = 0;
            }
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Registers the delegates.
        /// </summary>
        private void RegisterDelegates()
        {
            foreach (var pair in AudioGroups)
            {
                var audioIndex = pair.Key;
                var trackablesGroup = pair.Value;

                foreach (var trackable in trackablesGroup.trackables)
                {
                    trackable.OnTrackingFoundEvent.AddListener(() => OnGroupTrackableFound(audioIndex));
                    trackable.OnTrackingTimeoutEvent.AddListener(() => OnGroupTrackableTimeout(audioIndex));
                }
            }
        }

        #endregion Non Public Methods
#endif
    }
}