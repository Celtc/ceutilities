﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using CEUtilities.Helpers;

namespace CEUtilities.Networking
{
    /// <summary>
    /// Common interface to all messages
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// Is the message marked as invalid or damaged?
        /// </summary>
        bool Invalid { get; }

        /// <summary>
        /// Type of message
        /// </summary>
        MessageType MessageType { get; }

        /// <summary>
        /// Source UID
        /// </summary>
        string SourceUID { get; set; }

        /// <summary>
        /// Target UID
        /// </summary>
        string TargetUID { get; set; }

        /// <summary>
        /// Message payload
        /// </summary>
        byte[] Payload { get; set; }

        /// <summary>
        /// Construct a message from a byte array
        /// </summary>
        IMessage FromBytes(byte[] data);

        /// <summary>
        /// Construct a message from a byte array
        /// </summary>
        IMessage FromBytes(byte[] data, int offset);

        /// <summary>
        /// Extract bytes
        /// </summary>
        byte[] ToBytes();
    }

    /// <summary>
    /// Strong typed message interface
    /// </summary>
    public interface IMessage<T> : IMessage where T : IMessage
    {
        /// <summary>
        /// Construct a message from a byte array
        /// </summary>
        new T FromBytes(byte[] data, int offset);
    }

    /// <summary>
    /// Base massage class, subscribed to a strong typed message interface
    /// </summary>
    public class Message : IMessage<Message>
    {
        /// <summary>
        /// Is the message marked as invalid or damaged?
        /// </summary>
        public bool Invalid { get; protected set; } = false;

        /// <summary>
        /// Type of message
        /// </summary>
        public virtual MessageType MessageType { get; protected set; }

        /// <summary>
        /// Source UID
        /// </summary>
        public virtual string SourceUID { get; set; } = string.Empty;

        /// <summary>
        /// Target UID
        /// </summary>
        public virtual string TargetUID { get; set; } = string.Empty;

        /// <summary>
        /// Payload of the message
        /// </summary>
        public virtual byte[] Payload { get; set; } = new byte[0];


        /// <summary>
        /// Constructor
        /// </summary>
        public Message(IMessage other)
        {
            Invalid = other.Invalid;
            MessageType = other.MessageType;
            SourceUID = other.SourceUID;
            TargetUID = other.TargetUID;
            Payload = other.Payload;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Message(MessageType type)
        {
            MessageType = type;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Message(MessageType type, byte[] payload)
        {
            MessageType = type;
            Payload = payload;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Message(byte type)
        {
            MessageType = (MessageType)type;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Message(byte type, byte[] payload)
        {
            MessageType = (MessageType)type;
            Payload = payload;
        }

        /// <summary>
        /// Builds a whole message from it's bytes
        /// </summary>
        public Message(byte[] data)
        {
            FromBytes(data);
        }

        /// <summary>
        /// Builds a whole message from it's bytes
        /// </summary>
        public Message(byte[] data, int offset)
        {
            FromBytes(data, offset);
        }


        /// <summary>
        /// Construct a message from a byte array
        /// </summary>
        public virtual Message FromBytes(byte[] data)
        {
            try
            {
                SourceUID = IOHelper.BytesToString(data, 0, 6);
                TargetUID = IOHelper.BytesToString(data, 6, 6);
                MessageType = (MessageType)data[12];
                Payload = new byte[data.Length - 13];
                Array.Copy(data, 13, Payload, 0, data.Length - 13);
            }
            catch (Exception ex)
            {
                Debug.LogError("[Message] Error in message bytes generation: " + ex.Message);
                Invalid = true;
            }
            return this;
        }

        /// <summary>
        /// Construct a message from a byte array
        /// </summary>
        public virtual Message FromBytes(byte[] data, int offset)
        {
            byte[] msgBytes = null;
            try
            {
                msgBytes = new byte[data.Length - offset];
                Array.Copy(data, offset, msgBytes, 0, msgBytes.Length);
            }
            catch (Exception ex)
            {
                Debug.LogError("[Message] Error in message bytes generation: " + ex.Message);
                Invalid = true;
            }
            return Invalid ? this : FromBytes(msgBytes);
        }

        /// <summary>
        /// Extract bytes
        /// </summary>
        public virtual byte[] ToBytes()
        {
            // Check for valid message
            if (Invalid)
            {
                Debug.LogError("[Message] The message is not valid, is damage or it's content are not well defined");
                return null;
            }

            // Check source
            if (string.IsNullOrEmpty(SourceUID) || TargetUID.Length != 6)
            {
                Debug.LogError("[Message] Invalid source UID for serializing message");
                return null;
            }

            // Check target
            if (string.IsNullOrEmpty(TargetUID) || TargetUID.Length != 6)
            {
                Debug.LogError("[NetworkManager] Invalid destination UID  serializing message");
                return null;
            }

            // Set bytes
            byte[] messageBytes = new byte[Payload.Length + 13];

            // Add header
            var sourceUidBytes = IOHelper.StringToBytes(SourceUID);
            var targetUidBytes = IOHelper.StringToBytes(TargetUID);

            for (int i = 0; i < 6; i++) messageBytes[i] = sourceUidBytes[i];         // Source UID
            for (int i = 0; i < 6; i++) messageBytes[6 + i] = targetUidBytes[i];     // Target UID
            messageBytes[12] = (byte)MessageType;                                // Msg type

            // Payload
            Array.Copy(Payload, 0, messageBytes, 13, Payload.Length);

            return messageBytes;
        }


        IMessage IMessage.FromBytes(byte[] data)
        {
            return FromBytes(data);
        }

        IMessage IMessage.FromBytes(byte[] data, int offset)
        {
            return FromBytes(data, offset);
        }
    }
}