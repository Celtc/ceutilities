﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

using Sirenix.OdinInspector;
using CEUtilities.Helpers;

namespace CEUtilities.Networking
{
	public class SimpleUDPClient : MonoBehaviour
	{
        #region Exposed fields
        
        [Header("Network")]

		[SerializeField]
		private int localPort = 1122;		
		[SerializeField]
        [OnValueChanged("Clear")]
        private string targetIP = "255.255.255.255";
		[SerializeField]
        [OnValueChanged("Clear")]
		private int targetPort = 1155;
		[SerializeField]
        private string data = "Hello world!";
        
        [Header("Data Load")]

		[SerializeField]
		private bool loadFromFile = false;
		[ShowIf("loadFromFile")]
		[Indent]
		[SerializeField]
		private string mobileFilePath = "/sdcard/network.config";
		[ShowIf("loadFromFile")]
		[Indent]
		[SerializeField]
		private string standaloneFilepath = "%userprofile%/Desktop/standalone-network.config";

		#endregion Exposed fields

		#region Internal fields

		IPEndPoint remoteEndPoint;
		UdpClient client;

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties

		public int LocalPort
		{
			get
			{
				return localPort;
			}

			set
			{
				localPort = value;
			}
		}

		public string Data
		{
			get
			{
				return data;
			}

			set
			{
				data = value;
			}
		}

		public string TargetIP
		{
			get
			{
				return targetIP;
			}

			set
			{
				targetIP = value;
                Clear();
            }
		}

		public int TargetPort
		{
			get
			{
				return targetPort;
			}

			set
			{
				targetPort = value;
                Clear();
            }
		}

		public bool LoadFromFile
		{
			get
			{
				return loadFromFile;
			}

			set
			{
				loadFromFile = value;
			}
		}

		public string MobileFilepath
		{
			get
			{
				return mobileFilePath;
			}

			set
			{
				mobileFilePath = value;
			}
		}

		public string StandaloneFilepath
		{
			get
			{
				return standaloneFilepath;
			}

			set
			{
				standaloneFilepath = value;
			}
		}

		public string Filepath
		{
			get
			{
				if (!ApplicationHelper.AppInPlaymode)
					return StandaloneFilepath;
#if UNITY_STANDALONE
                return StandaloneFilepath;
#else
                return MobileFilepath;
#endif
            }
        }


        public IPEndPoint RemoteEndPoint
        {
            get
            {
                if (remoteEndPoint == null)
                    remoteEndPoint  = new IPEndPoint(IPAddress.Parse(TargetIP), TargetPort);

                return remoteEndPoint;
            }

            set
            {
                remoteEndPoint = value;
            }
        }

        public UdpClient Client
        {
            get
            {
                if (client == null)
                    client = new UdpClient();

                return client;
            }

            set
            {
                client = value;
            }
        }

        #endregion Properties

        #region Events methods

        private void Start()
		{
			if (LoadFromFile)
				LoadConfig();

            Clear();
		}

		#endregion Events methods

		#region Public Methods

		/// <summary>
		/// Send the data
		/// </summary>
		[Button]
		public void SendData()
		{
			try
			{
				byte[] bytes = Encoding.UTF8.GetBytes(Data);
				Client.Send(bytes, bytes.Length, RemoteEndPoint);

				Debug.Log(string.Format("[UDPClient] Sent {0} bytes of data to {1}", bytes.Length, RemoteEndPoint.Address));
            }
			catch (Exception ex)
			{
				Debug.LogError(ex.ToString());
			}
		}

		#endregion Public Methods

		#region Non Public Methods

		/// <summary>
		/// Load config
		/// </summary>
		private void LoadConfig()
		{
			// Check for file
			var expandedFilepath = Environment.ExpandEnvironmentVariables(Filepath);
			if (!File.Exists(expandedFilepath))
			{
				Debug.LogWarning("[UDPClient] No config file found");
				return;
			}

			// Read content
			var sr = new StreamReader(expandedFilepath);
			var entries = Regex.Split(sr.ReadToEnd(), "\r\n|\r|\n");
			sr.Close();
			sr.Dispose();
			sr = null;

			// Search configs
			string line;

			// Connection
			line = entries.First(x => x.StartsWith("localPort"));
			if (line != null)
				localPort = int.Parse(line.Substring(line.IndexOf('=') + 1));

			line = entries.First(x => x.StartsWith("targetPort"));
			if (line != null)
				targetPort = int.Parse(line.Substring(line.IndexOf('=') + 1));

			line = entries.First(x => x.StartsWith("targetIP"));
			if (line != null)
				targetIP = line.Substring(line.IndexOf('=') + 1);

			// Config
			line = entries.First(x => x.StartsWith("data"));
			if (line != null)
				data = line.Substring(line.IndexOf('=') + 1);
		}
        
        /// <summary>
        /// Clear the instances
        /// </summary>
        private void Clear()
        {
            RemoteEndPoint = null;
        }

		#endregion Non Public Methods
	}
}