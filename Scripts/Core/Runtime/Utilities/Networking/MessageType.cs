﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Networking
{
    public enum MessageType : byte
    {
        // Base network message types:
        
        /// <summary>
        /// Notifies the connection of a new client. Parameter: End Point Address.
        /// </summary>
        CONNECTION = 0,
        /// <summary>
        /// Server sends a request for synchronizint to a new client.
        /// </summary>
        SYNCHRONIZE = 1,
        /// <summary>
        /// Client confirms the request, and may send a preferential UID. Parameter: Preferential UID.
        /// </summary>
        ACKNOWLEDGED = 2,
        /// <summary>
        /// Server confirms the synchronization to client, by sending it the assigned UID (BROADCAST). Parameter: UID.
        /// </summary>
        READY_SYNC = 3,
        /// <summary>
        /// Notifies all the clients of a new synchronization. Parameter: UID.
        /// </summary>
        SYNCHRONIZED = 4,
        /// <summary>
        /// Notifies the desynchronization of a client, possible caused by a disconnection. Parameter: UID.
        /// </summary>
        DESYNCHRONIZED = 5,
        /// <summary>
        /// Notifies the disconnection of a client. Parameter: End Point Address.
        /// </summary>
        DISCONNECTION = 6,

        // User defined types:

        /// <summary>
        /// A command meant for the server.
        /// </summary>
        SERVER_COMMAND = 10,
        /// <summary>
        /// A command send and processed between clients.
        /// </summary>
        CLIENT_COMMAND = 11,
        /// <summary>
        /// An object event notified between clients.
        /// </summary>
        OBJECT_EVENT = 12
    }
}