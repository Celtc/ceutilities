﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using Sirenix.Serialization;
using CEUtilities.Helpers;

namespace CEUtilities.Networking
{
    [System.Serializable]
    public class NetworkClientData
    {
        #region Exposed fields

        [SerializeField]
        [ReadOnly]
        public string uid = NetworkUID.UNASSIGNED_UID;

        [SerializeField]
        public string name = "Unknown";

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Serialize into an string
        /// </summary>
        public virtual string Serialize()
        {
            var bytes = SerializationUtility.SerializeValue(this, DataFormat.JSON);
            return IOHelper.BytesToString(bytes);
        }

        /// <summary>
        /// Deserialize from a string
        /// </summary>
        public static NetworkClientData Deserialize(string data)
        {
            var bytes = IOHelper.StringToBytes(data);
            return SerializationUtility.DeserializeValue<NetworkClientData>(bytes, DataFormat.JSON);
        }

        /// <summary>
        /// Deserialize from a string
        /// </summary>
        public static NetworkClientData Deserialize(string buffer, int offset, int length)
        {
            var data = buffer.Substring(offset, length);
            return Deserialize(data);
        }        

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}