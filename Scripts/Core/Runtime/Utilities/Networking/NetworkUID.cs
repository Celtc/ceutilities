﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.Networking
{
    public static class NetworkUID
    {
        public const string BROADCAST_UID = "\u00ff\u00ff\u00ff\u00ff\u00ff\u00ff";
        
        public const string SERVER_UID = "\0\0\0\0\0\0";
        
        public const string UNASSIGNED_UID = "000000";
    }
}