﻿using UnityEngine;
using System;
using System.Net;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

using Sirenix.OdinInspector;
using Hazel;
using Hazel.Udp;
using CEUtilities.Patterns;
using CEUtilities.UtilityClasses;
using CEUtilities.Helpers;
using CEUtilities.Threading;

namespace CEUtilities.Networking
{
    [DefaultExecutionOrder(-100)]
    [CustomSingleton(AutoGeneration = false, Persistent = true)]
    public class NetworkServer : Singleton<NetworkServer>
    {
        #region Structures

        /// <summary>
        /// Message with connection label
        /// </summary>
        public struct RecvPacket
        {
            // Label
            public Connection client;

            // Message
            public Message msg;
        };

        #endregion

        #region Classes

        //https://stackoverflow.com/posts/9543797/revisions
        //https://stackoverflow.com/questions/9543715/generating-human-readable-usable-short-but-unique-ids?answertab=votes#tab-top
        /// <summary>
        /// Random identifier generator.
        /// </summary>
        public static class RandomIdGenerator
        {
            private static char[] _base62chars =
                "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
                .ToCharArray();

            private static System.Random _random = new System.Random();

            public static string GetBase62(int length)
            {
                var sb = new StringBuilder(length);

                for (int i = 0; i < length; i++)
                    sb.Append(_base62chars[_random.Next(62)]);

                return sb.ToString();
            }

            public static string GetBase36(int length)
            {
                var sb = new StringBuilder(length);

                for (int i = 0; i < length; i++)
                    sb.Append(_base62chars[_random.Next(36)]);

                return sb.ToString();
            }
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private int portNumber = 4296;

        [SerializeField]
        [HideInInspector]
        private bool rememberUids = true;


        private HashSet<Connection> connectedClients = new HashSet<Connection>();

        private BiDictionary<string, Connection> synchronizedClients = new BiDictionary<string, Connection>();

        #endregion Exposed fields

        #region Internal fields

        NetworkEndPoint endPoint;

        ConnectionListener listener;

        private Dictionary<ConnectionEndPoint, string> uidMemory = new Dictionary<ConnectionEndPoint, string>();

        private ConcurrentQueue<RecvPacket> queuedReceivedPackets = new ConcurrentQueue<RecvPacket>();

        #endregion Internal fields

        #region Custom Events

        public event EventHandler<string> OnConnectedClient;

        public event EventHandler<string> OnSynchronizedClient;

        public event EventHandler<string> OnDesynchronizedClient;

        public event EventHandler<string> OnDisconnectedClient;

        public event EventHandler<RecvPacket> OnReceivedPacket;

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Listening port
        /// </summary>
        [ShowInInspector]
        public int PortNumber
        {
            get
            {
                return portNumber;
            }

            set
            {
                portNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [remember uids].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [remember uids]; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        public bool RememberUids
        {
            get
            {
                return rememberUids;
            }

            set
            {
                rememberUids = value;
            }
        }

        /// <summary>
        /// Is the server running?
        /// </summary>
        [ShowInInspector]
        [ReadOnly]
        public bool Running { get; private set; }

        /// <summary>
        /// Gets or sets the connected clients.
        /// </summary>
        /// <value>
        /// The connected clients.
        /// </value>
        public HashSet<Connection> ConnectedClients
        {
            get
            {
                return connectedClients;
            }

            set
            {
                connectedClients = value;
            }
        }

        /// <summary>
        /// Gets or sets the synchronized clients.
        /// </summary>
        /// <value>
        /// The synchronized clients.
        /// </value>
        public BiDictionary<string, Connection> SynchronizedClients
        {
            get
            {
                return synchronizedClients;
            }

            set
            {
                synchronizedClients = value;
            }
        }

        /// <summary>
        /// Has clients?
        /// </summary>
        public bool HasClients => SynchronizedClients != null && SynchronizedClients.Count > 0;

#if UNITY_EDITOR

        /// <summary>
        /// Gets the displayable synchronized clients.
        /// </summary>
        /// <value>
        /// The displayable synchronized clients.
        /// </value>
        [ShowInInspector]
        [Spacing(Before = 5)]
        [TableMatrixExtended(ColumnLabelsMemberName = "ColumnsLabels")]
        private string[,] DisplayableClients
        {
            get
            {
                if (ConnectedClients == null || ConnectedClients.Count == 0)
                {
                    return new string[2, 0];
                }

                var result = new string[2, ConnectedClients.Count];
                for (int i = 0; i < ConnectedClients.Count; i++)
                {
                    var connection = ConnectedClients.ElementAt(i);
                    result[0, i] = connection.EndPoint.ToString();
                    result[1, i] = SynchronizedClients.ContainsKeyBySecond(connection) ?
                        SynchronizedClients[connection] :
                        string.Empty;
                }
                return result;
            }
        }

        /// <summary>
        /// Gets the columns labels.
        /// </summary>
        /// <value>
        /// The columns labels.
        /// </value>
        private string[] ColumnsLabels => new string[] { "Address", "UID" };

#endif

        #endregion Properties

        #region Events methods

        public void Start()
        {
            StartServer();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            ShutdownServer();
        }

        protected override void OnApplicationQuit()
        {
            base.OnApplicationQuit();

            ShutdownServer();
        }


        /// <summary>
        /// A new client connects
        /// </summary>
        private void NewConnectionHandler(object sender, NewConnectionEventArgs args)
        {
            var connection = args.Connection;

            // Register
            connection.DataReceived += DataReceivedHandler;
            connection.Disconnected += ClientDisconnectHandler;

            // Add to collection
            ConnectedClients.Add(connection);

            // Log
            VerboseLog(string.Format("New client connection from end point '{0}'.", connection.EndPoint));

            // Event
            if (OnConnectedClient != null)
            {
                UnityMainThreadDispatcher.Instance.Enqueue(() => OnConnectedClient(this, connection.EndPoint.ToString()));
            }

            // Communicate connection to other clients
            var msg = new Message(MessageType.CONNECTION, IOHelper.StringToBytes(connection.EndPoint.ToString()))
            {
                TargetUID = NetworkUID.BROADCAST_UID
            };
            SendNetMessage(msg);

            // Send SYN message to client
            msg = new Message(MessageType.SYNCHRONIZE)
            {
                TargetUID = NetworkUID.UNASSIGNED_UID
            };
            SendNetMessage(msg, connection);

            // Cleanup
            args.Recycle();
        }

        /// <summary>
        /// Clients the disconnect handler.
        /// </summary>
		private void ClientDisconnectHandler(object sender, DisconnectedEventArgs args)
        {
            var connection = (Connection)sender;

            // Was synchronized?
            if (SynchronizedClients.ContainsKeyBySecond(connection))
            {
                var uid = SynchronizedClients[connection];

                // Remove
                SynchronizedClients.RemoveByFirst(uid);

                // Log
                VerboseLog(string.Format("Client with UID '{0}' desynchronized.", uid));

                // Communicate desynchronization to other clients
                var msg = new Message(MessageType.DESYNCHRONIZED, IOHelper.StringToBytes(uid))
                {
                    TargetUID = NetworkUID.BROADCAST_UID
                };
                SendNetMessage(msg);

                // Event
                if (OnDesynchronizedClient != null)
                {
                    UnityMainThreadDispatcher.Instance.Enqueue(() => OnDesynchronizedClient(this, uid));
                }
            }

            // Was connected?
            if (ConnectedClients.Contains(connection))
            {
                // Remove
                ConnectedClients.Remove(connection);

                // Log
                VerboseLog(string.Format("Client at end point '{0}' disconnected.", connection.EndPoint));

                // Communicate desynchronization to other clients
                var msg = new Message(MessageType.DISCONNECTION, IOHelper.StringToBytes(connection.EndPoint.ToString()))
                {
                    TargetUID = NetworkUID.BROADCAST_UID
                };
                SendNetMessage(msg);

                // Event
                if (OnDisconnectedClient != null)
                {
                    UnityMainThreadDispatcher.Instance.Enqueue(() => OnDisconnectedClient(this, connection.EndPoint.ToString()));
                }
            }

            // Clean up
            args.Recycle();
        }

        /// <summary>
        /// Datas the received handler.
        /// </summary>
		private void DataReceivedHandler(object sender, DataReceivedEventArgs args)
        {
            var connection = (Connection)sender;
            var uid = SynchronizedClients.ContainsKeyBySecond(connection) ? SynchronizedClients[connection] : "Unkwnon";

            // Reconstruct message
            var msg = new Message(args.Bytes);
            if (msg.Invalid)
            {
                Debug.LogError(string.Format("[NetworkServer] Invalid message received from end point '{0}', UID '{1}'",
                    connection.EndPoint,
                    uid
                ));
                return;
            }

            // Packet the message
            var packet = new RecvPacket
            {
                client = connection,
                msg = msg
            };

            // Verbose
            VerboseLog(string.Format("Received packet from end point '{0}', UID '{1}', {2} bytes: {3}",
                connection.EndPoint,
                uid,
                args.Bytes.Length,
                string.Join(", ", args.Bytes)
            ));

            // Event
            if (OnReceivedPacket != null)
            {
                UnityMainThreadDispatcher.Instance.Enqueue(() => OnReceivedPacket(this, packet));
            }

            // Add queue to process
            queuedReceivedPackets.Enqueue(packet);
            ThreadPool.QueueUserWorkItem((arg) => ProcessQueueEntry());

            // Cleanup
            args.Recycle();
        }

        /// <summary>
        /// Called when [received message uid preference].
        /// </summary>
        private void OnReceivedMessageAcknowledged(Connection sourceConnection, string preferentialUID)
        {
            string uid;

            // Log
            VerboseLog(string.Format("Received acknowleged message from connection '{0}'.", sourceConnection.EndPoint));

            // Is the message sent from an already synchronized client?
            if (SynchronizedClients.ContainsKeyBySecond(sourceConnection))
            {
                uid = SynchronizedClients[sourceConnection];
            }

            // Not synchronized client
            else
            {
                // Valid preferential UID?
                if (NewUidIsValid(preferentialUID))
                {
                    uid = preferentialUID;
                }

                // Should try to remember the UID?
                else if (RememberUids)
                {
                    uid = uidMemory.GetOrAdd(sourceConnection.EndPoint, () => RandomIdGenerator.GetBase62(6));
                }

                // Generates a brand new UID
                else
                {
                    uid = RandomIdGenerator.GetBase62(6);
                }
            }

            // Add client to the Synchronized collection
            SynchronizedClients.SetOrAddBySecond(sourceConnection, uid);

            // Send confirmation to client
            var msg = new Message(MessageType.READY_SYNC, IOHelper.StringToBytes(uid)) { TargetUID = uid };
            SendNetMessage(msg);

            // Notify the rest of the clients of the synchronization
            msg = new Message(MessageType.SYNCHRONIZED, IOHelper.StringToBytes(uid)) { TargetUID = NetworkUID.BROADCAST_UID };
            SendNetMessage(msg);

            // Event
            if (OnSynchronizedClient != null)
            {
                UnityMainThreadDispatcher.Instance.Enqueue(() => OnSynchronizedClient(this, uid));
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Start the server
        /// </summary>
        public void StartServer()
        {
            // Init
            endPoint = new NetworkEndPoint(IPAddress.Any, PortNumber);
            listener = new UdpConnectionListener(endPoint);

            // Start server
            try
            {
                Debug.Log("[NetworkServer] Starting server...", this);

                listener.NewConnection += NewConnectionHandler;
                listener.Start();
                Running = true;

                Debug.LogFormat(this, "[NetworkServer] Server listening on '{0}'.", (listener as UdpConnectionListener).EndPoint);
            }
            catch (Exception ex)
            {
                listener.Close();
                Running = false;

                if (ex != null && ex.InnerException is SocketException && (ex.InnerException as SocketException).ErrorCode == 10048)
                {
                    Debug.LogWarning("[NetworkServer] Socket in use, possible another server running. Error: " + ex.InnerException.Message, this);
                }
                else
                {
                    Debug.LogError("[NetworkServer] Couldn't start server. Unknown error: " + ex.Message, this);
                }
            }
        }

        /// <summary>
        /// Close the server
        /// </summary>
        public void ShutdownServer()
        {
            if (Running)
            {
                Debug.Log("[NetworkServer] Shutting down server...", this);

                listener?.Close();
            }
        }


        /// <summary>
        /// Sends the net message.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        public void SendNetMessage(IMessage msg) => SendNetMessage(msg, SendOption.Reliable, null);

        /// <summary>
        /// Sends the net message.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <param name="forcedEndPoint">The forced end point.</param>
        public void SendNetMessage(IMessage msg, Connection forcedEndPoint) => SendNetMessage(msg, SendOption.Reliable, forcedEndPoint);

        /// <summary>
        /// Sends the net message.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <param name="sendOption">The send option.</param>
        public void SendNetMessage(IMessage msg, SendOption sendOption) => SendNetMessage(msg, sendOption, null);

        /// <summary>
        /// Sends the net message.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <param name="sendOption">The send option.</param>
        /// <param name="forcedEndPoint">In case of not a broadcast message, it forces the message send to this end point.</param>
        public void SendNetMessage(IMessage msg, SendOption sendOption, Connection forcedEndPoint)
        {
            // Check valid message
            if (msg.Invalid)
            {
                Debug.LogError("[NetworkServer] Trying to send an invalid message.");
                return;
            }

            // Skip if sent to server
            if (msg.TargetUID == NetworkUID.SERVER_UID)
            {
                VerboseLog("Skipped message redirection targeted to server.");
                return;
            }

            // Check source
            if (string.IsNullOrEmpty(msg.SourceUID))
            {
                msg.SourceUID = NetworkUID.SERVER_UID;
            }

            // Get bytes
            var messageBytes = msg.ToBytes();

            // Verbose
            VerboseLog(
                string.Format("{0} message to '{1}' from '{2}', {3} bytes: '{4}'.",
                    msg.SourceUID == NetworkUID.SERVER_UID ? "Sending" : "Redirecting",
                    forcedEndPoint != null ? forcedEndPoint.EndPoint.ToString() :
                        msg.TargetUID == NetworkUID.BROADCAST_UID ? "BROADCAST" : msg.TargetUID,
                    msg.SourceUID == NetworkUID.SERVER_UID ? "SERVER" : msg.SourceUID,
                    messageBytes.Length,
                    string.Join(", ", messageBytes)
                )
            );

            // Send to broadcast
            if (msg.TargetUID == NetworkUID.BROADCAST_UID)
            {
                foreach (var client in SynchronizedClients)
                {
                    if (client.Item1 != msg.SourceUID)  // Avoid broadcast to source
                    {
                        client.Item2.SendBytes(messageBytes, sendOption);
                    }
                }
            }

            // Manually set end point?
            else if (forcedEndPoint != null)
            {
                forcedEndPoint.SendBytes(messageBytes, sendOption);
            }

            // Send to specific client
            else if (SynchronizedClients.ContainsKeyByFirst(msg.TargetUID))
            {
                SynchronizedClients[msg.TargetUID].SendBytes(messageBytes, sendOption);
            }

            // Unknown target
            else
            {
                Debug.LogErrorFormat(this, "[NetworkServer] Trying to send message to unknown UID '{0}'.",
                    msg.TargetUID
                );
            }
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Preferentials the uid is valid.
        /// </summary>
        /// <param name="sourceConnection">The source connection.</param>
        /// <param name="uid">The uid.</param>
        /// <returns></returns>
        private bool NewUidIsValid(string uid)
        {
            return !string.IsNullOrEmpty(uid)
                && uid.Length == 6
                && uid != NetworkUID.UNASSIGNED_UID 
                && !SynchronizedClients.ContainsKeyByFirst(uid);
        }

        /// <summary>
        /// Extract a queued packet and process it.
        /// </summary>
        private void ProcessQueueEntry()
        {
            // Process loop
            RecvPacket packet;
            while (!queuedReceivedPackets.IsEmpty)
            {
                // Extract packet
                if (!queuedReceivedPackets.TryDequeue(out packet))
                {
                    break;
                }

                // Should redirect?
                if (packet.msg.TargetUID != NetworkUID.SERVER_UID)
                {
                    SendNetMessage(packet.msg);
                }

                // Process packet meant to server
                else
                {
                    switch (packet.msg.MessageType)
                    {
                        // Acknowledged packet
                        case MessageType.ACKNOWLEDGED:
                            OnReceivedMessageAcknowledged(packet.client, IOHelper.BytesToString(packet.msg.Payload));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Prints if verbose
        /// </summary>
        private void VerboseLog(string msg)
        {
#if VRLIFT_COMMS_VERBOSE
            Debug.Log("[NetworkServer] " + msg);
#endif
        }

        #endregion Non Public Methods
    }
}