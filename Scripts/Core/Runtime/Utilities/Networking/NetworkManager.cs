﻿using UnityEngine;
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using Sirenix.OdinInspector;
using Sirenix.Serialization;
using Hazel;
using Hazel.Udp;
using CEUtilities.Patterns;
using CEUtilities.Threading;
using CEUtilities.Helpers;
using System.IO;
using System.Text.RegularExpressions;

namespace CEUtilities.Networking
{
    [CustomSingleton(AutoGeneration = false, Persistent = true)]
    public class NetworkManager : Singleton<NetworkManager>
    {
        #region Exposed fields

        [OdinSerialize]
        [HideInInspector]
        protected NetworkClientData clientData;

        [SerializeField]
        [HideInInspector]
        private string address = "127.0.0.1";

        [SerializeField]
        [HideInInspector]
        private int port = 4296;

        [SerializeField]
        [HideInInspector]
        private bool connectOnStart = true;

        [SerializeField]
        [HideInInspector]
        private bool automaticReconnect = true;

        [SerializeField]
        [HideInInspector]
        private float retryTime = 5f;

        [SerializeField]
        [HideInInspector]
        private bool readConfig = false;

        [SerializeField]
        [HideInInspector]
        private string configFile = "NetworkManager.config";


        private Func<IMessage, bool> inboundRules = null;

        private Func<IMessage, bool> outboundRules = null;

        #endregion Exposed fields

        #region Internal fields

        private bool connecting;

        private Connection serverConnection;

        private bool synchronized = false;

        private UnityMainThreadDispatcher mainThreadDispatcher;

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// This client data
        /// </summary>
        public NetworkClientData ClientData
        {
            get
            {
                if (clientData == null)
                {
                    clientData = new NetworkClientData();
                }

                return clientData as NetworkClientData;
            }
        }

        /// <summary>
        /// Unique ID of this game instance among the server
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Client Data", order: 1)]
        [ReadOnly]
        public virtual string Uid
        {
            get
            {
                return ClientData.uid;
            }

            protected set
            {
                ClientData.uid = value;
            }
        }

        /// <summary>
        /// Unique ID of this game instance among the server
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Client Data")]
        public virtual string Name
        {
            get
            {
                return ClientData.name;
            }

            protected set
            {
                ClientData.name = value;
            }
        }


        /// <summary>
        /// Address of the server
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Server configuration", order: 2)]
        [Regex(@"^(?:\d{1,3}\.){3}\d{1,3}$", "Invalid IPv4 address!\nExample: '127.0.0.1'")]
        public string Address
        {
            get
            {
                return address;
            }

            set
            {
                address = value;
            }
        }

        /// <summary>
        /// Port of the server
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Server configuration")]
        public int Port
        {
            get
            {
                return port;
            }

            set
            {
                port = value;
            }
        }

        /// <summary>
        /// Connect on start?
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Server configuration")]
        public bool ConnectOnStart
        {
            get
            {
                return connectOnStart;
            }

            set
            {
                connectOnStart = value;
            }
        }

        /// <summary>
        /// Should reconnect automatically?
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Server configuration")]
        public bool AutomaticReconnect
        {
            get
            {
                return automaticReconnect;
            }

            set
            {
                automaticReconnect = value;
            }
        }

        /// <summary>
        /// Retry time for reconnection
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Server configuration")]
        [Indent]
        [EnableIf("automaticReconnect")]
        [LabelText("Retry Time (seconds)")]
        public float RetryTime
        {
            get
            {
                return retryTime;
            }

            set
            {
                retryTime = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Config file", order: 4)]
        [InfoBox("Config paths are relative to the streaming folder")]
        public bool ReadConfig
        {
            get
            {
                return readConfig;
            }

            set
            {
                readConfig = value;
            }
        }

        [ShowInInspector]
        [FoldoutGroup("Config file")]
        [EnableIf("ReadConfig")]
        public string ConfigFile
        {
            get
            {
                return configFile;
            }

            set
            {
                configFile = value;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="NetworkManager"/> is connecting.
        /// </summary>
        /// <value>
        ///   <c>true</c> if connecting; otherwise, <c>false</c>.
        /// </value>
        [ShowInInspector]
        [FoldoutGroup("Runtime", order: 5)]
        [ReadOnly]
        public bool Connecting
        {
            get
            {
                return connecting;
            }

            set
            {
                connecting = value;
            }
        }

        /// <summary>
        /// Is connected to a server?
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        [ReadOnly]
        public bool Connected => serverConnection?.State == ConnectionState.Connected;

        /// <summary>
        /// Is synchronized with a server?
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        [ReadOnly]
        public bool Synchronized
        {
            get
            {
                return synchronized;
            }

            protected set
            {
                synchronized = value;
            }
        }


        /// <summary>
        /// Has Inbound Rules?
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        public bool HasInboundRules => inboundRules != null && inboundRules.GetInvocationList().Length > 0;

        /// <summary>
        /// Has Outbound Rules?
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        public bool HasOutboundRules => outboundRules != null && outboundRules.GetInvocationList().Length > 0;


        /// <summary>
        /// Allows filtering incoming messages which satisfy certain conditions
        /// </summary>
        public Func<IMessage, bool> InboundRules
        {
            get
            {
                return inboundRules;
            }

            set
            {
                inboundRules = value;
            }
        }

        /// <summary>
        /// Filter out outgoing messages
        /// </summary>
        public Func<IMessage, bool> OutboundRules
        {
            get
            {
                return outboundRules;
            }

            set
            {
                outboundRules = value;
            }
        }

        #endregion Properties

        #region Custom Events

        [ShowInInspector]
        [FoldoutGroup("Events", order: 3)]
        public SimpleEventHandler<string> OnConnected;

        [ShowInInspector]
        [FoldoutGroup("Events")]
        public SimpleEventHandler OnConnectionLost;

        [ShowInInspector]
        [FoldoutGroup("Events")]
        public SimpleEventHandler<Message> OnReceiveMessage;

        #endregion Custom Events

        #region Events methods

        protected override void Awake()
        {
            base.Awake();
        }

        protected virtual void Start()
        {
            // Read files
            if (ReadConfig)
            {
                ReadConfigFile();
            }

            // Prepare disptacher for using in other threads
            mainThreadDispatcher = UnityMainThreadDispatcher.Instance;

            if (ConnectOnStart)
            {
                Connect();
            }
        }

        /// <summary>
        /// Raises the destroy event
        /// </summary>
        protected override void OnDestroy()
        {
            base.OnDestroy();

            if (serverConnection != null)
            {
                serverConnection.Close();
            }
        }


        /// <summary>
        /// Called when [received message synchronize].
        /// </summary>
        private void OnReceivedMessageSynchronize()
        {
            // Log
            VerboseLog("Received Synchronize message from server.");

            // Reply with preferential UID
            var msg = new Message(MessageType.ACKNOWLEDGED)
            {
                TargetUID = NetworkUID.SERVER_UID,
                Payload = IOHelper.StringToBytes(Uid)
            };
            SendNetMessage(msg);
        }

        /// <summary>
        /// Called when [received message ready sync].
        /// </summary>
        private void OnReceivedMessageReadySync(string uid)
        {
            // Log
            VerboseLog("Received ready sync message from server.");

            // Set UID
            Uid = uid;

            // Set flag
            Synchronized = true;

            // Log
            LogFormat("Synchronized with server, UID assigned '{0}'.", Uid);
        }


#if UNITY_EDITOR

        [OnInspectorGUI]
        protected virtual void OnInspectorGUI()
        {
            if (clientData == null)
            {
                clientData = new NetworkClientData();
            }
        }

#endif

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Try to connect to the endpoint
        /// </summary>
        [HideIf("Connected")]
        [HideInEditorMode]
        [Button]
        public virtual void Connect()
        {
            if (Connecting)
            {
                StopCoroutine(_Connect());

                Connecting = false;
            }

            StartCoroutine(_Connect());
        }

        /// <summary>
        /// Send a message
        /// </summary>
        public virtual void SendNetMessage(IMessage msg)
        {
            // Check if already has and Uid, if not means we have not handshake
            if (Uid == NetworkUID.UNASSIGNED_UID && msg.MessageType != MessageType.ACKNOWLEDGED)
            {
                Log("Not synchronized, cannot send messages apart from Handshake messages.", InfoMessageType.Error);
                return;
            }

            // Check valid message
            if (msg.Invalid)
            {
                Log("Trying to send an invalid message.", InfoMessageType.Error);
                return;
            }

            // Check source
            if (string.IsNullOrEmpty(msg.SourceUID))
            {
                msg.SourceUID = Uid;
            }

            // Outbound filtering
            var filtered = OutboundRules != null && !OutboundRules(msg);
            if (filtered)
            {
                VerboseLog(string.Format("A message about to be sent to UID '{0}' was filtered out, possible reroute.",
                    msg.TargetUID == NetworkUID.SERVER_UID ? "SERVER" : msg.TargetUID
                ));
                return;
            }

            // Extract bytes
            var messageBytes = msg.ToBytes();

            // Verbose
            VerboseLog(
                string.Format("Sending message to UID '{0}', {1} bytes: {2}.",
                    msg.TargetUID == NetworkUID.BROADCAST_UID ? "BROADCAST" :
                    msg.TargetUID == NetworkUID.SERVER_UID ? "SERVER" : msg.TargetUID,
                    messageBytes.Length,
                    string.Join(", ", messageBytes)
                )
            );

            // Send
            serverConnection.SendBytes(messageBytes, SendOption.Reliable);
        }

        /// <summary>
        /// Send a message to multiple UIDs
        /// </summary>
        public virtual void SendNetMessage(string[] uids, IMessage msg)
        {
            // Check if already has and Uid, if not means we have not handshake
            if (Uid == NetworkUID.UNASSIGNED_UID && msg.MessageType != MessageType.SYNCHRONIZE)
            {
                Log("Not connected or missing handshake validation", InfoMessageType.Error);
                return;
            }

            // Check valid message
            if (msg.Invalid)
            {
                Log("Trying to send an invalid message", InfoMessageType.Error);
                return;
            }

            // Check source
            if (string.IsNullOrEmpty(msg.SourceUID))
            {
                msg.SourceUID = Uid;
            }

            // Send
            byte[] messageBytes = null;
            List<string> sentUids = new List<string>();
            for (int i = 0; i < uids.Length; i++)
            {
                // Set target UID
                msg.TargetUID = uids[i];

                // Outbound filtering
                var filtered = OutboundRules != null && !OutboundRules(msg);
                if (filtered)
                {
                    VerboseLog(string.Format("A message about to be sent to UID '{0}' was filtered out, possible reroute",
                        msg.TargetUID == NetworkUID.SERVER_UID ? "SERVER" : msg.TargetUID
                    ));

                    continue;
                }

                // Extract bytes
                messageBytes = msg.ToBytes();

                // Send
                serverConnection.SendBytes(messageBytes, SendOption.Reliable);
            }

            // Verbose
            if (sentUids.Count > 0)
                VerboseLog(string.Format("Sending message to UIDs '{0}', {1} bytes: {2}",
                        string.Join(", ", sentUids),
                        messageBytes.Length,
                        string.Join(", ", messageBytes)
                ));
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Read config file
        /// </summary>
        private void ReadConfigFile()
        {
            // Check for file
            var configPath = Application.streamingAssetsPath + "/" + ConfigFile;
            if (!File.Exists(configPath))
            {
                return;
            }

            // Read content
            var sr = new StreamReader(configPath, Encoding.GetEncoding("iso-8859-1"));
            var text = sr.ReadToEnd().ToUpper();
            var entries = Regex.Split(text, "\r\n|\r|\n");
            sr.Close();
            sr.Dispose();
            sr = null;

            // Search configs
            string line;

            // Connection
            line = entries.FirstOrDefault(x => x.StartsWith("ADDRESS"));
            if (!string.IsNullOrEmpty(line))
            {
                Address = line.Substring(line.IndexOf('=') + 1);
            }

            line = entries.FirstOrDefault(x => x.StartsWith("PORT"));
            if (!string.IsNullOrEmpty(line))
            {
                Port = int.Parse(line.Substring(line.IndexOf('=') + 1));
            }
        }

        /// <summary>
        /// Connects to the client
        /// </summary>
        protected virtual IEnumerator _Connect()
        {
            var endPoint = new NetworkEndPoint(Address, Port);
            serverConnection = new UdpClientConnection(endPoint);
            serverConnection.DataReceived += HandleDataFromServer;
            serverConnection.Disconnected += HandleServerDisconnect;

            // Ensure singleness
            if (Connecting)
            {
                LogError("Already trying to connect.");
                yield break;
            }

            // Log
            LogFormat("Trying to connect to '{0}'.", endPoint);

            // Set flag
            Connecting = true;

            // Loop
            while (!Connected)
            {
                // Connect in another thread
                var working = true;
                Exception error = null;
                BackgroundWorkerDispatcher.Instance.Enqueue
                (
                    () => serverConnection.Connect(),
                    (result) =>
                    {
                        working = false;
                        error = result.error;
                    }
                );

                // Yield for result
                yield return new WaitUntil(() => !working);

                // Check if failed connection
                if (!Connected)
                {
                    // Log
                    LogErrorFormat("Server connecting failed. Error: '{0}'.",
                        error?.Message);

                    // Should retry?
                    if (AutomaticReconnect)
                    {
                        // Log
                        LogFormat("Retrying connection to '{0}' in '{1}' seconds.", endPoint, RetryTime);

                        yield return new WaitForSeconds(RetryTime);
                    }

                    // Stop trying
                    else
                    {
                        break;
                    }
                }

                // Connected
                else
                {
                    // Log
                    LogFormat("Connected to '{0}'.", endPoint);
                }
            }

            // Set flag
            Connecting = false;
        }
        

        /// <summary>
        /// Received data from server callback
        /// </summary>
        protected virtual void HandleDataFromServer(object sender, DataReceivedEventArgs args)
        {
            // Make a deep copy to avoid pointer reference
            var bytes = new byte[args.Bytes.Length];
            Array.Copy(args.Bytes, bytes, bytes.Length);

            // Dispatch
            mainThreadDispatcher?.Enqueue(() =>
                ReceiveNetMessage(bytes)
            );

            args.Recycle();
        }

        /// <summary>
        /// Server disconnection callback
        /// </summary>
        protected virtual void HandleServerDisconnect(object sender, DisconnectedEventArgs args)
        {
            var connection = (Connection)sender;

            Log("Server connection at " + connection.EndPoint + " lost");

            // Flags
            serverConnection = null;
            Synchronized = false;
            
            // Clears
            args.Recycle();

            // Event
            OnConnectionLost?.Invoke();

            // Should try to connect?
            if (AutomaticReconnect)
            {
                Connect();
            }
        }
        

        /// <summary>
        /// Receives data from server (server or another client through the server)
        /// </summary>
        protected virtual void ReceiveNetMessage(byte[] data)
        {
            // Reconstruct message
            var msg = new Message(data);
            if (msg.Invalid)
            {
                Log(string.Format("Invalid message received from possible UID '{0}'",
                    msg.SourceUID == NetworkUID.SERVER_UID ? "SERVER" : msg.SourceUID
                ), InfoMessageType.Error);
                return;
            }

            // Inbound filtering
            var filtered = InboundRules != null && !InboundRules(msg);
            if (filtered)
            {
                VerboseLog(string.Format("A message from UID '{0}' was filtered out",
                    msg.SourceUID == NetworkUID.SERVER_UID ? "SERVER" : msg.SourceUID
                ));
                return;
            }

            // Verbose
            VerboseLog(string.Format("Received message from UID '{0}', {1} bytes: {2}",
                msg.SourceUID == NetworkUID.SERVER_UID ? "SERVER" : msg.SourceUID,
                data.Length,
                string.Join(", ", data))
            );

            // Process
            ProcessMessage(msg);
        }

        /// <summary>
        /// Process a received message
        /// </summary>
        protected virtual void ProcessMessage(Message msg)
        {
            // Check message type to tell how to handle the msg
            switch(msg.MessageType)
            {
                case MessageType.SYNCHRONIZE:
                    OnReceivedMessageSynchronize();
                    break;

                case MessageType.READY_SYNC:
                    OnReceivedMessageReadySync(IOHelper.BytesToString(msg.Payload));
                    break;

                default:
                    OnReceiveMessage?.Invoke(msg);
                    break;
            }
        }

        #region Logging

        /// <summary>
        /// Prints an standard message.
        /// </summary>
        protected virtual void LogWarningFormat(string msg, params object[] args) => LogWarning(string.Format(msg, args));

        /// <summary>
        /// Prints an standard message.
        /// </summary>
        protected virtual void LogWarning(string msg) => Log(msg, InfoMessageType.Warning);


        /// <summary>
        /// Prints an standard message.
        /// </summary>
        protected virtual void LogErrorFormat(string msg, params object[] args) => LogError(string.Format(msg, args));

        /// <summary>
        /// Prints an standard message.
        /// </summary>
        protected virtual void LogError(string msg) => Log(msg, InfoMessageType.Error);


        /// <summary>
        /// Prints an standard message.
        /// </summary>
        protected virtual void LogFormat(string msg, params object[] args) => Log(string.Format(msg, args));

        /// <summary>
        /// Prints an standard message.
        /// </summary>
        protected virtual void Log(string msg, InfoMessageType mode = InfoMessageType.Info)
        {
            var fullMsg = string.Format("[NetworkManager] [UID: {0}] {1}",
                Uid == NetworkUID.UNASSIGNED_UID ? "Unassigned" : Uid,
                msg);

            switch (mode)
            {
                case InfoMessageType.Warning: Debug.LogWarning(fullMsg, this); break;
                case InfoMessageType.Error: Debug.LogError(fullMsg, this); break;
                default: Debug.Log(fullMsg, this); break;
            }
        }


        /// <summary>
        /// Prints if verbose
        /// </summary>
        protected virtual void VerboseLog(string msg)
        {
#if VRLIFT_COMMS_VERBOSE
            Debug.Log(string.Format("[NetworkManager] [UID: {0}] {1}",
                Uid == NetworkUID.UNASSIGNED_UID ? "Unassigned" : Uid,
                msg)
            );
#endif
        }

        #endregion Logging

        #endregion Non Public Methods
    }
}