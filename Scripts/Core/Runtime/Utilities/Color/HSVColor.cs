﻿/// <summary>
/// HSVColor v1.1.0 by Christian Chomiak, christianchomiak@gmail.com
/// 
/// Struct to represent colors in the HSV (hue, saturation, value) system.
/// Includes:
///     * Functions to convert from/to RGB and to HEX.
///     * Basic operations: ==, !=, +, -, *, /
///     * Shortcuts to create: black and white instances.
/// </summary>
using UnityEngine;

using CEUtilities.Helpers;

namespace CEUtilities.Colors
{
    [System.Serializable]
    public struct HSVColor
    {
        #region Static Accessors

        /// <summary>
        /// Solid white. HSVA is (0, 0, 1, 1).
        /// </summary>
        public static HSVColor white
        {
            get { return new HSVColor(0f, 0f, 1f, 1f); }
        }

        /// <summary>
        /// Solid white. HSVA is (0, 0, 0, 1).
        /// </summary>
        public static HSVColor black
        {
            get { return new HSVColor(0f, 0f, 0f, 1f); }
        }

        /// <summary>
        /// Solid blue. HSVA is (240, 1, 1, 1).
        /// </summary>
        public static HSVColor blue
        {
            get { return new HSVColor(240f, 1f, 1f, 1f); }
        }

        /// <summary>
        /// Solid clear, transparent. HSVA is (0, 0, 0, 0).
        /// </summary>
        public static HSVColor clear
        {
            get { return new HSVColor(0f, 0f, 0f, 0f); }
        }

        /// <summary>
        /// Solid cyan. HSVA is (180, 1, 1, 1).
        /// </summary>
        public static HSVColor cyan
        {
            get { return new HSVColor(180f, 1f, 1f, 1f); }
        }

        /// <summary>
        /// Solid gray. HSVA is (0, 0, 50, 1).
        /// </summary>
        public static HSVColor gray
        {
            get { return new HSVColor(0f, 0f, 50f, 1f); }
        }

        /// <summary>
        /// Solid green. HSVA is (120, 1, 1, 1).
        /// </summary>
        public static HSVColor green
        {
            get { return new HSVColor(120f, 1f, 1f, 1f); }
        }

        /// <summary>
        /// Solid grey. HSVA is (0, 0, 1, 1).
        /// </summary>
        public static HSVColor grey
        {
            get { return HSVColor.gray; }
        }

        /// <summary>
        /// Solid magenta. HSVA is (300, 1, 1, 1).
        /// </summary>
        public static HSVColor magenta
        {
            get { return new HSVColor(300f, 1f, 1f, 1f); }
        }

        /// <summary>
        /// Solid red. HSVA is (0, 1, 1, 1).
        /// </summary>
        public static HSVColor red
        {
            get { return new HSVColor(0f, 1f, 1f, 1f); }
        }

        /// <summary>
        /// Solid yellow. HSVA is (60, 1, 1, 1).
        /// </summary>
        public static HSVColor yellow
        {
            get { return new HSVColor(60f, 1f, 1f, 1f); }
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private float hue;

        [SerializeField]
        [HideInInspector]
        private float saturation;

        [SerializeField]
        [HideInInspector]
        private float value;

        [SerializeField]
        [HideInInspector]
        private float alpha;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Hue from [0, 360] degrees
        /// </summary>
        public float Hue
        {
            get
            {
                return hue;
            }

            set
            {
                hue = value % 360f;
            }
        }

        /// <summary>
        /// Gets or sets the saturation.
        /// </summary>
        /// <value>
        /// The saturation.
        /// </value>
        public float Saturation
        {
            set
            {
                saturation = Mathf.Clamp01(value);
            }
            get
            {
                return saturation;
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public float Value
        {
            set
            {
                this.value = Mathf.Clamp01(value);
            }
            get
            {
                return value;
            }
        }

        /// <summary>
        /// Gets or sets the alpha.
        /// </summary>
        /// <value>
        /// The alpha.
        /// </value>
        public float Alpha
        {
            set
            {
                alpha = Mathf.Clamp01(value);
            }
            get
            {
                return alpha;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="HSVColor"/> struct.
        /// </summary>
        /// <param name="hue">The h.</param>
        /// <param name="saturation">The s.</param>
        /// <param name="value">The v.</param>
        /// <param name="alpha">a.</param>
        public HSVColor(float hue, float saturation, float value, float alpha = 1f)
        {
            this.hue = hue % 360f;
            this.saturation = Mathf.Clamp01(saturation);
            this.value = Mathf.Clamp01(value);
            this.alpha = Mathf.Clamp01(alpha);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HSVColor"/> struct.
        /// </summary>
        /// <param name="copy">The original.</param>
        public HSVColor(HSVColor copy) : this(copy.hue, copy.saturation, copy.value, copy.alpha)
        {
        }
                

        public override string ToString()
        {
            return string.Format("HSV({0}, {1}, {2}, {3})", hue, saturation, value, alpha);
        }

        public override bool Equals(object obj)
        {
            var other = (HSVColor)obj;
            if (ReferenceEquals(other, null))
            {
                return false;
            }
            else
            {
                return this == other;
            }
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + hue.GetHashCode();
            hash = hash * 23 + saturation.GetHashCode();
            hash = hash * 23 + value.GetHashCode();
            hash = hash * 23 + alpha.GetHashCode();
            return hash;
        }

        #region Operators

        public static bool operator ==(HSVColor a, HSVColor b)
        {
            return
                Mathf.Approximately(a.hue, b.hue) &&
                Mathf.Approximately(a.saturation, b.saturation) &&
                Mathf.Approximately(a.value, b.value) &&
                Mathf.Approximately(a.alpha, b.alpha);
        }

        public static bool operator !=(HSVColor a, HSVColor b)
        {
            return !(a == b);
        }

        public static HSVColor operator +(HSVColor a, HSVColor b)
        {
            return new HSVColor(a.hue + b.hue, a.saturation + b.saturation, a.value + b.value, a.alpha + b.alpha);
        }

        public static HSVColor operator -(HSVColor a, HSVColor b)
        {
            return new HSVColor(a.hue - b.hue, a.saturation - b.saturation, a.value - b.value, a.alpha - b.alpha);
        }

        public static HSVColor operator *(float factor, HSVColor a)
        {
            return a * factor;
        }

        public static HSVColor operator *(HSVColor a, float factor)
        {
            return new HSVColor(a.hue * factor, a.saturation * factor, a.value * factor, a.alpha * factor);
        }

        public static HSVColor operator /(HSVColor a, float factor)
        {
            if (Mathf.Approximately(factor, 0f))
            {
                throw new System.ArgumentException("Fatal error: Cannot divide by 0");
            }

            return new HSVColor(a.hue / factor, a.saturation / factor, a.value / factor, a.alpha / factor);
        }

        public static implicit operator HSVColor(Color rgb)
        {
            return HSVColorHelper.FromRGB(rgb);
        }

        public static implicit operator Color(HSVColor hsv)
        {
            return HSVColorHelper.ToRGB(hsv);
        }

        public static implicit operator Vector3(HSVColor hsv)
        {
            return HSVColorHelper.ToVector3(hsv);
        }

        public static implicit operator Vector4(HSVColor hsv)
        {
            return HSVColorHelper.ToVector4(hsv);
        }

        #endregion

        #endregion

        #region Non Public Methods

        #endregion Non Public Methods
    }
}