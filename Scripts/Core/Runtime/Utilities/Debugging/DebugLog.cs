﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Helpers;

namespace CEUtilities.Debugging
{
    public class DebugLog : MonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private bool logOnlyInEditor = true;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public bool LogOnlyInEditor
        {
            get
            {
                return logOnlyInEditor;
            }

            set
            {
                logOnlyInEditor = value;
            }
        }

        private bool ShouldLog
        {
            get
            {
                return !(LogOnlyInEditor && !ApplicationHelper.PlatformIsEditor);
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Logs a message to console
        /// </summary>
        public void Log(string message)
        {
            if (!ShouldLog)
                return;

            UnityEngine.Debug.Log(message);
        }

        /// <summary>
        /// Logs a warning to console
        /// </summary>
        public void LogWarning(string message)
        {
            if (!ShouldLog)
                return;

            UnityEngine.Debug.LogWarning(message);
        }

        /// <summary>
        /// Logs an error to console
        /// </summary>
        public void LogError(string message)
        {
            if (!ShouldLog)
                return;

            UnityEngine.Debug.LogError(message);
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}