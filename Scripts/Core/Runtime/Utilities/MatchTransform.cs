﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using System.Xml.Linq;
using System.IO;
using System.Linq;
using System;

using Sirenix.Serialization;

namespace CEUtilities
{
    public class MatchTransform : SerializedMonoBehaviour
    {
        #region Enums

        public const int NONE = 0;

        [Flags]
        public enum Vector3Component
        {
            X = 1 << 0,
            Y = 1 << 1,
            Z = 1 << 2
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private Transform targetToMatch;
        [SerializeField]
        [HideInInspector]
        private bool onlyIfTargetIsActive;

        [SerializeField]
        [HideInInspector]
        private Vector3Component positionMatch;
        [HideInInspector]
        [SerializeField]
        private bool posStartValueAsOffset = true;
        [SerializeField]
        [HideInInspector]
        private Vector3 posAddOffset = Vector3.zero;

        [SerializeField]
        [HideInInspector]
        private Vector3Component scaleMatch;
        [SerializeField]
        [HideInInspector]
        private bool scaleStartValueAssOffset = true;
        [SerializeField]
        [HideInInspector]
        private Vector3 scaleAddOffset = Vector3.zero;

        [SerializeField]
        [HideInInspector]
        private Vector3Component rotationMatch;
        [SerializeField]
        [HideInInspector]
        private bool rotStartValueAsOffset = true;
        [SerializeField]
        [HideInInspector]
        private Vector3 rotAddSourceOffset = Vector3.zero;
        [SerializeField]
        [HideInInspector]
        private Vector3 rotAddEndOffset = Vector3.zero;

        [SerializeField]
        [HideInInspector]
        private bool loadOffsetsFromConfig = false;
        [SerializeField]
        [HideInInspector]
        private string configPath;
        [OdinSerialize]
        [HideInInspector]
        private Guid guid;

        #endregion Exposed fields

        #region Internal fields

        private bool initedConfig = false;

        private bool initedOffsets = false;

        private Vector3 positionOffset = Vector3.zero;

        private Vector3 scaleOffset = Vector3.zero;

        private Quaternion rotationOffset = Quaternion.identity;

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Target transform.
        /// </summary>
        [ShowInInspector]
        [PropertyTooltip("Which target should be used to copy the values from.")]
        public Transform Target
        {
            get
            {
                return targetToMatch;
            }

            set
            {
                targetToMatch = value;
            }
        }

        /// <summary>
        /// Match only when the target is enabled.
        /// </summary>
        [ShowInInspector]
        public bool OnlyIfTargetIsActive
        {
            get
            {
                return onlyIfTargetIsActive;
            }

            set
            {
                onlyIfTargetIsActive = value;
            }
        }


        /// <summary>
        /// Position start value is taken as offset.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Position")]
        [LabelText("Starting Value as Offset")]
        public bool PosStartValueAsOffset
        {
            get
            {
                return posStartValueAsOffset;
            }

            set
            {
                posStartValueAsOffset = value;
            }
        }

        /// <summary>
        /// Scale start value is taken as offset.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Scale")]
        [LabelText("Starting Value as Offset")]
        public bool ScaleStartValueAssOffset
        {
            get
            {
                return scaleStartValueAssOffset;
            }

            set
            {
                scaleStartValueAssOffset = value;
            }
        }

        /// <summary>
        /// Rotation start value is taken as offset.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Rotation")]
        [LabelText("Starting Value as Offset")]
        public bool RotStartValueAsOffset
        {
            get
            {
                return rotStartValueAsOffset;
            }

            set
            {
                rotStartValueAsOffset = value;
            }
        }


        /// <summary>
        /// Which components of the position Vector are copied to the target transform.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Position")]
        [EnumToggleButtons]
        public Vector3Component PositionMatch
        {
            get
            {
                return positionMatch;
            }

            set
            {
                positionMatch = value;
            }
        }

        /// <summary>
        /// Which components of the scale Vector are copied to the target transform.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Scale")]
        [EnumToggleButtons]
        public Vector3Component ScaleMatch
        {
            get
            {
                return scaleMatch;
            }

            set
            {
                scaleMatch = value;
            }
        }

        /// <summary>
        /// Which components of the rotation Vector are copied to the target transform.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Rotation")]
        [EnumToggleButtons]
        public Vector3Component RotationMatch
        {
            get
            {
                return rotationMatch;
            }

            set
            {
                rotationMatch = value;
            }
        }


        /// <summary>
        /// Additional offset
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Position")]
        [LabelText("Additional Offset")]
        [PropertyTooltip("Additional offset applied to target position.")]
        public Vector3 PositionAdditionalOffset
        {
            get
            {
                return posAddOffset;
            }

            set
            {
                posAddOffset = value;
            }
        }

        /// <summary>
        /// Additional offset
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Scale")]
        [LabelText("Additional Offset")]
        [PropertyTooltip("Additional offset applied to target scale.")]
        public Vector3 ScaleAdditionalOffset
        {
            get
            {
                return scaleAddOffset;
            }

            set
            {
                scaleAddOffset = value;
            }
        }

        /// <summary>
        /// Additional offset
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Rotation")]
        [LabelText("Additional Source Offset")]
        [PropertyTooltip("Additional offset applied to target rotation before calculation.")]
        [DisableInPlayMode]
        public Vector3 RotationAdditionalSourceOffset
        {
            get
            {
                return rotAddSourceOffset;
            }

            set
            {
                rotAddSourceOffset = value;
            }
        }

        /// <summary>
        /// Additional offset
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Rotation")]
        [LabelText("Additional End Offset")]
        [PropertyTooltip("Additional offset applied to this object rotation after calculation.")]
        public Vector3 RotationAdditionalEndOffset
        {
            get
            {
                return rotAddEndOffset;
            }

            set
            {
                rotAddEndOffset = value;
            }
        }


        /// <summary>
        /// If true, tries to load the offsets from the config file.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Config File")]
        public bool LoadOffsetsFromConfig
        {
            get
            {
                return loadOffsetsFromConfig;
            }

            set
            {
                loadOffsetsFromConfig = value;
            }
        }

        /// <summary>
        /// Path of the configuration file.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Config File")]
        [FilePath(ParentFolder = "Assets/StreamingAssets")]
        public string ConfigPath
        {
            get
            {
                return configPath;
            }

            set
            {
                configPath = value;
            }
        }

        /// <summary>
        /// Guid of this instance.
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Config File")]
        public Guid Guid
        {
            get
            {
                return guid;
            }

            set
            {
                guid = value;
            }
        }


        /// <summary>
        /// Position offset.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        [ReadOnly]
        public Vector3 PositionOffset
        {
            get
            {
                return positionOffset;
            }

            private set
            {
                positionOffset = value;
            }
        }

        /// <summary>
        /// Scale Offset.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        [ReadOnly]
        public Vector3 ScaleOffset
        {
            get
            {
                return scaleOffset;
            }

            private set
            {
                scaleOffset = value;
            }
        }

        /// <summary>
        /// Rotation offset.
        /// </summary>
        [ShowInInspector]
        [FoldoutGroup("Runtime")]
        [ReadOnly]
        public Quaternion RotationOffset
        {
            get
            {
                return rotationOffset;
            }

            private set
            {
                rotationOffset = value;
            }
        }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        private void Start()
        {
            if (!initedConfig && LoadOffsetsFromConfig)
            {
                ReadConfig();
            }
        }

        private void LateUpdate()
        {
            // Check for target availability
            if (!Target.gameObject.activeInHierarchy && OnlyIfTargetIsActive)
            {
                return;
            }
            else if (!initedOffsets)
            {
                InitOffsets();
            }

            // Match position
            if (PositionMatch != NONE)
            {
                var matchedPos = transform.position;

                var targetPosition = targetToMatch.position;
                targetPosition += PositionAdditionalOffset;

                if (PositionMatch.HasFlag(Vector3Component.X))
                {
                    matchedPos.x = targetPosition.x - PositionOffset.x;
                }

                if (PositionMatch.HasFlag(Vector3Component.Y))
                {
                    matchedPos.y = targetPosition.y - PositionOffset.y;
                }

                if (PositionMatch.HasFlag(Vector3Component.Z))
                {
                    matchedPos.z = targetPosition.z - PositionOffset.z;
                }

                transform.position = matchedPos;
            }

            // Match scale
            if (ScaleMatch != NONE)
            {
                var matchedScale = transform.localScale;

                var targetScale = targetToMatch.localScale;
                targetScale += ScaleAdditionalOffset;

                if (ScaleMatch.HasFlag(Vector3Component.X))
                {
                    matchedScale.x = targetScale.x - ScaleOffset.x;
                }

                if (ScaleMatch.HasFlag(Vector3Component.Y))
                {
                    matchedScale.y = targetScale.y - ScaleOffset.y;
                }

                if (ScaleMatch.HasFlag(Vector3Component.Z))
                {
                    matchedScale.z = targetScale.z - ScaleOffset.z;
                }

                transform.localScale = matchedScale;
            }

            // Match rotation
            if (RotationMatch != NONE)
            {
                var matchedRotation = transform.rotation;
                var targetRotation = targetToMatch.rotation;

                targetRotation *= Quaternion.Euler(RotationAdditionalSourceOffset);
                matchedRotation = Quaternion.Euler
                (
                    RotationMatch.HasFlag(Vector3Component.X) ? targetRotation.eulerAngles.x - RotationOffset.eulerAngles.x : matchedRotation.eulerAngles.x,
                    RotationMatch.HasFlag(Vector3Component.Y) ? targetRotation.eulerAngles.y - RotationOffset.eulerAngles.y : matchedRotation.eulerAngles.y,
                    RotationMatch.HasFlag(Vector3Component.Z) ? targetRotation.eulerAngles.z - RotationOffset.eulerAngles.z : matchedRotation.eulerAngles.z
                );

                transform.rotation = matchedRotation * Quaternion.Euler(RotationAdditionalEndOffset);
            }
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Initialize the offsets.
        /// </summary>
        private void InitOffsets()
        {
            initedOffsets = true;

            // Copy Offsets
            if (PosStartValueAsOffset)
            {
                PositionOffset = targetToMatch.position - transform.position;
            }

            if (ScaleStartValueAssOffset)
            {
                ScaleOffset = targetToMatch.localScale - transform.localScale;
            }

            if (RotStartValueAsOffset)
            {
                RotationOffset = targetToMatch.rotation * Quaternion.Euler(RotationAdditionalSourceOffset) * Quaternion.Inverse(transform.rotation);
            }
        }

        /// <summary>
        /// Read the config.
        /// </summary>
        private void ReadConfig()
        {
            // Set flag
            initedConfig = true;

            // Load xml
            var xDoc = XDocument.Load(Path.Combine(Application.streamingAssetsPath, configPath));

            // Look for info
            var xIds = xDoc.Descendants("match_transform").Descendants("id");
            foreach (var xId in xIds)
            {
                //
                if (xId.Attribute("value").Value == Guid.ToString())
                {
                    // Position offset
                    var xPos = xId.Element("position_additional_offset");
                    if (xPos != null)
                    {
                        PositionAdditionalOffset = new Vector3
                        (
                            float.Parse(xPos.Element("x").Value),
                            float.Parse(xPos.Element("y").Value),
                            float.Parse(xPos.Element("z").Value)
                        );
                    }

                    // Scale offset
                    var xScale = xId.Element("scale_additional_offset");
                    if (xScale != null)
                    {
                        ScaleAdditionalOffset = new Vector3
                        (
                            float.Parse(xScale.Element("x").Value),
                            float.Parse(xScale.Element("y").Value),
                            float.Parse(xScale.Element("z").Value)
                        );
                    }

                    // Rotation source offset
                    var xSourceRot = xId.Element("rotation_additional_source_offset");
                    if (xSourceRot != null)
                    {
                        RotationAdditionalSourceOffset = new Vector3
                        (
                            float.Parse(xSourceRot.Element("x").Value),
                            float.Parse(xSourceRot.Element("y").Value),
                            float.Parse(xSourceRot.Element("z").Value)
                        );
                    }

                    // Rotation end offset
                    var xEndRot = xId.Element("rotation_additional_end_offset");
                    if (xEndRot != null)
                    {
                        RotationAdditionalEndOffset = new Vector3
                        (
                            float.Parse(xEndRot.Element("x").Value),
                            float.Parse(xEndRot.Element("y").Value),
                            float.Parse(xEndRot.Element("z").Value)
                        );
                    }
                }
            }
        }

        #endregion Methods
    }
}