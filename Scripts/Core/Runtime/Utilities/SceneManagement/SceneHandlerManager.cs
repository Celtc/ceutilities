﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;
using CEUtilities.Helpers;
using CEUtilities.Debugging;
using UnityEngine.Events;

namespace CEUtilities
{
    [CustomSingleton(Persistent = true, AutoGeneration = true)]
    public class SceneHandlerManager : Singleton<SceneHandlerManager>
    {
        #region Exposed fields

        private HashSet<SceneHandler> persistedHandlers = new HashSet<SceneHandler>();

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        public UnityEvent_SceneHandler OnHandlerPreload = new UnityEvent_SceneHandler();

        public UnityEvent_SceneHandler OnHandlerLoad = new UnityEvent_SceneHandler();

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Handlers which were make persistent
        /// </summary>
        public List<SceneHandler> PersistedHandlers => persistedHandlers.ToList();

        /// <summary>
        /// Get all scene handlers which are loading
        /// </summary>
        public List<SceneHandler> LoadingHandlers
        {
            get
            {
                var loadings = new List<SceneHandler>();
                foreach (var handler in persistedHandlers)
                    if (handler.Loading)
                        loadings.Add(handler);
                return loadings;
            }
        }

        /// <summary>
        /// Is any of the handlers loading?
        /// </summary>
        public bool IsLoading
        {
            get
            {
                return persistedHandlers.Any(x => x.Loading);
            }
        }

        /// <summary>
        /// Avarage progress of all loading handlers. 1 is returned if none is loading.
        /// </summary>
        public float OverallLoadingProgress
        {
            get
            {
                var loadings = LoadingHandlers;
                if (loadings.Count == 0)
                    return 1f;
                float sum = 0f, avarage = 0f;
                for (int i = 0; i < loadings.Count; i++)
                    sum += loadings[i].Progress;
                avarage = sum / loadings.Count;
                return avarage;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Persist a handler
        /// </summary>
        public bool PersistHandler(SceneHandler handler)
        {
            if (!handler.IsValid)
            {
                Debug.LogError(DebugHelper.Format("Trying to persist an invalid handler"));
                return false;
            }

            return persistedHandlers.Add(handler);
        }

        /// <summary>
        /// Remove a handler from the persistent collection
        /// </summary>
        public bool RemoveHandler(SceneHandler handler)
        {
            return persistedHandlers.Remove(handler);
        }


        /// <summary>
        /// Retrieve the handler for the main active scene
        /// </summary>
        public SceneHandler GetMainActiveHandler()
        {
            var handler = new SceneHandler(SceneManager.GetActiveScene().buildIndex);
            handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
            handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);
            return handler;
        }

        /// <summary>
        /// Retrieve a handler from the stored collection
        /// </summary>
        public SceneHandler GetOrCreateHandlerForPath(string scenePath)
        {
            var result = GetHandlerForPath(scenePath);
            if (result == null)
                result = CreateHandlerForPath(scenePath, true);
            return result;
        }

        /// <summary>
        /// Retrieve a handler from the stored collection
        /// </summary>
        public SceneHandler GetOrCreateHandlerForName(string sceneName)
        {
            var result = GetHandlerForName(sceneName);
            if (result == null)
                result = CreateHandlerForName(sceneName, true);
            return result;
        }

        /// <summary>
        /// Retrieve a handler from the stored collection
        /// </summary>
        public SceneHandler GetOrCreateHandlerForIndex(int buildIndex)
        {
            var result = GetHandlerForIndex(buildIndex);
            if (result == null)
                result = CreateHandlerForIndex(buildIndex, true);
            return result;
        }


        /// <summary>
        /// Retrieve a handler from the stored collection
        /// </summary>
        public SceneHandler GetHandlerForPath(string scenePath)
        {
            foreach (var handler in PersistedHandlers)
                if (handler.Path == scenePath)
                    return handler;
            return null;
        }

        /// <summary>
        /// Retrieve a handler from the stored collection
        /// </summary>
        public SceneHandler GetHandlerForName(string sceneName)
        {
            foreach (var handler in PersistedHandlers)
                if (handler.Name == sceneName)
                    return handler;
            return null;
        }

        /// <summary>
        /// Retrieve a handler from the stored collection
        /// </summary>
        public SceneHandler GetHandlerForIndex(int buildIndex)
        {
            foreach (var handler in PersistedHandlers)
                if (handler.BuildIndex == buildIndex)
                    return handler;
            return null;
        }


        /// <summary>
        ///  Create a new handler
        /// </summary>
        /// <param name="scenePath">Path of the scene relative to the project folder</param>
        /// <param name="persist">Should persist the handler between scenes?</param>
        public SceneHandler CreateHandlerForPath(string scenePath, bool persist)
        {
            var handler = new SceneHandler(scenePath);
            handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
            handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);

            if (persist)
                PersistHandler(handler);
            return handler;
        }

        /// <summary>
        ///  Create a new handler
        /// </summary>
        /// <param name="sceneName">Name of the scene</param>
        /// <param name="persist">Should persist the handler between scenes?</param>
        public SceneHandler CreateHandlerForName(string sceneName, bool persist)
        {
            var path = SceneManagerHelper.GetFirstPathFromName(sceneName);

            var handler = new SceneHandler(path);
            handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
            handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);

            if (persist)
                PersistHandler(handler);

            return handler;
        }

        /// <summary>
        ///  Create a new handler
        /// </summary>
        /// <param name="buildIndex">Index number in the builds scenes list</param>
        /// <param name="persist">Should persist the handler between scenes?</param>
        public SceneHandler CreateHandlerForIndex(int buildIndex, bool persist)
        {
            var handler = new SceneHandler(buildIndex);
            handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
            handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);

            if (persist)
                PersistHandler(handler);

            return handler;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void LoadSingle(string scenePath)
        {
            var handler = GetHandlerForPath(scenePath);
            if (handler == null)
            {
                handler = CreateHandlerForPath(scenePath, true);
                handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
                handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);
            }

            handler.LoadSingleAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void LoadSingle(string scenePath, bool reload)
        {
            var handler = GetHandlerForPath(scenePath);
            if (handler == null)
            {
                handler = CreateHandlerForPath(scenePath, true);
                handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
                handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);
            }

            handler.LoadSingleAsync(reload);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void LoadAdditive(string scenePath)
        {
            var handler = GetHandlerForPath(scenePath);
            if (handler == null)
            {
                handler = CreateHandlerForPath(scenePath, true);
                handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
                handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);
            }

            handler.LoadAdditiveAsync();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void PreloadSingle(string scenePath)
        {
            var handler = GetHandlerForPath(scenePath);
            if (handler == null)
            {
                handler = CreateHandlerForPath(scenePath, true);
                handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
                handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);
            }

            handler.PreloadSingleAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void PreloadAdditive(string scenePath)
        {
            var handler = GetHandlerForPath(scenePath);
            if (handler == null)
            {
                handler = CreateHandlerForPath(scenePath, true);
                handler.OnPreloaded += () => OnHandlerPreload.Invoke(handler);
                handler.OnLoaded += () => OnHandlerLoad.Invoke(handler);
            }

            handler.PreloadAdditiveAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void LoadPreloaded(string scenePath)
        {
            var handler = GetHandlerForPath(scenePath);
            if (handler != null)
                handler.LoadPreloaded();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void LoadPreloaded(SceneHandler handler)
        {
            handler.LoadPreloaded();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void Unload(string scenePath)
        {
            var handler = GetHandlerForPath(scenePath);
            if (handler != null)
                handler.Unload();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePath"></param>
        public void Unload(SceneHandler handler)
        {
            handler.Unload();
        }

        ///// <summary>
        ///// Changes the scene
        ///// </summary>
        //[Obsolete("ChangeScene is deprecated, please use LoadScene instead.", true)]
        //public void ChangeScene(string name)
        //{
        //    throw new NotImplementedException();
        //    /*
        //    var path = SceneManagerHelper.GetFirstPathFromName(name);
        //    var sceneInfo = new SceneHandler(path);
        //    StartCoroutine(_LoadWithLoadingScreen(sceneInfo, LoadSceneMode.Single, true));
        //    */
        //}

        ///// <summary>
        ///// Load a scene showing a loading screen
        ///// </summary>
        ///// <param name="sceneHandler">Scene handler, must be unloaded</param>
        ///// <param name="mode">Loading mode</param>
        ///// <param name="forceReload">Should use a loading screen?</param>
        ///// <param name="useLoadingScreen">Should use a loading screen?</param>
        ///// <param name="unloadLoadeds">Unload current loaded scenes when showing the loading screen?</param>
        //public void LoadScene(SceneHandler sceneHandler, LoadSceneMode mode, bool forceReload, bool unloadLoadeds)
        //{
        //    // Valid?
        //    if (!sceneHandler.IsValid)
        //    {
        //        ConsoleLogger.LogError("Trying to load an invalid scene handler", true, true);
        //        return;
        //    }

        //    // Not loaded?
        //    if (!sceneHandler.Unloaded)
        //    {
        //        ConsoleLogger.LogError("Trying to load an already loaded scene handler", true, true);
        //        return;
        //    }

        //    StartCoroutine(_LoadWithLoadingScreen(sceneHandler, mode, unloadLoadeds));;
        //}

        #endregion Methods

        #region Non Public Methods

        ///// <summary>
        ///// Load the loading screen in additive
        ///// </summary>
        //private IEnumerator _LoadLoadingScreen(bool replaceLoadeds)
        //{
        //    var mode = replaceLoadeds ? LoadSceneMode.Single : LoadSceneMode.Additive;
        //    SceneManager.LoadScene(loadingScreen, mode);
        //    yield return null;
        //}

        ///// <summary>
        ///// Unload the loading screen
        ///// </summary>
        ///// <returns></returns>
        //private IEnumerator _UnloadLoadingScreen()
        //{
        //    var operation = SceneManager.UnloadSceneAsync(loadingScreen);
        //    while (!operation.isDone)
        //        yield return null;
        //}

        ///// <summary>
        ///// Load a level in single or additive mode, with loading screen
        ///// </summary>
        //private IEnumerator _LoadWithLoadingScreen(SceneHandler scene, LoadSceneMode mode, bool unloadLoadeds)
        //{
        //    // Show loading screen
        //    yield return _LoadLoadingScreen(unloadLoadeds);

        //    // Preload target scene
        //    scene.PreloadAsync(false, mode);
        //    yield return new WaitUntil(() => scene.Preloaded);

        //    // Unload loading screen if target is additive
        //    if (mode == LoadSceneMode.Additive)
        //        _UnloadLoadingScreen();

        //    // Activate target scene
        //    scene.LoadPreloaded();
        //}

        #endregion Methods
    }
}