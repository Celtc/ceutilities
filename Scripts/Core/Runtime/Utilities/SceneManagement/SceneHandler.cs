﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using CEUtilities.Debugging;
using CEUtilities.Helpers;

namespace CEUtilities
{
    [Serializable]
    public class SceneHandler
    {
        #region Exposed fields

        [SerializeField]
        private int cachedBuildIndex = -1;


        private Scene scene;

        private float progress;

        #endregion Exposed fields

        #region Internal fields

        private AsyncOperation operationHandler;

        private bool preloadedFlag;

        #endregion Internal fields

        #region Custom Events

        /// <summary>
        /// Called when progress is updated
        /// </summary>
        public Action<float> OnProgress;

        /// <summary>
        /// Called when the scene is preloaded, and ready to be shown
        /// </summary>
        public Action OnPreloaded;

        /// <summary>
        /// Called when the scene is loaded and showing
        /// </summary>
        public Action OnLoaded;

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// The scene struct associated if loaded
        /// </summary>
        public Scene Scene
        {
            get
            {
                return scene;
            }

            private set
            {
                scene = value;
            }
        }

        /// <summary>
        /// Is this handler pointing to a valid scene?
        /// </summary>
        public bool IsValid => BuildIndex >= 0;

        /// <summary>
        /// Build index
        /// </summary>
        public int BuildIndex => Scene.IsValid() ? Scene.buildIndex : cachedBuildIndex;

        /// <summary>
        /// Path
        /// </summary>
        public string Path => Scene.IsValid() ? Scene.path : SceneUtility.GetScenePathByBuildIndex(BuildIndex);

        /// <summary>
        /// Name
        /// </summary>
        public string Name => Scene.IsValid() ? Scene.name : System.IO.Path.GetFileNameWithoutExtension(Path);

        /// <summary>
        /// Is the scene being loaded?
        /// </summary>
        public bool Loading => Scene.IsValid() && Scene.GetState() == SceneExtension.LoadingState.Loading;

        /// <summary>
        /// Is the scene preloaded (loaded but not showing)?
        /// </summary>
        public bool Preloaded => Loading && preloadedFlag;

        /// <summary>
        /// Is the scene loaded (loaded and showing)?
        /// </summary>
        public bool Loaded => Scene.IsValid() && Scene.isLoaded;

        /// <summary>
        /// Is the scene not loaded neither loading
        /// </summary>
        public bool Unloaded => !Scene.IsValid() || Scene.GetState() == SceneExtension.LoadingState.NotLoaded;

        /// <summary>
        /// Current progress of the loading. 1 if loaded, 0 if not loaded
        /// </summary>
        public float Progress => progress;

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Ctor
        /// </summary>
        internal SceneHandler(string scenePath)
        {
            // Retrieve index from path
            cachedBuildIndex = SceneUtility.GetBuildIndexByScenePath(scenePath);

            CheckForExisitingLoaded();
        }

        /// <summary>
        /// Ctor
        /// </summary>
        internal SceneHandler(int buildIndex)
        {
            // Check for valid index
            if (SceneManager.sceneCountInBuildSettings <= buildIndex)
                cachedBuildIndex = -1;
            else
                cachedBuildIndex = buildIndex;

            CheckForExisitingLoaded();
        }


        /// <summary>
        /// Loads the associated scene with this scene info.
        /// </summary>
        public void LoadSingle()
        {
            InternalLoad(LoadSceneMode.Single, false);
        }

        /// <summary>
        /// Loads the associated scene with this scene info.
        /// </summary>
        /// <param name = "reload">If True, the scene will be reloaded if it's already open.</param>
        public void LoadSingle(bool reload)
        {
            InternalLoad(LoadSceneMode.Single, reload);
        }

        /// <summary>
        /// Loads the associated scene with this scene info.
        /// </summary>
        public void LoadAdditive()
        {
            InternalLoad(LoadSceneMode.Additive, true);
        }
        

        /// <summary>
        /// Loads the scene asynchronously.
        /// </summary>
        public void LoadSingleAsync()
        {
            InternalLoadAsync(LoadSceneMode.Single, false, true);
        }

        /// <summary>
        /// Loads the scene asynchronously.
        /// </summary>
        /// <param name = "reload">If True, the scene will be reloaded if it's already open.</param>
        public void LoadSingleAsync(bool reload)
        {
            InternalLoadAsync(LoadSceneMode.Single, reload, true);
        }


        /// <summary>
        /// Loads the scene asynchronously.
        /// </summary>
        public void LoadAdditiveAsync()
        {
            InternalLoadAsync(LoadSceneMode.Additive, true, true);
        }


        /// <summary>
        /// Loads the scene asynchronously.
        /// </summary>
        /// <param name = "forceReload">If True, the scene will be re-loaded if it is already open (only in single mode), if not the call will be ignored.</param>
        public void PreloadSingleAsync()
        {
            InternalLoadAsync(LoadSceneMode.Single, false, false);
        }

        /// <summary>
        /// Loads the scene asynchronously.
        /// </summary>
        /// <param name = "reload">If True, the scene will be reloaded if it's already open.</param>
        public void PreloadSingleAsync(bool reload)
        {
            InternalLoadAsync(LoadSceneMode.Single, reload, false);
        }

        /// <summary>
        /// Loads the scene asynchronously.
        /// </summary>
        /// <param name = "forceReload">If True, the scene will be re-loaded if it is already open (only in single mode), if not the call will be ignored.</param>
        public void PreloadAdditiveAsync()
        {
            InternalLoadAsync(LoadSceneMode.Additive, true, false);
        }


        /// <summary>
        /// Load the preloaded data
        /// </summary>
        public void LoadPreloaded()
        {
            // Check for preloaded data
            if (Preloaded)
            {
                AllowActivation();
                return;
            }
            else
                Debug.LogError(DebugHelper.Format("The scene was not preloaded."));
        }


        /// <summary>
        /// Closes the scene additively added.
        /// </summary>
        /// <returns>True if the operation was successful</returns>
        public bool Unload()
        {
            bool result = false;
            if (Loaded || Loading)
                result = SceneManagerHelper.CloseScene(Scene);
            else
                Debug.LogError(DebugHelper.Format("Trying to unload a not loaded scene"));
            return result;
        }


        /// <summary>
        /// Set the persistent state of this handler between scenes
        /// </summary>
        public void SetPersistent(bool persistent)
        {
            if (persistent)
                SceneHandlerManager.Instance.PersistHandler(this);
            else
                SceneHandlerManager.Instance.RemoveHandler(this);
        }

        /// <summary>
        /// Set this scene as main active
        /// </summary>
        public void SetAsMainActive()
        {
            if (IsValid)
                SceneManager.SetActiveScene(scene);
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Check wanted scene is loaded
        /// </summary>
        private void CheckForExisitingLoaded()
        {
            // Get loaded scene
            Scene result = default(Scene);
            if (cachedBuildIndex >= 0)
                result = SceneManager.GetSceneByBuildIndex(cachedBuildIndex);

            // If valid, set and sync cache
            if (result.IsValid())
            {
                Scene = result;
                cachedBuildIndex = Scene.buildIndex;

                // Check if was preloaded
                preloadedFlag = Scene.isLoaded;
            }
        }

        /// <summary>
        /// Allow the activation if was preloaded
        /// </summary>
        private void AllowActivation()
        {
            if (Preloaded && operationHandler != null)
                operationHandler.allowSceneActivation = true;
        }


        /// <summary>
        /// Loads a scene
        /// </summary>
        private void InternalLoad(LoadSceneMode mode, bool forceReload)
        {
            // Check for preloaded data
            if (Preloaded)
            {
                Debug.Log(DebugHelper.Format("Trying to load an already preloaded scene."));
                return;
            }

            // Load scene
            Scene loadedScene;
            if (cachedBuildIndex != -1)
            {
                if (SceneManagerHelper.OpenScene(cachedBuildIndex, forceReload, mode, out loadedScene))
                    Scene = loadedScene;

                // Fail loading
                else
                    Debug.LogError(DebugHelper.Format("Loading scene by index {0} failed", cachedBuildIndex.ToString()));
            }

            // Invalid info
            else
                Debug.LogError(DebugHelper.Format("Trying to load invalid SceneInfo"));

            OnPreloaded?.Invoke();
            OnLoaded?.Invoke();
        }

        /// <summary>
        /// Load the scene async
        /// </summary>
        private void InternalLoadAsync(LoadSceneMode mode, bool forceReload, bool allowActivation)
        {
            // Check for preloaded data
            if (Preloaded)
            {
                Debug.Log(DebugHelper.Format("Trying to load an already preloaded scene."));
                return;
            }

            Scene loadingScene = default(Scene);

            // Load async (with automatic activation)
            if (cachedBuildIndex != -1)
            {
                operationHandler = SceneManagerHelper.OpenSceneAsync(cachedBuildIndex, forceReload, mode, allowActivation, out loadingScene);
            }
            else
            {
                Debug.LogError(DebugHelper.Format("Trying to load invalid SceneInfo"));
                return;
            }

            // Valid?
            if (operationHandler != null)
            {
                Scene = loadingScene;

                // Track
                TrackLoading(operationHandler);
            }
            else
            {
                Debug.LogError(DebugHelper.Format("Loading of the scene failed"));
                return;
            }
        }


        /// <summary>
        /// Wait for the preloaded has finished
        /// </summary>
        private void TrackLoading(AsyncOperation operationHandler)
        {
            Coroutines.StaticCoroutine.Start(_TrackLoading(operationHandler));
        }

        /// <summary>
        /// Track the operation progress
        /// </summary>
        private IEnumerator _TrackLoading(AsyncOperation operationHandler)
        {
            progress = 0f;
            OnProgress?.Invoke(Progress);

            // Loading loop
            while (!operationHandler.isDone)
            {
                // Progress event
                if (Progress != operationHandler.progress)
                {
                    progress = operationHandler.progress;
                    OnProgress?.Invoke(Progress);
                }

                // Preloaded event?
                if (!Preloaded && Progress >= .9f)
                {
                    preloadedFlag = true;
                    OnPreloaded?.Invoke();
                }

                yield return null;
            }

            // Events
            progress = 1f;
            if (!preloadedFlag)
            {
                preloadedFlag = true;
                OnPreloaded?.Invoke();
            }
            OnLoaded?.Invoke();
        }

        #endregion Non Public Methods
    }
}