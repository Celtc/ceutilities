﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;
using CEUtilities.Helpers;
using CEUtilities.Debugging;
using UnityEngine.Events;

namespace CEUtilities
{
    public class SceneHandlerManagerAccess : MonoBehaviour
    {
        #region Exposed fields
        
        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events
        
        #endregion Custom Events

        #region Properties
        
        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods
        
        public void LoadSingle(string scenePath)
        {
            if (SceneHandlerManager.Exists)
            {
                SceneHandlerManager.Instance.LoadSingle(scenePath);
            }
        }

        public void LoadOrReloadSingle(string scenePath)
        {
            if (SceneHandlerManager.Exists)
            {
                SceneHandlerManager.Instance.LoadSingle(scenePath, true);
            }
        }

        public void LoadAdditive(string scenePath)
        {
            if (SceneHandlerManager.Exists)
            {
                SceneHandlerManager.Instance.LoadAdditive(scenePath);
            }
        }

        #endregion Methods

        #region Non Public Methods
        
        #endregion Methods
    }
}