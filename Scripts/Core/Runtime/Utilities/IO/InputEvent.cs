﻿using UnityEngine;
using UnityEngine.Events;

using Sirenix.OdinInspector;
using System;

namespace CEUtilities.IO
{
    [System.Serializable]
    public abstract class InputEvent
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private bool ignoreTimeScale = true;

        [SerializeField]
        [HideInInspector]
        private bool allowHold = false;

        [SerializeField]
        [HideInInspector]
        private float rate = .2f;

        [ShowInInspector]
        [HideInInspector]
        private InputState state = InputState.Inactive;

        #endregion

        #region Internal fields

        private float lastTriggered;
        private bool pressedLastCheck;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        /// <summary>
        /// Should ignore tiume scale?
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(10)]
        public bool IgnoreTimeScale
        {
            get
            {
                return ignoreTimeScale;
            }

            set
            {
                ignoreTimeScale = value;
            }
        }

        /// <summary>
        /// Allow triggering while the input is pressed?
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(11)]
        public bool AllowHold
        {
            get
            {
                return allowHold;
            }

            set
            {
                allowHold = value;
            }
        }

        /// <summary>
        /// Rate of triggering in case holding is allowed
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(12)]
        [ShowIf("allowHold")]
        [Indent]
        public float Rate
        {
            get
            {
                return rate;
            }

            set
            {
                rate = value;
            }
        }

        /// <summary>
        /// Current state of the input
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(13)]
        [ReadOnly]
        public InputState State
        {
            get
            {
                return state;
            }

            internal set
            {
                state = value;
            }
        }


        /// <summary>
        /// Get the current time
        /// </summary>
        protected float CurrentTime
        {
            get
            {
                return IgnoreTimeScale ? Time.realtimeSinceStartup : Time.time;
            }
        }

        /// <summary>
        /// Last time this event was triggered
        /// </summary>
        protected float LastTriggered
        {
            get
            {
                return lastTriggered;
            }

            set
            {
                lastTriggered = value;
            }
        }

        /// <summary>
        /// Cached value of last check
        /// </summary>
        protected bool PressedLastCheck
        {
            get
            {
                return pressedLastCheck;
            }

            set
            {
                pressedLastCheck = value;
            }
        }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        public virtual InputState Check()
        {
            // Being pressed?
            State = InternalCheckPressed();
            if (State == InputState.Inactive)
            {
                PressedLastCheck = false;
                return State;
            }

            // New press || Keep pressing (and valid rate)
            if ((!PressedLastCheck && ValidRate()) ||
                (AllowHold && ValidRate()))
            {
                Pressed();
                PressedLastCheck = true;
                LastTriggered = CurrentTime;
                State |= InputState.Triggered;
            }

            return State;
        }

        public abstract void Pressed();

        #endregion

        #region Non Public Methods

        /// <summary>
        /// Is this frame valid for triggering the button?
        /// </summary>
        private bool ValidRate()
        {
            return CurrentTime - LastTriggered >= Rate;
        }

        /// <summary>
        /// Checks if the input is pressed
        /// </summary>
        /// <returns>Is being pressed?</returns>
        protected abstract InputState InternalCheckPressed();

        #endregion
    }

    [System.Serializable]
    public class InputKeyCode : InputEvent
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private KeyCode code;

        #endregion

        #region Custom Events

        [SerializeField]
        [BoxGroup("Events", order: 100)]
        public UnityEvent OnPressed = new UnityEvent();

        #endregion

        #region Properties

        /// <summary>
        /// Key code to track
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(1)]
        public KeyCode Code
        {
            get
            {
                return code;
            }

            set
            {
                code = value;
            }
        }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Simulate a press event
        /// </summary>
        public override void Pressed()
        {
            OnPressed.Invoke();
        }

        #endregion

        #region Non Public Methods

        /// <summary>
        /// Check for press
        /// </summary>
        protected override InputState InternalCheckPressed()
        {
            return Input.GetKey(Code) ? InputState.Pressed : InputState.Inactive;
        }

        #endregion
    }

    [Serializable]
    public class InputVirtualKey : InputEvent
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private string name;

        private float axisValue;

        #endregion

        #region Custom Events

        [SerializeField]
        [BoxGroup("Events", order: 100)]
        public UnityEvent OnPressedPositive = new UnityEvent();
        //public UnityEvent_Float OnPressedPositive = new UnityEvent_Float();

        [SerializeField]
        [BoxGroup("Events")]
        public UnityEvent OnPressedNegative = new UnityEvent();
        //public UnityEvent_Float OnPressedNegative = new UnityEvent_Float();

        #endregion

        #region Properties

        /// <summary>
        /// Name of the virtual key
        /// </summary>
        [ShowInInspector]
        [PropertyOrder(1)]
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Current axis value
        /// </summary>
        public float AxisValue
        {
            get
            {
                return axisValue;
            }

            private set
            {
                axisValue = value;
            }
        }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Simulate a press event
        /// </summary>
        public override void Pressed()
        {
            if (State.HasFlag(InputState.PressedPositive))
                OnPressedPositive.Invoke();//AxisValue);


            else if (State.HasFlag(InputState.PressedNegative))
                OnPressedNegative.Invoke();//AxisValue);
        }

        #endregion

        #region Non Public Methods

        /// <summary>
        /// Check for press
        /// </summary>
        protected override InputState InternalCheckPressed()
        {
            var state = InputState.Inactive;
            AxisValue = Input.GetAxisRaw(Name);

            if (Input.GetButton(Name))
                state |= InputState.PressedPositive;

            else if (Mathf.Approximately(AxisValue, 1f))
                state |= InputState.PressedPositive;

            else if (Mathf.Approximately(AxisValue, -1f))
                state |= InputState.PressedNegative;

            return state;
        }

        #endregion
    }
}