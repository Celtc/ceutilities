﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Flags]
public enum InputState
{
    Inactive = 0,
    Triggered = 1 << 0,
    Pressed = PressedPositive,
    PressedPositive = 1 << 1,
    PressedNegative = 1 << 2,
}