﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using CEUtilities.Patterns;

namespace CEUtilities.IO
{
    [DefaultExecutionOrder(-100)]
    [CustomSingleton(AutoGeneration = true, Persistent = true)]
    public class InputTrackerManager : Singleton<InputTrackerManager>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private Dictionary<string, InputTracker> registered = new Dictionary<string, InputTracker>();

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        public InputTracker this[string id]
        {
            get
            {
                if (registered.ContainsKey(id))
                    return registered[id];
                else
                    return null;
            }
        }

        #endregion Properties

        #region Event Methods

        /// <summary>
        /// Execution order -100
        /// </summary>
        void Update()
        {
            foreach (var tracker in registered.Values.ToArray())
                tracker.InputUpdate();
        }

        #endregion Event Methods

        #region Public Methods

        public void Register(InputTracker tracker)
        {
            if (!registered.ContainsKey(tracker.Id))
                registered.Add(tracker.Id, tracker);
            else
                Debug.LogWarning("[InputTrackerManager] Duplicated InputTracker Id: " + tracker.Id);
        }

        public void Unregister(InputTracker tracker)
        {
            registered.Remove(tracker.Id);
        }

        #endregion Public Methods
    }
}
