﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

using Sirenix.OdinInspector;
using CEUtilities.IO;

namespace CEUtilities.IO
{
    [RequireComponent(typeof(InputTracker))]
    public class InputTrackerEvents : MonoBehaviour
    {
        #region Enums

        public enum InputType
        {
            KeyCode,
            VirtualKey
        }

        #endregion

        #region Exposed fields

        [InfoBox("This component is Deprecated. UnityEvents are already supported inside non Unity.Object classes.", InfoMessageType.Warning)]

        public InputType type = InputType.KeyCode;
        
        [ShowIf("IsKeyCode")]
        public KeyCode keyCode;

        [HideIf("IsKeyCode")]
        public string virtualKey;

        #endregion Exposed fields

        #region Internal fields

        InputTracker targetTracker;

        #endregion Internal fields

        #region Custom Events

        [ShowIf("IsKeyCode")]
        public UnityEvent OnPressed = new UnityEvent();

        [HideIf("IsKeyCode")]
        public UnityEvent_Float OnPressedPositive = new UnityEvent_Float();

        [HideIf("IsKeyCode")]
        public UnityEvent_Float OnPressedNegative = new UnityEvent_Float();

        #endregion Custom Events

        #region Properties

        public bool IsKeyCode
        {
            get { return type == InputType.KeyCode; }
        }

        private InputTracker TargetTracker
        {
            get
            {
                if (targetTracker == null)
                    targetTracker = GetComponent<InputTracker>();

                return targetTracker;
            }
        }

        #endregion Properties

        #region Events methods

        private void OnEnable()
        {
            if (IsKeyCode)
                RegisterKeyCodeEvents();
            else
                RegisterVirtualKeyEvents();
        }

        private void OnDisable()
        {
            if (IsKeyCode)
                UnregisterKeyCodeEvents();
            else
                UnregisterVirtualKeyEvents();
        }

        void OnPressedCallback()
        {
            OnPressed.Invoke();
        }

        void OnPressedPositiveCallback()//float p1)
        {
            OnPressedPositive.Invoke(.5f);// p1);
        }

        void OnPressedNegativeCallback()//float p1)
        {
            OnPressedNegative.Invoke(.5f);// p1);
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        void RegisterKeyCodeEvents()
        {
			if (TargetTracker == null)
				return;

            var input = (InputKeyCode)TargetTracker[keyCode];
            if (input != null)
                input.OnPressed.AddListener(OnPressedCallback);
        }

        void UnregisterKeyCodeEvents()
        {
            if (TargetTracker == null)
                return;

            var input = (InputKeyCode)TargetTracker[keyCode];
            if (input != null)
                input.OnPressed.RemoveListener(OnPressedCallback);
        }


        void RegisterVirtualKeyEvents()
        {
            if (TargetTracker == null)
                return;

            var input = (InputVirtualKey)TargetTracker[virtualKey];
            if (input != null)
            {
                input.OnPressedPositive.AddListener(OnPressedPositiveCallback);
                input.OnPressedNegative.AddListener(OnPressedNegativeCallback);
            }
        }

        void UnregisterVirtualKeyEvents()
        {
            if (TargetTracker == null)
                return;

            var input = (InputVirtualKey)TargetTracker[virtualKey];
            if (input != null)
            {
                input.OnPressedPositive.RemoveListener(OnPressedPositiveCallback);
                input.OnPressedNegative.RemoveListener(OnPressedNegativeCallback);
            }
        }

        #endregion Methods
    }
}