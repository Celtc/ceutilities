﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Patterns;

namespace CEUtilities.IO
{
    public class IdleTracker : Singleton<IdleTracker>
    {
        #region Exposed fields

        [SerializeField]
        private float secondsToIdle = 30f;

        #endregion Exposed fields

        #region Internal fields

        private float timer = 0;

        #endregion Internal fields

        #region Custom Events

        [SerializeField]
        public UnityEvent OnIdle = new UnityEvent();

        #endregion Custom Events

        #region Properties (public)

        public float SecondsToIdle
        {
            get
            {
                return secondsToIdle;
            }

            set
            {
                secondsToIdle = value;
            }
        }

        #endregion Properties

        #region Unity events

        protected virtual void Update()
        {
            CaptureTouchs();

            CaptureKeyEvents();

            CheckForIdle();
        }

        #endregion Unity events

        #region Methods

        /// <summary>
        /// Capture any touch to reset the idle tracker
        /// </summary>
        public virtual void CaptureTouchs()
        {
            bool alive = false;

            if (Input.touchCount > 0)
                alive = true;

            else if (Input.GetMouseButtonDown(0))
                alive = true;

            if (alive)
            {
                AliveSignal();
                Debug.Log("[IdleTracker] Touch detected");
            }
        }

        /// <summary>
        /// Capture any keyboard event to reset the idle tracker
        /// </summary>
        public virtual void CaptureKeyEvents()
        {
            if (Input.anyKeyDown)
            {
				/*
				KeyCode pressed = KeyCode.None;
				foreach (var keyCode in (KeyCode[])System.Enum.GetValues(typeof(KeyCode)))
				{
					if (Input.GetKey(keyCode))
					{
						pressed = keyCode;
						break;
					}
				}

                AliveSignal();
                Debug.Log("[IdleTracker] Keyboard detected: " + pressed.ToString());
				*/

                AliveSignal();
                Debug.Log("[IdleTracker] Keyboard detected");
            }
        }

        /// <summary>
        /// Check for idle
        /// </summary>
        public virtual void CheckForIdle()
        {
            if (secondsToIdle <= 0) return;

            timer += Time.deltaTime;
            if (timer > secondsToIdle)
            {
                OnIdle.Invoke();
                AliveSignal();
            }
        }

        /// <summary>
        /// Notificate the component that the app is alive
        /// </summary>
        public virtual void AliveSignal()
        {
            timer = 0f;
        }

        #endregion Methods
    }
}
