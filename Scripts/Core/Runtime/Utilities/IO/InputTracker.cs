﻿using UnityEngine;
using UnityEngine.Events;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.IO
{
    [DefaultExecutionOrder(-50)]
    public class InputTracker : SerializedMonoBehaviour
    {
        #region Exposed fields

        [SerializeField]
        private Guid id = new Guid();

        [SerializeField]
        [InfoBox("Inputs will be handle by order", "LimitSimultaneousInputs")]
        private bool limitSimultaneousInputs = false;

        [SerializeField]
        [Tooltip("Delay the input tracking after enabling.")]
        private float delayedActivation = 0f;

        [SerializeField]
        [Range(1, 100)]
        [ShowIf("LimitSimultaneousInputs")]
        private int maxInputs = 1;

        [SerializeField]
        //[ListDrawerSettings(OnTitleBarGUI = "DrawTitleBarGUI", HideAddButton = true)]
        private List<InputEvent> inputs = new List<InputEvent>();

        #endregion Exposed fields

        #region Internal fields
        
        private bool running = true;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        /// <summary>
        /// Unique id of this tracker
        /// </summary>
        public string Id => id.ToString();

        /// <summary>
        /// An input event contained in this tracker
        /// </summary>
        public InputEvent this[string virtualKey]
        {
            get
            {
                return inputs
                    .Where(x => x is InputVirtualKey)
                    .FirstOrDefault(x => (x as InputVirtualKey).Name == virtualKey);
            }
        }

        /// <summary>
        /// An input event contained in this tracker
        /// </summary>
        public InputEvent this[KeyCode keyCode]
        {
            get
            {
                return inputs
                    .Where(x => x is InputKeyCode)
                    .FirstOrDefault(x => (x as InputKeyCode).Code == keyCode);
            }
        }

        /// <summary>
        /// Should limit the simultaneous inputs?
        /// </summary>
        public bool LimitSimultaneousInputs
        {
            get
            {
                return limitSimultaneousInputs;
            }

            set
            {
                limitSimultaneousInputs = value;
            }
        }

        /// <summary>
        /// In case of limiting, the amount of maximun inputs
        /// </summary>
        public int MaxInputs
        {
            get
            {
                return maxInputs;
            }

            set
            {
                maxInputs = value;
            }
        }

        /// <summary>
        /// All the inputs tracked
        /// </summary>
        public List<InputEvent> Inputs
        {
            get
            {
                return inputs;
            }

            set
            {
                inputs = value;
            }
        }

        #endregion Properties

        #region Event Methods
        
        private void Awake()
        {
            InputTrackerManager.Instance.Register(this);
        }

        private void OnDestroy()
        {
            if (InputTrackerManager.Exists)
                InputTrackerManager.Instance.Unregister(this);
        }

        private void OnEnable()
        {
            if (delayedActivation > 0f)
            {
                running = false;
                StopAllCoroutines();
                StartCoroutine(DelayActivation());
            }
        }

        internal void InputUpdate()
        {
            if (!isActiveAndEnabled || !running)
                return;

            int inputs = 0;
            foreach (var inputEvent in Inputs)
            {
                InputState state = InputState.Inactive;

                // No limitations
                if (!LimitSimultaneousInputs)
                    state = inputEvent.Check();

                // Simultaneos limitation
                else
                {
                    // No more
                    if (inputs >= MaxInputs)
                        inputEvent.State = state;

                    // Checking
                    else
                    {
                        state = inputEvent.Check();
                        if (state.HasFlag(InputState.Pressed))
                            inputs++;
                    }
                }
            }
        }

        #endregion Event Methods

        #region Public Methods

        /// <summary>
        /// Check for any virtual key input tracked
        /// </summary>
        public bool Any(params string[] names)
        {
            foreach (var input in Inputs)
            {
                if (input is InputVirtualKey)
                {
                    var virtualInput = input as InputVirtualKey;
                    if (names.Contains(virtualInput.Name) &&
                        virtualInput.State != InputState.Inactive)
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Disable this tracker
        /// </summary>
        public void DisableTracker()
        {
            enabled = false;
            running = false;
        }

        /// <summary>
        /// Enable this tracker
        /// </summary>
        public void EnableTracker()
        {
            enabled = true;
            running = true;
        }

        #endregion Public Methods

        #region Non Public Methods

        private IEnumerator DelayActivation()
        {
            yield return new WaitForSeconds(delayedActivation);
            running = true;
        }

#if UNITY_EDITOR
        /*
        private void DrawTitleBarGUI()
        {
            if (Sirenix.Utilities.Editor.SirenixEditorGUI.ToolbarButton(Sirenix.Utilities.Editor.EditorIcons.Plus))
            {
                var input = new InputKeyCode();
                input.OnPressed = new UnityEvent();
                Inputs.Add(input);
            }
        }
        */
#endif

        #endregion
    }
}
