﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CEUtilities
{
    public static class Utilities
    {
        #region Inner fields

        #endregion Inner fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        #endregion Properties

        #region Event Functions

        #endregion Event Functions

        #region Public Methods

        /// <summary>
        /// Rerturn the first root game object which posses a T component
        /// </summary>
        public static GameObject GetRootGameObjectWithComponent<T>()
        {
            Scene activeScene;
#if UNITY_EDITOR
        activeScene = UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene();
#else
            activeScene = SceneManager.GetActiveScene();
#endif

            var rootGOs = activeScene.GetRootGameObjects();
            if (rootGOs == null || rootGOs.Length == 0) return null;

            foreach (var rootGO in rootGOs)
                if (rootGO.HasComponent<T>()) return rootGO;

            return null;
        }

        /// <summary>
        /// Rerturn the first T component found in the first root game object
        /// </summary>
        public static T GetRootGameObjectComponent<T>()
        {
            Scene activeScene;
#if UNITY_EDITOR
        activeScene = UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene();
#else
            activeScene = SceneManager.GetActiveScene();
#endif

            var rootGOs = activeScene.GetRootGameObjects();
            if (rootGOs == null || rootGOs.Length == 0) return default(T);

            foreach (var rootGO in rootGOs)
            {
                var comp = rootGO.GetComponent<T>();
                if (!comp.Equals(null)) return comp;
            }

            return default(T);
        }

        /// <summary>
        /// Returns the UI Camera if exists
        /// </summary>
        public static Camera UICamera()
        {
            Camera result = null;

            var uiCamGO = GameObject.FindGameObjectWithTag("UICamera");
            if (uiCamGO != null)
                result = uiCamGO.GetComponent<Camera>();

            return result;
        }

        /// <summary>
        /// Destroy the specified object immediately, unless not in the editor, in which case the regular Destroy is used instead.
        /// </summary>
        public static void DestroyImmediate(UnityEngine.Object obj)
        {
            if (obj != null)
            {
                if (Application.isEditor) UnityEngine.Object.DestroyImmediate(obj);
                else UnityEngine.Object.Destroy(obj);
            }
        }

        /// <summary>
        /// Check if any of the given objs is null
        /// </summary>
        public static bool AnyIsNull(params object[] args)
        {
            for (int i = 0; i < args.Length; i++)
                if (args[i] == null)
                    return true;
            return false;
        }

        /// <summary>
        /// Generate unique string ID
        /// </summary>
        public static string GenerateUniqueGUID()
        {
            return Guid.NewGuid().ToString();

            /*
            var random = new System.Random();
            var epochStart = new DateTime(1970, 1, 1, 8, 0, 0, DateTimeKind.Utc);
            var timestamp = (DateTime.UtcNow - epochStart).TotalSeconds;

            string uniqueID =
                    Application.platform.ToString() + "-" +                                 //Device    
                    String.Format("{0:X}", Convert.ToInt32(timestamp)) + "-" +              //Time
                    String.Format("{0:X}", random.Next(1000000000)) + "-" +                 //Random number
                    String.Format("{0:X}", random.Next(1000000000));                        //Random number               

            return uniqueID;
            */
        }

        /// <summary>
        /// Generate unique ID
        /// </summary>
        public static int GenerateUniqueID()
        {
            var guid = GenerateUniqueGUID();
            var bytes = new byte[guid.Length * sizeof(char)];
            Buffer.BlockCopy(guid.ToCharArray(), 0, bytes, 0, bytes.Length);
            int id = BitConverter.ToInt32(bytes, 0);
            return id;
        }

        /// <summary>
        /// Same as Random.Range, but the returned value is between min and max, inclusive.
        /// Unity's Random.Range is less than max instead, unless min == max.
        /// This means Range(0,1) produces 0 instead of 0 or 1. That's unacceptable.
        /// </summary>
        public static int RandomRange(int min, int max)
        {
            if (min == max) return min;
            return UnityEngine.Random.Range(min, max + 1);
        }

        /// <summary>
        /// Returns the hierarchy of the object in a human-readable format.
        /// </summary>
        public static string GetHierarchy(GameObject obj)
        {
            if (obj == null) return "";
            string path = obj.name;

            while (obj.transform.parent != null)
            {
                obj = obj.transform.parent.gameObject;
                path = obj.name + "\\" + path;
            }
            return path;
        }

        /// <summary>
        /// Find all active objects of specified type.
        /// </summary>
        public static T[] FindActive<T>() where T : Component
        {
            return GameObject.FindObjectsOfType(typeof(T)) as T[];
        }

        /// <summary>
        /// Helper function that returns the string name of the type.
        /// </summary>
        public static string GetTypeName<T>()
        {
            string s = typeof(T).ToString();
            if (s.StartsWith("UI")) s = s.Substring(2);
            else if (s.StartsWith("UnityEngine.")) s = s.Substring(12);
            return s;
        }

        /// <summary>
        /// Helper function that returns the string name of the type.
        /// </summary>
        public static string GetTypeName(UnityEngine.Object obj)
        {
            if (obj == null) return "Null";
            string s = obj.GetType().ToString();
            if (s.StartsWith("UI")) s = s.Substring(2);
            else if (s.StartsWith("UnityEngine.")) s = s.Substring(12);
            return s;
        }

        /// <summary>
        /// Destroy the specified object, immediately if in edit mode.
        /// </summary>
        public static void Destroy(UnityEngine.Object obj)
        {
            if (obj)
            {
                if (obj is Transform)
                {
                    Transform t = (obj as Transform);
                    GameObject go = t.gameObject;

                    if (Application.isPlaying)
                    {
                        t.SetParent(null);
                        UnityEngine.Object.Destroy(go);
                    }
                    else UnityEngine.Object.DestroyImmediate(go);
                }
                else if (obj is GameObject)
                {
                    GameObject go = obj as GameObject;
                    Transform t = go.transform;

                    if (Application.isPlaying)
                    {
                        t.SetParent(null);
                        UnityEngine.Object.Destroy(go);
                    }
                    else UnityEngine.Object.DestroyImmediate(go);
                }
                else if (Application.isPlaying) UnityEngine.Object.Destroy(obj);
                else UnityEngine.Object.DestroyImmediate(obj);
            }
        }

        /// <summary>
        /// Determines whether the 'parent' contains a 'child' in its hierarchy.
        /// </summary>
        public static bool IsChild(Transform parent, Transform child)
        {
            if (parent == null || child == null) return false;

            while (child != null)
            {
                if (child == parent) return true;
                child = child.parent;
            }
            return false;
        }

        /// <summary>
        /// Save the specified binary data into the specified file.
        /// </summary>
        public static bool Save(string fileName, byte[] bytes)
        {
#if UNITY_WEBGL || UNITY_WP_8_1
		return false;
#else
            //if (!fileAccess) return false;

            string path = Application.persistentDataPath + "/" + fileName;

            if (bytes == null)
            {
                if (File.Exists(path)) File.Delete(path);
                return true;
            }

            FileStream file = null;
            try
            {
                file = File.Create(path);
            }
            catch (System.Exception ex)
            {
                Debug.LogError(ex.Message);
                return false;
            }

            file.Write(bytes, 0, bytes.Length);
            file.Close();
            return true;
#endif
        }

        /// <summary>
        /// Load all binary data from the specified file.
        /// </summary>
        public static byte[] Load(string fileName)
        {
#if UNITY_WEBGL || UNITY_WP_8_1
		return null;
#else
            //if (!fileAccess) return null;

            string path = Application.persistentDataPath + "/" + fileName;

            if (File.Exists(path))
            {
                return File.ReadAllBytes(path);
            }
            return null;
#endif
        }

        /// <summary>
        /// Convenience function that converts Class + Function combo into Class.Function representation.
        /// </summary>
        public static string GetFuncName(object obj, string method)
        {
            if (obj == null) return "<null>";
            string type = obj.GetType().ToString();
            int period = type.LastIndexOf('/');
            if (period > 0) type = type.Substring(period + 1);
            return string.IsNullOrEmpty(method) ? type : type + "/" + method;
        }

        /// <summary>
        /// Execute the specified function on the target game object.
        /// </summary>
        public static void Execute<T>(GameObject go, string funcName) where T : Component
        {
            T[] comps = go.GetComponents<T>();

            foreach (T comp in comps)
            {
#if !UNITY_EDITOR && (UNITY_WEBPLAYER || UNITY_FLASH || UNITY_METRO || UNITY_WP8 || UNITY_WP_8_1)
				comp.SendMessage(funcName, SendMessageOptions.DontRequireReceiver);
#else
                MethodInfo method = comp.GetType().GetMethod(funcName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                if (method != null) method.Invoke(comp, null);
#endif
            }
        }

        #endregion Public Methods
    }
}
