﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

using Sirenix.OdinInspector;

namespace CEUtilities
{
	public class AutoFade : SerializedMonoBehaviour
	{
		#region Exposed fields

		#endregion Exposed fields

		#region Internal fields

		#endregion Internal fields

		#region Custom Events

		#endregion Custom Events

		#region Properties (public)

		#endregion Properties

		#region Unity events

		#endregion Unity events

		#region Methods

		private static AutoFade m_Instance = null;
		private Material m_Material = null;
		private string m_LevelName = "";
		private int m_LevelIndex = 0;
		private bool m_Fading = false;

		private static AutoFade Instance
		{
			get
			{
				if (m_Instance == null)
				{
					m_Instance = (new GameObject("AutoFade")).AddComponent<AutoFade>();
				}
				return m_Instance;
			}
		}
		public static bool Fading
		{
			get { return Instance.m_Fading; }
		}

		private void Awake()
		{
			DontDestroyOnLoad(this);
			m_Instance = this;
			m_Material = new Material(Shader.Find("Plane/No zTest"));
		}

		private void DrawQuad(Texture2D aTexture, float aAlpha)
		{
			Color aColor = Color.white;
			Color oColor = GUI.color;
			aColor.a = aAlpha;

			m_Material.SetPass(0);
			GL.PushMatrix();
			GL.LoadPixelMatrix(0, Screen.width, Screen.height, 0);
			GUI.color = aColor;
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), aTexture);
			GUI.color = oColor;
			GL.PopMatrix();
		}

		private IEnumerator Fade(float aFadeTime)
		{
			Texture2D m_Texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
			yield return new WaitForEndOfFrame();
			m_Texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, true);
			m_Texture.Apply();

			float t = 1.0f;
			AsyncOperation loading;
			if (m_LevelName != "")
				loading = SceneManager.LoadSceneAsync(m_LevelName);
			else
				loading = SceneManager.LoadSceneAsync(m_LevelIndex);
			loading.allowSceneActivation = false;

			while (t > 0.0f)
			{
				yield return new WaitForEndOfFrame();
				t = Mathf.Clamp01(t - Time.deltaTime / aFadeTime);
				DrawQuad(m_Texture, t);
			}
			
			loading.allowSceneActivation = true;
			m_Fading = false;
		}

		private void StartFade(float aFadeTime)
		{
			m_Fading = true;
			StartCoroutine(Fade(aFadeTime));
		}

		public static void LoadLevel(string aLevelName, float aFadeTime)
		{
			if (Fading) return;
			Instance.m_LevelName = aLevelName;
			Instance.StartFade(aFadeTime);
		}

		public static void LoadLevel(int aLevelIndex, float aFadeTime)
		{
			if (Fading) return;
			Instance.m_LevelName = "";
			Instance.m_LevelIndex = aLevelIndex;
			Instance.StartFade(aFadeTime);
		}

		#endregion Methods
	}
}