﻿using System.Collections;
using System.Collections.Generic;

namespace CEUtilities
{
    public class FixedSizedQueue<T> : Queue<T>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Properties

        /// <summary>
        /// Fixed size of the queue
        /// </summary>
        public int Size { get; private set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Constructor
        /// </summary>
        public FixedSizedQueue(int size) : base(size)
        {
            Size = size;
        }

        /// <summary>
        /// Add a new object to the queue
        /// </summary>
        public new void Enqueue(T item)
        {
            if (Count >= Size)
            {
                Dequeue();
            }
            base.Enqueue(item);
        }

        #endregion Public Methods
    }
}
