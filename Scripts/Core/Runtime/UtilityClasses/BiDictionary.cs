﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.UtilityClasses
{
    [System.Serializable]
    public class BiDictionary<TFirst, TSecond> : IEnumerable<Tuple<TFirst, TSecond>>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        IDictionary<TFirst, TSecond> firstToSecond = new Dictionary<TFirst, TSecond>();

        IDictionary<TSecond, TFirst> secondToFirst = new Dictionary<TSecond, TFirst>();

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public TSecond this[TFirst first] => GetByFirst(first);

        public TFirst this[TSecond second] => GetBySecond(second);

        public ICollection<TFirst> Firsts => firstToSecond.Keys;

        public ICollection<TSecond> Seconds => secondToFirst.Keys;

        public int Count => firstToSecond.Count;

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Adds a new entry.
        /// </summary>
        public void Add(TFirst first, TSecond second)
        {
            if (firstToSecond.ContainsKey(first) ||
                secondToFirst.ContainsKey(second))
            {
                throw new ArgumentException("Duplicate first or second");
            }
            firstToSecond.Add(first, second);
            secondToFirst.Add(second, first);
        }

        /// <summary>
        /// Sets a value by first key.
        /// </summary>
        public void SetByFirst(TFirst first, TSecond second)
        {
            if (!ContainsKeyByFirst(first))
            {
                throw new KeyNotFoundException("Key for first not found");
            }
            firstToSecond[first] = second;
            secondToFirst.Remove(second);
            secondToFirst.Add(second, first);
        }

        /// <summary>
        /// Sets a value by first key.
        /// </summary>
        public void SetBySecond(TSecond second, TFirst first)
        {
            if (!ContainsKeyBySecond(second))
            {
                throw new KeyNotFoundException("Key for second not found");
            }
            secondToFirst[second] = first;
            firstToSecond.Remove(first);
            firstToSecond.Add(first, second);
        }

        /// <summary>
        /// Set or add an entry.
        /// </summary>
        public void SetOrAddByFirst(TFirst first, TSecond second)
        {
            if (ContainsKeyByFirst(first))
            {
                SetByFirst(first, second);
            }
            else
            {
                Add(first, second);
            }
        }

        /// <summary>
        /// Set or add an entry.
        /// </summary>
        public void SetOrAddBySecond(TSecond second, TFirst first)
        {
            if (ContainsKeyBySecond(second))
            {
                SetBySecond(second, first);
            }
            else
            {
                Add(first, second);
            }
        }


        /// <summary>
        /// Remove an entry
        /// </summary>
        public void RemoveByFirst(TFirst first)
        {
            TSecond second;
            if (TryGetByFirst(first, out second))
            {
                firstToSecond.Remove(first);
                secondToFirst.Remove(second);
            }
        }

        /// <summary>
        /// Remove an entry
        /// </summary>
        public void RemoveBySecond(TSecond second)
        {
            TFirst first;
            if (TryGetBySecond(second, out first))
            {
                firstToSecond.Remove(first);
                secondToFirst.Remove(second);
            }
        }


        /// <summary>
        /// Get by first
        /// </summary>
        public TSecond GetByFirst(TFirst first)
        {
            return firstToSecond[first];
        }

        /// <summary>
        /// Get by second
        /// </summary>
        public TFirst GetBySecond(TSecond second)
        {
            return secondToFirst[second];
        }


        /// <summary>
        /// Determines whether [contains] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsKeyByFirst(TFirst key)
        {
            return firstToSecond.ContainsKey(key);
        }

        /// <summary>
        /// Determines whether [contains] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public bool ContainsKeyBySecond(TSecond key)
        {
            return secondToFirst.ContainsKey(key);
        }


        /// <summary>
        /// Get by first
        /// </summary>
        public bool TryGetByFirst(TFirst first, out TSecond second)
        {
            return firstToSecond.TryGetValue(first, out second);
        }

        /// <summary>
        /// Get by second
        /// </summary>
        public bool TryGetBySecond(TSecond second, out TFirst first)
        {
            return secondToFirst.TryGetValue(second, out first);
        }


        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            firstToSecond.Clear();
            secondToFirst.Clear();
        }


        public IEnumerator<Tuple<TFirst, TSecond>> GetEnumerator()
        {
            List<Tuple<TFirst, TSecond>> pairs = new List<Tuple<TFirst, TSecond>>();
            foreach (var pair in firstToSecond)
                pairs.Add(new Tuple<TFirst, TSecond>(pair.Key, pair.Value));
            return pairs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}