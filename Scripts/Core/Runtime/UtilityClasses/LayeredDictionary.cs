﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.UtilityClasses
{
    [Serializable]
    public class LayeredDictionary<TKey, TValue> : LayeredCollection<Dictionary<TKey, TValue>>, IEnumerable<KeyValuePair<int, Dictionary<TKey, TValue>>>, IEnumerable
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public bool CreateLayersOnDemand { get; set; }

        public bool CreateEntriesOnDemand { get; set; }

        /// <summary>
        /// Layer key access
        /// </summary>
        public TValue this[int layer, TKey key]
        {
            get
            {
                return internalLayers[layer][key];
            }

            set
            {
                var l = CreateLayersOnDemand ? GetOrAddLayer(layer) : GetLayer(layer);

                if (l == null)
                    throw new IndexOutOfRangeException("Layer not exists");

                if (l.ContainsKey(key))
                    l[key] = value;

                else if (CreateEntriesOnDemand)
                    l.Add(key, value);

                else
                    throw new KeyNotFoundException();
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Ctor
        /// </summary>
        public LayeredDictionary(params int[] layers) : base(layers) { }

        /// <summary>
        /// Adds an element to a layer
        /// </summary>
        public void Add(int layer, TKey key, TValue value)
        {
            var l = CreateLayersOnDemand ? GetOrAddLayer(layer) : GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            if (l.ContainsKey(key))
                throw new ArgumentException("Key already exists");

            l.Add(key, value);
        }

        /// <summary>
        /// Remove an element from a layer
        /// </summary>
        public void Remove(int layer, TKey key)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            l.Remove(key);
        }

        /// <summary>
        /// Remove an element from a layer
        /// </summary>
        public void RemoveFromAllLayers(TKey key)
        {
            foreach(var layer in Layers)
            {
                if (layer.ContainsKey(key))
                {
                    layer.Remove(key);
                }
            }
        }

        /// <summary>
        /// The layer contains the key?
        /// </summary>
        public bool ContainsKey(int layer, TKey key)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            return l.ContainsKey(key);
        }

        /// <summary>
        /// The layer contains the key?
        /// </summary>
        public bool ContainsValue(int layer, TValue value)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            return l.ContainsValue(value);
        }

        /// <summary>
        /// Clear a layer
        /// </summary>
        public void Clear(int layer)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            l.Clear();
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }

    [Serializable]
    public class LayeredDictionary<TKey, TKey2, TValue> : LayeredCollection<Dictionary<TKey, TKey2, TValue>>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public bool CreateLayersOnDemand { get; set; }

        public bool CreateEntriesOnDemand { get; set; }

        /// <summary>
        /// Layer key access
        /// </summary>
        public TValue this[int layer, TKey key1, TKey2 key2]
        {
            get
            {
                return internalLayers[layer][key1, key2];
            }

            set
            {
                var l = CreateLayersOnDemand ? GetOrAddLayer(layer) : GetLayer(layer);

                if (l == null)
                    throw new IndexOutOfRangeException("Layer not exists");

                if (CreateEntriesOnDemand)
                    l.SetOrAdd(key1, key2, value);
                else
                    l.Set(key1, key2, value);
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="layers">Pre created layers</param>
        public LayeredDictionary(params int[] layers) : base(layers) { }

        /// <summary>
        /// Adds an element to a layer
        /// </summary>
        public void Add(int layer, TKey key1, TKey2 key2, TValue value)
        {
            var l = CreateLayersOnDemand ? GetOrAddLayer(layer) : GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            if (l.ContainsKey(key1, key2))
                throw new ArgumentException("Key already exists");

            l.Add(key1, key2, value);
        }

        /// <summary>
        /// Remove an element from a layer
        /// </summary>
        public void Remove(int layer, TKey key1, TKey2 key2)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            l.Remove(key1, key2);
        }

        /// <summary>
        /// The layer contains the key?
        /// </summary>
        public bool ContainsKey(int layer, TKey key1, TKey2 key2)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            return l.ContainsKey(key1, key2);
        }

        /// <summary>
        /// The layer contains the key?
        /// </summary>
        public bool ContainsValue(int layer, TValue value)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            return l.ContainsValue(value);
        }

        /// <summary>
        /// Clear a layer
        /// </summary>
        public void Clear(int layer)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            l.Clear();
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }

    [Serializable]
    public class LayeredDictionary<TKey, TKey2, TKey3, TValue> : LayeredCollection<Dictionary<TKey, TKey2, TKey3, TValue>>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public bool CreateLayersOnDemand { get; set; }

        public bool CreateEntriesOnDemand { get; set; }

        /// <summary>
        /// Layer key access
        /// </summary>
        public TValue this[int layer, TKey key1, TKey2 key2, TKey3 key3]
        {
            get
            {
                return internalLayers[layer][key1, key2, key3];
            }

            set
            {
                var l = CreateLayersOnDemand ? GetOrAddLayer(layer) : GetLayer(layer);

                if (l == null)
                    throw new IndexOutOfRangeException("Layer not exists");

                if (CreateEntriesOnDemand)
                    l.SetOrAdd(key1, key2, key3, value);
                else
                    l.Set(key1, key2, key3, value);
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="layers">Pre created layers</param>
        public LayeredDictionary(params int[] layers) : base(layers) {}

        /// <summary>
        /// Adds an element to a layer
        /// </summary>
        public void Add(int layer, TKey key1, TKey2 key2, TKey3 key3, TValue value)
        {
            var l = CreateLayersOnDemand ? GetOrAddLayer(layer) : GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            if (l.ContainsKey(key1, key2, key3))
                throw new ArgumentException("Key already exists");

            l.Add(key1, key2, key3, value);
        }

        /// <summary>
        /// Remove an element from a layer
        /// </summary>
        public void Remove(int layer, TKey key1, TKey2 key2, TKey3 key3)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            l.Remove(key1, key2, key3);
        }

        /// <summary>
        /// The layer contains the key?
        /// </summary>
        public bool ContainsKey(int layer, TKey key1, TKey2 key2, TKey3 key3)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            return l.ContainsKey(key1, key2, key3);
        }

        /// <summary>
        /// The layer contains the key?
        /// </summary>
        public bool ContainsValue(int layer, TValue value)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            return l.ContainsValue(value);
        }

        /// <summary>
        /// Clear a layer
        /// </summary>
        public void Clear(int layer)
        {
            var l = GetLayer(layer);

            if (l == null)
                throw new IndexOutOfRangeException("Layer not exists");

            l.Clear();
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}