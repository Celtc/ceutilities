﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.UtilityClasses
{
    public class Dictionary<TKey1, TKey2, TValue> : Dictionary<Tuple<TKey1, TKey2>, TValue>, IDictionary<Tuple<TKey1, TKey2>, TValue>
    {
        public TValue this[TKey1 key1, TKey2 key2]
        {
            get { return base[Tuple.Create(key1, key2)]; }
            set { base[Tuple.Create(key1, key2)] = value; }
        }

        public void Add(TKey1 key1, TKey2 key2, TValue value)
        {
            base.Add(Tuple.Create(key1, key2), value);
        }

        public void Set(Tuple<TKey1, TKey2> keys, TValue value)
        {
            this[keys] = value;
        }

        public void Set(TKey1 key1, TKey2 key2, TValue value)
        {
            this[key1, key2] = value;
        }

        public void SetOrAdd(Tuple<TKey1, TKey2> keys, TValue value)
        {
            if (!ContainsKey(keys))
                Add(keys, value);
            else
                this[keys] = value;
        }

        public void SetOrAdd(TKey1 key1, TKey2 key2, TValue value)
        {
            if (!ContainsKey(key1, key2))
                Add(key1, key2, value);
            else
                this[key1, key2] = value;
        }

        public void Remove(TKey1 key1, TKey2 key2)
        {
            base.Remove(Tuple.Create(key1, key2));
        }

        public bool ContainsKey(TKey1 key1, TKey2 key2)
        {
            return base.ContainsKey(Tuple.Create(key1, key2));
        }
    }

    public class Dictionary<TKey1, TKey2, TKey3, TValue> : Dictionary<Tuple<TKey1, TKey2, TKey3>, TValue>, IDictionary<Tuple<TKey1, TKey2, TKey3>, TValue>
    {
        public TValue this[TKey1 key1, TKey2 key2, TKey3 key3]
        {
            get { return base[Tuple.Create(key1, key2, key3)]; }
            set { base[Tuple.Create(key1, key2, key3)] = value; }
        }

        public void Add(TKey1 key1, TKey2 key2, TKey3 key3, TValue value)
        {
            base.Add(Tuple.Create(key1, key2, key3), value);
        }

        public void Set(TKey1 key1, TKey2 key2, TKey3 key3, TValue value)
        {
            this[key1, key2, key3] = value;
        }

        public void Set(Tuple<TKey1, TKey2, TKey3> keys, TValue value)
        {
            this[keys] = value;
        }

        public void SetOrAdd(TKey1 key1, TKey2 key2, TKey3 key3, TValue value)
        {
            if (!ContainsKey(key1, key2, key3))
                Add(key1, key2, key3, value);
            else
                this[key1, key2, key3] = value;
        }

        public void SetOrAdd(Tuple<TKey1, TKey2, TKey3> keys, TValue value)
        {
            if (!ContainsKey(keys))
                Add(keys, value);
            else
                this[keys] = value;
        }

        public void Remove(TKey1 key1, TKey2 key2, TKey3 key3)
        {
            base.Remove(Tuple.Create(key1, key2, key3));
        }

        public bool ContainsKey(TKey1 key1, TKey2 key2, TKey3 key3)
        {
            return base.ContainsKey(Tuple.Create(key1, key2, key3));
        }
    }
}