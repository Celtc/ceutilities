﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

namespace CEUtilities.UtilityClasses
{
    [System.Serializable]
    public class LayeredList<T> : LayeredCollection<List<T>>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        public bool CreateLayersOnDemand { get; set; }

        /// <summary>
        /// Layer index access
        /// </summary>
        public T this[int layer, int index]
        {
            get
            {
                return internalLayers[layer][index];
            }

            set
            {
                var l = CreateLayersOnDemand ? GetOrAddLayer(layer) : GetLayer(layer);

                if (l == null)
                    throw new IndexOutOfRangeException("Layer not exists");

                l[index] = value;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Adds an element to a layer
        /// </summary>
        public void Add(int layer, T element, bool createIfNecessary = true)
        {
            List<T> l = createIfNecessary ? GetOrAddLayer(layer) : GetLayer(layer);
            if (l == null)
                throw new System.IndexOutOfRangeException("Layer not exists");
            l.Add(element);
        }

        /// <summary>
        /// Remove an element from a layer
        /// </summary>
        public void Remove(int layer, T element)
        {
            List<T> l = GetLayer(layer);
            if (l == null)
                throw new System.IndexOutOfRangeException("Layer not exists");
            l.Remove(element);
        }

        /// <summary>
        /// Index of an element from a layer
        /// </summary>
        public int IndexOf(int layer, T element)
        {
            List<T> l = GetLayer(layer);
            if (l == null)
                throw new System.IndexOutOfRangeException("Layer not exists");
            return l.IndexOf(element);
        }

        #endregion Methods

        #region Non Public Methods

        #endregion Methods
    }
}