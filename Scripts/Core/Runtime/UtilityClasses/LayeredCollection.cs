﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CEUtilities.UtilityClasses
{
    [System.Serializable]
    public class LayeredCollection<T> : IEnumerable<KeyValuePair<int, T>>, IEnumerable where T : ICollection, IEnumerable
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        protected Dictionary<int, T> internalLayers;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Layer index access
        /// </summary>
        public T this[int layer]
        {
            get
            {
                return internalLayers[layer];
            }
        }

        /// <summary>
        /// All layers
        /// </summary>
        public IEnumerable<T> Layers
        {
            get
            {
                return internalLayers.Values;
            }
        }

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Ctor
        /// </summary>
        public LayeredCollection()
        {
            internalLayers = new Dictionary<int, T>();
        }

        /// <summary>
        /// Ctor
        /// </summary>
        public LayeredCollection(params int[] layers)
        {
            internalLayers = new Dictionary<int, T>();
            foreach (var layer in layers)
                AddLayer(layer);
        }

        /// <summary>
        /// Elements in a layer
        /// </summary>
        public int Count(int layer)
        {
            T l = GetLayer(layer);
            if (l == null)
                throw new System.IndexOutOfRangeException("Layer not exists");
            return l.Count;
        }

        /// <summary>
        /// Number of layers
        /// </summary>
        public int CountLayers()
        {
            return internalLayers.Keys.Count;
        }

        /// <summary>
        /// Elements in all layers
        /// </summary>
        public int CountAll()
        {
            int sum = 0;
            for (int i = 0; i < internalLayers.Keys.Count; i++)
                sum += internalLayers[i].Count;
            return sum;
        }

        /// <summary>
        /// Has a layer?
        /// </summary>
        public bool HasLayer(int layer)
        {
            return internalLayers.ContainsKey(layer);
        }

        /// <summary>
        /// Add a layer. If exist returns the existing one
        /// </summary>
        public T AddLayer(int layer)
        {
            return GetOrAddLayer(layer);
        }

        /// <summary>
        /// Removes a layer.
        /// </summary>
        public void RemoveLayer(int layer)
        {
            if (HasLayer(layer))
            {
                internalLayers.Remove(layer);
            }
        }

        /// <summary>
        /// Add a layer. If exist returns the existing one
        /// </summary>
        public void SetLayer(int layer, T value)
        {
            if (!HasLayer(layer))
            {
                AddLayer(layer);
            }
            internalLayers[layer] = value;
        }


        public IEnumerator<KeyValuePair<int, T>> GetEnumerator()
        {
            return internalLayers.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return internalLayers.GetEnumerator();
        }

        #endregion Methods

        #region Non Public Methods

        protected T GetOrAddLayer(int level)
        {
            T result;

            if (!internalLayers.ContainsKey(level))
            {
                result = Activator.CreateInstance<T>();
                internalLayers.Add(level, result);
            }
            else
            {
                result = internalLayers[level];
            }

            return result;
        }

        protected T GetLayer(int level)
        {
            T result = default(T);

            if (internalLayers.ContainsKey(level))
                result = internalLayers[level];

            return result;
        }

        #endregion Methods
    }
}