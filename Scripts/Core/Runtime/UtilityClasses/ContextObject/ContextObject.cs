﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CEUtilities
{
    [Serializable]
    public abstract class ContextObject
    {
        #region Static Factory

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject Create(Type type) => Create(type, type.GetDefault());

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject Create(Type type, object staticValue)
        {
            var genericBase = typeof(ContextObject<>);
            var combinedType = genericBase.MakeGenericType(type);
            return (ContextObject)Activator.CreateInstance(combinedType, staticValue);
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="typeOne">The type one.</param>
        /// <param name="typeTwo">The type two.</param>
        /// <returns></returns>
        public static ContextObject Create(Type typeOne, Type typeTwo)
        {
            var genericBase = typeof(ContextObject<,>);
            var combinedType = genericBase.MakeGenericType(typeOne, typeTwo);
            return (ContextObject)Activator.CreateInstance(combinedType);
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="typeOne">The type one.</param>
        /// <param name="typeTwo">The type two.</param>
        /// <returns></returns>
        public static ContextObject Create(Type typeOne, Type typeTwo, Type typeThree)
        {
            var genericBase = typeof(ContextObject<,,>);
            var combinedType = genericBase.MakeGenericType(typeOne, typeTwo, typeThree);
            return (ContextObject)Activator.CreateInstance(combinedType);
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="typeOne">The type one.</param>
        /// <param name="typeTwo">The type two.</param>
        /// <returns></returns>
        public static ContextObject Create(Type typeOne, Type typeTwo, Type typeThree, Type typeFour)
        {
            var genericBase = typeof(ContextObject<,,,>);
            var combinedType = genericBase.MakeGenericType(typeOne, typeTwo, typeThree, typeFour);
            return (ContextObject)Activator.CreateInstance(combinedType);
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject Create(string name, Type type) => Create(name, type, type.GetDefault());

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject Create(string name, Type type, object staticValue)
        {
            var genericBase = typeof(ContextObject<>);
            var combinedType = genericBase.MakeGenericType(type);
            return (ContextObject)Activator.CreateInstance(combinedType, name, staticValue);
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="typeOne">The type one.</param>
        /// <param name="typeTwo">The type two.</param>
        /// <returns></returns>
        public static ContextObject Create(string name, Type typeOne, Type typeTwo)
        {
            var genericBase = typeof(ContextObject<,>);
            var combinedType = genericBase.MakeGenericType(typeOne, typeTwo);
            return (ContextObject)Activator.CreateInstance(combinedType, name);
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="typeOne">The type one.</param>
        /// <param name="typeTwo">The type two.</param>
        /// <returns></returns>
        public static ContextObject Create(string name, Type typeOne, Type typeTwo, Type typeThree)
        {
            var genericBase = typeof(ContextObject<,,>);
            var combinedType = genericBase.MakeGenericType(typeOne, typeTwo, typeThree);
            return (ContextObject)Activator.CreateInstance(combinedType, name);
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="typeOne">The type one.</param>
        /// <param name="typeTwo">The type two.</param>
        /// <returns></returns>
        public static ContextObject Create(string name, Type typeOne, Type typeTwo, Type typeThree, Type typeFour)
        {
            var genericBase = typeof(ContextObject<,,,>);
            var combinedType = genericBase.MakeGenericType(typeOne, typeTwo, typeThree, typeFour);
            return (ContextObject)Activator.CreateInstance(combinedType, name);
        }


        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject<T> Create<T>(params DynamicParameter[] parameters)
        {
            var result = new ContextObject<T>()
            {
                Parameters = new DynamicParameterCollection(parameters)
            };
            return result;
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject<T1, T2> Create<T1, T2>(params DynamicParameter[] parameters)
        {
            var result = new ContextObject<T1, T2>()
            {
                Parameters = new DynamicParameterCollection(parameters)
            };
            return result;
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject<T1, T2, T3> Create<T1, T2, T3>(params DynamicParameter[] parameters)
        {
            var result = new ContextObject<T1, T2, T3>()
            {
                Parameters = new DynamicParameterCollection(parameters)
            };
            return result;
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject<T1, T2, T3, T4> Create<T1, T2, T3, T4>(params DynamicParameter[] parameters)
        {
            var result = new ContextObject<T1, T2, T3, T4>()
            {
                Parameters = new DynamicParameterCollection(parameters)
            };
            return result;
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject<T> Create<T>(string name, params DynamicParameter[] parameters)
        {
            var result = new ContextObject<T>(name)
            {
                Parameters = new DynamicParameterCollection(parameters)
            };
            return result;
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject<T1, T2> Create<T1, T2>(string name, params DynamicParameter[] parameters)
        {
            var result = new ContextObject<T1, T2>(name)
            {
                Parameters = new DynamicParameterCollection(parameters)
            };
            return result;
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject<T1, T2, T3> Create<T1, T2, T3>(string name, params DynamicParameter[] parameters)
        {
            var result = new ContextObject<T1, T2, T3>(name)
            {
                Parameters = new DynamicParameterCollection(parameters)
            };
            return result;
        }

        /// <summary>
        /// Creates the specified typed context object.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="staticValue">The static value.</param>
        /// <returns></returns>
        public static ContextObject<T1, T2, T3, T4> Create<T1, T2, T3, T4>(string name, params DynamicParameter[] parameters)
        {
            var result = new ContextObject<T1, T2, T3, T4>(name)
            {
                Parameters = new DynamicParameterCollection(parameters)
            };
            return result;
        }

        #endregion

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private string name;

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        private bool dynamic;

        [SerializeField]
        [HideInInspector]
        private int dynamicIndex;

        [SerializeField]
        [HideInInspector]
        private int staticIndex;

        [SerializeField]
        [HideInInspector]
        private DynamicParameterCollection parameters;

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ContextObject"/> is name.
        /// </summary>
        /// <value>
        ///   <c>true</c> if name; otherwise, <c>false</c>.
        /// </value>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value
        {
            get
            {
                return Dynamic ? DynamicValue : StaticValue;
            }

            set
            {
                Dynamic = true;

                StaticValue = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance has value.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance has value; otherwise, <c>false</c>.
        /// </value>
        public bool HasValue => Value != null;

        /// <summary>
        /// Gets or sets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public DynamicParameterCollection Parameters
        {
            get
            {
                return parameters;
            }

            protected set
            {
                if (parameters != value)
                {
                    // Unregister previous
                    if (parameters != null)
                    {
                        parameters.OnCollectionChanged -= OnParametersCollectionChangedCallback;
                    }

                    // Set
                    parameters = value;

                    // Register
                    parameters.OnCollectionChanged += OnParametersCollectionChangedCallback;

                    // First manual call
                    OnParametersCollectionChanged?.Invoke();
                }
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ContextObject"/> is dynamic.
        /// </summary>
        /// <value>
        ///   <c>true</c> if dynamic; otherwise, <c>false</c>.
        /// </value>
        protected bool Dynamic
        {
            get
            {
                return dynamic;
            }

            set
            {
                if (dynamic != value)
                {
                    dynamic = value;

                    OnChangedTypeOfReference?.Invoke();
                }
            }
        }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        protected int DynamicIndex
        {
            get
            {
                return dynamicIndex;
            }

            set
            {
                if (dynamicIndex != value)
                {
                    dynamicIndex = value;

                    if (Dynamic)
                    {
                        OnValueSourceChanged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the index of the static.
        /// </summary>
        /// <value>
        /// The index of the static.
        /// </value>
        protected int StaticIndex
        {
            get
            {
                return staticIndex;
            }

            set
            {
                if (staticIndex != value)
                {
                    staticIndex = value;

                    if (!Dynamic)
                    {
                        OnValueSourceChanged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the dynamic value.
        /// </summary>
        /// <value>
        /// The dynamic value.
        /// </value>
        protected virtual object DynamicValue
        {
            get
            {
                return Parameters.ElementAt(DynamicIndex).Value;
            }
        }

        /// <summary>
        /// Gets the static value.
        /// </summary>
        /// <value>
        /// The static value.
        /// </value>
        protected virtual object StaticValue
        {
            get
            {
                return GetStaticValue();
            }

            set
            {
                SetStaticValue(value);
            }
        }

#if UNITY_EDITOR

        [ShowInInspector]
        [HideLabel]
        [EnableGUI]
        [ShowIf("Dynamic")]
        [Spacing(After = 2, Before = 2)]
        [DisplayAsStringExtended(Style = "Tag TextField")]
        [InlineButton("ShowContextMenu", "+")]
        [InlineButton("EditorDynamicFieldInfo", "?")]
        [PropertyTooltip("$EditorDynamicFieldTooltip")]
        private string EditorDynamicField
        {
            get
            {
                string result = string.Empty;
                if (Dynamic && CheckValidSelection())
                {
                    result = Parameters.ElementAt(DynamicIndex).Name;
                }
                return result;
            }
        }

        /// <summary>
        /// Gets the editor dynamic field tooltip.
        /// </summary>
        /// <value>
        /// The editor dynamic field tooltip.
        /// </value>
        private void EditorDynamicFieldInfo()
        {
            var parameter = Parameters.ElementAt(DynamicIndex);
            var parameterType = parameter.GetType();

            var getterField = parameterType.GetField("getter", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            var getter = getterField.GetValue(parameter);
            var getterType = getter.GetType();

            var target = getterType.GetProperty("Target").GetValue(getter);
            var method = (System.Reflection.MethodInfo)getterType.GetProperty("Method").GetValue(getter);

            Debug.Log(string.Format("Target: {0} - Declaring type: {1} - Return type: {2}",
                target == null ? "None" :
                    target is UnityEngine.Object ? target.ToString() :
                    target.GetType().GetAliasOrName(),
                method.DeclaringType.GetAliasOrName(true, true),
                method.ReturnType.GetAliasOrName(true, true)
            ));
        }

#endif

        #endregion Properties

        #region Custom Events

        /// <summary>
        /// Triggered by changing the static value.
        /// </summary>
        [SerializeField]
        [HideInInspector]
        public SimpleEventHandler OnStaticValueChanged;

        /// <summary>
        /// Triggered when the source of the value changes.
        /// For ex. changing the pointed parameter, or changing between static or dynamic parameters.
        /// </summary>
        [SerializeField]
        [HideInInspector]
        public SimpleEventHandler OnValueSourceChanged;

        /// <summary>
        /// Triggered when changing the type of value reference. For ex chaging from static, to dynamic, or vice versa.
        /// </summary>
        [SerializeField]
        [HideInInspector]
        public SimpleEventHandler OnChangedTypeOfReference;

        /// <summary>
        /// Triggered when the dynamic parameters collection changed, by addning, removing, or modifing the collection.
        /// </summary>
        [SerializeField]
        [HideInInspector]
        public SimpleEventHandler OnParametersCollectionChanged;

        #endregion Custom Events

        #region Events methods

        /// <summary>
        /// Called when [parameters collection changed callback]. 
        /// Called by the collection itself, and propagated outside.
        /// </summary>
        private void OnParametersCollectionChangedCallback()
        {
            OnParametersCollectionChanged?.Invoke();
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject"/> class.
        /// </summary>
        public ContextObject() : this(string.Empty) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public ContextObject(string name)
        {
            Name = name;
            Parameters = new DynamicParameterCollection();
        }


        /// <summary>
        /// Selects the parameter.
        /// </summary>
        /// <param name="index">The index.</param>
        public void SelectDynamicParameter(int index)
        {
            Dynamic = true;

            DynamicIndex = index;
        }

        /// <summary>
        /// Clears the parameters.
        /// </summary>
        /// <returns></returns>
        public ContextObject ClearParameters()
        {
            Parameters.Clear();

            return this;
        }

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public ContextObject AddParameter(DynamicParameter parameter)
        {
            Parameters.Add(parameter);

            return this;
        }

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public ContextObject AddParameterRange(params DynamicParameter[] parameters)
        {
            foreach (var parameter in parameters)
            {
                Parameters.Add(parameter);
            }
            return this;
        }

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public ContextObject RemoveParameter(DynamicParameter parameter)
        {
            Parameters.Remove(parameter);

            return this;
        }

        /// <summary>
        /// Removes the parameter.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public ContextObject RemoveParameter(string name, string category)
        {
            Parameters.RemoveWhere((param) => param.Name == name && param.Category == category);

            return this;
        }

        /// <summary>
        /// Removes the category.
        /// </summary>
        /// <returns></returns>
        public ContextObject RemoveCategory(string category)
        {
            Parameters.RemoveWhere((param) => param.Category == category);

            return this;
        }

        /// <summary>
        /// Copies the parameters.
        /// </summary>
        /// <param name="from">From.</param>
        /// <returns></returns>
        public ContextObject CopyParametersFrom(ContextObject from)
        {
            foreach (var param in from.Parameters)
            {
                AddParameter(param);
            }
            return this;
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Checks the valid selection.
        /// </summary>
        protected virtual bool CheckValidSelection()
        {
            if (Dynamic && (DynamicIndex >= Parameters.Count || !ParamIsValid(DynamicIndex)))
            {
                Dynamic = false;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Gets the static value.
        /// </summary>
        /// <returns></returns>
        protected abstract object GetStaticValue();

        /// <summary>
        /// Sets the static value.
        /// This works only when the generics types are different, which should always be the case.
        /// </summary>
        /// <param name="value">The value.</param>
        protected abstract void SetStaticValue(object value);

        /// <summary>
        /// Parameters the is valid.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected abstract bool ParamIsValid(int index);


#if UNITY_EDITOR

        /// <summary>
        /// Shows the context menu.
        /// </summary>
        protected virtual void ShowContextMenu()
        {
            // Create the menu and add items to it
            var menu = new GenericMenu();

            // Add dynamic parameters
            MenuAddDynamicParams(ref menu);

            // Add static params
            MenuAddStaticParams(ref menu);

            // Display
            menu.ShowAsContext();
        }

        /// <summary>
        /// Builds the menu with all dynamic parameters (non static).
        /// </summary>
        /// <returns></returns>
        protected virtual void MenuAddDynamicParams(ref GenericMenu menu)
        {
            // Dynamic params
            if (Parameters.Count > 0)
            {
                var groupedParams = Parameters.GroupBy(x => x.Category);
                foreach (var group in groupedParams)
                {
                    // Category title
                    menu.AddSeparator("");
                    menu.AddDisabledItem(new GUIContent("• [ " + group.Key + " ]"));

                    foreach (var param in group)
                    {
                        var index = Parameters.IndexOf(param);
                        var guiContent = new GUIContent(string.Format("{0}* ({1})", param.Name, param.Type.GetAliasOrName(true, true)));

                        // Is param valid?
                        if (ParamIsValid(index))
                        {
                            var selected = Dynamic && DynamicIndex == index;
                            GenericMenu.MenuFunction onSelected = () =>
                            {
                                Dynamic = true;
                                DynamicIndex = index;
                            };
                            menu.AddItem(guiContent, selected, onSelected);
                        }

                        // Not a valid param
                        else
                        {
                            menu.AddDisabledItem(guiContent);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Builds the menu with all dynamic parameters (non static).
        /// </summary>
        /// <returns></returns>
        protected abstract void MenuAddStaticParams(ref GenericMenu menu);

#endif

        #endregion Methods
    }

    [Serializable]
    [InlineProperty]
    [HideReferenceObjectPicker]
    public class ContextObject<T> : ContextObject
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        private T staticValueOne;

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public T SmartValue
        {
            get
            {
                return Dynamic ? DynamicSmartValue : StaticSmartValue;
            }

            set
            {
                Dynamic = false;

                StaticSmartValue = value;
            }
        }

        /// <summary>
        /// Gets or sets the static value.
        /// </summary>
        /// <value>
        /// The static value.
        /// </value>
        protected T StaticSmartValue
        {
            get
            {
                return StaticValueOne;
            }

            set
            {
                StaticValueOne = value;
            }
        }

        /// <summary>
        /// Gets the dynamic smart value.
        /// </summary>
        /// <value>
        /// The dynamic smart value.
        /// </value>
        protected T DynamicSmartValue
        {
            get
            {
                return (DynamicSmartValue as DynamicParameter<T>).SmartValue;
            }
        }

        /// <summary>
        /// Gets or sets the static value one.
        /// </summary>
        /// <value>
        /// The static value one.
        /// </value>
        protected T StaticValueOne
        {
            get
            {
                return staticValueOne;
            }

            set
            {
                if (!EqualityComparer<T>.Default.Equals(staticValueOne, value))
                {
                    staticValueOne = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

#if UNITY_EDITOR

        [ShowInInspector]
        [HideLabel]
        [HideIf("Dynamic")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T EditorStaticField
        {
            get
            {
                return StaticValueOne;
            }

            set
            {
                StaticValueOne = value;
            }
        }

#endif

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T}" /> class.
        /// </summary>
        public ContextObject() : base()
        {
            StaticValue = default(T);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T}" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public ContextObject(string name) : base(name)
        {
            StaticValue = default(T);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T}" /> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T staticValue) : base()
        {
            StaticValue = staticValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T}" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T staticValue) : base(name)
        {
            StaticValue = staticValue;
        }


        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public new ContextObject<T> AddParameter(DynamicParameter parameter) => (ContextObject<T>)base.AddParameter(parameter);

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public new ContextObject<T> AddParameterRange(params DynamicParameter[] parameters) => (ContextObject<T>)base.AddParameterRange(parameters);

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public new ContextObject<T> RemoveParameter(DynamicParameter parameter) => (ContextObject<T>)base.RemoveParameter(parameter);

        /// <summary>
        /// Removes the parameter.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public new ContextObject<T> RemoveParameter(string name, string category) => (ContextObject<T>)base.RemoveParameter(name, category);

        /// <summary>
        /// Removes the category.
        /// </summary>
        /// <returns></returns>
        public new ContextObject<T> RemoveCategory(string category) => (ContextObject<T>)base.RemoveCategory(category);

        /// <summary>
        /// Copies the parameters.
        /// </summary>
        /// <param name="from">From.</param>
        /// <returns></returns>
        public new ContextObject<T> CopyParametersFrom(ContextObject from) => (ContextObject<T>)base.CopyParametersFrom(from);

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Gets the static value.
        /// </summary>
        /// <returns></returns>
        protected override object GetStaticValue()
        {
            return StaticValueOne;
        }

        /// <summary>
        /// Sets the static value.
        /// This works only when the generics types are different, which should always be the case.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void SetStaticValue(object value)
        {
            StaticValueOne = (T)value;
        }

        /// <summary>
        /// Parameters the is valid.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected override bool ParamIsValid(int index)
        {
            return typeof(T).IsAssignableFrom(Parameters.ElementAt(index).Type);
        }

#if UNITY_EDITOR

        /// <summary>
        /// Does the context menu.
        /// </summary>
        protected override void MenuAddStaticParams(ref GenericMenu menu)
        {
            // Add static params
            menu.AddSeparator("");
            menu.AddDisabledItem(new GUIContent("• [ Predefined Parameters ]"));
            menu.AddItem
            (
                new GUIContent(typeof(T).GetAliasOrName(true)),
                !Dynamic,
                () => Dynamic = false
            );
        }

#endif

        #endregion Non Public Methods
    }

    [Serializable]
    [InlineProperty]
    [HideReferenceObjectPicker]
    public class ContextObject<T1, T2> : ContextObject
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        private T1 staticValueOne;

        [SerializeField]
        [HideInInspector]
        private T2 staticValueTwo;

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the static value one.
        /// </summary>
        /// <value>
        /// The static value one.
        /// </value>
        protected T1 StaticValueOne
        {
            get
            {
                return staticValueOne;
            }

            set
            {
                if (!EqualityComparer<T1>.Default.Equals(staticValueOne, value))
                {
                    staticValueOne = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the static value two.
        /// </summary>
        /// <value>
        /// The static value two.
        /// </value>
        protected T2 StaticValueTwo
        {
            get
            {
                return staticValueTwo;
            }

            set
            {
                if (!EqualityComparer<T2>.Default.Equals(staticValueTwo, value))
                {
                    staticValueTwo = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

#if UNITY_EDITOR

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT1")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T1 EditorStaticFieldOne
        {
            get
            {
                return StaticValueOne;
            }

            set
            {
                StaticValueOne = value;
            }
        }

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT2")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T2 EditorStaticFieldTwo
        {
            get
            {
                return StaticValueTwo;
            }

            set
            {
                StaticValueTwo = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [showing t1].
        /// </summary>
        private bool ShowingT1 => !Dynamic && StaticIndex == 0;

        /// <summary>
        /// Gets a value indicating whether [showing t2].
        /// </summary>
        private bool ShowingT2 => !Dynamic && StaticIndex == 1;

#endif

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2}"/> class.
        /// </summary>
        public ContextObject() : base()
        {
            StaticIndex = 0;
            StaticValueOne = default(T1);
            StaticValueTwo = default(T2);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2}"/> class.
        /// </summary>
        public ContextObject(string name) : base(name)
        {
            StaticIndex = 0;
            StaticValueOne = default(T1);
            StaticValueTwo = default(T2);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T1 staticValue) : base()
        {
            StaticIndex = 0;
            StaticValueOne = staticValue;
            StaticValueTwo = default(T2);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T1 staticValue) : base(name)
        {
            StaticIndex = 0;
            StaticValueOne = staticValue;
            StaticValueTwo = default(T2);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T2 staticValue) : base()
        {
            StaticIndex = 1;
            StaticValueOne = default(T1);
            StaticValueTwo = staticValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T2 staticValue) : base(name)
        {
            StaticIndex = 1;
            StaticValueOne = default(T1);
            StaticValueTwo = staticValue;
        }


        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2> AddParameter(DynamicParameter parameter) => (ContextObject<T1, T2>)base.AddParameter(parameter);

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2> AddParameterRange(params DynamicParameter[] parameters) => (ContextObject<T1, T2>)base.AddParameterRange(parameters);

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2> RemoveParameter(DynamicParameter parameter) => (ContextObject<T1, T2>)base.RemoveParameter(parameter);

        /// <summary>
        /// Removes the parameter.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2> RemoveParameter(string name, string category) => (ContextObject<T1, T2>)base.RemoveParameter(name, category);

        /// <summary>
        /// Removes the category.
        /// </summary>
        /// <returns></returns>
        public new ContextObject<T1, T2> RemoveCategory(string category) => (ContextObject<T1, T2>)base.RemoveCategory(category);

        /// <summary>
        /// Copies the parameters.
        /// </summary>
        /// <param name="from">From.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2> CopyParametersFrom(ContextObject from) => (ContextObject<T1, T2>)base.CopyParametersFrom(from);

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Gets the static value.
        /// </summary>
        /// <returns></returns>
        protected override object GetStaticValue()
        {
            switch (StaticIndex)
            {
                case 0: return StaticValueOne;
                case 1: return StaticValueTwo;
                default: return null;
            }
        }

        /// <summary>
        /// Sets the static value.
        /// This works only when the generics types are different, which should always be the case.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void SetStaticValue(object value)
        {
            var valueType = value.GetType();
            if (typeof(T1).IsAssignableFrom(valueType))
            {
                StaticIndex = 0;
            }
            else if (typeof(T2).IsAssignableFrom(valueType))
            {
                StaticIndex = 1;
            }
            else
            {
                Debug.LogWarningFormat("[ContextObject] Trying to assign value of type '{0}' to a context which only accepts types '{1}' and '{2}'.",
                    valueType,
                    typeof(T1).GetAliasOrName(true, true),
                    typeof(T2).GetAliasOrName(true, true)
                );
                return;
            }

            switch (StaticIndex)
            {
                case 0: StaticValueOne = (T1)value; break;
                case 1: StaticValueTwo = (T2)value; break;
            }
        }

        /// <summary>
        /// Parameters the is valid.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected override bool ParamIsValid(int index)
        {
            var paramType = Parameters.ElementAt(index).Type;
            return typeof(T1).IsAssignableFrom(paramType) || typeof(T2).IsAssignableFrom(paramType);
        }

#if UNITY_EDITOR

        /// <summary>
        /// Does the context menu.
        /// </summary>
        protected override void MenuAddStaticParams(ref GenericMenu menu)
        {
            // Add static params
            menu.AddSeparator("");
            menu.AddDisabledItem(new GUIContent("• [ Predefined Parameters ]"));
            menu.AddItem
            (
                new GUIContent(typeof(T1).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 0,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 0;
                }
            );
            menu.AddItem
            (
                new GUIContent(typeof(T2).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 1,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 1;
                }
            );
        }

#endif

        #endregion Non Public Methods
    }

    [Serializable]
    [InlineProperty]
    [HideReferenceObjectPicker]
    public class ContextObject<T1, T2, T3> : ContextObject
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        private T1 staticValueOne;

        [SerializeField]
        [HideInInspector]
        private T2 staticValueTwo;

        [SerializeField]
        [HideInInspector]
        private T3 staticValueThree;

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the static value one.
        /// </summary>
        /// <value>
        /// The static value one.
        /// </value>
        public T1 StaticValueOne
        {
            get
            {
                return staticValueOne;
            }

            set
            {
                if (!EqualityComparer<T1>.Default.Equals(staticValueOne, value))
                {
                    staticValueOne = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the static value three.
        /// </summary>
        /// <value>
        /// The static value two.
        /// </value>
        public T2 StaticValueTwo
        {
            get
            {
                return staticValueTwo;
            }

            set
            {
                if (!EqualityComparer<T2>.Default.Equals(staticValueTwo, value))
                {
                    staticValueTwo = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the static value three.
        /// </summary>
        /// <value>
        /// The static value three.
        /// </value>
        public T3 StaticValueThree
        {
            get
            {
                return staticValueThree;
            }

            set
            {
                if (!EqualityComparer<T3>.Default.Equals(staticValueThree, value))
                {
                    staticValueThree = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

#if UNITY_EDITOR

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT1")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T1 EditorStaticFieldOne
        {
            get
            {
                return StaticValueOne;
            }

            set
            {
                StaticValueOne = value;
            }
        }

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT2")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T2 EditorStaticFieldTwo
        {
            get
            {
                return StaticValueTwo;
            }

            set
            {
                StaticValueTwo = value;
            }
        }

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT3")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T3 EditorStaticFieldThree
        {
            get
            {
                return StaticValueThree;
            }

            set
            {
                StaticValueThree = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [showing t1].
        /// </summary>
        private bool ShowingT1 => !Dynamic && StaticIndex == 0;

        /// <summary>
        /// Gets a value indicating whether [showing t2].
        /// </summary>
        private bool ShowingT2 => !Dynamic && StaticIndex == 1;

        /// <summary>
        /// Gets a value indicating whether [showing t3].
        /// </summary>
        private bool ShowingT3 => !Dynamic && StaticIndex == 2;

#endif

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3}"/> class.
        /// </summary>
        public ContextObject() : base()
        {
            StaticIndex = 0;
            StaticValueOne = default(T1);
            StaticValueTwo = default(T2);
            StaticValueThree = default(T3);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3}"/> class.
        /// </summary>
        public ContextObject(string name) : base(name)
        {
            StaticIndex = 0;
            StaticValueOne = default(T1);
            StaticValueTwo = default(T2);
            StaticValueThree = default(T3);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T1 staticValue) : base()
        {
            StaticIndex = 0;
            StaticValueOne = staticValue;
            StaticValueTwo = default(T2);
            StaticValueThree = default(T3);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T1 staticValue) : base(name)
        {
            StaticIndex = 0;
            StaticValueOne = staticValue;
            StaticValueTwo = default(T2);
            StaticValueThree = default(T3);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T2 staticValue) : base()
        {
            StaticIndex = 1;
            StaticValueOne = default(T1);
            StaticValueTwo = staticValue;
            StaticValueThree = default(T3);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T2 staticValue) : base(name)
        {
            StaticIndex = 1;
            StaticValueOne = default(T1);
            StaticValueTwo = staticValue;
            StaticValueThree = default(T3);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T3 staticValue) : base()
        {
            StaticIndex = 2;
            StaticValueOne = default(T1);
            StaticValueTwo = default(T2);
            StaticValueThree = staticValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T3 staticValue) : base(name)
        {
            StaticIndex = 2;
            StaticValueOne = default(T1);
            StaticValueTwo = default(T2);
            StaticValueThree = staticValue;
        }


        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3> AddParameter(DynamicParameter parameter) => (ContextObject<T1, T2, T3>)base.AddParameter(parameter);

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3> AddParameterRange(params DynamicParameter[] parameters) => (ContextObject<T1, T2, T3>)base.AddParameterRange(parameters);

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3> RemoveParameter(DynamicParameter parameter) => (ContextObject<T1, T2, T3>)base.RemoveParameter(parameter);

        /// <summary>
        /// Removes the parameter.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3> RemoveParameter(string name, string category) => (ContextObject<T1, T2, T3>)base.RemoveParameter(name, category);

        /// <summary>
        /// Removes the category.
        /// </summary>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3> RemoveCategory(string category) => (ContextObject<T1, T2, T3>)base.RemoveCategory(category);

        /// <summary>
        /// Copies the parameters.
        /// </summary>
        /// <param name="from">From.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3> CopyParametersFrom(ContextObject from) => (ContextObject<T1, T2, T3>)base.CopyParametersFrom(from);

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Gets the static value.
        /// </summary>
        /// <returns></returns>
        protected override object GetStaticValue()
        {
            switch (StaticIndex)
            {
                case 0: return StaticValueOne;
                case 1: return StaticValueTwo;
                case 2: return StaticValueThree;
                default: return null;
            }
        }

        /// <summary>
        /// Sets the static value.
        /// This works only when the generics types are different, which should always be the case.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void SetStaticValue(object value)
        {
            var valueType = value.GetType();
            if (typeof(T1).IsAssignableFrom(valueType))
            {
                StaticIndex = 0;
            }
            else if (typeof(T2).IsAssignableFrom(valueType))
            {
                StaticIndex = 1;
            }
            else if (typeof(T3).IsAssignableFrom(valueType))
            {
                StaticIndex = 2;
            }
            else
            {
                Debug.LogWarningFormat("[ContextObject] Trying to assign value of type '{0}' to a context which only accepts types '{1}', '{2}' and '{3}'.",
                    valueType,
                    typeof(T1).GetAliasOrName(true, true),
                    typeof(T2).GetAliasOrName(true, true),
                    typeof(T3).GetAliasOrName(true, true)
                );
                return;
            }

            switch (StaticIndex)
            {
                case 0: StaticValueOne = (T1)value; break;
                case 1: StaticValueTwo = (T2)value; break;
                case 3: StaticValueThree = (T3)value; break;
            }
        }

        /// <summary>
        /// Parameters the is valid.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected override bool ParamIsValid(int index)
        {
            var paramType = Parameters.ElementAt(index).Type;
            return typeof(T1).IsAssignableFrom(paramType) || typeof(T2).IsAssignableFrom(paramType) || typeof(T3).IsAssignableFrom(paramType);
        }

#if UNITY_EDITOR

        /// <summary>
        /// Does the context menu.
        /// </summary>
        protected override void MenuAddStaticParams(ref GenericMenu menu)
        {
            // Add static params
            menu.AddSeparator("");
            menu.AddDisabledItem(new GUIContent("• [ Predefined Parameters ]"));
            menu.AddItem
            (
                new GUIContent(typeof(T1).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 0,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 0;
                }
            );
            menu.AddItem
            (
                new GUIContent(typeof(T2).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 1,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 1;
                }
            );
            menu.AddItem
            (
                new GUIContent(typeof(T3).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 2,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 2;
                }
            );
        }

#endif

        #endregion Non Public Methods
    }

    [Serializable]
    [InlineProperty]
    [HideReferenceObjectPicker]
    public class ContextObject<T1, T2, T3, T4> : ContextObject
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        [SerializeField]
        [HideInInspector]
        private T1 staticValueOne;

        [SerializeField]
        [HideInInspector]
        private T2 staticValueTwo;

        [SerializeField]
        [HideInInspector]
        private T3 staticValueThree;

        [SerializeField]
        [HideInInspector]
        private T4 staticValueFour;

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the static value one.
        /// </summary>
        /// <value>
        /// The static value one.
        /// </value>
        public T1 StaticValueOne
        {
            get
            {
                return staticValueOne;
            }

            set
            {
                if (!EqualityComparer<T1>.Default.Equals(staticValueOne, value))
                {
                    staticValueOne = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the static value three.
        /// </summary>
        /// <value>
        /// The static value two.
        /// </value>
        public T2 StaticValueTwo
        {
            get
            {
                return staticValueTwo;
            }

            set
            {
                if (!EqualityComparer<T2>.Default.Equals(staticValueTwo, value))
                {
                    staticValueTwo = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the static value three.
        /// </summary>
        /// <value>
        /// The static value three.
        /// </value>
        public T3 StaticValueThree
        {
            get
            {
                return staticValueThree;
            }

            set
            {
                if (!EqualityComparer<T3>.Default.Equals(staticValueThree, value))
                {
                    staticValueThree = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the static value four.
        /// </summary>
        /// <value>
        /// The static value four.
        /// </value>
        public T4 StaticValueFour
        {
            get
            {
                return staticValueFour;
            }

            set
            {
                if (!EqualityComparer<T4>.Default.Equals(staticValueFour, value))
                {
                    staticValueFour = value;

                    if (!Dynamic)
                    {
                        OnStaticValueChanged?.Invoke();
                    }
                }
            }
        }

#if UNITY_EDITOR

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT1")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T1 EditorStaticFieldOne
        {
            get
            {
                return StaticValueOne;
            }

            set
            {
                StaticValueOne = value;
            }
        }

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT2")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T2 EditorStaticFieldTwo
        {
            get
            {
                return StaticValueTwo;
            }

            set
            {
                StaticValueTwo = value;
            }
        }

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT3")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T3 EditorStaticFieldThree
        {
            get
            {
                return StaticValueThree;
            }

            set
            {
                StaticValueThree = value;
            }
        }

        [ShowInInspector]
        [HideLabel]
        [ShowIf("ShowingT4")]
        [Spacing(After = 2, Before = 2)]
        [InlineButton("ShowContextMenu", "+")]
        private T4 EditorStaticFieldFour
        {
            get
            {
                return StaticValueFour;
            }

            set
            {
                StaticValueFour = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [showing t1].
        /// </summary>
        private bool ShowingT1 => !Dynamic && StaticIndex == 0;

        /// <summary>
        /// Gets a value indicating whether [showing t2].
        /// </summary>
        private bool ShowingT2 => !Dynamic && StaticIndex == 1;

        /// <summary>
        /// Gets a value indicating whether [showing t3].
        /// </summary>
        private bool ShowingT3 => !Dynamic && StaticIndex == 2;

        /// <summary>
        /// Gets a value indicating whether [showing t4].
        /// </summary>
        private bool ShowingT4 => !Dynamic && StaticIndex == 3;

#endif

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        public ContextObject() : base()
        {
            StaticIndex = 0;
            staticValueOne = default(T1);
            staticValueTwo = default(T2);
            staticValueThree = default(T3);
            staticValueFour = default(T4);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        public ContextObject(string name) : base(name)
        {
            StaticIndex = 0;
            staticValueOne = default(T1);
            staticValueTwo = default(T2);
            staticValueThree = default(T3);
            staticValueFour = default(T4);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T1 staticValue) : base()
        {
            StaticIndex = 0;
            staticValueOne = staticValue;
            staticValueTwo = default(T2);
            staticValueThree = default(T3);
            staticValueFour = default(T4);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T1 staticValue) : base(name)
        {
            StaticIndex = 0;
            staticValueOne = staticValue;
            staticValueTwo = default(T2);
            staticValueThree = default(T3);
            staticValueFour = default(T4);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T2 staticValue) : base()
        {
            StaticIndex = 1;
            staticValueOne = default(T1);
            staticValueTwo = staticValue;
            staticValueThree = default(T3);
            staticValueFour = default(T4);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T2 staticValue) : base(name)
        {
            StaticIndex = 1;
            staticValueOne = default(T1);
            staticValueTwo = staticValue;
            staticValueThree = default(T3);
            staticValueFour = default(T4);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T3 staticValue) : base()
        {
            StaticIndex = 2;
            staticValueOne = default(T1);
            staticValueTwo = default(T2);
            staticValueThree = staticValue;
            staticValueFour = default(T4);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T3 staticValue) : base(name)
        {
            StaticIndex = 2;
            staticValueOne = default(T1);
            staticValueTwo = default(T2);
            staticValueThree = staticValue;
            staticValueFour = default(T4);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(T4 staticValue) : base()
        {
            StaticIndex = 3;
            staticValueOne = default(T1);
            staticValueTwo = default(T2);
            staticValueThree = default(T3);
            staticValueFour = staticValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContextObject{T1, T2, T3, T4}"/> class.
        /// </summary>
        /// <param name="staticValue">The static value.</param>
        public ContextObject(string name, T4 staticValue) : base(name)
        {
            StaticIndex = 3;
            staticValueOne = default(T1);
            staticValueTwo = default(T2);
            staticValueThree = default(T3);
            staticValueFour = staticValue;
        }


        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3, T4> AddParameter(DynamicParameter parameter) => (ContextObject<T1, T2, T3, T4>)base.AddParameter(parameter);

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3, T4> AddParameterRange(params DynamicParameter[] parameters) => (ContextObject<T1, T2, T3, T4>)base.AddParameterRange(parameters);

        /// <summary>
        /// Adds a new parameter.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3, T4> RemoveParameter(DynamicParameter parameter) => (ContextObject<T1, T2, T3, T4>)base.RemoveParameter(parameter);

        /// <summary>
        /// Removes the parameter.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3, T4> RemoveParameter(string name, string category) => (ContextObject<T1, T2, T3, T4>)base.RemoveParameter(name, category);

        /// <summary>
        /// Removes the category.
        /// </summary>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3, T4> RemoveCategory(string category) => (ContextObject<T1, T2, T3, T4>)base.RemoveCategory(category);

        /// <summary>
        /// Copies the parameters.
        /// </summary>
        /// <param name="from">From.</param>
        /// <returns></returns>
        public new ContextObject<T1, T2, T3, T4> CopyParametersFrom(ContextObject from) => (ContextObject<T1, T2, T3, T4>)base.CopyParametersFrom(from);

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Gets the static value.
        /// </summary>
        /// <returns></returns>
        protected override object GetStaticValue()
        {
            switch (StaticIndex)
            {
                case 0: return StaticValueOne;
                case 1: return StaticValueTwo;
                case 2: return StaticValueThree;
                case 3: return StaticValueFour;
                default: return null;
            }
        }

        /// <summary>
        /// Sets the static value.
        /// This works only when the generics types are different, which should always be the case.
        /// </summary>
        /// <param name="value">The value.</param>
        protected override void SetStaticValue(object value)
        {
            var valueType = value.GetType();
            if (typeof(T1).IsAssignableFrom(valueType))
            {
                StaticIndex = 0;
            }
            else if (typeof(T2).IsAssignableFrom(valueType))
            {
                StaticIndex = 1;
            }
            else if (typeof(T3).IsAssignableFrom(valueType))
            {
                StaticIndex = 2;
            }
            else if (typeof(T4).IsAssignableFrom(valueType))
            {
                StaticIndex = 3;
            }
            else
            {
                Debug.LogWarningFormat("[ContextObject] Trying to assign value of type '{0}' to a context which only accepts types '{1}', '{2}', '{3}' nad '{4}'.",
                    valueType,
                    typeof(T1).GetAliasOrName(true, true),
                    typeof(T2).GetAliasOrName(true, true),
                    typeof(T3).GetAliasOrName(true, true),
                    typeof(T4).GetAliasOrName(true, true)
                );
                return;
            }

            switch (StaticIndex)
            {
                case 0: StaticValueOne = (T1)value; break;
                case 1: StaticValueTwo = (T2)value; break;
                case 2: StaticValueThree = (T3)value; break;
                case 3: StaticValueFour = (T4)value; break;
            }
        }

        /// <summary>
        /// Parameters the is valid.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        protected override bool ParamIsValid(int index)
        {
            var paramType = Parameters.ElementAt(index).Type;
            return typeof(T1).IsAssignableFrom(paramType) || typeof(T2).IsAssignableFrom(paramType) || typeof(T3).IsAssignableFrom(paramType) || typeof(T4).IsAssignableFrom(paramType);
        }

#if UNITY_EDITOR

        /// <summary>
        /// Does the context menu.
        /// </summary>
        protected override void MenuAddStaticParams(ref GenericMenu menu)
        {
            // Add static params
            menu.AddSeparator("");
            menu.AddDisabledItem(new GUIContent("• [ Predefined Parameters ]"));
            menu.AddItem
            (
                new GUIContent(typeof(T1).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 0,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 0;
                }
            );
            menu.AddItem
            (
                new GUIContent(typeof(T2).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 1,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 1;
                }
            );
            menu.AddItem
            (
                new GUIContent(typeof(T3).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 2,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 2;
                }
            );
            menu.AddItem
            (
                new GUIContent(typeof(T4).GetAliasOrName(true)),
                !Dynamic && StaticIndex == 3,
                () =>
                {
                    Dynamic = false;
                    StaticIndex = 3;
                }
            );
        }

#endif

        #endregion Non Public Methods
    }
}