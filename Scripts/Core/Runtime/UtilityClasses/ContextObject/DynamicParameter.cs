﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.Helpers;

namespace CEUtilities
{
    [Serializable]
    public abstract class DynamicParameter
    {
        #region Static Factory

        /// <summary>
        /// Creates the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="type">The type.</param>
        /// <param name="getter">The getter.</param>
        /// <returns></returns>
        public static DynamicParameter Create(string name, Type type, Func<object> getter)
        {
            var genericBase = typeof(DynamicParameter<>);
            var combinedType = genericBase.MakeGenericType(type);
            var lambda = TypeHelper.Convert(getter, type);

            return (DynamicParameter)Activator.CreateInstance(combinedType, new object[] { name, lambda });
        }

        /// <summary>
        /// Creates the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <param name="type">The type.</param>
        /// <param name="getter">The getter.</param>
        /// <returns></returns>
        public static DynamicParameter Create(string name, string category, Type type, Func<object> getter)
        {
            var genericBase = typeof(DynamicParameter<>);
            var combinedType = genericBase.MakeGenericType(type);
            var lambda = TypeHelper.Convert(getter, type);

            return (DynamicParameter)Activator.CreateInstance(combinedType, new object[] { name, category, lambda });
        }

        /// <summary>
        /// Creates a new instance of the <see cref="DynamicParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <param name="getter">The getter.</param>
        /// <returns></returns>
        public static DynamicParameter Create<T>(string name, Func<T> getter)
        {
            return new DynamicParameter<T>(name, getter);
        }

        /// <summary>
        /// Creates a new instance of the <see cref="DynamicParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <param name="getter">The getter.</param>
        /// <returns></returns>
        public static DynamicParameter Create<T>(string name, string category, Func<T> getter)
        {
            return new DynamicParameter<T>(name, category, getter);
        }

        #endregion Static Factory

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private string name;

        [SerializeField]
        [HideInInspector]
        private string category;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        public string Category
        {
            get
            {
                return category;
            }

            set
            {
                category = value;
            }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public abstract Type Type { get; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public abstract object Value { get; }

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        protected DynamicParameter(string name) : this(name, "Built-in") { }

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        protected DynamicParameter(string name, string category)
        {
            this.name = name;
            this.category = category;
        }

        #endregion Non Public Methods
    }

    [Serializable]
    public class DynamicParameter<T> : DynamicParameter
    {
        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        internal Func<T> getter;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public override Type Type => typeof(T);

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public T SmartValue => getter();

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public override object Value => SmartValue;

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicParameter{T}"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="getter">The getter.</param>
        public DynamicParameter(string name, Func<T> getter) : base(name)
        {
            this.getter = getter;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicParameter{T}"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="category">The category.</param>
        /// <param name="getter">The getter.</param>
        public DynamicParameter(string name, string category, Func<T> getter) : base(name, category)
        {
            this.getter = getter;
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}