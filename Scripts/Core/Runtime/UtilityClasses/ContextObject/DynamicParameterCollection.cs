﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using CEUtilities.UtilityClasses;

namespace CEUtilities
{
    [Serializable]
    public class DynamicParameterCollection : IEnumerable<DynamicParameter>, IEnumerable
    {
        #region Static Factory

        /// <summary>
        /// Creates a new instance of the <see cref="DynamicParameterCollection"/> class.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static DynamicParameterCollection Create(params DynamicParameter[] parameters)
        {
            return new DynamicParameterCollection(parameters);
        }

        #endregion Static Factory

        #region Exposed fields

        [SerializeField]
        [HideInInspector]
        private Dictionary<string, string, DynamicParameter> parameters = new Dictionary<string, string, DynamicParameter>();

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count => Parameters.Count;


        /// <summary>
        /// Gets or sets the parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        private Dictionary<string, string, DynamicParameter> Parameters
        {
            get
            {
                return parameters;
            }

            set
            {
                parameters = value;
            }
        }

        #endregion Properties

        #region Custom Events

        /// <summary>
        /// The on collection changed event.
        /// </summary>
        [SerializeField]
        [HideInInspector]
        public SimpleEventHandler OnCollectionChanged;

        #endregion Custom Events

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicParameterCollection"/> class.
        /// </summary>
        public DynamicParameterCollection()
        {
            Parameters = new Dictionary<string, string, DynamicParameter>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DynamicParameterCollection"/> class.
        /// </summary>
        /// <param name="collection">The collection.</param>
        public DynamicParameterCollection(IEnumerable<DynamicParameter> collection)
        {
            Parameters = new Dictionary<string, string, DynamicParameter>();

            AddRange(collection);
        }

        #region Collection Implementation

        /// <summary>
        /// Adds a new parameter to the collection.
        /// </summary>
        /// <param name="parameter">The item.</param>
        /// <returns></returns>
        public bool Add(DynamicParameter parameter)
        {
            var added = false;
            if (!Parameters.ContainsKey(parameter.Name, parameter.Category))
            {
                Parameters.Add(parameter.Name, parameter.Category, parameter);
                OnCollectionChanged?.Invoke();
                added = true;
            }
            return added;
        }

        /// <summary>
        /// Adds a range of items to the HashSet.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="parameers">The items.</param>
        /// <returns>Returns true any item is added.</returns>
        public int AddRange(IEnumerable<DynamicParameter> parameters)
        {
            var added = 0;
            foreach (var parameter in parameters)
            {
                if (!Parameters.ContainsKey(parameter.Name, parameter.Category))
                {
                    Parameters.Add(parameter.Name, parameter.Category, parameter);
                    added++;
                }
            }
            if (added > 0)
            {
                OnCollectionChanged?.Invoke();
            }
            return added;
        }

        /// <summary>
        /// Adds a range of items to the HashSet, if every items satisfy a predicate.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="items">The items.</param>
        /// <returns>Returns true all items are added.</returns>
        public int AddRange(IEnumerable<DynamicParameter> parameters, Func<DynamicParameter, bool> predicate)
        {
            var added = 0;
            foreach (var parameter in parameters)
            {
                if (!Parameters.ContainsKey(parameter.Name, parameter.Category) && predicate(parameter))
                {
                    Parameters.Add(parameter.Name, parameter.Category, parameter);
                    added++;
                }
            }
            if (added > 0)
            {
                OnCollectionChanged?.Invoke();
            }
            return added;
        }

        /// <summary>
        /// Clears the parameters.
        /// </summary>
        /// <returns></returns>
        public int Clear()
        {
            var cleared = Parameters.Count;
            if (cleared > 0)
            {
                Parameters.Clear();
                OnCollectionChanged?.Invoke();
            }
            return cleared;
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public bool Remove(DynamicParameter parameter)
        {
            var removed = false;
            if (Parameters.ContainsKey(parameter.Name, parameter.Category))
            {
                Parameters.Remove(parameter.Name, parameter.Category);
                OnCollectionChanged?.Invoke();
                removed = true;
            }
            return removed;
        }

        /// <summary>
        /// Removes the where.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns></returns>
        public int RemoveWhere(Predicate<DynamicParameter> match)
        {
            var removed = 0;
            foreach (var pair in Parameters)
            {
                if (match(pair.Value))
                {
                    Parameters.Remove(pair.Key);
                    removed++;
                }
            }
            if (removed > 0)
            {
                OnCollectionChanged?.Invoke();
            }
            return removed;
        }

        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Contains(DynamicParameter item)
        {
            return Parameters.ContainsKey(item.Name, item.Category);
        }


        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        public IEnumerator<DynamicParameter> GetEnumerator()
        {
            return Parameters.Values.GetEnumerator();
        }

        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (Parameters as IEnumerable).GetEnumerator();
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="d1">The d1.</param>
        /// <param name="d2">The d2.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static DynamicParameterCollection operator +(DynamicParameterCollection d1, DynamicParameterCollection d2)
        {
            var result = new DynamicParameterCollection();
            result.AddRange(d1);
            result.AddRange(d2);
            return result;
        }

        #endregion HashSet Implementation

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}