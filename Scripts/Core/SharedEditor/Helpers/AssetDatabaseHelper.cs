﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace CEUtilities.Helpers
{
    public static class AssetDatabaseHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        #endregion Properties

        #region Event Methods

        #endregion Event Methods

        #region Public Methods

        /// <summary>
        /// is the path relative? (No matter if it exists or not)
        /// </summary>
        public static bool IsRelative(string path)
        {
            return path.TrimStart(new char[] { '/', '\\' }).ToUpper().StartsWith("ASSETS");
        }

        /// <summary>
        /// Does a folder exists in the project hierarchy?
        /// Should start with "Assets/"
        /// </summary>
        public static bool ExistsRelativeFolder(string path)
        {
            return Directory.Exists(Path.GetDirectoryName(path));
        }

        /// <summary>
        /// Does a folder exists in the project hierarchy?
        /// Should start with "Assets/"
        /// </summary>
        public static bool ExistsRelativeFile(string path)
        {
            return File.Exists(path);
        }

        /// <summary>
        /// Copy a folder and all its contents but the meta files.
        /// Should start with "Assets/"
        /// </summary>
        public static void CopyFolder(string sourcePath, string newPath, bool overwrite)
        {
            // Create new path
            CreateFolderTree(newPath);

            // Copy folders
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(sourcePath, newPath));

            // Copy files
            foreach (string filePath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                if (!filePath.EndsWith(".meta"))
                    File.Copy(filePath, filePath.Replace(sourcePath, newPath), overwrite);
            }
        }

        /// <summary>
        /// Returns the relative to the project folder
        /// </summary>
        public static string GetRelativeProjectPath(string path)
        {
            var index = path.IndexOf("Assets");
            return index >= 0 ? path.Substring(index) : null;
        }

        /// <summary>
        /// Create a path inside "Assets" folder, path uses forward slashes "/"
        /// </summary>
        public static bool CreateFolderTree(string path)
        {
            var result = true;
            try
            {
                // Split by folders
                var nodes = new List<string>(path.Split('/'));

                // Check for folders to create
                if (nodes.Count == 0)
                    return true;

                // Check for initial node
                if (nodes[0].ToUpper() != "ASSETS")
                    nodes.Insert(0, "Assets");

                // Create path
                var constructedPath = nodes[0];
                for (int i = 1; i < nodes.Count; i++)
                {
                    var currNode = nodes[i];
                    if (!Directory.Exists(constructedPath + "/" + currNode))
                        AssetDatabase.CreateFolder(constructedPath, currNode);

                    constructedPath += "/" + currNode;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Loads all assets at folder of certain type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static List<T> LoadAllAssetsAtFolder<T>(string path) where T : Object
        {
            if (path != "")
            {
                if (path.EndsWith("/"))
                {
                    path = path.TrimEnd('/');
                }
            }

            DirectoryInfo dirInfo = new DirectoryInfo(path);
            FileInfo[] fileInfos = dirInfo.GetFiles();

            // Loop through directory loading the assets and checking it type
            List<T> loadedAssets = new List<T>();
            foreach (FileInfo fileInfo in fileInfos)
            {
                string fullPath = fileInfo.FullName.Replace(@"\", "/");
                string assetPath = "Assets" + fullPath.Replace(Application.dataPath, "");
                var asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(T));
                if (asset is T)
                {
                    loadedAssets.Add(asset as T);
                }
            }
            return loadedAssets;
        }

        /// <summary>
        /// So, there's no "load all assets in directory" function in unity. 
        /// I guess this is to avoid people using Prefabs as "data blobs". 
        /// They'd rather you use ScriptableObjects... which is fine in some cases, 
        /// but sometimes the thing you're blobbing is just a bunch of child transforms anyway,
        /// so it's huge buckets of sweat to reproduce the scene tools unity already has. 
        /// The "AssetsDatabase.LoadAllAssetsAtPath" refers to /compound/ raw assets,
        /// like maya files. But it doesn't care about humble prefabs, 
        /// which are more like compounds of assets, rather than raw assets.
        /// This function collates all the the behaviours you want in the directory you point it at. The path is relative to your Assets.
        /// i.e. "Assets/MyDirectory/"
        /// It returns the Prefab References, remember! Not instantiated scene objects! 
        /// So it's only used for *editor-side* tools. Not run time.
        /// Quite useful in conjunction with "PrefabUtility".
        /// </summary>
        public static List<T> LoadAllPrefabsOfType<T>(string path) where T : MonoBehaviour
        {
            if (path != "")
            {
                if (path.EndsWith("/"))
                {
                    path = path.TrimEnd('/');
                }
            }

            DirectoryInfo dirInfo = new DirectoryInfo(path);
            FileInfo[] fileInf = dirInfo.GetFiles("*.prefab");

            //loop through directory loading the game object and checking if it has the component you want
            List<T> prefabComponents = new List<T>();
            foreach (FileInfo fileInfo in fileInf)
            {
                string fullPath = fileInfo.FullName.Replace(@"\", "/");
                string assetPath = "Assets" + fullPath.Replace(Application.dataPath, "");
                GameObject prefab = AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)) as GameObject;

                if (prefab != null)
                {
                    T hasT = prefab.GetComponent<T>();
                    if (hasT != null)
                    {
                        prefabComponents.Add(hasT);
                    }
                }
            }
            return prefabComponents;
        }

        #endregion Public Methods

        #region Non Public Methods

        #endregion
    }
}
#endif