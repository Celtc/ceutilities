﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using LazyCache;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using Sirenix.Utilities;

namespace CEUtilities.Helpers
{
    public delegate void CloseDelegate();

    public delegate void SelectionDelegate<T>(T selection);

    public delegate string CustomElementNameDelegate<T>(T element);

    public class ObjectPicker<T>
    {
        #region Inner Types

        public class SearchResult
        {
            #region Exposed fields

            [ShowInInspector]
            [TableColumnWidth(60)]
            [DisplayAsString]
            public string name;

            [ShowInInspector]
            [DisplayAsString]
            [TableColumnWidth(260)]
            [GUIColorIf("isEmbedded", .7f, .7f, .7f, 1f)]
            public string path;

            [HideInInspector]
            [NonSerialized]
            public T value;

            [HideInInspector]
            [NonSerialized]
            public bool isEmbedded = false;

            #endregion Exposed fields

            #region Internal fields

            SelectionDelegate<T> selectionCallback;

            CustomElementNameDelegate<T> customNameCallback;

            #endregion Internal fields

            #region Custom Events

            #endregion Custom Events

            #region Properties

            #endregion Properties

            #region Events methods

            #endregion Events methods

            #region Public Methods

            /// <summary>
            /// Ctor.
            /// </summary>
            public SearchResult(T value, SelectionDelegate<T> selectionCallback, CustomElementNameDelegate<T> customNameCallback)
            {
                // Variables
                this.value = value;
                this.selectionCallback = selectionCallback;
                this.customNameCallback = customNameCallback;

                // Custom name callback
                if (customNameCallback != null)
                {
                    name = this.customNameCallback(value);
                }

                // Get name from element
                else
                {
                    // Unity object
                    if (value is UnityEngine.Object)
                    {
                        var uObj = value as UnityEngine.Object;
                        name = uObj.name;
                        path = AssetDatabase.GetAssetPath(uObj).TrimStart(new char[] { '/', '\\' }).TrimStart("Assets").TrimStart(new char[] { '/', '\\' });
                    }

                    // Generic obj
                    else
                    {
                        name = value.ToString();
                    }
                }

                // Empty path?
                if (string.IsNullOrEmpty(path))
                {
                    isEmbedded = true;
                    path = "[Embedded]";
                }
            }

            /// <summary>
            /// Selects this result.
            /// </summary>
            [TableColumnWidth(80, false)]
            [Button("Select")]
            public void Select()
            {
                selectionCallback?.Invoke(value);
            }

            #endregion Methods

            #region Non Public Methods

            #endregion Methods
        }

        #endregion

        #region Static

        /// <summary>
        /// Cache used for results.
        /// </summary>
        private static IAppCache cache = new CachingService();

        /// <summary>
        /// Shows the picker.
        /// </summary>
        public static void Show(string title, bool closeOnSelected, IList<T> elements, SelectionDelegate<T> selectedCallback, CustomElementNameDelegate<T> elementNameCallback, CloseDelegate closeCallback)
        {
            // Create picker
            var picker = new ObjectPicker<T>();
            picker.closeOnSelected = closeOnSelected;
            picker.selectedCallback = selectedCallback;
            picker.elementNameCallback = elementNameCallback;
            picker.DefinedValues = true;
            picker.rawResults = elements.Select(x => new SearchResult(x, picker.OnElementSelectedHandler, picker.elementNameCallback)).ToList();
            picker.PrepareResults();

            // Create editor
            var window = OdinEditorWindow.InspectObject(picker);
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(520f, 490f);
            window.titleContent = new GUIContent(title);
            window.OnClose += () => closeCallback?.Invoke();

            picker.window = window;
        }

        /// <summary>
        /// Shows the picker.
        /// </summary>
        public static void Show(string title, bool closeOnSelected, string searchFolder, SelectionDelegate<T> selectedCallback, CustomElementNameDelegate<T> elementNameCallback, CloseDelegate closeCallback)
        {
            // Create picker
            var picker = new ObjectPicker<T>();
            picker.closeOnSelected = closeOnSelected;
            picker.selectedCallback = selectedCallback;
            picker.elementNameCallback = elementNameCallback;
            picker.searchingFolder = searchFolder;
            picker.QuickSearch();

            // Create window
            var window = OdinEditorWindow.InspectObject(picker);
            window.position = GUIHelper.GetEditorWindowRect().AlignCenter(520f, 490f);
            window.titleContent = new GUIContent(title);
            window.OnClose += () => closeCallback?.Invoke();

            picker.window = window;
        }

        /// <summary>
        /// Shows the picker.
        /// </summary>
        public static IEnumerator _Show(string title, bool closeOnSelected, IList<T> elements, SelectionDelegate<T> selectedCallback, CustomElementNameDelegate<T> elementNameCallback)
        {
            var closed = false;
            Show(title, closeOnSelected, elements, selectedCallback, elementNameCallback, () => closed = true);
            yield return new WaitUntil(() => closed);
        }

        /// <summary>
        /// Shows the picker.
        /// </summary>
        public static IEnumerator _Show(string title, bool closeOnSelected, string searchFolder, SelectionDelegate<T> selectedCallback, CustomElementNameDelegate<T> elementNameCallback)
        {
            var closed = false;
            Show(title, closeOnSelected, searchFolder, selectedCallback, elementNameCallback, () => closed = true);
            yield return new WaitUntil(() => closed);
        }

        #endregion

        #region Exposed fields

        private string searchingFolder;

        private string filter;

        private bool assetsOnly;

        private bool closeOnSelected;

        private List<SearchResult> results;

        #endregion Exposed fields

        #region Internal fields

        private OdinEditorWindow window;

        private bool definedValues;

        private SelectionDelegate<T> selectedCallback;

        private CustomElementNameDelegate<T> elementNameCallback;

        private List<SearchResult> rawResults = new List<SearchResult>();

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        /// <summary>
        /// Gets the type of the generic.
        /// </summary>
        /// <value>
        /// The type of the searching.
        /// </value>
        public Type SearchingType => typeof(T);

        /// <summary>
        /// Type being picked.
        /// </summary>
        [ShowInInspector]
        [HideIf("DefinedValues")]
        [BoxGroup("Searching Options")]
        [LabelText("Searching for: ")]
        public string TypePrintable => typeof(T).GetAliasOrName(false, true);

        /// <summary>
        /// Folder to seach in when refreshed
        /// </summary>  
        [ShowInInspector]
        [HideIf("DefinedValues")]
        [BoxGroup("Searching Options")]
        [LabelText("Folder")]
        [FolderPath(RequireExistingPath = false, UseBackslashes = false)]
        public string SearchingFolder
        {
            get
            {
                return searchingFolder;
            }

            set
            {
                searchingFolder = value;
            }
        }

        /// <summary>
        /// Filter being applied
        /// </summary>
        [ShowInInspector]
        [BoxGroup("Searching Options")]
        [OnValueChanged("PrepareResults")]
        public string Filter
        {
            get
            {
                return filter;
            }

            set
            {
                filter = value;
                PrepareResults();
            }
        }

        /// <summary>
        /// Only assets?
        /// </summary>
        [ShowInInspector]
        [HideIf("DefinedValues")]
        [BoxGroup("Searching Options")]
        [PropertyOrder(4)]
        [OnValueChanged("PrepareResults")]
        public bool AssetsOnly
        {
            get
            {
                return assetsOnly;
            }

            set
            {
                assetsOnly = value;
                PrepareResults();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the editor will close on selection.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [close on selected]; otherwise, <c>false</c>.
        /// </value>
        public bool CloseOnSelected
        {
            get
            {
                return closeOnSelected;
            }

            set
            {
                closeOnSelected = value;
            }
        }

        /// <summary>
        /// Has results before filter?
        /// </summary>
        public bool HasRawResults => rawResults.Count > 0;

        /// <summary>
        /// Has results after filter?
        /// </summary>
        public bool HasResults => results.Count > 0;

        /// <summary>
        /// Filtered results
        /// </summary>
        [ShowInInspector]
        [Spacing(8)]
        [TableList(HideToolbar = true, AlwaysExpanded = true)]
        public List<SearchResult> Results
        {
            get
            {
                return results;
            }

            private set
            {
                results = value;
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether [defined values].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [defined values]; otherwise, <c>false</c>.
        /// </value>
        private bool DefinedValues
        {
            get
            {
                return definedValues;
            }

            set
            {
                definedValues = value;
            }
        }

        #endregion Properties

        #region Events methods

        /// <summary>
        /// Called upon selection
        /// </summary>
        protected virtual void OnElementSelectedHandler(T selected)
        {
            selectedCallback?.Invoke(selected);

            if (CloseOnSelected)
            {
                EditorApplication.delayCall += () => window?.Close();
            }
        }

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Do a quick search in selected folders.
        /// </summary>
        public void QuickSearch(bool useCache = true)
        {
            // Basic search of instances
            Search();

            // Deep search in an specific folder?
            if (!string.IsNullOrEmpty(searchingFolder))
            {
                DeepSearch(searchingFolder, useCache);
            }

            // Present results
            PrepareResults();
        }

        /// <summary>
        /// Do a quick search but without using the cache.
        /// </summary>
        [ButtonGroup("Searching Options/Buttons", order: 10)]
        [DisableIf("DefinedValues")]
        public void Refresh()
        {
            QuickSearch(false);
        }

        /// <summary>
        /// Searches all.
        /// </summary>
        [ButtonGroup("Searching Options/Buttons", order: 10)]
        [DisableIf("DefinedValues")]
        public void SearchAll()
        {
            Search();
            DeepSearch(null, false);
            PrepareResults();
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        /// Do a quick search, clearing all previous results.
        /// </summary>
        private void Search()
        {
            rawResults.Clear();

            if (SearchingType.IsSubclassOf(typeof(UnityEngine.Object)))
            {
                foreach (var value in Resources.FindObjectsOfTypeAll(typeof(T)))
                {
                    rawResults.Add(new SearchResult((T)(object)value, OnElementSelectedHandler, elementNameCallback));
                }
            }
        }

        /// <summary>
        /// Search the entire project or an specicif folder for required assets.
        /// </summary>
        /// <param name="folder">Folder to filter the deep search</param>
        /// <param name="readCache">If reading cache, and hit a cache result will return it and will not do a deep serach</param>
        private void DeepSearch(string folder, bool readCache = true)
        {
            // Cache key
            var cacheKey = SearchingType.GetAliasOrName(false, false) +
                (!string.IsNullOrEmpty(folder) ? (", " + folder) : "");

            // Read cache?
            if (readCache)
            {
                var cached = cache.Get<T[]>(cacheKey);
                if (cached != null && cached.Length != 0)
                {
                    rawResults = cached.Select(x => new SearchResult(x, OnElementSelectedHandler, elementNameCallback)).ToList();
                    return;
                }
            }

            // Get paths and type properties
            var paths = AssetDatabase.GetAllAssetPaths();
            bool isUnityObject = SearchingType.IsSubclassOf(typeof(UnityEngine.Object));
            bool isComponent = isUnityObject && SearchingType.IsSubclassOf(typeof(Component));
            bool isInterface = SearchingType.IsInterface;

            // Filter paths?
            if (!string.IsNullOrEmpty(folder))
            {
                paths = paths.Where(x => x.StartsWith(folder)).ToArray();
            }

            // For every path
            string path;
            object candidate;
            for (int i = 0; i < paths.Length; ++i)
            {
                EditorUtility.DisplayProgressBar("Loading", "Searching assets, please wait...", (float)i / paths.Length);

                path = paths[i];

                // Only projects assets
                if (!AssetDatabaseHelper.IsRelative(path))
                {
                    continue;
                }

                // Avoid repeats
                var uObj = AssetDatabase.LoadMainAssetAtPath(path);
                if (uObj == null || rawResults.Any(x => x.value.Equals(uObj)))
                {
                    continue;
                }

                candidate = null;

                // UObject
                if (isUnityObject)
                {
                    /*
                    // Only prefabs
                    if (PrefabUtility.GetPrefabType(uobj) != PrefabType.Prefab)
                        continue;
                    */

                    // Component -> Has component?
                    if (isComponent)
                    {
                        candidate = (uObj as GameObject)?.GetComponent(SearchingType);
                    }

                    // Unity Object
                    else if (uObj.GetType() == SearchingType)
                    {
                        candidate = uObj;
                    }
                }

                // Generic
                else if (isInterface)
                {
                    Type uobjType = uObj.GetType();
                    if (uobjType.IsSubclassOf(SearchingType) || SearchingType.IsAssignableFrom(uobjType))
                    {
                        candidate = uObj;
                    }
                }

                // Add to results
                if (candidate != null && !rawResults.Any(x => x.value.Equals(candidate)))
                {
                    rawResults.Add(new SearchResult((T)candidate, OnElementSelectedHandler, elementNameCallback));
                }

                // Add to cache
                cache.Add(cacheKey, rawResults.Select(x => x.value));
            }

            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// Update the results list from the raw results
        /// </summary>
        private void PrepareResults()
        {
            DoFilter();
            DoOrder();
        }

        /// <summary>
        /// Update the filter
        /// </summary>
        private void DoFilter()
        {
            results = new List<SearchResult>(rawResults);

            // Assets only?
            if (assetsOnly)
            {
                results.RemoveAll(x => x.isEmbedded);
            }

            // Filter
            if (!string.IsNullOrEmpty(filter))
            {
                var upperFilter = Filter.ToUpper();
                results.RemoveAll(x => !x.name.ToUpper().Contains(upperFilter));
            }
        }

        /// <summary>
        /// Order the results
        /// </summary>
        private void DoOrder()
        {
            if (results != null && results.Count > 0)
            {
                results.Sort(delegate (SearchResult a, SearchResult b) { return a.name.CompareTo(b.name); });
            }
        }

        #endregion Non Public Methods
    }
}
#endif
