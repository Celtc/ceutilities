﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace CEUtilities.Triggers
{
    public class StayAwake : MonoBehaviour
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Properties

        #endregion Properties

        #region Custom Events

        #endregion Custom Events

        #region Events methods

        private void Awake()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

#if UNITY_EDITOR

        [OnInspectorGUI]
        [PropertyOrder(int.MaxValue)]
        private void DrawInfoBox()
        {
             Sirenix.Utilities.Editor.SirenixEditorGUI.MessageBox(
                "Prevents device screen from turning off.\nMay require special permissions to work, for example \"android.permission.WAKE_LOCK\" in Android.",
                MessageType.Info,
                true
            );
        }

#endif

        #endregion Methods
    }
}