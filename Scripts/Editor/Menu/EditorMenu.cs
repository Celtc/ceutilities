﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace CEUtilities.Editor
{
    public class EditorMenu : ScriptableObject
    {
        // Add a menu item named "Do Something with a Shortcut Key" to MyMenu in the menu bar
        // and give it a shortcut (ctrl-g on Windows, cmd-g on OS X).
        [MenuItem("Tools/GameObject/Enable-Disable")]
        private static void ChangeEnableStatus()
        {
            foreach (GameObject go in Selection.gameObjects)
                go.SetActive(!go.activeSelf);
        }

        [MenuItem("Tools/GameObject/Enable-Disable", true)]
        private static bool ChangeEnableStatus_ValidateSelection()
        {
            return Selection.transforms.Length != 0;
        }


        // Add a menu item named "Do Something with a Shortcut Key" to MyMenu in the menu bar
        // and give it a shortcut (ctrl-g on Windows, cmd-g on OS X).
        [MenuItem("Tools/GameObject/Reset Transform (Child independant)")]
        private static void ResetTransformKeepChildsWorldTransform()
        {
            Undo.RecordObjects(Selection.gameObjects, "Reset transform");

            List<Transform> childs;
            foreach (GameObject go in Selection.gameObjects)
            {
                // Unparent childs
                childs = new List<Transform>(go.transform.childCount);
                for (int i = go.transform.childCount - 1; i >= 0; i--)
                {
                    var child = go.transform.GetChild(i);
                    childs.Add(child);
                    child.SetParent(null, true);
                }

                // Reset transform
                go.transform.localPosition = Vector3.zero;
                go.transform.localScale = Vector3.one;
                go.transform.localRotation = Quaternion.identity;

                // Reparent childs
                foreach (var child in childs)
                    child.SetParent(go.transform, true);
            }
        }

        [MenuItem("Tools/GameObject/Reset Transform (Child independant)", true)]
        private static bool ResetTransformKeepChildsWorldTransform_ValidateSelection()
        {
            return Selection.transforms.Length != 0;
        }


        /// <summary>
        /// Extract from a mesh one or more submehes
        /// </summary>
        [MenuItem("Tools/GameObject/Extract Submeshes")]
        private static void ExtractSubmesh()
        {
            EditorWindow.GetWindow<ExtractSubmeshes>().Show();
        }


        /// <summary>
        /// Add the childs of the current selection to the selection
        /// </summary>
        [MenuItem("Tools/Selection/Add Childs (1 level) %#DOWN")]
        private static void AddChildsToSelection()
        {
            var newSelection = new HashSet<GameObject>(Selection.gameObjects);
            foreach (var selected in Selection.gameObjects)
                newSelection.AddRange(selected.transform.GetLevelChilds(1).Select(x => x.gameObject));
            Selection.objects = newSelection.ToArray();
        }

        /// <summary>
        /// Select only the childs of the current selection
        /// </summary>
        [MenuItem("Tools/Selection/Select Childs %#&DOWN")]
        private static void SelectChilds()
        {
            var newSelection = new HashSet<GameObject>();
            foreach (var selected in Selection.gameObjects)
                newSelection.AddRange(selected.transform.GetLevelChilds(1).Select(x => x.gameObject));

            // Remove possible gameobjects previously selected (in case father and child were selected)
            newSelection.Except(Selection.gameObjects);

            Selection.objects = newSelection.ToArray();
        }


        /// <summary>
        /// Add the parents of the current selection to the selection
        /// </summary>
        [MenuItem("Tools/Selection/Add Parents (1 level) %#UP")]
        private static void AddParentsToSelection()
        {
            var newSelection = new HashSet<GameObject>(Selection.gameObjects);
            foreach (var selected in Selection.gameObjects)
                newSelection.Add(selected.transform.parent.gameObject);
            Selection.objects = newSelection.ToArray();
        }

        /// <summary>
        /// Select only the parents of the current selection
        /// </summary>
        [MenuItem("Tools/Selection/Select Parents %#&UP")]
        private static void SelectParents()
        {
            var newSelection = new HashSet<GameObject>();
            foreach (var selected in Selection.gameObjects)
                newSelection.Add(selected.transform.parent.gameObject);

            // Remove possible gameobjects previously selected (in case father and child were selected)
            newSelection.Except(Selection.gameObjects);

            Selection.objects = newSelection.ToArray();
        }


        /// <summary>
        /// Instance in the selected gameobject one o more prefabs
        /// </summary>
        [MenuItem("Tools/GameObject/Instance in selection")]
        private static void InstanceInSelection()
        {
            EditorWindow.GetWindow<InstanceInSelectionHelper>().Show();
        }
    }
}