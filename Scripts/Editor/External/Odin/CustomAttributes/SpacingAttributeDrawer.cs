﻿#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Editor.Drawers
{
    using Utilities.Editor;
    using UnityEngine;

    /// <summary>
    /// Draws properties marked with <see cref="SpacingAttribute"/>.
    /// Adds GUILayout.Space before and/or after field rendering, while maintaining any drawers further down in the drawer chain.
    /// </summary>
    [DrawerPriority(DrawerPriorityLevel.SuperPriority)]
    public sealed class SpacingAttributeDrawer : OdinAttributeDrawer<SpacingAttribute>
    {
        /// <summary>
        /// Draws the attribute.
        /// </summary>
        protected override void DrawPropertyLayout(GUIContent label)
        {
            GUILayout.Space(Attribute.Before);

            CallNextDrawer(label);

            GUILayout.Space(Attribute.After);
        }
    }
}
#endif