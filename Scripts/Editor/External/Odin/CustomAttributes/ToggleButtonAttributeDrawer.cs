﻿#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Editor.Drawers
{
    using UnityEngine;
    using UnityEditor;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Draws properties marked with <see cref="ToggleButtonAttribute"/>.
    /// Calls the properties ToString method to get the string to draw.
    /// </summary>
    /// <seealso cref="ButtonAttribute"/>
    /// <seealso cref="ButtonGroupAttribute"/>
    /// <seealso cref="InlineButtonAttribute"/>
    public class ToggleButtonAttributeDrawer : OdinAttributeDrawer<ToggleButtonAttribute, bool>
    {
        protected override void DrawPropertyLayout(GUIContent label)
        {
            PropertyContext<GUIStyle> buttonStyle;
            PropertyContext<GUIContent> buttonGuiContent;

            // GUI Content
            if (Property.Context.Get(this, "buttonGuiContent", out buttonGuiContent))
            {
                var content = !string.IsNullOrEmpty(Attribute.IconContent) ?
                    EditorGUIUtility.IconContent(Attribute.IconContent) :
                    new GUIContent();

                content.text = !string.IsNullOrEmpty(Attribute.Label) ? 
                    Attribute.Label : 
                    label.text;

                buttonGuiContent.Value = content;
            }

            // GUI Style
            if (Property.Context.Get(this, "buttonStyle", out buttonStyle))
            {
                buttonStyle.Value = !string.IsNullOrEmpty(Attribute.Style) ?
                    new GUIStyle(Attribute.Style) :
                    new GUIStyle("Button");
            }

            ValueEntry.WeakSmartValue = GUILayout.Toggle(ValueEntry.SmartValue, buttonGuiContent.Value, buttonStyle.Value);
        }
    }
}
#endif