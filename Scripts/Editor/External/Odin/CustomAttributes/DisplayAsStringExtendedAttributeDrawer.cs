﻿#if UNITY_EDITOR

namespace Sirenix.OdinInspector.Editor.Drawers
{
    using Sirenix.Utilities;
    using Sirenix.Utilities.Editor;
    using UnityEditor;
    using UnityEngine;

    /// <summary>
    /// Draws properties marked with <see cref="DisplayAsStringExtendedAttribute"/>.
    /// Calls the properties ToString method to get the string to draw.
    /// </summary>
    /// <seealso cref="DisplayAsStringAttribute"/>
    /// <seealso cref="HideLabelAttribute"/>
    /// <seealso cref="LabelTextAttribute"/>
    /// <seealso cref="InfoBoxAttribute"/>
    /// <seealso cref="DetailedInfoBoxAttribute"/>
    /// <seealso cref="MultiLinePropertyAttribute"/>
    /// <seealso cref="MultilineAttribute"/>
    public sealed class DisplayAsStringExtendedAttributeDrawer<T> : OdinAttributeDrawer<DisplayAsStringExtendedAttribute, T>
    {
        /// <summary>
        /// Draws the property.
        /// </summary>
        protected override void DrawPropertyLayout(GUIContent label)
        {
            var entry = this.ValueEntry;
            var attribute = this.Attribute;

            if (entry.Property.ChildResolver is ICollectionResolver)
            {
                this.CallNextDrawer(label);
                return;
            }

            string str = entry.SmartValue == null ? "Null" : entry.SmartValue.ToString();

            PropertyContext<GUIStyle> style;
            if (Property.Context.Get(this, "buttonStyle", out style))
            {
                style.Value = !string.IsNullOrEmpty(Attribute.Style) ?
                    new GUIStyle(Attribute.Style) :
                    new GUIStyle("label");

                style.Value.richText = true;
                style.Value.stretchWidth = false;
                if (!attribute.Overflow)
                {
                    style.Value.wordWrap = true;
                    style.Value.margin = new RectOffset(0, 0, 0, 0);
                }
            }

            if (label == null)
            {
                EditorGUILayout.LabelField(str, style.Value, GUILayoutOptions.MinWidth(0));
            }
            else if (!attribute.Overflow)
            {
                var stringLabel = GUIHelper.TempContent(str);
                var position = EditorGUILayout.GetControlRect(false, SirenixGUIStyles.MultiLineLabel.CalcHeight(stringLabel, entry.Property.LastDrawnValueRect.width - GUIHelper.BetterLabelWidth), GUILayoutOptions.MinWidth(0));
                var rect = EditorGUI.PrefixLabel(position, label);
                GUI.Label(rect, stringLabel, style.Value);
            }
            else
            {
                int id;
                bool keyboard;
                Rect rect;
                SirenixEditorGUI.GetFeatureRichControlRect(label, out id, out keyboard, out rect);
                GUI.Label(rect, str, style.Value);
            }
        }
    }
}
#endif