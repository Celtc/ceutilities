﻿#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Editor.Drawers
{
    using Utilities.Editor;
    using UnityEngine;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Draws properties marked with <see cref="RegexAttribute"/>.
    /// Draw a helpbox with error in case the regex validation fails.
    /// Sample:
    ///   * For IPv4: [Regex(@"^(?:\d{1,3}\.){3}\d{1,3}$", "Invalid IPv4 address!\nExample: '127.0.0.1'")]
    ///   * For IPv6: [Regex(@"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", "Invalid IPv6 address!\nExample: 'FE80:0000:0000:0000:0202:B3FF:FE1E:8329'")]
    /// </summary>
    [DrawerPriority(DrawerPriorityLevel.SuperPriority)]
    public sealed class RegexAttributeDrawer : OdinAttributeDrawer<RegexAttribute>
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields        

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        /// <summary>
        /// Draws the attribute.
        /// </summary>
        protected override void DrawPropertyLayout(GUIContent label)
        {
            if (!IsValid(Property, Attribute.pattern))
            {
                SirenixEditorGUI.ErrorMessageBox(Attribute.helpMessage);
            }
            CallNextDrawer(label);
        }

        #endregion Events methods

        #region Public Methods

        #endregion Methods

        #region Non Public Methods

        bool IsValid(InspectorProperty prop, string pattern)
        {
            return Regex.IsMatch((string)prop.ValueEntry.WeakSmartValue, pattern);
        }

        #endregion Methods
    }
}
#endif