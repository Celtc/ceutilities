﻿#if UNITY_EDITOR
//-----------------------------------------------------------------------
// <copyright file="EnabledIfAttributeDrawer.cs" company="Sirenix IVS">
// Copyright (c) Sirenix IVS. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace Sirenix.OdinInspector.Editor.Drawers
{
    using System;
    using System.Reflection;
    using Utilities.Editor;
    using UnityEngine;
    using UnityEditor;
    using Utilities;

    internal static class IndentingAttributesHelper
    {
        private class IndentingAttributesContext
        {
            public Func<int> StaticMemberGetter;
            public Func<object, int> InstanceMemberGetter;
            public string ErrorMessage;
            public int Result;
        }

        public static void HandleIndentingAttributeCondition(OdinDrawer drawer, InspectorProperty property, string memberName, out int result, out string errorMessage)
        {
            var context = property.Context.Get(drawer, "IndentingAttributeContext", (IndentingAttributesContext)null);

            if (context.Value == null)
            {
                context.Value = new IndentingAttributesContext();
                MemberInfo memberInfo = property.ParentType
                    .FindMember()
                    .IsNamed(memberName)
                    .HasNoParameters()
                    .GetMember(out context.Value.ErrorMessage);

                if (memberInfo != null)
                {
                    string name = (memberInfo is MethodInfo) ? memberInfo.Name + "()" : memberInfo.Name;

                    if (memberInfo.GetReturnType() == typeof(int))
                    {
                        if (memberInfo.IsStatic())
                        {
                            context.Value.StaticMemberGetter = DeepReflection.CreateValueGetter<int>(property.ParentType, name);
                        }
                        else
                        {
                            context.Value.InstanceMemberGetter = DeepReflection.CreateWeakInstanceValueGetter<int>(property.ParentType, name);
                        }
                    }
                    else
                    {
                        context.Value.ErrorMessage = "An member with a non-int value was referenced.";
                    }
                }
            }
            errorMessage = context.Value.ErrorMessage;

            if (Event.current.type != EventType.Layout)
            {
                result = context.Value.Result;
                return;
            }

            context.Value.Result = 0;

            if (context.Value.ErrorMessage == null)
            {
                if (context.Value.InstanceMemberGetter != null)
                {
                    if (property.ParentValues.Count > 0)
                    {
                        context.Value.Result = context.Value.InstanceMemberGetter(property.ParentValues[0]);
                    }
                    /*
                    for (int i = 0; i < property.ParentValues.Count; i++)
                    {
                        if (context.Value.InstanceMemberGetter(property.ParentValues[i]))
                        {
                            context.Value.Result = true;
                            break;
                        }
                    }
                    */
                }
                else if (context.Value.StaticMemberGetter != null)
                {
                    context.Value.Result = context.Value.StaticMemberGetter();
                    /*
                    if (context.Value.StaticMemberGetter())
                    {
                        context.Value.Result = true;
                    }
                    */
                }
            }

            result = context.Value.Result;
        }
    }


    /// <summary>
    /// Draws properties marked with <see cref="IndentingAttribute"/>.
    /// </summary>
    [DrawerPriority(DrawerPriorityLevel.SuperPriority)]
    public sealed class IndentingAttributeDrawer : OdinAttributeDrawer<IndentingAttribute>
    {
        /// <summary>
        /// Draws the property.
        /// </summary>
        protected override void DrawPropertyLayout(GUIContent label)
        {
            if (GUI.enabled == false)
            {
                CallNextDrawer(label);
                return;
            }

            int result;
            string errorMessage;

            IndentingAttributesHelper.HandleIndentingAttributeCondition(this, Property, Attribute.MemberName, out result, out errorMessage);

            if (errorMessage != null)
            {
                SirenixEditorGUI.ErrorMessageBox(errorMessage);
                CallNextDrawer(label);
            }
            else
            {
                GUIHelper.PushIndentLevel(EditorGUI.indentLevel + result);
                CallNextDrawer(label);
                GUIHelper.PopIndentLevel();
            }
        }
    }
}
#endif