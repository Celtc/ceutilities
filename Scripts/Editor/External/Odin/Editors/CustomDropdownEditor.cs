﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector.Editor;
using System;
using System.Reflection;
using System.Linq;
using Sirenix.Utilities.Editor;

namespace CEUtilities.UI
{
    [CustomEditor(typeof(CustomDropdown), true)]
    public class CustomDropdownEditor : SelectableOdinEditor
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        private SerializedProperty m_Template;
        private SerializedProperty m_CaptionText;
        private SerializedProperty m_CaptionImage;
        private SerializedProperty m_ItemText;
        private SerializedProperty m_ItemImage;
        private SerializedProperty m_OnSelectionChanged;
        private SerializedProperty m_Value;
        private SerializedProperty m_Options;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        protected override void OnEnable()
        {
            base.OnEnable();
            this.m_Template = this.serializedObject.FindProperty("m_Template");
            this.m_CaptionText = this.serializedObject.FindProperty("m_CaptionText");
            this.m_CaptionImage = this.serializedObject.FindProperty("m_CaptionImage");
            this.m_ItemText = this.serializedObject.FindProperty("m_ItemText");
            this.m_ItemImage = this.serializedObject.FindProperty("m_ItemImage");
            this.m_OnSelectionChanged = this.serializedObject.FindProperty("m_OnValueChanged");
            this.m_Value = this.serializedObject.FindProperty("m_Value");
            this.m_Options = this.serializedObject.FindProperty("m_Options");
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            // Base unity selectable GUI
            DrawUnityInspector();

            // Base dropdown GUI
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(this.m_Template);
            EditorGUILayout.PropertyField(this.m_CaptionText);
            EditorGUILayout.PropertyField(this.m_CaptionImage);
            EditorGUILayout.PropertyField(this.m_ItemText);
            EditorGUILayout.PropertyField(this.m_ItemImage);
            EditorGUILayout.PropertyField(this.m_Value);
            EditorGUILayout.PropertyField(this.m_Options);
            EditorGUILayout.PropertyField(this.m_OnSelectionChanged);
            this.serializedObject.ApplyModifiedProperties();

            // Custom GUI
            var targetType = Tree.TargetType;
            EditorGUILayout.Space();
            SirenixEditorGUI.BeginBox(targetType.GetAliasOrName(true, true));
            DrawChildGUI(Tree.TargetType);
            SirenixEditorGUI.EndBox();
        }

        #endregion Events methods

        #region Public Methods

        #endregion Public Methods

        #region Non Public Methods

        #endregion Non Public Methods
    }
}