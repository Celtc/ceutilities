﻿#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Editor.Drawers
{
    using Sirenix.Utilities.Editor;
    using System;
    using UnityEngine;
    using UnityEditor;
    using Sirenix.Utilities;

    /// <summary>
    /// Int property drawer.
    /// </summary>
    [DrawerPriority(DrawerPriorityLevel.ValuePriority)]
    public sealed class GuidDrawer : OdinValueDrawer<Guid>
    {
        /// <summary>
        /// Draws the property.
        /// </summary>
        protected override void DrawPropertyLayout(GUIContent label)
        {
            if (ValueEntry.Property.Info.GetMemberInfo().IsDefined(typeof(MinimalGuidAttribute), true))
            {
                ValueEntry.SmartValue = GuidMinimalField(label, ValueEntry.SmartValue);
            }
            else
            {
                CallNextDrawer(label);
            }
        }

        /// <summary>
        /// Draws a Guid field.
        /// </summary>
        /// <param name="label">Label of field. Set to <c>null</c> for no label.</param>
        /// <param name="value">Current value.</param>
        /// <returns>Value assigned to the field.</returns>
        public static Guid GuidMinimalField(GUIContent label, Guid value)
        {
            Rect rect = EditorGUILayout.GetControlRect(label != null, EditorGUIUtility.singleLineHeight, EditorStyles.textField);

            if (label != null)
            {
                rect = EditorGUI.PrefixLabel(rect, label, EditorStyles.label);
            }

            string text = value.ToString("D");

            EditorGUI.BeginChangeCheck();

            string newText = EditorGUI.DelayedTextField(rect, text, EditorStyles.textField);

            if (EditorGUI.EndChangeCheck() || newText != text)
            {
                text = newText;

                try
                {
                    value = new Guid(text);
                    GUI.changed = true;
                }
                catch
                {
                    // Ignore
                }
            }

            return value;
        }
    }
}
#endif