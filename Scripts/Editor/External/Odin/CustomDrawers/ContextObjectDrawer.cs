﻿#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Editor.Drawers
{
    using UnityEngine;
    using UnityEditor;
    using UnityEditor.Callbacks;
    using System;
    using System.Reflection;
    using System.Collections.Generic;

    using Sirenix.Utilities;
    using CEUtilities;
    using static UnityEditor.GenericMenu;
    using System.Linq;
    using Sirenix.Utilities.Editor;

    /*
    /// <summary>
    /// Int property drawer.
    /// </summary>
    [DrawerPriority(DrawerPriorityLevel.ValuePriority)]
    public sealed class ContextObjectDrawer<T> : OdinValueDrawer<T> where T : ContextObject
    {
        #region Internal Fields

        private static GUIStyle dynamicFieldStyle;

        private static FieldInfo typeOfValueField;
        private static PropertyInfo propertyTypeProp;
        private static PropertyInfo baseValueEntryProp;
        private static PropertyInfo valueEntryProp;
        private static PropertyInfo childrenProp;
        private static FieldInfo childrenLastSeenTypeField;
        private static MethodInfo updateChildrenMethod;

        private static Dictionary<InspectorProperty, ContextObject> generatedEditors = new Dictionary<InspectorProperty, ContextObject>();

        #endregion Internal Fields

        #region Properties

        public static GUIStyle DynamicFieldStyle
        {
            get
            {
                if (dynamicFieldStyle == null)
                {
#if UNITY_PRO_LICENSE 
                    var colorFG = new Color(220f / 255f, 220f / 255f, 220f / 255f, .4f);
#else
                    var colorFG = new Color(75f / 255f, 75f / 255f, 220f / 75f, .4f);
#endif
                    dynamicFieldStyle = new GUIStyle("textField");

                    var normal = dynamicFieldStyle.normal;
                    normal.textColor = colorFG;
                    dynamicFieldStyle.normal = normal;

                    var onNormal = dynamicFieldStyle.onNormal;
                    onNormal.textColor = colorFG;
                    dynamicFieldStyle.onNormal = onNormal;

                    var active = dynamicFieldStyle.active;
                    active.textColor = colorFG;
                    dynamicFieldStyle.active = active;
                }
                return dynamicFieldStyle;
            }
        }

        public static FieldInfo TypeOfValueField
        {
            get
            {
                if (typeOfValueField == null)
                {
                    typeOfValueField = typeof(InspectorPropertyInfo).GetField("typeOfValue", BindingFlags.Instance | BindingFlags.NonPublic);
                }
                return typeOfValueField;
            }
        }

        public static PropertyInfo PropertyTypeProp
        {
            get
            {
                if (propertyTypeProp == null)
                {
                    propertyTypeProp = typeof(InspectorPropertyInfo).GetProperty("PropertyType", BindingFlags.Instance | BindingFlags.Public);
                }
                return propertyTypeProp;
            }
        }

        public static PropertyInfo BaseValueEntryProp
        {
            get
            {
                if (baseValueEntryProp == null)
                {
                    baseValueEntryProp = typeof(InspectorProperty).GetProperty("BaseValueEntry", BindingFlags.Instance | BindingFlags.Public);
                }
                return baseValueEntryProp;
            }
        }

        public static PropertyInfo ValueEntryProp
        {
            get
            {
                if (valueEntryProp == null)
                {
                    valueEntryProp = typeof(InspectorProperty).GetProperty("ValueEntry", BindingFlags.Instance | BindingFlags.Public);
                }
                return valueEntryProp;
            }
        }

        public static PropertyInfo ChildrenProp
        {
            get
            {
                if (childrenProp == null)
                {
                    childrenProp = typeof(InspectorProperty).GetProperty("Children", BindingFlags.Instance | BindingFlags.Public);
                }
                return childrenProp;
            }
        }

        public static FieldInfo ChildrenLastSeenTypeField
        {
            get
            {
                if (childrenLastSeenTypeField == null)
                {
                    childrenLastSeenTypeField = typeof(InspectorProperty).GetField("children_LastSeenValueEntryType", BindingFlags.Instance | BindingFlags.NonPublic);
                }
                return childrenLastSeenTypeField;
            }
        }

        public static MethodInfo UpdateChildrenMethod
        {
            get
            {
                if (updateChildrenMethod == null)
                {
                    updateChildrenMethod = typeof(InspectorProperty).GetMethod("UpdateChildren", BindingFlags.Instance | BindingFlags.NonPublic);
                }
                return updateChildrenMethod;
            }
        }

        #endregion Properties

        #region Event Methods

        [DidReloadScripts]
        private static void OnReloadScripts()
        {
            generatedEditors.Clear();
        }

        #endregion Event Methods

        #region Non Public Methods

        /// <summary>
        /// Draws the property.
        /// </summary>
        protected override void DrawPropertyLayout(GUIContent label)
        {
            var entryValue = ValueEntry.SmartValue;
            var valueProperty = ValueEntry.Property.Children["value"];

            // Type checks
            VerifyObjectValue(entryValue);

            // Property has changed => regenerate editor
            if (generatedEditors.ContainsKey(ValueEntry.Property) && generatedEditors[ValueEntry.Property].validTypes != entryValue.validTypes)
            {
                generatedEditors[ValueEntry.Property] = entryValue;
                if (!entryValue.isDynamic)
                {
                    var type = entryValue.validTypes[entryValue.Index];
                    RegenerateEditor(type, valueProperty);
                }
            }

            // First time drawing property => regenerate editor
            if (!generatedEditors.ContainsKey(ValueEntry.Property))
            {
                generatedEditors.Add(ValueEntry.Property, entryValue);
                if (!entryValue.isDynamic)
                {
                    var type = entryValue.validTypes[entryValue.Index];
                    RegenerateEditor(type, valueProperty);
                }
            }

            EditorGUILayout.BeginHorizontal();

            // Prefix Label
            var showedLabel = label ?? new GUIContent("Not Defined");
            if (!string.IsNullOrEmpty(entryValue.Label))
            {
                showedLabel.text = entryValue.Label;
            }
            EditorGUILayout.PrefixLabel(showedLabel);

            EditorGUILayout.BeginVertical();

            // Draw a label field, or the actual property field
            if (entryValue.isDynamic)
            {
                var prevColorBG = GUI.backgroundColor;
                GUI.backgroundColor = Color.white;
                EditorGUILayout.LabelField(entryValue.dynamicParams[entryValue.Index].Name, DynamicFieldStyle);
                GUI.backgroundColor = prevColorBG;
            }
            else
            {
                valueProperty.Draw(GUIContent.none);
            }

            EditorGUILayout.EndVertical();

            // Change value
            if (GUILayout.Button("+", EditorStyles.miniButton, GUILayoutOptions.ExpandWidth(false).MinWidth(20)))
            {
                DoContextMenu(valueProperty, entryValue);
            }

            EditorGUILayout.EndHorizontal();

            // Property has changed => regenerate editor
            if (generatedEditors.ContainsKey(ValueEntry.Property) && generatedEditors[ValueEntry.Property] != ValueEntry.SmartValue)
            {
                Debug.Log("3");

                generatedEditors[ValueEntry.Property] = ValueEntry.SmartValue;
                if (!ValueEntry.SmartValue.isDynamic)
                {
                    var type = ValueEntry.SmartValue.validTypes[ValueEntry.SmartValue.Index];
                    RegenerateEditor(type, valueProperty);
                }
            }
        }

        /// <summary>
        /// Verifies that the value is the correct type.
        /// </summary>
        private void VerifyObjectValue(ContextObject entryValue)
        {
            if (entryValue.isDynamic)
            {
                return;
            }

            var type = entryValue.Type;
            var value = entryValue.value;

            if ((value == null && type.IsValueType) || (value != null && !type.IsAssignableFrom(value.GetType())))
            {
                entryValue.value = type.IsValueType ? Activator.CreateInstance(type) : null;
            }
        }

        /// <summary>
        /// Does the context menu.
        /// </summary>
        private void DoContextMenu(InspectorProperty valueProperty, ContextObject entryValue)
        {
            // Create the menu and add items to it
            var menu = new GenericMenu();

            // Dynamic params
            if (entryValue.HasDynamicParameters)
            {
                var dynamicParams = entryValue.dynamicParams;
                var groupedDynamicParams = dynamicParams.GroupBy(x => x.Category);

                foreach (var group in groupedDynamicParams)
                {
                    menu.AddSeparator("");
                    menu.AddDisabledItem(new GUIContent("• [ " + group.Key + " ]"));
                    foreach (var param in group)
                    {
                        var index = dynamicParams.IndexOf(param);
                        var guiContent = new GUIContent(string.Format("{0}* ({1})", param.Name, param.Type.GetAliasOrName(true, true)));

                        var selected = entryValue.isDynamic && entryValue.Index == index;

                        MenuFunction onSelected = () =>
                        {
                            entryValue.isDynamic = true;
                            entryValue.Index = index;
                        };

                        if (entryValue.DynamicParamIsValid(index))
                        {
                            menu.AddItem(guiContent, selected, onSelected);
                        }
                        else
                        {
                            menu.AddDisabledItem(guiContent);
                        }
                    }
                }
            }

            // Static params
            menu.AddSeparator("");
            menu.AddDisabledItem(new GUIContent("• [ Predefined Parameters ]"));
            for (int i = 0; i < entryValue.validTypes.Count; i++)
            {
                var index = i;
                var type = entryValue.validTypes[i];
                menu.AddItem
                (
                    new GUIContent(type.GetAliasOrName(true)),
                    !entryValue.isDynamic && entryValue.Index == index,
                    () =>
                    {
                        if (entryValue.isDynamic || entryValue.Index != index)
                        {
                            entryValue.isDynamic = false;
                            ChangeValueType(index, type, entryValue);
                            RegenerateEditor(type, valueProperty);
                        }
                    }
                );
            }

            // Display
            menu.ShowAsContext();
        }

        /// <summary>
        /// Changes the type of the inner value.
        /// </summary>
        private void ChangeValueType(int index, Type type, ContextObject entryValue)
        {
            // Change instance types
            entryValue.Index = index;
            entryValue.value = type.IsValueType ? Activator.CreateInstance(type) : null;
        }

        /// <summary>
        /// Regenerates the editor.
        /// </summary>
        private void RegenerateEditor(Type type, InspectorProperty valueProperty)
        {
            // Change reflected type
            TypeOfValueField.SetValue(valueProperty.Info, type);

            // Set property type (value or reference)
            PropertyTypeProp.SetValue(valueProperty.Info, PropertyType.Value, null);

            // Create new value entry (getter / setter)
            BaseValueEntryProp.SetValue(valueProperty, PropertyValueEntry.CreateAlias(valueProperty.BaseValueEntry, valueProperty.ParentType), null);
            ValueEntryProp.SetValue(valueProperty, valueProperty.BaseValueEntry, null);

            // Update children
            ChildrenProp.SetValue(valueProperty, null, null);
            ChildrenLastSeenTypeField.SetValue(valueProperty, null);
            UpdateChildrenMethod.Invoke(valueProperty, null);
        }

        #endregion
    }
    */
}
#endif