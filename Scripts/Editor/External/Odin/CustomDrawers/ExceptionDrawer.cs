﻿#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Editor.Drawers
{
    using Utilities.Editor;
    using System;
    using UnityEngine;
    using UnityEditor;

    /// <summary>
    /// Exception property drawer.
    /// </summary>
    public sealed class ExceptionDrawer : OdinValueDrawer<Exception>
    {
        /// <summary>
        /// Draws the property.
        /// </summary>
        protected override void DrawPropertyLayout(GUIContent label)
        {
            GUILayout.BeginHorizontal();
            {
                if (label != null)
                {
                    //Renders this field's prefix label.
                    //Note that since this uses EditorGUI, we need to add some GUILayout.Space to take up the Rect space in the layouting system.
                    EditorGUI.PrefixLabel(GUIHelper.GetCurrentLayoutRect(), label);

                    //Subtract 8 from the space, to line up with the rest of the inspector control column.
                    //Without this, label text will be indented a bit too much.
                    GUILayout.Space(EditorGUIUtility.labelWidth);
                }

                //Make sure all of our labels have a static width of 42 pixels, as that's all we'll need.
                GUIHelper.PushLabelWidth(15f);

                //We don't want any indent, so make sure we don't use any.
                GUIHelper.PushIndentLevel(0);

                //We'll stack our IntFields into two rows, so start a vertical layout here.
                GUILayout.BeginVertical();
                {
                    GUIHelper.PushGUIEnabled(false);
                    EditorGUILayout.LabelField(ValueEntry.SmartValue?.Message);
                    GUIHelper.PopGUIEnabled();
                }
                GUILayout.EndVertical();

                //Clean up and pop our indent and label width.
                GUIHelper.PopIndentLevel();
                GUIHelper.PopLabelWidth();
            }
            GUILayout.EndHorizontal();
        }
    }
}
#endif