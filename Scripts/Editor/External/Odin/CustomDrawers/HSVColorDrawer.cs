﻿#if UNITY_EDITOR
namespace Sirenix.OdinInspector.Editor.Drawers
{
    using System;
    using UnityEngine;

    using Sirenix.Utilities.Editor;
    using CEUtilities.Colors;

    /// <summary>
    /// Int property drawer.
    /// </summary>
    public sealed class HSVColorDrawer : OdinValueDrawer<HSVColor>
    {
        /// <summary>
        /// Draws the property.
        /// </summary>
        protected override void DrawPropertyLayout(GUIContent label)
        {
            var rgbColor = (Color)ValueEntry.SmartValue;
            var hsvColor = (HSVColor)SirenixEditorFields.ColorField(label, rgbColor);
            ValueEntry.SmartValue = hsvColor;
        }
    }
}
#endif