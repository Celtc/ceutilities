﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using CEUtilities.Helpers;
using System.Text;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Globalization;
using UnityEngine.SceneManagement;
using System;

namespace CEUtilities.Geometry
{
    public class ObjExporter : OdinEditorWindow
    {
        #region Static

        private static string lastOutputFilepath;

        [MenuItem("Tools/Export to OBJ...")]
        private static void OpenWindow()
        {
            GetWindow<ObjExporter>().Show();
        }

        #endregion

        #region Exposed fields

        private bool onlySelectedObjects = true;

        private bool includeChildren = true;

        private bool applyPosition = true;

        private bool applyRotation = true;

        private bool applyScale = true;

        private bool generateMaterials = true;

        private bool exportTextures = true;

        private bool splitObjects = true;

        #endregion Exposed fields

        #region Internal fields

        private string outputFilepath = lastOutputFilepath;

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        [ShowInInspector]
        public bool OnlySelectedObjects
        {
            get
            {
                return onlySelectedObjects;
            }

            set
            {
                onlySelectedObjects = value;
            }
        }

        [ShowInInspector]
        [ShowIf("OnlySelectedObjects")]
        [Indent]
        public bool IncludeChildren
        {
            get
            {
                return includeChildren;
            }

            set
            {
                includeChildren = value;
            }
        }

        [ShowInInspector]
        public bool SplitObjects
        {
            get
            {
                return splitObjects;
            }

            set
            {
                splitObjects = value;
            }
        }

        [ShowInInspector]
        public bool ApplyPosition
        {
            get
            {
                return applyPosition;
            }

            set
            {
                applyPosition = value;
            }
        }

        [ShowInInspector]
        public bool ApplyRotation
        {
            get
            {
                return applyRotation;
            }

            set
            {
                applyRotation = value;
            }
        }

        [ShowInInspector]
        public bool ApplyScale
        {
            get
            {
                return applyScale;
            }

            set
            {
                applyScale = value;
            }
        }

        [ShowInInspector]
        public bool GenerateMaterials
        {
            get
            {
                return generateMaterials;
            }

            set
            {
                generateMaterials = value;
            }
        }

        [ShowInInspector]
        [ShowIf("GenerateMaterials")]
        [Indent]
        public bool ExportTextures
        {
            get
            {
                return exportTextures;
            }

            set
            {
                exportTextures = value;
            }
        }
        
        public string OutputFolder => Path.GetDirectoryName(outputFilepath);
        
        public string OutputFilename => Path.GetFileName(outputFilepath);

        #endregion Properties

        #region Events methods

        protected override void OnEnable()
        {
            base.OnEnable();
        }

        #endregion Events methods

        #region Public Methods

        [Button("Export")]
        public void ExportButonClicked()
        {
            // Default name
            if (string.IsNullOrEmpty(outputFilepath))
            {
                outputFilepath = SceneManager.GetActiveScene().name + ".obj";
            }

            // Ask for path
            outputFilepath = EditorUtility.SaveFilePanel("Export OBJ", OutputFolder, OutputFilename, "obj");
            if (string.IsNullOrEmpty(outputFilepath))
            {
                // Cancelled
                return;
            }

            // Check folder
            if (!Directory.Exists(OutputFolder))
            {
                EditorUtility.DisplayDialog("Error", "The ouput directory does not exist. Create it or select a valid output path.", "OK");
                return;
            }

            // Cache
            lastOutputFilepath = outputFilepath;

            // Exports
            if (OnlySelectedObjects)
            {
                ExportSelection(OutputFilename, OutputFolder, IncludeChildren);
            }
            else
            {
                ExportScene(OutputFilename, OutputFolder);
            }
        }

        /// <summary>
        /// Exports all visible meshes from the scene
        /// </summary>
        public void ExportScene(string filename, string folderPath)
        {
            // Export
            InternalExport(SceneManager.GetActiveScene().GetRootGameObjects(), true, filename, folderPath);
        }

        /// <summary>
        /// Exports all visible meshes from selection
        /// </summary>
        public void ExportSelection(string filename, string folderPath, bool includeChildren)
        {
            InternalExport(Selection.gameObjects, includeChildren, filename, folderPath);
        }

        /// <summary>
        /// Export all visible meshes contained in the gameobjects
        /// </summary>
        public void Export(GameObject[] gameObjects, bool includeChildren, string filename, string folderPath)
        {
            // Export
            InternalExport(gameObjects, includeChildren, filename, folderPath);
        }

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Main export
        /// </summary>
        private void InternalExport(GameObject[] targets, bool includeChildren, string filename, string folderPath)
        {
            // Check for targets
            if (targets == null || targets.Length == 0)
            {
                Debug.LogWarning("[ObjExporter] No valid targets selected.");
                return;
            }

            // Outputs
            Texture[] textures;
            string modelsOBJ, materialsOBJ;

            // Progress
            Action<float, string, string> progressCallback = (progress, task, subtask) => { EditorUtility.DisplayProgressBar(task, subtask, progress);  };

            // Generate OBJs
            GeometryHelper.GenerateOBJ(targets, filename, includeChildren, SplitObjects,
                ApplyPosition, ApplyScale, ApplyRotation, GenerateMaterials, progressCallback, out modelsOBJ, out materialsOBJ, out textures);

            // Progress
            EditorUtility.DisplayProgressBar("Saving files...", "", .99f);

            // Save modelsOBJ
            var path = Path.Combine(folderPath, filename);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            File.WriteAllText(path, modelsOBJ);

            // Save materialsOBJ
            if (GenerateMaterials)
            {
                var mathpath = Path.Combine(folderPath, filename + ".mtl");
                if (File.Exists(mathpath))
                {
                    File.Delete(mathpath);
                }
                File.WriteAllText(mathpath, materialsOBJ);
            }

            // Save textures
            if (GenerateMaterials && ExportTextures && textures != null)
            {
                foreach (var tex in textures)
                {
                    ((Texture2D)tex).ExportAsPNG(folderPath, true);
                }
            }

            // Export complete, close progress dialog
            EditorUtility.ClearProgressBar();

            Debug.LogFormat("[ObjExporter] Exported to '{0}'.", path);
        }

        #endregion Methods
    }
}