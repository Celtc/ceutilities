using UnityEngine;
using UnityEngine.UI;
using UnityEditor.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;

namespace CEUtilities.Helpers
{
	public static class EditorGUILayoutHelper
    {
        /// <summary>
        /// Draw the default inspector of any child class of the specified (without inheritance)
        /// </summary>
        public static void DrawChildDefaultInspector<T>(SerializedObject serializedObject, bool hideScriptProp = true) where T : class
        {
            DrawChildDefaultInspector<T>(serializedObject, null, hideScriptProp);
        }

        /// <summary>
        /// Draw the default inspector of any child class of the specified (without inheritance)
        /// </summary>
        public static void DrawChildDefaultInspector<T>(SerializedObject serializedObject, string[] hideProps, bool hideScriptProp = true) where T : class
        {
            var iterator = serializedObject.GetIterator();
            if (!iterator.NextVisible(true)) return;

            do
            {
                // Skip current props and inherit props
                if (!PropIsFromChild(typeof(T), iterator)) continue;

                // Skip script prop
                if (hideScriptProp && iterator.propertyPath.Equals("m_Script")) continue;

                // Skip hideables
                if (hideProps != null && hideProps.Any(x => x.Equals(iterator.name))) continue;

                // Show child prop
                EditorGUILayout.PropertyField(iterator, true);

            } while (iterator.NextVisible(false));

            serializedObject.ApplyModifiedProperties();
        }

        /// <summary>
        /// Draw the default inspector of a particular class (without inheritance)
        /// </summary>
        public static void DrawClassDefaultInspector<T>(SerializedObject serializedObject, bool hideScriptProp = true) where T : class
		{
            DrawClassDefaultInspector<T>(serializedObject, null, hideScriptProp);
		}

        /// <summary>
        /// Draw the default inspector of a particular class (without inheritance)
        /// </summary>
        public static void DrawClassDefaultInspector<T>(SerializedObject serializedObject, string[] hideProps, bool hideScriptProp = true) where T : class
		{
			var iterator = serializedObject.GetIterator();
			if (!iterator.NextVisible(true)) return;

			do
			{
				// Skip base props
				if (PropIsInherit(typeof(T), iterator)) continue;

                // Skip script prop
                if (hideScriptProp && iterator.propertyPath.Equals("m_Script")) continue;

                // Skip hideables
                if (hideProps != null && hideProps.Any(x => x.Equals(iterator.name))) continue;

                // Show child prop
                EditorGUILayout.PropertyField(iterator, true);

			}while(iterator.NextVisible(false));

			serializedObject.ApplyModifiedProperties();
		}


		private static bool PropIsInherit(Type propClass, SerializedProperty prop)
		{
			var childProps = propClass.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
				.Where(p => p.DeclaringType == propClass);
			return !childProps.Any(x => x.Name == prop.name);
        }

        private static bool PropIsFromChild(Type propClass, SerializedProperty prop)
        {
            var childProps = propClass.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            return !childProps.Any(x => x.Name == prop.name);
        }
    }
}