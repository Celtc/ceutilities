﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector;
using CEUtilities.Helpers;
using UnityEditor;
using System.Linq;
using System.IO;

namespace CEUtilities.Editor
{
    public class InstanceInSelectionHelper : OdinEditorWindow
    {
        #region Exposed fields

        public bool selectInstances;

        public GameObject[] prefabs;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Instance the prefabs
        /// </summary>
        [Button(ButtonSizes.Medium)]
        [HorizontalGroup("Buttons")]
        public void Instance()
        {
            var instances = new List<GameObject>();

            foreach (var gameObject in Selection.gameObjects)
                instances.AddRange(InstanceMultiple(gameObject, prefabs));

            if (!selectInstances)
                foreach (var instance in instances)
                    EditorGUIUtility.PingObject(instance);
            else
                Selection.objects = instances.ToArray();
        }

        /// <summary>
        /// Cancel
        /// </summary>
        [Button(ButtonSizes.Medium)]
        [HorizontalGroup("Buttons")]
        public void Cancel()
        {
            Close();
        }

        #endregion Methods

        #region Non Public Methods

        private List<GameObject> InstanceMultiple(GameObject parent, GameObject[] prefabs)
        {
            var instances = new List<GameObject>();
            foreach (var prefab in prefabs)
                instances.Add(InstanceSingle(parent, prefab));
            return instances;
        }

        private GameObject InstanceSingle(GameObject parent, GameObject prefab)
        {
            var go = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
            go.transform.SetParent(parent.transform, false);
            return go;
        }

        #endregion Methods
    }
}