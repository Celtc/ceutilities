﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;

namespace CEUtilities.Helpers
{
    public static class ScriptableObjectHelper
    {
        #region Exposed fields

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties (public)

        #endregion Properties

        #region Event Methods

        #endregion Event Methods

        #region Public Methods

        /// <summary>
        ///	Will create an scriptable object at the current selected path of project
        /// </summary>
        public static ScriptableObject CreateAssetAtSelection<T>() where T : ScriptableObject
        {
            return CreateAssetAtSelection<T>(null);
        }

        /// <summary>
        ///	Will create an scriptable object at the current selected path of project
        /// </summary>
        public static ScriptableObject CreateAssetAtSelection<T>(string defaultName) where T : ScriptableObject
        {
            // Get selection folder
            var path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (string.IsNullOrEmpty(path))
                path = "Assets";
            else if (Path.GetExtension(path) != "")
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");

            return CreateAsset(typeof(T), path, defaultName);
        }


        /// <summary>
        /// Create an scriptable object at root project folder with unique name
        /// </summary>
        public static ScriptableObject CreateAsset(string className)
        {
            return CreateAsset(className, null, null);
        }

        /// <summary>
        /// Create an scriptable object at path with unique name
        /// </summary>
        public static ScriptableObject CreateAsset(string className, string path)
        {
            return CreateAsset(className, path, null);
        }

        /// <summary>
        /// Create an scriptable object at path with default unique name
        /// </summary>
        public static ScriptableObject CreateAsset(string className, string path, string defaultName)
        {
            var type = TypeHelper.GetType(className);
            return CreateAsset(type, path, defaultName);
        }


        /// <summary>
        /// Create an scriptable object at root project folder with unique name
        /// </summary>
        public static T CreateAsset<T>() where T : ScriptableObject
        {
            return (T)CreateAsset(typeof(T), null, null);
        }

        /// <summary>
        /// Create an scriptable object at path with unique name
        /// </summary>
        public static T CreateAsset<T>(string path) where T : ScriptableObject
        {
            return (T)CreateAsset(typeof(T), path, null);
        }

        /// <summary>
        /// Create an scriptable object at path with default unique name
        /// </summary>
        public static T CreateAsset<T>(string path, string defaultName) where T : ScriptableObject
        {
            return (T)CreateAsset(typeof(T), path, defaultName);
        }


        /// <summary>
        /// Duplicates the asset.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="asset">The asset.</param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static T DuplicateAsset<T>(T asset, string path, string defaultName) where T : ScriptableObject
        {
            var duplicate = CreateAsset(typeof(T), path, defaultName);
            EditorUtility.CopySerialized(asset, duplicate);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            return (T)duplicate;
        }

        #endregion Public Methods

        #region Non Public Methods

        /// <summary>
        ///	Internal.
        /// </summary>
        private static ScriptableObject CreateAsset(Type type, string path, string defaultName)
        {
            // Create folder
            if (string.IsNullOrEmpty(path))
            {
                path = "Assets";
            }
            AssetDatabaseHelper.CreateFolderTree(path);

            // Get end path
            var name = (string.IsNullOrEmpty(defaultName) ? ("New" + type.Name) : defaultName) + ".asset";
            var assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + name);

            // Create instance
            var asset = ScriptableObject.CreateInstance(type);

            // Save
            AssetDatabase.CreateAsset(asset, assetPathAndName);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            // Ping
            EditorGUIUtility.PingObject(asset);
            //EditorUtility.FocusProjectWindow();
            //Selection.activeObject = asset;

            return asset;
        }

        #endregion
    }
}