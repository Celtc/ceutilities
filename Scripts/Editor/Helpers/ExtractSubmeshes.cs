﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector;
using CEUtilities.Helpers;
using UnityEditor;
using System.Linq;
using System.IO;

namespace CEUtilities.Editor
{
    public class ExtractSubmeshes : OdinEditorWindow
    {
        #region Enums

        public enum SaveMode
        {
            Prefab,
            Scene,
            Both
        }

        #endregion

        #region Exposed fields

        [Spacing(Before = 8)]
        public bool useSelection = true;

        [HideIf("useSelection")]
        public GameObject[] gameObjects;

        [Spacing(Before = 8)]
        public bool allSubmeshes = true;

        [HideIf("allSubmeshes")]
        public int[] indexes = new int[] { 0 };

        [Spacing(Before = 5)]
        [FolderPath]
        [DisableIf("mode", SaveMode.Scene)]
        public string outputPath = "Assets";

        [Spacing(Before = 5)]
        public bool optimizeMesh = true;

        [Spacing(Before = 5)]
        public bool storeAsGameObject = true;

        [Spacing(Before = 5, After = 5)]
        [ShowIf("storeAsGameObject")]
        [Indent]
        public SaveMode mode = SaveMode.Both;

        #endregion Exposed fields

        #region Internal fields

        #endregion Internal fields

        #region Custom Events

        #endregion Custom Events

        #region Properties

        #endregion Properties

        #region Events methods

        #endregion Events methods

        #region Public Methods

        /// <summary>
        /// Extract the submeshes
        /// </summary>
        [Button(ButtonSizes.Medium)]
        [HorizontalGroup("Buttons")]
        public void Extract()
        {
            // Check output folder
            if (!AssetDatabaseHelper.ExistsRelativeFolder(outputPath))
                if (!AssetDatabaseHelper.CreateFolderTree(outputPath))
                {
                    Debug.LogError(string.Format("[ExtractSubmeshes] Couldn't create output folder '{0}'", outputPath));
                    return;
                }

            // Extract submeshes
            if (useSelection)
            {
                // Get actual targets
                GameObject[] gameObjects;
                if (useSelection)
                    gameObjects = Selection.gameObjects;
                else
                    gameObjects = this.gameObjects;

                // Extract foreach
                foreach (var gameObject in gameObjects)
                {
                    // Get mesh
                    var mesh = gameObject?.GetComponent<MeshFilter>()?.sharedMesh;
                    if (mesh == null)
                    {
                        Debug.LogWarning(string.Format("[ExtractSubmeshes] Gameobject {0} does not have a mesh filter", gameObject));
                        continue;
                    }

                    // Get indexes
                    int[] indexes;
                    if (allSubmeshes)
                        indexes = Enumerable.Range(0, mesh.subMeshCount).ToArray();
                    else
                        indexes = this.indexes;

                    // Extract
                    var submeshes = Extract(mesh, indexes, optimizeMesh);

                    // Save them
                    if (!storeAsGameObject)
                        Save(submeshes, outputPath);
                    else
                        SaveAsGameObject(gameObject, submeshes, outputPath, mode);
                }
            }
        }

        /// <summary>
        /// Cancel
        /// </summary>
        [Button(ButtonSizes.Medium)]
        [HorizontalGroup("Buttons")]
        public void Cancel()
        {
            Close();
        }

        #endregion Methods

        #region Non Public Methods

        /// <summary>
        /// Extract submeshes from mesh
        /// </summary>
        private Mesh[] Extract(Mesh mesh, int[] indexes, bool optimize)
        {
            var result = new List<Mesh>();
            foreach (int index in indexes)
            {
                try
                {
                    var submesh = mesh.GetSubmesh(index);
                    submesh.name = mesh.name + "_" + index;

                    if (optimize)
                        MeshUtility.Optimize(submesh);

                    result.Add(submesh);
                }
                catch
                {
                    Debug.LogError(string.Format("[ExtractSubmeshes] Error extracting from mesh '{0}' submesh with index '{1}'", mesh, index));
                }
            }
            return result.ToArray();
        }

        /// <summary>
        /// Save the submesh as an asset file
        /// </summary>
        private void Save(Mesh[] submeshes, string outputPath)
        {
            foreach (var submesh in submeshes)
            {
                var filename = submesh.name + ".asset";
                var filepath = Path.Combine(outputPath, filename);
                filepath = AssetDatabase.GenerateUniqueAssetPath(filepath);
                try
                {
                    AssetDatabase.CreateAsset(submesh, filepath);
                    AssetDatabase.SaveAssets();
                }
                catch
                {
                    Debug.LogError(string.Format("[ExtractSubmeshes] Error saving mesh {0} as an asset file at path {1}", filename, filepath));
                }
            }
        }

        /// <summary>
        /// Save the submeshes in a gameobject prefab
        /// </summary>
        private void SaveAsGameObject(GameObject source, Mesh[] submeshes, string outputPath, SaveMode mode)
        {
            // Get source mesh renderer
            var sourceRenderer = source.GetComponent<MeshRenderer>();

            // Create container
            var gameObject = new GameObject(source.name + "_Split");
            gameObject.transform.SetParent(source.transform.parent);
            gameObject.transform.SetSiblingIndex(source.transform.GetSiblingIndex() + 1);

            // Copy attributes
            gameObject.transform.position = source.transform.position;
            gameObject.transform.localScale = source.transform.localScale;
            gameObject.transform.rotation = source.transform.rotation;

            gameObject.layer = source.layer;
            gameObject.tag = source.tag;

            // Create an actual meshfilter for every submesh
            for (int i = 0; i < submeshes.Length; i++)
            {
                var submesh = submeshes[i];

                // Create submesh container
                var subGO = new GameObject(submesh.name);
                subGO.transform.SetParent(gameObject.transform, false);

                // Set Mesh
                subGO.AddComponent<MeshFilter>().sharedMesh = submesh;

                // Set materials
                subGO.AddComponent<MeshRenderer>().sharedMaterial = sourceRenderer.sharedMaterials[i];
            }

            // Create a prefab to store the gameobject
            if (mode != SaveMode.Scene)
            {
                var filename = gameObject.name + ".prefab";
                var filepath = Path.Combine(outputPath, filename);
                filepath = AssetDatabase.GenerateUniqueAssetPath(filepath);
                try
                {
                    // Create prefab
                    var rootPrefab = PrefabUtility.CreatePrefab(filepath, gameObject);

                    // Set prefab meshes
                    for (int i = 0; i < submeshes.Length; i++)
                    {
                        var childPrefab = rootPrefab.transform.GetChild(i);
                        var filter = childPrefab.GetComponent<MeshFilter>();
                        if (filter != null)
                        {
                            filter.sharedMesh = submeshes[i];
                            filter.sharedMesh.RecalculateBounds();

                            // Store the mesh inside the prefab
                            AssetDatabase.AddObjectToAsset(submeshes[i], rootPrefab);
                        }
                    }
                    AssetDatabase.SaveAssets();

                    // Link go
                    PrefabUtility.ConnectGameObjectToPrefab(gameObject, rootPrefab);
                }
                catch
                {
                    Debug.LogError(string.Format("[ExtractSubmeshes] Error saving container gameobject {0} as an asset file at path {1}", gameObject, filepath));
                    return;
                }
            }

            // Destroy scene GO
            if (mode == SaveMode.Prefab)
                DestroyImmediate(gameObject);
        }        

        #endregion Methods
    }
}